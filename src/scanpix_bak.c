static int scanPicsFolder(int level, const char *const * const folders, const int nrFolders, const int nrPermissions);
static int scanAndMergePicsInFolder(const int level, struct mdh_album_entry ** const entries, int * const nrEntries,
                                    struct md_album_entry * const thisAlbum, const int nrPermissions, struct mdh_album_data_by_permission * const albumData,
                                    const int currentFolder, uint32_t * const strTotalLen,
                                    int * const nrPicsWithDate, int * const nrPicsWithoutDate, struct mdh_album_description **albumDescr)
{
    struct dirent **namelist;
    int i,r,l;
    int picsDirLenSave,dataDirLenSave,n,albumNeedsParsing=1,albumHasDescriptionFile;
    int nrTotalUnknowns=0;//number of all unknown items in this albums and all sub-albums
    uint64_t sizeOfUnknownsInAlbum=0;
    int albumPathPosSave=ptrParsing->albumPathPos;

    ph_assert(*nrEntries== *nrPicsWithDate + *nrPicsWithoutDate,NULL);
    ph_assert(currentFolder>=0,NULL);

    LOG0("### Scanning pics in %s",ptrParsing->picsDir);

    //make sure picsDir ends with /
    ph_assert(ptrParsing->picsDirLen>0,NULL);
    ph_assert(ptrParsing->picsDirLen==strlen(ptrParsing->picsDir),NULL);
    if(ptrParsing->picsDir[ptrParsing->picsDirLen-1]!='/'){
        ptrParsing->picsDir[ptrParsing->picsDirLen]='/';
        ptrParsing->picsDirLen++;
        ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
    }
    picsDirLenSave=ptrParsing->picsDirLen;

    //same for dataDir
    ph_assert(ptrParsing->dataDirLen>0,NULL);
    if(ptrParsing->dataDir[ptrParsing->dataDirLen-1]!='/'){
        ptrParsing->dataDir[ptrParsing->dataDirLen]='/';
        ptrParsing->dataDirLen++;
        ptrParsing->dataDir[ptrParsing->dataDirLen]='\0';
    }
    dataDirLenSave=ptrParsing->dataDirLen;

    n=scandir(ptrParsing->picsDir, &namelist, &scandir_filter, NULL);
    LOG0close_return(n<0,-1,thisAlbum->flags|=FlagAlbumProcessingFailed,"Error scanning pictures in %s: %s",
                     ptrParsing->picsDir,strerror(errno));
    LOG0close_return(n>65535,-1,thisAlbum->flags|=FlagAlbumProcessingFailed,
                     "Too many pictures/entries in this album (%s). Exiting.",ptrParsing->picsDir);

    //we create the corresponding data folder, even if the album folder is empty
    r=dirService(ptrParsing->dataDir,1);
    if(r)
    ph_assert(!r,NULL);//we should be able to create the folder if we got so far

    //TODO: some error

    if(!n){
        //there is nothing (known) in this folder. Free data and return
        if(ptrParsing->sizeOfUnknownsInAlbum>0)
            LOG0("Folder (%s) contains no recognized media (but contains other stuff).",ptrParsing->picsDir);
        else
            LOG0("Empty folder: %s",ptrParsing->picsDir);
        free(namelist);
        return 0;
    }

    //check if we really need to parse this folder, or if we already have the data
    //first, get the modification date of the album.dat file. It should be later than the modification date of
    //this picture folder and the modofication date of all the pictures inside
    memcpy(ptrParsing->dataDir+dataDirLenSave,KAlbumFilename,strlen(KAlbumFilename)+1);
    if(!stat(ptrParsing->dataDir,&ptrParsing->statBuf)){
        //stat succeeded. There is a possibility that we do not need parsing
        time_t albumDatMtime=ptrParsing->statBuf.st_mtime;
        albumNeedsParsing=0; //we suppose it does NOT need parsing, unless we prove it actually needs

        //check the modification time of the album folder
        r=stat(ptrParsing->picsDir,&ptrParsing->statBuf);
        ph_assert(!r,NULL); //this folder should be available, we just scanned it
        if(ptrParsing->statBuf.st_mtime>=albumDatMtime){//picture folder modified after album.dat
            albumNeedsParsing=1;
            LOG0("Will parse album. Album folder modified after album.dat was created");
        } else {
            for(i=0;i<n;i++){
                memcpy(ptrParsing->picsDir+picsDirLenSave,namelist[i]->d_name,strlen(namelist[i]->d_name)+1);
                r=stat(ptrParsing->picsDir,&ptrParsing->statBuf);
                ph_assert(!r,"stat failed!");

                if(ptrParsing->statBuf.st_mtime>=albumDatMtime){//picture folder modified after album.dat
                    albumNeedsParsing=1;
                    LOG0("Will parse album. One picture (%s) was modified after album.dat was created",ptrParsing->picsDir);
                    break;
                }
            }//for
        }//else

        if(!albumNeedsParsing){
            //check the modification time of the description file
            memcpy(ptrParsing->picsDir+picsDirLenSave,KDescriptionFilename,strlen(KDescriptionFilename)+1);
            r=stat(ptrParsing->picsDir,&ptrParsing->statBuf);
            if(!r && ptrParsing->statBuf.st_mtime>=albumDatMtime){ //description file exists and its modification time is after album.dat
                albumNeedsParsing=1;
                LOG0("Will parse album. Album description file exists and it is modified after album.dat was created");
            }
        }
        ptrParsing->picsDir[picsDirLenSave]='\0';
    } else {
        albumNeedsParsing=1; //because we do not have the album.dat file
        LOG0("Will parse album. No corresponding album.dat found.");
    }

    //if we do not need parsing, we load the album from the album.dat file
    if(!albumNeedsParsing){
        //LOG0("Loading album from album.dat file.");
        //f=fopen(ptrParsing->dataDir,"r");
        //ph_assert(f,NULL);

        //TODO

        //fclose(f);
    } else {
        //we delete the tumbnails.dat file, so we recreate it
        memcpy(ptrParsing->dataDir+dataDirLenSave,KThumbnailsFilename,strlen(KThumbnailsFilename)+1);
        r=unlink(ptrParsing->dataDir);
        ph_assert(!r || errno==ENOENT,"There was an error unlinking the thumbnails file: %s",strerror(errno));
    }

    ptrParsing->dataDir[dataDirLenSave]='\0';

    //non-empty album. Allocate entries
    *entries=(struct mdh_album_entry*)ph_realloc(*entries,(*nrEntries+n)*sizeof(struct mdh_album_entry));
    albumData[0].nrEntries+=n;
    *nrEntries+=n;

    //do we have description file for this album?
    memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,KDescriptionFilename,strlen(KDescriptionFilename)+1);
    albumHasDescriptionFile=hasAlbumDescription(ptrParsing->picsDir);
    ptrParsing->picsDir[picsDirLenSave]='\0';

    //processing the photos first
    for(i=0;i<n;i++){
        struct dirent * __restrict d=namelist[i];
        l=strlen(d->d_name);
        *strTotalLen+=l+1;

        if(d->d_type==DT_UNKNOWN){
            //we must find the type
            ph_assert(strlen(ptrParsing->picsDir)==ptrParsing->picsDirLen,NULL);
            memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,d->d_name,l+1);

            if(!stat(ptrParsing->picsDir,&ptrParsing->statBuf)){
                //stat succeeded
                if(S_ISREG(ptrParsing->statBuf.st_mode))d->d_type=DT_REG;
                else if(S_ISDIR(ptrParsing->statBuf.st_mode))d->d_type=DT_DIR;
                else ph_assert(0,"Cannot find entry type. ");
            } else {
                //stat failing is not a good sign
                ph_assert(0,"stat failed");
            }
            ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
        }

        if(d->d_type==DT_DIR)continue;

        ph_assert(d->d_type==DT_REG,NULL); //there should be no other case here

        //init tempBuffer
        ph_assert(ptrParsing->tempBuf,NULL);
        ph_assert(ptrParsing->tempBufSize>=TEMP_BUF_MIN_SIZE,NULL);
        ptrParsing->tempBufPos=0;

        //is this a picture or a video?
        ph_assert(l>4,NULL); //otherwise it would not have passed the scandir filter
        char *extension=(char*)d->d_name+l-4;

        if(!strcasecmp(extension,KJpegExtension)){
            //this is a picture
            struct mdh_album_entry *c=NULL;
            int32_t captureTime=DATE_MAX;
            int8_t timezone;

            ph_assert(strlen(ptrParsing->picsDir)==ptrParsing->picsDirLen,NULL);
            memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,d->d_name,l+1);

            //get the size of this file and add it to album size
            if(!stat(ptrParsing->picsDir,&ptrParsing->statBuf)){
                //stat succeeded
                albumData[0].nrTotalPictrs++;
                albumData[0].sizeOfPictrsInAlbum+=ptrParsing->statBuf.st_size;
            } else {
                //stat failing is not a good sign
                ph_assert(0,"stat failed");
            }

            //exif part
            ExifData *ed=exif_data_new_from_file(ptrParsing->picsDir);
            if(ed){
                //get the camera date first, so we can sort it
                captureTime=getExifCaptureDate(ptrParsing->tempBuf,ed,d->d_name,&timezone);
                /* VERY DEBUG FEATURE
                if(captureTime!=DATE_MAX && (captureTime<1200000000 || captureTime>1400000000)){
                    ph_assert(0,"Interesting capture time (%d) for picture: %s",captureTime,d->d_name);
                }*/

                c=getCurrentEntry(*entries,*nrEntries,captureTime,nrPicsWithDate,nrPicsWithoutDate);
                if(ed->data && ed->size){
                    c->flags=FlagHasThumbnail;
                    thisAlbum->flags|=FlagHasThumbnail;
                } else {
                    c->flags=0;
                    LOG0("Found photo with EXIF but without thumbnail! (%s)",ptrParsing->picsDir);
                }

                //check for orientation
                get_tag(ptrParsing->tempBuf,ptrParsing->tempBufSize,ed,EXIF_IFD_0,EXIF_TAG_ORIENTATION);
                if(strlen(ptrParsing->tempBuf)>=8){
                    //it can either be e.g. Top-left or top - left
                    if(!strcmp(ptrParsing->tempBuf,"Top-left") || !strcmp(ptrParsing->tempBuf,"top - left"));
                    else if(!strcmp(ptrParsing->tempBuf,"Left-bottom") || !strcmp(ptrParsing->tempBuf,"left - bottom"))
                        c->flags|=FlagRotatePlus90+FlagRotatePlus180;
                    else if(!strcmp(ptrParsing->tempBuf,"Bottom-right") || !strcmp(ptrParsing->tempBuf,"bottom - right"))
                        c->flags|=FlagRotatePlus180;
                    else if(!strcmp(ptrParsing->tempBuf,"Right-top") || !strcmp(ptrParsing->tempBuf,"right - top"))
                        c->flags|=FlagRotatePlus90;
                    else LOG0("UNKNOWN ORIENTATION: #%s#",ptrParsing->tempBuf);
                } else ph_assert(strlen(ptrParsing->tempBuf)==0,"UNKNOWN ORIENTATION (2): %s",ptrParsing->tempBuf);
                //LOG0("Flags: %d (%s)",c->flags,ptrParsing->tempBuf);

                //check for GPS data
                getExifGPSCoordinates(ptrParsing->tempBuf,ptrParsing->tempBufSize,ed,&c->lat,&c->lng,d->d_name);
                if(c->lat!=LAT_LNG_ERROR){
                    ph_assert(c->lng!=LAT_LNG_ERROR,NULL);
                    c->flags|=FlagPhotoHasGPSInfo;
                }
                //GPS data done

                //Check for various image-related data
                ExifRational exifRat;
                ExifShort exifShort;
                //Focal length
                get_tag_rational(ed,EXIF_IFD_EXIF,EXIF_TAG_FOCAL_LENGTH,&exifRat);
                //if no focal length, it defaults to 0/0, so assignment below is safe
                if(exifRat.denominator)
                    c->focalLength=(float)exifRat.numerator/(float)exifRat.denominator;
                else
                    c->focalLength=0.0f;

                //LOG0("FOCAL LENGTH: %f (%u/%u)",c->focalLength,(unsigned int)exifRat.numerator,(unsigned int)exifRat.denominator);
                //get the make and model
                c->entriesIndex=getExifCameraEntry(ptrParsing->tempBuf,ptrParsing->tempBufSize,ed,c->focalLength);

                //Focal length 35mm (int 16bit)
                get_tag_short(ed,EXIF_IFD_EXIF,EXIF_TAG_FOCAL_LENGTH_IN_35MM_FILM,&exifShort);
                c->focalLength35=exifShort; //if not found, defaults to 0

                //exposure
                r=get_tag_rational(ed,EXIF_IFD_EXIF,EXIF_TAG_EXPOSURE_TIME,&exifRat);
                if(exifRat.denominator)
                    c->exposureTime=(float)exifRat.numerator/(float)exifRat.denominator;
                else
                    c->exposureTime=0.0f;

                //exposure program
                get_tag_short(ed,EXIF_IFD_EXIF,EXIF_TAG_EXPOSURE_PROGRAM,&exifShort);
                c->exposureProgram=exifShort; //if not found, defaults to 0, which is fine

                //F number
                r=get_tag_rational(ed,EXIF_IFD_EXIF,EXIF_TAG_FNUMBER,&exifRat);
                if(exifRat.denominator)
                    c->fNumber=(float)exifRat.numerator/(float)exifRat.denominator;
                else
                    c->fNumber=0.0f;

                //ISO
                get_tag_short(ed,EXIF_IFD_EXIF,EXIF_TAG_ISO_SPEED_RATINGS,&exifShort);
                c->iso=exifShort; //if not found, defaults to 0, which is fine

                //do we have a timezone?
                c->timezone=timezone;
                if(timezone!=TIMEZONE_UNAVAILABLE){
                    ph_assert(timezone>=-12 && timezone<=13,NULL);
                    //c->flags|=FlagHasTimezoneByte;
                }
                //zero width and height (will be discovered later)
                c->width=c->height=0;

                //reserve space for offset in description file and for permission byte
                ph_assert(ptrParsing->tempBufPos==0,NULL);
                if(albumHasDescriptionFile){
                    *(char**)(ptrParsing->tempBuf+ptrParsing->tempBufPos)=NULL;
                    *(uint8_t*)(ptrParsing->tempBuf+ptrParsing->tempBufPos+sizeof(void*))=0;
                    ptrParsing->tempBufPos+=sizeof(void*)+1;
                    c->flags|=FlagTemp;
                }

                if(!level)
                    *(uint8_t*)(ptrParsing->tempBuf+ptrParsing->tempBufPos++)=currentFolder;


                //add this point we can add the filename to the buffer
                ptrParsing->tempBufLen=strlen(d->d_name)+1;
                ph_assert(ptrParsing->tempBufLen<TEMP_BUF_MIN_SIZE,NULL);
                memcpy(ptrParsing->tempBuf+ptrParsing->tempBufPos,d->d_name,ptrParsing->tempBufLen);
                ptrParsing->tempBufPos+=ptrParsing->tempBufLen;
                REALLOCATE_TEMP_IF_NEEDED;

                //check if there is any image description
                get_tag(ptrParsing->tempBuf+ptrParsing->tempBufPos,ptrParsing->tempBufSize-ptrParsing->tempBufPos,
                        ed,EXIF_IFD_0,EXIF_TAG_IMAGE_DESCRIPTION);
                ptrParsing->tempBufLen=strlen(ptrParsing->tempBuf+ptrParsing->tempBufPos);
                if(ptrParsing->tempBufLen>0){
                    //LOG0("THIS PICTURE (%s) HAS DESCRIPTION: %s (length: %d) at %p",d->d_name,ptrParsing->tempBuf+ptrParsing->tempBufPos,strlen(ptrParsing->tempBuf+ptrParsing->tempBufPos),ptrParsing->tempBuf+ptrParsing->tempBufPos);
                    c->flags|=FlagHasDescription;
                    ptrParsing->tempBufPos+=ptrParsing->tempBufLen+1;
                    REALLOCATE_TEMP_IF_NEEDED;
                }

                //check if there are user comments
                get_tag(ptrParsing->tempBuf+ptrParsing->tempBufPos,ptrParsing->tempBufSize-ptrParsing->tempBufPos,
                        ed,EXIF_IFD_EXIF,EXIF_TAG_USER_COMMENT);
                ptrParsing->tempBufLen=strlen(ptrParsing->tempBuf+ptrParsing->tempBufPos);
                if(ptrParsing->tempBufLen>0){
                    //LOG0("THIS PICTURE (%s) HAS USER COMMENT: %s",d->d_name,ptrParsing->tempBuf+ptrParsing->tempBufPos);
                    c->flags|=FlagHasUserComment;
                    ptrParsing->tempBufPos+=ptrParsing->tempBufLen+1;
                    REALLOCATE_TEMP_IF_NEEDED;
                }

                /*
                show_tag(ed, EXIF_IFD_0, EXIF_TAG_ARTIST);
                show_tag(ed, EXIF_IFD_0, EXIF_TAG_XP_AUTHOR);
                show_tag(ed, EXIF_IFD_0, EXIF_TAG_COPYRIGHT);


                show_tag(ed, EXIF_IFD_EXIF, EXIF_TAG_USER_COMMENT);
                show_tag(ed, EXIF_IFD_0, EXIF_TAG_IMAGE_DESCRIPTION);
                show_tag(ed, EXIF_IFD_1, EXIF_TAG_IMAGE_DESCRIPTION);
                */



                //we are done with EXIF data
                //LOG0("##########################################################");
                //exif_data_dump(ed);
                //show_tag(ed, EXIF_IFD_0, EXIF_TAG_MAKE);

                //free the exif data
                exif_data_unref(ed);
                ed=NULL;
            }
            else {
                //LOG0("%s has no EXIF data", d->d_name);

                captureTime=getDateTimeFromString(d->d_name,1,1);
                c=getCurrentEntry(*entries,*nrEntries,captureTime,nrPicsWithDate,nrPicsWithoutDate);
                //set some other vars
                c->entriesIndex=CAMERA_INDEX_NO_CAMERA_INFO;
                c->flags=0; //we do not have much data
                c->focalLength35=0;
                c->iso=0;
                c->exposureProgram=0;
                c->timezone=TIMEZONE_UNAVAILABLE;
                c->focalLength=0.0f;
                c->exposureTime=0.0f;
                c->fNumber=0.0f;

                //zero width and height (will be discovered later)
                c->width=c->height=0;

                //reserve space for offset in description file and for permission byte
                ph_assert(ptrParsing->tempBufPos==0,NULL);
                if(albumHasDescriptionFile){
                    *(char**)(ptrParsing->tempBuf+ptrParsing->tempBufPos)=NULL;
                    *(uint8_t*)(ptrParsing->tempBuf+ptrParsing->tempBufPos+sizeof(void*))=0;
                    ptrParsing->tempBufPos+=sizeof(void*)+1;
                    c->flags|=FlagTemp;
                }
                if(!level)
                    *(uint8_t*)(ptrParsing->tempBuf+ptrParsing->tempBufPos++)=currentFolder;

                //copy filename to tempBuf
                ph_assert(l==strlen(d->d_name),NULL);
                l++;
                ph_assert(l<TEMP_BUF_MIN_SIZE,NULL);
                memcpy(ptrParsing->tempBuf+ptrParsing->tempBufPos,d->d_name,l);
                ptrParsing->tempBufPos+=l;
                REALLOCATE_TEMP_IF_NEEDED;
            }
            //copy p->tempBuf to c->data
            c->data=(char*)ph_malloc(ptrParsing->tempBufPos);
            memcpy(c->data,ptrParsing->tempBuf,ptrParsing->tempBufPos);
            c->size=ptrParsing->statBuf.st_size;
            //change earliestCaptureTime and/or latestCaptureTime
            if(captureTime!=DATE_MAX){
                if(albumData[0].earliestCaptureTime>captureTime)albumData[0].earliestCaptureTime=captureTime;
                if(albumData[0].latestCaptureTime==DATE_MAX || albumData[0].latestCaptureTime<captureTime)
                    albumData[0].latestCaptureTime=captureTime;
            } else thisAlbum->flags|=FlagAlbumHasUndatedEntries;

            //done for this picture
            ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
        } else if(!strcasecmp(extension,KMp4Extension) || !strcasecmp(extension,KMovExtension)){
            //this is a movie
            struct mdh_album_entry *c=NULL;
            int32_t captureTime=DATE_MAX;


            ph_assert(strlen(ptrParsing->picsDir)==ptrParsing->picsDirLen,NULL);
            memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,d->d_name,l+1);

            //get the size of this file and add it to album size
            if(!stat(ptrParsing->picsDir,&ptrParsing->statBuf)){
                //stat succeeded
                albumData[0].nrTotalVideos++;
                albumData[0].sizeOfVideosInAlbum+=ptrParsing->statBuf.st_size;
            } else {
                //stat failing is not a good sign
                ph_assert(0,"stat failed");
            }

            captureTime=getDateTimeFromString(d->d_name,1,1);
            c=getCurrentEntry(*entries,*nrEntries,captureTime,nrPicsWithDate,nrPicsWithoutDate);
            //set some other vars
            c->entriesIndex=CAMERA_INDEX_NO_CAMERA_INFO;
            c->flags=FlagIsVideo; //we do not have much data about videos yet
            //zero width and height (will be discovered later)
            c->width=c->height=0;

            //reserve space for offset in description file and for permission byte
            ph_assert(ptrParsing->tempBufPos==0,NULL);
            if(albumHasDescriptionFile){
                *(char**)(ptrParsing->tempBuf+ptrParsing->tempBufPos)=NULL;
                *(uint8_t*)(ptrParsing->tempBuf+ptrParsing->tempBufPos+sizeof(void*))=0;
                ptrParsing->tempBufPos+=sizeof(void*)+1;
                c->flags|=FlagTemp;
            }
            if(!level)
                *(uint8_t*)(ptrParsing->tempBuf+ptrParsing->tempBufPos++)=currentFolder;

            //copy filename to tempBuf
            ph_assert(l==strlen(d->d_name),NULL);
            l++;
            ph_assert(l<TEMP_BUF_MIN_SIZE,NULL);
            memcpy(ptrParsing->tempBuf+ptrParsing->tempBufPos,d->d_name,l);
            ptrParsing->tempBufPos+=l;
            REALLOCATE_TEMP_IF_NEEDED;

            //copy p->tempBuf to c->data
            c->data=(char*)ph_malloc(ptrParsing->tempBufPos);
            memcpy(c->data,ptrParsing->tempBuf,ptrParsing->tempBufPos);
            c->size=ptrParsing->statBuf.st_size;
            //change earliestCaptureTime and/or latestCaptureTime
            if(captureTime!=DATE_MAX){
                if(albumData[0].earliestCaptureTime>captureTime)albumData[0].earliestCaptureTime=captureTime;
                if(albumData[0].latestCaptureTime==DATE_MAX || albumData[0].latestCaptureTime<captureTime)
                    albumData[0].latestCaptureTime=captureTime;
            } else thisAlbum->flags|=FlagAlbumHasUndatedEntries;

            //done for this video
            ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
        }
        else ph_assert(0,NULL); //there should be no other case here
    }
    ph_assert(picsDirLenSave==strlen(ptrParsing->picsDir),NULL);
    ph_assert(dataDirLenSave==strlen(ptrParsing->dataDir),NULL);

    //now process folders/albums
    for(i=0;i<n;i++){
        struct dirent * __restrict d=namelist[i];
        if(d->d_type==DT_DIR){
            struct mdh_album_entry *c=NULL;
            struct md_album_entry album4children; //passed to children, so they put data into it
            int offset;
            struct mdh_album_data_by_permission *dataAlbumData; //chached from current entry data

            //add our dir name to picsDir and dataDir
            l=strlen(d->d_name);
            memcpy(ptrParsing->picsDir+picsDirLenSave,d->d_name,l+1);
            ptrParsing->picsDirLen=picsDirLenSave+l;
            memcpy(ptrParsing->dataDir+dataDirLenSave,d->d_name,l+1);
            ptrParsing->dataDirLen=dataDirLenSave+l;
            ptrParsing->album=&album4children;
            //add our album name to albumPath
            r=regexec(&ptrParsing->rxd,d->d_name,1,&ptrParsing->rxdm,0);
            ph_assert(r || ptrParsing->rxdm.rm_so==0,NULL); //this means that, if there is a match, it starts from beginning of the string
            if(!r && ptrParsing->rxdm.rm_so==0)//we can take out the identified expression
                offset=ptrParsing->rxdm.rm_eo;
            else offset=0;
            //TODO: if we have a name in the album description, we should use that instead of the filename!!
            if(l==offset)//no other name, just a date
                offset=0;
            memcpy(ptrParsing->albumPath+albumPathPosSave,d->d_name+offset,l+1-offset);
            ptrParsing->albumPathPos=albumPathPosSave+l+1-offset;

            //do the scanning
            r=scanPicsFolder(level+1,NULL,0,nrPermissions);
            ptrParsing->picsDir[ptrParsing->picsDirLen]='\0'; //TODO: move this to scanPicsFolder
            if(r>=0)albumData[0].nrTotalAlbums++;


            //check if we have a date here
            if(album4children.earliestCaptureTime==DATE_MAX && offset>0){
                LOG0("NO DATE for this album, but we can get one from the name!");
                album4children.earliestCaptureTime=getDateTimeFromString(d->d_name,0,0);
            }

            c=getCurrentEntry(*entries,*nrEntries,album4children.earliestCaptureTime,nrPicsWithDate,nrPicsWithoutDate);
            c->flags=album4children.flags;
            c->entriesIndex=album4children.entriesIndex;
            if(r<0){
                LOG0("Could not process folder: %s",ptrParsing->picsDir);
                ph_assert(c->flags&FlagAlbumProcessingFailed,NULL);
                //TODO: anything else to do here?
            }

            //fill album data

            c->size=ptrParsing->albumData[0].sizeOfPictrsInAlbum+ptrParsing->albumData[0].sizeOfVideosInAlbum;
            c->unsupportedSize=ptrParsing->sizeOfUnknownsInAlbum;
            c->nrTotalPictures=ptrParsing->albumData[0].nrTotalPictrs;
            c->nrTotalVideos=ptrParsing->albumData[0].nrTotalVideos;
            c->nrTotalSubalbums=ptrParsing->albumData[0].nrTotalAlbums;
            c->nrTotalUnknowns=ptrParsing->nrTotalUnknowns;
            c->latestCaptureTime=album4children.latestCaptureTime;
            c->albumData=album4children.album;
            //albumDataByPermission
            r=sizeof(struct mdh_album_data_by_permission)*(nrPermissions+1);
            c->albumDataByPermission=dataAlbumData=(struct mdh_album_data_by_permission*)ph_malloc(r);
            //fill-in dataAlbumData from ptrParsing->albumData
            memcpy(dataAlbumData,ptrParsing->albumData,r);
            //fill in some albumData fields
            albumData[0].nrTotalPictrs+=ptrParsing->albumData[0].nrTotalPictrs;
            albumData[0].nrTotalVideos+=ptrParsing->albumData[0].nrTotalVideos;
            albumData[0].nrTotalAlbums+=ptrParsing->albumData[0].nrTotalAlbums;
            albumData[0].sizeOfPictrsInAlbum+=ptrParsing->albumData[0].sizeOfPictrsInAlbum;
            albumData[0].sizeOfVideosInAlbum+=ptrParsing->albumData[0].sizeOfVideosInAlbum;
            //check earliestCaptureDate and latestCaptureDate
            if(dataAlbumData[0].earliestCaptureTime!=DATE_MAX && dataAlbumData[0].earliestCaptureTime<albumData[0].earliestCaptureTime)
                albumData[0].earliestCaptureTime=dataAlbumData[0].earliestCaptureTime;
            if(dataAlbumData[0].latestCaptureTime!=DATE_MAX){
                if(albumData[0].latestCaptureTime==DATE_MAX)albumData[0].latestCaptureTime=dataAlbumData[0].latestCaptureTime;
                else if(dataAlbumData[0].latestCaptureTime>albumData[0].latestCaptureTime)
                    albumData[0].latestCaptureTime=dataAlbumData[0].latestCaptureTime;
            }

            //reserve space for offset in description file and for permission byte
            ptrParsing->tempBufPos=0;
            if(albumHasDescriptionFile){
                *(char**)(ptrParsing->tempBuf+ptrParsing->tempBufPos)=NULL;
                *(uint8_t*)(ptrParsing->tempBuf+ptrParsing->tempBufPos+sizeof(void*))=0;
                ptrParsing->tempBufPos+=sizeof(void*)+1;
                c->flags|=FlagTemp;
            }

            if(offset){
                ph_assert(offset>0,NULL);
                c->flags|=FlagHasNameOffset;
                *(uint8_t*)(ptrParsing->tempBuf+ptrParsing->tempBufPos++)=offset;
            }

            if(!level)
                *(uint8_t*)(ptrParsing->tempBuf+ptrParsing->tempBufPos++)=currentFolder;

            //add the filename
            memcpy(ptrParsing->tempBuf+ptrParsing->tempBufPos,d->d_name,l+1);
            ptrParsing->tempBufPos+=l+1;
            //allocate an entry for this album data
            c->data=(char*)ph_malloc(ptrParsing->tempBufPos);
            memcpy(c->data,ptrParsing->tempBuf,ptrParsing->tempBufPos);
            nrTotalUnknowns+=ptrParsing->nrTotalUnknowns;
            sizeOfUnknownsInAlbum+=ptrParsing->sizeOfUnknownsInAlbum;

            //check for pictures without date
            if(album4children.flags&FlagAlbumHasUndatedEntries)
                thisAlbum->flags|=FlagAlbumHasUndatedEntries;
            if(album4children.flags&FlagHasThumbnail)
                thisAlbum->flags|=FlagHasThumbnail;

            //done for this album/folder
        }
        free(d);
    }

    free(namelist); //we do not need it any more
    ptrParsing->picsDirLen=picsDirLenSave;
    ptrParsing->dataDirLen=dataDirLenSave;
    ptrParsing->albumPathPos=albumPathPosSave;
    ptrParsing->picsDir[picsDirLenSave]='\0';
    ptrParsing->dataDir[dataDirLenSave]='\0';
    ptrParsing->albumPath[ptrParsing->albumPathPos]='\0';
    ph_assert(*nrEntries== *nrPicsWithDate + *nrPicsWithoutDate,NULL);


    //get the album description for this album
    if(albumHasDescriptionFile){
        memcpy(ptrParsing->picsDir+picsDirLenSave,KDescriptionFilename,strlen(KDescriptionFilename)+1);
        *albumDescr=getAlbumDescription(ptrParsing->picsDir); //returned struct can be NULL if there is no description
        LOG0c(!(*albumDescr),"WARNING: The album description file (%s) could not be opened and parsed!",ptrParsing->picsDir);
        ptrParsing->picsDir[picsDirLenSave]='\0';

        //find elements description in the description file
        l=0;
        for(i=0;i<*nrEntries;i++)
            if((*entries)[i].flags&FlagTemp)
                setEntryDescription(&(*entries)[i],*albumDescr,level); //this function also removes the FlagTemp
        //done with finding descriptions
        checkAndFreeAlbumEntries(*albumDescr);
    } else *albumDescr=NULL;

    return 0;
}

static int scanPicsFolder(int level, const char *const * const folders, const int nrFolders, const int nrPermissions)
{
    struct mdh_album_entry *entries=NULL;
    struct md_album_entry *album,* const thisAlbum=ptrParsing->album;

    int i,j,r,l,nrEntries=0;
    int thumbnailsNeedCreation=1;
    const int albumPathPosSave=ptrParsing->albumPathPos;
    //album info
    int nrPicsWithDate=0,nrPicsWithoutDate=0; //we have pictures with date in front, pics without date at the end, and folders between
    uint32_t strTotalLen=0,strAlbumPos;
    //cached from ptrParsing
    struct mdh_album_data_by_permission *albumData;
    int nrTotalUnknowns;//number of all unknown items in this albums and all sub-albums
    uint64_t sizeOfUnknownsInAlbum;
    struct mdh_album_data_by_permission *dataAlbumData; //chached from current entry data
    //either one of the next 2 pointers are used
    struct mdh_album_description *albumDescriptor=NULL; //for one folder
    struct mdh_album_description **albumDescriptors=NULL; //for multiple folders

    FILE *f;//album.dat file
    FILE *thumbnailF1=NULL;//file containing thumbnails

    char *t;//pointer to something, do not free
    char *tEscaped=NULL;
    time_t startTime=time(NULL);


    ph_assert(level<7,"Too many levels (%d - max: 7)",level);

    thisAlbum->flags=FlagIsAlbum;
    thisAlbum->entriesIndex=0;
    thisAlbum->earliestCaptureTime=DATE_MAX;
    thisAlbum->latestCaptureTime=DATE_MAX;
    thisAlbum->album=NULL; //for the moment

    nrTotalUnknowns=ptrParsing->nrTotalUnknowns=0;
    sizeOfUnknownsInAlbum=ptrParsing->sizeOfUnknownsInAlbum=0;
    ptrParsing->albumAllocatedSize=0; //not yet allocated our album
    //allocate albumData
    albumData=(struct mdh_album_data_by_permission*)ph_malloc(sizeof(struct mdh_album_data_by_permission)*(nrPermissions+1));
    for(j=0;j<nrPermissions+1;j++){
        ptrParsing->albumData[j].nrEntries=0;
        ptrParsing->albumData[j].earliestCaptureTime=DATE_MAX;
        ptrParsing->albumData[j].latestCaptureTime=DATE_MAX;
        ptrParsing->albumData[j].nrTotalPictrs=0;
        ptrParsing->albumData[j].nrTotalVideos=0;
        ptrParsing->albumData[j].nrTotalAlbums=0;
        ptrParsing->albumData[j].sizeOfPictrsInAlbum=0;
        ptrParsing->albumData[j].sizeOfVideosInAlbum=0;

        albumData[j].nrEntries=0;
        albumData[j].earliestCaptureTime=DATE_MAX;
        albumData[j].latestCaptureTime=DATE_MAX;
        albumData[j].nrTotalPictrs=0;
        albumData[j].nrTotalVideos=0;
        albumData[j].nrTotalAlbums=0;
        albumData[j].sizeOfPictrsInAlbum=0;
        albumData[j].sizeOfVideosInAlbum=0;
    };

    if(nrFolders>0){
        ph_assert(folders,NULL);
        ph_assert(ptrParsing->picsDirLen==0,NULL);
        ph_assert(ptrParsing->dataDirLen==0,NULL);
        ph_assert(!level,NULL);

        albumDescriptors=(struct mdh_album_description **)ph_malloc(nrFolders*sizeof(struct mdh_album_description *));
        //set all the albumDescriptors to NULL
        memset(albumDescriptors,0,nrFolders*sizeof(struct mdh_album_description *));

        for(i=0;i<nrFolders;i++){
            //prepare picsDir and dataDir
            l=strlen(folders[i]);

            memcpy(ptrParsing->picsDir,folders[i],l);
            memcpy(ptrParsing->dataDir,folders[i],l);
            r=strlen(KPicturesFolder);
            memcpy(ptrParsing->picsDir+l,KPicturesFolder,r+1);
            ptrParsing->picsDirLen=l+r;
            r=strlen(KPhotostovisData);
            memcpy(ptrParsing->dataDir+l,KPhotostovisData,r+1);
            ptrParsing->dataDirLen=l+r;

            //scan
            r=scanAndMergePicsInFolder(level,&entries,&nrEntries,thisAlbum,nrPermissions,albumData,i,
                                       &strTotalLen,&nrPicsWithDate,&nrPicsWithoutDate,&albumDescriptors[i]);
            //TODO: check r
            //merge some data
            nrTotalUnknowns+=ptrParsing->nrTotalUnknowns;
            sizeOfUnknownsInAlbum+=ptrParsing->sizeOfUnknownsInAlbum;
        }
        //we need to leave this branch with valid picsDir and dataDir (s)
        //we will be using the first folder
        l=strlen(folders[0]);
        memcpy(ptrParsing->picsDir,folders[0],l);
        memcpy(ptrParsing->dataDir,folders[0],l);
        r=strlen(KPicturesFolder);
        memcpy(ptrParsing->picsDir+l,KPicturesFolder,r+1);
        ptrParsing->picsDirLen=l+r;
        r=strlen(KPhotostovisData);
        memcpy(ptrParsing->dataDir+l,KPhotostovisData,r+1);
        ptrParsing->dataDirLen=l+r;
        //make sure folders end with '/'
        if(ptrParsing->picsDir[ptrParsing->picsDirLen-1]!='/'){
            ptrParsing->picsDir[ptrParsing->picsDirLen++]='/';
            ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
        }
        if(ptrParsing->dataDir[ptrParsing->dataDirLen-1]!='/'){
            ptrParsing->dataDir[ptrParsing->dataDirLen++]='/';
            ptrParsing->dataDir[ptrParsing->dataDirLen]='\0';
        }
    } else {
        ph_assert(!folders,NULL);
        ph_assert(ptrParsing->picsDirLen>0,NULL);
        ph_assert(ptrParsing->dataDirLen>0,NULL);
        r=scanAndMergePicsInFolder(level,&entries,&nrEntries,thisAlbum,nrPermissions,albumData,0,
                                   &strTotalLen,&nrPicsWithDate,&nrPicsWithoutDate,&albumDescriptor);
        //TODO: check r
        //merge some data
        nrTotalUnknowns+=ptrParsing->nrTotalUnknowns;
        sizeOfUnknownsInAlbum+=ptrParsing->sizeOfUnknownsInAlbum;
    }

    //At this point we have all the relevant info in the entries structure. We need to sort it and create the album structure

    //sort the pictures without date
    ph_assert(nrPicsWithDate+nrPicsWithoutDate==nrEntries,NULL);
    if(nrPicsWithoutDate>0){
        LOG0("Sorting pictures in %s",ptrParsing->picsDir);
        /*
        LOG0("Pics with date:");
        for(i=0;i<nrPicsWithDate;i++){
            //get the current entry
            struct mdh_album_entry *ci=&(entries[i]);
            char *nameI=getFilenameFromEntry(ci,level);

            LOG0("%s",nameI);
        }*/


        //we should sort the pictures without date by name
        for(i=nrPicsWithDate;i<nrEntries;i++){
            //get the current entry
            struct mdh_album_entry *ci=&(entries[i]);
            char *nameI=getFilenameFromEntry(ci,level);

            //LOG0("Sorting %s",nameI);

            for(j=0;j<i;j++){
                struct mdh_album_entry *cCurrent=&(entries[j]);
                char *nameCurrent=getFilenameFromEntry(cCurrent,level);

                if(strcasecmp(nameI,nameCurrent)<0){
                    //we insert i between prev and current
                    LOG2("Inserting %s before %s",nameI,nameCurrent);
                    struct mdh_album_entry temp=*ci;
                    memmove(entries+j+1,entries+j,(i-j)*sizeof(struct mdh_album_entry));
                    *cCurrent=temp;

                    //we are done for this element
                    break;
                }
            }//for(j=...
        }//for(i=...

        //lets print what we have sorted
        LOG0("We sorted %d pictures here.",nrPicsWithoutDate);
    }
    //here we have the pictures sorted

    //check the original picsDir & dataDir
    ph_assert(ptrParsing->albumPathPos==albumPathPosSave,NULL);
    ph_assert(ptrParsing->picsDir[ptrParsing->picsDirLen-1]=='/',NULL);//we need the picsDir path ending with /
    ph_assert(ptrParsing->dataDir[ptrParsing->dataDirLen-1]=='/',NULL);//we need the dataDir path ending with /

    //do we need to create thumbnails? If so, do it here
    ph_assert(sizeof(uint32_t)==4,NULL); //otherwise some of the size assumptions below do not work
    int deleteOldThumbnailsFile=0; //we do not delete it unless needed
    memcpy(ptrParsing->dataDir+ptrParsing->dataDirLen,KThumbnailsFilename,strlen(KThumbnailsFilename)+1);
    ph_assert(thumbnailsNeedCreation,NULL);
    //check if there is an old thumbnail file and if we need to delete it
    thumbnailF1=fopen(ptrParsing->dataDir,"r");
    if(thumbnailF1){
        struct md_thmbfile_header thmbfileHeader;
        struct md_thumbnails *thmbEntries=NULL;
        r=fread(&thmbfileHeader,sizeof(struct md_thmbfile_header),1,thumbnailF1);
        if(r!=1 || thmbfileHeader.version!=CURRENT_THUMBNAILS_FILE_VERSION){
            deleteOldThumbnailsFile=1;
            LOG0("Delete old thumbnails file because r(%d)!=1 or thumb file version (%d)!=current version (%d)",
                 r,thmbfileHeader.version,CURRENT_THUMBNAILS_FILE_VERSION);
        }

        if(!deleteOldThumbnailsFile){//we can check further
            if(thmbfileHeader.nrEntries!=nrEntries){
                deleteOldThumbnailsFile=1;
                LOG0("Delete old thumbnails file because nrThumbnails(%d)!=nrEntries(%d)",
                     thmbfileHeader.nrEntries,nrEntries);
            }
        }

        if(!deleteOldThumbnailsFile){//we can check further
            thmbEntries=ph_malloc((nrEntries+1)*sizeof(struct md_thumbnails));//+1 for the size
            r=fread(thmbEntries,nrEntries*sizeof(struct md_thumbnails),1,thumbnailF1);
            if(r!=1){
                deleteOldThumbnailsFile=1;
                LOG0("Delete old thumbnails file because r(%d)!=1",r);
            }
            thmbEntries[nrEntries].size=0;
            fseek(thumbnailF1,0,SEEK_END);
            thmbEntries[nrEntries].offset=ftell(thumbnailF1);
        }
        fclose(thumbnailF1);//not needed any more

        if(!deleteOldThumbnailsFile)//we can check further
            for(i=0;i<nrEntries;i++){
                if((entries[i].size&0xFFFFFFFF)!=thmbEntries[i].size){
                    deleteOldThumbnailsFile=1;
                    LOG0("Delete old thumbnails file because sizes are different (%u!=%u) for i=%d",
                         (unsigned int)(entries[i].size&0xFFFFFFFF),(unsigned int)thmbEntries[i].size,i);
                    break;
                } else {
                    if(thmbEntries[i+1].offset-thmbEntries[i].offset>0)
                        entries[i].flags|=FlagTemp; //we mark it, it has a valid thumbnail
                    if(!(entries[i].flags&FlagIsAlbum)){
                        entries[i].width=thmbEntries[i].width;
                        entries[i].height=thmbEntries[i].height;
                    }
                }
            }
        //here we should have an answer
        if(!deleteOldThumbnailsFile){
            thumbnailsNeedCreation=0; //the old file is fine!
            //LOG0("¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤ Old thumbnail file is fine!");
            ptrParsing->nrThumbnailFiles++;
            ptrParsing->sizeOfThumbnailFiles+=thmbEntries[nrEntries].offset;
        } else LOG0("Thumbnail file will be (re)created");
        LOG0("Freeing thmbEntries (%p)",thmbEntries);
        free(thmbEntries);
    }

    /*} else //does the file exist?
        if(!stat(ptrParsing->dataDir,&ptrParsing->statBuf)){
            //stat succeeded, the file exists and we do not need it.
            deleteOldThumbnailsFile=0;//deleted
            r=unlink(ptrParsing->dataDir);
            //TODO: handle the error
            LOG0c(r,"ERROR unlinking the thumbnails file: %s",strerror(errno));
        }*/
    int tempScannedEntries=0;
    if(thumbnailsNeedCreation){
        //we create the thumbnails file
        thumbnailF1=fopen(ptrParsing->dataDir,"w+");
        setOwnership(ptrParsing->dataDir,0);
        ph_assert(thumbnailF1,"ERROR opening the thumbnails1 file (%s): %s",ptrParsing->dataDir,strerror(errno));
        //open the second (retina) thumbnail file
        //first, get the name right
        r=strlen(ptrParsing->dataDir);
        ph_assert(ptrParsing->dataDir[r-5]=='1',NULL);
        ptrParsing->dataDir[r-5]='2';
        FILE *thumbnailF2=fopen(ptrParsing->dataDir,"w+");
        setOwnership(ptrParsing->dataDir,0);
        ph_assert(thumbnailF2,"ERROR opening the thumbnails2 file (%s): %s",ptrParsing->dataDir,strerror(errno));
        //TODO: handle thumbnailF1==NULL and/or thumbnailF2==NULL

        //fill in the metadata part
        //int initialNrEntries=nrEntries;
        long thmbEntriesSize=sizeof(struct md_thmbfile_header)+nrEntries*sizeof(struct md_thumbnails),pos1,pos2;
        void *thumbMem1=ph_malloc(thmbEntriesSize);
        void *thumbMem2=ph_malloc(thmbEntriesSize);
        struct md_thumbnails *thumbEntries1=(struct md_thumbnails *)((char*)thumbMem1+sizeof(struct md_thmbfile_header));
        struct md_thumbnails *thumbEntries2=(struct md_thumbnails *)((char*)thumbMem2+sizeof(struct md_thmbfile_header));

        //we will write the metadata part at the end, jump over it for the moment
        r=fseek(thumbnailF1,thmbEntriesSize,SEEK_SET);
        r=fseek(thumbnailF2,thmbEntriesSize,SEEK_SET);
        pos1=pos2=thmbEntriesSize;
        ph_assert(pos1=ftell(thumbnailF1),NULL);
        ph_assert(pos2=ftell(thumbnailF2),NULL);
        ph_assert(ptrParsing->picsDir[ptrParsing->picsDirLen-1]=='/',NULL);
        for(i=0;i<nrEntries;i++){
            //shortcuts
            struct mdh_album_entry *ce=&(entries[i]);
            thumbEntries1[i].size=thumbEntries2[i].size=ce->size&0xFFFFFFFF;//it is redundant to specify the size for thmbEntries2, because
                                //it is not used, but it is easier to keep things in symetry
            if(!(ce->flags&FlagIsAlbum) && !(ce->flags&FlagIsVideo)){
                //add the thumbnail at the end
                char *filename=getFilenameFromEntry(ce,level);
                memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,filename,strlen(filename)+1);
                r=createThumbnail(ptrParsing->picsDir,thumbnailF1,thumbnailF2,&ce->width,&ce->height);
                if(r){
                    //there was an error creating the thumbnail
                    /*
                    memmove(entries+i,entries+i+1,(nrEntries-i-1)*sizeof(struct mdh_album_entry));
                    i--;
                    nrEntries--;
                    //adjust albumData
                    albumData[0].nrEntries--;
                    albumData[0].nrTotalPictrs--;
                    if(!stat(ptrParsing->picsDir,&ptrParsing->statBuf))
                        albumData[0].sizeOfPictrsInAlbum-=ptrParsing->statBuf.st_size;//stat succeeded
                    */
                    LOG0("Thumbnail creation failed, entry was skipped.");
                    //tempScannedEntries++; //because it was counted in the first place
                    //continue;
                } else {
                    ce->flags|=FlagHasThumbnail;
                    thisAlbum->flags|=FlagHasThumbnail;
                }

                ce->flags&=~FlagTemp;//not necessary any more

                thumbEntries1[i].offset=pos1;
                thumbEntries1[i].width=ce->width;
                thumbEntries1[i].height=ce->height;
                pos1=ftell(thumbnailF1);
                thumbEntries2[i].offset=pos2;
                thumbEntries2[i].width=ce->width;
                thumbEntries2[i].height=ce->height;
                pos2=ftell(thumbnailF2);
            } else {
                //we do not have/need a thumb for this album or video entry
                thumbEntries1[i].offset=pos1;
                thumbEntries2[i].offset=pos2;
            }
            if(!(ce->flags&FlagIsAlbum))
                tempScannedEntries++;
            if(tempScannedEntries>ptrParsing->sendStatusStep){
                struct ps_md_pics *mdPics=getMdPics();
                mdPics->scannedEntries+=tempScannedEntries;
                releaseMdPics(&mdPics);
                tempScannedEntries=0;
                sendStatusToConnectedClients();
            }
        }

        //done with writing thumbnails. Now write the metadata.
        struct md_thmbfile_header *thumbHeader1=(struct md_thmbfile_header *)thumbMem1;
        struct md_thmbfile_header *thumbHeader2=(struct md_thmbfile_header *)thumbMem2;

        thumbHeader1->version=thumbHeader2->version=CURRENT_THUMBNAILS_FILE_VERSION;
        thumbHeader1->nrEntries=thumbHeader2->nrEntries=nrEntries;

        //First is F2 because we check F1 for rightness. If something fails here (power loss), F1 is incomplete.
        //for thumbnailF2:
        r=fseek(thumbnailF2,0,SEEK_SET);
        ph_assert(!r,NULL);//TODO
        r=fwrite(thumbMem2,thmbEntriesSize,1,thumbnailF2);
        free(thumbMem2);
        ph_assert(r==1,NULL);//TODO
        //TODO: handle errors above


        fclose(thumbnailF2);
        LOG0("Thumbnail2 file has %ld KBytes (%ld bytes)",pos2>>10,pos2);
        ptrParsing->nrThumbnailFiles++;
        ptrParsing->sizeOfThumbnailFiles+=pos2;

        //for thumbnailF1:
        r=fseek(thumbnailF1,0,SEEK_SET);
        ph_assert(!r,NULL);//TODO
        r=fwrite(thumbMem1,thmbEntriesSize,1,thumbnailF1);
        free(thumbMem1);
        ph_assert(r==1,NULL);//TODO
        //TODO: handle errors above


        fclose(thumbnailF1);
        LOG0("Thumbnail1 file has %ld KBytes (%ld bytes)",pos1>>10,pos1);
        ptrParsing->nrThumbnailFiles++;
        ptrParsing->sizeOfThumbnailFiles+=pos1;
    } else {
        ph_assert(!deleteOldThumbnailsFile,NULL); //we assert it is already deleted
        //transform FlagTemp into FlagHasThumbnail
        for(i=0;i<nrEntries;i++){
            //shortcuts
            struct mdh_album_entry *ce=&(entries[i]);
            if(ce->flags&FlagTemp){
                ce->flags|=FlagHasThumbnail;
                ce->flags&=~FlagTemp;//not necessary any more
                thisAlbum->flags|=FlagHasThumbnail;
            }
            if(!(ce->flags&FlagIsAlbum))
                tempScannedEntries++;
        }
    }
    if(tempScannedEntries>0){
        struct ps_md_pics *mdPics=getMdPics();
        mdPics->scannedEntries+=tempScannedEntries;
        releaseMdPics(&mdPics);
        tempScannedEntries=0;
        sendStatusToConnectedClients();
    }


    //create the memory structure for compact version of albums
    strAlbumPos=sizeof(struct md_album_entry)*nrEntries+16; //16 comes from the md5 sum, which will be stored right after the entries
    strTotalLen+=strAlbumPos+nrEntries; //strTotalLen becomes the total allocated size.+n comes from the permission byte.
    if(!level)
        strTotalLen+=nrEntries;
    album=(struct md_album_entry*)ph_malloc(strTotalLen);
    //LOG0("Allocated album (%p) size: %d bytes",album,strTotalLen);
    //allocated data
    ptrParsing->nrAllocatedChunks++;
    ptrParsing->sizeOfAllocatedChunks+=strTotalLen;
    ptrParsing->albumAllocatedSize=strTotalLen;

    //write data as album (md_album_entry) and to file
    memcpy(ptrParsing->dataDir+ptrParsing->dataDirLen,KAlbumFilename,strlen(KAlbumFilename)+1);
    f=fopen(ptrParsing->dataDir,"w");
    setOwnership(ptrParsing->dataDir,0);
    ph_assert(f,"ERROR: Opening file %s for writing failed with error: %s",ptrParsing->dataDir,strerror(errno));
    ptrParsing->dataDir[ptrParsing->dataDirLen]='\0'; //restore dataDir
    //TODO: handle f==NULL

    //varaibles for writing into the file
    int nrInts=5+nrPermissions+nrEntries;
    uint32_t *filePosLengths=(uint32_t*)ph_malloc(nrInts*sizeof(uint32_t));
#ifndef NDEBUG //this makes Valgrind happy
    memset(filePosLengths,0,nrInts*sizeof(uint32_t));
#endif
    //write the common JSON part (the path element) to tempBuf
    /* XXX
    sprintf(ptrParsing->tempBuf,"{\"p\":[");
    offset=0;
    for(i=0;i<level+1;i++){
        ptrParsing->tempBufPos=strlen(ptrParsing->tempBuf);
        if(i<level)sprintf(ptrParsing->tempBuf+ptrParsing->tempBufPos,"\"%s\",",ptrParsing->albumPath+offset);
        else sprintf(ptrParsing->tempBuf+ptrParsing->tempBufPos,"\"%s\"],\n",ptrParsing->albumPath+offset);
        offset+=strlen(ptrParsing->albumPath+offset)+1;
    }
    ph_assert(offset==albumPathPosSave,"%d %d",offset,albumPathPosSave);

    //write the common part of the album element
    ptrParsing->tempBufPos=strlen(ptrParsing->tempBuf);
    sprintf(ptrParsing->tempBuf+ptrParsing->tempBufPos,"\"a\":{");
    REALLOCATE_TEMP_IF_NEEDED;
    */

    //give some values to position and lengths variables
    filePosLengths[0]=nrPermissions; //nr of permissions
    filePosLengths[1]=nrInts<<2; //album data start position
    filePosLengths[2]=filePosLengths[1]+strTotalLen; //common part start position

    //fill in all the data related to entries->dataAlbumData
    for(i=0;i<nrEntries;i++){
        //shortcuts
        struct mdh_album_entry *ce=&(entries[i]);
        struct md_album_entry *ca=&(album[i]);
        char *cd=entries[i].data;
        uint8_t permissions=0; //default value if we do not have permissions
        if(ce->flags&FlagIsAlbum)dataAlbumData=ce->albumDataByPermission;
        else dataAlbumData=NULL;

        //building ca
        if(i>0 && entries[i-1].captureTime!=DATE_MAX)
            ph_assert(ce->captureTime>=entries[i-1].captureTime,NULL);

        if(ce->flags&FlagHasThumbnail)
            ph_assert(thisAlbum->flags&FlagHasThumbnail,NULL);

        //common part of ca
        ca->flags=ce->flags;
        ca->entriesIndex=ce->entriesIndex;
        ca->dataOffset=strAlbumPos;
        ca->size=ce->size;
        if(ce->flags&FlagHasDescriptionTxtEntry){
            permissions=*(uint8_t*)(cd+sizeof(void*));
            t=cd+sizeof(void*)+1;
        } else t=cd;
        if(ce->flags&FlagIsAlbum){
            ca->earliestCaptureTime=ce->captureTime;
            ca->latestCaptureTime=ce->latestCaptureTime;
            ca->album=ce->albumData;
        }
        else {
            ca->captureTime=ce->captureTime;

            if(ce->flags&FlagPhotoHasGPSInfo){
                ca->lat=ce->lat;
                ca->lng=ce->lng;
                ph_assert(ca->flags&FlagPhotoHasGPSInfo,NULL);
            } else ca->md5=0; //we will compute it later, if needed
        }
        if(ce->flags&FlagHasNameOffset)t++;
        //write/assign permission
        *((uint8_t*)album+strAlbumPos)=permissions;

        //current folder and filename
        if(!level){
            uint8_t currentFolder=*(uint8_t*)t;
            *((uint8_t*)album+strAlbumPos+1)=currentFolder;
            t++;
            l=strlen(t)+1;
            memcpy((char*)album+strAlbumPos+2,t,l);
            strAlbumPos+=l+2;
        } else {
            l=strlen(t)+1;
            memcpy((char*)album+strAlbumPos+1,t,l);
            strAlbumPos+=l+1;
        }
        //LOG0("%d/%d P: %d #%s#",i,n,permissions,t);

        for(j=1;j<nrPermissions+1;j++){ //albumData[0] is already done
            if(permissions&1){
                //this entry is considered in the current permision class
                if(ce->flags&FlagIsAlbum){
                    //this is an album
                    albumData[j].nrEntries+=dataAlbumData[j].nrEntries;
                    albumData[j].nrTotalPictrs+=dataAlbumData[j].nrTotalPictrs;
                    albumData[j].nrTotalVideos+=dataAlbumData[j].nrTotalVideos;
                    albumData[j].nrTotalAlbums+=dataAlbumData[j].nrTotalAlbums;
                    albumData[j].sizeOfPictrsInAlbum+=dataAlbumData[j].sizeOfPictrsInAlbum;
                    albumData[j].sizeOfVideosInAlbum+=dataAlbumData[j].sizeOfVideosInAlbum;


                    //check earliestCaptureDate and latestCaptureDate
                    if(dataAlbumData[j].earliestCaptureTime!=DATE_MAX &&
                            dataAlbumData[j].earliestCaptureTime<albumData[j].earliestCaptureTime)
                        albumData[j].earliestCaptureTime=dataAlbumData[j].earliestCaptureTime;
                    if(dataAlbumData[j].latestCaptureTime!=DATE_MAX){
                        if(albumData[j].latestCaptureTime==DATE_MAX)albumData[j].latestCaptureTime=dataAlbumData[j].latestCaptureTime;
                        else if(dataAlbumData[j].latestCaptureTime>albumData[j].latestCaptureTime)
                            albumData[j].latestCaptureTime=dataAlbumData[j].latestCaptureTime;
                    }
                } else {
                    //this is a picture or a video
                    albumData[j].nrEntries++;
                    if(ce->flags&FlagIsVideo){
                        albumData[j].nrTotalVideos++;
                        albumData[j].sizeOfVideosInAlbum+=ce->size;
                    } else {
                        albumData[j].nrTotalPictrs++;
                        albumData[j].sizeOfPictrsInAlbum+=ce->size;
                    }

                    if(ce->captureTime!=DATE_MAX){
                        if(albumData[j].earliestCaptureTime>ce->captureTime)albumData[j].earliestCaptureTime=ce->captureTime;
                        if(albumData[j].latestCaptureTime==DATE_MAX || albumData[j].latestCaptureTime<ce->captureTime)
                            albumData[j].latestCaptureTime=ce->captureTime;
                    }
                }
            }
            permissions>>=1;

        }//for(j...
        free(dataAlbumData);
    }//for(i...
    //at this point the album data is almost fully computed.The only thing to do is to compute its md5
    MD5_CTX ctx;
    MD5_Init(&ctx);
    for(i=0;i<nrEntries;i++){
        struct md_album_entry *ca=&(album[i]);
        MD5_Update(&ctx,ca,16); //we skip either the pointer to album or the lat+lng or md5
        //LOG0bin(ca,16,0);
    }
    MD5_Update(&ctx,(uint32_t*)(album+nrEntries)+4,strTotalLen-16-nrEntries*sizeof(struct md_album_entry));
    //LOG0bin((uint32_t*)(album+n)+4,strTotalLen-16-n*sizeof(struct md_album_entry),1);
    //LOG0("String part has %d bytes",strTotalLen-16-n*sizeof(struct md_album_entry));
    MD5_Final(&ctx);
    *(uint32_t*)(album+nrEntries)=ctx.a;
    *((uint32_t*)(album+nrEntries)+1)=ctx.b;
    *((uint32_t*)(album+nrEntries)+2)=ctx.c;
    *((uint32_t*)(album+nrEntries)+3)=ctx.d;
    //LOG0("MD5: ");
    //LOG0bin(album+nrEntries,16,0);

    //write the Administrator permission part for non-common album part
    sprintf(ptrParsing->tempBuf,"{\"a\":{");
    ptrParsing->tempBufPos=strlen(ptrParsing->tempBuf);
    filePosLengths[3]=filePosLengths[2]+ptrParsing->tempBufPos;

    sprintf(ptrParsing->tempBuf+ptrParsing->tempBufPos,"\"n\":%d,\"p\":%d,\"v\":%d,\"a\":%d,\"u\":%d,\"ec\":%d,\"lc\":%d,"
            "\"sp\":%"UINT64_FMT",\"sv\":%"UINT64_FMT",\"su\":%"UINT64_FMT"}",
            albumData[0].nrEntries,albumData[0].nrTotalPictrs,albumData[0].nrTotalVideos,albumData[0].nrTotalAlbums,nrTotalUnknowns,
            albumData[0].earliestCaptureTime,albumData[0].latestCaptureTime,
            albumData[0].sizeOfPictrsInAlbum,albumData[0].sizeOfVideosInAlbum,sizeOfUnknownsInAlbum);

    //write non-common album parts for non-administrator permissions
    for(j=1;j<nrPermissions+1;j++){
        ptrParsing->tempBufPos=strlen(ptrParsing->tempBuf);
        filePosLengths[3+j]=filePosLengths[2]+ptrParsing->tempBufPos;

        sprintf(ptrParsing->tempBuf+ptrParsing->tempBufPos,"\"n\":%d,\"p\":%d,\"v\":%d,\"a\":%d,\"ec\":%d,\"lc\":%d,"
                "\"sp\":%"UINT64_FMT",\"sv\":%"UINT64_FMT"}",
                albumData[j].nrEntries,albumData[0].nrTotalPictrs,albumData[0].nrTotalVideos,albumData[j].nrTotalAlbums,
                albumData[j].earliestCaptureTime,albumData[j].latestCaptureTime,
                albumData[0].sizeOfPictrsInAlbum,albumData[0].sizeOfVideosInAlbum);
        //missing: ",\n\"e\":[\n"
    };

    //compute the position of the first album element
    ptrParsing->tempBufPos=strlen(ptrParsing->tempBuf);
    filePosLengths[4+nrPermissions]=filePosLengths[2]+ptrParsing->tempBufPos;


    //dump the stuff so far into the file
    fwrite(filePosLengths,nrInts*sizeof(uint32_t),1,f); //positions
    fwrite(album,strTotalLen,1,f); //full album binary data
    fwrite(ptrParsing->tempBuf,ptrParsing->tempBufPos,1,f); //album data (JSON)



    //write album elements as JSON
    for(i=0;i<nrEntries;i++){
        //shortcuts
        struct mdh_album_entry *ce=&(entries[i]);
        struct md_album_entry *ca=&(album[i]);
        char *cd=entries[i].data;
        int currentOffset=0;
        t=getFilenameFromEntry(ce,level);

        //TODO: we are also sending info about unknown items, this is not so good (privacy)
        currentOffset=0;
        if(ce->flags&FlagIsAlbum){
            tEscaped=escapeString(t);
            fprintf(f,"{\"t\":\"%s\",\"fl\":%u,\"p\":%u,\"v\":%u,\"a\":%u,\"u\":%u,\"ec\":%d,\"lc\":%d,\"sp\":%llu,\"su\":%llu",
                    tEscaped,(unsigned int)ce->flags,ce->nrTotalPictures,ce->nrTotalVideos,ce->nrTotalSubalbums,ce->nrTotalUnknowns,
                    (int)ca->earliestCaptureTime,(int)ca->latestCaptureTime,
                    (unsigned long long int)ce->size,(unsigned long long int)ce->unsupportedSize);
            FREE_ESCAPED_STRING(tEscaped,t);
        } else {
            //stuff common for video and pictures
            tEscaped=escapeString(t);
            fprintf(f,"{\"c\":%d,\"fl\":%u,\"m\":%u,\"sz\":%llu,\"fn\":\"%s\",\"tz\":%d",
                    (int)ce->captureTime,(unsigned int)ce->flags,(unsigned int)ce->entriesIndex,(unsigned long long int)ce->size,tEscaped,
                    (int)ce->timezone);
            FREE_ESCAPED_STRING(tEscaped,t);

            if(ce->flags&FlagIsVideo){
                //video-specific
                //TODO
            } else {
                //picture-specific
                fprintf(f,",\"w\":%u,\"h\":%u,\"fln\":%f,\"fl35\":%u,\"et\":%f,\"ep\":%u,\"fnr\":%f,\"iso\":%u",
                        (unsigned int)ce->width,(unsigned int)ce->height,
                        ce->focalLength,(unsigned int)ce->focalLength35,
                        ce->exposureTime,(unsigned int)ce->exposureProgram,ce->fNumber,(unsigned int)ce->iso);
            }
        }
        if(ce->flags&FlagPhotoHasGPSInfo)
            fprintf(f,",\"lat\":%d,\"lng\":%d",(int)ca->lat,(int)ca->lng);

        if(ce->flags&FlagHasDescriptionTxtEntry)
            currentOffset+=sizeof(void*)+1;

        if(ce->flags&FlagHasNameOffset){
            fprintf(f,",\"no\":%u",(unsigned int)*(uint8_t*)(cd+currentOffset));
            currentOffset++;
        }

        //write comments & stuff to JSON
        char *filename=t;
        //common for both files and albums/folders
        if(ce->flags&FlagHasDescriptionTxtEntry){
            if(ce->flags&FlagHasDescription){
                t+=strlen(t)+1;
                tEscaped=escapeString(t);
                writeImageDescriptionAndTitle(f,ce,filename,tEscaped);
                FREE_ESCAPED_STRING(tEscaped,t);
            } else
                writeImageDescriptionAndTitle(f,ce,filename,NULL);
        } else
            if(ce->flags&FlagHasDescription){
                t+=strlen(t)+1;
                tEscaped=escapeString(t);
                fprintf(f,",\"d\":{\"xx\":\"%s\"}",tEscaped);
                FREE_ESCAPED_STRING(tEscaped,t);
            }


        if(ce->flags&FlagHasUserComment){
            t+=strlen(t)+1;
            tEscaped=escapeString(t);
            LOG0("Escaped String: #%s#",tEscaped);
            LOG0("Original: %s",t);
            fprintf(f,",\"uc\":\"%s\"",tEscaped);
            FREE_ESCAPED_STRING(tEscaped,t);
        }
        if(i==nrEntries-1)fprintf(f,"}\n");
        else fprintf(f,"},\n");

        //compute position
        filePosLengths[5+nrPermissions+i]=ftell(f);
    }//for(i...
    //missing:"]}"
    //write remaining positions
    fseek(f,(5+nrPermissions)<<2,SEEK_SET);
    fwrite(filePosLengths+5+nrPermissions,nrEntries<<2,1,f);
    //get the size of the file
    fseek(f,0,SEEK_END);
    ptrParsing->sizeOfCachedData+=ftell(f);
    fclose(f);//done writing
    //free associated data
    free(filePosLengths);
    filePosLengths=NULL;

    //set our parsing data
    free(ptrParsing->albumData);
    ptrParsing->albumData=albumData;
    //memcpy(ptrParsing->albumData,albumData,sizeof(struct mdh_album_data_by_permission)*(ptrParsing->mdAll->nrPermissions+1));
    ptrParsing->nrTotalUnknowns=nrTotalUnknowns;
    ptrParsing->sizeOfUnknownsInAlbum=sizeOfUnknownsInAlbum;

    //set our parent album
    ph_assert(thisAlbum->flags&FlagIsAlbum,NULL); //should be set already

    thisAlbum->entriesIndex=nrEntries;
    thisAlbum->earliestCaptureTime=albumData[0].earliestCaptureTime;
    thisAlbum->latestCaptureTime=albumData[0].latestCaptureTime;
    thisAlbum->album=album;
    //we could set here album=NULL;

    LOG0("freeing data for %s. Nr subalbums: %d",ptrParsing->picsDir,albumData[0].nrTotalAlbums);
    //free our data
    for(i=0;i<nrEntries;i++)
        free(entries[i].data);
    free(entries);

    if(albumDescriptor){
        freeAlbumDescriptionStruct(albumDescriptor);
        free(albumDescriptor);
    }
    if(albumDescriptors){
        ph_assert(nrFolders>0,NULL);
        for(i=0;i<nrFolders;i++){
            //LOG0("i=%d/%d p=%p",i,nrFolders,albumDescriptors[i]);
            freeAlbumDescriptionStruct(albumDescriptors[i]);
            free(albumDescriptors[i]);
        }
        free(albumDescriptors);
    }
    //LOG0("Done!");

    //one more thing to do: delete those folders in our dataDir that have their modification time before startTime (updated in the beginning of this function)
    if(nrFolders>0){
        ph_assert(folders,NULL);
        ph_assert(!level,NULL);

        for(i=0;i<nrFolders;i++){
            //prepare dataDir
            l=strlen(folders[i]);

            memcpy(ptrParsing->dataDir,folders[i],l);
            r=strlen(KPhotostovisData);
            memcpy(ptrParsing->dataDir+l,KPhotostovisData,r+1);
            ptrParsing->dataDirLen=l+r;
            //add / at the end
            ptrParsing->dataDir[ptrParsing->dataDirLen++]='/';
            ptrParsing->dataDir[ptrParsing->dataDirLen]='\0';

            //delete unused folders
            deleteUnusedFolders(startTime);
            //restore data (not needed)
            //ptrParsing->dataDirLen=0;
            //ptrParsing->dataDir[0]='\0';
        }
    } else {
        ph_assert(!folders,NULL);
        ph_assert(ptrParsing->dataDirLen>0,NULL);
        //delete unused folders
        deleteUnusedFolders(startTime);
    }
    return 0;
}






#if 0
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// id and id list functions:


void idListReplace(struct md_ids_list *list, struct md_entry_id * const id)
{
    ph_assert(list,NULL);
    ph_assert((list->nrIds && list->ids) || (!list->nrIds && !list->ids),NULL);
    if(list->nrIds!=1){
        free(list->ids);
        list->ids=(struct md_entry_id*)ph_malloc(sizeof(struct md_entry_id));
        list->nrIds=1;
    }
    memcpy(list->ids,id,sizeof(struct md_entry_id));
}

void idListAdd(struct md_ids_list *list, struct md_entry_id * const id)
{
    ph_assert(list,NULL);
    ph_assert((list->nrIds && list->ids) || (!list->nrIds && !list->ids),NULL);
    list->nrIds++;
    list->ids=(struct md_entry_id*)ph_realloc(list->ids,list->nrIds*sizeof(struct md_entry_id));
    memcpy(list->ids+list->nrIds-1,id,sizeof(struct md_entry_id));

}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//md5 functions:

uint64_t getMd5FromFilename(const char * const filename, int size)
{
    MD5_CTX md5ctx;
    uint64_t md5=0;
    int bytesRead=0,r,fd=open(filename,O_RDONLY);
    char buffer[MD5_READ_BUF_SIZE];
    if(fd<0){
#ifndef NDEBUG
        ph_assert(0,"Could not open file: %s",filename);
#endif
        return 0;
    }

    //compute md5
    MD5_Init(&md5ctx);
    while(1){
        r=read(fd,buffer,MD5_READ_BUF_SIZE);
        if(r<=0)break;

        bytesRead+=r;
        MD5_Update(&md5ctx,buffer,r);
    };

    ph_assert(size==bytesRead,"We read from the file (%s) %d bytes, but size should be %d.",filename,bytesRead,size);

    close(fd);
    MD5_Final(&md5ctx);
    md5=md5ctx.a+md5ctx.b;
    md5=md5<<32;
    md5+=md5ctx.c+md5ctx.d;
    return md5;
}



void fillInMd5(struct md_entry_id *id)
{
    char filename[PATH_MAX];
    struct md_album_entry *c=getEntryAndPath(id,0,KPicturesFolder,filename);
    ph_assert(!(c->flags&FlagIsAlbum),NULL);
    c->md5=getMd5FromFilename(filename,c->size);
    c->flags|=FlagFullFileMD5;
    releaseMdPics(NULL);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int findClosestAlbum(struct md_album_entry * const pic, const char * const picName, const char * const picFilename,
                     struct md_album_entry * const currentAlbum, const int n, struct md_entry_id * const currentAlbumId,
                     struct md_entry_id *closestAlbum, int32_t *closestPictureCaptureTimeDiff)
{
    int i,r;
    int32_t diff=0x7FFFFFFF;
    ph_assert(pic,NULL);
    ph_assert(!(pic->flags&FlagIsAlbum),NULL);
    ph_assert(pic->captureTime!=DATE_MAX,NULL);
    ph_assert(picName,NULL);
    ph_assert(picFilename,NULL);
    ph_assert(currentAlbum,NULL);
    ph_assert(currentAlbumId,NULL);
    ph_assert(n>=0,NULL);
    ph_assert(closestAlbum->flags&FlagIdClosestPicture,NULL); //it should not be anything else

    //check if closest picture is in this album
    for(i=0;i<n;i++){
        const struct md_album_entry * const currentEntry=&(currentAlbum[i]);
        if(currentEntry->flags&FlagIsAlbum && currentEntry->entriesIndex>0){ //TODO: properly handle empty albums!!!
            int closestPictureChanged=0;

            //check for closest picture
            if(currentEntry->earliestCaptureTime!=DATE_MAX){
                diff=pic->captureTime-currentEntry->earliestCaptureTime;
                if(ABS(diff)<=ABS(*closestPictureCaptureTimeDiff)){
                    *closestPictureCaptureTimeDiff=diff;
                    ph_assert(sizeof(currentAlbumId->flags)==1,NULL); //if this is not 1, then we need to adjust the +1 below. We do not want to memcopy flags
                    currentAlbumId->entry[currentAlbumId->level]=i;
                    currentAlbumId->level++;
                    memcpy((char*)(closestAlbum)+1,(char*)(currentAlbumId)+1,sizeof(struct md_entry_id)-1);
                    currentAlbumId->level--;
                    closestPictureChanged=1;
                    //LOG0("diff now: %d days",diff/86400);
                }
            }
            if(currentEntry->latestCaptureTime!=DATE_MAX){
                diff=pic->captureTime-currentEntry->latestCaptureTime;
                if(ABS(diff)<=ABS(*closestPictureCaptureTimeDiff)){
                    *closestPictureCaptureTimeDiff=diff;
                    ph_assert(sizeof(currentAlbumId->flags)==1,NULL); //if this is not 1, then we need to adjust the +1 below. We do not want to memcopy flags
                    currentAlbumId->entry[currentAlbumId->level]=i;
                    currentAlbumId->level++;
                    memcpy((char*)(closestAlbum)+1,(char*)(currentAlbumId)+1,sizeof(struct md_entry_id)-1);
                    currentAlbumId->level--;
                    closestPictureChanged=1;
                    //LOG0("diff now: %d days",diff/86400);
                }
            }

            if(closestPictureChanged || (currentEntry->earliestCaptureTime<=pic->captureTime && pic->captureTime<=currentEntry->latestCaptureTime)){
                currentAlbumId->entry[currentAlbumId->level]=i;
                currentAlbumId->level++;
                r=findClosestAlbum(pic,picName,picFilename,currentEntry->album,currentEntry->entriesIndex,currentAlbumId,closestAlbum,closestPictureCaptureTimeDiff);
                currentAlbumId->level--;
            }
        }//is album
    }//for
    return 0;
}

int albumsOrDuplicate(struct md_album_entry * const pic, const char * const picName, const char * const picFilename,
                      struct md_album_entry * const currentAlbum, const int n, struct md_entry_id * const currentAlbumId,
                      struct md_ids_list *suggestions)
{
    int i,r,nrSuggestions,hasSubalbums=0,hasPhotos=0;
    ph_assert(pic,NULL);
    ph_assert(picName,NULL);
    ph_assert(picFilename,NULL);
    ph_assert(currentAlbum,NULL);
    ph_assert(currentAlbumId,NULL);
    ph_assert(n>=0,NULL);
    ph_assert(!(pic->flags&FlagIsAlbum),NULL);
    ph_assert(suggestions,NULL);

    if(pic->captureTime==DATE_MAX){
        //picture without capture date
        if(!(currentAlbum->flags&FlagAlbumHasUndatedPhotos))
            return 0; //this album and its subalbums do not have undated photos, so nothing to check

        //check if this picture is duplicated in this album
        for(i=0;i<n;i++){
            struct md_album_entry * const currentEntry=&(currentAlbum[i]);
            if(!(currentEntry->flags&FlagIsAlbum) && currentEntry->captureTime==DATE_MAX){
                //currentEntry is an undated picture, just as our pic
                if(pic->size==currentEntry->size && !strcmp(picName,(char*)currentAlbum+currentEntry->dataOffset+1)){
                    //there is a good change this is our picture, lets check the md5
                    currentAlbumId->entry[currentAlbumId->level]=i;
                    currentAlbumId->level++;

                    if(!currentEntry->md5){
                        //we need to compute the md5 for this picture

                        ph_assert(ptrMerging,NULL);
                        fillInMd5(ptrMerging->mdAll,currentAlbumId);
                        ph_assert(currentEntry->md5,NULL);
                    }
                    if(!pic->md5)//we need to compute the md5 for our picture
                        pic->md5=getMd5FromFilename(picFilename,pic->size);

                    if(pic->md5==currentEntry->md5){
                        //yes, this is our picture
                        suggestions->flags=currentAlbumId->flags=FlagIdPhotoIsDuplicate;
                        idListReplace(suggestions,currentAlbumId);
                        return 0; //no need to check further
                    }
                    //if we are here, bring the id back
                    currentAlbumId->level--;
                }
            }
        }//for ....

        //check this picture with the subalbums
        for(i=0;i<n;i++){
            struct md_album_entry * const currentEntry=&(currentAlbum[i]);
            if(currentEntry->flags&FlagIsAlbum && currentEntry->flags&FlagAlbumHasUndatedPhotos){
                currentAlbumId->entry[currentAlbumId->level]=i;
                currentAlbumId->level++;
                r=albumsOrDuplicate(pic,picName,picFilename,currentEntry->album,currentEntry->entriesIndex,currentAlbumId,suggestions);
                currentAlbumId->level--;
                //check if the suggestions contain a duplicate
                if(suggestions->flags&FlagIdPhotoIsDuplicate){
                    ph_assert(suggestions->nrIds==1,NULL);
                    return 0;//because we found a duplicate inside a subalbum
                }
            } //entry is subalbum with undated photos
        }//for ...
    } else {
        //this picture has a date

        //check for duplicates among the photos in this album
        for(i=0;i<n;i++){
            const struct md_album_entry * const currentEntry=&(currentAlbum[i]);
            if(!(currentEntry->flags&FlagIsAlbum)){
                hasPhotos=1;
                if(pic->captureTime==currentEntry->captureTime){
                    //probably this is our picture
                    if(pic->size==currentEntry->size && !strcmp(picName,(char*)currentAlbum+currentEntry->dataOffset+1)){
                        //yes, this is our picture
                        currentAlbumId->entry[currentAlbumId->level]=i;
                        currentAlbumId->level++;
                        suggestions->flags=currentAlbumId->flags=FlagIdPhotoIsDuplicate;
                        idListReplace(suggestions,currentAlbumId);
                        return 0; //no need to check further
                    }
                }//same capture time
            }//entry is photo
        }//for ...

        //if we are here, the picture was not found among the pictures in this album. Try the subalbums
        nrSuggestions=suggestions->nrIds;
        for(i=0;i<n;i++){
            const struct md_album_entry * const currentEntry=&(currentAlbum[i]);
            if(currentEntry->flags&FlagIsAlbum){
                hasSubalbums=1;

                //check for picture inside album
                if(pic->captureTime>=currentEntry->earliestCaptureTime && pic->captureTime<=currentEntry->latestCaptureTime){
                    //the picture could belong to this album
                    currentAlbumId->entry[currentAlbumId->level]=i;
                    currentAlbumId->level++;
                    r=albumsOrDuplicate(pic,picName,picFilename,currentEntry->album,currentEntry->entriesIndex,currentAlbumId,suggestions);
                    currentAlbumId->level--;
                    //check if the suggestions contain a duplicate
                    if(suggestions->flags&FlagIdPhotoIsDuplicate){
                        ph_assert(suggestions->nrIds==1,NULL);
                        ph_assert(suggestions->ids[0].flags&FlagIdPhotoIsDuplicate,NULL);
                        return 0;//because we found a duplicate inside a subalbum
                    }
                }//photo's capture time inside album range
            }//entry is subalbum
        }//for ...

        //check if suggestions were added by subalbums.
        ph_assert(nrSuggestions<=suggestions->nrIds,NULL);
        if(suggestions->nrIds==nrSuggestions && currentAlbumId->level>0){ //if level==0 no point in adding this
            //if we are here, there were no suggestions added. Add this album
            currentAlbumId->flags=FlagIdPhotoIsInThisAlbum;
            if(!hasPhotos && hasSubalbums)currentAlbumId->flags|=FlagIdSuggestSubalbum;
            idListAdd(suggestions,currentAlbumId);
        }
    }//picture with date
    return 0;
}

int scandir_merge_filter(const struct dirent *d)
{
    int r,l;
    //we return 1 for jpegs and folders
#ifdef _DIRENT_HAVE_D_TYPE
    if(d->d_type==DT_DIR){
        if(d->d_name[0]=='.')return 0;//we filter out ., .. and all hidden folders
        else {
            //ptrParsing->nrTotalAlbums++;
            return 1;
        }
    }
    else if(d->d_type==DT_REG){
        //check extension
        char extension[4];
        l=strlen(d->d_name);
        strcpy(extension,d->d_name+l-4);
        if(!strcasecmp(extension,KJpegExtension))r=1;
        else r=0;
    }
    else r=0;//skipping this file
#else
#error ERROR: No implementation for missing d_type in dirent
#endif
    if(r)return r; //we stat vaid files later

    //get the size of this file and add it to album size
    /*
    ph_assert(strlen(ptrParsing->picsDir)==ptrParsing->picsDirLen,NULL);
    memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,d->d_name,l+1);
    if(!stat(ptrParsing->picsDir,&ptrParsing->statBuf)){
        //stat succeeded (for an unknown item)
        ptrParsing->nrTotalUnknowns++;
        ptrParsing->sizeOfUnknownsInAlbum+=ptrParsing->statBuf.st_size;
    } else {
        //stat failing is not a good sign
        ph_assert(0,NULL);//TODO
        r=0; //we would skip this anyway
    }
    ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';//we make it back
    */

    //LOG0("SKIPPING %s",d->d_name);
    return r;
}


int scanUploadsFolder()
{
    struct dirent **namelist;
    struct md_album_entry *uploaded=NULL,*thisAlbum=ptrMerging->album;
    struct md_entry_id entryId,closestAlbumId;
    struct md_ids_list suggestions;
    int i,j,l,n;
    int strTotalLen=0;
    int photosWithoutExifExist=0,photosWithoutDateExist=0;
    int32_t earliestCaptureTime=DATE_MAX;
    int32_t latestCaptureTime=DATE_MAX;
    int32_t closestPictureCaptureTimeDiff;
    int uploadsDirLenSave,duplicatesDirLenSave;
    struct md_album_entry *c=NULL;

    //make sure uploadsDir ends with /
    ph_assert(ptrMerging->uploadsDirLen>0,NULL);
    ph_assert(ptrMerging->uploadsDirLen==strlen(ptrMerging->uploadsDir),NULL);
    if(ptrMerging->uploadsDir[ptrMerging->uploadsDirLen-1]!='/'){
        ptrMerging->uploadsDir[ptrMerging->uploadsDirLen]='/';
        ptrMerging->uploadsDirLen++;
        ptrMerging->uploadsDir[ptrMerging->uploadsDirLen]='\0';
    }
    uploadsDirLenSave=ptrMerging->uploadsDirLen;

    //same for duplicatesDir
    ph_assert(ptrMerging->duplicatesDirLen>0,NULL);
    if(ptrMerging->duplicatesDir[ptrMerging->duplicatesDirLen-1]!='/'){
        ptrMerging->duplicatesDir[ptrMerging->duplicatesDirLen]='/';
        ptrMerging->duplicatesDirLen++;
        ptrMerging->duplicatesDir[ptrMerging->duplicatesDirLen]='\0';
    }
    duplicatesDirLenSave=ptrMerging->duplicatesDirLen;

    n=scandir(ptrMerging->uploadsDir, &namelist, &scandir_merge_filter, NULL);
    if(n<0){
        LOG0("Error scanning pictures in %s",ptrMerging->uploadsDir);
        perror("scandir");
        thisAlbum->flags|=FlagAlbumProcessingFailed;
        //TODO: check for memory-related
        return -1;
    }
    if(n>65535){
        LOG0("Too many pictures/entries in this album. Exiting.");
        thisAlbum->flags|=FlagAlbumProcessingFailed;
        return -1;
    }

    if(!n){
        //there is nothing (known) in this folder. Free data and return
        /*
        if(ptrMerging->sizeOfUnknownsInAlbum>0)
            LOG0("Folder (%s) contains no recognized media (but contains other stuff).",ptrMerging->uploadsDir);
        else
            LOG0("Empty folder: %s",ptrMerging->uploadsDir);
            */
        free(namelist);
        return 0;
    }

    LOG0("We have %d uploaded photos to merge.",n);

    //allocate struct md_album_entry
    uploaded=(struct md_album_entry*)ph_malloc(n*sizeof(struct md_album_entry));

    //processing the photos first
    for(i=0;i<n;i++){
        l=strlen(namelist[i]->d_name);
        strTotalLen+=l+1;
#ifdef _DIRENT_HAVE_D_TYPE
        if(namelist[i]->d_type==DT_DIR)continue;
        else if(namelist[i]->d_type==DT_REG) {
            int32_t captureTime=DATE_MAX;
            c=NULL;

            ph_assert(strlen(ptrMerging->uploadsDir)==ptrMerging->uploadsDirLen,NULL);
            memcpy(ptrMerging->uploadsDir+ptrMerging->uploadsDirLen,namelist[i]->d_name,l+1);
            ptrMerging->uploadsDirLen+=l;

            //get the size of this file
            if(!stat(ptrMerging->uploadsDir,&ptrMerging->statBuf)){
                //stat succeeded
                //nrTotalPics++;
                //sizeOfPicturesInAlbum+=ptrMerging->statBuf.st_size;

            } else {
                //stat failing is not a good sign
                ph_assert(0,NULL);
            }


            //exif part
            ExifData *ed=exif_data_new_from_file(ptrMerging->uploadsDir);
            if(ed){
                //get the camera date first, so we can sort it
                captureTime=getExifCaptureDate(ptrMerging->tempBuf,ed,namelist[i]->d_name);

                ph_assert(!c,NULL);
                //c=getCurrentEntry(entries,n,captureTime,&nrPicsWithDate,&nrPicsWithoutDate);
                c=&(uploaded[i]);
                c->captureTime=captureTime;
                if(ed->data && ed->size){
                    c->flags=FlagHasThumbnail;
                    thisAlbum->flags|=FlagHasThumbnail;
                } else {
                    c->flags=0;
                    LOG0("Found photo with EXIF but without thumbnail! (%s)",ptrParsing->picsDir);
                }

                //get the make and model
                ph_assert(ptrMerging->tempBufPos==0,NULL);
                c->entriesIndex=getExifCameraEntry(ptrMerging->tempBuf,ptrMerging->tempBufSize,ed,ptrMerging->mdAll);

                //check for GPS data
                getExifGPSCoordinates(ptrMerging->tempBuf,ptrMerging->tempBufSize,ed,&(c->lat),&(c->lng),namelist[i]->d_name);
                if(c->lat!=LAT_LNG_ERROR){
                    ph_assert(c->lng!=LAT_LNG_ERROR,NULL);
                    c->flags|=FlagPhotoHasGPSInfo;
                } else
                    c->md5=0; //there is no GPS info for this picture
                //GPS data done

                //free the exif data
                exif_data_unref(ed);
                ed=NULL;

            }
            else {
                LOG0("%s has no EXIF data", namelist[i]->d_name);
                photosWithoutExifExist=1;
                //TODO: try to inherit the date from filename

                //put this picture at the end
                //TODO: sort pictures by filename
                //nrPicsWithoutDate++;
                //c=&(entries[n-nrPicsWithoutDate]); //current entry
                c=&(uploaded[i]);
                //some vars
                captureTime=c->captureTime=DATE_MAX;
                c->entriesIndex=CAMERA_INDEX_NO_CAMERA_INFO;
                c->flags=0; //we do not have much data
                c->md5=0;
            }
            ph_assert(c,NULL);
            c->dataOffset=0; //invalid value
            c->size=ptrMerging->statBuf.st_size;

            //change earliestCaptureTime and/or latestCaptureTime
            if(captureTime!=DATE_MAX){
                if(earliestCaptureTime>captureTime)earliestCaptureTime=captureTime;
                if(latestCaptureTime==DATE_MAX || latestCaptureTime<captureTime)latestCaptureTime=captureTime;
            } else photosWithoutDateExist=1;

            //done for this picture
            ptrMerging->uploadsDirLen=uploadsDirLenSave;
            ptrMerging->uploadsDir[ptrMerging->uploadsDirLen]='\0';
        }
        else ph_assert(0,NULL); //there should be no other case here
#else
#error ERROR: No implementation for missing d_type in dirent
#endif
    }

    //check photos

    char tempBuf[PATH_MAX];
    const char *KMsgPicureHere="Picture here";
    const char *KMsgSubalbum="Picture in a separate subalbum of this album";
    const char *msg;

    for(i=0;i<n;i++){
        c=&(uploaded[i]);
        LOG0("Checking %s (%d) size: %d. ",namelist[i]->d_name,c->captureTime,c->size);
        l=strlen(namelist[i]->d_name);
        memcpy(ptrMerging->uploadsDir+ptrMerging->uploadsDirLen,namelist[i]->d_name,l+1);
        ptrMerging->uploadsDirLen+=l;
        entryId.level=0;
        entryId.flags=0;
        memset(&suggestions,0,sizeof(struct md_ids_list));

        albumsOrDuplicate(c,namelist[i]->d_name,ptrMerging->uploadsDir,ptrMerging->mdAll->folders,ptrMerging->mdAll->nrFolders,&entryId,&suggestions);

        closestPictureCaptureTimeDiff=0x7FFFFFFF;
        if(c->captureTime!=DATE_MAX && suggestions.nrIds==0){ //i.e. no suggested albums
            closestAlbumId.level=0;
            closestAlbumId.flags=FlagIdClosestPicture;
            findClosestAlbum(c,namelist[i]->d_name,ptrMerging->uploadsDir,ptrMerging->mdAll->folders,ptrMerging->mdAll->nrFolders,&entryId,&closestAlbumId,&closestPictureCaptureTimeDiff);
        }

        //display findings
        if(suggestions.flags&FlagIdPhotoIsDuplicate){
            ph_assert(suggestions.nrIds==1,NULL);
            getEntryAndPath(ptrMerging->mdAll,&(suggestions.ids[0]),0,KPicturesFolder,tempBuf);
            LOG0("This picture is a duplicate of another picture (%s).",tempBuf);
        } else {
            if(suggestions.nrIds>0){
                LOG0("Several suggested albums: %d",suggestions.nrIds);
                for(j=0;j<suggestions.nrIds;j++){
                    if(suggestions.ids[j].level>0)
                        getEntryAndPath(ptrMerging->mdAll,&(suggestions.ids[j]),0,KPicturesFolder,tempBuf);
                    else tempBuf[0]='\0';
                    ph_assert(!(suggestions.ids[j].flags&FlagIdClosestPicture),NULL);
                    if(suggestions.ids[j].flags&FlagIdPhotoIsInThisAlbum){
                        if(suggestions.ids[j].flags&FlagIdSuggestSubalbum)
                            msg=KMsgSubalbum;
                        else msg=KMsgPicureHere;
                    }
                    LOG0("     %s (%s)",tempBuf,msg);
                }//for j
            } else if(closestPictureCaptureTimeDiff!=0x7FFFFFFF){
                getEntryAndPath(ptrMerging->mdAll,&closestAlbumId,0,KPicturesFolder,tempBuf);
                LOG0("     Closest album: %s (difference: %d days)",tempBuf,closestPictureCaptureTimeDiff/86400);
            } else
                LOG0("Cannot propose anything about this picture.");
        }

        //done for this picture
        ptrMerging->uploadsDirLen=uploadsDirLenSave;
        ptrMerging->uploadsDir[ptrMerging->uploadsDirLen]='\0';
        //free suggestions
        if(suggestions.nrIds){
            ph_assert(suggestions.ids,NULL);
            free(suggestions.ids);
            //
            suggestions.ids=NULL;
            suggestions.nrIds=0;
        };
    }


    ph_assert(ptrMerging->uploadsDirLen==uploadsDirLenSave,NULL);
    if(ptrMerging->uploadsDir[ptrMerging->uploadsDirLen-1]='/'){
        ptrMerging->uploadsDirLen--;
        ptrMerging->uploadsDir[ptrMerging->uploadsDirLen]='\0';
    };

    return 0;
}





int look4uploads(struct ps_all_md *mdAll)
{
    struct timeval ts,tm,te;
    long int tdiff;
    int i,r,l;

    //create and allocate the parsing structure
    ptrMerging=(struct mdh_merging*)ph_malloc(sizeof(struct mdh_merging));
    ptrMerging->tempBufSize=2*TEMP_BUF_MIN_SIZE;
    ptrMerging->tempBuf=(char*)ph_malloc(ptrMerging->tempBufSize);
    ptrMerging->tempBufPos=0;
    ptrMerging->tempBufLen=0;
    ptrMerging->mdAll=mdAll;
    //allocated data
    ptrMerging->nrAllocatedChunks=0;
    ptrMerging->sizeOfAllocatedChunks=0;



    gettimeofday(&ts,NULL);

    for(i=0;i<mdAll->nrFolders;i++){
        char *cFoldername=(char*)mdAll->folders+mdAll->folders[i].dataOffset+1;
        gettimeofday(&tm,NULL);

        //give a value to UploadsDir and duplicatesDir
        ptrMerging->uploadsDirLen=ptrMerging->duplicatesDirLen=strlen(cFoldername);
        l=strlen(KUploadsFolder);
        ph_assert(ptrMerging->uploadsDirLen+l<PATH_MAX,NULL);
        memcpy(ptrMerging->uploadsDir,cFoldername,ptrMerging->uploadsDirLen);
        memcpy(ptrMerging->uploadsDir+ptrMerging->uploadsDirLen,KUploadsFolder,l+1);
        ptrMerging->uploadsDirLen+=l;
        l=strlen(KDuplicatesFolder);
        ph_assert(ptrMerging->duplicatesDirLen+l<PATH_MAX,NULL);
        memcpy(ptrMerging->duplicatesDir,cFoldername,ptrMerging->duplicatesDirLen);
        memcpy(ptrMerging->duplicatesDir+ptrMerging->duplicatesDirLen,KDuplicatesFolder,l+1);
        ptrMerging->duplicatesDirLen+=l;

        ptrMerging->album=&(mdAll->folders[i]);

        LOG0("Checking uploaded photos in %s",ptrMerging->uploadsDir);


        /////////////////////////////////////////
        r=scanUploadsFolder(); //TODO: do something with r



        gettimeofday(&te,NULL);
        tdiff=(te.tv_sec-tm.tv_sec)*1000+(te.tv_usec-tm.tv_usec)/1000;
        LOG0("Done looking for uploaded pictures in %s. Sorting took %ldms",ptrMerging->uploadsDir,tdiff);


    }
    if(mdAll->nrFolders>1){
        //total time
        tdiff=(te.tv_sec-ts.tv_sec)*1000+(te.tv_usec-ts.tv_usec)/1000;
        LOG0("Done looking for picture uploads overall. Sorting took %ldms.",tdiff);
    }

    //free data
    free(ptrMerging->tempBuf);
    free(ptrMerging);
    ptrMerging=NULL;

    return 0;
}
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if 0
char *getAlbumJsonWs(struct md_entry_id *id, size_t *bufLen, struct ps_all_md* mdAll)
{
    char path[PATH_MAX];
    int l;
    struct md_album_entry *e=getEntryAndPath(mdAll,id,path,KPhotostovisData); //e is part of mdAll, we do not free it
    if(!e){
        index2string(id,path,PATH_MAX);
        LOG0("We were unable to find the path for this index: %s",path);
        return NULL; //we did not find it
    }

    l=strlen(path);
    path[l]='/';
    memcpy(path+l+1,KJsonFilename,strlen(KJsonFilename)+1);
    //check if this file exists
    struct stat st;
    int offset,fd;
    if(stat(path,&st)){
        //stat did not succeeded, not a good sign
        LOG0("We were unable to stat the JSON file: %s",path);
        return NULL;
    }
    //open the file
    fd=open(path,O_RDONLY);
    if(fd==-1){
        LOG0("Unable to open %s, although stat succeeded. Strange ...",path);
        return NULL;
    }

    //if here, the file exist. Allocate a length for the websocket buffer
    if(st.st_size<126)offset=2;
    else if(st.st_size<65536)offset=4;
    else offset=10;
    *bufLen=st.st_size+offset;

    //allocate and fill in the buffer
    char *b=(char*)ph_malloc(*bufLen);
    b[0]=0x81;
    if(st.st_size<126)b[1]=st.st_size;
    else if(st.st_size<65536){
        b[1]=126;
        *(uint16_t*)&(b[2])=htons(st.st_size);
    } else {
        b[1]=127;
        ph_assert(st.st_size<0xFFFFFFFF,NULL); //we do not implement for bigger than this
        *(uint32_t*)&(b[2])=0;
        *(uint32_t*)&(b[6])=htonl(st.st_size);
    };

    //read the file into the buffer
    l=read(fd,b+offset,st.st_size);
    close(fd);
    if(l<0)perror("Error reading file");
    ph_assert(l==st.st_size,NULL);

    return b;
}

char *getThumbnailWs(struct md_entry_id *id, size_t *bufLen, struct ps_all_md* mdAll, uint16_t *rawid)
{
    ph_assert(id,NULL);
    ph_assert(mdAll,NULL);
    char path[PATH_MAX];
    //the following code is based on getEntryAndPath
    int pathLen=0;
    int i,l,currentN=mdAll->nrFolders;
    struct md_album_entry *currentAlbum=mdAll->folders,*currentEntry=NULL;
    char *cname;

    for(i=0;i<id->level;i++){
#ifdef NDEBUG //we should not trust data coming from the user
        if(!currentN)return NULL;
        if(id->entry[i]>=currentN)return NULL;
        if(!currentAlbum)return NULL;
#else
        ph_assert(id->entry[i]<currentN,NULL);
        ph_assert(currentAlbum,NULL);
#endif
        currentEntry=&(currentAlbum[id->entry[i]]);
        if(!(currentEntry->flags&FlagHasThumbnail)){
            LOG0("Current element has no thumbnail.");
            return NULL; //no point in continuing, because we know this subtree has no thumbnail
        }
        cname=(char*)currentAlbum+currentEntry->dataOffset+1;
        l=strlen(cname);
        memcpy(path+pathLen,cname,l);
        pathLen+=l;
        if(!i){
            //add KPicturesFolder
            l=strlen(KPicturesFolder);
            memcpy(path+pathLen,KPicturesFolder,l);
            pathLen+=l;//KPicturesFolder already ends with /
        }
        path[pathLen]='/';
        pathLen++;
        if(currentEntry->flags&FlagIsAlbum){
            currentAlbum=currentEntry->album;
            currentN=currentEntry->entriesIndex;
        } else {
            currentAlbum=NULL;
            currentN=0;
        }
    }

    if(currentAlbum && !(currentAlbum->flags&FlagHasThumbnail))
        return NULL;//no point in continuing, because we know this subtree has no thumbnail

    //in the end, we need to reach a picture with a thumbnail. We KNOW we have one, at least
    while(currentAlbum){
        //if here, we are looking for a picture in the current album
        for(i=0;i<currentN;i++){
            currentEntry=&(currentAlbum[i]);
            if(!(currentEntry->flags&FlagIsAlbum) && currentEntry->flags&FlagHasThumbnail){
                //this is a picture with a thumbnail, in the current album
                cname=(char*)currentAlbum+currentEntry->dataOffset+1;
                l=strlen(cname);
                memcpy(path+pathLen,cname,l);//we will terminate the string at the end
                pathLen+=l;
                //lets break from here
                currentAlbum=NULL;
                break;
            }
        }//for

        if(!currentAlbum)break; //we found our picture

        //if here, check among the subalbums
        for(i=0;i<currentN;i++){
            currentEntry=&(currentAlbum[i]);
            if(currentEntry->flags&FlagIsAlbum && currentEntry->flags&FlagHasThumbnail){
                //go inside this album
                cname=(char*)currentAlbum+currentEntry->dataOffset+1;
                l=strlen(cname);
                memcpy(path+pathLen,cname,l);
                pathLen+=l;
                path[pathLen]='/';
                pathLen++;
                //lets break from here
                currentAlbum=currentEntry->album;
                currentN=currentEntry->entriesIndex;
                break;
            }
        }//for
        ph_assert(i<currentN,NULL);//otherwise we were not able to find a thumbnail, altough the flags said there should be one
    }
    ph_assert(currentEntry,NULL);
    ph_assert(!(currentEntry->flags&FlagIsAlbum),NULL); //it should be a picture
    ph_assert(currentEntry->flags&FlagHasThumbnail,NULL); //...with a thumbnail
    if(path[pathLen-1]=='/')pathLen--;
    path[pathLen]='\0'; //terminate the path string

    //check if this file exists
    struct stat st;
    int offset,totalMsgLen;
    if(stat(path,&st)){
        //stat did not succeeded, not a good sign
        LOG0("We were unable to stat the picture file: %s",path);
        return NULL;
    }
    //open the file
    ExifData *ed=exif_data_new_from_file(path);
    if(!ed || !ed->data || !ed->size){
        ph_assert(0,"We were unable to find a thumbnail in this file (%s). Strange ...",path);
        return NULL;
    }
    //if here, the thumbnail exists. Allocate a length for the websocket buffer
    totalMsgLen=ed->size/*+(id->level<<1)*/;

    if(totalMsgLen<126)offset=2;
    else if(totalMsgLen<65536)offset=4;
    else offset=10;
    *bufLen=totalMsgLen+offset;

    //allocate and fill in the buffer
    char *b=(char*)ph_malloc(*bufLen);
    b[0]=0x82;//binary frame
    if(st.st_size<126)b[1]=totalMsgLen;
    else if(totalMsgLen<65536){
        b[1]=126;
        *(uint16_t*)&(b[2])=htons(totalMsgLen);
    } else {
        b[1]=127;
        ph_assert(totalMsgLen<0xFFFFFFFF,NULL); //we do not implement for bigger than this
        *(uint32_t*)&(b[2])=0;
        *(uint32_t*)&(b[6])=htonl(totalMsgLen);
    };
    /*
    //first, add the id of the requested thumbnail
    ph_assert(id->level>0,NULL);
    for(i=0;i<id->level;i++)
        *(uint16_t*)(b+offset+i)=rawid[i];
    offset+=id->level<<1;
    */

    //read the exif data into the buffer
    memcpy(b+offset,ed->data,ed->size);
    //free the exif data
    exif_data_unref(ed);

    return b;
}

int getFolderIndex(int rootIndex, struct ps_all_md* mdAll)
{
    if(rootIndex<0)return -1;

    int i;
    for(i=0;i<mdAll->nrFolders;i++)
        if(rootIndex<mdAll->folders[i].entriesIndex)return i;
        else rootIndex-=mdAll->folders[i].entriesIndex;
    //if here, we could not find it, so we need to return -1
    return -1;
}
#endif
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


