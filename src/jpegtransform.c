#define _GNU_SOURCE
#include <stdio.h>
#include <jpeglib.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <setjmp.h>
//#include <turbojpeg.h> //just so that we are sure we use turbojpeg and not the normal libjpeg

#include <libheif/heif.h>

#ifndef __APPLE__
#include <semaphore.h>
#endif

#include "photostovis.h"
#include "log.h"

static const uint8_t KThumbnailQuality=70; //range: 0..100
static const uint8_t KHeicToJpegQuality=80;


#if !defined(HAVE_JPEG_WRITE_ICC_PROFILE)

#define ICC_MARKER  (JPEG_APP0 + 2)     /* JPEG marker code for ICC */
#define ICC_OVERHEAD_LEN  14            /* size of non-profile data in APP2 */
#define MAX_BYTES_IN_MARKER  65533      /* maximum data len of a JPEG marker */
#define MAX_DATA_BYTES_IN_MARKER (MAX_BYTES_IN_MARKER - ICC_OVERHEAD_LEN)

/*
* This routine writes the given ICC profile data into a JPEG file.  It *must*
* be called AFTER calling jpeg_start_compress() and BEFORE the first call to
* jpeg_write_scanlines().  (This ordering ensures that the APP2 marker(s) will
* appear after the SOI and JFIF or Adobe markers, but before all else.)
*/

/* This function is copied almost as is from libjpeg-turbo */

void jpeg_write_icc_profile(j_compress_ptr cinfo, const JOCTET* icc_data_ptr,
                            unsigned int icc_data_len)
{
  unsigned int num_markers;     /* total number of markers we'll write */
  int cur_marker = 1;           /* per spec, counting starts at 1 */
  unsigned int length;          /* number of bytes to write in this marker */

  /* Calculate the number of markers we'll need, rounding up of course */
  num_markers = icc_data_len / MAX_DATA_BYTES_IN_MARKER;
  if (num_markers * MAX_DATA_BYTES_IN_MARKER != icc_data_len)
    num_markers++;

  while (icc_data_len > 0) {
    /* length of profile to put in this marker */
    length = icc_data_len;
    if (length > MAX_DATA_BYTES_IN_MARKER)
      length = MAX_DATA_BYTES_IN_MARKER;
    icc_data_len -= length;

    /* Write the JPEG marker header (APP2 code and marker length) */
    jpeg_write_m_header(cinfo, ICC_MARKER,
                        (unsigned int) (length + ICC_OVERHEAD_LEN));

    /* Write the marker identifying string "ICC_PROFILE" (null-terminated).  We
     * code it in this less-than-transparent way so that the code works even if
     * the local character set is not ASCII.
     */
    jpeg_write_m_byte(cinfo, 0x49);
    jpeg_write_m_byte(cinfo, 0x43);
    jpeg_write_m_byte(cinfo, 0x43);
    jpeg_write_m_byte(cinfo, 0x5F);
    jpeg_write_m_byte(cinfo, 0x50);
    jpeg_write_m_byte(cinfo, 0x52);
    jpeg_write_m_byte(cinfo, 0x4F);
    jpeg_write_m_byte(cinfo, 0x46);
    jpeg_write_m_byte(cinfo, 0x49);
    jpeg_write_m_byte(cinfo, 0x4C);
    jpeg_write_m_byte(cinfo, 0x45);
    jpeg_write_m_byte(cinfo, 0x0);

    /* Add the sequencing info */
    jpeg_write_m_byte(cinfo, cur_marker);
    jpeg_write_m_byte(cinfo, (int) num_markers);

    /* Add the profile data */
    while (length--) {
      jpeg_write_m_byte(cinfo, *icc_data_ptr);
      icc_data_ptr++;
    }
    cur_marker++;
  }
}

#endif  // !defined(HAVE_JPEG_WRITE_ICC_PROFILE)

struct my_error_mgr {
    struct jpeg_error_mgr pub;	/* "public" fields */
    //int err;
    jmp_buf setjmp_buffer;
};
void my_error_exit(j_common_ptr cinfo){
  struct my_error_mgr *myerr=(struct my_error_mgr*)cinfo->err;
  char buf[JMSG_LENGTH_MAX];

  (*cinfo->err->format_message)(cinfo,buf);
  LOG0("libjpeg ERROR: %s",buf);
  //myerr->err=-1;

  longjmp(myerr->setjmp_buffer,-1);
}


#define KNrParallelScanlines 32

struct encoding_data {
    struct jpeg_compress_struct *dstinfo;
    JSAMPARRAY tmpBuffer[KNrParallelScanlines];
    JDIMENSION num_scanlines[KNrParallelScanlines];

    ph_sem_t readyToEncode;
    ph_sem_t encodingDone;
};

void *doEncoding(void *data){
    struct encoding_data *d=(struct encoding_data *)data;
    int i;
    while(1){
        ph_sem_wait(&d->readyToEncode);
        if(!d->tmpBuffer[0])break;

        for(i=0;i<KNrParallelScanlines;i++)
            if(d->num_scanlines[i])
                jpeg_write_scanlines(d->dstinfo,d->tmpBuffer[i],d->num_scanlines[i]);
        ph_sem_post(&d->encodingDone);
    }
    return NULL;
}

char *scaleJpeg2Thr(char *srcBuf, size_t srcBufLen, size_t *bufLen, uint8_t scaleNum, uint32_t originalFileSize){
    struct jpeg_decompress_struct srcinfo;
    struct jpeg_compress_struct dstinfo;
    //struct jpeg_error_mgr jsrcerr, jdsterr;
    struct my_error_mgr jsrcerr, jdsterr;
    JDIMENSION num_scanlines[KNrParallelScanlines];
    JSAMPARRAY tmpBuffer1[KNrParallelScanlines],tmpBuffer2[KNrParallelScanlines];
    JDIMENSION tmpBufferHeight, rowWidth;
    jpeg_saved_marker_ptr marker;
    unsigned char *buf=NULL;
    int i;
    unsigned long bufLenUL=0;
    unsigned int scaleStart=LOG0("################# scaleJpeg2Thr++ %d/8",(int)scaleNum);

    *bufLen=0;

    if(setjmp(jsrcerr.setjmp_buffer)){
        if(!buf){
            LOG0("ERROR (not recoverable) decoding image file, early stages.");
            jpeg_destroy_decompress(&srcinfo);
            jpeg_destroy_compress(&dstinfo);
            return NULL;
        }

        LOG0("WARNING: faulty decoding process (image corrupted?). Returning the data we have so far.");
        //if here, the decoding process was faulty (the image was corrupted)
        //jpeg_finish_decompress(&srcinfo); -> do not uses, calls the error function again
        jpeg_destroy_decompress(&srcinfo);
        jpeg_finish_compress(&dstinfo);
        jpeg_destroy_compress(&dstinfo);

        *bufLen=bufLenUL;
        LOG0("Resizing done (WITH ERRORS), Size=%lu",bufLenUL);
        return (char*)buf;
    }

    // Initialize the JPEG decompression object with default error handling.
    srcinfo.err = jpeg_std_error(&jsrcerr.pub);
    jsrcerr.pub.error_exit=my_error_exit;
    jpeg_create_decompress(&srcinfo);
    // Initialize the JPEG compression object with default error handling.
    dstinfo.err = jpeg_std_error(&jdsterr.pub);
    jdsterr.pub.error_exit=my_error_exit;
    jpeg_create_compress(&dstinfo);

    //source
    jpeg_mem_src(&srcinfo,(unsigned char*)srcBuf,srcBufLen);

    //save EXIF markers
    jpeg_save_markers(&srcinfo,JPEG_APP0,0xFFFF);
    jpeg_save_markers(&srcinfo,JPEG_APP0+1,0xFFFF);

    //Read file header
    jpeg_read_header(&srcinfo, TRUE);

    if(scaleNum==4){
        srcinfo.scale_num=1;
        srcinfo.scale_denom=2; //4/8
    } else if(scaleNum==2 || scaleNum==6){
        srcinfo.scale_num=scaleNum>>1;
        srcinfo.scale_denom=4;
    } else { //1,3,5
        srcinfo.scale_num=scaleNum;
        srcinfo.scale_denom=8;
    }

    //Calculate output image dimensions so we can allocate space
    jpeg_calc_output_dimensions(&srcinfo);
    LOG0("Original image (%dx%d) will be scaled with %d/%dx%d/%d to %dx%d",
         srcinfo.image_width,srcinfo.image_height,srcinfo.scale_num,srcinfo.scale_denom,srcinfo.scale_num,srcinfo.scale_denom,srcinfo.output_width,srcinfo.output_height);
    // Determine width of rows in the destination memory (padded to 4-byte boundary).
    rowWidth=srcinfo.output_width*srcinfo.output_components;

    // Create decompressor output buffer.
    tmpBufferHeight=srcinfo.rec_outbuf_height;
    for(i=0;i<KNrParallelScanlines;i++)
        tmpBuffer1[i]=(*srcinfo.mem->alloc_sarray)((j_common_ptr) &srcinfo, JPOOL_IMAGE, rowWidth, tmpBufferHeight);
    for(i=0;i<KNrParallelScanlines;i++)
        tmpBuffer2[i]=(*srcinfo.mem->alloc_sarray)((j_common_ptr) &srcinfo, JPOOL_IMAGE, rowWidth, tmpBufferHeight);

    //output
    jpeg_mem_dest(&dstinfo,&buf,&bufLenUL);

    //color space
    if(srcinfo.jpeg_color_space==JCS_YCbCr && srcinfo.out_color_space==JCS_RGB)
        srcinfo.out_color_space=JCS_YCbCr;


    //set the compressor
    dstinfo.in_color_space=srcinfo.out_color_space;
    dstinfo.input_components=srcinfo.out_color_components;
    dstinfo.image_width=srcinfo.output_width;
    dstinfo.image_height=srcinfo.output_height;
    //dstinfo.data_precision=8; //not needed

    jpeg_set_defaults(&dstinfo);

    //LOG0("%dx%d, %d",dstinfo.image_width,dstinfo.image_height,dstinfo.input_components);
    jpeg_default_colorspace(&dstinfo);


    //start doing something
    jpeg_start_decompress(&srcinfo);
    jpeg_start_compress(&dstinfo,TRUE);

    //copy markers
    for(marker=srcinfo.marker_list;marker;marker=marker->next)
        jpeg_write_marker(&dstinfo,marker->marker,marker->data,marker->data_length);

    //create the encoding thread
    pthread_t encThreadId;
    struct encoding_data encData;
    ph_sem_init(&encData.readyToEncode,0,0);
    ph_sem_init(&encData.encodingDone,0,1);
    memcpy(encData.tmpBuffer,tmpBuffer2,KNrParallelScanlines*sizeof(JSAMPARRAY));
    encData.dstinfo=&dstinfo;
    int err=pthread_create(&encThreadId,NULL,doEncoding,&encData);
    ph_assert(!err,"Thread creation failed: %s",strerror(errno)); //TODO


    unsigned int headerTime=getRelativeTimeMs();
    unsigned int decodingTime=0;
    unsigned int encodingTime=0;
    unsigned int tNow,tNowD,tNowE=0;

    tNow=headerTime;

    //do the processing
    while(srcinfo.output_scanline < srcinfo.output_height){
        for(i=0;i<KNrParallelScanlines;i++){
            if(srcinfo.output_scanline < srcinfo.output_height)
                num_scanlines[i]=jpeg_read_scanlines(&srcinfo,tmpBuffer1[i],tmpBufferHeight);
            else num_scanlines[i]=0;
        }
        tNowD=getRelativeTimeMs();
        //encoding
        ph_sem_wait(&encData.encodingDone);
        memcpy(encData.num_scanlines,num_scanlines,KNrParallelScanlines*sizeof(JDIMENSION));
        memcpy(tmpBuffer2,encData.tmpBuffer,KNrParallelScanlines*sizeof(JSAMPARRAY));
        memcpy(encData.tmpBuffer,tmpBuffer1,KNrParallelScanlines*sizeof(JSAMPARRAY));
        memcpy(tmpBuffer1,tmpBuffer2,KNrParallelScanlines*sizeof(JSAMPARRAY));
        ph_sem_post(&encData.readyToEncode);

        //timings
        tNowE=getRelativeTimeMs(); //depends on how much we waited for the previous encoding to end. SHould be almost zero
        //if(tNowE-tNowD>2)LOG0("!!! We had to wait for encoding: %ums",tNowE-tNowD);
        decodingTime+=tNowD-tNow;
        encodingTime+=tNowE-tNowD;
        tNow=tNowE;
    }

    //wait for the last encoding to finish
    ph_sem_wait(&encData.encodingDone);
    //end the encoding thread
    encData.tmpBuffer[0]=NULL;
    ph_sem_post(&encData.readyToEncode);
    pthread_join(encThreadId,NULL);
    ph_sem_destroy(&encData.readyToEncode);
    ph_sem_destroy(&encData.encodingDone);



    //end stuff
    jpeg_finish_decompress(&srcinfo);
    jpeg_destroy_decompress(&srcinfo);
    jpeg_finish_compress(&dstinfo);
    jpeg_destroy_compress(&dstinfo);

    tNow=getRelativeTimeMs()-scaleStart;
    tNowD=(headerTime-scaleStart)+decodingTime+encodingTime+(tNow+scaleStart-tNowE);

    LOG0("################# scaleJpeg2Thr--: Scaling finished in %ums (%u). Decoding: %ums, Encoding: %ums, header+end: %u+%ums.",
         tNow,tNowD,decodingTime,encodingTime,headerTime-scaleStart,tNow+scaleStart-tNowE);
    double sizePerMPixel=(double)bufLenUL*1000/(double)(srcinfo.output_width*srcinfo.output_height);
    double originalSizePerMPixel=(double)originalFileSize*1000/(double)(srcinfo.image_width*srcinfo.image_height);
    LOG0("Size per Mpixel: %f. Original size per Mpixel: %f",sizePerMPixel,originalSizePerMPixel);
    LOG0("RATIO: <<<<%7.3f>>>>",originalSizePerMPixel/sizePerMPixel);

    *bufLen=bufLenUL;
    return (char*)buf;
}


void doJpegScaling(struct jpeg_decompress_struct *srcinfo, struct jpeg_compress_struct *dstinfo)
{
    JDIMENSION num_scanlines;
    JSAMPARRAY tmpBuffer;
    JDIMENSION tmpBufferHeight, rowWidth;
    jpeg_saved_marker_ptr marker;

    //Calculate output image dimensions so we can allocate space
    jpeg_calc_output_dimensions(srcinfo);
    LOG0("Source image (%dx%d) will be scaled with %d/%d to %dx%d",
         srcinfo->image_width,srcinfo->image_height,srcinfo->scale_num,srcinfo->scale_denom,srcinfo->output_width,srcinfo->output_height);
    // Determine width of rows in the destination memory (padded to 4-byte boundary).
    rowWidth=srcinfo->output_width*srcinfo->output_components;

    // Create decompressor output buffer.
    tmpBufferHeight=srcinfo->rec_outbuf_height;
    tmpBuffer=(*srcinfo->mem->alloc_sarray)((j_common_ptr) srcinfo, JPOOL_IMAGE, rowWidth, tmpBufferHeight);


    //color space
    if(srcinfo->jpeg_color_space==JCS_YCbCr && srcinfo->out_color_space==JCS_RGB)
        srcinfo->out_color_space=JCS_YCbCr;

    //set the compressor
    dstinfo->in_color_space=srcinfo->out_color_space;
    dstinfo->input_components=srcinfo->out_color_components;
    dstinfo->image_width=srcinfo->output_width;
    dstinfo->image_height=srcinfo->output_height;
    //dstinfo->data_precision=8; //not needed

    jpeg_set_defaults(dstinfo);
    jpeg_set_quality(dstinfo,KThumbnailQuality,0);

    //LOG0("%dx%d, %d",dstinfo.image_width,dstinfo.image_height,dstinfo.input_components);
    jpeg_default_colorspace(dstinfo);


    //start doing something
    jpeg_start_decompress(srcinfo);
    jpeg_start_compress(dstinfo,TRUE);

    //copy markers
    for(marker=srcinfo->marker_list;marker;marker=marker->next)
        jpeg_write_marker(dstinfo,marker->marker,marker->data,marker->data_length);
    //do the processing
    while(srcinfo->output_scanline < srcinfo->output_height){
        num_scanlines=jpeg_read_scanlines(srcinfo,tmpBuffer,tmpBufferHeight);
        jpeg_write_scanlines(dstinfo,tmpBuffer,num_scanlines);
    }
    //end stuff
    jpeg_finish_decompress(srcinfo);
    jpeg_destroy_decompress(srcinfo);
    jpeg_finish_compress(dstinfo);
    jpeg_destroy_compress(dstinfo);
}

int createJpegThumbnail(const char *path, FILE *thumbnailFile1, FILE *thumbnailFile2, unsigned int *resizeTimeMS, unsigned int *resizeTimeMSperMpixel){
    struct jpeg_decompress_struct srcinfo;
    struct jpeg_compress_struct dstinfo;
    struct my_error_mgr jsrcerr, jdsterr;
    //struct jpeg_error_mgr jsrcerr, jdsterr;
    FILE *f=NULL;

    int sn,snp2,dMin,first=1,fileNr=2; //first we do the first file, then we do the second
    unsigned char *buf=NULL;
    unsigned long bufLenUL=0;
    unsigned int resizeTime=LOG0("Creating thumbnails for image: %s",path);
    float megapixels=1;

    if(setjmp(jsrcerr.setjmp_buffer)){
        if(!buf){
            LOG0("ERROR (not recoverable) decoding image file, early stages.");
            if(f)fclose(f);
            jpeg_destroy_decompress(&srcinfo);
            jpeg_destroy_compress(&dstinfo);
            return -1;
        }

        if(!f){
            LOG0("ERROR (not recoverable) encoding the thumbnail file, rather strange.");
            jpeg_destroy_decompress(&srcinfo);
            jpeg_destroy_compress(&dstinfo);
            //LOG0c_do(buf,free(buf),"We had to free buf, rather strange."); -> crashing if we free buf
            return -1;
        }

        LOG0("WARNING: faulty decoding process (image corrupted?). Trying to continue and encode the decoded data.");
        //if here, the decoding process was faulty (the image was corrupted)
        //jpeg_finish_decompress(&srcinfo); -> do not uses, calls the error function again
        jpeg_destroy_decompress(&srcinfo);
        jpeg_finish_compress(&dstinfo);
        jpeg_destroy_compress(&dstinfo);

        //try to continue with encoding of what we already have
        fclose(f);
        f=NULL;
        first=0;
    }

    if(first){
        f=fopen(path,"r");
        LOG0return(!f,-1,"ERROR: Image %s could not be opened for creating thumbnail!",path);
    }

    do{
        sn=1;
        snp2=0;
        // Initialize the JPEG decompression object with default error handling.
        srcinfo.err = jpeg_std_error(&jsrcerr.pub);
        jsrcerr.pub.error_exit=my_error_exit;
        jpeg_create_decompress(&srcinfo);
        // Initialize the JPEG compression object with default error handling.
        dstinfo.err = jpeg_std_error(&jdsterr.pub);
        jdsterr.pub.error_exit=my_error_exit;
        jpeg_create_compress(&dstinfo);

        //set source
        if(buf){
            //LOG0("Source set: buffer.");
            jpeg_mem_src(&srcinfo,buf,bufLenUL);
            if(f){
                fclose(f);//we do not need it any more
                //LOG0("File was closed.");
                f=NULL;
            }
        } else {
            ph_assert(f,NULL);
            jpeg_stdio_src(&srcinfo,f);
            //LOG0("Source set: file.");
        }

        //save EXIF markers
        //jpeg_save_markers(&srcinfo,JPEG_APP0,0xFFFF);
        //jpeg_save_markers(&srcinfo,JPEG_APP0+1,0xFFFF);

        //Read file header
        jpeg_read_header(&srcinfo, TRUE);
        //LOG0("jpeg_read_header succeeded");

        if(!buf){
            //*width=srcinfo.image_width;
            //*height=srcinfo.image_height;
            megapixels=srcinfo.image_width*srcinfo.image_height/1000000.0f;
            LOG0("Image has %f megapixels",megapixels);
        }

        //adjust scaling parameters
        dMin=srcinfo.image_height<srcinfo.image_width?srcinfo.image_height:srcinfo.image_width;
        if(fileNr==2){
            while(dMin>>1>300){
                sn<<=1;
                snp2++;
                dMin>>=1;
            }
        } else {
            ph_assert(fileNr==1,NULL);
            while(dMin>>1>150){
                sn<<=1;
                snp2++;
                dMin>>=1;
            }
        }

        LOG0c(first,"Original image (%dx%d) will be scaled to %dx%d (%d) and to %dx%d (%d)",
              srcinfo.image_width,srcinfo.image_height,
              srcinfo.image_width>>snp2,srcinfo.image_height>>snp2,sn,
              srcinfo.image_width>>(snp2+1),srcinfo.image_height>>(snp2+1),sn<<1);
        first=0;

        if(snp2==0 && buf){
            //we can copy directly
            if(fileNr==2){
                LOG0("Copying from buffer to thumbnail file 2");
                fileNr=1;
                fwrite(buf,bufLenUL,1,thumbnailFile2);
            } else {
                ph_assert(fileNr==1,NULL);
                LOG0("Copying from buffer to thumbnail file 1");
                fileNr=0;
                fwrite(buf,bufLenUL,1,thumbnailFile1);
            }
            jpeg_destroy_decompress(&srcinfo);
            jpeg_destroy_compress(&dstinfo);
            continue;
        }

        if(snp2>=3){
            srcinfo.scale_num=1;
            srcinfo.scale_denom=8;
            snp2-=3;
            ph_assert(!buf,NULL);
            LOG0("Destination: buffer");
            jpeg_mem_dest(&dstinfo,&buf,&bufLenUL);
        } else {
            srcinfo.scale_num=1;
            srcinfo.scale_denom=sn;
            if(fileNr==2){
                fileNr=1;
                LOG0("Destination: thumbnail file 2. Scaling to 1/%d",sn);
                jpeg_stdio_dest(&dstinfo,thumbnailFile2);
            } else {
                ph_assert(fileNr==1,NULL);
                fileNr=0;
                LOG0("Destination: thumbnail file 1. Scaling to 1/%d",sn);
                jpeg_stdio_dest(&dstinfo,thumbnailFile1);
            }
        }

        //do the scaling
        doJpegScaling(&srcinfo,&dstinfo);
        //close f if open
        if(f)fseek(f,0,SEEK_SET);
    } while(fileNr);
    //done
    if(f)fclose(f);
    if(buf)free(buf);

    resizeTime=getRelativeTimeMs()-resizeTime;

    LOG0("Thumbnail creation done. Creating thumbnail took -------------->  %4ums",resizeTime);
    if(resizeTimeMS)
        *resizeTimeMS=resizeTime;
    if(resizeTimeMSperMpixel)
        *resizeTimeMSperMpixel=(unsigned int)(resizeTime/megapixels);

    return 0;
}

static int hasExifMetadata(const struct heif_image_handle* handle){
    heif_item_id metadata_id;
    int count=heif_image_handle_get_list_of_metadata_block_IDs(handle,"Exif",&metadata_id,1);
    return count>0;
}


int createJpegFromHeif(struct heif_context *ctx, struct heif_image_handle *handle, const char *dstPath, char const * const srcPath, size_t* const dstSize){
    /*
    FILE *f=fopen(path,"r");
    LOG0return(!f,-1,"ERROR: createJpegFromHeif: Image (%s) could not be opened!",path);
    uint8_t hdr[12];
    fread(hdr,12,1,f);
    enum heif_filetype_result filetypeCheck=heif_check_filetype(hdr,12);
    fclose(f);f=NULL;

    LOG0return(filetypeCheck==heif_filetype_no,-1,"ERROR: createHeifThumbnail: file is NOT a HEIF/AVIF file: %s",path);
    LOG0return(filetypeCheck==heif_filetype_yes_unsupported,-1,"ERROR: createHeifThumbnail: file is an UNSUPPORTED HEIF/AVIF file: %s",path);
    */

    //make sure the destination path does exist
    LOG0("     ######     createJpegFromHeif, dstPath: %s",dstPath);
    char *dstDir=strrchr(dstPath,'/');
    if(dstDir){
        *dstDir='\0';
        int r=dirService(dstPath,1);
        ph_assert(!r,NULL);
        *dstDir='/';
    }
    FILE *f=fopen(dstPath,"wb");
    LOG0return(!f,-1,"createJpegFromHeif: could not create output file: %s",dstPath);

    struct heif_error err;
    //int hasAlpha=heif_image_handle_has_alpha_channel(handle); -> not used
    struct heif_decoding_options* decodeOptions=heif_decoding_options_alloc();

    //encoder->UpdateDecodingOptions(handle, decode_options);
    if(hasExifMetadata(handle))
        decodeOptions->ignore_transformations=1;
    decodeOptions->convert_hdr_to_8bit=1;

    int bitDepth=heif_image_handle_get_luma_bits_per_pixel(handle);
    LOG0close_return(bitDepth<0,-1,heif_decoding_options_free(decodeOptions);heif_image_handle_release(handle);heif_context_free(ctx),
            "ERROR: createHeifThumbnail: undefined bit-depth for image %s",srcPath);

    struct heif_image* image;
    err=heif_decode_image(handle,&image,heif_colorspace_YCbCr,heif_chroma_420,decodeOptions);
    heif_decoding_options_free(decodeOptions);
    LOG0close_return(err.code || !image,-1,heif_image_handle_release(handle);heif_context_free(ctx),
             "ERROR: createHeifThumbnail: could not decode image %s",srcPath);

    //encode

    LOG0("ENCODING...");
    struct jpeg_compress_struct dstinfo;
    struct my_error_mgr jdsterr;
    //struct jpeg_compress_struct cinfo;
    //struct ErrorHandler jerr;

    // Initialize the JPEG compression object with default error handling.
    dstinfo.err = jpeg_std_error(&jdsterr.pub);
    jdsterr.pub.error_exit=my_error_exit;
    jpeg_create_compress(&dstinfo);
    jpeg_stdio_dest(&dstinfo,f);

    dstinfo.image_width=heif_image_get_width(image, heif_channel_Y);
    dstinfo.image_height=heif_image_get_height(image, heif_channel_Y);

    //this is purely informative, next 2 lines can be deleted
    float megapixels=dstinfo.image_width*dstinfo.image_height/1000000.0f;
    LOG0("Image has %.2f megapixels",megapixels);

    dstinfo.input_components=3;
    dstinfo.in_color_space=JCS_YCbCr;
    jpeg_set_defaults(&dstinfo);
    jpeg_set_quality(&dstinfo, KHeicToJpegQuality,TRUE);
    jpeg_start_compress(&dstinfo,TRUE);

    //we skip the exif data because we do not need it in the transcoded jpeg

    size_t profileSize=heif_image_handle_get_raw_color_profile_size(handle);
    if(profileSize>0){
        uint8_t* profileData=(uint8_t*)malloc(profileSize);
        heif_image_handle_get_raw_color_profile(handle,profileData);
        jpeg_write_icc_profile(&dstinfo, profileData, (unsigned int)profileSize);
        free(profileData);
    }
    LOG0return(heif_image_get_bits_per_pixel(image,heif_channel_Y)!=8,-1,"createJpegFromHeif: JPEG writer cannot handle image with >8 bpp.");


    int stride_y;
    const uint8_t* row_y=heif_image_get_plane_readonly(image, heif_channel_Y, &stride_y);
    int stride_u;
    const uint8_t* row_u=heif_image_get_plane_readonly(image, heif_channel_Cb, &stride_u);
    int stride_v;
    const uint8_t* row_v=heif_image_get_plane_readonly(image, heif_channel_Cr, &stride_v);

    JSAMPARRAY buffer = dstinfo.mem->alloc_sarray((j_common_ptr)&dstinfo, JPOOL_IMAGE, dstinfo.image_width * dstinfo.input_components, 1);
    JSAMPROW row[1]={buffer[0]};

    while (dstinfo.next_scanline < dstinfo.image_height) {
        size_t offset_y = dstinfo.next_scanline * stride_y;
        const uint8_t* start_y = &row_y[offset_y];
        size_t offset_u = (dstinfo.next_scanline / 2) * stride_u;
        const uint8_t* start_u = &row_u[offset_u];
        size_t offset_v = (dstinfo.next_scanline / 2) * stride_v;
        const uint8_t* start_v = &row_v[offset_v];

        JOCTET* bufp = buffer[0];
        for (JDIMENSION x = 0; x < dstinfo.image_width; ++x) {
            *bufp++ = start_y[x];
            *bufp++ = start_u[x / 2];
            *bufp++ = start_v[x / 2];
        }
        jpeg_write_scanlines(&dstinfo, row, 1);
    }
    jpeg_finish_compress(&dstinfo);
    *dstSize=ftell(f);
    fclose(f);
    jpeg_destroy_compress(&dstinfo);

    LOG0("The size of the newly created JPEG: %d",(int)(*dstSize));

    //free stuff
    heif_image_release(image);
    return 0;
}











int getJpegDimensions(const char *path, uint16_t *width, uint16_t *height){
    struct jpeg_decompress_struct srcinfo;
    struct my_error_mgr jsrcerr;
    FILE *f=NULL;

    //unsigned int resizeTime=LOG0("Get image dimensions: %s",path);

    f=fopen(path,"r");
    LOG0return(!f,-1,"ERROR: Image %s could not be opened for finding size!",path);

    if(setjmp(jsrcerr.setjmp_buffer)){
        LOG0("ERROR (not recoverable) decoding image file, early stages.");
        if(f)fclose(f);
        jpeg_destroy_decompress(&srcinfo);
        *width=*height=0;
        return -1;
    }

    // Initialize the JPEG decompression object with default error handling.
    srcinfo.err = jpeg_std_error(&jsrcerr.pub);
    jsrcerr.pub.error_exit=my_error_exit;
    jpeg_create_decompress(&srcinfo);

    ph_assert(f,NULL);
    jpeg_stdio_src(&srcinfo,f);

    //Read file header
    jpeg_read_header(&srcinfo, TRUE);

    *width=srcinfo.image_width;
    *height=srcinfo.image_height;


    jpeg_destroy_decompress(&srcinfo);

    if(f)fclose(f);

    //resizeTime=LOG0("Get image dimensions done.")-resizeTime;
    //LOG0("Getting image dimensions took %ums",resizeTime);
    return 0;
}

char *scaleJpeg1Thr(char *srcBuf, size_t srcBufLen, size_t *bufLen, uint8_t scaleNum, uint32_t originalFileSize)
{
    struct jpeg_decompress_struct srcinfo;
    struct jpeg_compress_struct dstinfo;
    //struct jpeg_error_mgr jsrcerr, jdsterr;
    struct my_error_mgr jsrcerr, jdsterr;
    JDIMENSION num_scanlines;
    JSAMPARRAY tmpBuffer;
    JDIMENSION tmpBufferHeight, rowWidth;
    jpeg_saved_marker_ptr marker;
    unsigned char *buf=NULL;
    //int dW,dH,sW,sH,sn=8;
    //float rW,rH;
    unsigned long bufLenUL=0;
    unsigned int scaleStart=LOG0("################# scaleJpeg1Thr++ %d/8",(int)scaleNum);

    *bufLen=0;

    if(setjmp(jsrcerr.setjmp_buffer)){
        if(!buf){
            LOG0("ERROR (not recoverable) decoding image file, early stages.");
            jpeg_destroy_decompress(&srcinfo);
            jpeg_destroy_compress(&dstinfo);
            return NULL;
        }

        LOG0("WARNING: faulty decoding process (image corrupted?). Returning the data we have so far.");
        //if here, the decoding process was faulty (the image was corrupted)
        //jpeg_finish_decompress(&srcinfo); -> do not uses, calls the error function again
        jpeg_destroy_decompress(&srcinfo);
        jpeg_finish_compress(&dstinfo);
        jpeg_destroy_compress(&dstinfo);

        *bufLen=bufLenUL;
        LOG0("Resizing done (WITH ERRORS), Size=%lu",bufLenUL);
        return (char*)buf;
    }

    // Initialize the JPEG decompression object with default error handling.
    srcinfo.err = jpeg_std_error(&jsrcerr.pub);
    jsrcerr.pub.error_exit=my_error_exit;
    jpeg_create_decompress(&srcinfo);
    // Initialize the JPEG compression object with default error handling.
    dstinfo.err = jpeg_std_error(&jdsterr.pub);
    jdsterr.pub.error_exit=my_error_exit;
    jpeg_create_compress(&dstinfo);

    //source
    jpeg_mem_src(&srcinfo,(unsigned char*)srcBuf,srcBufLen);

    //save EXIF markers
    jpeg_save_markers(&srcinfo,JPEG_APP0,0xFFFF);
    jpeg_save_markers(&srcinfo,JPEG_APP0+1,0xFFFF);

    //Read file header
    jpeg_read_header(&srcinfo, TRUE);

    if(scaleNum==4){
        srcinfo.scale_num=1;
        srcinfo.scale_denom=2; //4/8
    } else if(scaleNum==2 || scaleNum==6){
        srcinfo.scale_num=scaleNum>>1;
        srcinfo.scale_denom=4;
    } else { //1,3,5
        srcinfo.scale_num=scaleNum;
        srcinfo.scale_denom=8;
    }

    //Calculate output image dimensions so we can allocate space
    jpeg_calc_output_dimensions(&srcinfo);
    LOG0("Original image (%dx%d) will be scaled with %d/%dx%d/%d to %dx%d",
         srcinfo.image_width,srcinfo.image_height,srcinfo.scale_num,srcinfo.scale_denom,srcinfo.scale_num,srcinfo.scale_denom,srcinfo.output_width,srcinfo.output_height);
    // Determine width of rows in the destination memory (padded to 4-byte boundary).
    rowWidth=srcinfo.output_width*srcinfo.output_components;

    // Create decompressor output buffer.
    tmpBufferHeight=srcinfo.rec_outbuf_height;
    tmpBuffer=(*srcinfo.mem->alloc_sarray)((j_common_ptr) &srcinfo, JPOOL_IMAGE, rowWidth, tmpBufferHeight);

    //output
    jpeg_mem_dest(&dstinfo,&buf,&bufLenUL);

    //color space
    if(srcinfo.jpeg_color_space==JCS_YCbCr && srcinfo.out_color_space==JCS_RGB)
        srcinfo.out_color_space=JCS_YCbCr;

    //set the compressor
    dstinfo.in_color_space=srcinfo.out_color_space;
    dstinfo.input_components=srcinfo.out_color_components;
    dstinfo.image_width=srcinfo.output_width;
    dstinfo.image_height=srcinfo.output_height;
    //dstinfo.data_precision=8; //not needed

    jpeg_set_defaults(&dstinfo);

    //LOG0("%dx%d, %d",dstinfo.image_width,dstinfo.image_height,dstinfo.input_components);
    jpeg_default_colorspace(&dstinfo);


    //start doing something
    jpeg_start_decompress(&srcinfo);
    jpeg_start_compress(&dstinfo,TRUE);

    //copy markers
    for(marker=srcinfo.marker_list;marker;marker=marker->next)
        jpeg_write_marker(&dstinfo,marker->marker,marker->data,marker->data_length);




    unsigned int headerTime=getRelativeTimeMs();
    unsigned int decodingTime=0;
    unsigned int encodingTime=0;
    unsigned int tNow,tNowD,tNowE=0;

    tNow=headerTime;

    //do the processing
    while(srcinfo.output_scanline < srcinfo.output_height){
        num_scanlines=jpeg_read_scanlines(&srcinfo,tmpBuffer,tmpBufferHeight);
        tNowD=getRelativeTimeMs();
        jpeg_write_scanlines(&dstinfo,tmpBuffer,num_scanlines);
        tNowE=getRelativeTimeMs();

        decodingTime+=tNowD-tNow;
        encodingTime+=tNowE-tNowD;
        tNow=tNowE;
    }


    //end stuff
    jpeg_finish_decompress(&srcinfo);
    jpeg_destroy_decompress(&srcinfo);
    jpeg_finish_compress(&dstinfo);
    jpeg_destroy_compress(&dstinfo);

    tNow=getRelativeTimeMs()-scaleStart;
    tNowD=(headerTime-scaleStart)+decodingTime+encodingTime+(tNow+scaleStart-tNowE);

    LOG0("################# scaleJpeg1Thr--: Scaling finished in %ums (%u). Decoding: %ums, Encoding: %ums, header+end: %u+%ums.",tNow,tNowD,decodingTime,encodingTime,headerTime-scaleStart,tNow+scaleStart-tNowE);
    double sizePerMPixel=(double)bufLenUL*1000/(double)(srcinfo.output_width*srcinfo.output_height);
    double originalSizePerMPixel=(double)originalFileSize*1000/(double)(srcinfo.image_width*srcinfo.image_height);
    LOG0("Size per Mpixel: %f. Original size per Mpixel: %f",sizePerMPixel,originalSizePerMPixel);
    LOG0("RATIO: <<<<%7.3f>>>>",originalSizePerMPixel/sizePerMPixel);

    *bufLen=bufLenUL;
    return (char*)buf;
}


///////////////////////// Video thumbnail

static int _createVideoThumbnailWithCommand(const char *path, char const * const Kcommand,
                                            FILE *thumbnailFile1, FILE *thumbnailFile2, unsigned int *resizeTimeMS, unsigned int *resizeTimeMSperMpixel){


    /* -loglevel panic
    const struct { const char *name; int level; } log_levels[] = {
        { "quiet"  , AV_LOG_QUIET   },
        { "panic"  , AV_LOG_PANIC   },
        { "fatal"  , AV_LOG_FATAL   },
        { "error"  , AV_LOG_ERROR   },
        { "warning", AV_LOG_WARNING },
        { "info"   , AV_LOG_INFO    },
        { "verbose", AV_LOG_VERBOSE },
        { "debug"  , AV_LOG_DEBUG   },
    };
    */

    LOG0return(!KFFmpegBin,-1,"createVideoThumbnail: there is no ffmpeg ...");
    unsigned int resizeTime=LOG0("Creating thumbnails for video: %s",path);

    //escape the path
    char *escapedPath=escape_str(path);

    char *tempFilename=ph_malloc(100);
    snprintf(tempFilename,100,"/tmp/photostovisVideoThumbXXXXXX");
    int fd=mkstemp(tempFilename);
    close(fd);
    char *command=ph_malloc(strlen(escapedPath)+strlen(tempFilename)+200);
    int r=strlen(tempFilename);
    char *tempFilename1=ph_malloc(r+10);
    sprintf(tempFilename1,"%s1.jpg",tempFilename);
    char *tempFilename2=ph_malloc(r+10);
    sprintf(tempFilename2,"%s2.jpg",tempFilename);
    free(tempFilename);
    tempFilename=NULL;

    //generate the command for non-retina thumbnail
    sprintf(command,Kcommand,KFFmpegBin,escapedPath,320,180,tempFilename1);
    LOG0("Creating video thumnail with command: %s",command);
    r=system(command);
    LOG0close_return(r,-2,unlink(tempFilename1);free(tempFilename1);free(tempFilename2);free(command),
            "Getting non-retina thumbnail for video failed with error: %d",r);

    //generate the command for retina thumbnail
    sprintf(command,Kcommand,KFFmpegBin,escapedPath,640,360,tempFilename2);
    LOG0("Creating video thumnail with command: %s",command);
    r=system(command);
    LOG0close_return(r,-2,unlink(tempFilename1);unlink(tempFilename2);free(tempFilename1);free(tempFilename2);free(command),
            "Getting retina thumbnail for video failed with error: %d",r);
    free(command);
    command=NULL;
    if(escapedPath!=path)free(escapedPath);

    LOG0("Video thumbnails created!");
    //if here, we have the thumbnails. Copy them into thumbnailFile1 and thumbnailFile2
    char buf[BUFSIZ];
    size_t n;
    FILE *f=fopen(tempFilename1,"r");
    while((n=fread(buf,1,sizeof(buf),f)) > 0)
        ph_assert(fwrite(buf,1,n,thumbnailFile1)==n,"Error: Writing video thumbnail1 to file.");
    fclose(f);
    f=fopen(tempFilename2,"r");
    while((n=fread(buf,1,sizeof(buf),f)) > 0)
        ph_assert(fwrite(buf,1,n,thumbnailFile2)==n,"Error: Writing video thumbnail2 to file.");
    fclose(f);

    //free stuff
    unlink(tempFilename1);
    unlink(tempFilename2);
    free(tempFilename1);
    free(tempFilename2);

    resizeTime=getRelativeTimeMs()-resizeTime;
    LOG0("Video Thumbnail creation done. Creating thumbnail took -------------->  %4ums",resizeTime);
    if(resizeTimeMS)
        *resizeTimeMS=resizeTime;
    if(resizeTimeMSperMpixel)
        *resizeTimeMSperMpixel=0;
    return 0;
}

int createVideoThumbnail(const char *path, FILE *thumbnailFile1, FILE *thumbnailFile2, unsigned int *resizeTimeMS, unsigned int *resizeTimeMSperMpixel){
    //we suppose the video is 16:9
    //retina thumbnail is 640x360, non retina is 320x180
    char const * const KFFmpegCommand="%s  -loglevel error -i \"%s\" -vf \"thumbnail,scale=%d:%d\" -qscale:v 2 -frames:v 1 -y %s";

    int r=_createVideoThumbnailWithCommand(path,KFFmpegCommand,thumbnailFile1,thumbnailFile2,resizeTimeMS,resizeTimeMSperMpixel);
    if(r==-2){
        //try with the second command
        char const * const KFFmpegCommand2="%s  -loglevel error -i \"%s\" -vframes 1 -an -s %dx%d -ss 1 %s";
        r=_createVideoThumbnailWithCommand(path,KFFmpegCommand2,thumbnailFile1,thumbnailFile2,resizeTimeMS,resizeTimeMSperMpixel);
    }
    return r;
}
