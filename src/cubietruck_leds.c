#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include "photostovis.h"
#include "log.h"

#define STATUS_LED_DIR "/sys/class/leds/orange:ph07:led4/"
#define NETWORK_LED_DIR "/sys/class/leds/white:ph11:led3/"

enum LedStatusFlags {
    LedOn=1,
    LedFlash10Percent=2,
    LedFlash50Percent=4,
    LedFlash90Percent=8
};

static unsigned int statusLed=0;

int ledCommand(enum LedCommands lc, int force){
    if((myHardware&0xFF000000)!=0x91000000)return 0;//led commands only for cubietruck
    LOG0("LEDS Command received: %d. Force it: %d",lc,force);
    if(force)statusLed=0;
    //change the status led accordingly
    switch(lc){
    case LCOff:
        if(statusLed==0 && !force)return 0;//already off
        else statusLed=0;
        break;
    case LCOn:
        if(statusLed&LedOn && !force)return 0;//already there and nothing to do
        else statusLed|=LedOn;
        break;
    case LCFlash10PercentOn:
        if(statusLed&LedFlash10Percent && !force)return 0;//already there and nothing to do
        else statusLed|=LedFlash10Percent;
        break;
    case LCFlash10PercentOff:
        if(!(statusLed&LedFlash10Percent) && !force)return 0;//already there and nothing to do
        else statusLed&=~LedFlash10Percent;
        break;
    case LCFlash50PercentOn:
        if(statusLed&LedFlash50Percent && !force)return 0;//already there and nothing to do
        else statusLed|=LedFlash50Percent;
        break;
    case LCFlash50PercentOff:
        if(!(statusLed&LedFlash50Percent) && !force)return 0;//already there and nothing to do
        else statusLed&=~LedFlash50Percent;
        break;
    case LCFlash90PercentOn:
        if(statusLed&LedFlash90Percent && !force)return 0;//already there and nothing to do
        else statusLed|=LedFlash90Percent;
        break;
    case LCFlash90PercentOff:
        if(!(statusLed&LedFlash90Percent) && !force)return 0;//already there and nothing to do
        else statusLed&=~LedFlash90Percent;
        break;
    default:ph_assert(0,"NOT IMPLEMENTED");
    }
    LOG0("LEDS target status: %u",statusLed);

    //implement the status
    FILE *f;
    if(statusLed>1){
        //we have flashing
        int delayOn,delayOff;
        ph_assert(statusLed&LedOn,"How did we ended up in this situation?");
        if(statusLed&LedFlash10Percent){
            delayOn=100;
            delayOff=1900;
        } else if(statusLed&LedFlash50Percent){
            delayOn=500;
            delayOff=499;
        } else /*if(statusLed&LedFlash90Percent)*/{
            delayOn=1900;
            delayOff=100;
        }
        LOG0("LEDS Applying command: flash %d on %d off",delayOn,delayOff);

        f=fopen(STATUS_LED_DIR "trigger","w");
        if(!f)return -1;
        fprintf(f,"timer");
        fclose(f);

        f=fopen(STATUS_LED_DIR "delay_on","w");
        if(!f)return -1;
        fprintf(f,"%d",delayOn);
        fclose(f);

        f=fopen(STATUS_LED_DIR "delay_off","w");
        if(!f)return -1;
        fprintf(f,"%d",delayOff);
        fclose(f);
    } else {
        //first remove the trigger from the led
        f=fopen(STATUS_LED_DIR "trigger","w");
        if(!f)return -1;
        fprintf(f,"none");
        fclose(f);
        //now write the brightness
        f=fopen(STATUS_LED_DIR "brightness","w");
        if(!f)return -1;
        if(statusLed){
            fprintf(f,"1");
            LOG0("LEDS Applying command: ON");
        } else {
            fprintf(f,"0");
            LOG0("LEDS Applying command: OFF");
        }
        fclose(f);
    }
    return 0;
}

int ledClientActivity(enum LedCommands lc, int force){
    FILE *f;
    ph_assert(lc==LCOn || lc==LCOff,NULL);
    if((myHardware&0xFF000000)!=0x91000000)return 0;//led commands only for cubietruck

    /* no need to remove the trigger, we are not using it
    //first remove the trigger from the led
    f=fopen(NETWORK_LED_DIR "trigger","w");
    if(!f)return -1;
    fprintf(f,"none");
    fclose(f);
    */
    //now write the brightness
    f=fopen(NETWORK_LED_DIR "brightness","w");
    if(!f)return -1;
    if(lc==LCOn)
        fprintf(f,"1");
    else
        fprintf(f,"0");
    fclose(f);
    return 0;
}
