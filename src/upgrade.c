#ifdef CONFIG_NO_PHONET
#error "This file should not be compiled in this build type"
#endif

#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <time.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <openssl/rand.h>
#include "photostovis.h"
#include "log.h"
#ifndef __APPLE__
#include <sys/prctl.h>
#endif

//detect the architecture
#ifdef __i386
const char *KArchitecture="i386";
#endif
#ifdef __x86_64
const char *KArchitecture="amd64";
#endif
#ifdef __ARM_ARCH_6__
const char *KArchitecture="armv6l"; //i.e. RaspberryPi 1
#endif
#ifdef __ARM_ARCH_7A__
const char *KArchitecture="armv7l"; //i.e. Cubieboard, RPi 2
#endif
#ifdef __ARM_ARCH_8A__
const char *KArchitecture="armv8";  //i.e. RPi 3
#endif
#if defined(__arm64__) && defined(__APPLE__)
const char *KArchitecture="M1";
#endif 

const char *KPhotostovisUpgraderName="photostovis_upgr";

static const char *KPackagePathPart="/usr/share/photostovis/%s.part";
static const int KPartStrLen=5;
//static const char *KRemovePackagesCmd="rm /usr/share/photostovis/photostovis_*.deb*"; //last * should also delete .part files
static const char *KUpgradePkgPath="/usr/share/photostovis/";
//static const char *KUpgradePkgCmd="dpkg -i /usr/share/photostovis/";
static const char *KUpgradePkgCmd="/usr/share/photostovis/install.sh ";

void setUpgradeCheckAlarm(int *firstRun){
    if(*firstRun){
        //if this is the first run, check for updates really early
        *firstRun=0;
        alarm(10);
        return;
    }
#ifdef NDEBUG
    //we target the alarm somewhere between 3 and 4 AM
    int h,m;
    time_t tNow=time(NULL);
    struct tm tNowTm;
    localtime_r(&tNow,&tNowTm);
    if(tNowTm.tm_hour<3)
        h=4-tNowTm.tm_hour;
    else
        h=23-tNowTm.tm_hour+4;
    RAND_bytes((unsigned char *)&m,4);
    if(m<0)m=-m;
    m%=60;
    LOG0("Setting the upgrade check in %dh and %dmin.",h,m);

    alarm((h*60+m)*60);
#else
    //we check for upgrade every 10 minutes
    alarm(600);
#endif
}

int getLinuxDistributionName(char *namePlaceholder){
    const char *KStretch="stretch";
    const char *KWheezy="wheezy";
    const char *KJessie="jessie";
    const char *KTrusty="trusty";
    int found=0;

    *namePlaceholder='\0';
    FILE *f=fopen("/etc/os-release","rt");
    if(!f)return -1;
    char line[1024];

    while(fgets(line,1024,f)){
        if(strstr(line,KStretch)){
            strcpy(namePlaceholder,KStretch);
            found=1;
            break;
        }
        if(strstr(line,KWheezy)){
            strcpy(namePlaceholder,KWheezy);
            found=1;
            break;
        }
        if(strstr(line,KJessie)){
            strcpy(namePlaceholder,KJessie);
            found=1;
            break;
        }
        if(strcasestr(line,KTrusty)){
            strcpy(namePlaceholder,KTrusty);
            found=1;
            break;
        }
    }//while
    fclose(f);
    if(!found){
        strcpy(namePlaceholder,"unknown");
        return -1;
    }
    return 0;
}

int checkForUpgrade(){
    LOG0("Check for upgrade: start");
    //fork and wait for the process
    pid_t pid=fork();
    if(pid==0){
        //in child process
#ifndef __APPLE__
        prctl(PR_SET_NAME,KPhotostovisUpgraderName);
        //pthread_setname_np
#endif
        enum PhonetCfgStatus err;
        char url[128],*answer=NULL,*fn=NULL,*fnEnd;
        char filepath[128];
        unsigned int version=(KPhotostovisVersionMajor<<8)+KPhotostovisVersionMinor,l;
        //name
        char distributionCodename[10];
#ifdef __APPLE__
        strcpy(distributionCodename,"apple");
#else
        getLinuxDistributionName(distributionCodename);
#endif
        sprintf(url,"check4update.php?v=%u&a=%s&n=%s",version,KArchitecture,distributionCodename);
        err=phonetRequestString(NULL,url,&answer);
        if(err)exit(1); //CURL error
        if(!answer || !strlen(answer) || strlen(answer)>1000)exit(2); //not found

        //do we have a positive answer?
        const char *KTrueFNAnswer="{\"a\":true,\"fn\":\"";
        l=strlen(KTrueFNAnswer);
        if(strncmp(answer,KTrueFNAnswer,l)){
            free(answer);
            exit(2); //not a positive answer
        }
        //extract the filename from the answer
        fn=answer+l;
        fnEnd=strchr(fn,'"');
        if(!fnEnd){
            free(answer);
            exit(2); //malformed answer
        }
        *fnEnd='\0';

        //if here, we have a filename
        snprintf(url,128,"check4update.php?fn=%s&n=%s",fn,distributionCodename);
        snprintf(filepath,128,KPackagePathPart,fn);


        LOG0("Downloading upgrade package %s to: %s",fn,filepath);
        FILE *f=fopen(filepath,"w");
        if(!f)exit(3); //file opening error

        err=phonetRequestFile(NULL,url,f);
        fclose(f);
        if(err){
            unlink(filepath);
            exit(1); //CURL error
        }

        //rename the package. Reuse url for the new path
        strncpy(url,filepath,strlen(filepath)-KPartStrLen);
        url[strlen(filepath)-KPartStrLen]='\0';
        rename(filepath,url);

        //do we also have an "extra" package?
        const char *KExtraPackage="\"fnx\":\"";

        fn=strstr(fnEnd+1,KExtraPackage);
        if(!fn){
            //nothing extra ...
            free(answer);
            fnEnd=fn=answer=NULL;
            exit(0); //success: we have (just) an upgrade package that has been downloaded successfully
        }
        fn+=strlen(KExtraPackage);

        //if here, we also have an extra package
        fnEnd=strchr(fn,'"');
        if(!fnEnd){
            free(answer);
            exit(2); //malformed answer
        }
        *fnEnd='\0';


        //if here, we have a filename
        snprintf(url,128,"check4update.php?fn=%s",fn);
        snprintf(filepath,128,KPackagePathPart,fn);

        f=fopen(filepath,"w");
        if(!f)exit(3); //file opening error

        err=phonetRequestFile(NULL,url,f);
        fclose(f);
        if(err)exit(1); //CURL error

        //rename the package. Reuse url for the new path
        strncpy(url,filepath,strlen(filepath)-KPartStrLen);
        url[strlen(filepath)-KPartStrLen]='\0';
        rename(filepath,url);

        //done
        free(answer);
        fnEnd=fn=answer=NULL;
        exit(0); //success: we have an upgrade package that has been downloaded successfully
    } else {
        //in parent process
        LOG0return(pid<0,-10,"Check for upgrade: fork() ERROR");
        int err,status;
        while(1){
            err=waitpid(pid,&status,0);
            if(err>0)break;
            ph_assert(err==-1,NULL);
            if(errno==EINTR)continue;
            else break;
        }
        if(err>0 && WIFEXITED(status))
            err=WEXITSTATUS(status);
        else
            LOG0return(1,-11,"Check for upgrade: child process exited abnormally.");

        LOG0return(err==0,0,"Check for upgrade: success (upgrade package found and downloaded)");
        LOG0return(err==1,-1,"Check for upgrade: CURL ERROR.");
        LOG0return(err==2,-2,"Check for upgrade: no upgrade package found.");
        LOG0return(err==3,-3,"Check for upgrade: ERROR opening local file for package download.");
    }

    return -11; //should not reach here
}


static int ispkg_filter(const struct dirent *d){
    if(d->d_type!=DT_REG)return 0;

    int l=strlen(d->d_name);
    const int KPkgNameLenMin=23; //strlen("photostovis_0.0-0_x.deb")
    const int KPkgNameLenMax=32; //strlen("photostovis_99.99-99_archtct.deb")
    if(l<KPkgNameLenMin || l>KPkgNameLenMax)return 0;

    unsigned int v1,v2,v3;
    char ending[64];
    if(strlen(d->d_name)>=64)return 0; //this avoids a buffer overflow in the next sscanf

    l=sscanf(d->d_name,"photostovis_%u.%u-%u_%s",&v1,&v2,&v3,ending);

    if(l!=4 || strlen(ending)>16 || strcmp(ending+strlen(ending)-4,".deb"))return 0;
    return 1;
}

char *getUpgradePackageCommand(){
    //first, check for upgrade packages
    char *cmd=NULL;
    struct dirent **namelist;
    int i,r,l;
    int n=scandir(KUpgradePkgPath,&namelist,&ispkg_filter,NULL);
    if(n<=0)return NULL;

    unsigned int v1i=0,v2i=0,v3i=0;
    unsigned int v1=0,v2=0,v3=0;
    int index=-1;
    char ending[16];
    for(i=0;i<n;i++){
        //find the entry with the highest version
        v1=v2=v3=0;
        l=sscanf(namelist[i]->d_name,"photostovis_%u.%u-%u_%s",&v1,&v2,&v3,ending);
        if(l==4 && ((v1>v1i) || (v1==v1i && v2>v2i) || (v1==v1i && v2==v2i && v3>v3i)) &&
                ((v1>KPhotostovisVersionMajor) || (v1==KPhotostovisVersionMajor && v2>KPhotostovisVersionMinor))){
            //this could be our upgrade package
            index=i;
        }
    }

    if(index>=0){
        //there is an upgrade package
        l=strlen(KUpgradePkgCmd);
        r=strlen(namelist[index]->d_name);
        cmd=ph_malloc(l+r+1);
        memcpy(cmd,KUpgradePkgCmd,l);
        memcpy(cmd+l,namelist[index]->d_name,r+1);
    }

    //free namelist data
    for(i=0;i<n;i++)
        free(namelist[i]);
    free(namelist);

    return cmd;
}

static int isextrapkg_filter(const struct dirent *d){
    if(d->d_type!=DT_REG)return 0;

    int l=strlen(d->d_name);
    const int KPkgNameLenMin=20; //strlen("phextras_0.0-0_x.deb")
    const int KPkgNameLenMax=29; //strlen("phextras_99.99-99_archtct.deb")
    if(l<KPkgNameLenMin || l>KPkgNameLenMax)return 0;

    unsigned int v1,v2,v3;
    char ending[64];
    if(strlen(d->d_name)>=64)return 0; //this avoids a buffer overflow in the next sscanf

    l=sscanf(d->d_name,"phextras_%u.%u-%u_%s",&v1,&v2,&v3,ending);

    if(l!=4 || strlen(ending)>16 || strcmp(ending+strlen(ending)-4,".deb"))return 0;
    return 1;
}

char *getExtraUpgradePackageCommand(){
    //first, check for upgrade packages
    char *cmd=NULL;
    struct dirent **namelist;
    int i,r,l;
    int n=scandir(KUpgradePkgPath,&namelist,&isextrapkg_filter,NULL);
    if(n<=0)return NULL;

    unsigned int v1i=0,v2i=0,v3i=0;
    unsigned int v1=0,v2=0,v3=0;
    int index=-1;
    char ending[16];
    for(i=0;i<n;i++){
        //find the entry with the highest version
        v1=v2=v3=0;
        l=sscanf(namelist[i]->d_name,"phextras_%u.%u-%u_%s",&v1,&v2,&v3,ending);
        if(l==4 && ((v1>v1i) || (v1==v1i && v2>v2i) || (v1==v1i && v2==v2i && v3>v3i)) &&
                ((v1>KPhotostovisVersionMajor) || (v1==KPhotostovisVersionMajor && v2>KPhotostovisVersionMinor))){
            //this could be our upgrade package
            index=i;
        }
    }

    if(index>=0){
        //there is an upgrade package
        l=strlen(KUpgradePkgCmd);
        r=strlen(namelist[index]->d_name);
        cmd=ph_malloc(l+r+1);
        memcpy(cmd,KUpgradePkgCmd,l);
        memcpy(cmd+l,namelist[index]->d_name,r+1);
    }

    //free namelist data
    for(i=0;i<n;i++)
        free(namelist[i]);
    free(namelist);

    return cmd;
}

/*
const char * const getDeleteUpgradePackagesCommand(){
    return KRemovePackagesCmd;
}*/
