#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <stdint.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <limits.h>
#include "log.h"
#include "photostovis.h"


//TODO:
//for later:
// 4. Delete old logfiles
// 5. Scan old logfiles for errors and warnings, and if found, upload the logfile
// 7. detect patterns in the logfiles and remove them (??)
// 8. Tag log messages -> use tags for parsing log messages for errors and warnings
// 9. If sync is lost, resync

extern const char *KPhotostovisDaemonName; //defined in main.c
extern uint8_t daemonFlags; //defined in main.c
static const char *KLogDaemonLogfileNameLocal="photostovis-%F---%H-%M-%S.log";


////////////////////////////////////////////////////////////
struct flushLogBufferStruct {
    FILE *f;
    char *buffer;
    int bufLen;
    int offset;
};

static void *doFlushLogBuffer(void *data){
    struct flushLogBufferStruct *flbData=(struct flushLogBufferStruct *)data;
    //LOG0("doFlushLogBuffer++");
    fwrite(flbData->buffer+flbData->offset,flbData->bufLen-flbData->offset,1,flbData->f);
    fflush(flbData->f);
    //free stuff
    free(flbData->buffer);
    free(flbData);
    //LOG0("doFlushLogBuffer--");
    return NULL;
}

static int flushLogBuffer(char *logtext, int offset, int len, FILE *f){
    //we create a thread in which we flush the buffer.
    //We want a new thread because we might wait few seconds for the HDD to wake up from sleep
    //SO NO EXPLICIT LOGs in this function, because we block the server for ~2 seconds!
    int r;
    pthread_t t;
    pthread_attr_t attr;
    r=pthread_attr_init(&attr);
    LOG0return(r,-1,"flushLogBuffer FAILURE: pthread_attr_init failed.");
    r=pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
    LOG0return(r,-1,"flushLogBuffer FAILURE: pthread_attr_setdetachstate failed.");

    struct flushLogBufferStruct *flbData=(struct flushLogBufferStruct*)ph_malloc(sizeof(struct flushLogBufferStruct));
    flbData->f=f;
    flbData->buffer=logtext;
    flbData->bufLen=len;
    flbData->offset=offset;

    r=pthread_create(&t,&attr,&doFlushLogBuffer,flbData);//transfering ownership of flbData
    LOG0close_return(r,-1,free(logtext);free(flbData),"flushLogBuffer ERROR (%d) creating a thread for flushing log buffer.",r);
    return 0;
}

////////////////////////////////////////////////////////////
struct sendLogBufferStruct {
    char *buffer;
    int bufLen;
};

static void *doSendLogBuffer(void *data){
    struct sendLogBufferStruct *slbData=(struct sendLogBufferStruct *)data;
    phonetSendLog(NULL,slbData->buffer,slbData->bufLen);
    //free stuff
    free(slbData->buffer);
    free(slbData);
    return NULL;
}

static int sendLogBuffer(char const * const buf, int const len){
    //we create a thread in which we send the buffer.
    //We want a new thread because we might wait few seconds, up to 20 (the timeout)
    int r,i,j,nrEols=0;
    pthread_t t;
    pthread_attr_t attr;
    r=pthread_attr_init(&attr);
    LOG0return(r,-1,"pthread_attr_init failed.");
    r=pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
    LOG0return(r,-1,"pthread_attr_setdetachstate failed.");

    //count how many \n-s we have
    for(i=0;i<len;i++)
        if(buf[i]=='\n')nrEols++;

    struct sendLogBufferStruct *slbData=(struct sendLogBufferStruct*)ph_malloc(sizeof(struct sendLogBufferStruct));
    slbData->buffer=ph_malloc(len+nrEols+1);

    //copy the data/text into the new buffer and change \n to \r\n
    for(i=0,j=0;i<len;i++,j++)
        if(buf[i]=='\n'){
            slbData->buffer[j++]='\r';
            slbData->buffer[j]='\n';
        } else
            slbData->buffer[j]=buf[i];
    slbData->bufLen=len+nrEols;

    r=pthread_create(&t,&attr,&doSendLogBuffer,slbData);//transfering ownership of flbData
    LOG0close_return(r,-1,free(slbData->buffer);free(slbData),"ERROR (%d) creating a thread for flushing log buffer.",r);

    return 0;
}



void* logServerThread(void *data){
    int srvFd=((struct ph_log_data*)data)->srvSocket;

    //open logfile
    FILE *logF=NULL;
    struct timeval daemonLogStart;
    gettimeofday(&daemonLogStart,NULL);

    {
        char *rootPath=((struct ph_log_data*)data)->rootPath;
        time_t startTime=daemonLogStart.tv_sec;
        struct tm startTm;
        localtime_r(&startTime,&startTm);
        char tempLogFilename[PATH_MAX];

        //we have now our filename
        ph_assert(rootPath,NULL);
        int rootPathLen=strlen(rootPath);
        memcpy(tempLogFilename,rootPath,rootPathLen);
        //do we havd a '/' at the end of rootPath? If not, insert it
        if(tempLogFilename[rootPathLen-1]!='/')
            tempLogFilename[rootPathLen++]='/';
        //add "log/"
        memcpy(tempLogFilename+rootPathLen,"log/",5); //4+1 for the \0
        rootPathLen+=4;
        //does this exist? if not, create it
        dirService(tempLogFilename,1);
        //add "filename"
        strftime(tempLogFilename+rootPathLen,PATH_MAX-rootPathLen,KLogDaemonLogfileNameLocal,&startTm);
        logF=fopen(tempLogFilename,"w");
        free(rootPath);
        LOG0return(!logF,NULL,"%s: Could not open %s as logfile. ERROR: %s",KPhotostovisDaemonName,tempLogFilename,strerror(errno));
    }
    free(data);data=NULL; //not needed anymore

    ssize_t err;
    uint32_t l; //using 32bit for what it actually fits into 2 bytes, so that we can synchronize the messages if something is lost.
    int pos=MAX_LOG_SIZE_SEND2SERVER;
    char *logtext=ph_malloc(MAX_LOG_BUFFER_SIZE);
    memset(logtext,' ',MAX_LOG_SIZE_SEND2SERVER); //fill it with space, in case we need to send it by mail due to crash.

    //wait for log messages and write them into the logfile
    while(1){
        err=read(srvFd,&l,sizeof(uint32_t));
        LOG0c_continue(err<0 && errno==EINTR,"%s: log read (msg length) was interrupted by a signal",KPhotostovisDaemonName);
        LOG0c_break(err<0 && errno!=EINTR,"%s: log read (msg length) returned an error: %s. Exiting the log thread.",KPhotostovisDaemonName,strerror(errno));

        if(err==sizeof(uint32_t)){
            if(l<=MAX_LOG_MESSAGE_SIZE){
                while(l>0){
                    err=read(srvFd,logtext+pos,l);
                    LOG0c_continue(err<0 && errno==EINTR,"%s: read was interrupted by a signal",KPhotostovisDaemonName);
                    if(err<0)break;
                    l-=err;
                    pos+=err;
                }
                
                LOG0c_break(err<0 && errno!=EINTR,"%s: read returned an error: %s. Exiting the log thread.",KPhotostovisDaemonName,strerror(errno));

                ph_assert(l==0 && err>=0,NULL);

                if((pos+MAX_LOG_MESSAGE_SIZE > MAX_LOG_BUFFER_SIZE) || (daemonFlags&DaemonFlagFlushLog)){
                    ph_assert(pos>=MAX_LOG_SIZE_SEND2SERVER,NULL);
                    char *new_logtext=ph_malloc(MAX_LOG_BUFFER_SIZE);
                    memcpy(new_logtext,logtext+pos-MAX_LOG_SIZE_SEND2SERVER,MAX_LOG_SIZE_SEND2SERVER);
                    flushLogBuffer(logtext,MAX_LOG_SIZE_SEND2SERVER,pos,logF);
                    logtext=new_logtext;
                    pos=MAX_LOG_SIZE_SEND2SERVER;
                }
            } else {
                if(l==LOG_CMD_FLUSH){
                    ph_assert(pos>=MAX_LOG_SIZE_SEND2SERVER,NULL);
                    if(pos>MAX_LOG_SIZE_SEND2SERVER){
                        char *new_logtext=ph_malloc(MAX_LOG_BUFFER_SIZE);
                        memcpy(new_logtext,logtext+pos-MAX_LOG_SIZE_SEND2SERVER,MAX_LOG_SIZE_SEND2SERVER);
                        flushLogBuffer(logtext,MAX_LOG_SIZE_SEND2SERVER,pos,logF);
                        logtext=new_logtext;
                        pos=MAX_LOG_SIZE_SEND2SERVER;
                    }
                } else if(l==LOG_CMD_EXIT){
                    pos+=sprintf(logtext+pos,"LOG closing (EXIT cmd received)\n");
                    break; //from the wile loop
                } else if(l==LOG_CMD_SEND2SERVER || l==LOG_CMD_EXIT_AND_SEND){
                    if(l==LOG_CMD_EXIT_AND_SEND){
                        pos+=sprintf(logtext+pos,"LOG closing (EXIT_AND_SEND cmd received)\n");
                        LOG0("DBG: logging thread: EXIT_AND_SEND cmd received.");
                    }
                    //we send the last MAX_LOG_SIZE_SEND2SERVER of data to photostovis.net
#ifndef CONFIG_NO_PHONET
                    int posToSend=pos-MAX_LOG_SIZE_SEND2SERVER;
                    ph_assert(posToSend>=0,NULL);
                    LOG0("DBG: logging thread: lenToSend=%d (pos=%d, posToSend=%d)",MAX_LOG_SIZE_SEND2SERVER,pos,posToSend);
                    sendLogBuffer(logtext+posToSend,MAX_LOG_SIZE_SEND2SERVER);
#endif
                    if(l==LOG_CMD_EXIT_AND_SEND)
                        break; //from the wile loop
                } else {
                    //unhandled command
                    LOG0("Unhandled LOG command received from the client: %x (%c%c%c%c)",l,
                         (char)(l>>24),(char)((l>>16)&0xFF),(char)((l>>8)&0xFF),(char)(l&0xFF));

                    //ph_assert(0,"Unhandled LOG command: %d",l);
                }
            }
        }//if(err==sizeof(uint32_t))
    }//while
    //clean stuff

    if(pos>0)
        fwrite(logtext,pos,1,logF);

    fclose(logF);
    close(srvFd);
    free(logtext);
    LOG0("%s: logging thread exiting.",KPhotostovisDaemonName);
    return 0;
}

