#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <zlib.h>
#include <limits.h> //PATH_MAX

#include "photostovis.h"
#include "log.h"

//after initialization these are basically treated as constants
static struct ps_fm_file *files=NULL;
static char **fileUrls=NULL;
static int nrFiles=0;
//static pthread_mutex_t filesMutex=PTHREAD_MUTEX_INITIALIZER;-> not needed, only fileManagerInit() and fileManagerClose() are changing data


int loadFile(const char *url, const int urlLen, const char *reqETag, const int openGzipped, struct ps_fm_file * const fmFile){
    int r;
    ph_assert(fmFile,NULL);
    FILE *f=NULL;
    memset(fmFile,0,sizeof(struct ps_fm_file));

    {
        //get the filename
        char filename[PATH_MAX];
        struct stat st;
        int statted=0;
        //we need our own copy of serverDocRoot
        struct ps_md_config const * const mdConfig=getMdConfigRd();
        int filenameLen=strlen(mdConfig->srvDocRoot);
        memcpy(filename,mdConfig->srvDocRoot,filenameLen+1);
        releaseMdConfig(&mdConfig);
        if(filename[filenameLen-1]!='/'){
            filename[filenameLen++]='/';
            filename[filenameLen]='\0';
        }
        //add the url
        const char *KIndexHtml="index.html";
        const char *KIndexAllHtml="index.all.html";
        if(!strcmp(url,KIndexHtml)){
            //do we have a "index.all.html" file? If yes, use that
            memcpy(filename+filenameLen,KIndexAllHtml,strlen(KIndexAllHtml)+1);
            r=stat(filename,&st);
            if(r){//apparently not supported
                LOG0("WARNING: %s not found.",filename);
                memcpy(filename+filenameLen,KIndexHtml,strlen(KIndexHtml)+1);
            } else statted=1;
        } else
            memcpy(filename+filenameLen-(url[0]=='/'?1:0),url,urlLen+1);

        //add .gz if the browser accepts gzipped content
        if(openGzipped){
            filenameLen=strlen(filename);
            memcpy(filename+filenameLen,".gz",4);
            r=stat(filename,&st);
            if(r){//apparently we do not have a .gz version
                statted=0;
                filename[filenameLen]='\0'; //reset the filename
                LOG0("INFO: Gzipped version of %s not found!",filename);
            } else {
                //we have the gzipped version
                statted=1;
                fmFile->flags|=FlagIsGZipped;
            }
        }

        if(!statted){
            r=stat(filename,&st);
            LOG0return(r,-1,"WARNING: getFile: requested file (%s) for the given url (%s) not found.",filename,url);
        }
        r=snprintf(fmFile->eTag,17,"%08X%08X",(uint32_t)st.st_size,(uint32_t)st.st_mtime);
        ph_assert(r<17,NULL);
        if(reqETag && !strcmp(fmFile->eTag,reqETag)){
            //we have the same tag, we can answer differently
            fmFile->buf=(char*)reqETag;
            return 0;
        }
        fmFile->bufLen=st.st_size;
        f=fopen(filename,"r");
        LOG0("Opened %s (size: %d)",filename,fmFile->bufLen);
    }
    ph_assert(f,NULL);
    //if here, we have the right file opened, we just need to allocate the buffer and read it!
    fmFile->buf=ph_malloc(fmFile->bufLen);
    LOG0("Data allocated");
    r=fread(fmFile->buf,fmFile->bufLen,1,f);
    fclose(f);
    LOG0("Data read (r=%d)",r);
    LOG0close_return(r!=1,-1,free(fmFile->buf);fmFile->buf=NULL;fmFile->bufLen=0,"ERROR: reading from file failed.");

    return 0;
}

void fileManagerInit(){
    nrFiles=1; //we preload 1 file
    fileUrls=ph_malloc(nrFiles*sizeof(char*));
    files=ph_malloc(nrFiles*sizeof(struct ps_fm_file));

    //index.html
    fileUrls[0]=ph_strdup("index.html");
    loadFile(fileUrls[0],strlen(fileUrls[0]),NULL,1,&files[0]);
}

void fileManagerClose(){
    int i;
    for(i=0;i<nrFiles;i++){
        free(fileUrls[i]);
        free(files[i].buf);
    }
    free(fileUrls);fileUrls=NULL;
    free(files);files=NULL;
    nrFiles=0;
}

int getFile(const char *url, const int urlLen, const char *reqETag, const int acceptsGzip, struct ps_fm_file * const fmFile){
    //check first if we have this file cached in memory
    int i;
    const char *searchedUrl=url;
    if((urlLen==1 && url[0]=='/') || !strcmp(url,"/index.html"))
        searchedUrl="index.html";
    for(i=0;i<nrFiles;i++){
        if(!strcmp(searchedUrl,fileUrls[i]) && (acceptsGzip || !(files[i].flags&FlagIsGZipped)) ){
            //we found it!
            if(reqETag && !strcmp(files[i].eTag,reqETag)){
                fmFile->buf=(char*)reqETag;
                return 0;
            }
            memcpy(fmFile,&files[i],sizeof(struct ps_fm_file));
            return 0;
        }
    }

    //if here, it was not cached
    LOG0("Requested file URL (%s) is NOT cached, loading it from disk.",searchedUrl);
    int r=loadFile(searchedUrl,strlen(searchedUrl),reqETag,acceptsGzip,fmFile);
    fmFile->flags|=FlagFreeData;
    LOG0("Returning %d from getFile",r);
    return r;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// We only do this (concatenating) for system daemons

#define LINE_SIZE 1024
#define FILENAME_LEN 256

static const char *KFilenameSrc="/usr/share/photostovis/web/index.html";
static const char *KFilenameAllUncompressed="/usr/share/photostovis/web/index.all.html";
static const char *KFilenameAllCompressed="/usr/share/photostovis/web/index.all.html.gz";

static const char *KCssLine="<link rel=\"stylesheet\" media=\"screen\" href=\"";
static const char *KJsLine ="<script type=\"text/javascript\" src=\"";
static const char *KJsRemoteLine ="<script type=\"text/javascript\" src=\"http";

static void addFileToIndexHtml(FILE* fOut, const char *filenameLine){
    char filename[FILENAME_LEN];
    const char *KPath="/usr/share/photostovis/web/";
    const int KPathLen=strlen(KPath);
    int lineLen=strlen(filenameLine);
    if(lineLen>=FILENAME_LEN-KPathLen)
        lineLen=FILENAME_LEN-KPathLen-1;


    memcpy(filename,KPath,KPathLen);
    memcpy(filename+KPathLen,filenameLine,lineLen+1);
    filename[FILENAME_LEN-1]='\0'; //so we have an end of line
    char *end=strrchr(filename,'"');
    ph_assert(end,NULL);
    *end='\0';
    LOG0("Including file: %s",filename);

    FILE *fIn=fopen(filename,"r");
    ph_assert(fIn,NULL);
    fseek(fIn,0,SEEK_END);
    long size=ftell(fIn);
    fseek(fIn,0,SEEK_SET);

    char *buf=(char*)ph_malloc(size);
    int r=fread(buf,size,1,fIn);
    ph_assert(r==1,NULL);
    r=fwrite(buf,size,1,fOut);
    ph_assert(r==1,NULL);
    fclose(fIn);
    free(buf);
}

int CreateIndexAllHtmlGz(void){
    //first we need to check if we already have this file created
    //TODO



    LOG0("Concatenating and compressing index.html...");
    FILE *fIn=fopen(KFilenameSrc,"r");
    LOG0return(!fIn,-1,"WARNING: CreateIndexAllHtmlGz: Could not open %s, returning.",KFilenameSrc);
    FILE *fOut=fopen(KFilenameAllUncompressed,"w");
    LOG0close_return(!fOut,-1,fclose(fIn),"WARNING: CreateIndexAllHtmlGz: Could not open %s, returning.",KFilenameAllUncompressed);
    char line[LINE_SIZE],*ret;
    int inCss=0,inJs=0;

    //read the input file and insert stuff
    while(1){
        ret=fgets(line,sizeof(line),fIn);
        if(!ret)break;

        //handle CSS insertions
        if(!strncmp(line,"</style>",8)){
            inCss=1;
            continue;
        }
        if(inCss && !strncmp(line,KCssLine,strlen(KCssLine))){
            //we will replace this line with a file
            addFileToIndexHtml(fOut,line+strlen(KCssLine));
            continue;
        }
        if(inCss){
            //all css includes are included, end the </style>
            inCss=0;
            fprintf(fOut,"</style>\n");
            fputs(line,fOut);
            continue;
        }

        //handle JS insertions
        if(!strncmp(line,"</script>",9)){
            inJs=1;
            continue;
        }
        if(inJs && !strncmp(line,KJsLine,strlen(KJsLine)) && strncmp(line,KJsRemoteLine,strlen(KJsRemoteLine))){
            //we will replace this line with a file
            addFileToIndexHtml(fOut,line+strlen(KJsLine));
            continue;
        }
        if(inJs){
            //all css includes are included, end the </script>
            inJs=0;
            fprintf(fOut,"</script>\n");
            fputs(line,fOut);
            continue;
        }


        fputs(line,fOut);
    }//while

    fclose(fIn);
    fclose(fOut);
    LOG0("Done creating index.all.html");

    //now compress the file
    gzFile gzfOut=gzopen(KFilenameAllCompressed,"wb9");
    LOG0return(!gzfOut,-1,"WARNING: CreateIndexAllHtmlGz: Could not gzopen %s, returning.",KFilenameAllCompressed);

    fIn=fopen(KFilenameAllUncompressed,"r");
    ph_assert(fIn,NULL);
    fseek(fIn,0,SEEK_END);
    long size=ftell(fIn);
    fseek(fIn,0,SEEK_SET);

    char *buf=(char*)ph_malloc(size);
    int r=fread(buf,size,1,fIn);
    ph_assert(r==1,NULL);
    r=gzwrite(gzfOut,buf,size);
    ph_assert(r==size,NULL);
    fclose(fIn);
    free(buf);
    gzclose_w(gzfOut);
    LOG0("Done creating index.all.html.gz");

    return 0;
}
