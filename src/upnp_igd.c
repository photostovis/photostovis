#ifdef CONFIG_NO_PHONET
#error "This file should not be compiled in this build type"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <miniupnpc/miniupnpc.h>
#include <miniupnpc/upnpcommands.h>
#include <miniupnpc/upnperrors.h>
#include "photostovis.h"
#include "log.h"

static const char *KUPnPHttpDescription ="Photostovis server, http";
static const char *KUPnPHttpsDescription="Photostovis server, https";
static const char *KUPnPSshDescription="SSH (by Photostovis)";

static const char KProtoTCP[4] = { 'T', 'C', 'P', 0};

//TODO: DEBUG
#undef NDEBUG

#ifndef NDEBUG
static void ListRedirections(struct UPNPUrls * urls, struct IGDdatas * data){
    int r;
    int i = 0;
    char index[6];
    char intClient[40];
    char intPort[6];
    char extPort[6];
    char protocol[4];
    char desc[80];
    char enabled[6];
    char rHost[64];
    char duration[16];

    LOG0(" i protocol exPort->inAddr:inPort description remoteHost leaseTime");
    while(1) {
        snprintf(index, 6, "%d", i);
        rHost[0] = '\0'; enabled[0] = '\0';
        duration[0] = '\0'; desc[0] = '\0';
        extPort[0] = '\0'; intPort[0] = '\0'; intClient[0] = '\0';
        r = UPNP_GetGenericPortMappingEntry(urls->controlURL,
                                            data->first.servicetype,
                                            index,
                                            extPort, intClient, intPort,
                                            protocol, desc, enabled,
                                            rHost, duration);
        if(r)break;

        LOG0("%2d %s %5s->%s:%-5s '%s' '%s' %s",i, protocol, extPort, intClient, intPort, desc, rHost, duration);
        i++;
    };
}
#endif

/* Test function
 * 1 - get connection type
 * 2 - get extenal ip address
 * 3 - Add port mapping
 * 4 - get this port mapping from the IGD */
static enum GatewayCfgStatus SetRedirectAndTest(struct UPNPUrls * urls,
                                                struct IGDdatas * data,
                                                const char * iaddr,
                                                const char * iport,
                                                const char * eport,
                                                const char * leaseDuration,
                                                const char * description)
{
    char intClient[40];
    char intPort[6];
    char duration[16];
    int r;

    ph_assert(iaddr&&iport&&eport,NULL);

    //check if we have mapping already
    r=UPNP_GetSpecificPortMappingEntry(urls->controlURL,data->first.servicetype,
                                       eport, KProtoTCP, NULL/*remoteHost*/,
                                       intClient, intPort, NULL/*desc*/,
                                       NULL/*enabled*/, duration);

    if(r==UPNPCOMMAND_SUCCESS){
        //we have a mapping. If the internal client and port are the same, then we just leave it the way it is
        if(strcmp(intClient,iaddr) || strcmp(intPort,iport)){
            //this mapping is not exactly what we need
            LOG0("Wrong mapping of external port (%s) to %s:%s instead of %s:%s. Mapping will be replaced.",
                 eport,intClient,intPort,iaddr,iport);
            r=UPNP_DeletePortMapping(urls->controlURL,data->first.servicetype,eport,KProtoTCP,0);
            LOG0c(r!=UPNPCOMMAND_SUCCESS,"UPNP_DeletePortMapping returned an error (%d): %s",r,strupnperror(r));
        } else {
            LOG0return(1,GatewayCfgSuccess,"We already have the correct port mapping in place.");
        }
    } else {
        if(strupnperror(r))
            LOG0("UPNP_GetSpecificPortMappingEntry returned: %s",strupnperror(r));
        else
            LOG0("UPNP_GetSpecificPortMappingEntry returned an unknown error");
    }
    //else there is no mapping, we will create it

    //add the port mapping
    r=UPNP_AddPortMapping(urls->controlURL,data->first.servicetype,eport,iport,iaddr,description,KProtoTCP,0,leaseDuration);
    LOG0return(r!=UPNPCOMMAND_SUCCESS,GatewayCfgErrAddPortMappingFailed,
               "UPNP_AddPortMapping(%s, %s, %s) failed with code %d (%s)",eport,iport,iaddr,r,strupnperror(r));

    //verify the port mapping
    r=UPNP_GetSpecificPortMappingEntry(urls->controlURL,data->first.servicetype,
                                       eport, KProtoTCP, NULL/*remoteHost*/,
                                       intClient, intPort, NULL/*desc*/,
                                       NULL/*enabled*/, duration);
    LOG0return(r!=UPNPCOMMAND_SUCCESS,GatewayCfgErrGetSpecificPortMappingFailed,
               "UPNP_GetSpecificPortMappingEntry() failed with code %d (%s). Mapping probably failed.",r,strupnperror(r));

    LOG0("Port mapping succeeded! (%s<-->%s)",iport,eport);
    return GatewayCfgSuccess;
}

static enum GatewayCfgStatus RemoveRedirect(struct UPNPUrls * urls, struct IGDdatas * data, const char * eport){
    int r;

    ph_assert(eport,NULL);

    r=UPNP_DeletePortMapping(urls->controlURL,data->first.servicetype,eport,KProtoTCP,0);
    LOG0c(r!=UPNPCOMMAND_SUCCESS,"UPNP_DeletePortMapping returned an error (%d): %s",r,strupnperror(r));

    return GatewayCfgSuccess;
}

static enum GatewayCfgStatus SetSshRedirect(struct UPNPUrls * urls,
                                            struct IGDdatas * data,
                                            const char * iaddr,
                                            const char * iport,
                                            const char * eport,
                                            const char * leaseDuration,
                                            const char * description)
{
    char intClient[40];
    char intPort[6];
    char duration[16];
    int r;
    char sshMappingDescription[1024];

    ph_assert(iaddr&&iport&&eport,NULL);

    //check if we have mapping already
    r=UPNP_GetSpecificPortMappingEntry(urls->controlURL,data->first.servicetype,
                                       eport, KProtoTCP, NULL/*remoteHost*/,
                                       intClient, intPort, sshMappingDescription/*desc*/,
                                       NULL/*enabled*/, duration);

    if(r==UPNPCOMMAND_SUCCESS){
        //we have a mapping.
        if(strcmp(sshMappingDescription,description)){
            //This is not our description, which means there is another server registered on this port
            LOG0("SSH registration aborted because there is another (SSH?) server registered on the same port");
            return GatewayCfgSuccess;
        }

        //If the internal client and port are the same, then we just leave it the way it is
        if(strcmp(intClient,iaddr) || strcmp(intPort,iport)){
            //this mapping is not exactly what we need
            LOG0("Wrong mapping of external port (%s) to %s:%s instead of %s:%s. Mapping will be replaced.",
                 eport,intClient,intPort,iaddr,iport);
            r=UPNP_DeletePortMapping(urls->controlURL,data->first.servicetype,eport,KProtoTCP,0);
            LOG0c(r!=UPNPCOMMAND_SUCCESS,"UPNP_DeletePortMapping returned an error (%d): %s",r,strupnperror(r));
        } else LOG0return(1,GatewayCfgSuccess,"We already have the correct port mapping in place.");
    }
    //else there is no mapping, we will create it

    //add the port mapping
    r=UPNP_AddPortMapping(urls->controlURL,data->first.servicetype,eport,iport,iaddr,description,KProtoTCP,0,leaseDuration);
    LOG0return(r!=UPNPCOMMAND_SUCCESS,GatewayCfgErrAddPortMappingFailed,
               "UPNP_AddPortMapping(%s, %s, %s) failed with code %d (%s)",eport,iport,iaddr,r,strupnperror(r));

    //verify the port mapping
    r=UPNP_GetSpecificPortMappingEntry(urls->controlURL,data->first.servicetype,
                                       eport, KProtoTCP, NULL/*remoteHost*/,
                                       intClient, intPort, NULL/*desc*/,
                                       NULL/*enabled*/, duration);
    LOG0return(r!=UPNPCOMMAND_SUCCESS,GatewayCfgErrGetSpecificPortMappingFailed,
               "UPNP_GetSpecificPortMappingEntry() failed with code %d (%s). Mapping probably failed.",r,strupnperror(r));

    LOG0("SSH port mapping succeeded!");
    return GatewayCfgSuccess;
}

//this returns 0 if we found an IGD device,
static enum GatewayCfgStatus getIGDandData(struct UPNPUrls *urls, struct IGDdatas *data, char *internalIPAddress, const int internalIPAddressSize){
    int error=0;
    struct UPNPDev *devlist=upnpDiscover(2000,NULL,NULL,UPNP_LOCAL_PORT_ANY,0,2,&error);
    LOG0("upnpDiscover returned");

    if(!devlist){
        LOG0return(error,GatewayCfgErrUpnpDiscover,"upnpDiscover() error code=%d", error);
        LOG0return(1,GatewayCfgErrUpnpNoIGD,"upnpDiscover(): no UPnP devices discovered. Direct connection to internet?");
    }

    //show the UPnP devices on the network
#ifndef NDEBUG
    struct UPNPDev *device=NULL;
    LOG0("List of UPNP devices found on the network :");
    for(device=devlist; device; device=device->pNext){
        LOG0(" desc: %s  st: %s",device->descURL, device->st);
    }
#endif

    //find the right device
    memset(urls,0,sizeof(struct UPNPUrls));
    error=UPNP_GetValidIGD(devlist,urls,data, internalIPAddress, internalIPAddressSize);

    //free devlist, we don't need it anymore
    freeUPNPDevlist(devlist);
    devlist=NULL;

    LOG0close_return(error!=1,GatewayCfgErrUpnpNoIGD,FreeUPNPUrls(urls),"No valid IGD found. Not continuing with UPnP");

    return 0; //we found an IGD
}

enum GatewayCfgStatus configureGateway(int httpPort, int httpsPort, int unregisterHttpxPorts, int sshPortInt, int sshPortExt,
                                       uint32_t *lanIP, uint32_t *externalIP, int *upnpWorks){
    LOG0("configureGateway++: Ports: http: %d, https: %d, SSH internal: %d, SSH external: %d.",httpPort,httpsPort,sshPortInt,sshPortExt);

    if(upnpWorks)
        *upnpWorks=0; //if we return before a certain point it means that upnp does not work
    //TODO: listen on UPNP broadcast address for external IP address change

    struct UPNPUrls urls;
    struct IGDdatas data;
    char internalIPAddress[40];	/* my ip address on the LAN */
    char externalIPAddress[40];
    char iPort[6],ePort[6];

    ph_assert(lanIP,NULL);
    ph_assert(externalIP,NULL);
    *lanIP=*externalIP=INADDR_NONE; //in case we return early

    int error=getIGDandData(&urls,&data,internalIPAddress,sizeof(internalIPAddress));
    LOG0return(error,error,"configureGateway-- (no IGD)");


    *lanIP=(uint32_t)ntohl(inet_addr(internalIPAddress));
    LOG0close_return(*lanIP==INADDR_NONE,GatewayCfgErrCannotGetLanIP,FreeUPNPUrls(&urls),
            "Cannot get internal IP address. Aborting the internet gateway configuration.");

    if(upnpWorks)
        *upnpWorks=1; //if we are here it means that UPnP works

    memset(externalIPAddress,0,sizeof(externalIPAddress));
    UPNP_GetExternalIPAddress(urls.controlURL,data.first.servicetype,externalIPAddress);
    *externalIP=(uint32_t)ntohl(inet_addr(externalIPAddress));
    LOG0c(*externalIP==INADDR_NONE,"External IP address unavailable!");

    LOG0("UPNP Discovered IP addresses: Local: %s (%08x), External: %s (%08x). Port: %d",
         internalIPAddress,*lanIP,externalIPAddress,*externalIP,httpPort);

#ifndef NDEBUG
    //We do not list redirections anymore (in the release version). This info was never that useful
    ListRedirections(&urls,&data);
#endif

    //register our http port, if we have one
    ph_assert(httpPort>0,NULL);
    snprintf(ePort,6,"%d",httpPort);
    if(unregisterHttpxPorts){
        error=RemoveRedirect(&urls,&data,ePort);
    } else {
        snprintf(iPort,6,"%d",httpPort);
        error=SetRedirectAndTest(&urls,&data,internalIPAddress,iPort,ePort,"0",KUPnPHttpDescription);
        if(error!=GatewayCfgSuccess && *externalIP==INADDR_NONE && upnpWorks)
            *upnpWorks=0;
    }

    //register our https port, if we have one
    if(httpsPort>0){
        snprintf(ePort,6,"%d",httpsPort);
        if(unregisterHttpxPorts){
            error=RemoveRedirect(&urls,&data,ePort);
        } else {
            snprintf(iPort,6,"%d",httpsPort);
            error=SetRedirectAndTest(&urls,&data,internalIPAddress,iPort,ePort,"0",KUPnPHttpsDescription);
            if(error!=GatewayCfgSuccess && *externalIP==INADDR_NONE && upnpWorks)
                *upnpWorks=0;
        }
    }

    //register the ssh port
    if(sshPortInt>0 && sshPortExt>0){
        snprintf(iPort,6,"%d",sshPortInt);
        snprintf(ePort,6,"%d",sshPortExt);
        error=SetSshRedirect(&urls,&data,internalIPAddress,iPort,ePort,"0",KUPnPSshDescription);
    } //TODO: we do not know how to unregister the ssh ports

    //free data and return

    FreeUPNPUrls(&urls);

    LOG0("configureGateway--");
    return 0;
}

enum GatewayCfgStatus closeGateway(int httpPort, int httpsPort, uint32_t lanIP){
    LOG0("Close gateway++: Ports: http: %d, https: %d.",httpPort,httpsPort);

    struct UPNPUrls urls;
    struct IGDdatas data;
    char internalIPAddress[40];	/* my ip address on the LAN */

    char ePort[6];
    uint32_t discoveredLanIP;

    int error=getIGDandData(&urls,&data,internalIPAddress,sizeof(internalIPAddress));
    LOG0return(error,error,"configureGateway-- (no IGD)");


    discoveredLanIP=(uint32_t)ntohl(inet_addr(internalIPAddress));
    LOG0close_return(discoveredLanIP==INADDR_NONE,GatewayCfgErrCannotGetLanIP,FreeUPNPUrls(&urls),
            "Cannot get internal IP address. Aborting the internet gateway closing.");
    LOG0close_return(discoveredLanIP!=lanIP,GatewayCfgErrCannotGetLanIP,FreeUPNPUrls(&urls),
            "Discovered internal IP is different from our internal IP value. Aborting closing procedure.");
    //
    snprintf(ePort,6,"%d",httpPort);
    error=RemoveRedirect(&urls,&data,ePort);
    if(httpsPort>0){
        snprintf(ePort,6,"%d",httpsPort);
        error=RemoveRedirect(&urls,&data,ePort);
    }
    //we do not unregister the ssh port because we may need access by ssh even when the server is not working

    //free data and return
    FreeUPNPUrls(&urls);

    LOG0("Close gateway--");
    return error;
}
