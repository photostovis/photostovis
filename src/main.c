#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>
#include <dirent.h>
#include <ctype.h>
#include <limits.h>
#include <pwd.h> //for finding photostovis uid and gid
#include <execinfo.h>
#ifdef __APPLE__
#include <sys/sysctl.h>
#else
#include <sys/prctl.h>
#endif
#include "photostovis.h"
#include "log.h"


const char *KPhotostovisDaemonName="photostovis_d";
const char *KPhotostovisName="photostovis";
const char *KPhotostovisScriptName="photostovisd";

const char *KSystemLogPath="/data/photostovis/log";
const int KExtraCurrentPath=128;
const int KNrSecondsToWaitIfPhotostovisRunning=10;

const char *KDefaultDocRootLocal="../photostovis/web/";
const char *KDefaultDocRootSystem="/usr/share/photostovis/web/";

const char *KConfigFilenameLocal="photostovis.conf";
const char *KConfigFilenameSystem="/etc/photostovis.conf";

static const char *KDaemonLogfileNameSystem="/data/photostovis/log/daemon-%F---%H-%M-%S.log";
static const char *KDaemonLogfileNameSystemNoHDD="/var/log/photostovis_daemon-%F---%H-%M-%S.log";
static const char *KDaemonLogfileNameLocal="daemon-%F---%H-%M-%S.log";

uint32_t myHardware;
uid_t photostovisUid=65534; //this is default for nobody
gid_t photostovisGid=65534;
uint8_t daemonFlags=0;

int isPhotostovisRunning(){
    //returns 1 if it is running, zero if not
    pid_t p,myPid=getpid();
    size_t i, j;
    char* s = (char*)malloc(264);
    char buf[128];
    FILE* st;
    DIR* d=opendir("/proc");
    if(!d){
        free(s);
        return -3;
    }
    struct dirent* f;
    while ((f = readdir(d)) != NULL) {
        if (f->d_name[0] == '.') continue;
        for (i = 0; isdigit(f->d_name[i]); i++);
        if (i < strlen(f->d_name)) continue;
        strcpy(s, "/proc/");
        strcat(s, f->d_name);
        strcat(s, "/status");
        st = fopen(s, "r");
        if (st == NULL) { closedir(d); free(s); return -3; }
        do {
            if (fgets(buf, 128, st) == NULL) { fclose(st); closedir(d); free(s); return -3; }
        } while (strncmp(buf, "Name:", 5));
        fclose(st);
        for (j = 5; isspace(buf[j]); j++);
        *strchr(buf, '\n') = 0;
        if (!strncmp(&(buf[j]), KPhotostovisName, strlen(KPhotostovisName)) && strcmp(&(buf[j]), KPhotostovisScriptName)) {
            sscanf(&(s[6]), "%d", &p);
            if(p==myPid)continue;
            //looks like we found a photostovis instance
            LOG0("Found photostovis process with PID=%d (%s). My PID=%d",p,&(buf[j]),myPid);
            closedir(d);
            free(s);
            return 1;
        }
    }
    //if here, nothing found
    closedir(d);
    free(s);
    return 0;
}

int killAllPhotostovis() {
    pid_t p,myPid=getpid();
    size_t i, j;
    char* s = (char*)malloc(264);
    char buf[128];
    FILE* st;
    DIR* d=opendir("/proc");
    if(!d){
        free(s);
        return -3;
    }
    struct dirent* f;
    int err;
    while ((f = readdir(d)) != NULL) {
        if (f->d_name[0] == '.') continue;
        for (i = 0; isdigit(f->d_name[i]); i++);
        if (i < strlen(f->d_name)) continue;
        strcpy(s, "/proc/");
        strcat(s, f->d_name);
        strcat(s, "/status");
        st = fopen(s, "r");
        if (st == NULL) { closedir(d); free(s); return -3; }
        do {
            if (fgets(buf, 128, st) == NULL) { fclose(st); closedir(d); free(s); return -3; }
        } while (strncmp(buf, "Name:", 5));
        fclose(st);
        for (j = 5; isspace(buf[j]); j++);
        *strchr(buf, '\n') = 0;
        if (!strncmp(&(buf[j]), KPhotostovisName, strlen(KPhotostovisName)) && strcmp(&(buf[j]), KPhotostovisScriptName)) {
            sscanf(&(s[6]), "%d", &p);
            if(p==myPid)continue;
            err=kill(p,SIGKILL);
            if(err)
                LOG0("%s: ERROR sending signal SIGKILL to process with PID=%u: %s\n",KPhotostovisName,p,strerror(errno));
            else LOG0("KILLED process with PID=%d",p);
        }
    }
    closedir(d);
    free(s);
    return 0;
}


int signalToChild(int signalNr, const char *name) {
    pid_t p,myPid=getpid();
    size_t i, j;
    char* s = (char*)malloc(264);
    char buf[128];
    FILE* st;
    DIR* d=opendir("/proc");
    if(!d){
        free(s);
        return -3;
    }
    struct dirent* f;
    int err, found=-2; //-2 means not found
    int nameLen=strlen(name);
    //printf("My PID: %u\n",myPid);
    while ((f = readdir(d)) != NULL) {
        if (f->d_name[0] == '.') continue;
        for (i = 0; isdigit(f->d_name[i]); i++);
        if (i < strlen(f->d_name)) continue;
        strcpy(s, "/proc/");
        strcat(s, f->d_name);
        strcat(s, "/status");
        st = fopen(s, "r");
        if (st == NULL) { closedir(d); free(s); return -3; }
        do {
            if (fgets(buf, 128, st) == NULL) { fclose(st); closedir(d); free(s); return -3; }
        } while (strncmp(buf, "Name:", 5));
        fclose(st);
        for (j = 5; isspace(buf[j]); j++);
        *strchr(buf, '\n') = 0;
        if (!strncmp(buf+j,name,nameLen) && (strlen(buf+j)==nameLen || (strlen(buf+j)==nameLen+1 && buf[j+nameLen]>='0' && buf[j+nameLen]<='9'))) {
            sscanf(&(s[6]), "%d", &p);
            if(p==myPid)continue;
            err=kill(p,signalNr);
            if(err)
                printf("%s: ERROR sending signal %d to process with PID=%u: %s\n",KPhotostovisName,signalNr,p,strerror(errno));
            else printf("Photostovis process with PID=%d (%s) signalled with %s.\n",p,buf+j,strsignal(signalNr));
            if(err<0)found=err;
            else found=p;
        }
    }
    closedir(d);
    free(s);
    return found;
}

static void sigAlrmFunction(int s){
    LOG0("Signal caught: %s",strsignal(s));
}

void sigUSR1FunctionDaemon(int s){
    if(daemonFlags&DaemonFlagFlushLog){
        daemonFlags&=~DaemonFlagFlushLog;
        LOG0("Logging changed to normal/cached mode.");
    } else {
        LOG0("Logging changed to debug/flushing mode.");
        daemonFlags|=DaemonFlagFlushLog;
    }
}

static void sigAbortFunction(int s){
    unsigned int msSinceStart=LOG0("Daemon: Signal caught: %s",strsignal(s));

    //print trace
    const int KArraySize=20;
    void *array[KArraySize];
    size_t size,i;
    char **strings;

    size=backtrace(array,KArraySize);
    strings=backtrace_symbols(array,size);

    LOG0("Obtained %zu stack frames.",size);
    for(i=0;i<size;i++)
        LOG0("%s",strings[i]);
    free(strings);

    if(msSinceStart>600*1000){
        LOG0("Daemon crashed after %u minutes since start. Rebooting (although a restart MAY be just enough).",msSinceStart/60000);
        fflush(stderr);
        /*err=*/system("/sbin/reboot");
    } else {
        LOG0("Daemon crashed after %u seconds since start. Will not reboot.",msSinceStart/1000);
        fflush(stderr);
    }
}


static uint32_t getHardware(){
    int nrCores=sysconf(_SC_NPROCESSORS_ONLN);

    //first 4 bits are the architecture. Most significant bit is the processor type: ARM(1) vs Intel (0)
#ifdef __i386
    uint32_t hw=0;          //0000 xxxx  xxxx xxxx
#endif
#ifdef __x86_64
    uint32_t hw=0x10000000; //0001 xxxx  xxxx xxxx
#endif
#ifdef __ARM_ARCH_6__ //e.g. Raspberry Pi 1
    uint32_t hw=0x80000000; //1000 xxxx  xxxx xxxx
#endif
#ifdef __ARM_ARCH_7A__ //e.g. Raspberry Pi 2, Cubieboard, etc
    uint32_t hw=0x90000000; //1001 xxxx  xxxx xxxx
//define the models we know about in this architecture:
#define MODEL_CUBIEBOARD     0x01000000
#define MODEL_RASPBERRY_PI2  0x02000000
#define MODEL_RASPBERRY_PI3  0x03000000
#define MODEL_RASPBERRY_PI3P 0x04000000
#define MODEL_TINKERBOARD    0x05000000
#define MODEL_ODROID_XU4     0x06000000
#endif
#ifdef __ARM_ARCH_8A__ //e.g. Raspberry Pi 3 64bit,
    uint32_t hw=0xA0000000; //1010 xxxx  xxxx xxxx
//define the models we know about in this architecture:
#define MODEL_CUBIEBOARD     0x01000000
#define MODEL_RASPBERRY_PI2  0x02000000
#define MODEL_RASPBERRY_PI3  0x03000000
#define MODEL_RASPBERRY_PI3P 0x04000000
#define MODEL_TINKERBOARD    0x05000000
#define MODEL_ODROID_XU4     0x06000000
#endif
#if defined(__arm64__) && defined(__APPLE__)
    uint32_t hw=0x20000000; //0010 xxxx  xxxx xxxx
#endif 

    //next 4 bits: model
    FILE *cpuinfo=fopen("/proc/cpuinfo","r");
    if(cpuinfo){
        char line[1024];
#ifdef __arm__
        char *infoStart;
        int tmp1=0;
#endif

        while(fgets(line,1024,cpuinfo)){

#if defined(__ARM_ARCH_7A__) || defined(__ARM_ARCH_8A__)
            if(!strncasecmp(line,"Hardware",8) && (infoStart=strstr(line,": "))){
                infoStart+=2;
                char c=infoStart[strlen(infoStart)-1];
                while(c=='\n' || c=='\r' || c==' '){
                    infoStart[strlen(infoStart)-1]='\0';
                    c=infoStart[strlen(infoStart)-1];
                }
                LOG0("Detected hardware: <<%s>>",infoStart);
                if(!strcmp(infoStart,"BCM2835") || !strcmp(infoStart,"BCM2837"))
                    tmp1=1; //this means this is a Raspberry Pi
                if(!strcmp(infoStart,"sun7i"))
                    tmp1=2; //this means this is potentially a cubieboard/cubietruck
                if(!strcmp(infoStart,"Rockchip (Device Tree)"))
                    tmp1=3; //this means this is potentially a Tinkerboard
                if(!strcmp(infoStart,"Hardkernel ODROID-XU4")){
                    tmp1=4; //this means this is an ODROID-XU4 (or HC1 or HC2)
                    hw|=MODEL_ODROID_XU4;
                }
            }
            if(!strncasecmp(line,"Revision",8) && (infoStart=strstr(line,": "))){
                infoStart+=2;
                char c=infoStart[strlen(infoStart)-1];
                while(c=='\n' || c=='\r' || c==' '){
                    infoStart[strlen(infoStart)-1]='\0';
                    c=infoStart[strlen(infoStart)-1];
                }
                LOG0("Detected revision: <<%s>>",infoStart);
                if((!strcmp(infoStart,"a01040") || !strcmp(infoStart,"a01041") || !strcmp(infoStart,"a21041") || !strcmp(infoStart,"a22042"))
                        && nrCores==4 && tmp1==1)
                    hw|=MODEL_RASPBERRY_PI2;
                if((!strcmp(infoStart,"a02082") || !strcmp(infoStart,"a22082") || !strcmp(infoStart,"a32082")  || !strcmp(infoStart,"a52082"))
                        && nrCores==4 && tmp1==1)
                    hw|=MODEL_RASPBERRY_PI3;
                if((!strcmp(infoStart,"a020d3"))
                        && nrCores==4 && tmp1==1)
                    hw|=MODEL_RASPBERRY_PI3P;
            }
            if(tmp1==2 && nrCores==2)
                hw|=MODEL_CUBIEBOARD;
            if(tmp1==3 && nrCores==4){
                hw|=MODEL_TINKERBOARD;
                LOG0("Detected: probably Tinkerboard");
            }
#endif
        }//while
        fclose(cpuinfo);
    }//if(cpuinfo)

    //next 4 bits: 0=1 core, 15=32 cores, anything else=cores/2 (e.g. 1 means 2 cores, 2 means 4 cores, 3 means 6 cores, 4 means 8 cores)

    if(nrCores>0){
        nrCores>>=1;
        if(nrCores>15)nrCores=15;
    } else nrCores=14; //if this fails, we report 28 core, most probably such a thing does not exist
    hw|=(nrCores<<20);

    //next 4 bits: unused

    //last 16 bits: the ammout of memory in MB
#ifdef __APPLE__
    int mib[2]={CTL_HW,HW_MEMSIZE};
    unsigned int namelen=sizeof(mib)/sizeof(mib[0]);
    uint64_t mem;
    size_t len=sizeof(mem);
    if(!sysctl(mib,namelen,&mem,&len,NULL,0)){
        mem>>=20;
        LOG0("This system has %u MB.",(unsigned int)mem);
        if(mem>0x10000)
            mem=0xFFFF;
        hw|=(uint16_t)mem;
    }
#else
    uint64_t mem=getpagesize();
    ph_assert(mem>0,"getpagesize() returned <=0");
    long int nrPages=sysconf(_SC_PHYS_PAGES);
    if(nrPages>0){
        mem*=(uint64_t)nrPages;
        mem>>=20;
        LOG0("This system has %u MB.",(unsigned int)mem);
        if(mem>0x10000)
            mem=0xFFFF;
        hw|=(uint16_t)mem;
    }
#endif
    return hw;
}

int checkAndChangeScriptBin(){
	FILE *f=fopen("/boot/script.bin","r");
	uint8_t buf[50072];
	uint32_t crc=0;
	int fSize,i,r;
	if(!f)return -1;
	//check the size
	fseek(f,0,SEEK_END);
	fSize=ftell(f);
    if(fSize!=50072){fclose(f);return -2;}
	fseek(f,0,SEEK_SET);
	//read the file in memory
	r=fread(buf,50072,1,f);
	fclose(f);
	if(r!=1)return -3;
	if(buf[47920]==4)return 0;
	if(buf[47920]!=2)return -4;
	
	for(i=0;i<50072;i++)
	    crc+=buf[i];
	if(crc!=0x1e71e6)return -5;

    LOG0("/boot/script.bin will be upgraded\n");
	buf[47920]=4;
	f=fopen("/boot/script.bin","w");
	if(!f)return -6;
	fwrite(buf,50072,1,f);
	fclose(f);
    LOG0("Upgrade done, will reboot\n");
    r=system("/sbin/reboot");
	return 1;
}

static char *startDaemonLogging(const char *currentPath, const int currentPathLen){
    char *daemonLogFilename=NULL;

    time_t startTime=time(NULL);
    struct tm startTm;
    localtime_r(&startTime,&startTm);
    char tempLogFilename[PATH_MAX];
    FILE *logFile=NULL;
    //we have now our filename
    if(currentPath){
        memcpy(tempLogFilename,currentPath,currentPathLen);
        strftime(tempLogFilename+currentPathLen,PATH_MAX+currentPathLen,KDaemonLogfileNameLocal,&startTm);
        logFile=fopen(tempLogFilename,"w");
    } else {
        strftime(tempLogFilename,PATH_MAX,KDaemonLogfileNameSystem,&startTm);
        logFile=fopen(tempLogFilename,"w");
        if(!logFile){
            //is there a HDD? Try the "classic" location.
            printf("%s: Could not reopen stderr to %s.\n",KPhotostovisDaemonName,tempLogFilename);
            strftime(tempLogFilename,PATH_MAX,KDaemonLogfileNameSystemNoHDD,&startTm);
            logFile=fopen(tempLogFilename,"w");
        }
    }
    close(STDIN_FILENO); //this can fail if already closed
    if(!logFile){
        printf("%s: Could not open log file (%s).\n",KPhotostovisDaemonName,tempLogFilename);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
        return NULL;
    }
    dup2(fileno(logFile),STDOUT_FILENO);
    dup2(fileno(logFile),STDERR_FILENO);
    fclose(logFile);
    //if here, we succeeded!
    resetLog(-1); //reset the time
    daemonLogFilename=ph_strdup(tempLogFilename);
    return daemonLogFilename;
}

static void freeConfigs(struct ps_md_config **configs, int nrConfigs){
    int i;
    for(i=0;i<nrConfigs;i++){
        struct ps_md_config * __restrict config=configs[i];
        if(!config)continue;

        if(config->srvDocRoot!=KDefaultDocRootSystem && config->srvDocRoot!=KDefaultDocRootLocal)
            free(config->srvDocRoot);
        free(config->phonetUsername);
        free(config->phonetPassword);
        free(config->authUsers);
        if(config->rootFolder!=KOptionsRootFolder)
            free(config->rootFolder);
        free(config);
    }
    free(configs);
}


int launchPhotostovisAndLogPair(struct ps_md_config **configs, int nrConfigs, int currentConfig){
    int err;
    int logPipe[2];
    struct ps_md_config *config=configs[currentConfig];
    //create the logging pair of UNIX sockets
    err=pipe(logPipe);
    LOG0return(err==-1,-1,"ERROR creating log pipe with pipe: %s",strerror(errno));

    //create the photostovis process
    config->pidPhotostovis=fork();
    LOG0return(config->pidPhotostovis<0,-1,"%s: ERROR: could not fork to create photostovis, exiting.",KPhotostovisDaemonName);

    if(!config->pidPhotostovis){
        //in child (photostovis) process

        //change name back to photostovis
#ifndef __APPLE__
        if(nrConfigs>1){
            //we add the config number to the process
            int l=strlen(KPhotostovisName);
            ph_assert(nrConfigs<10,NULL);
            ph_assert(l<15,NULL);
            char photostovisName[17];
            memcpy(photostovisName,KPhotostovisName,l);
            photostovisName[l++]=currentConfig+'0';
            photostovisName[l]='\0';

            err=prctl(PR_SET_NAME,photostovisName);
            LOG0c(err,"%s: could not set process name to \"%s\"",KPhotostovisName,photostovisName);
        } else {
            err=prctl(PR_SET_NAME,KPhotostovisName);
            LOG0c(err,"%s: could not set process name to \"%s\"",KPhotostovisName,KPhotostovisName);
        }
#endif
        //close the log, we will be using the log daemon
        close(logPipe[0]); //close read end
        resetLog(logPipe[1]);
        //close stderr and stdout
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
        //free configs
        configs[currentConfig]=NULL;
        freeConfigs(configs,nrConfigs);
        //run photostovis
        err=photostovis(config); //config freed inside photostovis
        exit(err);
    }

    //here we are in parent process
    close(logPipe[1]); //close write end
    LOG0("%s process created with PID=%d for configuration having root folder=%s.",KPhotostovisName,config->pidPhotostovis,config->rootFolder);

    //create the logging thread
    pthread_t t;
    pthread_attr_t attr;
    err=pthread_attr_init(&attr);
    LOG0return(err,-1,"pthread_attr_init failed.");
    err=pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
    LOG0return(err,-1,"pthread_attr_setdetachstate failed.");

    struct ph_log_data *data=ph_malloc(sizeof(struct ph_log_data));
    data->srvSocket=logPipe[0];
    data->rootPath=ph_strdup(config->rootFolder);
    err=pthread_create(&t,&attr,&logServerThread,data);//transfering ownership of data

    return 0;
}

static int parsePermissions(const char *permissionsStr, uint8_t * const permissions){
    ph_assert(strlen(permissionsStr)==8,NULL);
    *permissions=0;
    int i;
    for(i=0;i<8;i++){
        if(permissionsStr[i]=='1')(*permissions)++;
        else ph_assert(permissionsStr[i]=='0',NULL);

        if(i<7)(*permissions)<<=1;
    }
    return 0;
}

static int getTrueOrFalse(const char *arg){
    if(!strcasecmp(arg,"true"))return 1;
    if(!strcasecmp(arg,"false"))return 0;
    return -1; //neither true or false
}

static int upgradePackage(const char *cmd){
    //install the new package
    LOG0("Upgrading to a new package. Upgrade script output follows:");
    LOG0("==========================================================");
    int err=system(cmd);
    LOG0("==========================================================");
    err=WEXITSTATUS(err);
    if(err){
        LOG0("Upgrading failed. Sleeping 1 minute then trying again.");
        sleep(60);
        LOG0("Upgrading to a new package (second attempt). Upgrade script output follows:");
        LOG0("==========================================================");
        err=system(cmd);
        LOG0("==========================================================");
        err=WEXITSTATUS(err);
    }
    return err;
}

int main(int argc, char **argv){
    int opt,daemonize=0;
    int status,err,currentPathSize=0,currentPathLen=0;
    char *currentPath=NULL;
    char *daemonLogFilename=NULL;
    FILE *f=NULL;
    int nrConfigs=0,cfgVersion=0;
    struct ps_md_config **configs=NULL;
    pid_t pid;

    while((opt=getopt(argc,argv,"dDkKcv"))!=-1){
        switch (opt) {
        case 'k':{
            printf("Stopping photostovis\n");
            err=signalToChild(SIGINT,KPhotostovisName);
            if(err==-2){
                printf("%s: WARNING: No photostovis process was found.\n",KPhotostovisName);
                return -1;
            }
            if(err>0){
                printf("Succesfully sent SIGINT to photostovis process(es)\n");
                return 0;
            } else return 1;
        };//break;
        case 'K':{
            printf("Stopping photostovis\n");
            err=signalToChild(SIGINT,KPhotostovisName);
            if(err==-2){
                printf("%s: INFO: No photostovis process was found.\n",KPhotostovisName);
                return -1;
            }
            if(err<0)return 1;
            else return 0;
        };//break;
        case 'c':{
            //we should check if we have a child process
            err=signalToChild(0,KPhotostovisName);
            if(err>0)return 0; //photostovis running
            if(err==-1)return 4; //program status unknown
            return 1; //nothing running
        };//break;
        case 'v':
            printf("%s\n",KPhotostovisServerString);
            return 0;
            //break;
        case 'd':
            daemonize=1;
            currentPath=getcwd(NULL,0);//get_current_dir_name();
            if(!currentPath){
                printf("%s: getting current directory name failed with the error: %s. Exiting.\n",KPhotostovisName,strerror(errno));
                return -1;
            }
            currentPathLen=strlen(currentPath);
            if(currentPathLen<1){
                printf("%s: current directory name too short. Exiting.\n",KPhotostovisName);
                return -1;
            }
            if(currentPath[0]!='/'){
                printf("%s: current path (%s) does not start with /\n",KPhotostovisName,currentPath);
                return -1;
            }

            currentPathSize=currentPathLen+KExtraCurrentPath;
            currentPath=(char*)ph_realloc(currentPath,currentPathSize);
            if(currentPath[currentPathLen-1]!='/'){
                //currentPath should end with /
                currentPath[currentPathLen++]='/';
                currentPath[currentPathLen]='\0';
            }
            printf("Going into local daemon mode. This should be the last console message.\n");
            break;
        case 'D':
            daemonize=1;
            //currentPath remains NULL
            break;
        default:
            printf("Usage: %s [-d | -D | -k | -K | -c | -v]\n",KPhotostovisName);
            return -1;
        }//switch
    }//while
    //NOTHING IS ALLOWED TO CRASH IN THE DAEMON!

    struct sigaction sigHandler;
    //catch sigpipe, sigabrt, sigsegv, etc
    sigHandler.sa_handler=sigAbortFunction;
    sigemptyset(&sigHandler.sa_mask);
    sigHandler.sa_flags=0;
    sigaction(SIGPIPE,&sigHandler,NULL);
    sigaction(SIGABRT,&sigHandler,NULL);
    sigaction(SIGSEGV,&sigHandler,NULL);
    sigaction(SIGFPE,&sigHandler,NULL);

    //catch sigusr1
    sigHandler.sa_handler=sigUSR1FunctionDaemon;
    sigemptyset(&sigHandler.sa_mask);
    sigHandler.sa_flags=0;
    sigaction(SIGUSR1,&sigHandler,NULL);

    //get the photostovis uid and gid
    {
        struct passwd *pw=getpwnam("photostovis");
        if(pw){
            photostovisUid=pw->pw_uid;
            photostovisGid=pw->pw_gid;
        } else {
            //try with "pi" username
            pw=getpwnam("pi");
            if(pw){
                photostovisUid=pw->pw_uid;
                photostovisGid=pw->pw_gid;
            }
        }
    }
    //if the above does not work, then photostovisUid and photostovisGid remain 65534 (nobody)

    //we will be launching Photostovis.
    if(daemonize){
        if(access("/data/photostovis",R_OK|W_OK|X_OK)){
            //we need to create /data/photostovis

            if(access("/data",R_OK|W_OK|X_OK)){
                err=mkdir("/data",0755);
                if(err){
                    printf("Canot create folder '/data'. Error: %s\n",strerror(errno));
                    return -1;
                }
            }
            if(access("/data/photostovis",R_OK|W_OK|X_OK)){
                err=mkdir("/data/photostovis",0755);
                if(err){
                    printf("Canot create folder '/data'. Error: %s\n",strerror(errno));
                    return -1;
                }
                if(photostovisUid!=65534){
                    chown("/data/photostovis",photostovisUid,photostovisGid);
                }
            }
        }
        //here we should have /data/photostovis

        if(!currentPath){
            //Delete all /var/log/photostovis files
            err=system("/bin/rm -f /var/log/photostovis*"); //-f so that it does not report "no files found"
            //we need to create /data/photostovis/log if it does not exist
            dirService(KSystemLogPath,1);
        }
        daemonLogFilename=startDaemonLogging(currentPath,currentPathLen);
        if(!daemonLogFilename){
            printf("No logging. Exiting.\n");
            goto reboot;
            //TODO: probably it is better to start the server and tell something to the user
        }

        //fork, daemonize HERE. Here because the launching script may be waiting for us.
        pid=fork();
        LOG0goto(pid<0,reboot,"%s: could not fork (%s), rebooting.",KPhotostovisName,strerror(errno));
        if(pid>0)exit(0);//exit the parent process

        //in child process
        umask(0);

        //new sid
        pid_t sid=setsid();
        LOG0goto(sid<0,reboot,"%s: ERROR: could not get new SID (%s), rebooting.",KPhotostovisDaemonName,strerror(errno));
    }
    //else we will just output to stderr
    LOG0(KPhotostovisServerString);
    LOG0c(daemonize,"Daemonized! (pid: %d)",(int)getpid());

    /////////////////////////////////////////////////
    /// A bit of housekeeping
    /////////////////////////////////////////////////

    //if here, we have to launch a photostovis process. Wait, if there are other photostovis processes still alive
    int i,j,r,runs;
    for(i=0;i<KNrSecondsToWaitIfPhotostovisRunning;i++){
        runs=isPhotostovisRunning();
        if(runs<=0)break; //if it is less than zero it is an error. E.g. running on Mac
        LOG0("Found Photostovis process(es) running. Will wait %d/%d seconds.",i+1,KNrSecondsToWaitIfPhotostovisRunning);
        sleep(1);
    }
    if(runs>0){
        LOG0("WARNING: Killing all Photostovis processes before going further!");
        killAllPhotostovis();
        LOG0("All Photostovis processes killed.");
    }

    /////////////////////////////////////////////////
    /// Read the global configuration file (if any) and create configuration(s).
    /////////////////////////////////////////////////
    //check if we have a configuration file
    if(!currentPath){
        if(daemonize)f=fopen(KConfigFilenameSystem,"r");
        else f=fopen(KConfigFilenameLocal,"r");
    } else {
        //we are looking for the config file in another folder
        ph_assert(daemonize,NULL);
        ph_assert(currentPathLen+strlen(KConfigFilenameLocal)<=currentPathSize,NULL);
        ph_assert(currentPathLen>=1,NULL);
        ph_assert(currentPath[currentPathLen-1]=='/',NULL);
        memcpy(currentPath+currentPathLen,KConfigFilenameLocal,strlen(KConfigFilenameLocal)+1);

        f=fopen(currentPath,"r");
        currentPath[currentPathLen]='\0';
    }
    if(f){
        //we found a configuration file
        char line[1024],*ret,*cmd,*arg;
        int sshPortExt=0,sshPortInt=0;
        int thisConfigHasAnonymousPermissions=0;
        int thisConfigHasAuthWithKeyPermissions=0;


        LOG0("Found a configuration file.");
        while(1){
            ret=fgets(line,sizeof(line),f);
            if(!ret)break;

            //is this a comment?
            if(line[0]=='#')continue;
            //remove the EOL
            trim_string_right(line);
            r=strlen(line);
            if(r<=0)continue; //empty line

            //parse our line
            arg=strchr(line,'=');
            if(!arg){
                LOG0("WARNING: Skipping invalid configuration line: %s",line);
                continue;
            }
            //LOG0("Line: %s",line);

            arg[0]='\0';
            arg++;
            cmd=line;

            //trim cmd and arg
            trim_string(&cmd);
            trim_string(&arg);

            //we need to make sure we have at least one configurartion here
            if(!nrConfigs){
                ph_assert(!configs,NULL);
                nrConfigs=1;
                i=0;
                configs=(struct ps_md_config**)ph_malloc(sizeof(struct ps_md_config*));
                configs[0]=(struct ps_md_config*)ph_malloc(sizeof(struct ps_md_config));
                memset(configs[0],0,sizeof(struct ps_md_config));
            }


            //do stuff
            if(!strcasecmp(cmd,"version")){
                cfgVersion=strtol(arg,NULL,10);
                ph_assert(cfgVersion>0 && cfgVersion<10,"Invalid version parsed from config file: %d.",cfgVersion);
                LOG0("Configuration file version: %d",cfgVersion);
                continue;
            }
            if(!strcasecmp(cmd,"pic_folder") || !strcasecmp(cmd,"root_folder")){
                //a pic_folder config option starts a new configuration (for a separate photostovis config) IF the current config's rootFolder already has a value
                if(configs[i]->rootFolder){
                    //set/verify anonymous and authwithkey permissions for old configuration
                    if(!thisConfigHasAnonymousPermissions)
                        configs[i]->permissionsAnonymous=DEFAULT_PERMISSIONS_ANONYMOUS;
                    if(!thisConfigHasAuthWithKeyPermissions)
                        configs[i]->permissionsAuthWithKey=DEFAULT_PERMISSIONS_AUTHWITHKEY;


                    nrConfigs++;
                    i=nrConfigs-1;
                    configs=(struct ps_md_config**)ph_realloc(configs,nrConfigs*sizeof(struct ps_md_config*));
                    configs[i]=(struct ps_md_config*)ph_malloc(sizeof(struct ps_md_config));
                    memset(configs[i],0,sizeof(struct ps_md_config));
                    thisConfigHasAnonymousPermissions=thisConfigHasAuthWithKeyPermissions=0;
                }
                ph_assert(!configs[i]->rootFolder,NULL);

                //set the rootFolder option
                if(arg[0]!='/' && currentPath){
                    configs[i]->rootFolder=(char*)ph_malloc(currentPathLen+strlen(arg)+1);
                    memcpy(configs[i]->rootFolder,currentPath,currentPathLen);
                    memcpy(configs[i]->rootFolder+currentPathLen,arg,strlen(arg)+1);
                } else
                    configs[i]->rootFolder=ph_strdup(arg);
                LOG0("[photostovis #%d] Configured pictures folder: %s",i,configs[i]->rootFolder);
                continue;
            }

            if(!strcasecmp(cmd,"port")){
                int port=strtol(arg,NULL,10);
                ph_assert(port>=0 && port<65536,"Invalid port parsed from config file: %d.",port);
                configs[i]->httpPort=port;
                LOG0("[photostovis #%d] Configured http port: %d",i,port);
                continue;
            }
            if(!strcasecmp(cmd,"port_https") || !strcasecmp(cmd,"https_port")){
                int port=strtol(arg,NULL,10);
                ph_assert(port>=0 && port<65536,"Invalid port (https) parsed from config file: %d.",port);
                configs[i]->httpsPort=port;
                LOG0("[photostovis #%d] Configured https port: %d",i,port);
                continue;
            }

            if(!strcasecmp(cmd,"register_ssh_port_int")){
                sshPortInt=strtol(arg,NULL,10);
                if(sshPortInt==0)continue; //means do not register anything
                if(sshPortInt==1)sshPortInt=DEFAULT_SSH_PORT_INT;
                ph_assert(sshPortInt>=0 && sshPortInt<65536,"Invalid internal SSH port parsed from config file: %d.",sshPortInt);
                if(sshPortExt>0){
                    configs[i]->sshPortInt=sshPortInt;
                    configs[i]->sshPortExt=sshPortExt;
                    LOG0("[photostovis #%d] Configured SSH port pair: internal=%d, external=%d",i,sshPortInt,sshPortExt);
                    sshPortInt=sshPortExt=0;
                }
                continue;
            }
            if(!strcasecmp(cmd,"register_ssh_port_ext")){
                sshPortExt=strtol(arg,NULL,10);
                if(sshPortExt==0)continue; //means do not register anything
                if(sshPortExt==1)sshPortExt=DEFAULT_SSH_PORT_EXT;
                ph_assert(sshPortExt>=0 && sshPortExt<65536,"Invalid external SSH port parsed from config file: %d.",sshPortExt);
                if(sshPortInt>0){
                    configs[i]->sshPortInt=sshPortInt;
                    configs[i]->sshPortExt=sshPortExt;
                    LOG0("[photostovis #%d] Configured SSH port pair: internal=%d, external=%d",i,sshPortInt,sshPortExt);
                    sshPortInt=sshPortExt=0;
                }
                continue;
            }

            if(!strcasecmp(cmd,"auth_user") && strlen(arg)>0){
                //we expect something like:
                //auth_user=username:bcrypt-ed password:AABBCCDD (the last 8-bit number is permissions; View/Upload&Edit/Share/Admin)

                //extract the username, password and permissionStr
                char *password=strchr(arg,':');
                ph_assert(password,"Config: auth_user line: cannot find delimiter in %s",arg);
                *password='\0';
                password++;
                char *permissionsStr=strchr(password,':');
                ph_assert(permissionsStr,"Config: auth_user line: cannot find delimiter in %s (after username was extracted)",password);
                *permissionsStr='\0';
                permissionsStr++;

                //check lengths
                int l=strlen(arg);
                ph_assert(l>=3,"Config: auth_user: username too short: <<%s>>, l=%d",arg,l);
                ph_assert(l<30,"Config: auth_user: username too long: <<%s>>, l=%d",arg,l);
                l=strlen(password);
                ph_assert(l==60,"Config: auth_user: pasword (bcrypted) does not have proper length (60): <<%s>>, l=%d",password,l);
                l=strlen(permissionsStr);
                ph_assert(l==8,"Config: auth_user: permission does not have proper length (8): <<%s>>, l=%d",permissionsStr,l);

                //parse the permissionString
                uint8_t permissions;
                r=parsePermissions(permissionsStr,&permissions);
                ph_assert(r==0,"Config: auth_user: permissions string could not be parsed: <<%s>>",permissionsStr);

                //if here, this line is good!
                configs[i]->authUsers=(struct ps_auth*)ph_realloc(configs[i]->authUsers,(configs[i]->nrAuthUsers+1)*sizeof(struct ps_auth));
                configs[i]->authUsers[configs[i]->nrAuthUsers].username=ph_strdup(arg);
                configs[i]->authUsers[configs[i]->nrAuthUsers].password=ph_strdup(password);
                configs[i]->authUsers[configs[i]->nrAuthUsers].permissions=permissions;
                configs[i]->nrAuthUsers++;

                continue;
            }

            if(!strcasecmp(cmd,"auth_user_anonymous") && strlen(arg)>0){
                int l=strlen(arg);
                ph_assert(l==8,"Config: auth_user_anonymous: permission does not have proper length (8): <<%s>>, l=%d",arg,l);

                //parse the permissionString
                uint8_t permissions;
                r=parsePermissions(arg,&permissions);
                ph_assert(r==0,"Config: auth_user_anonymous: permissions string could not be parsed: <<%s>>",arg);
                configs[i]->permissionsAnonymous=permissions;

                thisConfigHasAnonymousPermissions=1;
                continue;
            }

            if(!strcasecmp(cmd,"auth_user_authwithkey") && strlen(arg)>0){
                int l=strlen(arg);
                ph_assert(l==8,"Config: auth_user_authwithkey: permission does not have proper length (8): <<%s>>, l=%d",arg,l);

                //parse the permissionString
                uint8_t permissions;
                r=parsePermissions(arg,&permissions);
                ph_assert(r==0,"Config: auth_user_authwithkey: permissions string could not be parsed: <<%s>>",arg);
                configs[i]->permissionsAuthWithKey=permissions;

                thisConfigHasAuthWithKeyPermissions=1;
                continue;
            }

            if(!strcasecmp(cmd,"auth_lan_access")){
                int authLan=strtol(arg,NULL,10);
                ph_assert(authLan>0 && authLan<3,"ERROR in configuration file: auth_lan_access has invalid value (%d). Should be 1 or 2.",authLan);

                switch(authLan){
                case 1:
                    //Lan authentication not required. This is the default
                    LOG0("[photostovis #%d] LAN: Authentication NOT required.",i);
                    break;
                case 2:
                    //authentication is REQUIRED for LAN access
                    configs[i]->cfgFlags|=CfgFlagLanAuthRequired;
                    LOG0("[photostovis #%d] LAN: Authentication required.",i);
                    break;
                }
                continue;
            }
            if(!strcasecmp(cmd,"auth_lan_auth_over_http")){
                int trueOrFalse=getTrueOrFalse(arg);
                ph_assert(trueOrFalse>=0,"ERROR in configuration file: invalid value for auth_lan_auth_over_http (%s), expected true or false.",arg);
                if(!trueOrFalse){
                    configs[i]->cfgFlags|=CfgFlagLanAuthHttpNotAllowed;
                    LOG0("[photostovis #%d] LAN: Authentication over http NOT ALLOWED.",i);
                } else {
                    LOG0("[photostovis #%d] LAN: Authentication over http is allowed (default).",i);
                }
                continue;
            }

            if(!strcasecmp(cmd,"auth_inet_access")){
                int authInet=strtol(arg,NULL,10);
                ph_assert(authInet>0 && authInet<4,"ERROR in configuration file: auth_inet_access has invalid value (%d). Should be 1,2 or 3.",authInet);

                switch(authInet){
                case 1:
                    //inet authentication not required (pictures are free to see for everybody)
                    configs[i]->cfgFlags|=CfgFlagInetAuthNotRequired;
                    LOG0("[photostovis #%d] Internet: Authentication NOT required.",i);
                    break;
                case 2:
                    //inet authentication required. This is the default
                    LOG0("[photostovis #%d] Internet: Authentication required.",i);
                    break;
                case 3:
                    //inet requests are not allowed at all
                    configs[i]->cfgFlags|=CfgFlagInetNotAllowed;
                    LOG0("[photostovis #%d] Internet: not allowed at all.",i);
                    break;
                }
                continue;
            }
            if(!strcasecmp(cmd,"auth_inet_auth_over_http")){
                int trueOrFalse=getTrueOrFalse(arg);
                ph_assert(trueOrFalse>=0,"ERROR in configuration file: invalid value for auth_inet_auth_over_http (%s), expected true or false.",arg);
                if(trueOrFalse){
                    configs[i]->cfgFlags|=CfgFlagInetAuthHttpAllowed;
                    LOG0("[photostovis #%d] Internet: Authentication over http IS ALLOWED.",i);
                } else {
                    LOG0("[photostovis #%d] Internet: Authentication over http is not allowed (default).",i);
                }
                continue;
            }

            if(!strcasecmp(cmd,"phonet_username") && strlen(arg)>0){
                configs[i]->phonetUsername=ph_strdup(arg);
                LOG0("[photostovis #%d] Configured photostovis.net username: %s",i,configs[i]->phonetUsername);
                continue;
            }
            if(!strcasecmp(cmd,"phonet_password") && strlen(arg)>0){
                configs[i]->phonetPassword=ph_strdup(arg);
                for(int j=0;configs[i]->phonetPassword[j];j++)
                    configs[i]->phonetPassword[j]=toupper(configs[i]->phonetPassword[j]);
                LOG0("[photostovis #%d] Configured photostovis.net SHA256 password: %s",i,configs[i]->phonetPassword);
                continue;
            }

            if(!strcasecmp(cmd,"nr_viewing_permissions")){
                int nrViewingPermissions=strtol(arg,NULL,10);
                ph_assert(nrViewingPermissions>=0 && nrViewingPermissions<=8,
                           "Invalid nrPermissions parsed from settings file: %d.",nrViewingPermissions);
                configs[i]->nrViewingPermissions=nrViewingPermissions;
                LOG0("[photostovis #%d] Configured the number of permissions: %d",i,configs[i]->nrViewingPermissions);
                continue;
            }

            if(!strcasecmp(cmd,"salt") && strlen(arg)>0){
                configs[i]->salt=ph_strdup(arg);
                LOG0("[photostovis #%d] Configured photostovis.net salt.",i);
                continue;
            }

            if(!strcasecmp(cmd,"shutdown_on_cable_out")){
                int trueOrFalse=getTrueOrFalse(arg);
                ph_assert(trueOrFalse>=0,"ERROR in configuration file: invalid value for shotdown_on_cable_out (%s), expected true or false.",arg);
                if(trueOrFalse){
                    configs[i]->cfgFlags|=CfgFlagShutdownOnCableOut;
                    LOG0("[photostovis #%d] Will shutdown when cable out detected.",i);
                } else {
                    LOG0("[photostovis #%d] Will NOT shutdown when cable out detected (default).",i);
                }
                continue;
            }


            //if we are here, we found a command that is not understood
            LOG0("WARNING: Command not understood (%s) in configuration line (%s).",cmd,line);
        }//while(1)

        if(!thisConfigHasAnonymousPermissions)
            configs[i]->permissionsAnonymous=DEFAULT_PERMISSIONS_ANONYMOUS;
        if(!thisConfigHasAuthWithKeyPermissions)
            configs[i]->permissionsAuthWithKey=DEFAULT_PERMISSIONS_AUTHWITHKEY;

        fclose(f);f=NULL;
    } //done with the configuration file

    //if we have no configurations create a default one
    if(!nrConfigs){
        ph_assert(!configs,NULL);
        nrConfigs=1;
        configs=(struct ps_md_config**)ph_malloc(sizeof(struct ps_md_config*));
        configs[0]=(struct ps_md_config*)ph_malloc(sizeof(struct ps_md_config));
        memset(configs[0],0,sizeof(struct ps_md_config));

        configs[0]->permissionsAnonymous=DEFAULT_PERMISSIONS_ANONYMOUS;
        configs[0]->permissionsAuthWithKey=DEFAULT_PERMISSIONS_AUTHWITHKEY;
    }



    //verify configurations and add default values
    for(i=0;i<nrConfigs;i++){
        int skipThis=0;
        const unsigned int KAccessFromInetMask=0b01010101;
        const unsigned int KAccessFromLANMask =0b10101010;
        const unsigned int KViewPermissionLANMask   =0b10000000;
        const unsigned int KOtherPermissionsLANMask =0b00101010;
        const unsigned int KViewPermissionInetMask  =0b01000000;
        const unsigned int KOtherPermissionsInetMask=0b00010101;

        for(j=0;j<i;j++){
            //verification: rootFolder should be different from any previous rootFolder
            LOG0close_break(configs[i]->rootFolder==configs[j]->rootFolder ||
                            (configs[i]->rootFolder && configs[j]->rootFolder && !strcmp(configs[i]->rootFolder,configs[j]->rootFolder)),skipThis=1,
                     "ERROR: configuration %d has the same rootFolder as configuration %d. Skipping configuration %d.",i,j,i);
            //verification: the port should be different from any previous port
            LOG0close_break(configs[i]->httpPort==configs[j]->httpPort,skipThis=1,
                     "ERROR: configuration %d has the same port (%d) as configuration %d. Skipping configuration %d.",i,(int)configs[i]->httpPort,j,i);
            //TODO: verify https ports as well

            //verification: only one server/configuration is allowed to register the ssh ports
            LOG0close_break(configs[i]->sshPortInt>0 && configs[j]->sshPortInt,configs[i]->sshPortInt=configs[i]->sshPortExt=0,
                     "ERROR: previous configuration %d defined a SSH port pair (%d and %d). Skipping SSH in this configuration (%d).",
                            j,configs[j]->sshPortInt,configs[j]->sshPortExt,i);
        }
        if(skipThis){
            //remove this configuration and continue
            free(configs[i]->rootFolder);
            free(configs[i]->authUsers);
            //rest of the pointers should be NULL, still
            ph_assert(!configs[i]->srvDocRoot,NULL);
            ph_assert(!configs[i]->phonetUsername,NULL);
            ph_assert(!configs[i]->phonetPassword,NULL);
            //free this config
            free(configs[i]);
            nrConfigs--;
            memmove(configs+i,configs+i+1,(nrConfigs-i)*sizeof(struct ps_md_config*));
            //no point in reallocating the memory, too expensive
            i--;
            continue;
        }

        //if here, the verification was successful and this configuration is valid
        //set the path to web document root
        if(currentPath){
            ph_assert(daemonize,NULL);
            //change document root
            configs[i]->srvDocRoot=(char*)ph_malloc(currentPathLen+strlen(KDefaultDocRootLocal)+1);
            memcpy(configs[i]->srvDocRoot,currentPath,currentPathLen);
            memcpy(configs[i]->srvDocRoot+currentPathLen,KDefaultDocRootLocal,strlen(KDefaultDocRootLocal)+1);
        } else {
            if(daemonize)
                configs[i]->srvDocRoot=(char*)KDefaultDocRootSystem;
            else
                configs[i]->srvDocRoot=(char*)KDefaultDocRootLocal;
        }

        if(daemonize)configs[i]->cfgFlags|=CfgFlagIsDaemon;
        if(!configs[i]->rootFolder)
            configs[i]->rootFolder=(char*)KOptionsRootFolder;
        configs[i]->cfgNumber=i;
        configs[i]->nrTotalConfigurations=nrConfigs;


        //verify if permissions make sense (any other permission requires viewing permission)
        ph_assert(configs[i]->permissionsAnonymous&KViewPermissionLANMask || !(configs[i]->permissionsAnonymous&KOtherPermissionsLANMask),
                  "Anonymous, LAN: having other permissions but not viewing permissions");
        ph_assert(configs[i]->permissionsAnonymous&KViewPermissionInetMask || !(configs[i]->permissionsAnonymous&KOtherPermissionsInetMask),
                  "Anonymous, Inet: having other permissions but not viewing permissions");

        ph_assert(configs[i]->permissionsAuthWithKey&KViewPermissionLANMask || !(configs[i]->permissionsAuthWithKey&KOtherPermissionsLANMask),
                  "AuthWithKey, LAN: having other permissions but not viewing permissions");
        ph_assert(configs[i]->permissionsAuthWithKey&KViewPermissionInetMask || !(configs[i]->permissionsAuthWithKey&KOtherPermissionsInetMask),
                  "AuthWithKey, Inet: having other permissions but not viewing permissions");

        for(j=0;j<configs[i]->nrAuthUsers;j++){
            ph_assert(configs[i]->authUsers[j].permissions&KViewPermissionLANMask || !(configs[i]->authUsers[j].permissions&KOtherPermissionsLANMask),
                      "User %d/%d, LAN: having other permissions but not viewing permissions",j+1,configs[i]->nrAuthUsers);
            ph_assert(configs[i]->authUsers[j].permissions&KViewPermissionInetMask || !(configs[i]->authUsers[j].permissions&KOtherPermissionsInetMask),
                      "User %d/%d, Inet: having other permissions but not viewing permissions",j+1,configs[i]->nrAuthUsers);
        }

        //verify the configuration flags. Add them if they make sense.
        if(configs[i]->cfgFlags&CfgFlagLanAuthRequired){
            //the anonymous user should have no LAN permissions
            ph_assert(!(configs[i]->permissionsAnonymous&KAccessFromLANMask),"LAN Authentication required, but Anonymous user still has some permissions.");
        } else {
            if(!(configs[i]->permissionsAnonymous&KAccessFromLANMask)){
                configs[i]->cfgFlags|=CfgFlagLanAuthRequired;
                LOG0("CfgFlagLanAuthRequired flag added to this configuration (%d/%d)",i+1,nrConfigs);
            }
        }
        if(configs[i]->cfgFlags&CfgFlagInetAuthNotRequired){
            //the anonymous user should have at least viewing permissions for Inet
            ph_assert(configs[i]->permissionsAnonymous&KViewPermissionInetMask,
                      "Inet Authentication not required, but Anonymous user has no Inet viewing permissions.");
        } else {
            if(configs[i]->permissionsAnonymous&KViewPermissionInetMask){
                configs[i]->cfgFlags|=CfgFlagInetAuthNotRequired;
                LOG0("CfgFlagInetAuthRequired flag added to this configuration (%d/%d)",i+1,nrConfigs);
            }
        }
        if(configs[i]->cfgFlags&CfgFlagInetNotAllowed){
            //this is more complicated. Basically, nobody should have any permissios when coming from Inet
            ph_assert(!(configs[i]->permissionsAnonymous&KAccessFromInetMask),"Inet access forbidden, but Anonymous user still has some permissions.");
            ph_assert(!(configs[i]->permissionsAuthWithKey&KAccessFromInetMask),"Inet access forbidden, but AuthWithKey user still has some permissions.");
            for(j=0;j<configs[i]->nrAuthUsers;j++)
                ph_assert(!(configs[i]->authUsers[j].permissions&KAccessFromInetMask),"Inet access forbidden, but user %d still has some permissions.",j);
        } else {
            //should we actually add this flag to our configuration?
            int shouldAdd=1;
            if(configs[i]->permissionsAnonymous&KAccessFromInetMask || configs[i]->permissionsAuthWithKey&KAccessFromInetMask)
                shouldAdd=0; //because either anonymous or auht with key users can access photos from outside
            if(shouldAdd)
                for(j=0;j<configs[i]->nrAuthUsers;j++)
                    if(configs[i]->authUsers[j].permissions&KAccessFromInetMask){
                        shouldAdd=0; //because this user can access photos from outside (Inet)
                        break;
                    }
            if(shouldAdd){
                configs[i]->cfgFlags|=CfgFlagInetNotAllowed;
                LOG0("CfgFlagInetNotAllowed flag added to this configuration (%d/%d)",i+1,nrConfigs);
            }
        }

        LOG0("photostovis #%d/%d configured to run in %s",i+1,nrConfigs,configs[i]->rootFolder);
    }//for

    ph_assert(nrConfigs>0,NULL);
    ph_assert(configs,NULL);
    ph_assert(!f,NULL);

    if(nrConfigs==1 && cfgVersion<CURRENT_CONFIG_FILE_VERSION){
        LOG0("This server should prompt the user to run the configuration wizzard.");
        configs[0]->cfgFlags|=CfgFlagConfigurationRequired;
    }

    //get the hardware
    myHardware=getHardware();

    /////////////////////////////////////////////////
    /// Done with creating configuration(s), launch them.
    /////////////////////////////////////////////////

    if(!daemonize){
        //no daemon, just run photostovis here.
        ph_assert(!currentPath,NULL);
        ph_assert(!daemonLogFilename,NULL);
        ph_assert(nrConfigs==1,"Nr configurations (%d)!=1, not supported in console mode.",nrConfigs);
        struct ps_md_config *config=configs[0];
        free(configs);configs=NULL;
        err=photostovis(config); //config is freed inside photostovis
        return err;
    }

    //if here, we are in daemon mode
    ph_assert(daemonize,NULL);

    if((myHardware&0xFF000000)==0x91000000){
        err=checkAndChangeScriptBin();
        if(err==1){
            LOG0("Exiting due to iminent reboot.");
            return 0;
        }

        if(err<0)LOG0("Upgrading /boot/script.bin failed or unrecognized script.bin file (err=%d)",err);
        else LOG0("/boot/script.bin ok (supports all 4 leds).");
    }

    //the system time should be in 2015, but not 2016
    //TODO: make a proper solution out of this
    {
        time_t t=time(NULL);
        if(t<KMinTime || t>KMaxTime){
            const int KNrTimes=130;
            const int KNrTimesOk=10;
            int nrTimesOk=0;
            struct tm tmTime;
            char timeStr[100];
            localtime_r(&t,&tmTime);
            strftime(timeStr,100,"%F %H:%M:%S",&tmTime);

            LOG0("System time (%s) does not look right. Waiting for the system to update its time.",timeStr);
            for(i=0;i<KNrTimes;i++){
                sleep(1);
                t=time(NULL);
                if(t>KMinTime && t<KMaxTime)nrTimesOk++;
                else nrTimesOk=0;

                if(nrTimesOk>=KNrTimesOk){
                    localtime_r(&t,&tmTime);
                    strftime(timeStr,100,"%F %H:%M:%S",&tmTime);
                    LOG0("System time updated to %s.",timeStr);
                    //we have proper time now. Reopen our log file
                    char *newLogFilename=startDaemonLogging(currentPath,currentPathLen);
                    if(newLogFilename){
                        readjustLogStartTime();
                        //copy the stuff from the old file into the new file
                        FILE *fOld=fopen(daemonLogFilename,"r");
                        if(fOld){
                            //get the size
                            fseek(fOld,0,SEEK_END);
                            long size=ftell(fOld);
                            fseek(fOld,0,SEEK_SET);
                            if(size>0 && size<=1048576){
                                char *b=ph_malloc(size);
                                fread(b,size,1,fOld);
                                fwrite(b,size,1,stderr);
                                free(b);
                            }
                            fclose(fOld);
                        } else
                            LOG0("Failed to open OLD logfile (%s) to copy it into the new logfile (this one).",daemonLogFilename);

                        err=remove(daemonLogFilename);
                        LOG0c(err,"Removing %s failed: %s",daemonLogFilename,strerror(errno));

                        free(daemonLogFilename);
                        daemonLogFilename=newLogFilename;
                        LOG0("System time updated, log reopened to %s. Continuing.",daemonLogFilename);
                    } else
                        LOG0("Could not reopen logfile!");
                    //done
                    break;
                }
                LOG0("Waiting ... (%d/%d)",i+1,KNrTimes);
            }
        }
    }


    if(!currentPath){
        CreateIndexAllHtmlGz();
        //temporary fix:
        int l=strlen(KOptionsRootFolder);
        char path[PATH_MAX];
        memcpy(path,KOptionsRootFolder,l);
        memcpy(path+l,KPicturesFolder,strlen(KPicturesFolder)+1);
        setOwnership(path,1);
        //LOG0("After changing ownership.");
    }

    //change current directory to root
    err=chdir("/");
    LOG0goto(err,reboot,"%s: ERROR: could not chdir to \"/\" (%s), rebooting.",KPhotostovisDaemonName,strerror(errno));

    /*
    //close stdin, stdout
    LOG0("Closing stdin...");
    if(stdin){
        fclose(stdin);
        stdin=NULL;
    }

    if(stdout && stderr){
        LOG0("Closing stdout and stdout=stderr");
        fclose(stdout);
        stdout=stderr;
        LOG0("Done.");
    } else LOG0("stdout will NOT be closed because either stdout(%p) or stderr(%p) is NULL",stdout,stderr);
    */

    if(daemonLogFilename){
        free(daemonLogFilename);
        daemonLogFilename=NULL;
    }
    //from now on, daemon logging should be right
    fprintf(stdout,"Test message, printing to stdout (PID=%d).\n",(int)getpid());
    fflush(stdout);
    fprintf(stderr,"Test message, printing to stderr (PID=%d).\n",(int)getpid());
    fflush(stderr);

    if(currentPath){
        free(currentPath);
        currentPath=NULL;
        daemonize=2; //this means local daemon
        currentPathLen=currentPathSize=0;
    }

    //change name to photostovisd
#ifndef __APPLE__
    err=prctl(PR_SET_NAME,KPhotostovisDaemonName);
    LOG0c(err,"%s: could not set process name to \"%s\"",KPhotostovisDaemonName,KPhotostovisDaemonName);
#endif

    //handle SIGALRM
    //struct sigaction sigAlrmHandler;
    sigHandler.sa_handler=sigAlrmFunction;
    sigemptyset(&sigHandler.sa_mask);
    sigHandler.sa_flags=0;
    sigaction(SIGALRM,&sigHandler,NULL);

    //launch photostovis & the log server
    ph_assert(nrConfigs<10,"Are you insane? What are you doing with >=10 servers on the same machine??");
    for(i=0;i<nrConfigs;i++){
        err=launchPhotostovisAndLogPair(configs,nrConfigs,i);
        LOG0goto(err,reboot,"Launching a photostovis server failed, rebooting.");
    }

    //if we are here, we are in the daemon. All Children do exit in launchPhotostovisAndLogPair() function
    int upgradeAvailable=0,firstRun=1;

    while(1){
        while(1){
#ifndef CONFIG_NO_PHONET
            //set the alarm
            if(daemonize==1 && !upgradeAvailable)
                setUpgradeCheckAlarm(&firstRun);
#endif
            pid=wait(&status);
            if(pid>0)break;
            ph_assert(pid==-1,NULL);
            //handle the error
#ifndef CONFIG_NO_PHONET
            if(errno==EINTR && daemonize==1){
                err=checkForUpgrade();
                if(err){
                    LOG0c(err!=-2,"checkForUpgrade returned an error: %d",err);
                } else {
                    LOG0("checkForUpgrade: upgrade available and downloaded");
                    upgradeAvailable=1;
                    //sending SIGUSR1 to all photostovis servers
                    for(i=0;i<nrConfigs;i++){
                        err=kill(configs[i]->pidPhotostovis,SIGUSR1);
                        LOG0c(err,"%s: ERROR sending SIGUSR1 to photostovis server (PID=%u): %s\n",KPhotostovisName,configs[i]->pidPhotostovis,strerror(errno));
                    }
                    //wait them all
                    for(i=0;i<nrConfigs;i++){
                        err=waitpid(configs[i]->pidPhotostovis,NULL,0);
                        if(err==configs[i]->pidPhotostovis)LOG0("%s: photostovis process with PID=%d exited for upgrade.",KPhotostovisDaemonName,err);
                        else LOG0("%s: waitpid(%d ...) returned an error: %d",KPhotostovisDaemonName,configs[i]->pidPhotostovis,err);
                    }
                    break;
                }
            }
#endif
            ph_assert(errno==EINTR,"waitpid returned %d (%s)",err,strerror(errno));
        }//while(1)

        if(nrConfigs==1){
            ledCommand(LCOff,1);
            ledClientActivity(LCOff,1);
        } //else: TODO: we need to manage the case when there are more than 1 config

#ifndef CONFIG_NO_PHONET
        if(upgradeAvailable){
            ph_assert(pid<0,NULL);

            //do we have an extra package?
            char *cmd=getExtraUpgradePackageCommand();
            if(cmd){
                //yes, we also have an extra package
                LOG0("Upgrading the extra package (%s)",cmd);
                err=upgradePackage(cmd);
                free(cmd);
                if(err)LOG0("Upgrading the extra package FAILED!");
                else LOG0("Upgrading the extra package succeeded!");
            }

            //get the upgrade package filename
            cmd=getUpgradePackageCommand();
            LOG0goto(!cmd,reboot,"Getting the upgrade command failed. Rebooting.");

            err=upgradePackage(cmd);
            free(cmd);

            if(err){
                LOG0("The upgrade script returned an error (%d). Relaunch all the servers and continue to run.",err);
                upgradeAvailable=0;
                //launch all children
                for(i=0;i<nrConfigs;i++){
                    err=launchPhotostovisAndLogPair(configs,nrConfigs,i);
                    LOG0goto(err,reboot,"Launching a photostovis server failed, rebooting.");
                }
                continue;
            } else {
                LOG0("Upgrading successfull.");
                break; //from the main while(1) loop
            }
        }//if(upgradeAvailable)
#endif

        //identify the server configuration having our PID
        ph_assert(pid>0,NULL);
        for(i=0;i<nrConfigs;i++)
            if(pid==configs[i]->pidPhotostovis)break;
        ph_assert(i<nrConfigs,"ERROR: we were unable to identify the photostovis process (PID=%d) that wait returned.",pid);
        configs[i]->pidPhotostovis=0;

        int restartPhotostovis=1;
        if(WIFEXITED(status)){
            //photostovis exited with a status. Do something, depending on that status
            switch(WEXITSTATUS(status)){
            case 0:
                LOG0("%s: %s exited normally with ZERO status.",KPhotostovisDaemonName,KPhotostovisName);
                restartPhotostovis=0;
                break;
            case EXIT_DO_NOT_RESTART:
                LOG0("%s: %s exited with 'DO NOT RESTART ME' status.",KPhotostovisDaemonName,KPhotostovisName);
                restartPhotostovis=0;
                break;
            case EXIT_RESTART:
                LOG0("%s: %s exited with 'RESTART ME' status.",KPhotostovisDaemonName,KPhotostovisName);
                break;
            case EXIT_FOR_UPGRADE:
                LOG0("%s: %s exited with 'CAN UPGRADE' status, but there is no upgrade available. Restarting.",KPhotostovisDaemonName,KPhotostovisName);
                break;
            case EXIT_SHUTDOWN:
                LOG0("%s: %s exited with 'SHUTDOWN' status. Shutting down the system.",
                     KPhotostovisDaemonName,KPhotostovisName);
                LOG0("==========================================================");
                err=system("/sbin/poweroff");
                LOG0("==========================================================");
                restartPhotostovis=0;
                break;
            default:
                LOG0("Unknown exit status. Will restart Photostovis.");
            }//switch
        } else if(WIFSIGNALED(status)){
            if(WTERMSIG(status)==SIGKILL){
                LOG0("%s: %s was terminated by SIGKILL.",KPhotostovisDaemonName,KPhotostovisName);
                restartPhotostovis=0;
            } else {
                LOG0("%s: %s was terminated by the %s signal (%d).",
                     KPhotostovisDaemonName,KPhotostovisName,strsignal(WTERMSIG(status)),WTERMSIG(status));
            }
        };

        //check if we restart or not Photostovis
        if(restartPhotostovis){
            LOG0("Photostovis (ex-PID=%d) will be restarted.",pid);
            sleep(1);
            //restart
            err=launchPhotostovisAndLogPair(configs,nrConfigs,i);
            LOG0goto(err,reboot,"Launching a photostovis server failed, rebooting.");
            continue;
        } else
            LOG0("Photostovis (ex-PID=%d) will NOT be restarted",pid);

        //if here, there might be some processes left, or not. Lets check
        for(i=0;i<nrConfigs;i++)
            if(configs[i]->pidPhotostovis!=0)
                break;

        if(i>=nrConfigs)
            break; //it means there are no more photostovis processes to wait, so we exit the while(1) loop
    }//while(1)


    //daemon exiting
    LOG0("Photostovis daemon exiting.");
    return 0;

reboot:
    LOG0("SOME NASTY ERROR HAPPENED, REBOOTING (after sleeping for 1 minute).");
    sleep(60);
    err=system("/sbin/reboot");
    return 0;
}

