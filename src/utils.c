#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <time.h>
#include <sys/time.h>
#include <limits.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#ifndef __APPLE__
#include <malloc.h>
#endif

#include "photostovis.h"
#include "log.h"

extern const char *KPhotostovisName;

static struct timeval _ph_log_start={0,0};
static struct timeval _ph_last_log_entry={0,0};
static int logFd=-1;
static int nrLogErrors=0;
static const int KMaxLogErrorsDisplayed=100;

const char *KEol="\n";

void resetLog(int s){
    logFd=s;

    _ph_log_start.tv_sec=0;
    _ph_log_start.tv_usec=0;
    _ph_last_log_entry.tv_sec=0;
    _ph_last_log_entry.tv_usec=0;
}

void readjustLogStartTime(){
    gettimeofday(&_ph_log_start,NULL);
}


void ph_assertion_failed(const char *filename, const int line, const char *fmt, ...)
{
    char logtext[MAX_LOG_MESSAGE_SIZE];
    snprintf(logtext,MAX_LOG_MESSAGE_SIZE,"Assertion failed in %s, line %d: ",filename,line);
    int len=strlen(logtext);

    if(fmt){
        va_list args;
        va_start(args,fmt);
        vsnprintf(logtext+len,MAX_LOG_MESSAGE_SIZE-len,fmt,args);
        va_end(args);
    }
    else strcat(logtext,"(no description)");

    printlog("%s",logtext);
    flushlog();
    abort(); //calls closelog();
}

void flushlog(){
    if(logFd<=0)return;

    uint32_t l=LOG_CMD_FLUSH;
    /*ssize_t err=*/write(logFd,&l,sizeof(uint32_t));
    //TODO: do something with err
}

void outputLogError(){
    nrLogErrors++;
    if(nrLogErrors<KMaxLogErrorsDisplayed)
        fprintf(stderr,"%s: ERROR sending log length to logserver: %s\n",KPhotostovisName,strerror(errno));
    else if(nrLogErrors==KMaxLogErrorsDisplayed)
        fprintf(stderr,"%s: TOO MANY LOG ERRORS, WILL STOP DISPLAYING THEM.\n",KPhotostovisName);
}

unsigned int printlog(const char *fmt, ...)
{
    struct timeval tv;
    char format[MAX_LOG_MESSAGE_SIZE];
    int ms,ss,mm,hh,len;

    if(!_ph_log_start.tv_sec)
        gettimeofday(&_ph_log_start,NULL);//initialize _ph_log_start

    if(!fmt){
        if(logFd>=0){
            int err;
            char buf[6];
            *(uint32_t*)buf=1;
            buf[4]='\n';
            buf[5]='\0';
            err=write(logFd,buf,5);
            if(err<0 && stderr)outputLogError();
        } else {
            fputs(KEol,stderr);
            fflush(stderr);
        }

        return 0;
    }
    //the next part can be done safely outside the critical region

    gettimeofday(&tv,NULL);
    if(tv.tv_sec<_ph_log_start.tv_sec){
        //TODO: if here, it means we have a time glitch! This would need to be solved
        _ph_log_start=tv;
    }
    tv.tv_sec-=_ph_log_start.tv_sec;
    if(tv.tv_usec<_ph_log_start.tv_usec){
        tv.tv_usec=tv.tv_usec+1000000-_ph_log_start.tv_usec;
        tv.tv_sec--;
    } else
        tv.tv_usec-=_ph_log_start.tv_usec;
    ms=(int)(tv.tv_usec/1000);
    ss=(int)tv.tv_sec%60;
    mm=((int)tv.tv_sec/60)%60;
    hh=(int)tv.tv_sec/3600;
    len=snprintf(format,MAX_LOG_MESSAGE_SIZE,"[%02d:%02d:%02d:%03d %08X] %s\n",hh,mm,ss,ms,(unsigned)(intptr_t)pthread_self(),fmt);
    if(len>=MAX_LOG_MESSAGE_SIZE)
        return printlog("ERROR: LOG message (format) was too long, skipped.");

    char logtext[4+MAX_LOG_MESSAGE_SIZE];
    va_list args;
    va_start(args,fmt);
    //vfprintf(f,format,args);
    len=vsnprintf(logtext+4,MAX_LOG_MESSAGE_SIZE,format,args);
    va_end(args);
    if(len>=MAX_LOG_MESSAGE_SIZE)
        return printlog("ERROR: LOG message was too long (%d bytes), skipped.",len);

    if(logFd>=0){
        int err;
        uint32_t l=strlen(logtext+4);
        ph_assert(l<MAX_LOG_MESSAGE_SIZE,"ERROR: log message too long (%u bytes).",l);
        *(uint32_t*)logtext=l;
        err=write(logFd,logtext,l+4);
        if(err<0 && stderr)outputLogError();
    } else {
        fputs(logtext+4,stderr);
        fflush(stderr);
    }

    _ph_last_log_entry=tv;
    return tv.tv_sec*1000+ms;
}



unsigned int printlogbin(const char *ptr, int ptrLen, int printTxt){
    struct timeval tv;
    char format[MAX_LOG_MESSAGE_SIZE];
    unsigned int ms;

    if(!_ph_log_start.tv_sec)
        gettimeofday(&_ph_log_start,NULL);//initialize _ph_log_start

    gettimeofday(&tv,NULL);
    tv.tv_sec-=_ph_log_start.tv_sec;
    tv.tv_usec-=_ph_log_start.tv_usec;
    if(tv.tv_usec<0){tv.tv_usec+=1000000;tv.tv_sec--;}
    ms=(unsigned int)(tv.tv_usec/1000);
    snprintf(format,MAX_LOG_MESSAGE_SIZE,"[%06u:%03u] ",(unsigned int)tv.tv_sec,ms);

    char *txt=ph_malloc((ptrLen<<1)+3);
    char *txt2=NULL;
    if(printTxt)txt2=ph_malloc((ptrLen<<1)+3);
    int i,r,txt2Len=0,txtLen=0,formatLen=strlen(format);
    for(i=0;i<ptrLen;i++){
        r=snprintf(txt+txtLen,3,"%02x",(unsigned char)ptr[i]);
        ph_assert(r==2,"%d",r);
        txtLen+=2;
        if(printTxt){
            if(isprint(ptr[i])){
                r=snprintf(txt2+txt2Len,2,"%c",ptr[i]);
                ph_assert(r==1,"%d",r);
                txt2Len+=1;
            } else {
                r=snprintf(txt2+txt2Len,3,"%02x",(unsigned char)ptr[i]);
                ph_assert(r==2,"%d",r);
                txt2Len+=2;
            }
        }
    }
    ph_assert(strlen(txt)==txtLen,NULL);
    txt[txtLen++]='\n';
    txt[txtLen]='\0';
    if(printTxt){
        ph_assert(strlen(txt2)==txt2Len,NULL);
        txt2[txt2Len++]='\n';
        txt2[txt2Len]='\0';
    }

    if(logFd>=0){
        if(formatLen+txtLen+formatLen+txt2Len<MAX_LOG_MESSAGE_SIZE){
            char buf[4+MAX_LOG_MESSAGE_SIZE];
            uint32_t bufLen=0;
            memcpy(buf+4,format,formatLen);
            bufLen+=formatLen;
            memcpy(buf+4+bufLen,txt,txtLen);
            bufLen+=txtLen;
            if(printTxt){
                memcpy(buf+4+bufLen,format,formatLen);
                bufLen+=formatLen;
                memcpy(buf+4+bufLen,txt2,txt2Len);
                bufLen+=txt2Len;
            }

            int err;
            ph_assert(bufLen<MAX_LOG_MESSAGE_SIZE,"ERROR: log message too long (%u bytes).",bufLen);

            *(uint32_t*)buf=bufLen;
            err=write(logFd,buf,bufLen+4);
            if(err<0 && stderr)outputLogError();
        }
        // else the text is too long!
    } else {
        fwrite(format,formatLen,1,stderr);
        fwrite(txt,txtLen,1,stderr);
        if(printTxt){
            fwrite(format,formatLen,1,stderr);
            fwrite(txt2,txt2Len,1,stderr);
        }
        fflush(stderr);
    }

    free(txt);
    if(txt2)free(txt2);
    _ph_last_log_entry=tv;
    return tv.tv_sec*1000+ms;
}

unsigned int getRelativeTimeMs()
{
    struct timeval tv;

    if(!_ph_log_start.tv_sec)
        gettimeofday(&_ph_log_start,NULL);//initialize _ph_log_start

    gettimeofday(&tv,NULL);
    tv.tv_sec-=_ph_log_start.tv_sec;
    tv.tv_usec-=_ph_log_start.tv_usec;
    if(tv.tv_usec<0){tv.tv_usec+=1000000;tv.tv_sec--;}
    return tv.tv_sec*1000+(unsigned int)(tv.tv_usec/1000);
}



/*
unsigned int printlogbuf(const char *buf)
{
    struct timeval tv;
    char format[30];
    int ms,ss,mm,hh;

    if(!_ph_log_start.tv_sec)
        gettimeofday(&_ph_log_start,NULL);//initialize _ph_log_start

    ph_assert(buf && strlen(buf),NULL);

    //the next part can be done safely outside the critical region
    gettimeofday(&tv,NULL);
    tv.tv_sec-=_ph_log_start.tv_sec;
    if(tv.tv_usec<_ph_log_start.tv_usec){
        tv.tv_usec=tv.tv_usec+1000000-_ph_log_start.tv_usec;
        tv.tv_sec--;
    } else
        tv.tv_usec-=_ph_log_start.tv_usec;
    ms=(int)(tv.tv_usec/1000);
    ss=(int)tv.tv_sec%60;
    mm=((int)tv.tv_sec/60)%60;
    hh=(int)tv.tv_sec/3600;
    snprintf(format,30,"[%02d:%02d:%02d:%03d] ",hh,mm,ss,ms);

    if(logFd>=0){
        int err,bufLen=strlen(buf);
        uint32_t l=strlen(format)+bufLen+1;
        pthread_mutex_lock(&logMutex);
        err=write(logFd,&l,sizeof(uint32_t));
        if(err<0 && stderr)outputLogError();
        err=write(logFd,format,strlen(format));
        if(err<0 && stderr)outputLogError();
        err=write(logFd,buf,bufLen);
        if(err<0 && stderr)outputLogError();
        err=write(logFd,"\n",1);
        pthread_mutex_unlock(&logMutex);
        if(err<0 && stderr)outputLogError();
    } else {
        fputs(format,stderr);
        fputs(buf,stderr);
        fputs("\n",stderr);
        fflush(stderr);
    }

    _ph_last_log_entry=tv;
    return tv.tv_sec*1000+ms;
}
*/

void ph_malloc_info(){
#ifndef __APPLE__
    /* I do not understand the output of malloc_stats
    size_t bufSize=0;
    char *buf=NULL;
    FILE *f=open_memstream(&buf,&bufSize);
    fprintf(f,"Memory statistics:\n");
    malloc_info(0,f);
    fprintf(f,"\n\n");
    fclose(f);
    printlogbuf(buf);
    malloc_stats();
    free(buf);*/

    if(logFd<0)
        malloc_stats();
#endif
}

void closelog(int sendToServer){
    if(logFd<=0)return;

    uint32_t l;
    if(sendToServer)l=LOG_CMD_EXIT_AND_SEND;
    else l=LOG_CMD_EXIT;
    /*ssize_t err=*/write(logFd,&l,sizeof(uint32_t));
    //TODO: do something with err
    close(logFd);
    logFd=-1;
}

///////////////////////////
// Semaphore implementation using mutexes and conditional variables

#ifdef __APPLE__
int ph_sem_init(ph_sem_t *s, const int pshared, const int v){
    ph_assert(!pshared,NULL);
    pthread_mutex_init(&s->m,NULL);
    pthread_cond_init(&s->c,NULL);
    s->v=v;
    return 0;
}

int ph_sem_destroy(ph_sem_t *s){
    pthread_mutex_destroy(&s->m);
    pthread_cond_destroy(&s->c);
    s->v=0;
    return 0;
}

int ph_sem_wait(ph_sem_t *s){
    pthread_mutex_lock(&s->m);
    while(s->v==0)
        pthread_cond_wait(&s->c,&s->m);
    s->v--;
    pthread_mutex_unlock(&s->m);
    return 0;
}

int ph_sem_post(ph_sem_t *s){
    pthread_mutex_lock(&s->m);
    s->v++;
    pthread_cond_broadcast(&s->c);
    pthread_mutex_unlock(&s->m);
    return 0;
}

int ph_sem_getvalue(ph_sem_t *s, int *v){
    pthread_mutex_lock(&s->m);
    *v=s->v;
    pthread_mutex_unlock(&s->m);
    return 0;
}
#endif

void printIP(const uint32_t ip, char *ipStr){
    sprintf(ipStr,"%u.%u.%u.%u",
            (unsigned int)((ip>>24)&0xFF),
            (unsigned int)((ip>>16)&0xFF),
            (unsigned int)((ip>>8)&0xFF),
            (unsigned int)(ip&0xFF));
}

/* This is the basic CRC-32 calculation with some optimization but no
table lookup. The the byte reversal is avoided by shifting the crc reg
right instead of left and by using a reversed 32-bit word to represent
the polynomial.
   When compiled to Cyclops with GCC, this function executes in 8 + 72n
instructions, where n is the number of bytes in the input message. It
should be doable in 4 + 61n instructions.
   If the inner loop is strung out (approx. 5*8 = 40 instructions),
it would take about 6 + 46n instructions. */

uint32_t ph_crc32b(char const * const message) {
   int i, j;
   uint32_t byte, crc, mask;

   i = 0;
   crc = 0xFFFFFFFF;
   while (message[i] != 0) {
      byte = message[i];            // Get next byte.
      crc = crc ^ byte;
      for (j = 7; j >= 0; j--) {    // Do eight times.
         mask = -(crc & 1);
         crc = (crc >> 1) ^ (0xEDB88320 & mask);
      }
      i = i + 1;
   }
   return ~crc;
}



static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static int mod_table[] = {0, 2, 1};


char *base64_encode(const unsigned char *data,
                    size_t input_length,
                    size_t *output_length) {
    int i,j;

    *output_length = 4 * ((input_length + 2) / 3);

    char *encoded_data = malloc(*output_length+1);
    if (encoded_data == NULL) return NULL;

    for (i = 0, j = 0; i < input_length;) {

        uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';
    encoded_data[*output_length]='\0';
    return encoded_data;
}

/* These are commented out because they are not used
static char *decoding_table = NULL;
static void build_decoding_table() {
    int i;
    decoding_table = malloc(256);

    for (i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}

unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length) {

    int i,j;
    if (decoding_table == NULL) build_decoding_table();

    if (input_length % 4 != 0) return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;

    for (i = 0, j = 0; i < input_length;) {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);

        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}

void base64_cleanup() {
    free(decoding_table);
    decoding_table=NULL;
}

*/


char * escape_str(char const * const src){
    int srcLen=strlen(src);
    int dstLen=0;
    char const *src1=src;

    while(*src1){
        switch(*src1){
        case '\"':dstLen+=2;break;
        default:dstLen++;
        }
        src1++;
    }
    if(srcLen==dstLen)
        return (char *)src;

    //if here we need to alloc the string
    LOG0("escape_str: we need to escape this string (%s)",src);
    char *dst=ph_malloc(dstLen+1);
    char *dst1=dst;
    src1=src;

    while(*src1){
        switch(*src1){
        //case '\n' : strcat(dest++, "\\n"); break;
        case '\"' :
          *dst1='\\';
          dst1++;
          *dst1='\"';
          break;
        default:  *dst1 = *src1;
        }
        src1++;
        dst1++;
    }

    *dst1=0;
    LOG0("escape_str: escaped string: %s",dst);
    ph_assert(strlen(dst)==dstLen,NULL);
    return dst;
}

uint32_t ipToUInt32(char const * const ip){
    uint8_t ip1,ip2,ip3,ip4;
    uint32_t ipNum=0;
    int r=sscanf(ip,"%" SCNu8 ".%" SCNu8 ".%" SCNu8 ".%" SCNu8, &ip1,&ip2,&ip3,&ip4);
    if(r==4)
        ipNum=(ip1<<24)+(ip2<<16)+(ip3<<8)+ip4;
    else LOG0("WARNING: Scanning IP (%s) failed (r=%d)!",ip,r);
    return ipNum;
}

