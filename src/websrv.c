#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */
#include <errno.h>
//#define __USE_GNU //for memmem from string.h
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <sys/stat.h>
#include <poll.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/md5.h>
#include <openssl/rand.h>

#include <curl/curl.h>
#ifdef __APPLE__
#include <sys/time.h>
#include <ctype.h>
//#define MSG_NOSIGNAL 0 -> now defined
#else
#include <semaphore.h>
#endif

#include "photostovis.h"
#include "log.h"


#define DEFAULT_LISTENING_QUEUE_LEN 16
#define DEFAULT_CLIENT_SOCKET_RECV_TIMEOUT 5 //seconds
#define DEFAULT_CLIENT_SOCKET_SEND_TIMEOUT 30 //seconds

#define DEFAULT_WEBSOCKET_RECV_TIMEOUT 300 //seconds (300=5 minutes). default send timeout remains the same (DEFAULT_CLIENT_SOCKET_SEND_TIMEOUT)

#define DOC_ROOT_FILES_EXTRA_LEN 127
//TODO: the above 127 is temporary, for working with dropzone. Real value is 31

#define WEBSOCKET_UPGRADE 0x0FFFFFFF

const int KWebsocketBufferOffset=10;

struct ongoingPOST {
    uint64_t contentLen; //total length of the content
    uint64_t parsedLen; //how much was already parsed (removed from the buffer)
    char buffer[DEFAULT_HTTP_BUFFER_SIZE+DEFAULT_HTTP_BUFFER_SIZE];
    int boundaryLen;
    int connectionType;
    int pos; //position in the current buffer.

    union {
        struct {
            //command part (boundaryLen is zero)
            char command;
        };
        struct {
            //file upload part (boundaryLen is NOT zero)
            FILE *f;
            char *boundary;
            char *nameAfterChange;

            uint32_t uploadStartTime;
            uint32_t lastBitrateComputeTime;
            uint64_t lastBitrateComputeData;
        };
    };
};

enum SrvThreadFlags {
    FlagSrvThread_GetSrvMetadataExpectedToAuthorize=1,
    FlagSrvThread_ComingFromLAN=2,
    FlagSrvThread_ComingFromPhotostovisDotNet=4
};

struct srvThreadData {
    SSL *ssl;
    struct ongoingPOST *op;
    char ipString[16];
    uint64_t mySessionId;

    int c;
    unsigned int processingTimeStart;
    uint32_t reqClientIp;
    uint8_t userIndex;
    uint8_t flags;
};

static const char *KPhotostovisDotNet="photostovis.net";
static const char *KCookieName="photostovisSession";

//static const char *KColon=":";
static const char *KHTTP400="HTTP/1.1 400 Bad Request\r\n";
static const char *KHTTP403="HTTP/1.1 403 Forbidden\r\n";
static const char *KHTTP404="HTTP/1.1 404 Not Found\r\n";
static const char *KHTTP500="HTTP/1.1 500 Internal Server Error\r\n";
static const char *KHTTP501="HTTP/1.1 501 Not Implemented\r\n";
static const char *KHTTP503="HTTP/1.1 503 Service Unavailable\r\n";
static const char *KHTTP505="HTTP/1.1 505 HTTP Version Not Supported\r\n";

static const char *KConnectionKeepAlive="Keep-Alive";
static const char *KConnectionClose="Close";

static const char *KHTTPErrRspConnClose=
        "Content-Length: 0\r\n"
        "Server: " KPhotostovisServerString "\r\n"
        "Connection: close\r\n"
        "\r\n";
static int KHTTPErrRspConnCloseLen;
static const int KErrorSendBufferLength=256;

/* This is not used because we do not know the connection type
static const char *KHTTPErrRsp=
        "Connection: %s\r\n"
        "Content-Length: 0\r\n"
        "Server: " KPhotostovisServerString "\r\n"
        "\r\n";
static int KHTTPErrRspLen;
*/

static const char *KHTTPOk=
        "HTTP/1.1 200 OK\r\n"
        "Connection: %s\r\n"
        "Content-Type: %s\r\n"
        "Content-Length: %"UINT64_FMT"\r\n"
        "Server: " KPhotostovisServerString "\r\n"
        "Cache-Control: private\r\n"
        "ETag: \"%s\"\r\n"
        "Date: %s\r\n"
        "\r\n";
static const char *KHTTPOkGz=
        "HTTP/1.1 200 OK\r\n"
        "Connection: %s\r\n"
        "Content-Type: %s\r\n"
        "Content-Length: %"UINT64_FMT"\r\n"
        "Content-Encoding: gzip\r\n"
        "Server: " KPhotostovisServerString "\r\n"
        "Cache-Control: private\r\n"
        "ETag: \"%s\"\r\n"
        "Date: %s\r\n"
        "\r\n";
static const char *KHTTPOkNoETag=
        "HTTP/1.1 200 OK\r\n"
        "Connection: %s\r\n"
        "Content-Type: %s\r\n"
        "Content-Length: %"UINT64_FMT"\r\n"
        "Server: " KPhotostovisServerString "\r\n"
        "Cache-Control: private\r\n"
        "Date: %s\r\n"
        "\r\n";
static const char *KHTTPOkSetCookie=
        "HTTP/1.1 200 OK\r\n"
        "Connection: %s\r\n"
        "Content-Type: %s\r\n"
        "Content-Length: %"UINT64_FMT"\r\n"
        "Server: " KPhotostovisServerString "\r\n"
        "Cache-Control: private\r\n"
        "Date: %s\r\n"
        "Set-Cookie: %s=%016"UINT64X_FMT"; Path=/\r\n"
        "\r\n";
static const char *KHTTPWebsocket=
        "HTTP/1.1 101 Switching Protocols\r\n"
        "Upgrade: websocket\r\n"
        "Connection: Upgrade\r\n"
        "Server: " KPhotostovisServerString "\r\n"
        "Sec-WebSocket-Accept: %s\r\n"
        "Sec-WebSocket-Protocol: status.photostovis.net\r\n"
        "\r\n";

//////////// VIDEO answers++
static const char *KHTTPVideoOk=
        "HTTP/1.1 200 OK\r\n"
        "Connection: %s\r\n"
        "Content-Type: %s\r\n"
        "Content-Length: %"UINT64_FMT"\r\n"
        "Server: " KPhotostovisServerString "\r\n"
        "Cache-Control: private\r\n"
        "ETag: \"%s\"\r\n"
        "Accept-Ranges: bytes\r\n"
        "Date: %s\r\n"
        "\r\n";
static const char *KHTTPVideo206=
        "HTTP/1.1 206 Partial Content\r\n"
        "Connection: %s\r\n"
        "Content-Type: %s\r\n"
        "Content-Length: %"UINT64_FMT"\r\n"
        "Server: " KPhotostovisServerString "\r\n"
        "Cache-Control: private\r\n"
        "Accept-Ranges: bytes\r\n"
        "Date: %s\r\n"
        "Content-Range: bytes %"UINT64_FMT"-%"UINT64_FMT"/%"UINT64_FMT"\r\n"
        "\r\n";
//////////// VIDEO answers--

static const char *KHTTP304=
        "HTTP/1.1 304 Not Modified\r\n"
        "Connection: %s\r\n"
        "Server: " KPhotostovisServerString "\r\n"
        "Date: %s\r\n"
        "\r\n";
static const char *KHTTP307=
        "HTTP/1.1 307 Temporary Redirect\r\n"
        "Location: %s\r\n"
        "Connection: %s\r\n"
        "Server: " KPhotostovisServerString "\r\n"
        "Date: %s\r\n"
        "\r\n";
static const char *KHTTP403WithReason=
        "HTTP/1.1 403 Forbidden\r\n"
        "Server: " KPhotostovisServerString "\r\n"
        "Content-Type: application/json\r\n"
        "Content-Length: 14\r\n"
        "Connection: close\r\n"
        "Date: %s\r\n"
        "\r\n"
        "{\"reason\":%3d}";
static const char *KHTTPOkWithForbiddenAndReason=
        "HTTP/1.1 200 OK\r\n"
        "Server: " KPhotostovisServerString "\r\n"
        "Content-Type: application/json\r\n"
        "Content-Length: %%d\r\n"
        "Connection: close\r\n"
        "Date: %s\r\n"
        "\r\n"
        "{\"forbidden\":true,\"reason\":%d,\"port\":%u,\"fromLAN\":%u,\"un\":\"%s\"}";

enum HttpCommands {
    HttpGet=1,
    HttpPost=2,
    HttpOptions=3,
    HttpHead=4
};

enum WebsocketStages {
    WebsocketConnectionUpgrade=1,
    WebsocketUpgradeWebsocket=2,
    WebsocketKey=4,
    WebsocketProtocol=8,
    WebsocketVersion=16
};

static const char *KHTTPTypeHtml="text/html";
static const char *KHTTPTypeCss="text/css";
static const char *KHTTPTypePng="image/png";
static const char *KHTTPTypeGif="image/gif";
//static const char *KHTTPTypeJpg="image/pjpeg";
static const char *KHTTPTypeSvg="image/svg+xml";
static const char *KHTTPTypeIco="image/x-icon";
static const char *KHTTPTypeJpeg="image/jpeg";
static const char *KHTTPTypeJson="application/json; charset=utf-8";
static const char *KHTTPTypeJs=  "application/javascript";
static const char *KHTTPTypeText="text/plain";

static const char *KHTTPTypeMp4 ="video/mp4";
static const char *KHTTPTypeMov ="video/quicktime";


static ph_sem_t semThreads;
static SSL_CTX *ctx=NULL;
static pthread_mutex_t *phssl_mutex=NULL;

const char *KSSLExtraPath="/cert/";
const char *KSSLCertificateFile="server.crt";
const char *KSSLPrivateKeyFile="server.key";

//these are needed for CCShutdown
static const char *KJsonAnswerTrue="{\"a\":true}";
//static const char *KJsonAnswerFalse="{\"a\":false}";
static const char *KJsonAnswerFalseErr="{\"a\":false,\"err\":%d}";
static const char *KJsonAnswerFalseMsg="{\"a\":false,\"msg\":\"%s\"}";
static const char *KJsonAnswerForbidden="{\"forbidden\":true,\"reason\":%d,\"un\":\"%s\"}";

static void sendBitrateToClient(struct ps_session * const mySession, const uint32_t bitrate);
static void sendNrSessionsToConnectedClients(struct ps_session *s);

static void getTimeString(int extraSeconds, char *buf, int bufLen){
    time_t t=time(NULL);
    struct tm tmt;
    t+=extraSeconds;
    gmtime_r(&t,&tmt);
    strftime(buf,bufLen,"%a, %d %b %Y %H:%M:%S GMT",&tmt);
    //LOG0("Time: %s",tstr);
}

static int __authorizeCommand(const int request, const uint8_t permissions, const uint8_t comingFromLAN){
    ph_assert(request==RequestView || request==RequestUpload || request==RequestShare || request==RequestAdmin,NULL);
    uint8_t mask;
    if(comingFromLAN)mask=2;
    else mask=1;
    mask<<=request;
    LOG0("__authorizeCommand (request=%d), mask: "BYTETOBINARYPATTERN", permissions: "BYTETOBINARYPATTERN", result: %u",
         request,BYTETOBINARY(mask),BYTETOBINARY(permissions),(unsigned)(mask&permissions));


    if(mask&permissions)return 1;
    return 0;
}

static int authorizeRequest(const int request, struct srvThreadData * __restrict d){
    /* Returns:
     *  1: authenticated
     *  0: not authenticated
     * -1: not authenticated, credentials needed
     *
     * For values smaller than -10 we deny everything
     * -10: client not allowed to connect from Internet
     * -11: Client allowed in theory to connect, but there are no configured users to allow this.
     * -12: authentication requires https
     */
    struct ps_md_config const * const mdConfig=getMdConfigRd();

    int r,comingFromLAN=d->flags&FlagSession_ComingFromLAN;
    if(d->userIndex==0xFF){
        LOG0("Command coming from a non-authenticated user.");
        r=__authorizeCommand(request,mdConfig->permissionsAnonymous,comingFromLAN);
        if(!r && request==RequestView){
            if(comingFromLAN){
                LOG0("Coming from LAN (SSL: %p)",d->ssl);
                //so anonymous request is denied for View when coming from LAN.
                //to possibly succeed we need https and nrAuthUsers>0
                ph_assert(mdConfig->cfgFlags&CfgFlagLanAuthRequired,"LAN authentication required");
                if(!mdConfig->nrAuthUsers)r=-11;
                else {
                    //we have users ...
                    if(!d->ssl && mdConfig->cfgFlags&CfgFlagLanAuthHttpNotAllowed)
                        r=-12; // ... but we need ssl
                }
            } else {
                //NOT coming from LAN
                ph_assert(!(mdConfig->cfgFlags&CfgFlagInetAuthNotRequired),"Inet authentication required");

                if(mdConfig->cfgFlags&CfgFlagInetNotAllowed)
                    r=-10; //access from Internet not allowed
                else {
                    //access from Internet is allowed ...
                    if(!mdConfig->nrAuthUsers)
                        r=-11; //but we do not have users
                    else {
                        //we have users ...
                        if(!d->ssl && !(mdConfig->cfgFlags&CfgFlagInetAuthHttpAllowed))
                            r=-12; // ... but we need ssl
                    }
                }
            }
        }
    } else if(d->userIndex==0xFE){
        LOG0("Command coming from a key-based authorized user.");
        r=__authorizeCommand(request,mdConfig->permissionsAuthWithKey,comingFromLAN);
    } else {
        ph_assert(d->userIndex<mdConfig->nrAuthUsers,"Wrong userIndex value (%u)",(unsigned)d->userIndex);
        //user authentication may requires SSL
        r=0;
        if(!comingFromLAN && mdConfig->cfgFlags&CfgFlagInetNotAllowed){
            LOG0("Requests coming from Internet are not allowed");
            if(request==RequestView)
                r=-10;
            else
                r=-1;
        }
        if(!r && !d->ssl){
            //we do not have ssl. Can we authenticate without it?
            if(comingFromLAN){
                if(mdConfig->cfgFlags&CfgFlagLanAuthHttpNotAllowed){
                    r=-12; //we need ssl for LAN authentication
                    LOG0("LAN request requires SSL");
                }
            } else {
                if(!(mdConfig->cfgFlags&CfgFlagInetAuthNotRequired || mdConfig->cfgFlags&CfgFlagInetAuthHttpAllowed)){
                    r=-12; //we need ssl for Internet authentication
                    LOG0("Internet request requires SSL");
                }
            }
        }

        if(r){
            releaseMdConfig(&mdConfig);
            return r;
        }

        //if here we either have SSL or we can authenticate over plain http
        r=__authorizeCommand(request,mdConfig->authUsers[d->userIndex].permissions,comingFromLAN);
    }
    releaseMdConfig(&mdConfig);
    if(!r && request==RequestView)r=-1;
    return r;
}

static char *getMetadataJsonString(size_t *cntBufLen, int reservedOffset, int minNrCamerasKnownToClient, struct srvThreadData * __restrict d){
    char *cntBuf=NULL;
    int i,cntBufSize=1024;
    unsigned int version=(KPhotostovisVersionMajor<<8)+KPhotostovisVersionMinor;
    unsigned int nrActiveSessions=0;
    ph_assert(reservedOffset==KWebsocketBufferOffset || reservedOffset==0,NULL);
    LOG0("getMetadataJsonString++");

    struct ps_md_pics const *mdPics=getMdPicsRd();
    unsigned int scannedEntries=mdPics->scannedEntries,totalEntries=mdPics->totalEntries;

    if(mdPics->nrCameras>minNrCamerasKnownToClient)
        for(i=0;i<mdPics->nrCameras;i++)
            cntBufSize+=strlen(mdPics->fullMakeModelStrings+mdPics->cameras[i].fullMakeModelOffset)+32;
    releaseMdPics(&mdPics);

    //are we scanning
    unsigned int scanningStatus=0;
    if(statusFlagsCheck(StatusFlagPictureScanThreadActive)){
        if(statusFlagsCheck(StatusFlagThumbnailScanActive))
            scanningStatus=2;
        else scanningStatus=1;
    } else scanningStatus=0;

    //we can allocate the buffer now
    cntBuf=ph_malloc(cntBufSize+reservedOffset);
    //print those things we will print anyway
    struct ps_md_net const * const mdNet=getMdNetRd();
    for(i=0;i<NR_PARALLEL_SESSIONS;i++)
        if(mdNet->sessions[i].sessionId)
            nrActiveSessions++;
    ph_assert(nrActiveSessions>=1,"Nr active sessions is %u",nrActiveSessions);
    *cntBufLen=snprintf(cntBuf+reservedOffset,cntBufSize,
                        "{\"te\":%u,\"se\":%u,\"gcs\":%u,\"pcs\":%u,\"v\":%u,\"ss\":%u,\"scst\":%u",
                        totalEntries,scannedEntries,(unsigned int)mdNet->gatewayCfgStatus,(unsigned int)mdNet->phonetCfgStatus,
                        version,nrActiveSessions,scanningStatus);
    if(mdNet->phonetCfgCurlErrorString)
        *cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,",\"pcsErrStr\":\"%s\"",mdNet->phonetCfgCurlErrorString);
    releaseMdNet(&mdNet);

    //if we have phonetUsername, add it
    struct ps_md_config const * const mdConfig=getMdConfigRd();
    int nrUsers=mdConfig->nrAuthUsers;
    if(mdConfig->phonetUsername){
        *cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,",\"un\":\"%s\"",mdConfig->phonetUsername);
        ph_assert(*cntBufLen<cntBufSize,NULL);
    }
    releaseMdConfig(&mdConfig);

    //if we have a sessionId, add it
    if(d && d->mySessionId){
        unsigned int canSignin,canSignout;
        if(nrUsers && d->userIndex==0xFF)canSignin=1;
        else canSignin=0;
        if(d->userIndex<0xFF)canSignout=1; //we can signout from key-based authentication
        else canSignout=0;
        unsigned int comingFromLAN=d->flags&FlagSession_ComingFromLAN?1:0;
        int canAdmin=authorizeRequest(RequestAdmin,d);

        *cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,
                             ",\"sid\":\"%016"UINT64X_FMT"\",\"canUpload\":%u,\"canShare\":%u,\"canAdmin\":%u,\"canSignin\":%u,\"canSignout\":%u,\"fromLAN\":%u",
                             d->mySessionId,authorizeRequest(RequestUpload,d),authorizeRequest(RequestShare,d),canAdmin,
                             canSignin,canSignout,comingFromLAN);
        ph_assert(*cntBufLen<cntBufSize,NULL);

        //should the client configure this server?
        if(canAdmin){
            //send to the client information that it can configure this server
            struct ps_md_config const * const mdConfig=getMdConfigRd();
            char uid[33];
            getUniqueID(uid,mdConfig->cfgNumber);

            //LOG0("######################################### permissionsAuthWithKey=%u",(unsigned int)mdConfig->permissionsAuthWithKey);
            unsigned int hasHttps=mdConfig->httpsSocket>=0?1:0;
            *cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,
       ",\"config\":{\"cfgFlags\":%u,\"uid\":\"%s\",\"permissionsAnonymous\":%u,\"permissionsAuthWithKey\":%u,\"sshPortExt\":%u,\"hasHttps\":%u,\"authUsers\":[",
                                 (unsigned int)mdConfig->cfgFlags,uid,(unsigned int)mdConfig->permissionsAnonymous,(unsigned int)mdConfig->permissionsAuthWithKey,
                                 (unsigned int)mdConfig->sshPortExt,hasHttps);
            for(i=0;i<mdConfig->nrAuthUsers;i++){
                if(i)
                    *cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,",");
                *cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,
                                     "{\"username\":\"%s\",\"permissions\":%u}",
                                     mdConfig->authUsers[i].username,(unsigned int)mdConfig->authUsers[i].permissions);
            }
            *cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,"]}");
            releaseMdConfig(&mdConfig);

            //add info about free and used space
            unsigned int rootDataSizeMB=0,rootFreeSizeMB=0,dataDataSizeMB=0,dataFreeSizeMB=0;
            updateSizes(&rootDataSizeMB,&rootFreeSizeMB,&dataDataSizeMB,&dataFreeSizeMB);
            *cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,",\"tds\":%u,\"fds\":%u",dataDataSizeMB,dataFreeSizeMB);
        }
    }

    if(reservedOffset){//add a type
        *cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,",\"type\":\"md\"");
        ph_assert(*cntBufLen<cntBufSize,NULL);
    }

    mdPics=getMdPicsRd();
    if(mdPics->nrCameras>minNrCamerasKnownToClient){
        *cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,",\"n\":%u,\"m\":[",(unsigned int)mdPics->nrCameras);
        ph_assert(*cntBufLen<cntBufSize,NULL);
        for(i=0;i<mdPics->nrCameras;i++){
            struct ps_camera *camera=&mdPics->cameras[i];
            //get the camera name
            char *cameraName=NULL;
            if(camera->shortenedMakeModelOffset!=0xFFFF)
                cameraName=mdPics->shortenedMakeModelStrings+camera->shortenedMakeModelOffset;//we have a short name
            else
                cameraName=mdPics->fullMakeModelStrings+camera->fullMakeModelOffset;
            //write stuff
            if(i)*cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,",");
            *cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,"{\"c\":\"%s\",\"p\":%d,\"mfl\":%f}",
                                 cameraName,camera->nrPictures,camera->minFocalLength);

            ph_assert(*cntBufLen<cntBufSize,NULL);
        }//for
        *cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,"]}");
    } else
        *cntBufLen+=snprintf(cntBuf+reservedOffset+ *cntBufLen,cntBufSize- *cntBufLen,"}");
    //done
    releaseMdPics(&mdPics);
    ph_assert(*cntBufLen<cntBufSize,NULL);
    LOG0("getMetadataJsonString--");

    return cntBuf;
}

/*
static void closeSessionWebsocket(struct ps_session *s, int doNotCloseIfEqualToThis){
    ASSERT_SESSION_LOCKED(s);

    //there was an error writing, close the websocket
    if(s->websocketSsl && *s->websocketSsl){
        SSL_free(*s->websocketSsl);
        *s->websocketSsl=NULL;
    }

    if(doNotCloseIfEqualToThis!=s->websocket)
        close(s->websocket);
    s->websocket=-1;
    s->websocketSsl=NULL;
}*/

int enqueueWebsocketPacket(struct ps_session *s, char * const buffer, uint32_t bufferLen, uint32_t bufferOffset,
                           const int lockSession, const int createWebsocketPacket){
    int r;
    if(createWebsocketPacket){
        r=createWebsocketTextPacket(buffer,&bufferLen,&bufferOffset);
        ph_assert(!r,NULL);
    }

    if(lockSession){
        pthread_mutex_lock(&s->sessionMutex);
        s->flags|=FlagSession_IsLocked;
    }

    if(s->websocketPipe>=0){
        write(s->websocketPipe,&bufferLen,4);
        write(s->websocketPipe,buffer+bufferOffset,bufferLen);
        r=0;
    } else {
        LOG0("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ There is no session websocket, cannot enqueue websocket packet.");
        r=-1;
    }


    if(lockSession){
        s->flags&=~FlagSession_IsLocked;
        pthread_mutex_unlock(&s->sessionMutex);
    }
    return r;
}


///////////////////////////////////////////

static struct ps_session *getNewSession(const uint64_t oldSessionId){
    int i,freeSession=-1,earliestSessionIndex=-1;
    uint32_t earliestSession=0xFFFFFFFF;
    uint32_t lastTimeActivity;

    //use nrConnectedThreads to select a free session
    //refuse connections if there are too many clients (returning NULL)
    LOG0("getNewSession++");

    struct ps_md_net *mdNetW=getMdNetWritable();

    //if we have an old session, decrement the number of connected threads, because this thread will take its place
    if(oldSessionId){
        for(i=0;i<NR_PARALLEL_SESSIONS;i++)
            if(mdNetW->sessions[i].sessionId==oldSessionId){
                mdNetW->sessions[i].nrConnectedThreads--;
                break;
            }
    }


    //the callling entity is responsible with calling releaseMdNetW()
    for(i=0;i<NR_PARALLEL_SESSIONS;i++){
        if(freeSession<0 && mdNetW->sessions[i].sessionId==0){
            freeSession=i;
            break;
        } else if(mdNetW->sessions[i].nrConnectedThreads==0){
            lastTimeActivity=mdNetW->sessions[i].thumbsTransferStart;
            if(lastTimeActivity==0xFFFFFFFF || lastTimeActivity<mdNetW->sessions[i].lastSentPacketTime)
                lastTimeActivity=mdNetW->sessions[i].lastSentPacketTime;
            ph_assert(lastTimeActivity!=0xFFFFFFFF,NULL);
            if(earliestSession>lastTimeActivity){
                earliestSession=lastTimeActivity;
                earliestSessionIndex=i;
            }
        } else LOG0("Session %"UINT64X_FMT" has %d active threads (still)",mdNetW->sessions[i].sessionId,mdNetW->sessions[i].nrConnectedThreads);
    }

    //do we have a (potentially) free session?
    if(freeSession<0 && earliestSessionIndex<0){
        LOG0("WARNING: getNewSession: no free/idle session found. Server too busy?");
        releaseMdNetW(&mdNetW);
        return NULL;
    }

    struct ps_session * __restrict s;
    //if we are here it means we can return a session
    if(freeSession>=0){
        s=&(mdNetW->sessions[freeSession]);
        //ph_assert(s->lastRequestedAlbumKey==NULL,NULL);
        ph_assert(s->nrConnectedThreads==0,NULL);
#ifdef DEBUG_THUMBNAILS_PROCESSING_TIME
        ph_assert(s->thumbnailsProcessingTime==NULL,NULL);
#endif
        LOG0("Initializing a session that was previously free!");
    } else {
        //if here, we use the earliest session
        ph_assert(earliestSessionIndex>=0,NULL);
        ph_assert(earliestSession<0xFFFFFFFF && earliestSession>0,"%u",earliestSession);
        s=&(mdNetW->sessions[earliestSessionIndex]);

        /*
        if(s->lastRequestedAlbumKey)
            free(s->lastRequestedAlbumKey);*/
#ifdef DEBUG_THUMBNAILS_PROCESSING_TIME
        if(s->thumbnailsProcessingTime){
            free(s->thumbnailsProcessingTime);
            s->thumbnailsProcessingTime=NULL;
            s->thumbnailsProcessingTime_allocatedSize=0;
        }
#endif
        unsigned int minutesSince=(time(NULL)-earliestSession)/60;

        LOG0("Initializing a session that was previously in use %u minutes ago!",minutesSince);
    }

    //initializing/resetting
    memset((char*)s+sizeof(pthread_mutex_t),0,sizeof(struct ps_session)-sizeof(pthread_mutex_t));

    //we need to generate a random session ID
    int r=RAND_bytes((unsigned char *)&s->sessionId,8);
    ph_assert(r==1,"RAND_bytes returned an error (%d)",r);
    LOG0("Session ID: %016"UINT64X_FMT,s->sessionId);

    s->userIndex=0xFF;
    s->websocketPipe=-1;
    s->nrConnectedThreads=1; //this one!
    s->imageTransferStart=s->thumbsTransferStart=s->uploadRecvStart=s->uploadLastEnd=0xFFFFFFFF;
    s->lastSentPacketTime=time(NULL); //we are using this in case there are no bandwidth items
    s->averageViewTime=MIN_AVERAGE_VIEW_TIME;

    releaseMdNetW(&mdNetW);

    sendNrSessionsToConnectedClients(NULL);

    getMdNetRd();
    pthread_mutex_lock(&s->sessionMutex);
    s->flags|=FlagSession_IsLocked;
    LOG0("getNewSession-- (session %"UINT64X_FMT" locked)",s->sessionId);

    return s;
}

struct ps_session *getSession(uint64_t sessionId, uint32_t clientIp, int expectedToFind){
    if(!sessionId)return NULL;
    //if successfull the callling entity is responsible with calling unlockSession()
    ph_assert(clientIp,NULL);

    int i;
    char ipStr[16];
    printIP(clientIp,ipStr);
    LOG0("getSession++ (%"UINT64X_FMT", IP: %s))",sessionId,ipStr);
    struct ps_md_net const * mdNet=getMdNetRd();
    LOG0("getSession: got mdNet");
    //the callling entity is responsible with calling unlockSession()
    for(i=0;i<NR_PARALLEL_SESSIONS;i++){
        if(mdNet->sessions[i].sessionId==sessionId && mdNet->sessions[i].clientIp==clientIp){
            struct ps_session *s=(struct ps_session *)&mdNet->sessions[i];
            pthread_mutex_lock(&s->sessionMutex);
            s->flags|=FlagSession_IsLocked;
            LOG0("getSession-- (session %"UINT64X_FMT" locked)",s->sessionId);
            return s;//this is our session
        }
    }
    //if here, we could not find it!
    releaseMdNet(&mdNet);
    ph_assert(!expectedToFind,"getSession: we were expecting to find this session, but we didn't!");
    LOG0("getSession-- (not found)");
    return NULL;
}

static void unlockSession(struct ps_session **s){
    ph_assert(s,"Session to unlock is NULL");
    struct ps_session *ss=*s;
    ASSERT_SESSION_LOCKED(ss);
    ss->flags&=~FlagSession_IsLocked;
    LOG0("Session %"UINT64X_FMT" unlocked!",ss->sessionId);
    pthread_mutex_unlock(&ss->sessionMutex);
    releaseMdNet(NULL);
    *s=NULL;
}


static void printSslErrors(){
    unsigned long err;

    //ERR_print_errors_fp(stderr);
    while((err=ERR_get_error())){
        LOG0("SSL error: %s",ERR_error_string(err,NULL));
    }
}

int srvWriteGeneric(int c, void *ssl, void const * const  __restrict buf, const int bufLen, const int isWebsocket){
    int r;
    if(ssl){
        r=SSL_write((SSL*)ssl,buf,bufLen);
        if(r!=bufLen){
            LOG0("SSL_write(... %d) returned %d.",bufLen,r);
            if(r<=0)printSslErrors();
            return -1;
        }
    } else {
        r=send(c,buf,bufLen,MSG_NOSIGNAL);
        if(r!=bufLen){
            LOG0return(r<0,-1,"ERROR sending (%ssocket: %d): %s",isWebsocket?"web":"",c,strerror(errno));
            LOG0return(r!=bufLen,r,"ERROR sending (%ssocket: %d). Wrote %d bytes instead of %d",isWebsocket?"web":"",c,r,bufLen);
        }
    }
    return r;
}

static int srvWrite(struct srvThreadData * __restrict d, void const * const  __restrict buf, const int bufLen){
    return srvWriteGeneric(d->c,d->ssl,buf,bufLen,0);
}

static void sendError(struct srvThreadData * __restrict d, const int code){
    int l=0;
    char buf[KErrorSendBufferLength];
    switch(code){
    case 400:l=strlen(KHTTP400);memcpy(buf,KHTTP400,l);break;
    case 403:l=strlen(KHTTP403);memcpy(buf,KHTTP403,l);break;
    case 404:l=strlen(KHTTP404);memcpy(buf,KHTTP404,l);break;
    case 500:l=strlen(KHTTP500);memcpy(buf,KHTTP500,l);break;
    case 501:l=strlen(KHTTP501);memcpy(buf,KHTTP501,l);break;
    case 503:l=strlen(KHTTP503);memcpy(buf,KHTTP503,l);break;
    case 505:l=strlen(KHTTP505);memcpy(buf,KHTTP505,l);break;
    default:ph_assert(0,"HTTP error not handlerd");
    }
    memcpy(buf+l,KHTTPErrRspConnClose,KHTTPErrRspConnCloseLen+1);
    srvWrite(d,buf,l+KHTTPErrRspConnCloseLen);
}

static int getVideoHttp(struct md_entry_id const * const id, char *eTag, int eTagSize, const char *reqETag, const int httpKeepAlive,
                 struct srvThreadData *d, const char *dateStr, uint64_t rangeStart, uint64_t rangeEnd){
    char path[PATH_MAX];
    struct md_album_entry const *e=NULL;
    struct mdx_album_entry const *ex;

    int r=getEntryAndPath(id,KPicturesFolder,path,NULL,&e,&ex);
    LOG0close_return(!e,-1,releaseMdPics(NULL),"WARNING: requested video not found.");
    LOG0close_return(!(e->flags&FlagIsVideo),-1,releaseMdPics(NULL),"WARNING: requested entry is NOT a video!");

    //write the eTag (it was an etagf before (with flags), but I see no reason for the flags)
    r=snprintf(eTag,eTagSize,"%08X%08X%08X%08X",e->nameCRC,(unsigned int)e->captureTime,e->size,(unsigned int)e->modificationTime/*,(unsigned int)e->flags*/);
    ph_assert(r<eTagSize,NULL);
    if(reqETag && !strcmp(eTag,reqETag) && rangeStart==(uint64_t)-1){
        //we have the same tag, we can answer differently
        releaseMdPics(NULL);
        return 1;//the same tag for a request without range. return 1 (will return 304 to the client)
    }
    //get the file size
    uint64_t fileSize,remainingSize,originalFileSize,sentBytes;
    if(e->flags&FlagSizeIsInMB)
        fileSize=((uint64_t)e->size<<20)+ex->sizeLo;
    else
        fileSize=e->size;
    releaseMdPics(NULL); //locked inside getEntryAndPath

    if(reqETag && strcmp(eTag,reqETag) && rangeStart!=(uint64_t)-1)
        rangeStart=(uint64_t)-1;//different tag in a request with range. We provide full answer

    ph_assert(strlen(eTag),NULL);

    //open the video file
    int r2,fd=-1;
    if(!(d->flags&FlagSession_ComingFromLAN)){
        //try to find a transcoded video:

        char pathTrans[PATH_MAX];
        getTranscodedPathForMedia(path,NULL,pathTrans);
        LOG0("Looking for transcoded video in: %s",pathTrans);

        fd=open(pathTrans,O_RDONLY);
        if(fd>=0){
            LOG0("Found transcoded video, using it (%s).",pathTrans);
            //recalculate file size
            struct stat st;
            r=fstat(fd,&st);
            ph_assert(!r,NULL);
            fileSize=st.st_size;
        }
    }

    if(fd<0)
        fd=open(path,O_RDONLY);
    if(fd==-1){
        LOG0("Unable to open video (%s)",path);
        return -1;
    }

    //get the content type
    const char *cntType=NULL; //to silence a warning
    r=strlen(path);
    ph_assert(r>4,NULL);
    char *extension=(char*)path+r-4;
    if(!strcasecmp(extension,KMp4Extension) || !strcasecmp(extension,KM4vExtension))
        cntType=KHTTPTypeMp4;
    else if(!strcasecmp(extension,KMovExtension))
        cntType=KHTTPTypeMov;
    else LOG0return(1,-1,"Video type not recognized");

    //adjust fileSize according to the requested range. Using remainingSize as temp variable
    originalFileSize=fileSize;
    if(rangeStart!=(uint64_t)-1){
        remainingSize=fileSize;
        if(rangeStart<fileSize)
            remainingSize-=rangeStart;
        if(rangeEnd!=(uint64_t)-1 && rangeEnd<fileSize && rangeStart<=rangeEnd)
            remainingSize-=(fileSize-rangeEnd-1);
        fileSize=remainingSize;

        //position the read pointer according to rangeStart
        if(rangeStart){
            lseek(fd,rangeStart,SEEK_SET);
            LOG0("Video seeked to %"UINT64_FMT" bytes.",rangeStart);
        }
    }

    //sending the HTTP header
    char rspBuf[HTTP_RESPONSE_BUFFER_SIZE];
    if(rangeStart==(uint64_t)-1){
        //there is no range requested. Sending normal answer
        LOG0("Normal request (video).");
        r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,KHTTPVideoOk,httpKeepAlive?KConnectionKeepAlive:KConnectionClose,cntType,fileSize,eTag,dateStr);
    } else {
        //we have a range. Sending 206
        if(rangeEnd==(uint64_t)-1){
            LOG0("Video request with range: %"UINT64_FMT"-",rangeStart);
            rangeEnd=fileSize-1;
        } else
            LOG0("Video request with range: %"UINT64_FMT"-%"UINT64_FMT,rangeStart,rangeEnd);
        r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,KHTTPVideo206,httpKeepAlive?KConnectionKeepAlive:KConnectionClose,
                   cntType,fileSize,dateStr,rangeStart,rangeStart+fileSize-1,originalFileSize);
    }

    LOG0close_return(r<0,-1,sendError(d,500),"ERROR error writing the HTTP response header to the buffer");
    LOG0close_return(r>=HTTP_RESPONSE_BUFFER_SIZE,-1,sendError(d,500),
            "ERROR: Insufficient buffer size (%d) for the HTTP response header",HTTP_RESPONSE_BUFFER_SIZE);
    ph_assert(r==strlen(rspBuf),"%d!=%zu",r,strlen(rspBuf));
    r2=srvWrite(d,rspBuf,r);
    LOG0return(r2<0,-1,"ERROR writing the HTTP response header: %s",strerror(errno));
    LOG0return(r2!=r,-1,"ERROR writing the HTTP response header. Wrote %d bytes instead of %d",r2,r);

    LOG0("Answered [%d]:",r);
    LOG0("%s",rspBuf);

    //allocate the buffer
    char *b=NULL;
    if(fileSize<VIDEO_BUF_LEN)
        b=(char*)ph_malloc(fileSize);
    else
        b=(char*)ph_malloc(VIDEO_BUF_LEN);
    remainingSize=fileSize;

    //clear the timeout
    /*
    struct timeval timeout;
    timeout.tv_sec=0;
    timeout.tv_usec=0;
    setsockopt(d->c,SOL_SOCKET,SO_SNDTIMEO,&timeout,sizeof(timeout));*/

    //read and send the file
    int retries=0;
    while(remainingSize>0){
        if(remainingSize<VIDEO_BUF_LEN)
            r=read(fd,b,remainingSize);
        else
            r=read(fd,b,VIDEO_BUF_LEN);
        //send
        if(r<=0){
            if(retries<100){
                LOG0("getVideoHttp: read returned %d, remaining: %"UINT64_FMT"/%"UINT64_FMT". Retries: %d/100",r,remainingSize,fileSize,retries+1);
                retries++;
                continue;
            }
            LOG0close_return(1,-2,free(b);close(fd),"getVideoHttp: read error, aborting transmission.");
        }

        LOG0("Sending %d bytes.",r);
        retries=0;
        sentBytes=0;
        while(sentBytes<r){
            r2=srvWrite(d,b+sentBytes,r-sentBytes);
            LOG0c(r2>=0,"Video: sent %d bytes",r2);
            LOG0c(r2==-1,"getVideoHttp: Socket error: %s",strerror(errno));

            LOG0close_return(r2==-1,-2,free(b);close(fd),"getVideoHttp: Browser closed the socket. Sent %"UINT64_FMT" bytes..",fileSize-remainingSize-sentBytes);

            //if we are here it means thtat we managed to send something
            sentBytes+=r2;
            ph_assert(sentBytes<=r,NULL);
        }
        remainingSize-=sentBytes;
    }

    close(fd);
    free(b);

    return 0;
}


int httpStart(uint16_t port){
    int r,s;
    struct sockaddr_in saddr;

    //create the socket
    s=socket(AF_INET,SOCK_STREAM,0);
    LOG0return(s==-1,-1,"ERROR: cannot create TCP socket: %s",strerror(errno));
    fcntl(s,F_SETFD,FD_CLOEXEC); //just in case

    //bind
    saddr.sin_family=AF_INET;
    saddr.sin_port=htons(port);
    saddr.sin_addr.s_addr=INADDR_ANY; //TODO: options here

    while(1){
        r=bind(s,(struct sockaddr*)&saddr,sizeof(saddr));
        if(!r)break;
        if(r && errno==EADDRINUSE){
            LOG0close_return(shouldExit,-1,close(s),"Should exit.");
            LOG0("WARNING: address already in use. Retrying in 10 seconds.");
            LOG_FLUSH;
            sleep(10);
            continue;
        }
        LOG0close_return(r<0,-1,close(s),"ERROR: cannot bind socket: %s",strerror(errno));
    }

    //listen
    r=listen(s,DEFAULT_LISTENING_QUEUE_LEN);
    LOG0close_return(r<0,-1,close(s),"ERROR: cannot listen on socket: %s",strerror(errno));

    LOG0("Web server (http) listening on port %d",port);
    //create the semaphore for limiting the number of threads
    ph_sem_init(&semThreads,0,MAX_WEB_THREADS);

    //KHTTPErrRspLen=strlen(KHTTPErrRsp);
    KHTTPErrRspConnCloseLen=strlen(KHTTPErrRspConnClose);
    ph_assert(KErrorSendBufferLength>KHTTPErrRspConnCloseLen+64,"The buffer for sendError function needs to be increased.");

    return s;
}

////////////////////////////////// https/SSL-related functions

void phssl_locking_callback(int mode, int type, const char *file, int line){
    (void)file;
    (void)line;
    if(mode & CRYPTO_LOCK)
        pthread_mutex_lock(&(phssl_mutex[type]));
    else
        pthread_mutex_unlock(&(phssl_mutex[type]));
}

void phssl_thread_id(CRYPTO_THREADID *tid){
    CRYPTO_THREADID_set_numeric(tid, (unsigned long)pthread_self());
}

int httpsStart(uint16_t port){
    int r,s,l,i;
    struct sockaddr_in saddr;

    //SSL part
    //first, verify that the certificate and key do exist
    char sslPath[1024]; // 1kB should be enough
    char crtFile[1024]; // 1kB should be enough
    char keyFile[1024]; // 1kB should be enough

    FILE *f=NULL;
    struct ps_md_config const * const mdConfig=getMdConfigRd();
    l=strlen(mdConfig->rootFolder);

    memcpy(sslPath,mdConfig->rootFolder,l);
    memcpy(sslPath+l,KSSLExtraPath,strlen(KSSLExtraPath)+1);
    l=strlen(sslPath);

    memcpy(crtFile,sslPath,l);
    memcpy(keyFile,sslPath,l);

    releaseMdConfig(&mdConfig);

    memcpy(crtFile+l,KSSLCertificateFile,strlen(KSSLCertificateFile)+1);
    memcpy(keyFile+l,KSSLPrivateKeyFile, strlen(KSSLPrivateKeyFile)+1);

    //verify if these 4 files exist
    f=fopen(crtFile,"r");
    LOG0return(!f,-1,"httpsStart: Server Certificate file (%s) cannot be opened.",crtFile);
    fclose(f);

    f=fopen(keyFile,"r");
    LOG0return(!f,-1,"httpsStart: Server Private key file (%s) cannot be opened.",keyFile);
    fclose(f);

    f=NULL;

    //if we are here, the certificate and private key files do exist
    SSL_library_init();
    OpenSSL_add_all_algorithms();  /* load & register all cryptos, etc. */
    SSL_load_error_strings();   /* load all error messages */
    ph_assert(ctx==NULL,NULL);

    ctx=SSL_CTX_new(TLSv1_2_server_method()/*TLS_server_method()*/);   /* create new context from method */
    if(!ctx){
        LOG0("WARNING: SSL_CTX_new failed. Error(s):");
        printSslErrors();
        return -2;
    }
    //SSL_CTX_set_min_proto_version(ctx,TLS1_2_VERSION);


    //verify the certificate:
    //http://fm4dd.com/openssl/certverify.htm
    //http://stackoverflow.com/questions/21297853/how-to-determine-ssl-cert-expiration-date-from-a-pem-encoded-certificate/31718838#31718838
    //not v useful:
    //http://stackoverflow.com/questions/16291809/programmatically-verify-certificate-chain-using-openssl-api

    //Load the certificates
     /*
    if(SSL_CTX_load_verify_locations(ctx,crtFile,sslPath) != 1){
        LOG0("WARNING: SSL_CTX_load_verify_locations failed. Error(s):");
        printSslErrors();
        SSL_CTX_free(ctx);
        ctx=NULL;
        return -2;
    }

    if(SSL_CTX_set_default_verify_paths(ctx) != 1){
        LOG0("WARNING: SSL_CTX_set_default_verify_paths failed. Error(s):");
        printSslErrors();
        SSL_CTX_free(ctx);
        ctx=NULL;
        return -2;
    }*/

    /* set the local certificate from crtFile */
    if(SSL_CTX_use_certificate_chain_file(ctx,crtFile) <= 0){
        LOG0("WARNING: SSL_CTX_use_certificate_file failed. Error(s):");
        printSslErrors();
        SSL_CTX_free(ctx);
        ctx=NULL;
        return -2;
    }

    /* set the private key from keyFile (may be the same as CertFile) */
    if(SSL_CTX_use_PrivateKey_file(ctx,keyFile,SSL_FILETYPE_PEM) <= 0){
        LOG0("WARNING: SSL_CTX_use_PrivateKey_file failed. Error(s):");
        printSslErrors();
        SSL_CTX_free(ctx);
        ctx=NULL;
        return -2;
    }
    /* verify private key */
    if(!SSL_CTX_check_private_key(ctx)){
        LOG0("WARNING: SSL_CTX_check_private_key failed. The private key does not match the certificate.");
        SSL_CTX_free(ctx);
        ctx=NULL;
        return -2;
    }


    /*
    //set the sub.class1 certificate from subClass1File
    if(SSL_CTX_use_certificate_chain_file(ctx,subClass1File) <= 0){
        LOG0("WARNING: SSL_CTX_use_certificate_chain_file failed. Error(s):");
        printSslErrors();
        SSL_CTX_free(ctx);
        ctx=NULL;
        return -2;
    }
    // set the CA certificate from caFile
    if(SSL_CTX_use_certificate_chain_file(ctx,caFile) <= 0){
        LOG0("WARNING: SSL_CTX_use_certificate_chain_file failed. Error(s):");
        printSslErrors();
        SSL_CTX_free(ctx);
        ctx=NULL;
        return -2;
    }*/

    //threads setup
    r=CRYPTO_num_locks();
    LOG0("SSL required number of locks: %d",r);
    phssl_mutex= OPENSSL_malloc(r*sizeof(pthread_mutex_t));
    for(i=0;i<r;i++)
        pthread_mutex_init(&(phssl_mutex[i]),NULL);

    CRYPTO_THREADID_set_callback(phssl_thread_id);
    CRYPTO_set_locking_callback(phssl_locking_callback);
    //done with SSL stuff


    //create the socket
    s=socket(AF_INET,SOCK_STREAM,0);
    LOG0return(s==-1,-3,"ERROR: cannot create TCP socket: %s",strerror(errno));
    fcntl(s,F_SETFD,FD_CLOEXEC); //just in case

    //bind
    saddr.sin_family=AF_INET;
    saddr.sin_port=htons(port);
    saddr.sin_addr.s_addr=INADDR_ANY; //TODO: options here

    while(1){
        r=bind(s,(struct sockaddr*)&saddr,sizeof(saddr));
        if(!r)break;
        if(r && errno==EADDRINUSE){
            LOG0close_return(shouldExit,-3,close(s),"Should exit.");
            LOG0("WARNING: address already in use. Retrying in 10 seconds.");
            LOG_FLUSH;
            sleep(10);
            continue;
        }
        LOG0close_return(r<0,-3,close(s);SSL_CTX_free(ctx);ctx=NULL,"ERROR: cannot bind socket: %s",strerror(errno));
    }

    //listen
    r=listen(s,DEFAULT_LISTENING_QUEUE_LEN);
    LOG0close_return(r<0,-3,close(s);SSL_CTX_free(ctx);ctx=NULL,"ERROR: cannot listen on socket: %s",strerror(errno));

    LOG0("Web server (https) listening on port %d",port);

    return s;
}

int srvStop(){
    struct ps_md_config *mdConfigW=getMdConfigWritable();

    if(mdConfigW->httpSocket>=0)close(mdConfigW->httpSocket);
    mdConfigW->httpSocket=-1;

    if(mdConfigW->httpsSocket>=0){
        close(mdConfigW->httpSocket);
        mdConfigW->httpSocket=-1;

        //free/release thread locking
        int i,r=CRYPTO_num_locks();
        CRYPTO_set_locking_callback(NULL);
        for(i=0;i<r;i++)
            pthread_mutex_destroy(&(phssl_mutex[i]));
        OPENSSL_free(phssl_mutex);phssl_mutex=NULL;

        SSL_CTX_free(ctx);ctx=NULL;
    }

    releaseMdConfigW(&mdConfigW);
    ph_sem_destroy(&semThreads);
    return 0;
}


static int srvSendFile(struct srvThreadData * __restrict d, const char *reqUrl, const int reqUrlLen, const char *reqETag,
                       const int acceptsGzip, const int httpKeepAlive){
    /* returns:
     *  0 on success
     * -1 on errors (connection should be closed by the caller)
     */
    int r,r2;
    char rspBuf[HTTP_RESPONSE_BUFFER_SIZE];
    ph_assert(reqUrl,NULL);

    struct ps_fm_file fmFile;

    LOG0return(reqUrlLen<6 && (reqUrlLen!=1 && reqUrl[0]!='/'),-1,"WARNING: requested URL (%s) is malformed.",reqUrl);
    //find the file extension. We need it for the content type header
    const char *contentType=NULL;
    if(reqUrlLen==1 || !strcasecmp(reqUrl+reqUrlLen-5,".html"))contentType=KHTTPTypeHtml;
    else if(!strcasecmp(reqUrl+reqUrlLen-4,".css"))contentType=KHTTPTypeCss;
    else if(!strcasecmp(reqUrl+reqUrlLen-4,".png"))contentType=KHTTPTypePng;
    else if(!strcasecmp(reqUrl+reqUrlLen-4,".gif"))contentType=KHTTPTypeGif;
    else if(!strcasecmp(reqUrl+reqUrlLen-4,KJpegExtension))contentType=KHTTPTypeJpeg;
    else if(!strcasecmp(reqUrl+reqUrlLen-4,".svg"))contentType=KHTTPTypeSvg;
    else if(!strcasecmp(reqUrl+reqUrlLen-4,".ico"))contentType=KHTTPTypeIco;
    else if(!strcasecmp(reqUrl+reqUrlLen-3,".js"))contentType=KHTTPTypeJs;
    else LOG0close_return(1,-1,sendError(d,404),"WARNING: cannot get content type from the request URL (%s)",reqUrl);

    r=getFile(reqUrl,reqUrlLen,reqETag,acceptsGzip,&fmFile);
    LOG0close_return(r,-1,sendError(d,404),"WARNING: requested url not found: %s",reqUrl);

    //get the date, we will need it
    char dateStr[100];
    getTimeString(0,dateStr,100);

    if(fmFile.buf==reqETag){
        //we have the same tag, we can answer differently
        r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,KHTTP304,httpKeepAlive?KConnectionKeepAlive:KConnectionClose,dateStr);
        //continue sending the response header
        LOG0close_return(r<0,-1,sendError(d,500),"ERROR error writing the HTTP response header to the buffer");
        LOG0close_return(r>=HTTP_RESPONSE_BUFFER_SIZE,-1,sendError(d,500),
                         "ERROR: Insufficient buffer size (%d) for the HTTP response header",HTTP_RESPONSE_BUFFER_SIZE);
        ph_assert(r==strlen(rspBuf),"%d!=%zu",r,strlen(rspBuf));
        r2=srvWrite(d,rspBuf,r);
        //done writhing response
        LOG0("Done writing response (304 Not modified)");
        return 0;
    }
    //if here, we can send the file
    ph_assert(fmFile.buf,NULL);
    ph_assert(fmFile.bufLen>0,NULL);
    LOG0("Here in srvSendFile");

    //send the response header
    if(fmFile.flags&FlagIsGZipped)
        r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,
                   KHTTPOkGz,httpKeepAlive?KConnectionKeepAlive:KConnectionClose,contentType,(uint64_t)fmFile.bufLen,fmFile.eTag,dateStr);
    else
        r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,
                   KHTTPOk,httpKeepAlive?KConnectionKeepAlive:KConnectionClose,contentType,(uint64_t)fmFile.bufLen,fmFile.eTag,dateStr);

    LOG0close_return(r<0,-1,if(fmFile.flags&FlagFreeData)free(fmFile.buf);sendError(d,500),"ERROR error writing the HTTP response header to the buffer");
    LOG0close_return(r>=HTTP_RESPONSE_BUFFER_SIZE,-1,if(fmFile.flags&FlagFreeData)free(fmFile.buf);sendError(d,500),
            "ERROR: Insufficient buffer size (%d) for the HTTP response header",HTTP_RESPONSE_BUFFER_SIZE);
    ph_assert(r==strlen(rspBuf),"%d!=%zu",r,strlen(rspBuf));
    r2=srvWrite(d,rspBuf,r);
    LOG0close_return(r2!=r,-1,if(fmFile.flags&FlagFreeData)free(fmFile.buf),"ERROR writing the HTTP response header.");

    //send the buffer
    LOG0("Sending the data");
    r=srvWrite(d,fmFile.buf,fmFile.bufLen);
    LOG0("Data sent");
    if(fmFile.flags&FlagFreeData){free(fmFile.buf);fmFile.buf=NULL;}
    LOG0("Data freed.");
    LOG0return(r<0,-1,"ERROR writing response data: %s",strerror(errno));
    LOG0return(r!=fmFile.bufLen,-1,"ERROR: could not send all response content (%d written, %d attempted)",r,fmFile.bufLen);

    //done writhing response
    LOG0("Done writing response!");
    return 0;
}

static int isFacebookUrl(const char *reqUrl, const int reqUrlLen){
    if(reqUrlLen>5 && reqUrl[0]=='/' && reqUrl[1]=='f' && reqUrl[2]=='/' && reqUrl[4]=='/' && (reqUrl[3]=='a' || reqUrl[3]=='i' || reqUrl[3]=='t'))
        return 1; //true
    return 0;//false
}

static int parseKey(char const * const __restrict key, struct md_entry_id * __restrict id){
    int i,keyLen=strlen(key);
    LOG0return(keyLen<40,-1,"WARNING: key too short");

    LOG0("We are parsing a key: %s",key);
    //remove any %20 we may have in the end
    while(!strncmp(key+keyLen-3,"%20",3))
        keyLen-=3;
    LOG0return(keyLen<40,-1,"WARNING: key too short (after removing spaces)");

    //validate the key
    SHA_CTX sha1;
    SHA1_Init(&sha1);
    //first we add the server name
    struct ps_md_config const * const mdConfig=getMdConfigRd();
    int phonetUsernameLen=0;
    if(mdConfig->phonetUsername)
        phonetUsernameLen=strlen(mdConfig->phonetUsername);
    LOG0close_return(!phonetUsernameLen,-1,releaseMdConfig(&mdConfig),"WARNING: url with key, but we do not have a username. Invalidating the key.");

    SHA1_Update(&sha1,mdConfig->phonetUsername,phonetUsernameLen);
    //second we add the nameCRCs
    if(keyLen-40)
        SHA1_Update(&sha1,key+40,keyLen-40);
    //third we add the unique name
    SHA1_Update(&sha1,mdConfig->salt,strlen(mdConfig->salt));

    releaseMdConfig(&mdConfig); //not needed anymore

    unsigned char sha1Data[SHA_DIGEST_LENGTH];
    SHA1_Final(sha1Data,&sha1);
    //print and check the key
    char computedKey[41];
    ph_assert(SHA_DIGEST_LENGTH==20,NULL);
    for(i=0;i<SHA_DIGEST_LENGTH;i++)
        sprintf(computedKey+i+i,"%02X",sha1Data[i]);
    LOG0return(strncmp(computedKey,key,40),-1,"WARNING: the computed key does not match the received key");

    //if we are here the two keys to match and we are happy :-). Extract the pre-ID from the key
    id->level=(keyLen-40)>>3;
    LOG0return(id->level>=7,-1,"WARNING: Illegal request (too many levels: %d).",id->level);
    LOG0return(id->level*8+40!=keyLen,-1,"WARNING: ID part of the key does not have proper length");

    for(i=0;i<id->level;i++){
        sscanf(key+40+i*8,"%08X",&(id->nameCRC[i]));
        LOG0("IDk: nameCRC[%d]=%08X",i,id->nameCRC[i]);
    }
    return 0;
}

static int getRequestId(char *reqUrl, char **reqQuery, struct md_entry_id * __restrict id, uint8_t * const userIndex){
    id->reqPerm=0;
    id->level=0;
    int reqUrlLen=strlen(reqUrl);
    char *queryStart=strchr(reqUrl,'?');
    if(queryStart){
        *queryStart='\0';
        queryStart++;
        reqUrlLen=strlen(reqUrl);

        if(reqQuery)
            *reqQuery=queryStart;

        //do we have a key?
        char *key=strstr(queryStart,"key=");
        if(key){
            key+=4;
            *userIndex=0xFF; //if parsing the key fails
            //if here, it means that the user tries to get to an element using key authentication
            char *extraQuery=strchr(key,'&');
            if(extraQuery)*extraQuery='\0';

            int r=parseKey(key,id);
            LOG0return(r,r,"WARNING: parsing ID failed");

            //if here, everything is fine and we have a valid key & everything
            *userIndex=0xFE;
            if(extraQuery)
                *extraQuery='&'; //putting it back
            if(id->level==0){
                LOG0("WARNING: the key is for the root album, invalidating it!!");
                *userIndex=0xFF;
            }
        } else {
            if(*userIndex==0xFE)
                *userIndex=0xFF;
        }
    } else {
        if(reqQuery)
            *reqQuery=NULL;
        if(*userIndex==0xFE)
            *userIndex=0xFF;
    }

    if(id->level==0 && *userIndex==0xFE)
        *userIndex=0xFF;

    //do we have an ID or something else?
    LOG0return(reqUrlLen<2 || reqUrl[0]!='/' || (reqUrlLen>=4 && reqUrl[2]!='/'),0,"INFO: command does not contain an ID");

    if((reqUrlLen>2 && reqUrl[1]!='f') || isFacebookUrl(reqUrl,reqUrlLen)){
        char *ptrStart=reqUrl+3,*ptrEnd;
        size_t len;
        //extract the path
        if(reqUrl[1]=='f')ptrStart+=2; //we skip the leading /f
        while(1){
            ptrEnd=strchr(ptrStart,'/');
            if(ptrEnd)ptrEnd[0]='\0';//so we can extract the number
            len=strlen(ptrStart);
            if(len==0)break;
            LOG0return(len!=8,-1,"WARNING: wrong ID length: %zu (ID: %s)",len,ptrStart);
            sscanf(ptrStart,"%08X",&(id->nameCRC[id->level]));

            //LOG0("ID : nameCRC[%d]=%08X",id->level,id->nameCRC[id->level]);

            id->level++;
            if(ptrEnd){
                ptrEnd[0]='/';//we put it back
                ptrStart=ptrEnd+1;
            } else break;
            LOG0return(id->level>=7,-1,"WARNING: Illegal request (too many levels: %d).",id->level);
        }
    }
    //if here, we have a (potentially) legal id
    LOG2("Requested ID is legal");

    return 1;
}

static void processImgLoadedMessage(const char *msg, struct ps_session *mySession){
    ASSERT_SESSION_LOCKED(mySession);
    //look for load time, thumb & pic watch time
    int loadTime=-1;
    uint32_t nameCRC=0;
    sscanf(msg,"loadID=%08X loadTime=%d",&nameCRC,&loadTime);
    if(loadTime!=-1 && nameCRC){
        uint32_t bitrate=imageTransferConfirmed(mySession,loadTime,nameCRC);
        sendBitrateToClient(mySession,bitrate);
    } else LOG0("Could not parse img loaded message: %s",msg);

}

static void processImgWatchedMessage(const char *msg, struct ps_session *mySession){
    ASSERT_SESSION_LOCKED(mySession);
    //look for load time, thumb & pic watch time
    int thumbWatchTime=-1,picWatchTime=-1;
    uint32_t nameCRC=0;
    sscanf(msg,"watchedID=%08X picWT=%d thmbWT=%d",&nameCRC,&picWatchTime,&thumbWatchTime);

    if(nameCRC/* && picWatchTime>0*/){
        if(picWatchTime<MIN_AVERAGE_VIEW_TIME)picWatchTime=MIN_AVERAGE_VIEW_TIME;
        if(picWatchTime>MAX_AVERAGE_VIEW_TIME)picWatchTime=MAX_AVERAGE_VIEW_TIME;
        if(mySession->averageViewTime)
            mySession->averageViewTime=(3*mySession->averageViewTime+picWatchTime)>>2;
        else
            mySession->averageViewTime=picWatchTime;
        ph_assert(mySession->averageViewTime>=MIN_AVERAGE_VIEW_TIME && mySession->averageViewTime<=MAX_AVERAGE_VIEW_TIME,NULL);
    } else LOG0("Could not parse img watched message: %s",msg);
}

static void processImgPrefetchMessage(const char *msg, struct ps_session const * const mySession){
    ASSERT_SESSION_LOCKED(mySession);
    int priority=-1; //priority can have values 1,2,3
    uint32_t nameCRC=0;
    sscanf(msg,"prefetchID=%08X priority=%d",&nameCRC,&priority);

    if(nameCRC && priority>0 && priority<4){
        struct md_entry_id prefetchId=mySession->lastRequestedAlbumID;
        prefetchId.nameCRC[prefetchId.level++]=nameCRC;
        prefetchImage(&prefetchId,priority,mySession);
    }
}

static void processRenderingTimeMessage(const char *msg, struct ps_session *mySession){
    ASSERT_SESSION_LOCKED(mySession);
    int renderingTime=0;
    sscanf(msg,"renderingTimePerMPixel=%d",&renderingTime);

    if(renderingTime>0){
        if(renderingTime>0xFFFF)renderingTime=0xFFFF; //this would be HUGE!!
        mySession->averageRenderingTimePerMPixel=renderingTime;
        LOG0(">>>>>>>>>>>>>>>> averageRenderingTimePerMPixel: %d",renderingTime);
    }
}


static int parsePart(char * const __restrict buf, const int bufLen, FILE **f, char **nameAfterChange){
    char *eol,*bufPos2;
    char *bufPos=buf;
    int l,ll,processedBytes;
    char *partMIME=NULL;
    char *filename=NULL;
    int r=0; //returned value
    //parse header fields
    while((eol=strchr(bufPos,'\n'))){
        *eol='\0';
        if(eol>bufPos && *(eol-1)=='\r'){
            eol--;
            *eol='\0';
        } else LOG0return(1,-1,"ERROR: bad request (no \\r found before \\n at the end of header field)");
        l=eol-bufPos;
        if(!l)break;//end of header parsing

        //split into header and value
        bufPos2=strchr(bufPos,':');
        LOG0return(!bufPos2,-1,"ERROR: bad request (malformed header: %s)",bufPos);
        *bufPos2='\0';
        bufPos2++;
        while(*bufPos2!='\0' && isspace(*bufPos2))bufPos2++;//skip white spaces

        LOG0("parsePart: Header: <<%s>> Value: <<%s>>",bufPos,bufPos2);

        //get the header fields we want
        if(!strcasecmp(bufPos,"Content-Disposition")){
            //content disposition should contain several things; a form-data, a name="file" and a filename="something". We need that "something" :-)
            LOG0return(!strstr(bufPos2,"form-data"),-1,"form-data NOT FOUND in part header.");
            if(strstr(bufPos2,"name=\"file\"")){
                const char *KFilenameHeader="filename=\"";
                filename=strstr(bufPos2,KFilenameHeader);
                LOG0return(!filename,-1,"filename= NOT FOUND in part header.");
                ll=0;
                filename+=strlen(KFilenameHeader);
                while(filename[ll]!='\0'){
                    if(filename[ll]=='"' && (ll==0 || filename[ll-1]!='\\')){
                        filename[ll]='\0';
                        break;
                    }
                    ll++;
                }
                ll=strlen(filename);
                LOG0("Our filename is: %s (length: %d)",filename,ll);
            } else
                LOG0return(!strstr(bufPos2,"name=\"cf\""),-1,
                           "neither name=\"file\" nor name=\"cf\" NOT FOUND in part header.");
        } else if(!strcasecmp(bufPos,"Content-Type")){
            partMIME=bufPos2;
            //trim spaces
            while(*partMIME==' ')partMIME++;
            ll=strlen(partMIME)-1;
            while(ll>=0 && partMIME[ll]==' ')partMIME[ll--]='\0';
        }
        //prepare for the next header field
        bufPos=eol+2;
    }
    processedBytes=bufPos+2-buf;

    if(filename){
        //LOG0("Our uploaded file has %d bytes.",bufLen-processedBytes);
        char path[PATH_MAX];
        struct ps_md_config const * const mdConfig=getMdConfigRd();
        int l=strlen(mdConfig->rootFolder),filenameLen=strlen(filename);
        memcpy(path,mdConfig->rootFolder,l);
        releaseMdConfig(&mdConfig);
        memcpy(path+l,KUploadsFolder,strlen(KUploadsFolder)+1);
        l=strlen(path);

        //add the filename
        if(path[l-1]!='/')
            path[l++]='/';
        memcpy(path+l,filename,filenameLen+1);
        l+=filenameLen;
        LOG0("Uploaded file should go to: %s",path);

        //does this file already exist and does it have a generic filename (without numbers?) if yes, then change the filename
        struct stat st;
        if(!stat(path,&st)){
            int i;
            for(i=0;i<filenameLen;i++)
                if(isdigit(filename[i]))break;
            if(i>=filenameLen){
                //we should change the filename by adding a number
                //first, make space for this number by moving the extension with 5 places
                LOG0("This file (%s) already exists, adding some numbers to it!",path);
                char *insertPoint=strrchr(path,'.');
                if(insertPoint){
                    memmove(insertPoint+5,insertPoint,strlen(insertPoint)+1);
                    for(i=0;i<99999;i++){
                        sprintf(insertPoint,"%05d",i);
                        insertPoint[5]='.';
                        LOG0("Trying %s",path);
                        if(stat(path,&st) && errno==ENOENT){
                            *nameAfterChange=ph_strdup(path);
                            break;
                        }
                    }
                }
            }
            LOG0("Uploaded file will go to: %s",path);
        }

        //create path if some folders are missing
        char *delimiter;
        int pos=0;
        while((delimiter=strchr(path+pos,'/'))){
            pos=(int)(delimiter-path);
            if(!pos){
                pos=1;
                continue;
            }

            ph_assert(path[pos]=='/',NULL);
            path[pos]='\0';
            //LOG0("Path here: %s",path);
            r=dirService(path,1);
            path[pos]='/';
            pos++;
        }
        r=0;//if here, path is fine

        //open the file
        *f=fopen(path,"w");
        if(*f){
            setOwnership(path,0);
            fwrite(buf+processedBytes,bufLen-processedBytes,1,*f);
            //fclose(f); -> do not close it here, we might have partial part
        }
    } else {
        ph_assert(bufLen-processedBytes>=0,NULL);
    }
    return r;
}

static int parsePostCommand(struct srvThreadData * __restrict d){
    //parse the thing
    struct ongoingPOST * __restrict op=d->op;

    LOG0("parsePostCommand++ (we have: %d bytes)",op->pos);

    //we can safely suppose that the entire command will fit into the buffer
    if(op->pos<op->contentLen)return 0;

    //if here, we have the entire content into our buffer
    ph_assert(op->pos==op->contentLen,NULL);
    op->buffer[op->pos]='\0';
    char * __restrict reqQueryStr=op->buffer;

    size_t cntBufLen=0;
    char *cntBuf=NULL;
    const char *cntType=NULL;
    int cntBufShouldBeFreed=1;
    int setCookie=0;

    switch(op->command){
    case CCGetSrvMetadata:{
        char *unStr=NULL;
        char *pwStr=NULL;

        LOG0("Request string: %s",reqQueryStr);
        if((unStr=strstr(reqQueryStr,"un="))){
            unStr+=3;
            //we add the FlagSrvThread_GetSrvMetadataExpectedToAuthorize flag, because if the password is wrong, we want to display it again
            d->flags|=FlagSrvThread_GetSrvMetadataExpectedToAuthorize;

            if((pwStr=strchr(unStr,'&'))){
                pwStr[0]='\0';
                pwStr+=4;

                if(strlen(pwStr)==(SHA256_DIGEST_LENGTH+SHA256_DIGEST_LENGTH)){
                    //the pwStr has the right length (64bytes in practice)
                    //check the received username and password against our array of usernames & passwords
                    int i;
                    struct ps_md_config const *mdConfig=getMdConfigRd();

                    //first, lowercase the received password
                    for(i=0;pwStr[i];i++)
                        pwStr[i]=tolower(pwStr[i]);

                    for(i=0;i<mdConfig->nrAuthUsers;i++){
                        if(strcasecmp(mdConfig->authUsers[i].username,unStr))continue;

                        //verify this password
                        LOG0("Checking: %s",pwStr);
                        uint32_t bcryptStart=LOG0("Against %s",mdConfig->authUsers[i].password);
                        if(!bcrypt_checkpass(pwStr,mdConfig->authUsers[i].password)){
                            //we found it!!!
                            uint32_t bcryptDelta=LOG0("Authentication succeeded. Creating a new session.")-bcryptStart;
                            LOG0("Checking (bcrypt) took %ums",(unsigned int)bcryptDelta);

                            //initialize my session
                            struct ps_session *mySession=getNewSession(d->mySessionId);

                            LOG0close_return(!mySession,-1,if(cntBufShouldBeFreed)free(cntBuf);sendError(d,503),
                                    "WARNING: could not create new session. Sending 503 Service Unavailable");
                            d->mySessionId=mySession->sessionId;
                            mySession->userIndex=d->userIndex=i;
                            mySession->clientIp=d->reqClientIp;
                            if(d->flags&FlagSrvThread_GetSrvMetadataExpectedToAuthorize)
                                d->flags&=~FlagSrvThread_GetSrvMetadataExpectedToAuthorize; //clear flag
                            unlockSession(&mySession);
                            setCookie=1;
                            break;
                        }
                        uint32_t bcryptDelta=getRelativeTimeMs()-bcryptStart;
                        LOG0("Checking (bcrypt) took %ums, AND WAS NOT SUCCESSFUL",(unsigned int)bcryptDelta);
                    }//for(i=...
                    LOG0c(i>=mdConfig->nrAuthUsers,"WARNING: NO AUTHENTICATION, WRONG USERNAME OR PASSWORD.");
                    releaseMdConfig(&mdConfig);
                }//if(right length)
            }//if(pw string exists)
        }//if(un string exists)

        //if we have not authorized this session, we send a small JSON object stating our displease
        if(d->flags&FlagSrvThread_GetSrvMetadataExpectedToAuthorize){
            LOG0("This GetSrvMetadata command did not authorize this session, answering forbidden with reason=-1.");

            cntBufLen=strlen(KJsonAnswerForbidden)+64;
            cntBuf=ph_malloc(cntBufLen);
            struct ps_md_config const *mdConfig=getMdConfigRd();
            cntBufLen=snprintf(cntBuf,cntBufLen,KJsonAnswerForbidden,-1,mdConfig->phonetUsername?mdConfig->phonetUsername:"");
            releaseMdConfig(&mdConfig);
        } else {
            //if we are here we have a right to be here.
            //if we do not have a session, lets generate one
            struct ps_session *mySession=NULL;
            if(!d->mySessionId){
                mySession=getNewSession(0);
                LOG0close_return(!mySession,-1,if(cntBufShouldBeFreed)free(cntBuf);sendError(d,503),
                        "WARNING: could not create new session. Sending 503 Service Unavailable");
                d->mySessionId=mySession->sessionId;
                mySession->userIndex=d->userIndex;
                mySession->clientIp=d->reqClientIp;
                setCookie=1;
            } else
                mySession=getSession(d->mySessionId,d->reqClientIp,1);
            ph_assert(mySession,"mySession is NULL");

            //now, do the metadata thingy
            //first, reset it, in case we do not find it
            mySession->flags&=~FlagSession_FromSmallDevice;
            mySession->screenWidth=0;
            mySession->screenHeight=0;
            mySession->pixelRatio=0;

            if(reqQueryStr){
                char *paramStr=NULL;
                unsigned int sscanfParam;

                //small device?
                if(strstr(reqQueryStr,"smallDevice=true"))
                    mySession->flags|=FlagSession_FromSmallDevice;

                //screen width & height
                if((paramStr=strstr(reqQueryStr,"screenWidth=")))
                    if(sscanf(paramStr,"screenWidth=%u",&sscanfParam))
                        if(sscanfParam<65536)mySession->screenWidth=sscanfParam;
                if((paramStr=strstr(reqQueryStr,"screenHeight=")))
                    if(sscanf(paramStr,"screenHeight=%u",&sscanfParam))
                        if(sscanfParam<65536)mySession->screenHeight=sscanfParam;

                //pixel ratio
                if((paramStr=strstr(reqQueryStr,"pixelRatio=")))
                    if(sscanf(paramStr,"pixelRatio=%u",&sscanfParam))
                        if(sscanfParam<4)mySession->pixelRatio=sscanfParam;
            } else
                LOG0("WARNING: There was no query string found for GetServerMetadata command. Resetting screen dimensions, pixelRatio and smallDevice values.");
            unlockSession(&mySession);

            //create a JSON string with the phonetUsername, the number of cameras and camera model strings
            cntBuf=getMetadataJsonString(&cntBufLen,0,0,d);
        }
        cntType=KHTTPTypeJson;
    };break;

    case CCConfigure:{
        LOG0("CONFIGURE Request string: %s",reqQueryStr);
        struct ps_md_config_received receivedConfig;
        char *paramStr=NULL,*eoStr; //end-of-string
        const char *errorMsg=NULL; //if this stays NULL, then everything is successfull
        unsigned int temp;
        int i,l,r,saveToFile=0;
        memset(&receivedConfig,0,sizeof(struct ps_md_config_received));
        //set defaults for the data that can be missing
        receivedConfig.permissionsAuthWithKey=DEFAULT_PERMISSIONS_AUTHWITHKEY;

        if(!reqQueryStr)errorMsg="Internal error: Empty request string";

        //parse the anonymous permissions
        if(!errorMsg && (paramStr=strstr(reqQueryStr,"anonymous=")))
            if(sscanf(paramStr,"anonymous=%u",&temp))
                if(temp<256)receivedConfig.permissionsAnonymous=temp;
                else errorMsg="Internal error: Anonymous permission value too big";
            else errorMsg="Internal error: Error scanning anonymous permissions";
        else errorMsg="Internal error: Cannot find anonymous permissions";

        //parse the authWithKey permissions
        if(!errorMsg && (paramStr=strstr(reqQueryStr,"authwithkey="))){
            if(sscanf(paramStr,"authwithkey=%u",&temp))
                if(temp<256)receivedConfig.permissionsAuthWithKey=temp;
                else errorMsg="Internal error: AuthWithKey permission value too big";
            else errorMsg="Internal error: Error scanning AuthWithKey permissions";
        } //else: this can be missing, no problem

        //parse the inet value
        if(!errorMsg && (paramStr=strstr(reqQueryStr,"inet=")))
            if(sscanf(paramStr,"inet=%u",&temp)){
                switch(temp){
                case 1:receivedConfig.cfgFlags|=CfgFlagInetAuthNotRequired;break;
                case 2:break; //this is the default
                case 3:receivedConfig.cfgFlags|=CfgFlagInetNotAllowed;break;
                default:errorMsg="Internal error: Unknown inet value";
                }
            } else errorMsg="Internal error: Error scanning inet";
        else errorMsg="Internal error: Cannot find inet";

        //parse the server name, if any
        if(!errorMsg && (paramStr=strstr(reqQueryStr,"srv="))){
            paramStr+=4;
            eoStr=strchr(paramStr,'&');
            if(eoStr)*eoStr='\0';

            //verify the server name
            l=strlen(paramStr);
            if(l>20)errorMsg="Internal error: Server name too big";
            if(!errorMsg){
                for(i=0;i<l;i++)
                    if(!isalnum(paramStr[i]) && paramStr[i]!='_'){
                        errorMsg="Internal error: Unsupported character in server name.";
                        break;
                    }
            }

            if(!errorMsg)
                receivedConfig.phonetUsername=ph_strdup(paramStr);
            if(eoStr)*eoStr='&';
        } //else: this can be missing, no problem

        //parse users
        paramStr=reqQueryStr;
        while(!errorMsg && ((paramStr=strstr(paramStr,"un=")))){
            paramStr+=3;
            eoStr=strchr(paramStr,'&');
            if(eoStr)*eoStr='\0';

            //verify the username
            l=strlen(paramStr);
            if(l>20)errorMsg="Internal error: Username too big";
            if(!errorMsg){
                for(i=0;i<l;i++)
                    if(!isalnum(paramStr[i]) && paramStr[i]!='_'){
                        errorMsg="Internal error: Unsupported character in username.";
                        break;
                    }
            }

            if(errorMsg)break;

            //if here, the username is valid. Create a new entry
            receivedConfig.nrAuthUsers++;
            receivedConfig.authUsers=(struct ps_auth*)ph_realloc(receivedConfig.authUsers,receivedConfig.nrAuthUsers*sizeof(struct ps_auth));
            struct ps_auth * __restrict c=&receivedConfig.authUsers[receivedConfig.nrAuthUsers-1];
            memset(c,0,sizeof(struct ps_auth));
            c->username=ph_strdup(paramStr);

            if(!eoStr)break; //nothing follows
            *eoStr='&';
            paramStr=eoStr+1;
            ph_assert(!errorMsg,NULL);


            //is there a password?
            if(!strncmp(paramStr,"pw=",3)){
                //looks like we have a password
                paramStr+=3;
                eoStr=strchr(paramStr,'&');
                if(eoStr)*eoStr='\0';

                if(strlen(paramStr)!=64){
                    errorMsg="Internal error: User password has wrong length";
                    break;
                }


                for(i=0;i<64;i++){
                    char pwc=paramStr[i];
                    if((pwc>='0' && pwc<='9') || (pwc>='a' && pwc<='f') || (pwc>='A' && pwc<='F'))
                        continue;
                    //if here, we have the wrong character
                    errorMsg="Internal error: Wrong character in user password";
                    break;
                }
                if(errorMsg)break;

                //if here, the password is good!
                c->password=(char*)ph_malloc(65);
                for(i=0;i<65;i++)
                    c->password[i]=tolower(paramStr[i]);

                paramStr+=64;
                if(eoStr){
                    *eoStr='&';
                    paramStr++;
                }
            }//done with the password

            //TODO: permissions
            c->permissions=255;
        }//while

        //done with the parsing
        if(!errorMsg)
            errorMsg=verifyUserSubmittedConfig(&receivedConfig);
        if(!errorMsg && receivedConfig.phonetUsername){
            //register the server

            //first, create the password as a SHA256 of salt+servername
            char pwUnhashed[100];
            struct ps_md_config const * const mdConfig=getMdConfigRd();

            //it doesn't matter how we generate this password/which salt we are using, because we save the password anyway
            snprintf(pwUnhashed,100,"%s%s",mdConfig->salt,receivedConfig.phonetUsername);
            releaseMdConfig(&mdConfig);
            unsigned char pwSha256[SHA256_DIGEST_LENGTH];
            char pwSha256Str[SHA256_DIGEST_LENGTH+SHA256_DIGEST_LENGTH+1];
            SHA256((unsigned char*)pwUnhashed,strlen(pwUnhashed),pwSha256);
            for(i=0;i<SHA256_DIGEST_LENGTH;i++)
                sprintf(pwSha256Str+i+i,"%02X",pwSha256[i]); //uppercase, because this is what the server/mysql generates

            //send request to photostovis.net
            r=phonetServerRegister(receivedConfig.phonetUsername,pwSha256Str);
            if(r==0){
                LOG0("We successfully registered this server (%s).",receivedConfig.phonetUsername);

                struct ps_md_config *mdConfigW=getMdConfigWritable();
                if(mdConfigW->phonetUsername){free(mdConfigW->phonetUsername);mdConfigW->phonetUsername=NULL;}
                if(mdConfigW->phonetPassword){free(mdConfigW->phonetPassword);mdConfigW->phonetPassword=NULL;}
                ph_assert(!mdConfigW->phonetUsername,NULL);
                ph_assert(!mdConfigW->phonetPassword,NULL);

                mdConfigW->phonetUsername=receivedConfig.phonetUsername;
                mdConfigW->phonetPassword=ph_strdup(pwSha256Str);
                receivedConfig.phonetUsername=NULL;

                releaseMdConfigW(&mdConfigW);
                phonetServerStart();
            } else {
                //server registration failed. Lets see why
                switch(r){
                //photostovis server errors:
                case PhonetCfgErrCurlInitFailed:
                    errorMsg="Internal error: CURL failed";break;
                case PhonetCfgErrCurlConnectionFailed:
                    errorMsg="Cannot connect to registration server (https://photostovis.net). Are you connected to Internet? Please try again later.";break;
                case PhonetCfgErrAnswerParsingFailed:
                    errorMsg="Internal error: parsing the answer from server failed";break;
                case PhonetCfgErrAnswerOutsideRange:
                    errorMsg="Internal error: the registration server returned an unknow error";break;
                //phonet errors (10, 13, 15)
                case 10:
                    errorMsg="Your Photostovis server is unreachable from Internet. Most probable reason: you are behind a firewall. More information about what you can do can be found <a href=\\\"https://photostovis.com/unreachable.html\\\" target=\\\"_blank\\\">here</a>";
                    saveToFile=1; //we save this configuration even if the server is unreachable
                    break;
                case 13:
                    errorMsg="Internal error: invalid photostovis.net request";break;
                case 15:
                    errorMsg="Internal error: there is another Photostovis server registered with the same name.";break;
                default:
                    errorMsg="Internal error: unknown";break;
                }
            }
        }
        if(!errorMsg)saveToFile=1;
        if(saveToFile){
            r=saveToConfigurationFile(&receivedConfig);
            if(r){
                errorMsg="Saving the configuration failed";
                free(cntBuf);
                cntBuf=NULL;
                cntBufLen=0;
            }
            else {
                receivedConfig.authUsers=NULL; //the pointer was transfered to mdConfig
                receivedConfig.nrAuthUsers=0;
            }
        }

        ph_assert(!cntBuf,NULL);
        if(errorMsg){
            LOG0("CCConfigure: ERROR HAPPENED: %s",errorMsg);
            cntBuf=ph_malloc(strlen(KJsonAnswerFalseMsg)+strlen(errorMsg)+10);
            cntBufLen=sprintf(cntBuf,KJsonAnswerFalseMsg,errorMsg);
        } else {
            cntBufShouldBeFreed=0;
            cntBuf=(char*)KJsonAnswerTrue;
            cntBufLen=strlen(cntBuf);
        }
        cntType=KHTTPTypeJson;

        //free data
        free(receivedConfig.phonetUsername);
        for(i=0;i<receivedConfig.nrAuthUsers;i++){
            free(receivedConfig.authUsers[i].username);
            free(receivedConfig.authUsers[i].password);
        }

        //send the new metadata information to this client
        struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,0);

        if(mySession){
            if(mySession->websocketPipe>=0){
                //if here, we have a websocket
                size_t cntBufLen;
                char *buffer=getMetadataJsonString(&cntBufLen,KWebsocketBufferOffset,0,d);
                enqueueWebsocketPacket(mySession,buffer,cntBufLen,KWebsocketBufferOffset,0,1);
                free(buffer);
            }
            unlockSession(&mySession);
        }
    };break;
    default:LOG0("Unknown command: %c",op->command);
    };//switch

    //free d->op, not needed after this point
    free(d->op);
    d->op=NULL;


    /* not used yet
    if(answerAlreadySent){
        ph_assert(!cntBuf,NULL);
        return 0;
    }*/

    int r,r2;
    unsigned int processingTime=getRelativeTimeMs()-d->processingTimeStart;
    char rspBuf[HTTP_RESPONSE_BUFFER_SIZE];
    char dateStr[100];
    getTimeString(0,dateStr,100);

    if(processingTime>3600000)//nothing should take one hour
        processingTime=0;

    if(setCookie){
        r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,KHTTPOkSetCookie,op->connectionType?KConnectionKeepAlive:KConnectionClose,
                   cntType,(uint64_t)cntBufLen,dateStr,KCookieName,d->mySessionId);
    } else
        r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,KHTTPOkNoETag,op->connectionType?KConnectionKeepAlive:KConnectionClose,cntType,(uint64_t)cntBufLen,dateStr);


    LOG0close_return(r<0,-1,if(cntBufShouldBeFreed)free(cntBuf);sendError(d,500),
            "ERROR error writing the HTTP response header to the buffer");
    LOG0close_return(r>=HTTP_RESPONSE_BUFFER_SIZE,-1,if(cntBufShouldBeFreed)free(cntBuf);sendError(d,500),
            "ERROR: Insufficient buffer size (%d) for the HTTP response header",HTTP_RESPONSE_BUFFER_SIZE);

    ph_assert(r==strlen(rspBuf),"%d!=%zu",r,strlen(rspBuf));
    r2=srvWrite(d,rspBuf,r);
    LOG0close_return(r2<0,-1,if(cntBufShouldBeFreed)free(cntBuf),
                     "ERROR writing the HTTP response header: %s",strerror(errno));
    LOG0close_return(r2!=r,-1,if(cntBufShouldBeFreed)free(cntBuf),
                     "ERROR writing the HTTP response header. Wrote %d bytes instead of %d",r2,r);

    //LOG0("Sent answer:\r\n%s",rspBuf);
    //send the content
    if(cntBuf){
        int nrZeroSends=0;
        LOG0("Sending the content (length: %zu)",cntBufLen);

#if BITRATE_LIMITED_TO
        //we artificially limit the bitrate to this number by waiting this ammount
        ph_assert(cntBufLen>0,NULL);
        //compute the time we need to sleep
        useconds_t usecs=(cntBufLen<<3)/BITRATE_LIMITED_TO;

        unsigned int sleepTime=LOG0("Bitrate forced to %dMbps. Sleeping %dus. ",BITRATE_LIMITED_TO,(int)usecs);
        usleep(usecs);
        sleepTime=getRelativeTimeMs()-sleepTime;
        LOG0("Done sleeping. Slept for %ums.",sleepTime);
#endif
        r=0;
        while(r<cntBufLen){
            r2=srvWrite(d,cntBuf+r,cntBufLen-r);
            LOG0close_return(r2<0,-1,if(cntBufShouldBeFreed)free(cntBuf),
                             "ERROR writing the HTTP response content: %s",strerror(errno));
            if(r2==0){
                nrZeroSends++;
                LOG0close_return(nrZeroSends>100,-1,if(cntBufShouldBeFreed)free(cntBuf),
                                 "ERROR writing the HTTP response content. send returned zero too many times (100).");
                usleep(100000); //100ms
            } else nrZeroSends=0;
            r+=r2;
        }
        ph_assert(r==cntBufLen,NULL);

        if(cntBufShouldBeFreed)free(cntBuf);
    }

    return 0;
}

static int parsePostUpload(struct srvThreadData * __restrict d){
    //parse the thing
    int r,r2;
    //struct md_entry_id id;
    struct ongoingPOST * __restrict op=d->op;
    //int partOffset=0; //position of the current part counted from the beginning of the buffer
    int partLen;
    char *partEnd=NULL;
    int currentBoundaryLen=op->boundaryLen;
    ph_assert(op->boundaryLen>0,NULL);
    struct ps_session *mySession=NULL;

    LOG0("parsePostUpload++ (we have %d bytes)",op->pos);
    ph_assert(d->mySessionId,"There is NO session!!"); //TODO: replace with a LOG message and a return

    //do we have a boundary?
    char *partStart=memmem(op->buffer,op->pos,op->boundary,op->boundaryLen);
    if(!partStart){
        LOG0("parsedLen=%u, pos=%u, contentLen=%u",(uint32_t)op->parsedLen,(uint32_t)op->pos,(uint32_t)op->contentLen);
        if(!op->f && op->pos<DEFAULT_HTTP_BUFFER_SIZE && op->parsedLen+op->pos<op->contentLen)
            return 0; //lets make sure there is enough data to be able to parse something


        //we should have a file, then, and we should dump almost all of this data into that file
        LOG0close_return(!op->f,-1,sendError(d,400);free(op->boundary);free(d->op);d->op=NULL,
                "Answering 400 Bad Request: No boundary found in POST part (%d bytes), no file open either.",op->pos);
        //if here, we have a file and we dump data there
        currentBoundaryLen=op->boundaryLen+4; //+2 in front (\r\n, +2 after)
        LOG0("Writing %d bytes into the file.",op->pos-currentBoundaryLen);
        ph_assert(op->pos-currentBoundaryLen>=0,NULL);
        fwrite(op->buffer,op->pos-currentBoundaryLen,1,op->f);
        //move the boundary-sized data in the beginning
        memmove(op->buffer,op->buffer+op->pos-currentBoundaryLen,currentBoundaryLen);
        op->parsedLen+=op->pos-currentBoundaryLen;
        op->pos=currentBoundaryLen;

        //is this a long transfer? If yes, send the bitrate to client

        uint32_t endTime=getRelativeTimeMs();
        if(endTime-op->lastBitrateComputeTime>=1000){
            //recompute bitrate and send it to client
            mySession=getSession(d->mySessionId,d->reqClientIp,1);
            uint32_t bitrate=addUploadedData(mySession,op->uploadStartTime,endTime,op->parsedLen-op->lastBitrateComputeData,0);
            LOG0("Sending bitrate to client: %u",(unsigned int)bitrate);
            sendBitrateToClient(mySession,bitrate);
            unlockSession(&mySession);
            op->lastBitrateComputeTime=endTime;
            op->lastBitrateComputeData=op->parsedLen;
            LOG_FLUSH;
        }

        return 0;
    }

    //if we are here, we have a partStart
    LOG0("If we are here, we have a partStart");
    //check for \r\n before
    if((int)(partStart-op->buffer) >=2 && *(partStart-1)=='\n' && *(partStart-2)=='\r'){
        partStart-=2;
        currentBoundaryLen+=2;
    }

    //are we finishing the file?
    if(op->f){
        LOG0("We are finishing the file");
        //yes, we are
        fwrite(op->buffer,(int)(partStart-op->buffer),1,op->f);
        fclose(op->f);
        op->f=NULL;
        op->parsedLen+=(int)(partStart-op->buffer);
        //move the remaining data in the beginning
        memmove(op->buffer,partStart,op->pos-(int)(partStart-op->buffer));
        op->pos-=(int)(partStart-op->buffer);
        partStart=op->buffer;

        //was this a renamed file? If yes, change the name to a date-based name
        if(op->nameAfterChange){
            changePictureName(op->nameAfterChange);
            free(op->nameAfterChange);
            op->nameAfterChange=NULL;
        }

        //do we still have data to read? Because if we do, we better return to read it!
        int missingData=(int)((int64_t)op->contentLen-(int64_t)op->parsedLen-(int64_t)op->pos);
        ph_assert(missingData>=0,NULL);
        if(missingData>0){
            ph_assert(missingData<DEFAULT_HTTP_BUFFER_SIZE,NULL);
            return 0;
        }
    }

    //is this the last boundary?
    if((int)(partStart-op->buffer)+2<=op->pos && partStart[currentBoundaryLen]=='-' && partStart[currentBoundaryLen+1]=='-'){
        //yes, this is our last boundary. We ignore everything that comes after this boundary and send the answer now.
        ph_assert(op->f==NULL,NULL);
        //ph_assert(partStart==op->buffer,NULL);
        currentBoundaryLen+=2;

        //we should not have any missing/unparsed data
        ph_assert(op->parsedLen+op->pos==op->contentLen,
                  "ERROR: parsed (%"UINT64_FMT") + current data (%d) != content len (%"UINT64_FMT"). Difference: %d",
                  op->parsedLen,op->pos,op->contentLen,(int)((int64_t)op->contentLen-(int64_t)op->parsedLen-(int64_t)op->pos));


        uint32_t endTime=getRelativeTimeMs();
        //recompute bitrate
        LOG0("Upload at the end: contentLen=%u, lastBitrteComputedData=%u, difference=%d",
             (unsigned int)op->contentLen,(unsigned int)op->lastBitrateComputeData,(int)op->contentLen-(int)op->lastBitrateComputeData);
        mySession=getSession(d->mySessionId,d->reqClientIp,1);
        uint32_t bitrate=addUploadedData(mySession,op->uploadStartTime,endTime,op->contentLen-op->lastBitrateComputeData,1);
        unlockSession(&mySession);

        //send an answer to the client
        char dateStr[100];
        getTimeString(0,dateStr,100);
        char rspBuf[HTTP_RESPONSE_BUFFER_SIZE];
        r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,KHTTPOkNoETag,op->connectionType?KConnectionKeepAlive:KConnectionClose,
                   KHTTPTypeHtml,(uint64_t)0,dateStr);

        //free and nullify d->op (not needed after this point)
        free(op->boundary);
        free(op);
        d->op=op=NULL;

        LOG0close_return(r<0,-1,sendError(d,500),"ERROR writing the HTTP response header to the buffer");
        LOG0close_return(r>=HTTP_RESPONSE_BUFFER_SIZE,-1,sendError(d,500),
                         "ERROR: Insufficient buffer size (%d) for the HTTP response header",HTTP_RESPONSE_BUFFER_SIZE);
        ph_assert(r==strlen(rspBuf),"%d!=%zu",r,strlen(rspBuf));
        r2=srvWrite(d,rspBuf,r);
        LOG0return(r2<0,-1,"ERROR writing the HTTP response header: %s",strerror(errno));
        LOG0return(r2!=r,-1,"ERROR writing the HTTP response header. Wrote %d bytes instead of %d",r2,r);
        //LOG0("Sent answer:\r\n%s",rspBuf);

        //now send bitrate to client
        mySession=getSession(d->mySessionId,d->reqClientIp,1);
        sendBitrateToClient(mySession,bitrate);
        unlockSession(&mySession);

        return 0;
    }

    //if we are here, we should be at the beginning of the file
    ph_assert(!op->f,NULL);
    ph_assert(op->parsedLen==0,"parsedLen is not 0 but %"UINT64_FMT,op->parsedLen);

    LOG0("We are at the beginning of the file");

    if(op->pos<DEFAULT_HTTP_BUFFER_SIZE && op->pos<op->contentLen)
        return 0; //lets make sure there is enough data
    LOG0("We have enough data");

    //if we are here, we have enough data. We are in the beginning of the content (otherwise the op->parsedLen==0 assert fails)
    //check if we have \r\n after the boundary
    if((int)(partStart-op->buffer)+currentBoundaryLen+2 < op->pos &&
            *(partStart+currentBoundaryLen)=='\r' && *(partStart+currentBoundaryLen+1)=='\n'){
        currentBoundaryLen+=2;
        LOG0("we have \\r\\n after boundary");
    }

    partStart+=currentBoundaryLen;
    ph_assert(partStart>=op->buffer,NULL);
    op->parsedLen=(uint64_t)(partStart-op->buffer);

    //we also need to find a partEnd
    partEnd=memmem(partStart,op->pos-op->parsedLen,op->boundary,op->boundaryLen);
    if(partEnd){
        LOG0("Boundary found after %d bytes",(int)(partEnd-partStart));

        //there should be \r\n before partEnd
        LOG0close_return((int)(partEnd-partStart)<2,-1,
                         sendError(d,400);free(op->boundary);free(d->op);d->op=NULL,
                         "Answering 400 Bad Request: Start part too small!");
        LOG0close_return(*(partEnd-1)!='\n' || *(partEnd-2)!='\r',-1,
                         sendError(d,400);free(op->boundary);free(d->op);d->op=NULL,
                         "Answering 400 Bad Request: Boundary does NOT start on a new line");
        partEnd-=2;
        partLen=(int)(partEnd-partStart);
        currentBoundaryLen=op->boundaryLen+2;
    } else {
        partLen=op->pos-op->parsedLen-op->boundaryLen-6;
        ph_assert(partLen>0,"partLen=%d",partLen);
        currentBoundaryLen=0;
    }

    //parsing this part
    LOG0("Parsing part with length: %d starting from position %"UINT64_FMT,partLen,op->parsedLen);
    char c=partStart[partLen];
    partStart[partLen]='\0';

    r=parsePart(partStart,partLen,&op->f,&op->nameAfterChange);
    partStart[partLen]=c;
    LOG0close_return(r,-1,
                     sendError(d,400);free(op->boundary);free(d->op);d->op=NULL,
                     "Answering 400 Bad Request: parsing the first part failed");
    op->parsedLen+=partLen;
    //remove the parsed part from the file
    memmove(op->buffer,op->buffer+op->parsedLen,op->pos-op->parsedLen);
    op->pos-=op->parsedLen;

    if(currentBoundaryLen){
        LOG0("It looks that we already have the last part in this buffer, calling this function (parsePostUpload) again.");
        return parsePostUpload(d);
    }

    return 0;
}

int srvContinuePostRequest(struct srvThreadData * __restrict d, char * const __restrict buf, const int bufLen){
    ph_assert(d->op,"d->op is NULL");
    ph_assert(bufLen<DEFAULT_HTTP_BUFFER_SIZE,NULL);
    ph_assert(d->op->pos<DEFAULT_HTTP_BUFFER_SIZE,NULL);
    /*LOG0("bufLen=%d, pos=%d, parsedLen=%d, their sum: %d, contnetnLen=%d",bufLen,d->op->pos,(int)d->op->parsedLen,
         bufLen+d->op->pos+(int)d->op->parsedLen,(int)d->op->contentLen);*/
    LOG0close_return(bufLen+d->op->pos+d->op->parsedLen > d->op->contentLen,-1,
                     sendError(d,400);if(d->op->boundaryLen){free(d->op->boundary);fclose(d->op->f);}free(d->op);d->op=NULL,
                     "Answering 400 Bad Request: Current packet contains more data than just the POST message/content. Not handled yet. ");

    memcpy(d->op->buffer+d->op->pos,buf,bufLen);
    d->op->pos+=bufLen;
    int r;
    if(d->op->boundaryLen)r=parsePostUpload(d);
    else r=parsePostCommand(d);
    if(r){
        ph_assert(r<0,NULL);
        return r;
    }
    return bufLen;
}

static int parseSession(struct srvThreadData * __restrict d, char * const __restrict sessionIdTxt){
    uint64_t sessionId=0;
    int r=sscanf(sessionIdTxt,"%"UINT64X_FMT,&sessionId);
    LOG0c(r!=1,"WARNING: parsing the session string (%s) failed!",sessionIdTxt);
    LOG0c(r==1,"We found a session Id: %016"UINT64X_FMT" (parsed text: %s)",sessionId,sessionIdTxt);
    if(r==1 && sessionId>0){
        //we should find this session
        struct ps_session *mySession=getSession(sessionId,d->reqClientIp,0);
        if(!mySession){
            LOG0("WARNING: session with ID=%016"UINT64X_FMT" could not be found!",sessionId);
        } else {
            //is this session different from what we had before?
            if(d->mySessionId && sessionId!=d->mySessionId){
                //we have a different session from what we had before
                LOG0("!!!!!!!!!! INFO: we have a different session now from what we had before");
                struct ps_session *oldSession=getSession(d->mySessionId,d->reqClientIp,0);
                if(oldSession){
                    ph_assert(oldSession->nrConnectedThreads>0,NULL);
                    oldSession->nrConnectedThreads--;
                    unlockSession(&oldSession);
                }

                mySession->nrConnectedThreads++;
                d->mySessionId=sessionId;
            } else if(!d->mySessionId){
                mySession->nrConnectedThreads++;
                d->mySessionId=sessionId;
            } //else the two sessions are the same, and we do nothing!
            ph_assert(mySession->nrConnectedThreads<MAX_THREADS_PER_SESSION,"nrConnectedThreads too big: %u",(unsigned int)mySession->nrConnectedThreads);
            d->userIndex=mySession->userIndex;

            LOG0("We have a session! (userIndex: %u)",(unsigned)d->userIndex);
            unlockSession(&mySession);
        }
    }
    return 0;
}




static int srvProcessRequest(struct srvThreadData * __restrict d, char * const __restrict buf, const int bufLen, int *hddAccessed, char** const websocketAccept){
    enum {
        RequestFlag_httpKeepAlive=1,
        RequestFlag_acceptsGzip=2,
        RequestFlag_answerAlreadySent=4,
        RequestFlag_pingRequest=8
    };

    char *bufPos=buf,*bufPos2; //we can increment these
    char *eol;
    int r,r2,l,processedBytes,requestIsAuthorized=0; //by default the request is NOT authorized
    enum HttpCommands reqCmd=0;
    unsigned int websocketRequest=0,reqUrlLen;
    char *reqUrl=NULL,*reqUA=NULL,*reqETag=NULL,*reqContentType=NULL;
    struct ps_md_config const * mdConfig=NULL;
    uint64_t reqContentLength=0;
    uint64_t rangeStart=(uint64_t)-1,rangeEnd=(uint64_t)-1;
    unsigned int processingTime=0;
    uint8_t requestFlags=RequestFlag_httpKeepAlive; //see the enums below

    ph_assert(!(*websocketAccept),NULL);
    d->processingTimeStart=getRelativeTimeMs();
    //can we find \\r\\n in this request?
    LOG0return(!strstr(buf,"\r\n\r\n"),0,"INFO: incomplete HTTP request.");

    LOG0("Processing request totalling %d bytes.",bufLen);
    //parse the HTTP request
    //first, remove all space characters in front
    while(*bufPos!='\0' && isspace(*bufPos))bufPos++;
    LOG0return(*bufPos=='\0',-1,"ERROR: invalid request (%s)",buf);

    //parse the HTTP command line
    eol=strchr(bufPos,'\n');
    LOG0return(!eol,-1,"ERROR: bad request (no \\n found in request)");
    *eol='\0';
    l=eol-bufPos;
    if(eol>bufPos && *(eol-1)=='\r')
        *(eol-1)='\0';
    LOG0return(l<=1,-1,"ERROR: bad request (command line has zero length)");

    bufPos2=strchr(bufPos,' ');
    LOG0return(!bufPos2,-1,"ERROR: bad request (malformed command line - no command)");
    *bufPos2='\0';
    if(!strcmp(bufPos,"GET"))reqCmd=HttpGet;
    else if(!strcmp(bufPos,"POST"))reqCmd=HttpPost;
    else if(!strcmp(bufPos,"OPTIONS"))reqCmd=HttpOptions;
    else if(!strcmp(bufPos,"HEAD"))reqCmd=HttpHead;
    else //unsupported command
        LOG0close_return(1,-1,sendError(d,501),"ERROR: unsupported command: %s",bufPos);
    bufPos=bufPos2+1;
    bufPos2=strchr(bufPos,' ');
    LOG0return(!bufPos2,-1,"ERROR: bad request (malformed command line - no URL)");
    *bufPos2='\0';
    reqUrl=bufPos;
    //LOG0("URL: %s",reqUrl);
    ph_assert_debug(!strchr(reqUrl,'#'),"ERROR: # found inside the request URL: %s",reqUrl);

    //verify that the command line ends with HTTP/
    bufPos=bufPos2+1;
    LOG0return(strncmp("HTTP/",bufPos,5),-1,"ERROR: bad request (malformed command line - HTTP/x.y)");
    LOG0return(bufPos[6]!='.',-1,"ERROR: bad request (malformed command line - HTTP/x?y)");
    /*httpVersion*/r=(bufPos[5]-'0')*10+(bufPos[7]-'0');
    LOG0close_return(/*httpVersion*/r!=11,-1,sendError(d,505),"ERROR: unsupported HTTP version: %d",/*httpVersion*/r);
    bufPos=eol+1;

    //parse header fields
    while((eol=strchr(bufPos,'\n'))){
        *eol='\0';
        if(eol>bufPos && *(eol-1)=='\r'){
            eol--;
            *eol='\0';
        } else LOG0return(1,-1,"ERROR: bad request (no \\r found before \\n at the end of header field)");
        l=eol-bufPos;
        if(!l)break;//end of header parsing

        //split into header and value
        bufPos2=strchr(bufPos,':');
        LOG0return(!bufPos2,-1,"ERROR: bad request (malformed header: %s)",bufPos);
        *bufPos2='\0';
        bufPos2++;
        while(*bufPos2!='\0' && isspace(*bufPos2))bufPos2++;//skip white spaces

        //get the header fields we want
        if(!strcasecmp(bufPos,"Connection")){
            if(strcasestr(bufPos2,"close"))requestFlags&=~RequestFlag_httpKeepAlive;
            if(strcasestr(bufPos2,"keep-alive"))requestFlags|=RequestFlag_httpKeepAlive;
            if(strcasestr(bufPos2,"Upgrade"))websocketRequest|=WebsocketConnectionUpgrade;
        } else if(!strcasecmp(bufPos,"User-Agent")){
            reqUA=bufPos2;
        } else if(!strcasecmp(bufPos,"If-None-Match") || !strcasecmp(bufPos,"If-Range")){
            //TODO: If-Range can contain DATE, which is not handled
            reqETag=bufPos2;
            //get rid of "
            if(reqETag[0]=='"'){
                reqETag++;
                //find the closing "
                bufPos2=strrchr(reqETag,'"');
                if(bufPos2)*bufPos2='\0';
            }
            LOG0("Requested ETag: %s (%zu - %d)",reqETag,strlen(reqETag),(int)reqETag[0]);
        } else if(!strcasecmp(bufPos,"Range")){
            char *s;
            const char *KRangeBytes="bytes=";
            int l1=strlen(KRangeBytes);
            long long result;
            LOG0("Range header: %s",bufPos2);
            if(!strncasecmp(bufPos2,KRangeBytes,l1)){
                result=strtoll(bufPos2+l1,&s,10);
                if((result==0 && errno==EINVAL) || result<0){
                    LOG0("WARNING: invalid start of range in Range header: %s: %s",bufPos,bufPos2);
                } else if(s[0]=='-'){
                    rangeStart=(uint64_t)result;

                    s++;//jump over the - character

                    if(*s){
                        result=strtoll(s,NULL,10);
                        if((result==0 && errno==EINVAL) || result<0){
                            //invalid or no end range
                            rangeEnd=(uint64_t)-1;
                        } else
                            rangeEnd=(uint64_t)result;
                    } else
                        rangeEnd=(uint64_t)-1;
                }
            }
        } else if(!strcasecmp(bufPos,"Content-Length")){
            char *s;
            long long result=strtoll(bufPos2,&s,10);
            //TODO: replace the asserts with error messages
            ph_assert(strlen(s)==0,NULL);
            ph_assert(result>=0,NULL);
            reqContentLength=result;
            LOG0("Content Length: %"UINT64_FMT,reqContentLength);
        } else if(!strcasecmp(bufPos,"Content-Type")){
            reqContentType=bufPos2;
        } else if(!strcasecmp(bufPos,"Accept-Encoding")){
            if(strstr(bufPos2,"gzip"))
                requestFlags|=RequestFlag_acceptsGzip;
        } else if(!strcasecmp(bufPos,"Upgrade")){
            if(!strcasecmp(bufPos2,"websocket"))websocketRequest|=WebsocketUpgradeWebsocket;
            else LOG0return(1,-1,"ERROR: invalid/unknown value for Upgrade header: %s",bufPos2);
        } else if(!strcasecmp(bufPos,"Sec-WebSocket-Key")){
            websocketRequest|=WebsocketKey;
            const char *wsGUID="258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
            int ll1=strlen(bufPos2);
            int ll2=strlen(wsGUID);
            char *wsData=ph_malloc(ll1+ll2+1);
            memcpy(wsData,bufPos2,ll1);
            memcpy(wsData+ll1,wsGUID,ll2+1);
            unsigned char sha1Data[SHA_DIGEST_LENGTH];
            SHA1((unsigned char*)wsData,ll1+ll2,sha1Data);
            free(wsData);
            size_t websocketAcceptLen;
            *websocketAccept=base64_encode(sha1Data,SHA_DIGEST_LENGTH,&websocketAcceptLen);
            LOG0("Sec-WebSocket-Accept: %s (%zu)",*websocketAccept,websocketAcceptLen);
        } else if(!strcasecmp(bufPos,"Sec-WebSocket-Protocol")){
            if(!strcasecmp(bufPos2,"status.photostovis.net"))websocketRequest|=WebsocketProtocol;
            else LOG0return(1,-1,"ERROR: unknown value for Sec-WebSocket-Protocol header: %s. We only support \"status\"",bufPos2);
        } else if(!strcasecmp(bufPos,"Sec-WebSocket-Version")){
            websocketRequest|=WebsocketVersion;
            //the version must be 13, but I see no point in checking it
        } else if(!strcasecmp(bufPos,"Cookie")){
            char *sessionIdTxt;
            if((sessionIdTxt=strstr(bufPos2,KCookieName))){
                //we have a session cookie
                sessionIdTxt+=strlen(KCookieName)+1;
                while(*sessionIdTxt==' ' || *sessionIdTxt=='=')
                    sessionIdTxt++;

                //scan the text
                parseSession(d,sessionIdTxt);
            }//if(strstr(bufPos2,KCookieName))
        }
        //LOG0("Header field: %s, value: %s",bufPos,bufPos2);

        //prepare for the next header field
        bufPos=eol+2;
    }//while(...
    processedBytes=bufPos+2-buf;

    if(!reqUA || strcmp(reqUA,KPhotostovisDotNet))
        ledClientActivity(LCOn,0);

    //prepare and verify some common things
    char dateStr[100];
    getTimeString(0,dateStr,100);
    struct md_entry_id id;
    char *reqQueryStr=NULL;

    LOG0("request URL before processing: %s",reqUrl);
    //check the request URL for an ID. The request could be authorized by a key as well
    int requestIdResult=getRequestId(reqUrl,&reqQueryStr,&id,&d->userIndex);
    LOG0close_return(requestIdResult==-1,-1,sendError(d,404),"WARNING: wrong/unexisting ID requested. Answering 404.");
    LOG0("requestIdResult: %d",requestIdResult);

    reqUrlLen=strlen(reqUrl);
    //check if the query string contains a valid sid
    if(reqQueryStr){
        char *sessionIdTxt;
        if((sessionIdTxt=strstr(reqQueryStr,"sid="))){
            sessionIdTxt+=4;

            //scan the text
            parseSession(d,sessionIdTxt);
        }
    }//if(reqQueryStr)


    ph_assert(d->userIndex!=0xFE || id.level>0,"Level is zero for key-based authentication!");

    //now we can look for authorization of this command
    requestIsAuthorized=authorizeRequest(RequestView,d); //if View is not authorized for this user, no other command is

    LOG0("requestIsAuthorized=%d",requestIsAuthorized);
    if(requestIsAuthorized<0){
        //we want to add some exceptions! Meaning that we want to allow non-authorized access to GET file commands

        //We want to allow non-authorized access to all GET file commands, so that we can display the "forbidden" page :-)
        if(/*requestIsAuthorized>-10 && */reqCmd==HttpGet && !requestIdResult){
            requestIsAuthorized=1;
            LOG0("Authorizing static file request.");
        }
        //we also want to allow POST CCGetSrvMetadata command/request
        if(requestIsAuthorized>-10 && reqCmd==HttpPost && reqUrlLen==2 && reqUrl[1]==CCGetSrvMetadata){
            requestIsAuthorized=1;
            d->flags|=FlagSrvThread_GetSrvMetadataExpectedToAuthorize;
            LOG0("GetSrvMetadata command expected to authorize.");
        }
    }
    //we also identify and authorize PING commands coming from photostovis.net
    if(reqUA && !strcmp(reqUA,KPhotostovisDotNet) && reqUrlLen==2 && reqUrl[1]==CCPing && d->flags&FlagSrvThread_ComingFromPhotostovisDotNet){
        LOG0("Authorizing photostovis.net PING request");
        requestIsAuthorized=1;
        requestFlags|=RequestFlag_pingRequest;
    }

    if(requestIsAuthorized<0){
        //this request is not authorized. We send 403 Forbidden
        char buf[KErrorSendBufferLength];
        int l;

        if((reqCmd==HttpPost && reqUrlLen==2 && reqUrl[1]==CCGetSrvMetadata) ||
           (reqCmd==HttpGet && reqUrlLen>1 && reqUrl[1]==CCGetAlbum)){
            unsigned int port=0;
            char buf2[KErrorSendBufferLength];
            mdConfig=getMdConfigRd();
            if(requestIsAuthorized==-12)
                port=mdConfig->httpsPort;//we need the port
            l=snprintf(buf2,KErrorSendBufferLength,KHTTPOkWithForbiddenAndReason,dateStr,requestIsAuthorized,port,d->flags&FlagSession_ComingFromLAN?1:0,
                       mdConfig->phonetUsername?mdConfig->phonetUsername:"");
            releaseMdConfig(&mdConfig);
            //we need to fill in the Content-Length
            char *contentStart=strstr(buf2,"\r\n\r\n");
            ph_assert(contentStart,NULL);
            l=strlen(contentStart+4);
            l=snprintf(buf,KErrorSendBufferLength,buf2,l);

            LOG0("This request (either GetSrvMetadata or GetAlbum) is not authorized. Nicely answering a 'forbidden' JSON");
        } else {
            l=snprintf(buf,KErrorSendBufferLength,KHTTP403WithReason,dateStr,requestIsAuthorized);
            LOG0("This request is not authorized, answering 403 Forbidden.");
        }
        srvWrite(d,buf,l);

        return processedBytes;
    }


    //we want our HDD spinning NOW, unless this is a CCPing command
    if(reqUrlLen!=2 || reqUrl[1]!=CCPing){
        *hddAccessed=1;
    }

    //TODO: implement OPTIONS and possibly HEAD
    LOG0close_return(reqCmd!=HttpPost && reqCmd!=HttpGet,-1,sendError(d,501),"WARNING: command (other than GET and POST) not implemented (although we should ...)");

    if(reqCmd==HttpPost){
        LOG0("Received POST request (IP: %s URL: %s, UA: %s)",d->ipString,reqUrl,reqUA?reqUA:"(unknown)");

        LOG0close_return(!reqContentType,-1,sendError(d,400),"Answering 400 Bad Request: no content type header");
        LOG0close_return(!reqContentLength,-1,sendError(d,400),"Answering 400 Bad Request: bad content length (0)");

        ph_assert(!d->op,NULL);

        //verifying the content
        bufPos+=2; //skipping the last \\r\\n
        LOG0("Content length in buffer: %zu",strlen(bufPos));
        LOG0("Content length sent by the server: %d",(int)reqContentLength);
        LOG0("Content type: %s",reqContentType);

        if(requestIdResult==1 && reqUrlLen==2 && reqUrl[1]==CCUpload)
            requestIdResult=0; //this is an upload command

        //parse this POST command (we do not answer in this function)
        if(requestIdResult==1){
            //this is a command. Check some things

            //LOG0("Our buffer: <<%s>>",bufPos);
            LOG0close_return(
                        reqUrl[1]!=CCGetSrvMetadata &&
                        reqUrl[1]!=CCConfigure,-1,sendError(d,404),
                    "WARNING: POST: unknown command from client: %c",reqUrl[1]);

            /* this is not always set right by the browser
            //our content should be of type "application/x-www-form-urlencoded"
            LOG0close_return(!strstr(reqContentType,"application/x-www-form-urlencoded"),-1,sendError(d,400),
                             "Answering 400 Bad Request: Content-Type is not application/x-www-form-urlencoded: %s",reqContentType);
            */
            LOG0close_return(reqContentLength>=DEFAULT_HTTP_BUFFER_SIZE,-1,sendError(d,400),
                             "Answering 400 Bad Request: Content-Length too big: %d. Max: %d",(int)reqContentLength,DEFAULT_HTTP_BUFFER_SIZE);
        } else {
            //this is a file upload. Check some things

            //our content should be of type "multipart/form-data"
            LOG0close_return(!strstr(reqContentType,"multipart/form-data"),-1,sendError(d,400),
                             "Answering 400 Bad Request: Content-Type is not multipart/form-data: %s",reqContentType);

            LOG0close_return(!d->mySessionId,-1,sendError(d,403),"WARNING: mySession is NULL for file upload POST command. Answering 403 Forbidden.");

            LOG0close_return(!authorizeRequest(RequestUpload,d),-1,sendError(d,403),"Unauthorized POST file upload command received. Answering 403 Forbidden.");
        }

        //common part: allocate the op structure and fill the common part
        struct ongoingPOST * __restrict op;
        d->op=op=(struct ongoingPOST*)ph_malloc(sizeof(struct ongoingPOST));
        op->contentLen=reqContentLength;
        op->parsedLen=0;
        op->pos=0;
        op->connectionType=requestFlags&RequestFlag_httpKeepAlive;


        if(requestIdResult==1){
            op->boundaryLen=0;
            op->command=reqUrl[1];
        } else {
            op->f=NULL;
            op->nameAfterChange=NULL;
            op->uploadStartTime=op->lastBitrateComputeTime=getRelativeTimeMs();
            op->lastBitrateComputeData=0;
            //we should find a "boundary=" in our "multipart/form-data" content-type
            const char *KBoundaryHead="boundary=";
            char *boundaryStart=strstr(reqContentType,KBoundaryHead);

            LOG0close_return(!boundaryStart,-1,sendError(d,400);free(op);d->op=NULL,
                    "Answering 400 Bad Request: No boundary found in Content-Type Header: %s",reqContentType);

            boundaryStart+=strlen(KBoundaryHead);
            op->boundaryLen=strlen(boundaryStart);
            op->boundary=ph_malloc(op->boundaryLen+3); //+2+1
            op->boundary[0]='-';
            op->boundary[1]='-';
            memcpy(op->boundary+2,boundaryStart,op->boundaryLen);
            op->boundaryLen+=2;
            op->boundary[op->boundaryLen]='\0';
        }

        LOG0("POST command (headers) parsed successfully.");
        return processedBytes;
    } //end of POST processing

    //if we are here, our command is "GET"
    ph_assert(reqCmd==HttpGet,NULL);


    //prepare our answer
    if(requestIdResult==1){
        size_t cntBufLen=0;
        char *cntBuf=NULL;
        const char *cntType=NULL;
        int cntBufShouldBeFreed=1;

        const int eTagSize=PHOTOSTOVIS_ETAGF_SIZE;
        char eTag[eTagSize];
        eTag[0]='\0';

        LOG0("Answering GET request (IP: %s URL: %s (query: %s), UA: %s)",d->ipString,reqUrl,reqQueryStr?reqQueryStr:"(none)",reqUA?reqUA:"(unknown)");
        LOG0close_return(reqUrl[1]!=CCGetAlbum &&
                reqUrl[1]!=CCSetAlbum &&
                reqUrl[1]!=CCGetThumbnail &&
                reqUrl[1]!=CCGetImage &&
                reqUrl[1]!=CCGetVideo &&
                reqUrl[1]!=CCGetUrlToAlbum &&
                reqUrl[1]!=CCDownladAsZipInfo &&
                reqUrl[1]!=CCDownloadAsZip &&
                reqUrl[1]!=CCPing &&
                reqUrl[1]!=CCShutdown &&
                reqUrl[1]!=CCUpload &&
                reqUrl[1]!=CCCancelUpload &&
                reqUrl[1]!=CCFacebook &&
                reqUrl[1]!=CCDelete /*&&
                            reqUrl[1]!=CCGetLog*/,
                -1,sendError(d,404),
                "WARNING: GET: unknown command from client: %c",reqUrl[1]);

        LOG0close_return(!d->mySessionId && !(requestFlags&RequestFlag_pingRequest),-1,sendError(d,403),
                         "WARNING: mySession is NULL for this non-PING request. Answering 403 Forbidden.");
        ph_assert(d->mySessionId || requestFlags&RequestFlag_pingRequest,"mySession is NULL for a non-PING request");

        switch(reqUrl[1]){
        case CCGetAlbum:{
            int nrThumbnails=1;
            int pixelRatio=1;
            char *prStr=NULL;
            char *key=NULL;
            if(reqQueryStr){
                //The thing below does suppose that n=... is the first parameter in the query string
                if(sscanf(reqQueryStr,"n=%d",&nrThumbnails)==1){
                    if(nrThumbnails<1)nrThumbnails=1;
                    if(nrThumbnails>40)nrThumbnails=40;

                };
                prStr=strstr(reqQueryStr,"pr=");
                if(prStr && 1==sscanf(prStr+3,"%d",&pixelRatio)){
                    LOG0c_do(pixelRatio<1 || pixelRatio>3,pixelRatio=1,"WARNING: invalid pixelRatio value: %d",pixelRatio);
                };
                LOG0("GetAlbum: Thumbnail strip size: %d, pixel ratio: %d",nrThumbnails,pixelRatio);
                key=strstr(reqQueryStr,"key=");
                if(key){
                    key+=4;
                    char *keyEnd=strchr(key,'&');
                    if(keyEnd)
                        *keyEnd='\0';
                }
            }
            struct md_level_etagf lastRequestedAlbumETag;
            cntBuf=getAlbumJsonHttp(&id,key,reqETag,nrThumbnails,pixelRatio,0,eTag,eTagSize,&lastRequestedAlbumETag,&cntBufLen);
            if(!cntBuf){
                //if we are scanning pictures, answer 503
                LOG0close_return(statusFlagsCheck(StatusFlagPictureScanThreadActive),-1,sendError(d,503),
                                 "INFO: requested album not available during picture scan.");
                //otherwise answer 404
                LOG0close_return(1,-1,sendError(d,404),"WARNING: Album not found!");
            }
            struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
            newAlbumRequested(mySession,&id,nrThumbnails,&lastRequestedAlbumETag); //also resets thumbnail transfers
            unlockSession(&mySession);
            cntType=KHTTPTypeJson;
        };break;

        case CCSetAlbum:{
            //d->mySession->lastRequestedAlbumID=id; -> set in newAlbumRequested(...) below
            LOG0("New album set: %s (but we do nothing!)",reqUrl);

            //we do nothing because this creates more problem than it solves
            /*
            struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
            newAlbumRequested(mySession,&id,10,NULL); //also resets thumbnail transfers
            unlockSession(&mySession);*/

            cntBuf=ph_strdup(KJsonAnswerTrue);
            cntBufLen=strlen(cntBuf);
            cntType=KHTTPTypeJson;
        };break;

        case CCGetUrlToAlbum:{
            LOG0close_return(!authorizeRequest(RequestShare,d),-1,sendError(d,403),"Unauthorized Share command received. Answering 403 Forbidden.");
            LOG0("Link URL command received from %s. Executing it.",d->ipString);
            //we need to generate something like:
            //http://photostovis.net/servername/?key=SHA1ID0ID1ID2...
            //where the ID0, ID1, ID2 are the IDs of the album path.
            //the SHA1 is the SHA of the username + IDs strings + the KSalt
            const char *KLinkTemplate="{\"link\":\"http://photostovis.net/%s/?key=";
            cntBufLen=strlen(KLinkTemplate)-2+40+id.level*8+2; //40 is the SHA1 in hex, 8 is the nameCRC for each album ID
            SHA_CTX sha1;
            SHA1_Init(&sha1);
            //we need to add the length of the username
            mdConfig=getMdConfigRd();
            if(mdConfig->phonetUsername)
                l=strlen(mdConfig->phonetUsername);
            else l=0;
            if(l){
                cntBufLen+=l;
                //allocate the buffer
                cntBuf=ph_malloc(cntBufLen+1);
                //write the template+username
                snprintf(cntBuf,cntBufLen,KLinkTemplate,mdConfig->phonetUsername);
                //add username to sha1
                SHA1_Update(&sha1,mdConfig->phonetUsername,l);

                int i;
                const int cntBufPos=strlen(cntBuf);
                //add the nameCRCs at the end
                for(i=0;i<id.level;i++)
                    snprintf(cntBuf+cntBufPos+40+8*i,cntBufLen-cntBufPos-40-8*i,"%08X",id.nameCRC[i]);
                //compute the sha1 of this last part
                if(id.level)
                    SHA1_Update(&sha1,cntBuf+cntBufPos+40,8*id.level);
                //add the unique name
                SHA1_Update(&sha1,mdConfig->salt,strlen(mdConfig->salt));
                releaseMdConfig(&mdConfig);
                //finalise the SHA1
                unsigned char sha1Data[SHA_DIGEST_LENGTH];
                SHA1_Final(sha1Data,&sha1);


                //write these 20 bytes to the buffer
                for(i=0;i<SHA_DIGEST_LENGTH;i++)
                    sprintf(cntBuf+cntBufPos+i+i,"%02X",sha1Data[i]);
                //add the nameCRCs at the end. We need to add them again, because sprintf puts \0 at the end
                for(i=0;i<id.level;i++)
                    snprintf(cntBuf+cntBufPos+40+8*i,cntBufLen-cntBufPos-40-8*i,"%08X",id.nameCRC[i]);
                //write the final "}
                cntBuf[cntBufPos+40+8*id.level]='\"';
                cntBuf[cntBufPos+40+8*id.level+1]='}';
                cntBuf[cntBufPos+40+8*id.level+2]='\0';
                //our URL is ready!
                ph_assert(strlen(cntBuf)==cntBufLen,"%zu %zu",strlen(cntBuf),cntBufLen);
            } else {
                //we cannot have a URL link because we are not registered
                releaseMdConfig(&mdConfig);
                cntBufLen=strlen(KJsonAnswerFalseErr)+5+1;
                cntBuf=ph_malloc(cntBufLen);
                cntBufLen=snprintf(cntBuf,cntBufLen,KJsonAnswerFalseErr,-1);
            }
            LOG0("Link URL object: %s",cntBuf);
            cntType=KHTTPTypeJson;
        };break;

        case CCDownladAsZipInfo:{
            //first, check if we have a username (server is registered)
            /*
            mdConfig=getMdConfigRd();
            if(!mdConfig->phonetUsername){
                //cannot do it, server is not registered
                releaseMdConfig(&mdConfig);
                r=-11;
                cntBufLen=strlen(KJsonAnswerFalseErr)+5+1;
                cntBuf=ph_malloc(cntBufLen);
                cntBufLen=snprintf(cntBuf,cntBufLen,KJsonAnswerFalseErr,r);
            } else {
                releaseMdConfig(&mdConfig);
                */
                //we can try it!
                uint64_t contentSize;
                unsigned int nrPics,nrVideos,nrAlbums,r;
                r=getZipDownloadInfo(&id,&contentSize,&nrPics,&nrVideos,&nrAlbums);
                if(r){
                    cntBufLen=strlen(KJsonAnswerFalseErr)+5+1;
                    cntBuf=ph_malloc(cntBufLen);
                    cntBufLen=snprintf(cntBuf,cntBufLen,KJsonAnswerFalseErr,r);
                } else {
                    if((contentSize>>20)>MAX_ZIP_DOWNLOAD_SIZE_MB){
                        //answer some error
                        const char *KJsonAnswerFalseErrSize="{\"a\":false,\"err\":%d,\"sz\":%u}";
                        r=-10;
                        cntBufLen=strlen(KJsonAnswerFalseErrSize)+5+10+1;
                        cntBuf=ph_malloc(cntBufLen);
                        cntBufLen=snprintf(cntBuf,cntBufLen,KJsonAnswerFalseErrSize,r,(unsigned int)(contentSize>>20));
                    } else {
                        const char *KDownloadInfoTemplate="{\"sz\":%u,\"p\":%u,\"v\":%u,\"a\":%u}";
                        cntBufLen=strlen(KDownloadInfoTemplate)+4*10+1;//max length for a 32 unsigned integer: 10 characters
                        cntBuf=ph_malloc(cntBufLen);
                        cntBufLen=snprintf(cntBuf,cntBufLen,KDownloadInfoTemplate,(unsigned int)(contentSize>>20),nrPics,nrVideos,nrAlbums);

                        //do we have a key? If yes, we need to get it and use it for the download URL we are going to send
                        uint8_t visibleLayer=0;
                        if(reqQueryStr){
                            char *key=strstr(reqQueryStr,"key=");
                            if(key){
                                key+=4;
                                char *keyEnd=strchr(key,'&');
                                if(keyEnd)
                                    *keyEnd='\0';

                                ph_assert((strlen(key)-40)%8==0,NULL);
                                visibleLayer=(strlen(key)-40)/8;
                                ph_assert(visibleLayer<=id.level,NULL);
                            }
                        }

                        struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                        prepareZipFile(&id,visibleLayer,contentSize,mySession); //the zip is prepared in a separate thread
                        unlockSession(&mySession);
                    }
                }
            //}
            cntType=KHTTPTypeJson;
        };break;
        case CCDownloadAsZip:{
            LOG0("Received a Download as Zip request!");
            r=getZipDownload(&id,requestFlags&RequestFlag_httpKeepAlive,dateStr,d->c,d->ssl);

            LOG0close_return(r==-1,-1,sendError(d,404),"WARNING: Zip archive not found");
            LOG0return(r==-2,-1,"INFO: Zip Archive download ended by the browser.");
            if(r==0)
                requestFlags|=RequestFlag_answerAlreadySent;
            else ph_assert(0,NULL);
        };break;

        case CCGetThumbnail:{
            int pixelRatio=1;
            int isVideo=-1; //-1==we do not know!
            if(reqQueryStr){
                char *prStr=strstr(reqQueryStr,"pr=");
                if(prStr && sscanf(prStr,"pr=%d",&pixelRatio)==1)
                    if(pixelRatio!=1 && pixelRatio!=2 && pixelRatio!=3)
                        pixelRatio=1;
            }
            //LOG0("Detected pixel ratio: %d",pixelRatio);
            cntBuf=getThumbnailFromCache(&id,pixelRatio,&cntBufLen,&cntBufShouldBeFreed,eTag,eTagSize,reqETag,&isVideo);
            if(!cntBuf){
                //answer 307 temporary redirect to nothumb-photo.svg
                char rspBuf[HTTP_RESPONSE_BUFFER_SIZE];
                if(isVideo==1)
                    r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,KHTTP307,"/images/nothumb-video.svg",
                               requestFlags&RequestFlag_httpKeepAlive?KConnectionKeepAlive:KConnectionClose,dateStr);
                else
                    r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,KHTTP307,"/images/nothumb-photo.svg",
                               requestFlags&RequestFlag_httpKeepAlive?KConnectionKeepAlive:KConnectionClose,dateStr);
                r2=srvWrite(d,rspBuf,r);
                LOG0return(r2<0,-1,"ERROR writing the HTTP response header: %s",strerror(errno));
                LOG0return(r2!=r,-1,"ERROR writing the HTTP response header. Wrote %d bytes instead of %d",r2,r);
                requestFlags|=RequestFlag_answerAlreadySent;
            }
            cntType=KHTTPTypeJpeg;
        };break;
        case CCGetImage:{
            //check if we width and height
            int requestedWidth=0,requestedHeight=0;
            if(reqQueryStr){
                char *ptrStr=strstr(reqQueryStr,"w=");
                if(ptrStr && 1==sscanf(ptrStr,"w=%d",&requestedWidth))
                    if(requestedWidth<0)requestedWidth=0;
                ptrStr=strstr(reqQueryStr,"h=");
                if(ptrStr && 1==sscanf(ptrStr,"h=%d",&requestedHeight))
                    if(requestedHeight<0)requestedHeight=0;
                LOG0("Requested image size: %dx%d",requestedWidth,requestedHeight);
            }

            struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
            cntBuf=getImageHttp(&id,&cntBufLen,eTag,eTagSize,reqETag,requestedWidth,requestedHeight,mySession);
            unlockSession(&mySession);
            if(!cntBuf){
                //if we are scanning pictures, answer 503
                LOG0close_return(statusFlagsCheck(StatusFlagPictureScanThreadActive),-1,sendError(d,503),
                                 "INFO: requested image not available during picture scan.");
                //otherwise answer 404
                LOG0close_return(1,-1,sendError(d,404),"WARNING: Image not found!");
            }
            cntType=KHTTPTypeJpeg;
        };break;
        case CCGetVideo:{
            r=getVideoHttp(&id,eTag,eTagSize,reqETag,requestFlags&RequestFlag_httpKeepAlive,d,dateStr,rangeStart,rangeEnd);
            //TODO: differentiate for scanning pictures case??
            LOG0close_return(r==-1,-1,sendError(d,404),"WARNING: Video not found!");
            LOG0return(r==-2,-1,"INFO: Video streaming ended by the browser.");
            if(r==0)
                requestFlags|=RequestFlag_answerAlreadySent;
            else if(r==1)
                cntBuf=reqETag;
            else ph_assert(0,NULL);
        };break;
        case CCDelete:{
            LOG0close_return(!authorizeRequest(RequestUpload,d),-1,sendError(d,403),"Unauthorized Delete command received. Answering 403 Forbidden.");
            int err=deleteMedia(&id);
            if(err){
                cntBufLen=strlen(KJsonAnswerFalseErr)+5+1;
                cntBuf=ph_malloc(cntBufLen);
                cntBufLen=snprintf(cntBuf,cntBufLen,KJsonAnswerFalseErr,-1);
            } else cntBuf=ph_strdup(KJsonAnswerTrue);

            cntBufLen=strlen(cntBuf);
            cntType=KHTTPTypeJson;
        };break;
        case CCPing:{
            //create plain string containing the phonetUsername
            mdConfig=getMdConfigRd();
            if(mdConfig->phonetUsername){
                cntBufLen=strlen(mdConfig->phonetUsername);
                cntBuf=ph_malloc(cntBufLen+1);
                memcpy(cntBuf,mdConfig->phonetUsername,cntBufLen+1);
            } else {
                cntBufLen=1;
                cntBuf=ph_malloc(2);
                cntBuf[0]=' ';
                cntBuf[1]='\0';
                LOG0("Generic (blank) answer to Ping: we do not have a phonetUsername");
            }
            releaseMdConfig(&mdConfig);
            cntType=KHTTPTypeText;
        };break;
        case CCShutdown:{
            //Shutdown the server
            LOG0close_return(!authorizeRequest(RequestAdmin,d),-1,sendError(d,403),"Unauthorized Shutdown command received. Answering 403 Forbidden.");

            LOG0("Shutdown command received from %s. Executing it.",d->ipString);
            cntBuf=ph_strdup(KJsonAnswerTrue);
            shouldExit=EXIT_SHUTDOWN;

            cntBufLen=strlen(cntBuf);
            cntType=KHTTPTypeJson;
        };break;
        case CCUpload:{
            //create a new subfolder
            LOG0close_return(!authorizeRequest(RequestUpload,d),-1,sendError(d,403),"Unauthorized Upload command received. Answering 403 Forbidden.");

            char *strAlbum=NULL;
            char *strSubAlbum=NULL;
            if(reqQueryStr){
                strAlbum=strstr(reqQueryStr,"a=");
                if(strAlbum)strAlbum+=2;

                strSubAlbum=strstr(reqQueryStr,"s=");
                if(strSubAlbum)strSubAlbum+=2;

                if(strAlbum){
                    reqQueryStr=strchr(strAlbum,'&');
                    if(reqQueryStr)
                        *reqQueryStr='\0';
                }

                if(strSubAlbum){
                    reqQueryStr=strchr(strSubAlbum,'&');
                    if(reqQueryStr)
                        *reqQueryStr='\0';
                }
            }

            if(strSubAlbum){
                uint32_t nameCRC;
                r=createNewSubalbum(strAlbum,strSubAlbum,&nameCRC);
                if(!r){
                    cntBufLen=strlen(KJsonAnswerTrue)+20;
                    cntBuf=ph_malloc(cntBufLen);
                    cntBufLen=snprintf(cntBuf,cntBufLen,"{\"a\":true,\"id\":\"%08X\"}",nameCRC);

                    LOG0("Create new subalbum, answering: %s",cntBuf);
                }

            } else {
                r=moveUploadedContent(strAlbum);
                if(!r)
                    cntBuf=ph_strdup(KJsonAnswerTrue);
            }
            if(r){
                //there was an error with the moving
                cntBufLen=strlen(KJsonAnswerFalseErr)+5+1;
                cntBuf=ph_malloc(cntBufLen);
                snprintf(cntBuf,cntBufLen,KJsonAnswerFalseErr,r);
            }

            cntBufLen=strlen(cntBuf);
            cntType=KHTTPTypeJson;
        };break;

        /* This is old upload
        case CCUpload:{
            //Commit uploaded pictures (move them to their place)
            LOG0close_return(!authorizeRequest(RequestUpload,d),-1,sendError(d,403),"Unauthorized Upload command received. Answering 403 Forbidden.");

            char *strAlbum=NULL;
            char *strSubAlbum=NULL;
            if(reqQueryStr){
                strAlbum=strstr(reqQueryStr,"a=");
                if(strAlbum)strAlbum+=2;

                strSubAlbum=strstr(reqQueryStr,"s=");
                if(strSubAlbum)strSubAlbum+=2;

                if(strAlbum){
                    reqQueryStr=strchr(strAlbum,'&');
                    if(reqQueryStr)
                        *reqQueryStr='\0';
                }

                if(strSubAlbum){
                    reqQueryStr=strchr(strSubAlbum,'&');
                    if(reqQueryStr)
                        *reqQueryStr='\0';
                }
            }

            LOG0("Move uploaded command received from %s. Executing it.",d->ipString);
            r=moveUploadedContent(strAlbum,strSubAlbum);
            if(r){
                //there was an error with the moving
                cntBufLen=strlen(KJsonAnswerFalseErr)+5+1;
                cntBuf=ph_malloc(cntBufLen);
                snprintf(cntBuf,cntBufLen,KJsonAnswerFalseErr,r);
            } else {
                cntBuf=ph_strdup(KJsonAnswerTrue);
            }

            cntBufLen=strlen(cntBuf);
            cntType=KHTTPTypeJson;
        };break;*/
        case CCCancelUpload:{
            //Cancel uploaded pictures (move them to their place)
            LOG0close_return(!authorizeRequest(RequestUpload,d),-1,sendError(d,403),"Unauthorized CancelUpload command received. Answering 403 Forbidden.");

            LOG0("Cancel uploads command received from %s. Executing it.",d->ipString);
            r=cancelUploadedContent();
            if(r){
                //there was an error with the moving
                cntBufLen=strlen(KJsonAnswerFalseErr)+5+1;
                cntBuf=ph_malloc(cntBufLen);
                snprintf(cntBuf,cntBufLen,KJsonAnswerFalseErr,r);
            } else {
                cntBuf=ph_strdup(KJsonAnswerTrue);
            }

            cntBufLen=strlen(cntBuf);
            cntType=KHTTPTypeJson;
        };break;
        case CCFacebook:{
            LOG0close_return(!authorizeRequest(RequestShare,d),-1,sendError(d,403),"Unauthorized FB Share command received. Answering 403 Forbidden.");
            //get the og information for the requested url
            char *key=NULL;
            if(reqQueryStr){
                key=strstr(reqQueryStr,"key=");
                if(key){
                    key+=4;
                    char *keyEnd=strchr(key,'&');
                    if(keyEnd)
                        *keyEnd='\0';
                }
            }

            if(key)LOG0("Facebook request received (key=%s)",key);
            else LOG0("Facebook request received (no key)");

            char cmd=CCGetAlbum;
            if(reqUrlLen>3)cmd=reqUrl[3];
            switch(cmd){
            case CCGetAlbum:
                cntBuf=getAlbumOgProperties(&id,key,&cntBufLen,reqUrl+2);
                cntType=KHTTPTypeHtml;
                break;
            case CCGetImage:
                cntBuf=getImageOgProperties(&id,key,&cntBufLen,reqUrl+2);
                cntType=KHTTPTypeHtml;
                break;
            case CCGetThumbnail:{
                int pixelRatio=2;
                /*if(reqQueryStr){
                        char *prStr=strstr(reqQueryStr,"pr=");
                        if(prStr && sscanf(prStr,"pr=%d",&pixelRatio)==1)
                            if(pixelRatio!=1 && pixelRatio!=2 && pixelRatio!=3)
                                pixelRatio=1;
                    }*/
                cntBuf=getThumbnailFromCache(&id,pixelRatio,&cntBufLen,&cntBufShouldBeFreed,eTag,eTagSize,reqETag,NULL);
                cntType=KHTTPTypeJpeg;
            };break;
            default:ph_assert(0,"SWITCH case NOT IMPLEMENTED for letter: %c",reqUrl[3]);
            }
            if(!cntBuf){
                //if we are scanning pictures, answer 503
                LOG0close_return(statusFlagsCheck(StatusFlagPictureScanThreadActive),-1,sendError(d,503),
                                 "INFO: requested thingy not available during picture scan.");
                //otherwise answer 404
                LOG0close_return(1,-1,sendError(d,404),"WARNING: Thingy not found!");
            }
        };break;

            /*
            case CCGetLog:{
                cntType=KHTTPTypeText;
                cntBuf=getLogFile(&cntBufLen);
            };break;*/

        default:ph_assert(0,NULL); //there was a LOGclose_return above, we should not get here
        }//switch


        //sending the result
        processingTime=getRelativeTimeMs()-d->processingTimeStart;
        if(!(requestFlags&RequestFlag_answerAlreadySent)){
            int isImage=(cntType==KHTTPTypeJpeg && reqUrl[1]==CCGetImage)?1:0;
            int isThumb=(cntType==KHTTPTypeJpeg && reqUrl[1]==CCGetThumbnail)?1:0;
            uint32_t transferStartTime=0xFFFFFFFF;
            char rspBuf[HTTP_RESPONSE_BUFFER_SIZE];

            if(processingTime>3600000)//nothing should take one hour
                processingTime=0;
            //send the response header
            if(cntBuf==reqETag){
                LOG0("Answering: 304 Not Modified.");
                cntBufLen=0;
                if(cntType==KHTTPTypeJpeg){
                    //image or thumbnail
                    uint32_t bitrate=0xFFFFFFFF;;
                    struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                    if(reqUrl[1]==CCGetImage){
                        ph_assert(id.level>0,NULL);
                        addImageTransferStart(mySession,cntBufLen,processingTime,id.nameCRC[id.level-1],&transferStartTime);
                    } else if(reqUrl[1]==CCGetThumbnail)
                        bitrate=addThumbTransferStart(mySession,cntBufLen,&transferStartTime);
                    sendBitrateToClient(mySession,bitrate);
                    unlockSession(&mySession);
                }//if it is image or thumbnail

                //the requested content is the same, we can send a HTTP 304 Not Modified answer
                r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,KHTTP304,requestFlags&RequestFlag_httpKeepAlive?KConnectionKeepAlive:KConnectionClose,dateStr);
                cntBuf=NULL;
            } else {
                if(eTag[0]=='\0') //normal answer, no ETag
                    r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,KHTTPOkNoETag,requestFlags&RequestFlag_httpKeepAlive?KConnectionKeepAlive:KConnectionClose,
                               cntType,(uint64_t)cntBufLen,dateStr);
                else //normal answer with ETag
                    r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,KHTTPOk,requestFlags&RequestFlag_httpKeepAlive?KConnectionKeepAlive:KConnectionClose,
                               cntType,(uint64_t)cntBufLen,eTag,dateStr);
            }
            LOG0close_return(r<0,-1,if(cntBufShouldBeFreed)free(cntBuf);sendError(d,500);
                    if(transferStartTime!=0xFFFFFFFF){
                        struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                        if(isImage)imageTransferFailed(mySession,cntBufLen,processingTime,id.nameCRC[id.level-1],transferStartTime);
                        if(isThumb)thumbTransferFailed(mySession,cntBufLen,transferStartTime);
                        unlockSession(&mySession);
                    },"ERROR error writing the HTTP response header to the buffer");

            LOG0close_return(r>=HTTP_RESPONSE_BUFFER_SIZE,-1,if(cntBufShouldBeFreed)free(cntBuf);sendError(d,500);
                    if(transferStartTime!=0xFFFFFFFF){
                        struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                        if(isImage)imageTransferFailed(mySession,cntBufLen,processingTime,id.nameCRC[id.level-1],transferStartTime);
                        if(isThumb)thumbTransferFailed(mySession,cntBufLen,transferStartTime);
                        unlockSession(&mySession);
                    },"ERROR: Insufficient buffer size (%d) for the HTTP response header",HTTP_RESPONSE_BUFFER_SIZE);

            ph_assert(r==strlen(rspBuf),"%d!=%zu",r,strlen(rspBuf));
            r2=srvWrite(d,rspBuf,r);
            LOG0close_return(r2<0,-1,if(cntBufShouldBeFreed)free(cntBuf);
                    if(transferStartTime!=0xFFFFFFFF){
                        struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                        if(isImage)imageTransferFailed(mySession,cntBufLen,processingTime,id.nameCRC[id.level-1],transferStartTime);
                        if(isThumb)thumbTransferFailed(mySession,cntBufLen,transferStartTime);
                        unlockSession(&mySession);
                    },"ERROR writing the HTTP response header: %s",strerror(errno));

            LOG0close_return(r2!=r,-1,if(cntBufShouldBeFreed)free(cntBuf);
                    if(transferStartTime!=0xFFFFFFFF){
                        struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                        if(isImage)imageTransferFailed(mySession,cntBufLen,processingTime,id.nameCRC[id.level-1],transferStartTime);
                        if(isThumb)thumbTransferFailed(mySession,cntBufLen,transferStartTime);
                        unlockSession(&mySession);
                    },"ERROR writing the HTTP response header. Wrote %d bytes instead of %d",r2,r);

            //LOG0("Sent answer:\r\n%s",rspBuf);
            //send the content
            if(cntBuf){
#if BITRATE_LIMITED_TO
                //we artificially limit the bitrate to this number by waiting this ammount
                ph_assert(cntBufLen>0,NULL);
                //compute the time we need to sleep
                useconds_t usecs=(cntBufLen<<3)/BITRATE_LIMITED_TO;
#endif
                LOG0("Sending the content");
                int nrZeroSends=0;
                if(isImage){
                    struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                    addImageTransferStart(mySession,cntBufLen,processingTime,id.nameCRC[id.level-1],&transferStartTime);
                    unlockSession(&mySession);
                    LOG0("Sending image with length %zu",cntBufLen);
                } else if(isThumb){
                    struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                    uint32_t bitrate=addThumbTransferStart(mySession,cntBufLen,&transferStartTime);
                    sendBitrateToClient(mySession,bitrate);
#if BITRATE_LIMITED_TO
                    //we should multiply the time to sleep value with the number of connected threads
                    if(mySession->nrConnectedThreads>2)
                        usecs*=mySession->nrConnectedThreads-1; //-1 for the websocket
#endif
                    unlockSession(&mySession);
                    LOG0("Sending thumb with length %zu (bitrate: %ukbps)",cntBufLen,bitrate);
                } else
                    LOG0("Sending item with length %zu",cntBufLen);

#if BITRATE_LIMITED_TO
                unsigned int sleepTime=LOG0("Thread %08X: Bitrate forced to %dMbps. Sleeping %dus. ",
                                            d->threadId,BITRATE_LIMITED_TO,(int)usecs);
                usleep(usecs);
                sleepTime=getRelativeTimeMs()-sleepTime;
                LOG0("Thread %08X: Done sleeping. Slept for %ums.",d->threadId,sleepTime);
#endif
                r=0;
                while(r<cntBufLen){
                    r2=srvWrite(d,cntBuf+r,cntBufLen-r);
                    LOG0close_return(r2<0,-1,if(cntBufShouldBeFreed)free(cntBuf);
                            struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                            if(isImage)imageTransferFailed(mySession,cntBufLen,processingTime,id.nameCRC[id.level-1],transferStartTime);
                            if(isThumb)thumbTransferFailed(mySession,cntBufLen,transferStartTime);
                            unlockSession(&mySession);
                            ,"ERROR writing the HTTP response content: %s",strerror(errno));
                    if(r2==0){
                        nrZeroSends++;
                        LOG0close_return(nrZeroSends>100,-1,if(cntBufShouldBeFreed)free(cntBuf);
                                struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                                if(isImage)imageTransferFailed(mySession,cntBufLen,processingTime,id.nameCRC[id.level-1],transferStartTime);
                                if(isThumb)thumbTransferFailed(mySession,cntBufLen,transferStartTime);
                                unlockSession(&mySession);
                                ,"ERROR writing the HTTP response content. send returned zero too many times (100).");
                        usleep(100000); //100ms
                    } else nrZeroSends=0;
                    r+=r2;
                }
                ph_assert(r==cntBufLen,NULL);

                if(cntBufShouldBeFreed)free(cntBuf);
                cntBuf=NULL;
                if(isThumb){
                    struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                    thumbTransferSucceeded(mySession,cntBufLen,transferStartTime,processingTime);
                    unlockSession(&mySession);
                }
            } else if(isThumb && transferStartTime!=0xFFFFFFFF){
                struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                thumbTransferSucceeded(mySession,0,transferStartTime,processingTime);
                unlockSession(&mySession);
            }
        } else
            ph_assert(!cntBuf,NULL);
    } else {
        //GET commands without ID

        //if here, there are just 2 possibilities: websocket request or file request
        if(websocketRequest){
            //we have a potentially valid websocket request
            LOG0close_return(websocketRequest!=WebsocketConnectionUpgrade+WebsocketUpgradeWebsocket+WebsocketKey+WebsocketProtocol+WebsocketVersion,-1,
                             sendError(d,404),"WARNING: Incomplete websocket upgrade request. Answering 404.");
            LOG0close_return(!d->mySessionId,-1,sendError(d,403),"WARNING: websocket request without a session. Answering 403 Forbidden.");
            LOG0("WebSocket request received.");
            char rspBuf[HTTP_RESPONSE_BUFFER_SIZE];
            r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,KHTTPWebsocket,*websocketAccept);
            srvWrite(d,rspBuf,r);
            return WEBSOCKET_UPGRADE;
        }

        //if here, this is a file request
        LOG0close_return(reqUrlLen>DOC_ROOT_FILES_EXTRA_LEN,-1,sendError(d,404),"WARNING: file request URL (%s) too big",reqUrl);
        r=srvSendFile(d,reqUrl,reqUrlLen,reqETag,requestFlags&RequestFlag_acceptsGzip,requestFlags&RequestFlag_httpKeepAlive);
        if(r)return r; //request errored
    }

    LOG0("GET Request: success! Processing time: %ums. Processed bytes: %d",processingTime,processedBytes);
    LOG0return(!(requestFlags&RequestFlag_httpKeepAlive),-1,"Closing non-keep-alive connection.");

    return processedBytes;
}

void unmaskWebSocketPacket(uint8_t *payload, int payloadLen, uint8_t *mask){
    int i;
    for(i=0;i<payloadLen;i++)
        payload[i]^=mask[i%4];
}

void *srvReqHandlingThread(void *data){
    int r=0,processedBytes,pos,upgradedToWebsocket=0;
    int hddAccessed=0,logFlushedOnce=0;
    char buf[DEFAULT_HTTP_BUFFER_SIZE];
    struct srvThreadData * restrict d=(struct srvThreadData *)data;
    int wsPipe[2];

    //is this a SSL connection? If yes, do the SSL accept
    if(d->ssl==(void*)1){
        LOG0("This is a SSL request!");
        d->ssl=SSL_new(ctx);
        SSL_set_fd(d->ssl,d->c); // set connection socket to SSL state
        r=SSL_accept(d->ssl);
        //LOG0("SSL_accept returned %d",r);
        if(r!=1){
            LOG0("ERROR (SSL_accept returned %d) in the TLS/SSL handshake:",r);
            printSslErrors();
            r=-1; //this will skip the main while
        } else r=0; //so that we do not skip the main while
    } else ph_assert(r==0,NULL);

    pos=0;
    if(!r)while(1){
        if(!d->op){
            LOG0(NULL);
            LOG0c(!upgradedToWebsocket,"Reading from socket %d ...",d->c);
        }

        if(upgradedToWebsocket && wsPipe[0]>=0){
            struct pollfd pfd[2];
            pfd[0].fd=d->c;
            pfd[0].events=POLLIN;
            pfd[1].fd=wsPipe[0];
            pfd[1].events=POLLIN;

            r=poll(pfd,2,-1);
            //do we have packets to send?
            ph_assert(r,NULL); //cannot be zero because timeout is -1
            LOG0close_continue(r<0,sleep(1),"ERROR: poll returned an error: %s",strerror(errno));

            //if here, check for pipe events
            if(pfd[1].revents){
                uint32_t dataLen=0;
                r=read(wsPipe[0],&dataLen,4);
                ph_assert(r==4,"Could not read 4 bytes (only %d)",r);
                void *buf=ph_malloc(dataLen);
                int rr;
                r=0;
                while(r<dataLen){
                    rr=read(wsPipe[0],buf+r,dataLen-r);
                    if(rr<0)break;
                    r+=rr;
                }
                //we do not check if r==rr. If it is not, we are writing a garbage packet that will not be understood and the browser will reopen the websocket
                srvWriteGeneric(d->c,d->ssl,buf,dataLen,1);
                free(buf);
                continue;
            }
        }

        if(d->ssl){
            //LOG0("Reading here ... (pos=%d)",pos);
            r=SSL_read(d->ssl,buf+pos,DEFAULT_HTTP_BUFFER_SIZE-1-pos);
            //LOG0("SSL_read returned %d",r);

            if(r==0){
                int st=SSL_get_shutdown(d->ssl);
                switch(st){
                case 0:/*LOG0("No shutdown");*/break;
                case SSL_SENT_SHUTDOWN:LOG0("SSL Shutdown sent");break;
                case SSL_RECEIVED_SHUTDOWN:LOG0("SSL Shutdown received");break;
                default:LOG0("SSL Shutdown unknown");
                }
            }

            if(upgradedToWebsocket){
                //TODO: send websocket close packet
                LOG0close_break(r<0 && (errno==EAGAIN || errno==EWOULDBLOCK),printSslErrors(),"SSL WebSocket (%d) timed-out, closing it.",d->c);
                LOG0close_break(r<0,printSslErrors(),"ERROR reading data from SSL WebSocket connection: %s.",strerror(errno));
                LOG0close_break(r==0,printSslErrors(),"Reading 0 bytes from SSL WebSocket connection.");
            } else {
                LOG0close_break(r<0 && (errno==EAGAIN || errno==EWOULDBLOCK),printSslErrors(),"SSL Socket (%d) timed-out, closing it.",d->c);
                LOG0close_break(r<0,printSslErrors(),"ERROR reading data from SSL connection: %s.",strerror(errno));
                LOG0close_break(r==0,printSslErrors(),"Reading 0 bytes from SSL connection.");
            }
        } else {
            r=recv(d->c,buf+pos,DEFAULT_HTTP_BUFFER_SIZE-1-pos,MSG_NOSIGNAL); //-1 so we have enough space for \0

            if(upgradedToWebsocket){
                //TODO: send websocket close packet
                LOG0c_break(r<0 && (errno==EAGAIN || errno==EWOULDBLOCK),"WebSocket (%d) timed-out, closing it.",d->c);
                LOG0c_break(r<0,"ERROR reading data from WebSocket connection: %s",strerror(errno));
                LOG0c_break(r==0 ,"Reading 0 bytes from WebSocket connection.");
            } else {
                LOG0c_break(r<0 && (errno==EAGAIN || errno==EWOULDBLOCK),"Socket (%d) timed-out, closing it.",d->c);
                LOG0c_break(r<0,"ERROR reading data from connection: %s",strerror(errno));
                LOG0c_break(r==0 ,"Reading 0 bytes from connection.");
            }
        }

        ph_assert(r>=0,NULL);
        //do we have enough data?
        r+=pos;
        pos=0;
        if(r<6){
            LOG0("Re-reading, not enough data (nr bytes=%d)",r);
            pos=r;
            continue;
        }
        LOG0c_break(r>=DEFAULT_HTTP_BUFFER_SIZE,"ERROR: request too large (%d bytes).",r);
        buf[r]='\0';

        //process the data
        if(upgradedToWebsocket){
            ledClientActivity(LCOn,0);
            ph_assert(d->mySessionId,"mySession is NULL");
            uint8_t *websocketPacket=(uint8_t*)buf;

            LOG0("We have a WebSocket package of type %d",websocketPacket[0]&0xF);
            //check the packet for correctness
            LOG0c_continue(r<2,"WebSocket packet too short (%d<2), ignoring",r);
            LOG0c_continue(!(websocketPacket[0]&0x80),"WebSocket packet not a FIN packet, ignoring");
            LOG0c_continue(!(websocketPacket[1]&0x80),"WebSocket packet not masked, ignoring");
            //unmask the payload
            uint8_t *payload=NULL;
            int payloadLen=websocketPacket[1]&0x7F;
            if(payloadLen<126){
                //verify the length correctness
                LOG0c_continue(payloadLen!=r-6,"WebSocket packet length (%d) not matching reported payload length (%d) +6",r,payloadLen);
                payload=(uint8_t*)(websocketPacket+6);
                unmaskWebSocketPacket(payload,payloadLen,(uint8_t*)(websocketPacket+2));
            } else if(payloadLen==126){
                payloadLen=(((int)websocketPacket[2])<<8)+websocketPacket[3];
                //verify the length correctness
                LOG0c_continue(payloadLen!=r-8,"WebSocket packet length (%d) not matching reported payload length (%d) +8",r,payloadLen);
                payload=(uint8_t*)(websocketPacket+8);
                unmaskWebSocketPacket(payload,payloadLen,(uint8_t*)(websocketPacket+4));
            } else LOG0c_continue(1,"WebSocket reported payload length (%d) unsupported",payloadLen);

            //build the response packet
            int packetLen=0;
            switch(websocketPacket[0]&0xF){
            case 8:{//close packet
                //build the reply close packet
                uint8_t packet[4];
                packet[0]=0x88;//10001000

                if(payloadLen>1){
                    packetLen=4;
                    packet[1]=2;//0000 0010
                    packet[2]=payload[0];
                    packet[3]=payload[1];
                } else {
                    packetLen=2;
                    packet[1]=0;//0000 0000
                }

                //send the packet and close the connection (break)
                srvWrite(d,packet,packetLen);

                LOG0("Response CLOSE packet sent for Websocket");
                packetLen=-1; //so that we break from the while loop
            };break;
            case 9:{//received a ping packet
                //we must respond with a pong packet with the same content. Shift the content 4 bytes (overwrite the mask)
                if(payloadLen){
                    memmove(websocketPacket+r-payloadLen-4,websocketPacket+r-payloadLen,payloadLen);
                    packetLen=r-4;
                } else packetLen=2;
                websocketPacket[0]=0x8A;//10001010
                websocketPacket[1]&=0x7F;//clear the mask bit

                srvWrite(d,websocketPacket,packetLen);
                LOG0("Response PONG packet sent for Websocket");

            };break;
            case 10://received a pong packet. We need to do nothing
                LOG0("PONG packet received, doing nothing");
                break;
            case 1://text (data) packet
                payload[payloadLen]=0;
                LOG0("Data/text packet received: %s",(char*)payload);
                if(!strcmp((char*)payload,"thumbnails loaded")){
                    struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                    uint32_t bitrate=thumbnailsTransferEnded(mySession);
                    LOG0("Bitrate here: %u",bitrate);
                    sendBitrateToClient(mySession,bitrate);
                    unlockSession(&mySession);
                } else if(!strncmp((char*)payload,"watchedID=",10)){
                    //LOG0("Received watched time websocket packet: %s",(char*)payload);
                    struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                    processImgWatchedMessage((char*)payload,mySession);
                    unlockSession(&mySession);
                } else if(!strncmp((char*)payload,"loadID=",7)){
                    //LOG0("Received load time websocket packet: %s",(char*)payload);
                    struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                    processImgLoadedMessage((char*)payload,mySession);
                    unlockSession(&mySession);
                } else if(!strncmp((char*)payload,"prefetchID=",7)){
                    //LOG0("Received prefetch websocket packet: %s",(char*)payload);
                    struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                    processImgPrefetchMessage((char*)payload,mySession);
                    unlockSession(&mySession);
                } else if(!strncmp((char*)payload,"renderingTimePerMPixel=",23)){
                    //LOG0("Received renderingTimePerMPixel websocket packet: %s",(char*)payload);
                    struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                    processRenderingTimeMessage((char*)payload,mySession);
                    unlockSession(&mySession);
                } else
                    LOG0("WARNING: unknown text websocket packet received: %s",(char*)payload);
                break;
            default:
                LOG0("WebSocket: unknown packet received (type: %d). Will do nothing.",(int)(buf[0]&0xF));
            }//switch

            if(packetLen==-1)
                break; //from the while loop
        } else {
            //LOG0("Request received from server:\r\n%s",buf);
            pos=0;
            while(pos<r){
                if(d->op)//we have an ongoing POST operation
                    processedBytes=srvContinuePostRequest(d,buf+pos,r-pos);
                else {
                    LOG0("NEW REQUEST (socket: %d): Read %d bytes from client:",d->c,r);
                    char *websocketAccept=NULL;
                    processedBytes=srvProcessRequest(d,buf+pos,r-pos,&hddAccessed,&websocketAccept);
                    if(websocketAccept)free(websocketAccept); //we do this because it is much easier to free this here than in any return/exception
                }
                if(processedBytes==WEBSOCKET_UPGRADE){
                    LOG0("Socket (%d) upgraded to WebSocket!",d->c);
                    upgradedToWebsocket=1;
                    ph_assert(d->mySessionId,"mySession is NULL"); //the websocket needs to belong to an identified session
                    //set the socket timeout to infinity
                    //TODO: set it to few minutes and send ping packets
                    struct timeval timeout;
                    timeout.tv_usec=0;
                    timeout.tv_sec=DEFAULT_WEBSOCKET_RECV_TIMEOUT;
                    setsockopt(d->c,SOL_SOCKET,SO_RCVTIMEO,&timeout,sizeof(timeout));

                    //this is a websocket associated with this connection
                    struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
                    mySession->nrCamerasSent=0;
                    r=pipe(wsPipe);
                    if(r){
                        LOG0("ERROR: pipe creation failed: %s",strerror(errno));
                        wsPipe[0]=wsPipe[1]=-1;
                    } else mySession->websocketPipe=wsPipe[1];

                    //send nr of sessions, but only to
                    sendNrSessionsToConnectedClients(mySession);
                    unlockSession(&mySession);
                    break;
                }

                if(processedBytes<=0){
                    hddAccessed=1;//we want to force a LOG_FLUSH few lines below
                    break;
                }
                pos+=processedBytes;
                if(shouldExit)break;
            }//while(pos<r)
            if(processedBytes<0)break;
            if(shouldExit)break;

            if(pos==r)pos=r=0;
            ph_assert(pos<=r,NULL);
            if(processedBytes==0 && pos<r && pos>0){
                LOG0("Incomplete HTTP packet/request in the buffer. Flushing the beginning of the buffer");
                memmove(buf,buf+pos,r-pos+1);
                r-=pos;
                pos=r;
            }
            if(hddAccessed && !logFlushedOnce){
                logFlushedOnce=1;
                LOG0("Flushing log");
                LOG_FLUSH;
            }
        }
    }//main while

    if(upgradedToWebsocket)
        LOG0("Thread Exiting (websocket %d).",d->c);
    else
        LOG0("Thread Exiting (socket %d).",d->c);


    int nrConnectedThreads=-1;
    if(d->mySessionId){
        struct ps_session *mySession=getSession(d->mySessionId,d->reqClientIp,1);
        if(upgradedToWebsocket){
            //clear all websocket stuff
            close(wsPipe[1]);
            close(wsPipe[0]);
            mySession->websocketPipe=-1;
        }

        ph_assert(mySession->nrConnectedThreads>0,"nrConnectedThreads is zero");
        mySession->nrConnectedThreads--;
        LOG0("Nr connected threads for this session (%016"UINT64X_FMT"): %u",d->mySessionId,mySession->nrConnectedThreads);
        nrConnectedThreads=mySession->nrConnectedThreads;
        unlockSession(&mySession);
    }

    if(d->ssl)
        SSL_free(d->ssl);
    close(d->c);

    ph_assert(d==data,NULL);
    free(data);
    ph_sem_post(&semThreads);
    int remainingNrThreads;
    ph_sem_getvalue(&semThreads,&remainingNrThreads);
    LOG0("Remaining threads: %d",remainingNrThreads);

    if(hddAccessed)
        LOG_FLUSH;
    if(remainingNrThreads==MAX_WEB_THREADS)
        ledClientActivity(LCOff,0);

    if(shouldExit){
        LOG0("shouldExit=%d. Exiting Photostovis server from web thread.",shouldExit);
        exit(shouldExit);
        //TODO: exit from main thread?
    }

    //if this was the last thread of this session, update the clients of the new number of sessions
    if(!nrConnectedThreads) //it is not important if these are not perfectly synchronized
        sendNrSessionsToConnectedClients(NULL);

    return NULL;
}



int srvAcceptConnection(int isSSL){
    struct srvThreadData *data=NULL;
    struct sockaddr_in caddr;
    int r,srvSocket;
    socklen_t clen=sizeof(struct sockaddr_in);

    //can we handle a new thread?
    int availableThreads;
    ph_sem_getvalue(&semThreads,&availableThreads);
    LOG0("Waiting for a new available thread. We have %d available threads.",availableThreads);
    ph_sem_wait(&semThreads);
    ph_sem_getvalue(&semThreads,&availableThreads);
    LOG0("Waiting done. Available threads (without this one): %d",availableThreads);


    //if here, we can accept a new connection/thread
    struct ps_md_config const * const mdConfig=getMdConfigRd();
    if(isSSL)
        srvSocket=mdConfig->httpsSocket;
    else
        srvSocket=mdConfig->httpSocket;
    releaseMdConfig(&mdConfig);
    r=accept(srvSocket,(struct sockaddr *)&caddr,&clen); //trick so that we do not need to keep data allocated all the time

    LOG0close_return(r<0 && errno==EINTR,1,ph_sem_post(&semThreads),"NOTE: accept was interrupted by a signal.");
    LOG0close_return(r<0,-1,ph_sem_post(&semThreads),"ERROR: accepting connection failed: %s",strerror(errno));

    //check if this is the first thread in a connection series, so we can split the logfile
    ph_sem_getvalue(&semThreads,&availableThreads);
    LOG0("freeWebThreads=%d",availableThreads);

    data=(struct srvThreadData *)ph_malloc(sizeof(struct srvThreadData));
    memset(data,0,sizeof(struct srvThreadData));
    data->c=r;
    data->reqClientIp=htonl(caddr.sin_addr.s_addr);
    data->userIndex=0xFF; //not authenticated

    LOG0("Accepted new connection (%d) from %d.%d.%d.%d",data->c,
         (int)((data->reqClientIp>>24)&0xFF),
         (int)((data->reqClientIp>>16)&0xFF),
         (int)((data->reqClientIp>>8)&0xFF),
         (int)(data->reqClientIp&0xFF));

    //Where are we coming from?
    {
        uint8_t ip1=(data->reqClientIp>>24)&0xFF;
        uint8_t ip2=(data->reqClientIp>>16)&0xFF;
        uint8_t ip3=(data->reqClientIp>>8)&0xFF;
        uint8_t ip4=data->reqClientIp&0xFF;

        if((ip1==192 && ip2==168) || (ip1==127 && ip2==0 && ip3==0 && ip4==1)){
            data->flags|=FlagSrvThread_ComingFromLAN;
            LOG0("Request (%d.%d.%d.%d) coming from LAN",ip1,ip2,ip3,ip4);
        } else if(phonetIp==data->reqClientIp){ //the IP of photostovis.net
            data->flags|=FlagSrvThread_ComingFromPhotostovisDotNet;
            LOG0("Request (%d.%d.%d.%d) coming from photostovis.net",ip1,ip2,ip3,ip4);
        } else if(phoorgIp==data->reqClientIp){  //the IP of photostovis.org
            data->flags|=FlagSrvThread_ComingFromPhotostovisDotNet;
            LOG0("Request (%d.%d.%d.%d) coming from photostovis.org",ip1,ip2,ip3,ip4);
        } else
             LOG0("Request (%d.%d.%d.%d) coming from Internet",ip1,ip2,ip3,ip4);
        snprintf(data->ipString,16,"%d.%d.%d.%d",ip1,ip2,ip3,ip4);
    }

    //connection accepted, set some socket options
/*
#ifdef __APPLE__
    //set "no sigpipe"
    r=1;
    setsockopt(data->c,SOL_SOCKET,SO_NOSIGPIPE,(void *)&r,sizeof(int));
#endif
*/


    /*
    r=1;
    setsockopt(data->c,SOL_SOCKET,SO_KEEPALIVE,&r,sizeof(r)); //this is needed by the HTTP keep-alive
*/

    struct timeval timeout;
    timeout.tv_usec=0;
    timeout.tv_sec=DEFAULT_CLIENT_SOCKET_RECV_TIMEOUT;
    setsockopt(data->c,SOL_SOCKET,SO_RCVTIMEO,&timeout,sizeof(timeout));
    timeout.tv_sec=DEFAULT_CLIENT_SOCKET_SEND_TIMEOUT;
    setsockopt(data->c,SOL_SOCKET,SO_SNDTIMEO,&timeout,sizeof(timeout));

    ph_assert(!data->mySessionId,NULL);

    //SSL stuff
    ph_assert(!data->ssl,NULL);
    if(isSSL)data->ssl=(void*)1; //we will allocate and handle it in the thread

    //create a thread and handle this connection there
    pthread_t t;
    pthread_attr_t attr;
    r=pthread_attr_init(&attr);
    ph_assert(!r,NULL);//the above should not fail
    r=pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
    ph_assert(!r,NULL);//the above should not fail

    r=pthread_create(&t,&attr,&srvReqHandlingThread,data);//transfering ownership of data
    LOG0close_return(r,-1,ph_sem_post(&semThreads);sendError(data,500);close(data->c);free(data),
            "ERROR (%d)creating a thread for handling a connection",r);

    ph_sem_getvalue(&semThreads,&availableThreads);
    LOG0("Thread %08X created (%d/%d).",(unsigned int)(intptr_t)t,MAX_WEB_THREADS-availableThreads,MAX_WEB_THREADS);
    return 0;
}

////////////////////////////////////////////////

int createWebsocketTextPacket(char * const buffer, uint32_t * const bufferLen, uint32_t * const bufferOffset){
    ph_assert(*bufferOffset==KWebsocketBufferOffset,NULL);

    uint8_t *buf=(uint8_t*)buffer;

    if(*bufferLen<126){
        //we only need 2 bytes header
        *bufferOffset-=2;
        buf+=*bufferOffset;
        //writing the header
        // header bits: 1000|0001|0xxx|xxxx
        *(uint8_t*)(buf)=0x81;
        *(uint8_t*)(buf+1)=*bufferLen;
        *bufferLen+=2;
    } else if(*bufferLen<65536){
        //we use 4 bytes header
        *bufferOffset-=4;
        buf+=*bufferOffset;
        //writing the header
        // header bits: 1000|0001|0111|1110 xxxx|xxxx|xxxx|xxxx
        *(uint8_t*)(buf)=0x81;
        *(uint8_t*)(buf+1)=126;
        *(uint16_t*)(buf+2)=htons(*bufferLen);
        *bufferLen+=4;
    } else {
        //we use 10 bytes header
        *bufferOffset-=10;
        //buf+=(*packetOffset);
        //writing the header
        // header bits: 1000|0001|0111|1111 xxxx|xxxx|xxxx|xxxx
        *(uint8_t*)(buf)=0x81;
        *(uint8_t*)(buf+1)=127;
        *(uint32_t*)(buf+2)=0;
        *(uint32_t*)(buf+6)=htonl(*bufferLen);
        *bufferLen+=10;
        LOG0("Large websocket packet created (%d>65535 bytes)",*bufferLen);
    }
    return 0;
}


static void sendBitrateToClient(struct ps_session * const mySession, const uint32_t bitrate){
    //check some conditions:
    LOG0("sendBitrateToClient++");
    ASSERT_SESSION_LOCKED(mySession);

    LOG0return(mySession->websocketPipe<0,,"sendBitrateToClient-- (without sending because there is no websocket)");
    LOG0return(bitrate==0 || bitrate==0xFFFFFFFF,,"sendBitrateToClient-- (without sending because bitrate has invalid value: %u (hex: %08X)",
               (unsigned int)bitrate,(unsigned int)bitrate);
    LOG0return(bitrate>MAX_BITRATE,,"sendBitrateToClient-- (without sending because the bitrate is ridiculously big: %u (hex: %08X))",
               (unsigned int)bitrate,(unsigned int)bitrate);

    LOG0return(time(NULL)==mySession->lastSentPacketTime,,"sendBitrateToClient-- (without sending because last time we sent it was less than a second ago)");

    const int KBufLen=40; //this should be enough
    char buf[KBufLen];
    int bufLen=snprintf(buf+KWebsocketBufferOffset,KBufLen-KWebsocketBufferOffset,"{\"type\":\"br\",\"b\":%u}",(unsigned int)bitrate);

    enqueueWebsocketPacket(mySession,buf,bufLen,KWebsocketBufferOffset,0,1);
    LOG0("sendBitrateToClient-- (bitrate was: %u)",(unsigned int)bitrate);
}

static int getNumberOfActiveWebsockets(){
    unsigned int nrActiveWebsockets=0,i;
    struct ps_md_net const * mdNet=getMdNetRd();
    for(i=0;i<NR_PARALLEL_SESSIONS;i++){
        //there is no need to lock the session(s), what we need is an approx result
        struct ps_session *s=(struct ps_session *)&mdNet->sessions[i];
        //pthread_mutex_lock(&s->sessionMutex);
        //s->flags|=FlagSession_IsLocked;

        if(s->websocketPipe>=0)
            nrActiveWebsockets++;

        //s->flags&=~FlagSession_IsLocked;
        //pthread_mutex_unlock(&s->sessionMutex);
    }
    releaseMdNet(&mdNet);
    return nrActiveWebsockets;
}

static void sendWebsocketPacketToConnectedClients(char * const buf, uint32_t bufLen, uint32_t bufOffset){
    int r,i;
    ph_assert(bufOffset==KWebsocketBufferOffset,"sendWebsocketPacketToConnectedClients: offset does not have the required value.");

    r=createWebsocketTextPacket(buf,&bufLen,&bufOffset);
    ph_assert(!r,NULL);

    struct ps_md_net const * mdNet=getMdNetRd();
    for(i=0;i<NR_PARALLEL_SESSIONS;i++){
        struct ps_session *s=(struct ps_session *)&mdNet->sessions[i];
        enqueueWebsocketPacket(s,buf,bufLen,bufOffset,1,0);
    }
    releaseMdNet(&mdNet);
}

void sendStatusToConnectedClients(struct md_entry_id const * const __restrict impactedAlbum){
    ph_assert(impactedAlbum,NULL);

    int i,j,nrActiveWebsockets=0;
    unsigned int minNrCamerasKnownToClient=0xFFFF;
    struct ps_md_net const * mdNet=getMdNetRd();
    for(i=0;i<NR_PARALLEL_SESSIONS;i++){
        struct ps_session *s=(struct ps_session *)&mdNet->sessions[i];
        pthread_mutex_lock(&s->sessionMutex);
        s->flags|=FlagSession_IsLocked;

        if(s->websocketPipe>=0){
            nrActiveWebsockets++;
            if(s->nrCamerasSent<minNrCamerasKnownToClient)
                minNrCamerasKnownToClient=s->nrCamerasSent;
        }

        s->flags&=~FlagSession_IsLocked;
        pthread_mutex_unlock(&s->sessionMutex);
    }
    releaseMdNet(&mdNet);
    if(!nrActiveWebsockets)
        return; //no active websockets, nothing to do

    //if here, there is at least one active connection
    size_t cntBufLen;
    ph_assert(minNrCamerasKnownToClient<0xFFFF,NULL);
    char *cntBuf=getMetadataJsonString(&cntBufLen,KWebsocketBufferOffset,0,NULL);
    //put the buffer length in the beginning
    ph_assert(KWebsocketBufferOffset>=sizeof(int),NULL);
    sendWebsocketPacketToConnectedClients(cntBuf,cntBufLen,KWebsocketBufferOffset);
    free(cntBuf);
    cntBuf=NULL;

    //is there any client impacted by the current album change?
    mdNet=getMdNetRd();
    for(i=0;i<NR_PARALLEL_SESSIONS;i++){
        struct ps_session *s=(struct ps_session *)&mdNet->sessions[i];
        pthread_mutex_lock(&s->sessionMutex);
        s->flags|=FlagSession_IsLocked;

        if(s->websocketPipe>=0 /*&& !s->lastRequestedAlbumKey*/){
            struct ps_session const * const currentSession=s;
            LOG0("lastRequestedAlbumID.level=%d (for client i=%d)",currentSession->lastRequestedAlbumID.level,i);

            //are the paths overlapping? if not, there is no point in sending the client an update
            if(impactedAlbum->level>currentSession->lastRequestedAlbumID.level){
                s->flags&=~FlagSession_IsLocked;
                pthread_mutex_unlock(&s->sessionMutex);
                continue; //our album can NOT be impacted, because the impacted album is too many levels deep
            }

            for(j=0;j<impactedAlbum->level;j++){
                if(impactedAlbum->nameCRC[j]!=currentSession->lastRequestedAlbumID.nameCRC[j])
                    break; //the impacted album is not on our path
            }
            if(j<impactedAlbum->level && impactedAlbum->level>0){
                s->flags&=~FlagSession_IsLocked;
                pthread_mutex_unlock(&s->sessionMutex);
                continue;
            }

            //if we are here we should check if we need to resend the album to the client
            struct md_level_etagf const * const __restrict lraETag=&currentSession->lastRequestedAlbumETag;
            struct md_level_etagf newAlbumETagF;
            const int eTagSize=PHOTOSTOVIS_ETAGF_SIZE;
            char eTag[eTagSize];
            eTag[0]='\0';
            char reqETag[eTagSize];

            snprintf(reqETag,eTagSize,"%08X%08X%08X%08X%04X%04X",
                     lraETag->nameCRC,(unsigned int)lraETag->captureTime,(unsigned int)lraETag->sizeOrLatestCaptureTime,
                     (unsigned int)lraETag->modificationTime,(unsigned int)lraETag->flags,(unsigned int)lraETag->modificationCounter);

            size_t dataLen;
            char *data=getAlbumJsonHttp(impactedAlbum,NULL,reqETag,currentSession->lastRequestedAlbumNrThumbnailsPerRow,currentSession->pixelRatio,
                                         KWebsocketBufferOffset,eTag,eTagSize,&newAlbumETagF,&dataLen);
            LOG0("lraETAG: %s, returned ETAG: %s",reqETag,eTag);

            if(data && data!=reqETag){
                //we should send the buffer to the client
                enqueueWebsocketPacket(s,data,dataLen,KWebsocketBufferOffset,0,1);
                s->lastRequestedAlbumETag=newAlbumETagF;
                free(data);
            } else LOG0("Not sending album because it is the same");
        }//if(s->websocket>=0 && !s->lastRequestedAlbumKey)

        s->flags&=~FlagSession_IsLocked;
        pthread_mutex_unlock(&s->sessionMutex);
    }//for(i=0;i<NR_PARALLEL_SESSIONS;i++)
    releaseMdNet(&mdNet);
}


static void sendNrSessionsToConnectedClients(struct ps_session *session){
    int i;
    unsigned int nrSessions=0,nrActiveWebsockets=0;

    struct ps_md_net const * mdNet=getMdNetRd();
    for(i=0;i<NR_PARALLEL_SESSIONS;i++){
        struct ps_session *s=(struct ps_session *)&mdNet->sessions[i];
        if(s!=session){
            pthread_mutex_lock(&s->sessionMutex);
            s->flags|=FlagSession_IsLocked;
        } //session is already locked

        if(s->websocketPipe>=0)
            nrActiveWebsockets++;
        if(s->sessionId && s->nrConnectedThreads)
            nrSessions++;

        //nrActiveWebsockets and nrSessions can be different, because we can first create a session, and only after "a while" we have a websocket connected

        if(s!=session){
            s->flags&=~FlagSession_IsLocked;
            pthread_mutex_unlock(&s->sessionMutex);
        }
    }
    releaseMdNet(&mdNet);

    if(!nrActiveWebsockets)
        return; //no active websockets, nothing to do

    //if here, there is at least one active connection
    ph_assert(nrSessions>=1,NULL);

    const int KBufLen=40; //this should be enough
    char buf[KBufLen];
    uint32_t bufLen=snprintf(buf+KWebsocketBufferOffset,KBufLen-KWebsocketBufferOffset,"{\"type\":\"ss\",\"ss\":%u}",nrSessions);
    ph_assert(bufLen<KBufLen,NULL);
    uint32_t bufOffset=KWebsocketBufferOffset;

    int r=createWebsocketTextPacket(buf,&bufLen,&bufOffset);
    ph_assert(!r,NULL);

    if(session){
        ASSERT_SESSION_LOCKED(session);
        enqueueWebsocketPacket(session,buf,bufLen,bufOffset,0,0);
    } else {
        mdNet=getMdNetRd();
        for(i=0;i<NR_PARALLEL_SESSIONS;i++){
            struct ps_session *s=(struct ps_session *)&mdNet->sessions[i];
            enqueueWebsocketPacket(s,buf,bufLen,bufOffset,1,0);
        }
        releaseMdNet(&mdNet);
    }
}

void sendScanningStatusToConnectedClients(enum md_scanning_status status){
    if(!getNumberOfActiveWebsockets())
        return; //no active websockets, nothing to do

    //if here, there is at least one active connection
    const int KBufLen=50; //this should be enough
    char buf[KBufLen];
    int bufLen;
    //write the bitrate
    bufLen=snprintf(buf+KWebsocketBufferOffset,KBufLen-KWebsocketBufferOffset,"{\"type\":\"scanning\",\"status\":%u}",status);

    sendWebsocketPacketToConnectedClients(buf,bufLen,KWebsocketBufferOffset);
}
