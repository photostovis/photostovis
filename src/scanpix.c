#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <strings.h>
#ifndef __USE_XOPEN
#define __USE_XOPEN
#endif

#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <utime.h>

#include <errno.h>
#include <fcntl.h>

//#define _XOPEN_SOURCE 500
#ifndef __USE_XOPEN_EXTENDED
#define __USE_XOPEN_EXTENDED
#endif
#include <ftw.h>
#include <regex.h> //regular expressions
#include <arpa/inet.h> //ntohs
#include <limits.h> //PATH_MAX
#ifndef __APPLE__
#include <sys/inotify.h>
#endif

#include <libexif/exif-data.h>
#include <libiptcdata/iptc-data.h>
#include <libheif/heif.h>

#include <openssl/md5.h>

#define _GNU_SOURCE
#include <math.h> //defines NAN macro

#include "photostovis.h"

#include "log.h"

#define FILE_DATA_SHIFT_BUF_SIZE 1048576
#define TEMP_BUF_MIN_SIZE 1024
#define EXIF_DATETIME_SIZE 20
#define EXIF_DATE_SIZE 10
#define EXIF_LATLNG_TOKEN_DELIMITER " ,"
#define TIMEZONE_UNAVAILABLE 127
#define NR_TRACKED_TIMEZONES 4 //the number of how many timezones we track in an album

#define MD5_READ_BUF_SIZE 4096

#define LAT_LNG_ERROR 200*1000000
#define CAMERA_INDEX_NO_CAMERA_INFO 0xFFFF

#define ABS(x) ((x)<0?-(x):(x))

const char *KJpegExtension=".jpg";
const char *KJpegExtension2="jpeg";

const char *KHeicExtension="heic";
const char *KMp4Extension=".mp4";
const char *KM4vExtension=".m4v";
const char *KMovExtension=".mov";

const char *KAlbumFilename="album.dat";
const char *KDescriptionFilename="description.txt";
const char *KThumbnailsFilename="thumbnails1.dat";
const char *KCamerasFilename="cameras.dat";
const char *KPicturesFolder="/pictures";
const char *KUploadsFolder="/uploads";
//const char *KDuplicatesFolder="/duplicates"; -> not used
const char *KTranscodedFolder="/transcoded";

//static const char *KDatePattern="^([1-2][0-9]{3,3}[- _.]?)?((0[1-9])|(1[0-2]))[- _.]?(([0-2][1-9])|(3[0-1]))[- _.]?";
static const char *KDatePattern="^(IMG_)?(VID_)?([1-2][0-9]{3,3}[- _.]?)?((0[1-9])|(1[0-2]))[- _.]?(0[1-9]|([1-2][0-9])|(3[0-1]))([- _.]?[1-9])?[- _.]";
//static const char *KYearPattern="^([(18)(19)(20)][0-9][0-9] [- _]?)? [(0[1-9])(1[0-2])] [- _]? [([0-2][1-9])(3[0-1])] [- _]?";
////////////////////////////////|    18xx, 19xx, 20xx      |delim|  |     month        |delim |     day              |
/////////////////////////////////////////////////////////////////// ->the year is optional

static const char *KTimePattern="^(([0-1][0-9])|(2[0-3]))[- _.]?([0-5][0-9])[- _.]?([0-5][0-9])";



struct mdh_parsing {
    char picsDir[PATH_MAX];
    char dataDir[PATH_MAX];
    char *tempBuf;
    int tempBufSize;
    int tempBufLen;
    int tempBufPos;
    int picsDirLen;
    int dataDirLen;

    struct ps_md_pics const *mdPics;
    struct md_entry_id currentAlbum;

    //we need these 2 here because they are setup by the scandir filter function
    uint32_t nrTotalUnknowns;
    uint64_t sizeOfUnknownsInAlbum;

    struct stat statBuf;
    int nrAllocatedChunks;
    int sizeOfAllocatedChunks;
    int nrParsedAlbums;
    int nrLoadedAlbums;
    int nrReusedAlbums;
    int nrPicturesWithUnknownCamera;

    int nrCreatedThumbnails; //used for thumbnail scanning. How many thumbnails were actually created
    int nrCreatedThumbnailFiles; //used for thumbnail scanning. How many files we created
    int nrUntouchedThumbnails; //used for thumbnail scanning. How many thumbnails were left untouched (the whole file was ok)
    int nrUntouchedThumbnailFiles; //used for thumbnail scanning. How many thumbnail files were ok, not needing changes
    int nrReusedThumbnails; //used for thumbnail scanning. How many thumbnails we could reuse
    int thumbnailsCreationTimeMS;
    uint16_t thumbnailsCreationTimePerMPixelMS; //this is hopefully at most few hundreds ms

    uint8_t nrPermissions;
    uint8_t forcePicturesFullScan; //0=not necessary, 1=force scan, 2=force scan and delete allocated data

    uint64_t sizeOfCreatedThumbnailFiles; //used for thumbnail scanning.
    uint64_t sizeOfUntouchedThumbnailFiles; //used for thumbnail scanning.
    uint64_t sizeOfCachedData;
    //regex parsing (Date and Time)
    regex_t rxd;
    regmatch_t rxdm;
    regex_t rxt;
    regmatch_t rxtm;
};
//TODO: make a union from the above struct. We do not do parsing and thumbnails creation at the same time


extern uid_t photostovisUid;
extern gid_t photostovisGid;

////////////////////////////////////////////////////
// Global variables
////////////////////////////////////////////////////
static struct mdh_parsing *ptrParsing=NULL;
////////////////////////////////////////////////////

int createJpegFromHeif(struct heif_context *ctx, struct heif_image_handle *handle, const char *dstPath, char const * const srcPath, size_t* const dstSize);

static void mkdir_recursive(const char *dir, int mode) {
    char tmp[PATH_MAX];
    char *p = NULL;
    size_t len;

    snprintf(tmp, sizeof(tmp),"%s",dir);
    len = strlen(tmp);
    if(tmp[len - 1] == '/')
        tmp[len - 1] = 0;
    for(p = tmp + 1; *p; p++)
        if(*p == '/') {
            *p = 0;
            mkdir(tmp, mode);
            *p = '/';
        }
    mkdir(tmp, mode);
}

int dirService(const char *folder, int create){
    int r;
    struct stat statBuf;

    r=stat(folder,&statBuf);
    if(!r && S_ISDIR(statBuf.st_mode))return 0; //everything fine

    if(!create){ //if we do not create, we expect it to exist
        LOG0return(r,-1,"Cannot stat(%s). Is it an existing folder? Error: %s",folder,strerror(errno));
        LOG0return(!S_ISDIR(statBuf.st_mode),-1,"%s is not a directory.",folder);
    } else {
        //we have to create the folder
        r=mkdir(folder,0755);
        if(r){
            //try recursive
            mkdir_recursive(folder,0755);
            //can we stat it now?
            r=stat(folder,&statBuf);
            LOG0return(r,-1,"Cannot create folder recursively (%s).",folder);
            setOwnership(folder,1);
        } else
            setOwnership(folder,0);
    }
    return 0;
}

int _chown_cb(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf){
    chown(fpath,photostovisUid,photostovisGid);
    return 0;
}
int setOwnership(const char *folder, int recursive){
    if(photostovisUid==65534 && photostovisGid==65534)return 0;
    //if((myHardware&0xFF000000)!=0x91000000)//this setOwnership is only for cubietruck
    int err=chown(folder,photostovisUid,photostovisGid);
    LOG0return(err,-1,"chown-ing %s failed: %s",folder,strerror(errno));
    if(recursive)
        nftw(folder,&_chown_cb,5,FTW_DEPTH|FTW_PHYS);

    return 0;
}

int _unlink_cb(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf)
{
    int r=remove(fpath);
    //if(r)perror(fpath);
    LOG0c(r,"Removing %s returned %d",fpath,r);
    return 0;
}

int rmrf(const char *folder){
    return nftw(folder,&_unlink_cb,5,FTW_DEPTH|FTW_PHYS);
}

/*
void tabPrint(const int level)
{
    int i;
    for(i=0;i<level;i++)printf("  ");
}*/

static void trim_spaces(char *buf)
{
    int l=strlen(buf)-1;
    while(l>=0 && buf[l]==' ')buf[l--]='\0';
}

static void reallocate_temp_if_needed_helper(){
    ph_assert(ptrParsing,NULL);
    ph_assert(ptrParsing->tempBufSize-ptrParsing->tempBufPos<TEMP_BUF_MIN_SIZE,NULL);
    LOG0("Reallocating temp buffer from %d to %d bytes.",ptrParsing->tempBufSize,ptrParsing->tempBufSize+TEMP_BUF_MIN_SIZE);
    ptrParsing->tempBufSize+=TEMP_BUF_MIN_SIZE;
    ptrParsing->tempBuf=ph_realloc(ptrParsing->tempBuf,ptrParsing->tempBufSize);
}
#define REALLOCATE_TEMP_IF_NEEDED if(ptrParsing->tempBufSize-ptrParsing->tempBufPos<TEMP_BUF_MIN_SIZE)reallocate_temp_if_needed_helper()

void dateToString(int32_t captureTime, int8_t timezone, char *buf, int bufSize){
    time_t tt=captureTime;
    struct tm *t=gmtime(&tt);
    t->tm_gmtoff=timezone*15*60;
    strftime(buf,bufSize,"%Y-%m-%d %H:%M:%S,  %z",t);
}

struct mdh_album_data_by_permission *getAlbumDataByPermission(struct md_album_entry const *parentEntry){
    ph_assert(parentEntry->flags&FlagIsAlbum,NULL);
    ph_assert(parentEntry->album,NULL);
    struct mdh_album_data_by_permission *p=(struct mdh_album_data_by_permission *)((char*)(parentEntry->album+parentEntry->entriesIndex)+sizeof(void*)+4+16);
    return p;
}

struct mdh_album_data_by_permission *getAlbumDataByPermissionMdh(struct mdh_album_entry const *parentEntry){
    ph_assert(parentEntry->flags&FlagIsAlbum,NULL);
    struct mdh_album_entry_album *a=(struct mdh_album_entry_album *)(parentEntry+1);
    ph_assert(a->mdAlbum,NULL);
    struct mdh_album_data_by_permission *p=(struct mdh_album_data_by_permission *)((char*)(a->mdAlbum+parentEntry->entriesIndex)+sizeof(void*)+4+16);
    return p;
}

struct mdh_album_data_by_permission *getAlbumDataByPermissionMd(struct md_album_entry const *album, int nrEntries){
    struct mdh_album_data_by_permission *p=(struct mdh_album_data_by_permission *)((char*)(album+nrEntries)+sizeof(void*)+4+16);
    return p;
}

static int computeAlbumMD5(struct md_album_entry * const album, const int nrEntries, const int albumDataLen, const int nrPermissions, const int verify){
    //at this point the album data is almost fully computed.The only thing to do is to compute its md5
    int i,albumDataByPermissionSize=(nrPermissions+1)*sizeof(struct mdh_album_data_by_permission);
    MD5_CTX ctx;
    MD5_Init(&ctx);
    ph_assert(sizeof(struct md_album_entry)==32,NULL); //otherwhise change that 24 below
    for(i=0;i<nrEntries;i++){
        struct md_album_entry *ca=&(album[i]);
        MD5_Update(&ctx,ca,24); //we skip either the pointer to album or the lat+lng or md5
    }
    MD5_Update(&ctx,(char*)(album+nrEntries)+sizeof(void*)+4+16+albumDataByPermissionSize,
               albumDataLen-sizeof(void*)-4-16-albumDataByPermissionSize-nrEntries*sizeof(struct md_album_entry));
    unsigned char md5Data[MD5_DIGEST_LENGTH];
    MD5_Final(md5Data,&ctx);
    unsigned char *basePtr=(unsigned char*)(album+nrEntries)+sizeof(void*)+sizeof(uint32_t);

    if(verify){
        if(memcmp(md5Data,basePtr,MD5_DIGEST_LENGTH))
            return -1;
    } else {
        //we set it!
        memcpy(basePtr,md5Data,MD5_DIGEST_LENGTH);
    }
    return 0;
}

static void markAsBeingScanned(struct md_album_entry * const album, const uint16_t nrEntries, int level){
    int i;
    if(!album)return;
    for(i=0;i<nrEntries;i++)
        if(album[i].flags&FlagIsAlbum){
            album[i].flags|=FlagElmIsBeingScanned;
            LOG0c(album[i].album,"markAsBeingScanned level %d: marked album with %d entries (%s)",level+1,nrEntries,getNameFromAlbum(album,i));
            markAsBeingScanned(album[i].album,album[i].entriesIndex,level+1);
        }
}

static void checkIfStillMarkedAsBeingScanned(struct md_album_entry * const album, const uint16_t nrEntries){
    int i;
    if(!album)return;
    ph_assert(!(album->flags&FlagElmIsBeingScanned),"Album still marked as being scanned.");
    for(i=0;i<nrEntries;i++)
        if(album[i].flags&FlagIsAlbum)
            checkIfStillMarkedAsBeingScanned(album[i].album,album[i].entriesIndex);
}

/*
static void show_tag(ExifData *d, ExifIfd ifd, ExifTag tag)
{
    // See if this tag exists
    ExifEntry *entry = exif_content_get_entry(d->ifd[ifd],tag);
    if (entry) {
        char buf[1024];

        // Get the contents of the tag in human-readable form
        exif_entry_get_value(entry, buf, sizeof(buf));

        // Don't bother printing it if it's entirely blank
        trim_spaces(buf);
        if (*buf) {
            LOG0("%s: %s", exif_tag_get_name_in_ifd(tag,ifd), buf);
        }
    }
}*/

static void get_tag(char *buf, int bufLen, ExifData const * const d, const ExifIfd ifd, const ExifTag tag)
{
    ph_assert(buf,NULL);
    ph_assert(bufLen>0,NULL);
    /* See if this tag exists */
    ExifEntry *entry = exif_content_get_entry(d->ifd[ifd],tag);
    if(entry)
    {
        /* Get the contents of the tag in human-readable form */
        exif_entry_get_value(entry, buf, bufLen);
        trim_spaces(buf);
    }
    else *buf='\0';
}

static int get_tag_rational(ExifData const * const d, ExifIfd ifd, ExifTag tag, ExifRational *r)
{
    /* See if this tag exists */
    ExifEntry *entry = exif_content_get_entry(d->ifd[ifd],tag);
    if(entry)
    {
        ph_assert(entry->format==EXIF_FORMAT_RATIONAL,NULL);
        ExifByteOrder o=exif_data_get_byte_order((ExifData*)d);
        *r=exif_get_rational(entry->data, o);
        return 0;
    }
    r->denominator=r->numerator=0;
    return -1;
}

static int get_tag_short(ExifData const * const d, ExifIfd ifd, ExifTag tag, ExifShort *s)
{
    /* See if this tag exists */
    ExifEntry *entry = exif_content_get_entry(d->ifd[ifd],tag);
    if(entry)
    {
        ph_assert(entry->format==EXIF_FORMAT_SHORT,"%d",(int)entry->format);
        ExifByteOrder o=exif_data_get_byte_order((ExifData*)d);
        *s=exif_get_short(entry->data, o);
        return 0;
    }
    *s=0;
    return -1;
}
static int get_tag_uint(ExifData const * const d, ExifIfd ifd, ExifTag tag, unsigned int *s)
{
    /* See if this tag exists */
    ExifEntry *entry = exif_content_get_entry(d->ifd[ifd],tag);
    if(entry){
        if(entry->format==EXIF_FORMAT_SHORT){
            ExifByteOrder o=exif_data_get_byte_order((ExifData*)d);
            *s=exif_get_short(entry->data, o);
            return 0;
        } if(entry->format==EXIF_FORMAT_LONG){
            ExifByteOrder o=exif_data_get_byte_order((ExifData*)d);
            *s=exif_get_long(entry->data, o);
            return 0;
        } else {
            ph_assert(entry->format==EXIF_FORMAT_RATIONAL,"%d",(int)entry->format);
            ExifByteOrder o=exif_data_get_byte_order((ExifData*)d);
            ExifRational r=exif_get_rational(entry->data, o);
            *s=round((float)r.numerator/(float)r.denominator);
            LOG0("WARNING: EXIF RATIONAL (%u/%u) converted to integer (%u)",r.numerator,r.denominator,*s);
            return 0;
        }
    }
    *s=0;
    return -1;
}

static int32_t exifDateTime2UnixTime(const char *dtStr)
{
    struct tm t;
    memset(&t, 0, sizeof(struct tm));

    char *r=NULL;
    r=strptime(dtStr,"%Y:%m:%d %T",&t);
    if(!r){
        //some old phones have a strange date format
        LOG0("WARNING: Date with strange format: %s",dtStr);
        r=strptime(dtStr,"%Y:%m:%d:%T",&t);
        if(!r)return DATE_MAX;
    }

    if(*r!='\0'){
        LOG0("Date NOT successfully converted!! r=%s",r);
        return DATE_MAX;
    }
    return (int32_t)timegm(&t);
}

static int32_t getExifCaptureDate(char *tempBuf, ExifData const * const ed, char const * const name, int8_t *timezone){
    int32_t dt,captureTime=DATE_MAX;
    *timezone=TIMEZONE_UNAVAILABLE;
    get_tag(tempBuf,EXIF_DATETIME_SIZE,ed,EXIF_IFD_0,EXIF_TAG_DATE_TIME); //20 is the max length for date-time values
    get_tag(tempBuf+EXIF_DATETIME_SIZE,EXIF_DATETIME_SIZE,ed,EXIF_IFD_EXIF,EXIF_TAG_DATE_TIME_ORIGINAL);
    get_tag(tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE,EXIF_DATETIME_SIZE,ed,EXIF_IFD_EXIF,EXIF_TAG_DATE_TIME_DIGITIZED);
    if(strlen(tempBuf)==EXIF_DATETIME_SIZE-1 &&
            strlen(tempBuf+EXIF_DATETIME_SIZE)==EXIF_DATETIME_SIZE-1 &&
            strlen(tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE)==EXIF_DATETIME_SIZE-1 &&
            !strcmp(tempBuf,tempBuf+EXIF_DATETIME_SIZE) && !strcmp(tempBuf,tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE))
    {
        //all dates are present and they are the same. Digitize only once.
        captureTime=exifDateTime2UnixTime(tempBuf);
    } else {
        int32_t dt1,dt2,dt3;
        //possibly not all dates are present, and possibly not all are the same
        //we select the earliest date available
        if(strlen(tempBuf)==EXIF_DATETIME_SIZE-1){
            dt1=dt=exifDateTime2UnixTime(tempBuf);
            //LOG0("Date from tempBuf1: %s",tempBuf);
            if(dt<captureTime)
                captureTime=dt;
        } else dt1=DATE_MAX;
        if(strlen(tempBuf+EXIF_DATETIME_SIZE)==EXIF_DATETIME_SIZE-1){
            dt2=dt=exifDateTime2UnixTime(tempBuf+EXIF_DATETIME_SIZE);
            //LOG0("Date from tempBuf2: %s",tempBuf+EXIF_DATETIME_SIZE);
            if(dt<captureTime)
                captureTime=dt;
        } else dt2=DATE_MAX;
        if(strlen(tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE)==EXIF_DATETIME_SIZE-1){
            dt3=dt=exifDateTime2UnixTime(tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE);
            //LOG0("Date from tempBuf3: %s",tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE);
            if(dt<captureTime)
                captureTime=dt;
        } else dt3=DATE_MAX;
        //some phones (e.g. Moto G) have the DATE_TIME_DIGITIZED screwed up, and we want to avoid using it instead of the "right" date
        //on the other hand the DATE_TIME_DIGITIZED gets the capture date for edited pictures.
        //the next if catches the case when DATE_TIME_DIGITIZED is bigger than 1 year from the others
        //TODO: this is not a perfect solution
        if(dt1==dt2 && dt1!=dt3 && dt1!=DATE_MAX && dt3!=DATE_MAX && (dt1-dt3>31536000)){
            LOG0("DIGITIZED TIME is more than 1 year behind. Ignoring.");
            captureTime=dt1;
        }
    }

    if(name && captureTime==DATE_MAX && (strlen(tempBuf) || strlen(tempBuf+EXIF_DATETIME_SIZE) || strlen(tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE))){
        LOG0("WARNING: Malformed date:");
        LOG0("%s: #%s#%s#%s#",name,tempBuf,tempBuf+EXIF_DATETIME_SIZE,tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE);
    }

    //add timezone offset
    get_tag(tempBuf,EXIF_DATETIME_SIZE,ed,EXIF_IFD_EXIF,EXIF_TAG_TIME_ZONE_OFFSET);
    if(strlen(tempBuf)){
        LOG0("We have Timezone info in IFD_EXIF: %s",tempBuf);
        exit(0);
    }

    get_tag(tempBuf,EXIF_DATETIME_SIZE,ed,EXIF_IFD_0,EXIF_TAG_TIME_ZONE_OFFSET);
    if(strlen(tempBuf)){
        LOG0("We have Timezone info in IFD_0: %s",tempBuf);
        exit(0);
    }

    //GPS-based date
    get_tag(tempBuf,EXIF_DATE_SIZE+1,ed,EXIF_IFD_GPS,EXIF_TAG_GPS_DATE_STAMP);
    if(strlen(tempBuf)==EXIF_DATE_SIZE){
        //date string has correct size
        tempBuf[EXIF_DATE_SIZE]=' ';
        get_tag(tempBuf+EXIF_DATE_SIZE+1,EXIF_DATETIME_SIZE,ed,EXIF_IFD_GPS,EXIF_TAG_GPS_TIME_STAMP);
        if(strlen(tempBuf+EXIF_DATE_SIZE+1)>=8){

            if(strlen(tempBuf+EXIF_DATE_SIZE+1)>8){
                //adjust the time string
                tempBuf[EXIF_DATE_SIZE+1+8]='\0';
            }
            dt=exifDateTime2UnixTime(tempBuf);
            dt=captureTime-dt;
            LOG0("We have GPS date &time stamp: %s. Time difference (s) from picture time: %d",tempBuf,dt);
            int32_t rest=dt%3600;
            if(rest<360 && rest>-360)//we accept a 6 minutes clock error
                dt-=rest;
            else if(rest>3240)
                dt+=(3600-rest);
            else if(rest<-3240)
                dt+=(-3600-rest);
            else {
                LOG0("WARNING: Captured time and GPS time are too skewed from each other (more than 10%% of an hour). GPS time ignored.");
                return captureTime;
            }
            dt/=900; // 900=3600/4. dt represents the number of quarters of an hour
            if(dt<-48 || dt>52){
                LOG0("WARNING: Captured time and GPS time are too far from each other. GPS time ignored.");
                return captureTime;
            }

            *timezone=dt;
            LOG0("Detected timezone offset: %f",(float)dt/4.0f);
        }
    }
    return captureTime;
}
static void addShortenedCameraName(struct ps_camera *c, struct ps_md_pics *mdPicsW){
    //mdPicsW already locked in /owned in
    if(!c->modelOffset){
        c->shortenedMakeModelOffset=0xFFFF;
        return; //there is no camera maker, nothing to arrange
    }

    int l,newCameraModelOffset=0;
    char *newCameraName=NULL;
    char *cameraName=mdPicsW->fullMakeModelStrings+c->fullMakeModelOffset;
    char *cameraModel=cameraName+c->modelOffset;
    ph_assert(cameraModel-cameraName>0,NULL);
    ph_assert(*(char*)(cameraModel-1)==' ',NULL);

    const char *KNikonName="NIKON CORPORATION";
    const int KNikonNameLen=17;
    const int KNikonShortenStart=5,KNikonShortenEnd=17;

    const char *KOlympus1Name="OLYMPUS IMAGING CORP.";
    const int KOlympus1NameLen=21;
    const int KOlympus1ShortenStart=7,KOlympus1ShortenEnd=21;

    const char *KOlympus2Name="OLYMPUS OPTICAL CO.,LTD";
    const int KOlympus2NameLen=23;
    const int KOlympus2ShortenStart=7,KOlympus2ShortenEnd=23;

    const char *KPentaxName="PENTAX Corporation";
    const int KPentaxNameLen=18;
    const int KPentaxShortenStart=6,KPentaxShortenEnd=18;

    //change the maker
    if(!strncasecmp(cameraName,KNikonName,KNikonNameLen)){
        if(!newCameraName){
            newCameraName=ph_strdup(cameraName);
            newCameraModelOffset=c->modelOffset;
        }
        l=strlen(newCameraName);
        memmove(newCameraName+KNikonShortenStart,newCameraName+KNikonShortenEnd,l-newCameraModelOffset+2);
        newCameraModelOffset-=KNikonShortenEnd-KNikonShortenStart;
        ph_assert(newCameraModelOffset>0,NULL);
        ph_assert(*(newCameraName+newCameraModelOffset-1)==' ',NULL);
    }
    if(!strncasecmp(cameraName,KOlympus1Name,KOlympus1NameLen)){
        if(!newCameraName){
            newCameraName=ph_strdup(cameraName);
            newCameraModelOffset=c->modelOffset;
        }
        l=strlen(newCameraName);
        memmove(newCameraName+KOlympus1ShortenStart,newCameraName+KOlympus1ShortenEnd,l-newCameraModelOffset+2);
        newCameraModelOffset-=KOlympus1ShortenEnd-KOlympus1ShortenStart;
        ph_assert(newCameraModelOffset>0,NULL);
        ph_assert(*(newCameraName+newCameraModelOffset-1)==' ',NULL);
    }
    if(!strncasecmp(cameraName,KOlympus2Name,KOlympus2NameLen)){
        if(!newCameraName){
            newCameraName=ph_strdup(cameraName);
            newCameraModelOffset=c->modelOffset;
        }
        l=strlen(newCameraName);
        memmove(newCameraName+KOlympus2ShortenStart,newCameraName+KOlympus2ShortenEnd,l-newCameraModelOffset+2);
        newCameraModelOffset-=KOlympus2ShortenEnd-KOlympus2ShortenStart;
        ph_assert(newCameraModelOffset>0,NULL);
        ph_assert(*(newCameraName+newCameraModelOffset-1)==' ',NULL);
    }
    if(!strncasecmp(cameraName,KPentaxName,KPentaxNameLen)){
        if(!newCameraName){
            newCameraName=ph_strdup(cameraName);
            newCameraModelOffset=c->modelOffset;
        }
        l=strlen(newCameraName);
        memmove(newCameraName+KPentaxShortenStart,newCameraName+KPentaxShortenEnd,l-newCameraModelOffset+2);
        newCameraModelOffset-=KPentaxShortenEnd-KPentaxShortenStart;
        ph_assert(newCameraModelOffset>0,NULL);
        ph_assert(*(newCameraName+newCameraModelOffset-1)==' ',NULL);
    }
    //remove the maker name from the model name
    if(newCameraName){
        l=newCameraModelOffset;
        if(!strncmp(newCameraName,newCameraName+newCameraModelOffset,l)){
            if((newCameraName+newCameraModelOffset+l)[0]==' ')l++;//removes the space
            memmove(newCameraName+newCameraModelOffset,newCameraName+newCameraModelOffset+l,strlen(newCameraName+newCameraModelOffset)-l+1);
        }
    } else {
        l=c->modelOffset;
        if(!strncmp(cameraName,cameraModel,l)){
            if((cameraModel+l)[0]==' ')l++;//removes the space

            newCameraName=ph_strdup(cameraName);
            newCameraModelOffset=c->modelOffset;
            memmove(newCameraName+newCameraModelOffset,newCameraName+newCameraModelOffset+l,strlen(newCameraName+newCameraModelOffset)-l+1);
        }
    }

    //if we have something, add it
    if(newCameraName){
        l=strlen(newCameraName);
        mdPicsW->shortenedMakeModelStrings=(char*)ph_realloc(mdPicsW->shortenedMakeModelStrings,mdPicsW->shortenedMakeModelStringsLen+l+1);
        memcpy(mdPicsW->shortenedMakeModelStrings+mdPicsW->shortenedMakeModelStringsLen,newCameraName,l+1);
        c->shortenedMakeModelOffset=mdPicsW->shortenedMakeModelStringsLen;
        mdPicsW->shortenedMakeModelStringsLen+=l+1;
        free(newCameraName);
    } else c->shortenedMakeModelOffset=0xFFFF;
}

static int32_t addCameraMakerModel(char *makerModel, uint8_t modelOffset, float focalLength){
    int j,entriesIndex=CAMERA_INDEX_NO_CAMERA_INFO;
    int makerModelLen=strlen(makerModel);
    struct ps_camera * __restrict c;

    //check the make+model agains the makemodel array
    //but first we need a writable lock on mdPics
    ph_assert(ptrParsing->mdPics,NULL);
    releaseMdPics(&ptrParsing->mdPics);
    struct ps_md_pics *mdPicsW=getMdPicsWritable();
    for(j=0;j<mdPicsW->nrCameras;j++){
        c=&(mdPicsW->cameras[j]);
        if(modelOffset==c->modelOffset && !strcmp(mdPicsW->fullMakeModelStrings+c->fullMakeModelOffset,makerModel))
            break;
    }
    if(j>=mdPicsW->nrCameras){
        //we need to add this camera
        mdPicsW->cameras=(struct ps_camera*)ph_realloc(mdPicsW->cameras,(mdPicsW->nrCameras+1)*sizeof(struct ps_camera));
        c=&(mdPicsW->cameras[mdPicsW->nrCameras]);
        mdPicsW->fullMakeModelStrings=(char*)ph_realloc(mdPicsW->fullMakeModelStrings,mdPicsW->fullMakeModelStringsLen+makerModelLen+1);
        memcpy(mdPicsW->fullMakeModelStrings+mdPicsW->fullMakeModelStringsLen,makerModel,makerModelLen+1);
        c->fullMakeModelOffset=mdPicsW->fullMakeModelStringsLen;
        mdPicsW->fullMakeModelStringsLen+=makerModelLen+1;
        c->minFocalLength=focalLength;
        c->nrPictures=1;
        c->modelOffset=modelOffset;
        entriesIndex=mdPicsW->nrCameras;
        mdPicsW->nrCameras++;
        //do we have a short name?
        addShortenedCameraName(c,mdPicsW);
    } else {
        entriesIndex=j;
        c=&(mdPicsW->cameras[entriesIndex]);
        if(focalLength>0 && (focalLength<c->minFocalLength || !c->minFocalLength))
            c->minFocalLength=focalLength;
        c->nrPictures++;
    }
    releaseMdPicsW(&mdPicsW);
    ptrParsing->mdPics=getMdPicsRd();
    return entriesIndex;
}

static int32_t getExifCameraEntry(char *tempBuf, const int tempBufSize, ExifData const * const ed, float focalLength){
    int tempBufLen,entriesIndex=CAMERA_INDEX_NO_CAMERA_INFO;
    uint8_t modelOffset;
    //get the make and model
    get_tag(tempBuf,tempBufSize,ed,EXIF_IFD_0,EXIF_TAG_MAKE);
    tempBufLen=strlen(tempBuf);
    if(tempBufLen<tempBufSize-2){
        tempBuf[tempBufLen++]=' ';
        tempBuf[tempBufLen]='\0';
        modelOffset=tempBufLen;
        get_tag(tempBuf+tempBufLen,tempBufSize-tempBufLen,ed,EXIF_IFD_0,EXIF_TAG_MODEL);
        tempBufLen=strlen(tempBuf+tempBufLen);
        if(tempBufLen>1)
            entriesIndex=addCameraMakerModel(tempBuf,modelOffset,focalLength);
    };
    //done with the camera model
    return entriesIndex;
}

static int increaseCameraPicturesCount(const uint16_t index){
    if(index==CAMERA_INDEX_NO_CAMERA_INFO){
        ptrParsing->nrPicturesWithUnknownCamera++;
        return 0;
    }
    int r=0;
    //but first we need a writable lock on mdPics
    ph_assert(ptrParsing->mdPics,NULL);
    releaseMdPics(&ptrParsing->mdPics);
    struct ps_md_pics *mdPicsW=getMdPicsWritable();
    if(index<mdPicsW->nrCameras)
        mdPicsW->cameras[index].nrPictures++;
    else {
        r=-1;
        LOG0("WARNING: Invalid camera index. We should reparse everything.");
    }
    releaseMdPicsW(&mdPicsW);
    ptrParsing->mdPics=getMdPicsRd();
    return r;
}

static int32_t parseExifLatLong(char *latlngStr,int isLatitude){
    LOG0("parseExifLatLong: %s",latlngStr);
    char *endptr=NULL;
    char *token=strtok(latlngStr,EXIF_LATLNG_TOKEN_DELIMITER);
    double ll1=0,ll2=0,ll3=0;
    int32_t r; //returned value



    if(token && strcmp(token,"0/0")){
        ll1=strtod(token,&endptr);
#ifndef NDEBUG
        ph_assert(*endptr=='\0',"Token 1 (%s) not parsed completely",token);
#else
        LOG0c(*endptr!='\0',"Token 1 (%s) not parsed completely",token);
#endif

        token=strtok(NULL,EXIF_LATLNG_TOKEN_DELIMITER);
        if(token){
            ll2=strtod(token,&endptr);
#ifndef NDEBUG
            ph_assert(*endptr=='\0',"Token 2 (%s) not parsed completely",token);
#else
            LOG0c(*endptr!='\0',"Token 2 (%s) not parsed completely",token);
#endif

            ll1+=ll2/60.0;

            token=strtok(NULL,EXIF_LATLNG_TOKEN_DELIMITER);
            if(token){
                ll3=strtod(token,&endptr);
#ifndef NDEBUG
                ph_assert(*endptr=='\0',"Token 3 (%s) not parsed completely",token);
#else
                LOG0c(*endptr!='\0',"Token 3 (%s) not parsed completely",token);
#endif

                ll1+=ll3/3600.0;
            }
        }
    } else return LAT_LNG_ERROR;
    r=(int32_t)(ll1*1000000);
    //check for the range
    if(isLatitude){
#ifndef NDEBUG
        ph_assert(r<=90000000,NULL);
        ph_assert(r>=0,NULL);
#else
        if(r>90000000 || r<0)return LAT_LNG_ERROR;
#endif
    } else {
#ifndef NDEBUG
        ph_assert(r<=180000000,NULL);
        ph_assert(r>=0,NULL);
#else
        if(r>180000000 || r<0)return LAT_LNG_ERROR;
#endif
    }
    return r;
}

static void getExifGPSCoordinates(char *tempBuf, const int tempBufSize, ExifData const * const ed, int32_t *lat, int32_t *lng, char const * const name){
    //we do not use the GPS_VERSION anymore to check for GPS info in EXIF (the commented line below) because iPhone does NOT have the version info.
    //get_tag(tempBuf,tempBufSize,ed,EXIF_IFD_GPS,EXIF_TAG_GPS_VERSION_ID);
    get_tag(tempBuf,tempBufSize,ed,EXIF_IFD_GPS,EXIF_TAG_GPS_LATITUDE);
    if(strlen(tempBuf)>0){

        LOG0("getExifGPSCoordinates: WE HAVE LATITUDE IN EXIF YEEEIIII");
        //get the latitude in decimal format
        //get_tag(tempBuf,tempBufSize,ed,EXIF_IFD_GPS,EXIF_TAG_GPS_LATITUDE); -> no need, we already have the info.
        *lat=parseExifLatLong(tempBuf,1);
        if(*lat!=LAT_LNG_ERROR){
            get_tag(tempBuf,tempBufSize,ed,EXIF_IFD_GPS,EXIF_TAG_GPS_LATITUDE_REF);
            if(tempBuf[0]=='S' || tempBuf[0]=='s')*lat=-(*lat);

            //get the longitude in decimal format
            get_tag(tempBuf,tempBufSize,ed,EXIF_IFD_GPS,EXIF_TAG_GPS_LONGITUDE);
            *lng=parseExifLatLong(tempBuf,0);
            if(*lng!=LAT_LNG_ERROR){
                get_tag(tempBuf,tempBufSize,ed,EXIF_IFD_GPS,EXIF_TAG_GPS_LONGITUDE_REF);
                if(tempBuf[0]=='W' || tempBuf[0]=='w')*lng=-(*lng);
                //if we are here, we have both GPS coordinates
                LOG0("getExifGPSCoordinates: WE HAVE BOTH GPS COORDINATES");
                return;
            }
        }

        //if here, there was an error with parsing
#ifndef NDEBUG
        get_tag(tempBuf,tempBufSize,ed,EXIF_IFD_GPS,EXIF_TAG_GPS_LATITUDE);
        if(strlen(tempBuf)){
            LOG0("DEBUG: There was an error interpretting the GPS data for this picture (%s)",name);

            *lat=parseExifLatLong(tempBuf,1);
            LOG0("DEBUG: Latitude string: %s, latitude number: %d",tempBuf,*lat);

            get_tag(tempBuf,tempBufSize,ed,EXIF_IFD_GPS,EXIF_TAG_GPS_LONGITUDE);
            *lng=parseExifLatLong(tempBuf,0);
            LOG0("DEBUG: Longitude string: %s, longitude number: %d",tempBuf,*lng);
        }
#endif
    };
    *lat=*lng=LAT_LNG_ERROR;
    LOG0("getExifGPSCoordinates: NO GPS COORDINATES");
}

static int parsePictureExif(ExifData const * const ed, struct mdh_album_entry* const restrict c, struct mdh_album_entry_picture* const restrict cp,
                            struct md_album_entry* const restrict tempMdAlbumEntry, char const * const pictureName){
    //get the camera date first, so we can sort it
    c->captureTime=getExifCaptureDate(ptrParsing->tempBuf,ed,pictureName,&cp->timezone);
#ifndef NDEBUG
    if(c->captureTime==DATE_MAX)
        LOG0("DEBUG: EXIF capture time: none (%s)",pictureName);
    else if(cp->timezone==TIMEZONE_UNAVAILABLE)
        LOG0("DEBUG: EXIF capture time: %d. (%s)",c->captureTime,pictureName);
    else
        LOG0("DEBUG: EXIF capture time: %d. Timezone: %f (%s)",c->captureTime,cp->timezone/4.0f,pictureName);
#endif
    //do we have a timezone?
    ph_assert(cp->timezone==TIMEZONE_UNAVAILABLE || (cp->timezone>=-48 && cp->timezone<=52),NULL);

    if(ed->data && ed->size){
        c->flags=FlagHasExifThumbnail;
        tempMdAlbumEntry->flags|=FlagHasExifThumbnail;
    } else {
        c->flags=0;
        LOG0("Found photo with EXIF but without thumbnail! (%s)",ptrParsing->picsDir);
    }

    //check for orientation
    get_tag(ptrParsing->tempBuf,ptrParsing->tempBufSize,ed,EXIF_IFD_0,EXIF_TAG_ORIENTATION);
    if(strlen(ptrParsing->tempBuf)>=8){
        //it can either be e.g. Top-left or top - left
        if(!strcmp(ptrParsing->tempBuf,"Top-left") || !strcmp(ptrParsing->tempBuf,"top - left"));
        else if(!strcmp(ptrParsing->tempBuf,"Left-bottom") || !strcmp(ptrParsing->tempBuf,"left - bottom"))
            c->flags|=FlagRotatePlus90+FlagRotatePlus180;
        else if(!strcmp(ptrParsing->tempBuf,"Bottom-right") || !strcmp(ptrParsing->tempBuf,"bottom - right"))
            c->flags|=FlagRotatePlus180;
        else if(!strcmp(ptrParsing->tempBuf,"Right-top") || !strcmp(ptrParsing->tempBuf,"right - top"))
            c->flags|=FlagRotatePlus90;
        else LOG0("UNKNOWN ORIENTATION: #%s#",ptrParsing->tempBuf);
    } else ph_assert(strlen(ptrParsing->tempBuf)==0,"UNKNOWN ORIENTATION (2): %s",ptrParsing->tempBuf);
    //LOG0("Flags: %d (%s)",c->flags,ptrParsing->tempBuf);

    //check for GPS data
    getExifGPSCoordinates(ptrParsing->tempBuf,ptrParsing->tempBufSize,ed,&cp->lat,&cp->lng,pictureName);
    if(cp->lat!=LAT_LNG_ERROR){
        ph_assert(cp->lng!=LAT_LNG_ERROR,NULL);
        c->flags|=FlagPhotoHasGPSInfo;
    }
    //GPS data done

    //Check for various image-related data
    ExifRational exifRat;
    ExifShort exifShort;
    //Focal length
    get_tag_rational(ed,EXIF_IFD_EXIF,EXIF_TAG_FOCAL_LENGTH,&exifRat);
    //if no focal length, it defaults to 0/0, so assignment below is safe
    if(exifRat.denominator)
        cp->focalLength=(float)exifRat.numerator/(float)exifRat.denominator;
    else
        cp->focalLength=0.0f;
    //LOG0("FOCAL LENGTH: %f (%u/%u)",cp->focalLength,(unsigned int)exifRat.numerator,(unsigned int)exifRat.denominator);

    //get the make and model
    c->entriesIndex=getExifCameraEntry(ptrParsing->tempBuf,ptrParsing->tempBufSize,ed,cp->focalLength);
    if(c->entriesIndex==CAMERA_INDEX_NO_CAMERA_INFO)
        ptrParsing->nrPicturesWithUnknownCamera++;

    //Focal length 35mm (int 16bit)
    unsigned int focalLength35;
    get_tag_uint(ed,EXIF_IFD_EXIF,EXIF_TAG_FOCAL_LENGTH_IN_35MM_FILM,&focalLength35);
    cp->focalLength35=focalLength35; //if not found, defaults to 0

    //exposure
    get_tag_rational(ed,EXIF_IFD_EXIF,EXIF_TAG_EXPOSURE_TIME,&exifRat);
    if(exifRat.denominator)
        cp->exposureTime=(float)exifRat.numerator/(float)exifRat.denominator;
    else
        cp->exposureTime=0.0f;

    //exposure program
    get_tag_short(ed,EXIF_IFD_EXIF,EXIF_TAG_EXPOSURE_PROGRAM,&exifShort);
    cp->exposureProgram=exifShort; //if not found, defaults to 0, which is fine

    //F number
    get_tag_rational(ed,EXIF_IFD_EXIF,EXIF_TAG_FNUMBER,&exifRat);
    if(exifRat.denominator)
        cp->fNumber=(float)exifRat.numerator/(float)exifRat.denominator;
    else
        cp->fNumber=0.0f;

    //ISO
    get_tag_short(ed,EXIF_IFD_EXIF,EXIF_TAG_ISO_SPEED_RATINGS,&exifShort);
    cp->iso=exifShort; //if not found, defaults to 0, which is fine

    /*
show_tag(ed, EXIF_IFD_0, EXIF_TAG_ARTIST);
show_tag(ed, EXIF_IFD_0, EXIF_TAG_XP_AUTHOR);
show_tag(ed, EXIF_IFD_0, EXIF_TAG_COPYRIGHT);


show_tag(ed, EXIF_IFD_EXIF, EXIF_TAG_USER_COMMENT);
show_tag(ed, EXIF_IFD_0, EXIF_TAG_IMAGE_DESCRIPTION);
show_tag(ed, EXIF_IFD_1, EXIF_TAG_IMAGE_DESCRIPTION);
*/



    //we are done with EXIF data
    //LOG0("##########################################################");
    //exif_data_dump(ed);
    //show_tag(ed, EXIF_IFD_0, EXIF_TAG_MAKE);
    return 0;
}

static int initPictureFromSaved(struct mdh_album_entry* const restrict c,struct mdh_album_entry_picture* const restrict cp,
                            struct md_album_entry* const restrict tempMdAlbumEntry, char const * const pictureName,
                            struct md_album_entry const * const a, struct mdx_album_entry const * const ax){
    LOG0("Found reusable info about the current picture (%s/%s)",ptrParsing->picsDir,pictureName);
    c->size=a->size;
    c->captureTime=a->captureTime;
    c->flags=a->flags;
    if(c->flags&FlagHasExifThumbnail)
        tempMdAlbumEntry->flags|=FlagHasExifThumbnail;
    c->entriesIndex=a->entriesIndex;
    int r=increaseCameraPicturesCount(c->entriesIndex);

    LOG0return(r==-1,-1,"ERROR: camera not found.");

    cp->focalLength=ax->focalLength;
    cp->exposureTime=ax->exposureTime;
    cp->fNumber=ax->fNumber;
    cp->lat=a->lat;
    cp->lng=a->lng;
    cp->width=ax->width;
    cp->height=ax->height;
    cp->focalLength35=ax->focalLength35;
    cp->iso=ax->iso;
    cp->exposureProgram=ax->exposureProgram;
    cp->timezone=ax->timezone;
    return 0;
}

uint32_t static getDateTimeFromString(char const * const restrict name, const int checkDate, const int getTime){
    int r,y=0,m=0,d=0,h=0,min=0,s=0;
    char c, *p=(char *)name;
    uint32_t dt=DATE_MAX;

    if(checkDate){
        r=regexec(&ptrParsing->rxd,name,1,&ptrParsing->rxdm,0);
        ph_assert(r || ptrParsing->rxtm.rm_so==0,NULL); //this means that, if there is a match, it starts from beginning of the string
        if(r)return DATE_MAX;
    }
    if(!strncasecmp(p,"IMG_",4) || !strncasecmp(p,"VID_",4))
        p+=4;

    if(strlen(p)>=4){
        //try to find the year
        c=p[4];
        p[4]='\0';
        y=strtol(p,NULL,10);
        p[4]=c;
        p+=4;
        if(c==' ' || c=='-' || c=='_' || c=='.')p++;
    }
    if(y>1800 && y<2099 && strlen(p)>2){
        //try to find the month
        c=p[2];
        p[2]='\0';
        m=strtol(p,NULL,10);
        p[2]=c;
        p+=2;
        if(c==' ' || c=='-' || c=='_' || c=='.')p++;
    }
    if(m>0 && m<=12 && strlen(p)>2){
        //try to find the day
        c=p[2];
        p[2]='\0';
        d=strtol(p,NULL,10);
        p[2]=c;
        p+=2;
        if(c==' ' || c=='-' || c=='_' || c=='.')p++;
    }
    if(d>0 && d<32){
        struct tm tm;
        time_t ect;
        memset(&tm,0,sizeof(struct tm));
        tm.tm_year=y-1900;
        tm.tm_mon=m-1;//0-11 range
        tm.tm_mday=d;
        ph_assert(tm.tm_sec==0,NULL);
        ph_assert(tm.tm_min==0,NULL);
        ph_assert(tm.tm_hour==0,NULL);
        ect=timegm(&tm);
        if(ect!=(time_t)-1){
            LOG0("Found date: %d-%d-%d",y,m,d);
            dt=(uint32_t)ect;
        } else LOG0("INVALID FOUND DATE: %d-%d-%d",y,m,d);
    }
    if(dt==DATE_MAX || !getTime){
        if(strlen(name)==4 && y>=1800 && y<2099){
            //this is only a year. Set the time to 1-1-yyyy
            struct tm tm;
            time_t ect;
            memset(&tm,0,sizeof(struct tm));
            tm.tm_year=y-1900;
            tm.tm_mon=0;//0-11 range
            tm.tm_mday=1;
            ph_assert(tm.tm_sec==0,NULL);
            ph_assert(tm.tm_min==0,NULL);
            ph_assert(tm.tm_hour==0,NULL);
            ect=timegm(&tm);
            if(ect!=(time_t)-1)
                dt=(uint32_t)ect;
        }
        return dt; //no point in continuing if parsing was unsuccessful, or if we do not wish to have the time
    }

    //time parsing
    r=regexec(&ptrParsing->rxt,p,1,&ptrParsing->rxtm,0);
    ph_assert(r || ptrParsing->rxtm.rm_so==0,NULL); //this means that, if there is a match, it starts from beginning of the string
    if(r)return DATE_MAX;

    if(strlen(p)>2){
        //try to find the hour
        c=p[2];
        p[2]='\0';
        h=strtol(p,NULL,10);
        p[2]=c;
        p+=2;
        if(c==' ' || c=='-' || c=='_' || c=='.')p++;
    }
    if(h>=0 && h<24 && strlen(p)>2){
        //try to find the minute
        c=p[2];
        p[2]='\0';
        min=strtol(p,NULL,10);
        p[2]=c;
        p+=2;
        if(c==' ' || c=='-' || c=='_' || c=='.')p++;
    }
    if(min>=0 && min<60 && strlen(p)>=2){
        //try to find the seconds
        c=p[2];
        p[2]='\0';
        s=strtol(p,NULL,10);
        p[2]=c;
        p+=2;
        if(c==' ' || c=='-' || c=='_' || c=='.')p++;
    }
    if(s>=0 && s<60){
        struct tm tm;
        time_t ect;
        memset(&tm,0,sizeof(struct tm));
        tm.tm_year=y-1900;
        tm.tm_mon=m-1;//0-11 range
        tm.tm_mday=d;
        tm.tm_sec=s;
        tm.tm_min=min;
        tm.tm_hour=h;
        ect=timegm(&tm);
        if(ect!=(time_t)-1){
            LOG0("Found date & time: %d-%d-%d %2d:%2d:%2d",y,m,d,h,min,s);
            dt=(uint32_t)ect;
        } else LOG0("INVALID FOUND TIME: %2d:%2d:%2d",h,min,s);
    }
    return dt;
}

static int initForPictureWithoutExif(struct mdh_album_entry* const restrict c,struct mdh_album_entry_picture* const restrict cp, char const * const pictureName){
    //LOG0("%s has no EXIF data", d->d_name);
    c->captureTime=getDateTimeFromString(pictureName,1,1);
    //set some other vars
    c->entriesIndex=CAMERA_INDEX_NO_CAMERA_INFO;
    ptrParsing->nrPicturesWithUnknownCamera++;
    c->flags=0; //we do not have much data
    cp->focalLength35=0;
    cp->iso=0;
    cp->exposureProgram=0;
    cp->timezone=TIMEZONE_UNAVAILABLE;
    cp->focalLength=0.0f;
    cp->exposureTime=0.0f;
    cp->fNumber=0.0f;
    return 0;
}

static int parseVideoCreationDate(char const * const creationStr,struct mdh_album_entry* const restrict c, struct mdh_album_entry_video* const restrict cv){
    char *timezoneString;
    struct tm t;

    timezoneString=strptime(creationStr,"%Y-%m-%dT%H:%M:%S",&t);
    if(!timezoneString){
        LOG0("creation parsing failed (%s)",creationStr);
        ph_assert_debug(1,"Creation parsing failed");
        return -1;
    } else {
        //parsing succeeded, we should parse the timezone now
        int tzHH=-1,tzMM=-1;
        if(!strcmp(timezoneString,".000000Z"))
            cv->timezone=tzHH=tzMM=0;
        else {
            int r=sscanf(timezoneString,"%d:%d",&tzHH,&tzMM);
            if(r!=2 || tzHH>14 || tzHH<-12 || tzMM<0 || tzMM>60){
                r=sscanf(timezoneString,"%d",&tzHH);
                if(r!=1 || tzHH>1400 || tzHH<-1300){
                    LOG0("parsing timezone (%s) failed: %d:%d",timezoneString,tzHH,tzMM);
                    ph_assert_debug(1,"Timezone parsing failed");

                    //continue without timezone
                    cv->timezone=TIMEZONE_UNAVAILABLE;
                } else {
                    cv->timezone=tzHH/15.0;
                    tzMM=tzHH%100;
                    tzHH/=100;
                }
            } else {
                //we have datetime & timezone
                if(tzHH<0)
                    cv->timezone=tzHH*4-tzMM/15.0;
                else
                    cv->timezone=tzHH*4+tzMM/15.0;
            }
        }

        //continue with the datetime parsing
        c->captureTime=(int32_t)timegm(&t);
        LOG0("VIDEO creation: %d-%02d-%02d %d:%02d:%02d tz: %d:%02d",t.tm_year+1900,t.tm_mon+1,t.tm_mday,t.tm_hour,t.tm_min,t.tm_sec,tzHH,tzMM);
    }
    return 0;
}

static int parseVideoGPSdata(char const * const locationStr,struct mdh_album_entry* const restrict c, struct mdh_album_entry_video* const restrict cv){
    //parse the location
    float lat,lng,alt;
    int r=sscanf(locationStr,"%f%f%f",&lat,&lng,&alt);
    if(r!=3 || lat<-90 || lat>90 || lng<-180 || lng>180){
        LOG0("Location parsing failed (%s): r=%d, lat=%f, lng=%f",locationStr,r,lat,lng);
        ph_assert_debug(1,"Location parsing failed");
    } else {
        //parsing succeeded!
        cv->lat=lat*1000000;
        cv->lng=lng*1000000;
        c->flags|=FlagPhotoHasGPSInfo;
        LOG0("VIDEO location: lat: %f, lng: %f",lat,lng);
    }
    return 0;
}

static int ffprobeVideo(char const * const restrict filename, struct mdh_album_entry* const restrict c, struct mdh_album_entry_video* const restrict cv){
    //ffprobe command
    const size_t KCommandSize=1000;
    const size_t KLineSize=256;
    char ffprobeCmd[KCommandSize];
    const char *KCmdFfprobe="%s -v error -show_format -show_streams \"%s\"";
    snprintf(ffprobeCmd,KCommandSize,KCmdFfprobe,KFFprobeBin,filename);


    FILE *f=popen(ffprobeCmd,"r");
    if(!f){
        LOG0("popen/ffprobe command (%s) failed.",ffprobeCmd);
        cv->width=cv->height=0; //unknown
        return -1;
    } else LOG0("ffprobe command: %s",ffprobeCmd);
    ph_assert(f,NULL);
    char line[KLineSize];
    int inFormat=0,inStream=0,isVideoStream=0;

    char maker[KLineSize],model[KLineSize];
    maker[0]=model[0]='\0';

    while(fgets(line,KLineSize,f)){
        if(!strncmp(line,"[FORMAT]",8)){
            //entering format
            inFormat=1;
            continue;
        }
        if(!strncmp(line,"[/FORMAT]",9)){
            //exiting format
            inFormat=0;
            continue;
        }

        if(!strncmp(line,"[STREAM]",8)){
            //entering stream
            inStream=1;
            continue;
        }
        if(!strncmp(line,"[/STREAM]",9)){
            //exiting stream
            inStream=0;
            isVideoStream=0; //reset it
            continue;
        }

        if(inFormat){
            //check for make tag
            const char *KTagMake="TAG:com.apple.quicktime.make=";
            const int KTagMakeLen=strlen(KTagMake);
            if(!strncmp(line,KTagMake,KTagMakeLen)){
                strcpy(maker,line+KTagMakeLen);
                trim_string_right(maker);
                LOG0("VIDEO maker (TAG:com.apple.quicktime.make): %s",maker);
            }
            const char *KTagMake2="TAG:make=";
            const int KTagMake2Len=strlen(KTagMake2);
            if(!strncmp(line,KTagMake2,KTagMake2Len)){
                strcpy(maker,line+KTagMake2Len);
                trim_string_right(maker);
                LOG0("VIDEO maker (TAG:make): %s",maker);

                if(!strlen(model))strcpy(model,"(unknown)");
            }

            //check for model tag
            const char *KTagModel="TAG:com.apple.quicktime.model=";
            const int KTagModelLen=strlen(KTagModel);
            if(!strncmp(line,KTagModel,KTagModelLen)){
                strcpy(model,line+KTagModelLen);
                trim_string_right(model);
                LOG0("VIDEO model: %s",model);
            }

            //check for location tag
            const char *KTagLocation="TAG:com.apple.quicktime.location.ISO6709=";
            const int KTagLocationLen=strlen(KTagLocation);
            if(!strncmp(line,KTagLocation,KTagLocationLen)){
                char location[KLineSize];
                strcpy(location,line+KTagLocationLen);
                trim_string_right(location);
                parseVideoGPSdata(location,c,cv);
            }
            //check for location tag (generic)
            const char *KTagLocation2="TAG:location=";
            const int KTagLocation2Len=strlen(KTagLocation2);
            if(!strncmp(line,KTagLocation2,KTagLocation2Len)){
                char location[KLineSize];
                strcpy(location,line+KTagLocation2Len);
                trim_string_right(location);
                parseVideoGPSdata(location,c,cv);
            }

            //check for creation tag (Apple)
            const char *KTagCreationApple="TAG:com.apple.quicktime.creationdate=";
            const int KTagCreationAppleLen=strlen(KTagCreationApple);
            if(!strncmp(line,KTagCreationApple,KTagCreationAppleLen)){
                char creation[KLineSize];
                strcpy(creation,line+KTagCreationAppleLen);
                trim_string_right(creation);
                //parse the creation date
                LOG0("Parsing date in TAG:com.apple.quicktime.creationdate");
                parseVideoCreationDate(creation,c,cv);
            }
            //check for date tag
            const char *KTagDate="TAG:date=";
            const int KTagDateLen=strlen(KTagDate);
            if(!strncmp(line,KTagDate,KTagDateLen)){
                char creation[KLineSize];
                strcpy(creation,line+KTagDateLen);
                trim_string_right(creation);
                //parse the creation date
                LOG0("Parsing date in TAG:date");
                parseVideoCreationDate(creation,c,cv);
            }
            //check for creation time (generic) -> not really working, gives file creation instead of picture creation, on Apple at least
            /*
            const char *KTagCreation="TAG:creation_time=";
            const int KTagCreationLen=strlen(KTagCreation);
            if(!strncmp(line,KTagCreation,KTagCreationLen)){
                char creation[KLineSize];
                strcpy(creation,line+KTagCreationLen);
                trim_string_right(creation);
                //parse the creation date
                LOG0("Parsing date in TAG:creation_time=");
                parseVideoCreationDate(creation,c,cv);
            }*/

            //check for duration
            const char *KDuration="duration=";
            const int KDurationLen=strlen(KDuration);
            if(!strncmp(line,KDuration,KDurationLen)){
                char *duration=line+KDurationLen;
                trim_string(&duration);
                float durationFloat=strtof(duration,NULL);
                cv->duration=(int)roundf(durationFloat);
            }
        }//if(inFormat)

        if(inStream){
            //check for codec type
            const char *KCodecType="codec_type=";
            const int KCodecTypeLen=strlen(KCodecType);
            if(!strncmp(line,KCodecType,KCodecTypeLen)){
                char *codecType=line+KCodecTypeLen;
                trim_string(&codecType);
                if(!strcmp(codecType,"video"))
                    isVideoStream=1;
            }

            //check for width and height
            if(isVideoStream){
                //check width
                const char *KWidth="width=";
                const int KWidthLen=strlen(KWidth);
                if(!strncmp(line,KWidth,KWidthLen)){
                    char *width=line+KWidthLen;
                    trim_string(&width);
                    cv->width=strtol(width,NULL,10);
                }
                //check height
                const char *KHeight="height=";
                const int KHeightLen=strlen(KHeight);
                if(!strncmp(line,KHeight,KHeightLen)){
                    char *height=line+KHeightLen;
                    trim_string(&height);
                    cv->height=strtol(height,NULL,10);
                }
            }
        }
    }
    pclose(f);

    //do we have a maker+model to add?
    if(strlen(maker) && strlen(model)){
        ph_assert(strlen(maker)+strlen(model)+1<KLineSize,NULL);
        int l=strlen(maker);
        maker[l++]=' ';
        strcpy(maker+l,model);
        c->entriesIndex=addCameraMakerModel(maker,l,0);
    }

    return 0;
}

int isdir_filter(const struct dirent *d)
{
    //LOG0("isdir_filter d_type=%d (%d)",d->d_type,DT_DIR);
    int d_type=DT_UNKNOWN;
    if(d->d_type==DT_UNKNOWN){
        //we must find the type
        int l=strlen(d->d_name);
        ph_assert(strlen(ptrParsing->dataDir)==ptrParsing->dataDirLen,NULL);
        ph_assert(ptrParsing->dataDir[ptrParsing->dataDirLen-1]=='/',NULL);
        memcpy(ptrParsing->dataDir+ptrParsing->dataDirLen,d->d_name,l+1);

        if(!stat(ptrParsing->dataDir,&ptrParsing->statBuf)){
            //stat succeeded
            if(S_ISREG(ptrParsing->statBuf.st_mode))d_type=DT_REG;
            else if(S_ISDIR(ptrParsing->statBuf.st_mode))d_type=DT_DIR;
            else ph_assert(0,"Cannot find entry type. ");
        } else {
            //stat failing is not a good sign
            ptrParsing->dataDir[ptrParsing->dataDirLen]='\0';
            return 0; //skipping this file
        }
        ptrParsing->dataDir[ptrParsing->dataDirLen]='\0';
    }


    if(d->d_type==DT_DIR || d_type==DT_DIR)
        if(d->d_name[0]=='.' || d->d_name[0]=='@')return 0;//we filter out ., .. and all hidden folders. AND WE ALSO FILTER OUT SYNOLOGY-CREATED FOLDERS
        else return 1;
    else return 0;
}

static int scandir_known_files_filter(const struct dirent *d)
{
    //we filter out ., .. and all hidden folders and files. AND WE ALSO FILTER OUT SYNOLOGY-CREATED FOLDERS
    if(d->d_name[0]=='.' || d->d_name[0]=='@')
        return 0;

    int r,l=0,d_type=DT_UNKNOWN;
    //we return 1 for jpegs and folders
    if(d->d_type==DT_UNKNOWN){
        //we must find the type
        l=strlen(d->d_name);
        ph_assert(strlen(ptrParsing->picsDir)==ptrParsing->picsDirLen,NULL);
        memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,d->d_name,l+1);

        if(!stat(ptrParsing->picsDir,&ptrParsing->statBuf)){
            //stat succeeded
            if(S_ISREG(ptrParsing->statBuf.st_mode))d_type=DT_REG;
            else if(S_ISDIR(ptrParsing->statBuf.st_mode))d_type=DT_DIR;
            else ph_assert(0,"Cannot find entry type. ");
        } else {
            //stat failing is not a good sign, but can happen if this function is called while we delete files
            ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
            return 0; //skipping this file
        }
        ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
    }
    if(d->d_type==DT_DIR || d_type==DT_DIR){
        return 1;
    }
    else if(d->d_type==DT_REG || d_type==DT_REG){
        //check extension
        if(!l)l=strlen(d->d_name);
        if(l>4){
            char *extension=(char*)d->d_name+l-4;
            if(!strcasecmp(extension,KJpegExtension) || !strcasecmp(extension,KJpegExtension2) ||
               !strcasecmp(extension,KHeicExtension) ||

               !strcasecmp(extension,KMp4Extension) || !strcasecmp(extension,KM4vExtension) || !strcasecmp(extension,KMovExtension))
                r=1;
            else r=0;
        } else r=0;
    }
    else return 0;//skipping this file

    if(r)return r; //we stat valid files later

    //get the size of this file and add it to album size
    ph_assert(strlen(ptrParsing->picsDir)==ptrParsing->picsDirLen,NULL);
    memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,d->d_name,l+1);
    if(!stat(ptrParsing->picsDir,&ptrParsing->statBuf)){
        //stat succeeded (for an unknown item)
        ptrParsing->nrTotalUnknowns++;
        ptrParsing->sizeOfUnknownsInAlbum+=ptrParsing->statBuf.st_size;
    } else {
        //stat failing is not a good sign
        LOG0("WARNING: stat failed for %s with error: %s",ptrParsing->picsDir,strerror(errno));
        //ph_assert(errno==EOVERFLOW,"stat failed for %s",ptrParsing->picsDir);
        r=0; //we would skip this anyway
    }
    ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';//we make it back

    //LOG0("SKIPPING %s",d->d_name);
    return r;
}

static int getCurrentEntryIndex(struct mdh_album_entry ** __restrict entries, const int nrEntries, const int32_t captureTime, int *nrPicsWithDate, int *nrPicsWithoutDate){
    int j,index=-1;

    if(captureTime==DATE_MAX){
        //add this to pictures without date
        (*nrPicsWithoutDate)++;
        index=nrEntries-(*nrPicsWithoutDate);//current entry
    } else {
        //sort this photo
        for(j=0;j<(*nrPicsWithDate);j++)
            if(captureTime<entries[j]->captureTime){
                //shift remaining entries
                memmove(entries+j+1,entries+j,(*nrPicsWithDate-j)*sizeof(void*));
                //set the current entry
                index=j;
                //done
                break;
            }
        if(index<0)
            index=*nrPicsWithDate;
        (*nrPicsWithDate)++;
    }
    ph_assert(index>=0,NULL);
    return index;
}

static void swapElementAfter(struct md_album_entry *album, int src, int dst, int n){
    ph_assert(src!=dst,NULL);
    ph_assert(src>=0 && src<n,NULL);
    ph_assert(dst>=0 && dst<n,NULL);
    //md_album_entry
    struct md_album_entry tmp;
    int sz=sizeof(struct md_album_entry);
    memcpy(&tmp,album+src,sz);
    if(src>dst){
        memmove(album+dst+2,album+dst+1,sz*(src-dst-1));
        memcpy(album+dst+1,&tmp,sz);
    } else {
        memmove(album+src,album+src+1,sz*(dst-src));
        memcpy(album+dst,&tmp,sz);
    }

    //mdx_album_entry
    struct mdx_album_entry *albumX=*(struct mdx_album_entry **)(album+n);
    if(albumX){
        struct mdx_album_entry tmpX;
        sz=sizeof(struct mdx_album_entry);
        memcpy(&tmpX,albumX+src,sz);
        if(src>dst){
            memmove(albumX+dst+2,albumX+dst+1,sz*(src-dst-1));
            memcpy(albumX+dst+1,&tmpX,sz);
        } else {
            memmove(albumX+src,albumX+src+1,sz*(dst-src));
            memcpy(albumX+dst,&tmpX,sz);
        }
    }
}

static void swapElementBefore(struct md_album_entry *album, int src, int dst, int n){
    ph_assert(src!=dst,NULL);
    ph_assert(src>=0 && src<n,NULL);
    ph_assert(dst>=0 && dst<n,NULL);
    //md_album_entry
    struct md_album_entry tmp;
    int sz=sizeof(struct md_album_entry);
    memcpy(&tmp,album+src,sz);
    if(src>dst){
        memmove(album+dst+1,album+dst,sz*(src-dst));
        memcpy(album+dst,&tmp,sz);
    } else {
        memmove(album+src,album+src+1,sz*(dst-src-1));
        memcpy(album+dst-1,&tmp,sz);
    }

    //mdx_album_entry
    struct mdx_album_entry *albumX=*(struct mdx_album_entry **)(album+n);
    if(albumX){
        struct mdx_album_entry tmpX;
        sz=sizeof(struct mdx_album_entry);
        memcpy(&tmpX,albumX+src,sz);
        if(src>dst){
            memmove(albumX+dst+1,albumX+dst,sz*(src-dst));
            memcpy(albumX+dst,&tmpX,sz);
        } else {
            memmove(albumX+src,albumX+src+1,sz*(dst-src-1));
            memcpy(albumX+dst-1,&tmpX,sz);
        }
    }
}

static void initAlbum(struct md_album_entry * const __restrict album){
    memset(album,0,sizeof(struct md_album_entry));
    album->flags=FlagIsAlbum;
    album->earliestCaptureTime=album->latestCaptureTime=DATE_MAX;
}

static struct md_album_entry *createTempMdAlbumEntry(struct mdh_album_entry const * const * const entries, const int nrEntries, int nameStringsLen, int descriptionStringsLen,
                                                     uint32_t nrTotalUnknowns, uint64_t sizeOfUnknownsInAlbum){
    //create the memory structure for compact version of albums
    int i,j,k,l;
    char *t;
    int albumDataByPermissionSize=sizeof(struct mdh_album_data_by_permission)*(ptrParsing->nrPermissions+1);
    uint32_t strAlbumPos=sizeof(struct md_album_entry)*nrEntries+sizeof(struct mdx_album_entry*)+4+16+albumDataByPermissionSize; //16 comes from the md5 sum
    const int albumDataLen=strAlbumPos+nameStringsLen;
    struct md_album_entry *album=(struct md_album_entry*)ph_malloc(albumDataLen);
#ifndef NDEBUG //this makes Valgrind happy
    memset(album,0,albumDataLen);
#endif

    //initialize the albumDataByPermission part
    struct mdh_album_data_by_permission *albumDataByPermission=getAlbumDataByPermissionMd(album,nrEntries);
    for(j=0;j<ptrParsing->nrPermissions+1;j++){
        //initialize albumDataByPermission
        memset(&albumDataByPermission[j],0,sizeof(struct mdh_album_data_by_permission));
        albumDataByPermission[j].earliestCaptureTime=albumDataByPermission[j].latestCaptureTime=DATE_MAX;
    };
    albumDataByPermission[0].nrTotalUnknowns=nrTotalUnknowns;
    albumDataByPermission[0].sizeOfUnknownsInAlbumHi=sizeOfUnknownsInAlbum>>32;
    albumDataByPermission[0].sizeOfUnknownsInAlbumLo=sizeOfUnknownsInAlbum&0xFFFFFFFF;

    //create the memory structure for the extra data
    uint32_t strAlbumXPos=sizeof(struct mdx_album_entry)*nrEntries;
    const uint32_t albumExtraLen=strAlbumXPos+descriptionStringsLen;
    //TODO: above: take into consideration the description files
    struct mdx_album_entry *albumX=NULL;
    if(albumExtraLen>0){
        albumX=(struct mdx_album_entry *)ph_malloc(albumExtraLen);
        memset(albumX,0,strAlbumXPos); //init with zero the memory for the elements
    }

    //fill in all the data related to entries and albumDataByPermission
    for(i=0;i<nrEntries;i++){
        //shortcuts
        struct mdh_album_entry const * const __restrict ce=entries[i];
        struct md_album_entry * __restrict ca=&(album[i]);
        struct mdx_album_entry * __restrict cx=&(albumX[i]);
        struct mdh_album_data_by_permission const * __restrict adbpEntry=NULL;

        //building ca
        /* this is NOT CORRECT because it does not take into account timezones
        if(i>0 && entries[i-1]->captureTime!=DATE_MAX)
            ph_assert(ce->captureTime>=entries[i-1]->captureTime,NULL);
            */

        //common part of ca
        ca->flags=ce->flags;
        ca->entriesIndex=ce->entriesIndex;
        ca->permissions=0; //default value if we do not have permissions
        ca->dataOffsetHi=strAlbumPos>>16;
        ca->dataOffsetLo=strAlbumPos&0xFFFF;
        ca->nameCRC=ce->nameCRC;
        ph_assert(!(ce->nameCRC&3),NULL);//assumes the last 2 bits are 0
        for(k=0;k<4;k++){
            for(j=0;j<i;j++)
                if(ca->nameCRC+k==entries[j]->nameCRC)
                    break;
            if(j>=i)break; //we found it
        }
        ph_assert(k<4,"We found 4 entries with the same nameCRC in this album/folder. REALLY?!?!");
        LOG0c(k,"!!!!!!!!!!! INFO: this element has a non-zero nameCRC offset (%d)",k);
        ca->nameCRC+=k;

        ca->modificationTime=ce->modificationTime;
        t=(char*)ce+sizeof(struct mdh_album_entry)+
                (ce->flags&FlagIsAlbum?sizeof(struct mdh_album_entry_album):(ce->flags&FlagIsVideo?sizeof(struct mdh_album_entry_video):sizeof(struct mdh_album_entry_picture)));

        if(ce->flags&FlagHasDescriptionTxtEntry){
            ca->permissions=*(uint8_t*)(t+sizeof(void*));
            t+=sizeof(void*)+1;
            ph_assert(0,NULL); //not implemented yet
        }
        if(ce->flags&FlagIsAlbum){
            struct mdh_album_entry_album const * const __restrict cea=(struct mdh_album_entry_album const * const)(entries[i]+1);
            if(cea->mdAlbum)//it can actually be NULL in an incomplete parsing
                adbpEntry=getAlbumDataByPermissionMdh(ce);
            else adbpEntry=NULL;
            ca->earliestCaptureTime=ce->captureTime;
            ca->latestCaptureTime=cea->latestCaptureTime;
            ca->album=cea->mdAlbum;
            //ca->flags|=FlagElmIsBeingScanned;
            //extra stuff: nameOffset handled below
        } else if(ce->flags&FlagIsVideo){
            struct mdh_album_entry_video const * const __restrict cev=(struct mdh_album_entry_video const * const)(entries[i]+1);
            ca->captureTime=ce->captureTime;
            ca->size=ce->size;
            if(ce->flags&FlagPhotoHasGPSInfo){
                ca->lat=cev->lat;
                ca->lng=cev->lng;
                ph_assert(ca->flags&FlagPhotoHasGPSInfo,NULL);
            } else ca->lat=ca->lng=0;

            //extra stuff
            cx->width=cev->width;
            cx->height=cev->height;
            cx->duration=cev->duration;
            cx->timezone=cev->timezone;
            cx->sizeLo=cev->sizeLo;
        } else {
            //this is a picture
            struct mdh_album_entry_picture const * const __restrict cep=(struct mdh_album_entry_picture const * const)(entries[i]+1);
            ca->captureTime=ce->captureTime;
            ca->size=ce->size;
            if(ce->flags&FlagPhotoHasGPSInfo){
                ca->lat=cep->lat;
                ca->lng=cep->lng;
                ph_assert(ca->flags&FlagPhotoHasGPSInfo,NULL);
            } else ca->lat=ca->lng=0;

            //extra stuff
            cx->focalLength=cep->focalLength;
            cx->exposureTime=cep->exposureTime;
            cx->fNumber=cep->fNumber;
            cx->width=cep->width;
            cx->height=cep->height;
            cx->focalLength35=cep->focalLength35;
            cx->iso=cep->iso;

            cx->exposureProgram=cep->exposureProgram;
            cx->timezone=cep->timezone;
        }
        if(ce->flags&FlagHasNameOffset){
            ph_assert(ce->flags&FlagIsAlbum,NULL); //only albums can have name offset
            cx->nameOffset=*(uint8_t*)t;
            t++;
        }
        //copy name to album
        l=strlen(t)+1;
        //LOG0("Name in createTempMdAlbumEntry: %s (l=%d).",t,l-1);
        memcpy((char*)album+strAlbumPos,t,l);
        strAlbumPos+=l;
        t+=l;

        //copy descriptions to extra
        if(ce->flags&FlagHasDescription){
            l=strlen(t)+1;
            ph_assert(l>1,"l=%d",l);
            ph_assert(descriptionStringsLen>=l,"%d>=%d",descriptionStringsLen,l);
            LOG0("We have Description: %s (length: %d), will be written at pos=%d",t,l-1,strAlbumXPos);
            memcpy((char*)albumX+strAlbumXPos,t,l);
            if(!cx->stringsOffset)
                cx->stringsOffset=strAlbumXPos;
            strAlbumXPos+=l;
            t+=l;
        }
        if(ce->flags&FlagHasUserComment){
            l=strlen(t)+1;
            ph_assert(l>1,"l=%d",l);
            ph_assert(descriptionStringsLen>=l,"%d>=%d",descriptionStringsLen,l);
            LOG0("We have User Comment: %s (length: %d), will be written at pos=%d",t,l-1,strAlbumXPos);
            memcpy((char*)albumX+strAlbumXPos,t,l);
            if(!cx->stringsOffset)
                cx->stringsOffset=strAlbumXPos;
            strAlbumXPos+=l;
            t+=l;
        }

        uint8_t permissions=ca->permissions;
        for(j=0;j<ptrParsing->nrPermissions+1;j++){
            if(j==0 || permissions&1){
                //this entry is considered in the current permision class
                struct mdh_album_data_by_permission * __restrict adbp=&(albumDataByPermission[j]);
                if(ce->flags&FlagIsAlbum){
                    //this is an album
                    if(adbpEntry){
                        if(j){
                            //fill in "regular" entries
                            adbp->nrEntries+=adbpEntry[j].nrEntries;
                            //check earliestCaptureTime and latestCaptureTime
                            if(adbpEntry[j].earliestCaptureTime<adbp->earliestCaptureTime)
                                adbp->earliestCaptureTime=adbpEntry[j].earliestCaptureTime;

                            if(adbpEntry[j].latestCaptureTime!=DATE_MAX && (adbp->latestCaptureTime==DATE_MAX || adbp->latestCaptureTime<adbpEntry[j].latestCaptureTime))
                                adbp->latestCaptureTime=adbpEntry[j].latestCaptureTime;
                        } else {
                            adbp->nrTotalUnknowns+=adbpEntry[0].nrTotalUnknowns;
                            adbp->sizeOfUnknownsInAlbumHi+=adbpEntry[0].sizeOfUnknownsInAlbumHi;
                            adbp->sizeOfUnknownsInAlbumLo+=adbpEntry[0].sizeOfUnknownsInAlbumLo;
                        }
                        //common entries
                        adbp->nrTotalPictrs+=adbpEntry[j].nrTotalPictrs;
                        adbp->nrTotalVideos+=adbpEntry[j].nrTotalVideos;
                        adbp->nrTotalAlbums+=adbpEntry[j].nrTotalAlbums+1;
                        //LOG0c(j==0,"nrTotalAlbums for current album increased to %d because we added albums for %s which is %d+1",adbp->nrTotalAlbums,t,adbpEntry[j].nrTotalAlbums);
                        adbp->sizeOfPictrsInAlbum+=adbpEntry[j].sizeOfPictrsInAlbum;
                        adbp->sizeOfVideosInAlbum+=adbpEntry[j].sizeOfVideosInAlbum;
                    } else {
                        //we have at least the current entry/lbum
                        adbp->nrTotalAlbums++;
                    }
                } else {
                    //this is a picture or a video
                    if(j){
                        adbp->nrEntries++;
                        if(ce->captureTime!=DATE_MAX){
                            if(adbp->earliestCaptureTime>ce->captureTime)adbp->earliestCaptureTime=ce->captureTime;
                            if(adbp->latestCaptureTime==DATE_MAX || adbp->latestCaptureTime<ce->captureTime)
                                adbp->latestCaptureTime=ce->captureTime;
                        }
                    }

                    if(ce->flags&FlagIsVideo){
                        adbp->nrTotalVideos++;
                        adbp->sizeOfVideosInAlbum+=ce->size;
                    } else {
                        adbp->nrTotalPictrs++;
                        adbp->sizeOfPictrsInAlbum+=ce->size;
                    }
                }
                //LOG0c(j==0,"nrPics=%d, nrVids=%d, nrAlbums=%d. n=%d",adbp->nrTotalPictrs,adbp->nrTotalVideos,adbp->nrTotalAlbums,nrEntries);
            }
            permissions>>=1;
        }//for(j...
    }//for(i...
    LOG0("createTempMdAlbumEntry--: album=%p, adbp=%p",album,albumDataByPermission);
    //"put" albumX in album
    *(struct mdx_album_entry**)(album+nrEntries)=albumX;
    *(uint32_t*)((struct mdx_album_entry**)(album+nrEntries)+1)=albumExtraLen;

    ph_assert(albumDataLen==strAlbumPos,"albumDataLen: %d!=%d",albumDataLen,strAlbumPos);
    ph_assert(albumExtraLen==strAlbumXPos,"albumExtraLen %d!=%d",albumExtraLen,strAlbumXPos);

    return album;
}

static void deleteUnusedFolders(time_t startTime)
{
    struct dirent **namelist=NULL;
    int i,r,l;
    int n=scandir(ptrParsing->dataDir,&namelist,&isdir_filter,NULL);
    ph_assert(n>=0,NULL);
    //LOG0("deleteUnusedFolders: n=%d for %s",n,ptrParsing->dataDir);

    for(i=0;i<n;i++){
        l=strlen(namelist[i]->d_name);
        memcpy(ptrParsing->dataDir+ptrParsing->dataDirLen,namelist[i]->d_name,l);
        ptrParsing->dataDir[ptrParsing->dataDirLen+l]='/';
        memcpy(ptrParsing->dataDir+ptrParsing->dataDirLen+l+1,KAlbumFilename,strlen(KAlbumFilename)+1);
        r=stat(ptrParsing->dataDir,&ptrParsing->statBuf);
        //LOG0("### %s: r=%d mtime=%d, startTime=%d",ptrParsing->dataDir,r,ptrParsing->statBuf.st_mtime,startTime);
        if(r || (startTime-ptrParsing->statBuf.st_mtime>1)){ //we want to make the difference at least 2 (seconds)
            ptrParsing->dataDir[ptrParsing->dataDirLen+l]='\0';
            LOG0("#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@ Folder %s will be deleted. (%ld %ld, diff: %ld).",ptrParsing->dataDir,
                 (long)ptrParsing->statBuf.st_mtime,(long)startTime,(long)(startTime-ptrParsing->statBuf.st_mtime));
            LOG0c(r,"#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@#@ stat error: %s",strerror(errno));
            rmrf(ptrParsing->dataDir);
            /* old implementation was using system
            sprintf(ptrParsing->tempBuf,"rm -rf \"%s\"",ptrParsing->dataDir);
            r=system(ptrParsing->tempBuf);
            ph_assert(!r,"%s returned %d",ptrParsing->tempBuf,r);*/
        }
        free(namelist[i]);
    }
    free(namelist);
}


#ifdef DEBUG_PICS_FOLDER
struct debugPicsFolder {
    char *name;
    int count;
};

struct debugPicsFolder dps[10000];
int dpsNr=0;
#endif

static int countPicsFolder(int level, int inotifyFd, int *totalEntries)
{
    int n,i,l;
    struct dirent **namelist=NULL;
    int picsDirLenSave=ptrParsing->picsDirLen;

    ph_assert(level<7,"Too many levels (%d - max: 7)",level);

#ifdef DEBUG_PICS_FOLDER
    if(level==0)dpsNr=0;
#endif

    //make sure picsDir ends with /
    ph_assert(ptrParsing->picsDirLen>0,NULL);
    ph_assert(ptrParsing->picsDirLen==strlen(ptrParsing->picsDir),NULL);
    if(ptrParsing->picsDir[ptrParsing->picsDirLen-1]!='/'){
        ptrParsing->picsDir[ptrParsing->picsDirLen]='/';
        ptrParsing->picsDirLen++;
        ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
    }

    //if we have to do a rescan, do not do the scandir
    LOG0close_return(statusFlagsCheck(StatusFlagDoARescan),0,ptrParsing->picsDir[picsDirLenSave]='\0';ptrParsing->picsDirLen=picsDirLenSave,"RESCAN requested, counting aborted.");

    //add this folder to the watch list
#ifndef __APPLE__
    int wd=inotify_add_watch(inotifyFd,ptrParsing->picsDir,PHOTOSTOVIS_INOTIFY_WATCH_MASK);
    ph_assert(wd!=-1,"inotify_add_watch() returned an error: %s",strerror(errno));
    //TODO: do we need to store wd somewhere?
#endif

    //LOG0("Counting pictures, videos and albums in <<%s>> ...",ptrParsing->picsDir);
    n=scandir(ptrParsing->picsDir, &namelist, &scandir_known_files_filter, NULL);
    //LOG0("Counted: %d elements.",n);
    LOG0close_return(n<0,-1,ptrParsing->picsDir[picsDirLenSave]='\0';ptrParsing->picsDirLen=picsDirLenSave,
            "Error counting pictures in %s: %s",ptrParsing->picsDir,strerror(errno));
    LOG0close_return(n>65535,-1,ptrParsing->picsDir[picsDirLenSave]='\0';ptrParsing->picsDirLen=picsDirLenSave,
            "Too many pictures/entries in this album (%s). Exiting.",ptrParsing->picsDir);

    for(i=0;i<n;i++){
        struct dirent * __restrict d=namelist[i];
        l=strlen(d->d_name);

        if(d->d_type==DT_UNKNOWN){
            //we must find the type
            ph_assert(strlen(ptrParsing->picsDir)==ptrParsing->picsDirLen,NULL);
            memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,d->d_name,l+1);

            if(!stat(ptrParsing->picsDir,&ptrParsing->statBuf)){
                //stat succeeded
                if(S_ISREG(ptrParsing->statBuf.st_mode))d->d_type=DT_REG;
                else if(S_ISDIR(ptrParsing->statBuf.st_mode))d->d_type=DT_DIR;
                else ph_assert(0,"Cannot find entry type. ");
            } else {
                //stat failing can happen if the entry was deleted meanwhile
                ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
                free(d);
                continue;
            }
            ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
        }

        if(d->d_type==DT_DIR){
            ph_assert(ptrParsing->picsDirLen==strlen(ptrParsing->picsDir),"%d %zu",ptrParsing->picsDirLen,strlen(ptrParsing->picsDir));
            memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,d->d_name,l+1);
            ptrParsing->picsDirLen+=l;
            countPicsFolder(level+1,inotifyFd,totalEntries);
            ptrParsing->picsDirLen-=l;
            ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
        } else {
            ph_assert(d->d_type==DT_REG,NULL); //there should be no other case here
        }
        free(d);
    }//for
    free(namelist);
    *totalEntries+=n;

#ifdef DEBUG_PICS_FOLDER
    dps[dpsNr].count=n;
    dps[dpsNr].name=ph_strdup(ptrParsing->picsDir);
    dpsNr++;
    LOG0("Added: %s",ptrParsing->picsDir);
#endif

    ptrParsing->picsDir[picsDirLenSave]='\0';
    ptrParsing->picsDirLen=picsDirLenSave;

    return 0;
}


#ifdef DEBUG_PICS_FOLDER
static void checkAlbums(struct md_album_entry *album, int nrEntries, int level)
{
    int i,l,lSave;
    char *t;
    ph_assert(album,NULL);

    //make sure picsDir ends with /
    ph_assert(ptrParsing->picsDirLen>0,NULL);
    ph_assert(ptrParsing->picsDirLen==strlen(ptrParsing->picsDir),NULL);
    if(ptrParsing->picsDir[ptrParsing->picsDirLen-1]!='/'){
        ptrParsing->picsDir[ptrParsing->picsDirLen]='/';
        ptrParsing->picsDirLen++;
        ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
    }

    if(level==0){
        l=0;
        for(i=0;i<dpsNr;i++)
            l-=dps[i].count;
        LOG0("level 0: %s Count: %d",ptrParsing->picsDir,l);
    } else {
        //find the current folder
        for(i=0;i<dpsNr;i++)
            if(dps[i].name && !strcmp(dps[i].name,ptrParsing->picsDir)){//found it
                ph_assert(nrEntries==-dps[i].count,"Mismatch %d vs %d for %s",nrEntries,-dps[i].count,dps[i].name);
                free(dps[i].name);
                dps[i].name=NULL;
                break;
            }
        ph_assert(!nrEntries || i<dpsNr,"Not found: %s, level: %d",ptrParsing->picsDir,level);
    }


    lSave=ptrParsing->picsDirLen;
    for(i=0;i<nrEntries;i++)
        if(album[i].flags&FlagIsAlbum && album[i].album){
            if(level){
                t=getNameFromAlbum(album,i);
                l=strlen(t);

                memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,t,l+1);
                ptrParsing->picsDirLen+=l;
                //LOG0("<<%s>> (%s len=%d)",ptrParsing->picsDir,t,l);
            }
            checkAlbums(album[i].album,album[i].entriesIndex,level+1);
            if(level){
                ptrParsing->picsDirLen=lSave;
                ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
                //LOG0("<<%s>>* (%s len=%d)",ptrParsing->picsDir,t,l);
            }
        }
        //else LOG0("Picture entry: %s/%x",getNameFromAlbum(album,i),album[i].flags);
    //at the end
    if(level==0){
        for(i=0;i<dpsNr;i++)
            //LOG0c(dps[i].name,"%s %d",dps[i].name,dps[i].count);
            ph_assert(!dps[i].name,"%s %d",dps[i].name,dps[i].count);
    }
}

static void freeDpsData()
{
    int i;
    for(i=0;i<dpsNr;i++)
        if(dps[i].name){
            free(dps[i].name);
            dps[i].name=NULL;
        }
    dpsNr=0;
}
#else
static void freeDpsData(){}
#endif

static void propagateModificationTimeRecursive(const int level, const int32_t mtime, struct md_album_entry *a, const int n){
    if(level==ptrParsing->currentAlbum.level)return;
    int i;
    const uint32_t currentCRC=ptrParsing->currentAlbum.nameCRC[level];
    for(i=0;i<n;i++)
        if(currentCRC==a[i].nameCRC){
            ph_assert(a[i].flags&FlagIsAlbum,NULL);
            ph_assert(a[i].album,NULL);
            a[i].modificationTime=mtime;
            getExtraEntry(a,n,i)->modificationCounter++;
            //LOG0("propagateModification: %08X mt: %d, mc: %d",currentCRC,a[i].modificationTime,getExtraEntry(a,n,i)->modificationCounter);
            propagateModificationTimeRecursive(level+1,mtime,a[i].album,a[i].entriesIndex);
            return;
        }
    ph_assert(0,"level=%d, n=%d currentCRC=%u",level,n,currentCRC);
}

static void propagateModification(struct ps_md_pics *mdPicsW){
    int32_t mtime=time(NULL);
    struct md_album_entry *a=mdPicsW->rootAlbum->album;
    uint16_t n=mdPicsW->rootAlbum->entriesIndex;
    mdPicsW->rootAlbum->modificationTime=mtime;
    getExtraEntry(mdPicsW->rootAlbum,1,0)->modificationCounter++;
    //LOG0("propagateModification: root mt: %d, mc: %d",mdPicsW->rootAlbum->modificationTime,getExtraEntry(mdPicsW->rootAlbum,1,0)->modificationCounter);
    propagateModificationTimeRecursive(0,mtime,a,n);
}


static int scanPicsFolder(const int level, struct md_album_entry * const __restrict parentMdAlbum, const int parentMdIndex, const int parentMdN,
                          struct md_entry_id * const changedEntry, const int parentInChangedPath){
    /* Strategy:
     * -Fill in tempMdAlbumEntry and tempMdAlbumEntryX, compare them with parentMdAlbumEntry and parentMdAlbumEntryX
     * -return our album as parentMdAlbumEntry->album
     * -if tempMdAlbumEntry matches parentMdAlbumEntry then return 0 (parent album does not need scanning)
     * -else fix the values in parentMdAlbumEntry and return 1 (parent album needs scanning)
     */
    if(shouldExit)return -1;
    //sleep(1); //for debugging purposes
    ph_assert(parentMdAlbum,NULL);
    struct md_album_entry * const parentMdAlbumEntry=&parentMdAlbum[parentMdIndex];
    struct mdx_album_entry * const parentMdAlbumEntryX=getExtraEntry(parentMdAlbum,parentMdN,parentMdIndex);

    LOG0return(!(parentMdAlbumEntry->flags&FlagElmIsBeingScanned),0,"scanPicsFolder: skipping already parsed album (%s).",ptrParsing->picsDir);

    ph_assert(parentMdAlbumEntry->flags&(FlagIsAlbum|FlagElmIsBeingScanned),NULL); //should be set already
    ph_assert((parentMdAlbumEntry->dataOffsetHi<<16)+parentMdAlbumEntry->dataOffsetLo>=sizeof(struct md_album_entry),NULL);//our data offset should be valid
    ph_assert(ptrParsing->mdPics,NULL); //we assume it is locked for reading
    ph_assert(level<7,"Too many levels (%d - max: 7)",level);
    ph_assert(ptrParsing->picsDirLen>0,NULL);
    ph_assert(ptrParsing->dataDirLen>0,NULL);


    time_t startTime=time(NULL);

    int fastScan=0;
    struct dirent **namelist=NULL;
    struct mdh_file_info *fInfo=NULL;
    time_t albumDatMtime=INT32_MIN;
    int i,r,l;
    int picsDirLenSave,dataDirLenSave,n,albumNeedsParsing=0,parentNeedsParsing=0,albumHasDescriptionFile,albumExpectedSize=0;
    int albumDatFileSize=0,albumStructureSize=0,albumExtraSize=0,albumWasLoaded=0;
    int debugNrPicturesCamera=0;
    struct ps_md_pics *mdPicsW=NULL;
    //album needs parsing when the parentMdAlbumEntry->album or album loaded from file are not in line with the number of scanned pictures and their modification time
    //parent needs parsing when the provided data (in parentMdAlbumEntry) are not the same as the data collected by us in thisMdAlbumEntry
    char const *parentMdAlbumName=getNameFromAlbum(parentMdAlbum,parentMdIndex);
    ph_assert(parentMdAlbumName,NULL);

    uint64_t sizeOfUnknownsInAlbum=0;
    uint32_t nrTotalUnknowns=0;
    struct md_album_entry tempMdAlbumEntry;
    struct mdx_album_entry tempMdAlbumEntryX;
    initAlbum(&tempMdAlbumEntry);
    memset(&tempMdAlbumEntryX,0,sizeof(struct mdx_album_entry));

    //are we on the critical path?
    if(changedEntry){
        LOG0("changedEntry level: %d, currentlevel: %d, parentInChangedPath: %d",changedEntry->level,level,parentInChangedPath);
        if(level>0)
            LOG0("IDchanged: %08X, IDcurrent: %08X",changedEntry->nameCRC[level-1],parentMdAlbumEntry->nameCRC);
        if(parentInChangedPath && (
                (changedEntry->level<=level && (level==0 || changedEntry->nameCRC[level-1]==parentMdAlbumEntry->nameCRC)) || (changedEntry->level>level)
                ))
            fastScan=0; //we are in the changed path
        else fastScan=1;
    } //else fastScan=0 -> already

fastScanFailed:

    if(ptrParsing->forcePicturesFullScan){
        albumNeedsParsing=1;
        LOG0("FORCED!!!! scanPicsFolder++ (level %d, start time: %ld), album: %s album ptr=%p",
             level,(long)startTime,ptrParsing->picsDir,parentMdAlbumEntry->album);
    } else if(fastScan)
        LOG0("#### #### (fast)scanPicsFolder++ (level %d, start time: %ld), album: %s album ptr=%p",
             level,(long)startTime,ptrParsing->picsDir,parentMdAlbumEntry->album);
    else
        LOG0("#### scanPicsFolder++ (level %d, start time: %ld), album: %s album ptr=%p",
             level,(long)startTime,ptrParsing->picsDir,parentMdAlbumEntry->album);

    //make sure picsDir ends with /
    ph_assert(ptrParsing->picsDirLen>0,NULL);
    ph_assert(ptrParsing->picsDirLen==strlen(ptrParsing->picsDir),NULL);
    if(ptrParsing->picsDir[ptrParsing->picsDirLen-1]!='/'){
        ptrParsing->picsDir[ptrParsing->picsDirLen]='/';
        ptrParsing->picsDirLen++;
        ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
    }
    picsDirLenSave=ptrParsing->picsDirLen;

    //same for dataDir
    ph_assert(ptrParsing->dataDirLen>0,NULL);
    ph_assert(ptrParsing->dataDirLen==strlen(ptrParsing->dataDir),NULL);
    if(ptrParsing->dataDir[ptrParsing->dataDirLen-1]!='/'){
        ptrParsing->dataDir[ptrParsing->dataDirLen]='/';
        ptrParsing->dataDirLen++;
        ptrParsing->dataDir[ptrParsing->dataDirLen]='\0';
    }
    dataDirLenSave=ptrParsing->dataDirLen;

    //get nameCRC
    ph_assert(parentMdAlbumEntry->nameCRC,NULL);
    tempMdAlbumEntry.nameCRC=parentMdAlbumEntry->nameCRC; //to have the same 2-bit ending
    tempMdAlbumEntry.dataOffsetHi=parentMdAlbumEntry->dataOffsetHi;
    tempMdAlbumEntry.dataOffsetLo=parentMdAlbumEntry->dataOffsetLo;
    //set ptrParsing->currentAlbum
    ptrParsing->currentAlbum.level=level;
    if(level)
        ptrParsing->currentAlbum.nameCRC[level-1]=tempMdAlbumEntry.nameCRC;

    //we need these 2 in ptrParsing because their value is setup by the scandir filter
    ptrParsing->nrTotalUnknowns=0;
    ptrParsing->sizeOfUnknownsInAlbum=0;

    if(fastScan){
        //fast-scanning this album, we do not expect changes
        n=parentMdAlbumEntry->entriesIndex;
    } else {
        n=scandir(ptrParsing->picsDir, &namelist, &scandir_known_files_filter, NULL);
        LOG0return(n<0,-1,"ERROR processing %s: %s",ptrParsing->picsDir,strerror(errno));

        if(n>65535){
            LOG0("WARNING: processing %s: Too many pictures/entries in this album (%d).",ptrParsing->picsDir,n);
            //the solution is to limit the processing to 65535 entries
            for(i=65535;i<n;i++)free(namelist[i]);
            n=65535;
        }

        //we create the corresponding data folder, even if the album folder is empty
        r=dirService(ptrParsing->dataDir,1);
        LOG0close_return(r,-1,for(i=0;i<n;i++)free(namelist[i]);free(namelist),
                         "ERROR processing %s: Creating data folder (%s) failed.",ptrParsing->picsDir,ptrParsing->dataDir);

        //we need to get these values right here, because we have scanned them.
        sizeOfUnknownsInAlbum=ptrParsing->sizeOfUnknownsInAlbum;
        nrTotalUnknowns=ptrParsing->nrTotalUnknowns;
        //at this moment the values are the nr and size of unknown files in this album, not including subalbums
    }

    //check if we really need to parse this folder, or if we already have the data
    //we suppose it does NOT need parsing, unless we prove it actually needs
    tempMdAlbumEntry.entriesIndex=n;

    //first, get the modification date of the album.dat file. It should be later than the modification date of
    //this picture folder and the modification date of all the pictures inside
    if(!albumNeedsParsing)
        memcpy(ptrParsing->dataDir+dataDirLenSave,KAlbumFilename,strlen(KAlbumFilename)+1);
    if(!albumNeedsParsing && !stat(ptrParsing->dataDir,&ptrParsing->statBuf)){
        //stat succeeded. There is a possibility that we do not need parsing
        albumDatMtime=ptrParsing->statBuf.st_mtime;
        albumDatFileSize=ptrParsing->statBuf.st_size;

        //check the modification time of the album folder
        r=stat(ptrParsing->picsDir,&ptrParsing->statBuf);
        ph_assert(!r,NULL); //this folder should be available, we just scanned it
        if(ptrParsing->statBuf.st_mtime>=albumDatMtime){//picture folder modified after album.dat
            albumNeedsParsing=1;
            LOG0("Will parse album. Album folder modified after album.dat was created");
        } else if(!fastScan){
            albumExpectedSize=n*sizeof(struct md_album_entry)+sizeof(void*)+4+16+(ptrParsing->nrPermissions+1)*sizeof(struct mdh_album_data_by_permission);
            ph_assert(!fInfo,NULL);
            if(n>0)
                fInfo=(struct mdh_file_info*)ph_malloc(n*sizeof(struct mdh_file_info));
            for(i=0;i<n;i++){
                l=strlen(namelist[i]->d_name);
                albumExpectedSize+=l+1;
                memcpy(ptrParsing->picsDir+picsDirLenSave,namelist[i]->d_name,l+1);
                r=stat(ptrParsing->picsDir,&ptrParsing->statBuf);
                LOG0close_return(r,-1,for(i=0;i<n;i++)free(namelist[i]);free(namelist);free(fInfo),
                        "stat-ing %s failed: %s. Forcing full rescan.",ptrParsing->picsDir,strerror(errno));

                fInfo[i].size=ptrParsing->statBuf.st_size;
                fInfo[i].mtime=ptrParsing->statBuf.st_mtime;

                if(ptrParsing->statBuf.st_mtime>=albumDatMtime){//picture/album modified after album.dat
                    albumNeedsParsing=1;
                    LOG0("Will parse album. One picture (%s) was modified after album.dat was created",ptrParsing->picsDir);
                    //break; ->we do not break, we want to fill in the fInfo structure
                }
            }//for
            ph_assert(picsDirLenSave==ptrParsing->picsDirLen,NULL);
            ptrParsing->picsDir[picsDirLenSave]='\0';
        }//else

        if(!albumNeedsParsing && !fastScan){
            //check the modification time of the description file
            memcpy(ptrParsing->picsDir+picsDirLenSave,KDescriptionFilename,strlen(KDescriptionFilename)+1);
            r=stat(ptrParsing->picsDir,&ptrParsing->statBuf);
            if(!r && ptrParsing->statBuf.st_mtime>=albumDatMtime){ //description file exists and its modification time is after album.dat
                albumNeedsParsing=1;
                LOG0("Will parse album. Album description file exists and it is modified after album.dat was created");
            }
        }
        ptrParsing->picsDir[picsDirLenSave]='\0';
    } else {
        if(!albumNeedsParsing)
            LOG0("Will parse album. No corresponding album.dat (%s) found: %s",ptrParsing->dataDir,strerror(errno));
        else
            LOG0("Will parse album: albumNeedsParsing=%d",albumNeedsParsing);
        albumNeedsParsing=2; //because we do not have the album.dat file
    }

    if(n!=parentMdAlbumEntry->entriesIndex){
        parentNeedsParsing=1;
        if(parentMdAlbumEntry->album)
            albumNeedsParsing=1;
    }

    //if we have an album in memory or in a file, load it, perhaps we can reuse some of the data
    if(parentMdAlbumEntry->album){
        LOG0("Album already loaded in memory. Reusing. (%s)",ptrParsing->picsDir);
        if(!fastScan){
            albumStructureSize=albumExpectedSize;

            if(parentMdAlbumEntry->entriesIndex){
                ph_assert(*(struct mdx_album_entry**)(parentMdAlbumEntry->album+parentMdAlbumEntry->entriesIndex),NULL);
                albumExtraSize=*(uint32_t*)((struct mdx_album_entry**)(parentMdAlbumEntry->album+parentMdAlbumEntry->entriesIndex)+1);
            }else{
                ph_assert(NULL==*(struct mdx_album_entry**)(parentMdAlbumEntry->album+parentMdAlbumEntry->entriesIndex),"%d %p",
                          parentMdAlbumEntry->entriesIndex,*(struct mdx_album_entry**)(parentMdAlbumEntry->album+parentMdAlbumEntry->entriesIndex));
                ph_assert(0==*(uint32_t*)((struct mdx_album_entry**)(parentMdAlbumEntry->album+parentMdAlbumEntry->entriesIndex)+1),NULL);
            }

            for(i=0;i<parentMdAlbumEntry->entriesIndex;i++)
                if(parentMdAlbumEntry->album[i].flags&FlagIsAlbum){
                    if(parentMdAlbumEntry->album[i].flags&FlagElmIsBeingScanned)
                        LOG0("Sub-Album %s (%d) needs parsing.",getNameFromAlbum(parentMdAlbumEntry->album,i),parentMdAlbumEntry->album[i].dataOffsetLo);
                    /*else
                    LOG0("Sub-Album %s (%d) does NOT need parsing.",getNameFromAlbum(parentMdAlbumEntry->album,i),parentMdAlbumEntry->album[i].dataOffsetLo);
                    */
                }
#ifndef NDEBUG
            if(!albumNeedsParsing){
                LOG0("DEBUG: Checking album!");
                //verify if the data is the same as in the album.dat file
                struct md_albumdat_header albumDatHeader;
                FILE *f=fopen(ptrParsing->dataDir,"r");
                ph_assert(f,NULL);

                r=fread(&albumDatHeader,sizeof(struct md_albumdat_header),1,f);
                ph_assert(r==1,NULL);
                //check the version, nr permissions, nr of entries
                ph_assert(albumDatHeader.fileVersion==CURRENT_ALBUMDAT_FILE_VERSION,NULL);
                ph_assert(albumDatHeader.nrPermissions==ptrParsing->nrPermissions,NULL);
                ph_assert(albumDatHeader.nrEntries==n,NULL);
                //allocate and read the album
                ph_assert(n==albumDatHeader.nrEntries,NULL);
                ph_assert(albumStructureSize==albumDatHeader.lengthOfAlbumData,NULL);
                struct md_album_entry *album2=(struct md_album_entry *)ph_malloc(albumDatHeader.lengthOfAlbumData);
                r=fread(album2,albumDatHeader.lengthOfAlbumData,1,f);
                ph_assert(r==1,NULL);
                fclose(f);
                f=NULL;
                //verify elements
                for(i=0;i<n;i++){
                    if(parentMdAlbumEntry->album[i].flags&FlagIsAlbum){
                        if(parentMdAlbumEntry->album[i].flags&FlagElmIsBeingScanned)
                            LOG0("DEBUG: Sub-Album %s needs parsing.",getNameFromAlbum(parentMdAlbumEntry->album,i));
                        else
                            LOG0("DEBUG: Sub-Album %s does NOT need parsing.",getNameFromAlbum(parentMdAlbumEntry->album,i));
                        album2[i].modificationTime=parentMdAlbumEntry->album[i].modificationTime; //because we change the modification time for albums
                    }
                    album2[i].flags&=~RuntimeFlags;
                    uint16_t runtimeFlags=parentMdAlbumEntry->album[i].flags&RuntimeFlags;
                    //remove the runtime flags from album[i] before comparing. We will add them later
                    parentMdAlbumEntry->album[i].flags&=~RuntimeFlags;
                    /*
                LOG0("compare size: %d (%d/%d)",sizeof(struct md_album_entry)-8,i+1,albumNrElements);
                LOG0("flags: %u %u",album[i].flags,album2[i].flags);
                LOG0("entriesIndex: %u %u",album[i].entriesIndex,album2[i].entriesIndex);
                LOG0("permissions: %u %u",album[i].permissions,album2[i].permissions);
                LOG0("dataOffsetHi: %u %u",album[i].dataOffsetHi,album2[i].dataOffsetHi);
                LOG0("dataOffsetLo: %u %u",album[i].dataOffsetLo,album2[i].dataOffsetLo);
                LOG0("nameCRC: %u %u",album[i].nameCRC,album2[i].nameCRC);
                LOG0("tagsNotUsed: %u %u",album[i].tagsNotUsed,album2[i].tagsNotUsed);

                LOG0("earliestCaptureTime: %d %d",album[i].earliestCaptureTime,album2[i].earliestCaptureTime);
                LOG0("latestCaptureTime: %d %d",album[i].latestCaptureTime,album2[i].latestCaptureTime);
                */

                    ph_assert(!memcmp(&parentMdAlbumEntry->album[i],&album2[i],sizeof(struct md_album_entry)-8),NULL);
                    ph_assert(!strcmp(getNameFromAlbum(parentMdAlbumEntry->album,i),getNameFromAlbum(album2,i)),NULL);
                    if(runtimeFlags)
                        parentMdAlbumEntry->album[i].flags|=runtimeFlags;

                    if(parentMdAlbumEntry->album[i].flags&FlagIsAlbum){
                        if(parentMdAlbumEntry->album[i].flags&FlagElmIsBeingScanned)
                            LOG0("DEBUG: Sub-Album %s needs parsing.",getNameFromAlbum(parentMdAlbumEntry->album,i));
                        else
                            LOG0("DEBUG: Sub-Album %s does NOT need parsing.",getNameFromAlbum(parentMdAlbumEntry->album,i));
                    }
                }
                free(album2);
            }
#endif
        }//if(!fastScan)
    } else if(albumNeedsParsing!=2){
        struct md_albumdat_header albumDatHeader;
        struct md_album_entry *album=NULL;
        struct mdx_album_entry *albumX=NULL;

        /* We have 2 cases here:
         * 1. We load the data but we still need to parse the album
         * 2. The data we loaded is garbage/unusable and should be discarded. In this case we set albumeedsParsing to 2
         */

        LOG0("Loading album from album.dat file (%s).",ptrParsing->picsDir);
        FILE *f=fopen(ptrParsing->dataDir,"r");
        LOG0c_do(!f,albumNeedsParsing=2,
                 "Will parse album. album.dat (%s) could not be opened: %s",ptrParsing->dataDir,strerror(errno));

        if(albumNeedsParsing!=2)
            r=fread(&albumDatHeader,sizeof(struct md_albumdat_header),1,f);
        LOG0c_do(albumNeedsParsing!=2 && r!=1,albumNeedsParsing=2,
                 "Will parse album. Reading from album.dat failed: %s",strerror(errno));

        //check the version
        LOG0c_do(albumNeedsParsing!=2 && albumDatHeader.fileVersion!=CURRENT_ALBUMDAT_FILE_VERSION,albumNeedsParsing=2,
                 "Will parse album. album.dat file has a different (older?) version.");
        //check the number of permissions
        LOG0c_do(albumNeedsParsing!=2 && albumDatHeader.nrPermissions!=ptrParsing->nrPermissions,albumNeedsParsing=2,
                 "Will parse album. album.dat file has different number of permissions (%d vs %u).",ptrParsing->nrPermissions,albumDatHeader.nrPermissions);

        //allocate and read the album
        if(albumNeedsParsing!=2){
            albumStructureSize=albumDatHeader.lengthOfAlbumData;
            album=(struct md_album_entry *)ph_malloc(albumStructureSize);
            r=fread(album,albumStructureSize,1,f);
            LOG0c_do(r!=1,albumNeedsParsing=2,"Will parse album. Could not read album data from album.dat: %s",strerror(errno));
        }
        if(albumNeedsParsing!=2){
            albumExtraSize=albumDatHeader.lengthOfExtraData;
            if(albumExtraSize){
                albumX=(struct mdx_album_entry *)ph_malloc(albumExtraSize);
                r=fread(albumX,albumExtraSize,1,f);
                LOG0c_do(r!=1,albumNeedsParsing=2,"Will parse album. Could not read EXTRA album data (%d bytes) from album.dat: %s",
                         albumExtraSize,strerror(errno));
            } else albumX=NULL;
            *(struct mdx_album_entry **)(album+albumDatHeader.nrEntries)=albumX;
            *(uint32_t*)((struct mdx_album_entry**)(album+albumDatHeader.nrEntries)+1)=albumExtraSize;
        }


        if(f)
            fclose(f);
        if(albumNeedsParsing!=2){
            //is the data garbage? Compute the MD5
            r=computeAlbumMD5(album,albumDatHeader.nrEntries,albumDatHeader.lengthOfAlbumData,albumDatHeader.nrPermissions,1);
            LOG0c_do(r,albumNeedsParsing=2,"Will parse album. WARNING: Read album data not matching the MD5 sum. Data is damaged.");
        }

        if(albumNeedsParsing!=2){
            //nullify all ->album elements
            if(!albumNeedsParsing && n>0)
                ph_assert(fInfo,NULL);
            for(i=0;i<albumDatHeader.nrEntries;i++){
                album[i].flags&=~RuntimeFlags; //have it here, be cause FlagElmIsBeingScanned is a runtime flag
                if(album[i].flags&FlagIsAlbum){
                    album[i].album=NULL;
                    album[i].flags|=FlagElmIsBeingScanned;
                }
            }

            //here we can assign the album to the parent album, just in case a client will request some data
            releaseMdPics(&ptrParsing->mdPics);
            mdPicsW=getMdPicsWritable();
            parentMdAlbumEntry->album=album;
            parentMdAlbumEntry->entriesIndex=albumDatHeader.nrEntries;
            uint16_t oldCounter=parentMdAlbumEntryX->modificationCounter;
            propagateModification(mdPicsW);
            ph_assert(parentMdAlbumEntryX->modificationCounter==oldCounter+1,NULL);
            releaseMdPicsW(&mdPicsW);
            album=NULL;
            albumX=NULL;
            sendStatusToConnectedClients(&ptrParsing->currentAlbum);
            ptrParsing->mdPics=getMdPicsRd();

            //do we still need to parse the album?

            //check the number of entries
            LOG0c_do(!albumNeedsParsing && albumDatHeader.nrEntries!=n,albumNeedsParsing=1,
                     "Will parse album. album.dat file has different number of entries (%d vs %u).",albumDatHeader.nrEntries,n);
            LOG0c_do(!albumNeedsParsing && albumExpectedSize!=albumStructureSize,albumNeedsParsing=1,
                     "Will parse album. Read data size (%d) is different than the expected data size (%d)",albumStructureSize,albumExpectedSize);

            albumWasLoaded=1;
        }
        else if(albumNeedsParsing==2){
            //we need to free the album data
            free(album);//album=NULL; -> no need, declared in this block
            free(albumX);//albumX=NULL; -> no need, declared in this block
            albumNeedsParsing=1;
        }
    }
    if(albumNeedsParsing==2)
        albumNeedsParsing=1; //no need to keep it 2, we are past the loading album from album.dat

    if(!albumNeedsParsing){
        //Check for subfolders
        for(i=0;i<n;i++){
            if(parentMdAlbumEntry->album[i].flags&FlagIsAlbum){
                //this is an album
                char const * const albumName=getNameFromAlbum(parentMdAlbumEntry->album,i);
                //add our albumName name to picsDir and dataDir
                l=strlen(albumName);
                memcpy(ptrParsing->picsDir+picsDirLenSave,albumName,l+1);
                ptrParsing->picsDirLen=picsDirLenSave+l;
                memcpy(ptrParsing->dataDir+dataDirLenSave,albumName,l+1);
                ptrParsing->dataDirLen=dataDirLenSave+l;

                //do the scanning
                r=scanPicsFolder(level+1,parentMdAlbumEntry->album,i,n,changedEntry,!fastScan);
                ptrParsing->currentAlbum.level=level;

                ptrParsing->picsDir[picsDirLenSave]='\0';
                //we do not free the album in the 2 returning commands below. It is freed in the calling function
                if(fastScan){
                    ph_assert(!namelist,NULL);
                    ph_assert(!fInfo,NULL);
                    LOG0return(r==-1,-1,"ERROR parsing subalbum.");
                    LOG0return(shouldExit,-1,"Photostovis exiting (aborting picture scanning).");
                } else {
                    ph_assert(namelist,NULL);
                    ph_assert(fInfo,NULL);
                    LOG0close_return(r==-1,-1,for(i=0;i<n;i++)free(namelist[i]);free(namelist);free(fInfo),
                            "ERROR parsing subalbum.");
                    LOG0close_return(shouldExit,-1,for(i=0;i<n;i++)free(namelist[i]);free(namelist);free(fInfo),
                            "Photostovis exiting (aborting picture scanning).");
                }

                LOG0close_continue(r,albumNeedsParsing=1,"Will parse album. One subalbum (%s) asked for parent album parsing.",ptrParsing->dataDir);
                //we break because we will parse the albums anyway

                //set earliestCaptureTime and latest capture time
                if(tempMdAlbumEntry.earliestCaptureTime>parentMdAlbumEntry->album[i].earliestCaptureTime)
                    tempMdAlbumEntry.earliestCaptureTime=parentMdAlbumEntry->album[i].earliestCaptureTime;
                if(parentMdAlbumEntry->album[i].latestCaptureTime!=DATE_MAX &&
                        (tempMdAlbumEntry.latestCaptureTime==DATE_MAX || tempMdAlbumEntry.latestCaptureTime<parentMdAlbumEntry->album[i].latestCaptureTime))
                    tempMdAlbumEntry.latestCaptureTime=parentMdAlbumEntry->album[i].latestCaptureTime;
                //done for this album/folder
            }
            else {
                //process this picture or video
                if(parentMdAlbumEntry->album[i].captureTime==DATE_MAX)
                    tempMdAlbumEntry.flags|=FlagAlbumHasUndatedEntries;

                //set earliestCaptureTime and latest capture time
                if(parentMdAlbumEntry->album[i].captureTime!=DATE_MAX){
                    if(tempMdAlbumEntry.earliestCaptureTime>parentMdAlbumEntry->album[i].captureTime)
                        tempMdAlbumEntry.earliestCaptureTime=parentMdAlbumEntry->album[i].captureTime;
                    if(tempMdAlbumEntry.latestCaptureTime==DATE_MAX || tempMdAlbumEntry.latestCaptureTime<parentMdAlbumEntry->album[i].captureTime)
                        tempMdAlbumEntry.latestCaptureTime=parentMdAlbumEntry->album[i].captureTime;
                }
            }
            //common
            if(parentMdAlbumEntry->album[i].flags&FlagHasExifThumbnail)
                tempMdAlbumEntry.flags|=FlagHasExifThumbnail;
            if(parentMdAlbumEntry->album[i].flags&FlagAlbumHasUndatedEntries)
                tempMdAlbumEntry.flags|=FlagAlbumHasUndatedEntries;
        }//for(i=...
        ptrParsing->picsDirLen=picsDirLenSave;
    }
    //reset dataDir
    ptrParsing->dataDir[dataDirLenSave]='\0';
    ptrParsing->dataDirLen=dataDirLenSave;

    if(fastScan){
        //if this is fast scan, we do not care that much about Undated entries
        if(parentMdAlbumEntry->flags&FlagAlbumHasUndatedEntries)
            tempMdAlbumEntry.flags|=FlagAlbumHasUndatedEntries;
        else
            tempMdAlbumEntry.flags&=~FlagAlbumHasUndatedEntries;
    }

    //compute the nameOffset
    //LOG0("Computing name offset for %s (%d)",parentMdAlbumName,tempMdAlbumEntryX.nameOffset);
    r=regexec(&ptrParsing->rxd,parentMdAlbumName,1,&ptrParsing->rxdm,0);
    ph_assert(r || ptrParsing->rxdm.rm_so==0,NULL); //this means that, if there is a match, it starts from beginning of the string
    if(!r && ptrParsing->rxdm.rm_so==0){
        //we can take out the identified expression
        tempMdAlbumEntryX.nameOffset=ptrParsing->rxdm.rm_eo;

        //check if we have a date here
        if(!albumNeedsParsing && tempMdAlbumEntry.earliestCaptureTime==DATE_MAX){
            LOG0("NO DATE for this album, but perhaps we can get one from the name!");
            tempMdAlbumEntry.earliestCaptureTime=getDateTimeFromString(parentMdAlbumName,0,0);
            if(tempMdAlbumEntry.earliestCaptureTime!=DATE_MAX && tempMdAlbumEntry.latestCaptureTime==DATE_MAX)
                tempMdAlbumEntry.latestCaptureTime=tempMdAlbumEntry.earliestCaptureTime;
        }
        if(strlen(parentMdAlbumName)==tempMdAlbumEntryX.nameOffset)//no other name, just a date
            tempMdAlbumEntryX.nameOffset=0;
        else tempMdAlbumEntry.flags|=FlagHasNameOffset;
    } else {ph_assert(tempMdAlbumEntryX.nameOffset==0,NULL);}
    //LOG0("Computed name offsert: %d",tempMdAlbumEntryX.nameOffset);
    //this pretty much concludes the creation of the thisMdAlbumEntry. The only thing missing is assignation of the album, right before returning


    if(!albumNeedsParsing){
        int j;
        ptrParsing->nrAllocatedChunks+=2;

        if(fastScan){
            ph_assert(!albumStructureSize,NULL); //was not calculated
            ph_assert(!albumExtraSize,NULL); //was not calculated
        } else {
            ptrParsing->sizeOfAllocatedChunks+=albumStructureSize+albumExtraSize;
            ptrParsing->sizeOfCachedData+=albumDatFileSize;
            ph_assert(albumDatFileSize==sizeof(struct md_albumdat_header)+albumStructureSize+albumExtraSize,"%d==%d+%d+%d",
                      albumDatFileSize,(int)sizeof(struct md_albumdat_header),albumStructureSize,albumExtraSize);
        }


        //we already concluded this album does not need parsing
        //here we determine if the parent album needs parsing or not
        ph_assert(tempMdAlbumEntry.permissions==parentMdAlbumEntry->permissions,NULL);

        LOG0c_do(!parentNeedsParsing && tempMdAlbumEntry.flags!=(parentMdAlbumEntry->flags&~RuntimeFlags),parentNeedsParsing=1,
                 "Scanned flags (%u) do not match parent existing flags (%u). Parent album will need parsing .",
                 (unsigned int)tempMdAlbumEntry.flags,(unsigned int)(parentMdAlbumEntry->flags&~RuntimeFlags));
        LOG0c_do(!parentNeedsParsing && tempMdAlbumEntryX.nameOffset!=parentMdAlbumEntryX->nameOffset,parentNeedsParsing=1,
                 "Computed nameOffset did (%u) not match existing nameOffset (%u). Parent album will need parsing.",
                 (unsigned int)tempMdAlbumEntryX.nameOffset,(unsigned int)parentMdAlbumEntryX->nameOffset);
        LOG0c_do(!parentNeedsParsing && tempMdAlbumEntry.earliestCaptureTime!=parentMdAlbumEntry->earliestCaptureTime,parentNeedsParsing=1,
                 "Scanned earliestCaptureTime (%x) does not match parent existing earliestCaptureTime (%x). Parent album will need parsing.",
                 (unsigned int)tempMdAlbumEntry.earliestCaptureTime,(unsigned int)parentMdAlbumEntry->earliestCaptureTime);
        LOG0c_do(!parentNeedsParsing && tempMdAlbumEntry.latestCaptureTime!=parentMdAlbumEntry->latestCaptureTime,parentNeedsParsing=1,
                 "Scanned latestCaptureTime (%x) does not match parent existing latestCaptureTime (%x). Parent album will need parsing.",
                 (unsigned int)tempMdAlbumEntry.latestCaptureTime,(unsigned int)parentMdAlbumEntry->latestCaptureTime);


        if(parentNeedsParsing){
            //check a bit the integrity of thisMdAlbumEntry
            ph_assert(tempMdAlbumEntry.flags&FlagIsAlbum,NULL);
            //ph_assert(tempMdAlbumEntry.earliestCaptureTime>0,NULL);
            ph_assert(tempMdAlbumEntry.latestCaptureTime>0,NULL);
            //we need to set parent data (we do it in scanpics)
            //memcpy((struct md_album_entry * const)parentMdAlbumEntry,thisMdAlbumEntry,sizeof(struct md_album_entry));
        } else {
            //parent data should match our collected data, except for the album and dataOffset
            ph_assert((parentMdAlbumEntry->flags&~RuntimeFlags)==tempMdAlbumEntry.flags,NULL);
            ph_assert(parentMdAlbumEntry->entriesIndex==tempMdAlbumEntry.entriesIndex,NULL);
            ph_assert(parentMdAlbumEntry->permissions==tempMdAlbumEntry.permissions,NULL);
            ph_assert(parentMdAlbumEntry->nameCRC==tempMdAlbumEntry.nameCRC,NULL);
            ph_assert(parentMdAlbumEntry->dataOffsetHi==tempMdAlbumEntry.dataOffsetHi,NULL);
            ph_assert(parentMdAlbumEntry->dataOffsetLo==tempMdAlbumEntry.dataOffsetLo,NULL);

            ph_assert(parentMdAlbumEntry->earliestCaptureTime==tempMdAlbumEntry.earliestCaptureTime,NULL);
            ph_assert(parentMdAlbumEntry->latestCaptureTime==tempMdAlbumEntry.latestCaptureTime,NULL);
        }

        ph_assert(!tempMdAlbumEntry.album,NULL);
        ph_assert(parentMdAlbumEntry->album,NULL);

        for(i=0;i<n;i++){
            if(fastScan){ph_assert(!namelist,NULL);}
            else free(namelist[i]);
            if(!(parentMdAlbumEntry->album[i].flags&FlagIsAlbum)){
                //count this picture or video
                r=increaseCameraPicturesCount(parentMdAlbumEntry->album[i].entriesIndex);
                debugNrPicturesCamera++;
                LOG0close_return(r==-1,-1,
                                 if(!fastScan){
                                     for(j=i+1;j<n;j++)free(namelist[j]);
                                     free(namelist);
                                     free(fInfo);
                                 };
                                 ptrParsing->forcePicturesFullScan=1,
                        "ERROR: camera not found.");
            }
        }
        if(!fastScan){
            free(namelist);
            free(fInfo);
        }

        //"Touch" the album.dat file so that we do not delete it later
        ph_assert(strlen(ptrParsing->dataDir)==ptrParsing->dataDirLen,NULL);
        ph_assert(ptrParsing->dataDir[ptrParsing->dataDirLen-1]=='/',NULL);
        memcpy(ptrParsing->dataDir+ptrParsing->dataDirLen,KAlbumFilename,strlen(KAlbumFilename)+1);
        //struct utimbuf utb={startTime,startTime};
        //r=utime(ptrParsing->dataDir,&utb);
        r=utime(ptrParsing->dataDir,NULL);
        ph_assert(!r,"utimes error: %s",strerror(errno));
        ptrParsing->dataDir[ptrParsing->dataDirLen]='\0';

        if(albumWasLoaded)
            ptrParsing->nrLoadedAlbums++;
        else
            ptrParsing->nrReusedAlbums++;

        //check the number of pictures
        int debugCountedPicsAndVids=0;
        for(i=0;i<n;i++){
            if(!(parentMdAlbumEntry->album[i].flags&FlagIsAlbum))
                debugCountedPicsAndVids++;
        }
        ph_assert(debugCountedPicsAndVids==debugNrPicturesCamera,"%d!=%d",debugCountedPicsAndVids,debugNrPicturesCamera);


        //returning procedure (this is similar with the one at the end of this function)
        ph_assert((tempMdAlbumEntry.earliestCaptureTime==DATE_MAX && tempMdAlbumEntry.latestCaptureTime==DATE_MAX) ||
                  (tempMdAlbumEntry.earliestCaptureTime!=DATE_MAX && tempMdAlbumEntry.latestCaptureTime!=DATE_MAX),
                  "%x %x",tempMdAlbumEntry.earliestCaptureTime,tempMdAlbumEntry.latestCaptureTime);
        //changing the parentMdAlbum requires write lock to mdPics
        ph_assert(!tempMdAlbumEntry.album,NULL);
        releaseMdPics(&ptrParsing->mdPics);
        mdPicsW=getMdPicsWritable();
        if(parentNeedsParsing){
            tempMdAlbumEntry.album=parentMdAlbumEntry->album; //so we can use memcpy
            memcpy(parentMdAlbumEntry,&tempMdAlbumEntry,sizeof(struct md_album_entry));
            memcpy(parentMdAlbumEntryX,&tempMdAlbumEntryX,sizeof(struct mdx_album_entry));
            tempMdAlbumEntry.album=NULL;
        }

        //we will return from this function, so mark our album that it is not being scaned anymore
        parentMdAlbumEntry->flags&=~FlagElmIsBeingScanned;
        mdPicsW->scannedEntries+=parentMdAlbumEntry->entriesIndex;

#ifdef DEBUG_PICS_FOLDER
        if(!statusFlagsCheck(StatusFlagDoARescan)){
            //find the current folder
            for(i=0;i<dpsNr;i++)
                if(dps[i].name && !strcmp(dps[i].name,ptrParsing->picsDir)){//found it
                    ph_assert(parentMdAlbumEntry->entriesIndex==dps[i].count,
                              "Mismatch %d vs %d for %s",parentMdAlbumEntry->entriesIndex,dps[i].count,dps[i].name);
                    dps[i].count=-dps[i].count;
                    break;
                }
            ph_assert(!parentMdAlbumEntry->entriesIndex || i<dpsNr,"Not found: %s, level: %d",ptrParsing->picsDir,level);
        }
#endif

        uint16_t oldCounter=parentMdAlbumEntryX->modificationCounter;
        propagateModification(mdPicsW);
        ph_assert(parentMdAlbumEntryX->modificationCounter==oldCounter+1,NULL);
        releaseMdPicsW(&mdPicsW);
        sendStatusToConnectedClients(&ptrParsing->currentAlbum);
        ptrParsing->mdPics=getMdPicsRd();

        //one more thing to do: delete those folders in our dataDir that have their modification time before startTime (updated in the beginning of this function)
        deleteUnusedFolders(startTime);
        LOG0("scanPicsFolder-- (level %d), album: %s returning %d",level,ptrParsing->picsDir,parentNeedsParsing);

        LOG_FLUSH;
        return parentNeedsParsing;
    }

    //if we are here, the album needs parsing. If this album needs parsing, the parent album needs parsing as well.
    LOG0("!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#!#    PARSING ALBUM: %s",ptrParsing->picsDir);
    ptrParsing->nrParsedAlbums++;
    //parentNeedsParsing=1; ->not really needed, because we do not use it anymore
    if(fastScan){
        LOG0("WARNING: FastScan failed for some reason, going back and doing a full scan!");
        //reinit some vars
        if(namelist){
            for(i=0;i<n;i++)free(namelist[i]);
            free(namelist);
            namelist=NULL;
        }
        if(fInfo){
            free(fInfo);
            fInfo=NULL;
        }
        albumDatMtime=INT32_MIN;
        albumNeedsParsing=0;
        parentNeedsParsing=0;
        albumExpectedSize=0;
        albumDatFileSize=albumStructureSize=albumExtraSize=albumWasLoaded=0;
        debugNrPicturesCamera=0;
        sizeOfUnknownsInAlbum=0;
        nrTotalUnknowns=0;

        initAlbum(&tempMdAlbumEntry);
        memset(&tempMdAlbumEntryX,0,sizeof(struct mdx_album_entry));
        fastScan=0;

        goto fastScanFailed;
    }
    ph_assert(!fastScan,NULL); //if we have fastScan, we are missing some data

    if(parentMdAlbumEntry->album && parentMdAlbumEntry->entriesIndex){
        ph_assert(*(struct mdx_album_entry**)(parentMdAlbumEntry->album+parentMdAlbumEntry->entriesIndex),NULL);
        //LOG0("Pointers for album: %p and %p",parentMdAlbumEntry->album,*(struct mdx_album_entry**)(parentMdAlbumEntry->album+parentMdAlbumEntry->entriesIndex));
    }

    int j,k;
    int nrPicsWithDate=0,nrPicsWithoutDate=0; //we have pictures with date in front, pics without date at the end, and folders between
    int nrPicsWithoutTimezone=0,nrTimezones=0;
    int8_t timezones[NR_TRACKED_TIMEZONES];

    //Allocate entries
    uint32_t nameStringsLen=0,descriptionStringsLen=0;
    struct mdh_album_description *albumDescriptor=NULL;
    struct mdh_album_entry **entries=(struct mdh_album_entry**)ph_malloc(n*sizeof(struct mdh_album_entry*));
    memset(entries,0,n*sizeof(struct mdh_album_entry*));

    //do we have description file for this album?
    ph_assert(picsDirLenSave==ptrParsing->picsDirLen,NULL);
    memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,KDescriptionFilename,strlen(KDescriptionFilename)+1);
    albumHasDescriptionFile=hasAlbumDescription(ptrParsing->picsDir);
    ptrParsing->picsDir[picsDirLenSave]='\0';

    //processing the photos, videos and partially the albums (so that we can create the md_album_entry structure quickly
    if(n>0){
        if(!fInfo){
            fInfo=(struct mdh_file_info*)ph_malloc(n*sizeof(struct mdh_file_info));
            ph_assert(albumExpectedSize==0,NULL); //this way we know we need to fill in the fInfo
        } else
            ph_assert(albumExpectedSize,NULL);
    }

    for(i=0;i<n;i++){
        struct dirent * __restrict d=namelist[i];
        l=strlen(d->d_name);
        nameStringsLen+=l+1;

        if(d->d_type==DT_UNKNOWN || !albumExpectedSize){
            //we must find the type
            ph_assert(strlen(ptrParsing->picsDir)==ptrParsing->picsDirLen,NULL);
            memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,d->d_name,l+1);

            if(!stat(ptrParsing->picsDir,&ptrParsing->statBuf)){
                //stat succeeded
                if(d->d_type==DT_UNKNOWN){
                    if(S_ISREG(ptrParsing->statBuf.st_mode))d->d_type=DT_REG;
                    else if(S_ISDIR(ptrParsing->statBuf.st_mode))d->d_type=DT_DIR;
                    else ph_assert(0,"Cannot find entry type. ");
                }
                if(!albumExpectedSize){
                    fInfo[i].size=ptrParsing->statBuf.st_size;
                    fInfo[i].mtime=ptrParsing->statBuf.st_mtime;
                }
            } else {
                //stat failed
                LOG0close_return(1,-1,for(i=0;i<n;i++)free(namelist[i]);free(namelist);free(fInfo),
                        "stat-ing %s failed: %s. Forcing full rescan.",ptrParsing->picsDir,strerror(errno));
            }
            ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
        }

        struct mdh_album_entry c;
        int index=-1,j;
        struct md_album_entry const * __restrict a=NULL;
        struct mdx_album_entry const * __restrict ax=NULL;
        //init tempBuffer
        ph_assert(ptrParsing->tempBuf,NULL);
        ph_assert(ptrParsing->tempBufSize>=TEMP_BUF_MIN_SIZE,NULL);
        ptrParsing->tempBufPos=0;

        //if we have a valid album structure, try to identify the current element
        c.nameCRC=ph_crc32b(d->d_name)&0xFFFFFFFC;
        c.modificationTime=fInfo[i].mtime;
        if(parentMdAlbumEntry->album){
            //LOG0("Trying to find %s (n=%d, parent album has %d elements)",d->d_name,n,parentMdAlbumEntry->entriesIndex);
            for(j=0;j<parentMdAlbumEntry->entriesIndex;j++){
                //LOG0("    Trying %X with %X (%s)",parentMdAlbumEntry->album[j].nameCRC&0xFFFFFFFC,c.nameCRC,getNameFromAlbum(parentMdAlbumEntry->album,j));
                if((parentMdAlbumEntry->album[j].nameCRC&0xFFFFFFFC)==c.nameCRC){
                    //check also the name
                    char *name=getNameFromAlbum(parentMdAlbumEntry->album,j);
                    if(!strcmp(name,d->d_name)){
                        index=j;
                        //found it!
                        //LOG0("Found something! mtime: %u (%s)",c.modificationTime,d->d_name);
                        a=&parentMdAlbumEntry->album[index];
                        ax=getExtraEntry(parentMdAlbumEntry->album,parentMdAlbumEntry->entriesIndex,index);
                        break;
                    }
                }
            }
        }
        //if j>=0 then we have an element from the old album from where we can get the data

        if(d->d_type==DT_DIR){
            //this is an album. Quick-process it
            struct mdh_album_entry_album ca;
            int nameOffset=0;

            if(a){
                //we can initialize c and ca from the old album element
                LOG0("Found reusable info about the current  ALBUM  (%s%s). Scanning flag: %d",ptrParsing->picsDir,d->d_name,a->flags&FlagElmIsBeingScanned);
                c.size=0; //not used for albums
                c.captureTime=a->earliestCaptureTime;
                c.flags=a->flags;
                c.entriesIndex=a->entriesIndex;

                ca.mdAlbum=a->album;
                ca.latestCaptureTime=a->latestCaptureTime;

                if(c.flags&FlagHasNameOffset){
                    nameOffset=ax->nameOffset;
                    ph_assert(nameOffset>0,"%d",nameOffset);
                }
            } else {
                //we need to initialize c and ca with default/null values
                c.size=0;
                c.captureTime=DATE_MAX;
                c.flags=FlagIsAlbum|FlagElmIsBeingScanned;
                c.entriesIndex=0;

                ca.mdAlbum=NULL;
                ca.latestCaptureTime=DATE_MAX;
            }

            //timezone stuff
            if(c.captureTime!=DATE_MAX)
                nrPicsWithoutTimezone++; //because we do not know how to get the timezone for videos and albums

            //reserve space for offset in description file and for permission byte
            ph_assert(ptrParsing->tempBufPos==0,NULL);
            if(albumHasDescriptionFile){
                *(char**)(ptrParsing->tempBuf/*+ptrParsing->tempBufPos*/)=NULL;
                *(uint8_t*)(ptrParsing->tempBuf/*+ptrParsing->tempBufPos*/+sizeof(void*))=0;
                ptrParsing->tempBufPos/*+*/=sizeof(void*)+1;
            }

            //this is the computed nameOffset
            if(c.flags&FlagHasNameOffset){
                ph_assert(nameOffset>0,NULL);
                *(uint8_t*)(ptrParsing->tempBuf+ptrParsing->tempBufPos++)=nameOffset;
            }

            //add the filename
            memcpy(ptrParsing->tempBuf+ptrParsing->tempBufPos,d->d_name,l+1);
            ptrParsing->tempBufPos+=l+1;

            //create mdh element
            index=getCurrentEntryIndex(entries,n,c.captureTime,&nrPicsWithDate,&nrPicsWithoutDate);
            entries[index]=ph_malloc(sizeof(struct mdh_album_entry)+sizeof(struct mdh_album_entry_album)+ptrParsing->tempBufPos);
            memcpy(entries[index],&c,sizeof(struct mdh_album_entry));
            memcpy(entries[index]+1,&ca,sizeof(struct mdh_album_entry_album));
            memcpy((char*)(entries[index])+sizeof(struct mdh_album_entry)+sizeof(struct mdh_album_entry_album),ptrParsing->tempBuf,ptrParsing->tempBufPos);

            //done for this album/folder
            free(d);
            continue;
        }//done for album

        ph_assert(d->d_type==DT_REG,NULL); //there should be no other case here

        //is this a picture or a video?
        ph_assert(l>4,NULL); //otherwise it would not have passed the scandir filter
        char *extension=(char*)d->d_name+l-4;
        int mustCompletePictureRegistration=0;
        struct mdh_album_entry_picture cp;
        ExifData *ed=NULL;

        if(!strcasecmp(extension,KJpegExtension) || !strcasecmp(extension,KJpegExtension2)){
            //this is a JPEG picture

            debugNrPicturesCamera++;
            if(a && fInfo[i].size==a->size && c.modificationTime<albumDatMtime && c.modificationTime==a->modificationTime){
                //we can initialize c and cp from the old album element
                r=initPictureFromSaved(&c,&cp,&tempMdAlbumEntry,d->d_name,a,ax);
                LOG0close_return(r,r,for(k=0;k<n;k++)free(namelist[k]);free(namelist);free(fInfo);ptrParsing->forcePicturesFullScan=1,
                        "ERROR in picture initialization");
            } else {
                //if we are here it means that the picture should be parsed (was not found in the old album)
                ph_assert(strlen(ptrParsing->picsDir)==ptrParsing->picsDirLen,NULL);
                memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,d->d_name,l+1);

                //exif part
                ed=exif_data_new_from_file(ptrParsing->picsDir);
                if(ed)
                    parsePictureExif(ed,&c,&cp,&tempMdAlbumEntry,d->d_name);
                else
                    initForPictureWithoutExif(&c,&cp,d->d_name);

                //get IPTC info from file
                IptcData *dy=iptc_data_new_from_jpeg(ptrParsing->picsDir);
                if(dy){
                    int yy=-1,mm=-1,dd=-1,hh=-1,mn=-1,ss=-1,tzh=-1,tzm=-1;
#ifndef NDEBUG
                    if(dy->count){
                        LOG0("%6.6s %-20.20s %-9.9s %6s  %s", "Tag", "Name", "Type","Size", "Value");
                        LOG0(" ----- -------------------- --------- ------  -----");
                    }
#endif
                    for(k=0;k<dy->count;k++){
                        IptcDataSet * e = dy->datasets[k];
                        char buf[256];
#ifndef NDEBUG
                        char buf2[256];
                        switch (iptc_dataset_get_format (e)) {
                        case IPTC_FORMAT_BYTE:
                        case IPTC_FORMAT_SHORT:
                        case IPTC_FORMAT_LONG:
                            sprintf(buf2,"%d", iptc_dataset_get_value (e));
                            break;
                        case IPTC_FORMAT_BINARY:
                            iptc_dataset_get_as_str (e, buf, sizeof(buf));
                            sprintf(buf2,"%s", buf);
                            break;
                        default:
                            iptc_dataset_get_data (e, (unsigned char*)buf, sizeof(buf));
                            sprintf(buf2,"%s", buf);
                            break;
                        }

                        LOG0("%2d:%03d %-20.20s %-9.9s %6d  %s",
                             e->record, e->tag,
                             iptc_tag_get_title (e->record, e->tag),
                             iptc_format_get_name (iptc_dataset_get_format (e)),
                             e->size,buf2);
#endif
                        if(e->record==2 && e->tag==55){
                            //Date Created
                            iptc_dataset_get_data(e, (unsigned char *)buf, sizeof(buf));
                            int r=sscanf(buf,"%d-%d-%d",&yy,&mm,&dd);
                            if(r!=3 || yy<1900 || yy>2050 || mm<1 || mm>12 || dd<1 || dd>31)
                                yy=mm=dd=-1;
                            else LOG0("----------------- IPTC DATE CREATED: %d-%d-%d",yy,mm,dd);
                        }
                        if(e->record==2 && e->tag==60){
                            //Time Created
                            iptc_dataset_get_data(e, (unsigned char *)buf, sizeof(buf));
                            int hms=-1,tz=-1,r;
                            r=sscanf(buf,"%d%d",&hms,&tz);
                            if(r!=2)
                                hms=tz=-1;
                            else {
                                ss=hms%100;
                                hms-=ss;
                                hms/=100;

                                mn=hms%100;
                                hms-=mn;
                                hms/=100;

                                hh=hms;

                                tzm=tz%100;
                                tzh=(tz-tzm)/100;

                                if(hh>23 || mn>59 || ss>59 || tzh>12 || tzh<-12 || tzm>59)
                                    hh=mn=ss=tzh=tzm=-1;
                                else LOG0("----------------- IPTC TIME CREATED: %d:%02d:%02d TZ: %d:%02d",hh,mn,ss,tzh,tzm);
                            }
                        }
                    }//for(k=0...
                    iptc_data_unref(dy);
                    dy=NULL;

                    if(yy!=-1 && hh!=-1){
                        //IPCT capture is valid!!


                        struct tm t;
                        memset(&t, 0, sizeof(struct tm));

                        t.tm_year=yy-1900;
                        t.tm_mon=mm-1;
                        t.tm_mday=dd;
                        t.tm_hour=hh;
                        t.tm_min=mn;
                        t.tm_sec=ss;

                        int32_t captureTime=(int32_t)timegm(&t);
                        int8_t timezone=tzh<0?-tzh*4-tzm/15:tzh*4+tzm/15;

                        //if we have IPTC date, use it!
                        c.captureTime=captureTime;
                        cp.timezone=timezone;

                        LOG0("----------------- IPTC DATETIME IS VALID!! Timezone 15min: %d",(int)cp.timezone);
                        /*
                        if(c.captureTime==DATE_MAX){
                            c.captureTime=captureTime;
                            cp.timezone=timezone;
                        } else {
                            int diff=c.captureTime-captureTime;
                            if(diff){
                                char buf1[128],buf2[128];
                                dateToString(captureTime,timezone,buf1,128);
                                dateToString(c.captureTime,cp.timezone,buf2,128);
                                LOG0("%%%%%%%%%% captureTimeDifference: %s TZ:(IPTC) vs %s (EXIF)",buf1,buf2);
                            }
                        }*/
                    }
                }//if(dy)


                //pictures common part (EXIF or without EXIF)
                //get the dimensions of this picture
                getJpegDimensions(ptrParsing->picsDir,&cp.width,&cp.height);

                //get the size of this file
                if(fInfo[i].size>0xFFFFFFFF){
                    LOG0("WARNING: size of this picture (%s) is really big!!",d->d_name);
                    c.size=fInfo[i].size>>20; //in MB
                    c.flags|=FlagSizeIsInMB;
                } else c.size=fInfo[i].size;
            }
            mustCompletePictureRegistration=1;
        } else if(!strcasecmp(extension,KHeicExtension)){
            LOG0("HEIC Picture found!");

            debugNrPicturesCamera++;
            if(a && fInfo[i].size==a->size && c.modificationTime<albumDatMtime && c.modificationTime==a->modificationTime){
                //we can initialize c and cp from the old album element
                r=initPictureFromSaved(&c,&cp,&tempMdAlbumEntry,d->d_name,a,ax);
                LOG0close_return(r,r,for(i=0;i<n;i++)free(namelist[i]);free(namelist);free(fInfo);ptrParsing->forcePicturesFullScan=1,
                        "ERROR in picture initialization");
            } else {
                //if we are here it means that the picture should be parsed (was not found in the old album)
                ph_assert(strlen(ptrParsing->picsDir)==ptrParsing->picsDirLen,NULL);
                memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,d->d_name,l+1);

                //exif part
                struct heif_context* ctx=heif_context_alloc();
                ph_assert(ctx,"Cannot create HEIF context");
                struct heif_error err=heif_context_read_from_file(ctx, ptrParsing->picsDir, NULL);
                if(err.code){
                    LOG0("Cannot read HEIF");
                    heif_context_free(ctx);
                    ctx=NULL;
                }
                if(ctx){
                    int num_images=heif_context_get_number_of_top_level_images(ctx);
                    if(!num_images) {
                        LOG0("File (%s) doesn't contain any images",ptrParsing->picsDir);
                        heif_context_free(ctx);
                        ctx=NULL;
                    } else LOG0("File (%s) contains %d images!",ptrParsing->picsDir,num_images);
                }
                struct heif_image_handle* handle=NULL;
                if(ctx){
                    err=heif_context_get_primary_image_handle(ctx,&handle);
                    if(err.code || !handle){
                        LOG0("Could not get handle for HEIC image (%s).",ptrParsing->picsDir);
                        heif_context_free(ctx);
                        ctx=NULL;
                    }
                }
                if(handle){
                    cp.width=heif_image_handle_get_width(handle);
                    cp.height=heif_image_handle_get_height(handle);
                    LOG0("HEIC image is %dx%d",cp.width,cp.height);

                    int nrThumbnails=heif_image_handle_get_number_of_thumbnails(handle);
                    int nrAuxiliary=heif_image_handle_get_number_of_auxiliary_images(handle,0);
                    LOG0("HEIC image has %d thumbnails and %d auxiliary images",nrThumbnails,nrAuxiliary);

                    int nrMetadataBlocks=heif_image_handle_get_number_of_metadata_blocks(handle,NULL);
                    LOG0("We have %d metadata blocks!",nrMetadataBlocks);
                    heif_item_id *mdIds=malloc(nrMetadataBlocks*sizeof(heif_item_id));
                    heif_image_handle_get_list_of_metadata_block_IDs(handle,NULL,mdIds,nrMetadataBlocks);

                    for(k=0;k<nrMetadataBlocks;k++){
                        const char *mdName=heif_image_handle_get_metadata_type(handle,mdIds[k]);
                        size_t mdSize=heif_image_handle_get_metadata_size(handle,mdIds[k]);
                        LOG0("Metadata name: %s, size: %zu",mdName,mdSize);
                        unsigned char *mdData=malloc(mdSize);

                        err=heif_image_handle_get_metadata(handle,mdIds[k],mdData);

                        if(!strcasecmp(mdName,"exif")){
                            ed=exif_data_new_from_data(mdData+4,mdSize-4);//offset: 4 because first 4 bytes are some header offset
                            if(ed){
                                LOG0("We have EXIF data, yeeei!!");
                                parsePictureExif(ed,&c,&cp,&tempMdAlbumEntry,d->d_name);
                                //remove the rotation flags because the created jpeg is already rotated
                                c.flags&=~(FlagRotatePlus90+FlagRotatePlus180);
                            } else LOG0("No EXIF data :-(");

                        } else {
                            //LOG0bin(mdData,mdSize,1);
                        }



                        free(mdData);
                    }
                    free(mdIds);
                }

                //we need to create a jpeg for this file, as no browser can display HEIC
                char transPath[PATH_MAX];
                size_t jpegSize=0;
                getTranscodedPathForMedia(ptrParsing->picsDir,"jpeg",transPath);
                createJpegFromHeif(ctx,handle,transPath,ptrParsing->picsDir,&jpegSize);
                heif_image_handle_release(handle);
                heif_context_free(ctx);

                //get the size of this file
                if(jpegSize>0xFFFFFFFF){
                    LOG0("WARNING: size of this picture (%s) is really big!!",transPath);
                    c.size=jpegSize>>20; //in MB
                    c.flags|=FlagSizeIsInMB;
                } else c.size=jpegSize;

                if(!ed)
                    initForPictureWithoutExif(&c,&cp,d->d_name);
            }
            mustCompletePictureRegistration=1;
        } else if(!strcasecmp(extension,KMp4Extension) || !strcasecmp(extension,KM4vExtension) || !strcasecmp(extension,KMovExtension)){
            //this is a video
            struct mdh_album_entry_video cv;

            if(a && fInfo[i].size==(a->flags&FlagSizeIsInMB?((off_t)a->size<<20)+ax->sizeLo:a->size) && c.modificationTime<albumDatMtime && c.modificationTime==a->modificationTime){
                //we can initialize c and cv from the old album element
                LOG0("Found reusable info about the current  VIDEO  (%s/%s)",ptrParsing->picsDir,d->d_name);
                c.size=a->size;
                c.captureTime=a->captureTime;
                c.flags=a->flags;
                c.entriesIndex=a->entriesIndex;
                r=increaseCameraPicturesCount(c.entriesIndex);
                debugNrPicturesCamera++;
                LOG0close_return(r==-1,-1,for(i=0;i<n;i++)free(namelist[i]);free(namelist);free(fInfo);ptrParsing->forcePicturesFullScan=1,"ERROR: camera not found.");

                cv.lat=a->lat;
                cv.lng=a->lng;
                cv.width=ax->width;
                cv.height=ax->height;
                cv.duration=ax->duration;
                cv.timezone=ax->timezone;
                cv.sizeLo=ax->sizeLo;

                if(KFFprobeBin && (!cv.width || cv.width>10000 || !cv.height || cv.height>10000))
                    ffprobeVideo(ptrParsing->picsDir,&c,&cv);
            } else {
                //we need to "parse" this video
                LOG0("Parsing this video");
                ph_assert(strlen(ptrParsing->picsDir)==ptrParsing->picsDirLen,NULL);
                memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,d->d_name,l+1);

                c.flags=FlagIsVideo;
                c.entriesIndex=CAMERA_INDEX_NO_CAMERA_INFO; //if we cannot parse it from the video
                c.captureTime=DATE_MAX; //if we cannot parse it from the video
                cv.duration=0; //if we cannot parse it from the video
                ffprobeVideo(ptrParsing->picsDir,&c,&cv);
                if(c.entriesIndex==CAMERA_INDEX_NO_CAMERA_INFO)
                    ptrParsing->nrPicturesWithUnknownCamera++;
                debugNrPicturesCamera++;

                if(c.captureTime==DATE_MAX)
                    c.captureTime=getDateTimeFromString(d->d_name,1,1);

                //get the size of this file
                if(fInfo[i].size>0xFFFFFFFF){
                    c.size=fInfo[i].size>>20; //in MB
                    c.flags|=FlagSizeIsInMB;
                    cv.sizeLo=fInfo[i].size&((1<<20)-1);
                } else {
                    c.size=fInfo[i].size;
                    cv.sizeLo=0;
                }
            }

            //timezone stuff
            if(c.captureTime!=DATE_MAX)
                nrPicsWithoutTimezone++; //because we do not know how to get the timezone for videos and albums

            //reserve space for offset in description file and for permission byte
            ph_assert(ptrParsing->tempBufPos==0,NULL);
            if(albumHasDescriptionFile){
                *(char**)(ptrParsing->tempBuf+ptrParsing->tempBufPos)=NULL;
                *(uint8_t*)(ptrParsing->tempBuf+ptrParsing->tempBufPos+sizeof(void*))=0;
                ptrParsing->tempBufPos+=sizeof(void*)+1;
            }

            //copy filename to tempBuf
            ph_assert(l==strlen(d->d_name),NULL);
            ph_assert(l<TEMP_BUF_MIN_SIZE,NULL);
            memcpy(ptrParsing->tempBuf+ptrParsing->tempBufPos,d->d_name,l+1);
            ptrParsing->tempBufPos+=l+1;
            REALLOCATE_TEMP_IF_NEEDED;

            //create mdh element
            index=getCurrentEntryIndex(entries,n,c.captureTime,&nrPicsWithDate,&nrPicsWithoutDate);
            entries[index]=ph_malloc(sizeof(struct mdh_album_entry)+sizeof(struct mdh_album_entry_video)+ptrParsing->tempBufPos);
            memcpy(entries[index],&c,sizeof(struct mdh_album_entry));
            memcpy(entries[index]+1,&cv,sizeof(struct mdh_album_entry_video));
            memcpy((char*)(entries[index])+sizeof(struct mdh_album_entry)+sizeof(struct mdh_album_entry_video),ptrParsing->tempBuf,ptrParsing->tempBufPos);
        } else ph_assert(0,NULL); //there should be no other case here

        if(mustCompletePictureRegistration){
            //timezone stuff
            if(cp.timezone==TIMEZONE_UNAVAILABLE){
                if(c.captureTime!=DATE_MAX)
                    nrPicsWithoutTimezone++;
            } else {
                //find our timezone
                for(j=0;j<nrTimezones;j++)
                    if(timezones[j]==cp.timezone)break;
                if(j>=nrTimezones && nrTimezones<NR_TRACKED_TIMEZONES)
                    timezones[nrTimezones++]=cp.timezone;//add this timezone
            }

            //reserve space for offset in description file and for permission byte
            ph_assert(ptrParsing->tempBufPos==0,NULL);
            if(albumHasDescriptionFile){
                *(char**)(ptrParsing->tempBuf+ptrParsing->tempBufPos)=NULL;
                *(uint8_t*)(ptrParsing->tempBuf+ptrParsing->tempBufPos+sizeof(void*))=0;
                ptrParsing->tempBufPos+=sizeof(void*)+1;
            }

            //copy filename to tempBuf
            ph_assert(l==strlen(d->d_name),NULL);
            ph_assert(l<TEMP_BUF_MIN_SIZE,NULL);
            memcpy(ptrParsing->tempBuf+ptrParsing->tempBufPos,d->d_name,l+1);
            ptrParsing->tempBufPos+=l+1;
            REALLOCATE_TEMP_IF_NEEDED;

            if(a && fInfo[i].size==a->size && c.modificationTime<albumDatMtime && c.modificationTime==a->modificationTime){
                //we can have description and/or user comments in the copied element
                ph_assert(!ed,NULL);
                char *axData=getExtraEntryData(parentMdAlbumEntry->album,parentMdAlbumEntry->entriesIndex,index);
                if(c.flags&FlagHasDescription){
                    ph_assert(ax->stringsOffset>0,NULL);
                    LOG0("Found Picture Description in the reusable picture info: %s",axData);
                    l=strlen(axData)+1;
                    memcpy(ptrParsing->tempBuf+ptrParsing->tempBufPos,axData,l);
                    ptrParsing->tempBufPos+=l;
                    descriptionStringsLen+=l;
                    REALLOCATE_TEMP_IF_NEEDED;
                    axData+=l;
                }
                if(c.flags&FlagHasUserComment){
                    ph_assert(ax->stringsOffset>0,NULL);
                    //TODO: memory leaks here !!!!
                    LOG0("Found User Comment in the reusable picture info: %s (offset: %d)",axData,ax->stringsOffset);
                    l=strlen(axData)+1;
                    memcpy(ptrParsing->tempBuf+ptrParsing->tempBufPos,axData,l);
                    ptrParsing->tempBufPos+=l;
                    descriptionStringsLen+=l;
                    REALLOCATE_TEMP_IF_NEEDED;
                    axData+=l;
                }
            }
            if(ed){
                //check if there is any image description
                get_tag(ptrParsing->tempBuf+ptrParsing->tempBufPos,ptrParsing->tempBufSize-ptrParsing->tempBufPos,
                        ed,EXIF_IFD_0,EXIF_TAG_IMAGE_DESCRIPTION);
                ptrParsing->tempBufLen=strlen(ptrParsing->tempBuf+ptrParsing->tempBufPos);
                if(ptrParsing->tempBufLen>0){
                    c.flags|=FlagHasDescription;
                    ptrParsing->tempBufPos+=ptrParsing->tempBufLen+1;
                    descriptionStringsLen+=ptrParsing->tempBufLen+1;
                    REALLOCATE_TEMP_IF_NEEDED;
                }

                //check if there are user comments
                get_tag(ptrParsing->tempBuf+ptrParsing->tempBufPos,ptrParsing->tempBufSize-ptrParsing->tempBufPos,
                        ed,EXIF_IFD_EXIF,EXIF_TAG_USER_COMMENT);
                ptrParsing->tempBufLen=strlen(ptrParsing->tempBuf+ptrParsing->tempBufPos);
                if(ptrParsing->tempBufLen>0){
                    LOG0("Found User comment in picture <<%s>>: %s (length: %d)",d->d_name,ptrParsing->tempBuf+ptrParsing->tempBufPos,ptrParsing->tempBufLen);
                    c.flags|=FlagHasUserComment;
                    ptrParsing->tempBufPos+=ptrParsing->tempBufLen+1;
                    descriptionStringsLen+=ptrParsing->tempBufLen+1;
                    REALLOCATE_TEMP_IF_NEEDED;
                }

                //free the exif data
                exif_data_unref(ed);
                ed=NULL;
            }
            if(c.flags&FlagHasDescription)
                ph_assert(ptrParsing->tempBufPos>strlen(d->d_name)+1,"%d %zu",ptrParsing->tempBufPos,strlen(d->d_name)+1);
            if(c.flags&FlagHasDescription)
                ph_assert(ptrParsing->tempBufPos>strlen(d->d_name)+1,"%d %zu",ptrParsing->tempBufPos,strlen(d->d_name)+1);

            //create mdh element
            index=getCurrentEntryIndex(entries,n,c.captureTime,&nrPicsWithDate,&nrPicsWithoutDate);
            entries[index]=ph_malloc(sizeof(struct mdh_album_entry)+sizeof(struct mdh_album_entry_picture)+ptrParsing->tempBufPos);
            memcpy(entries[index],&c,sizeof(struct mdh_album_entry));
            memcpy(entries[index]+1,&cp,sizeof(struct mdh_album_entry_picture));
            memcpy((char*)(entries[index])+sizeof(struct mdh_album_entry)+sizeof(struct mdh_album_entry_picture),ptrParsing->tempBuf,ptrParsing->tempBufPos);
        }

        //common stuff for pictures (with or without EXIF) and movies
        if(c.captureTime==DATE_MAX)
            tempMdAlbumEntry.flags|=FlagAlbumHasUndatedEntries;
        else {
            //set earliestCaptureTime and latest capture time
            if(tempMdAlbumEntry.earliestCaptureTime>c.captureTime)
                tempMdAlbumEntry.earliestCaptureTime=c.captureTime;
            if(tempMdAlbumEntry.latestCaptureTime==DATE_MAX || tempMdAlbumEntry.latestCaptureTime<c.captureTime)
                tempMdAlbumEntry.latestCaptureTime=c.captureTime;
        }
        //done for this picture or video
        ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
        free(d);
    }
    free(namelist); //we do not need it any more
    free(fInfo);
    namelist=NULL;
    fInfo=NULL;
    ph_assert(picsDirLenSave==strlen(ptrParsing->picsDir),NULL);
    ph_assert(dataDirLenSave==strlen(ptrParsing->dataDir),NULL);

    //get the album description for this album
    if(albumHasDescriptionFile){
        memcpy(ptrParsing->picsDir+picsDirLenSave,KDescriptionFilename,strlen(KDescriptionFilename)+1);
        struct mdh_album_description *albumDescr=getAlbumDescription(ptrParsing->picsDir); //returned struct can be NULL if there is no description
        LOG0c(!albumDescr,"WARNING: The album description file (%s) could not be opened and parsed!",ptrParsing->picsDir);
        ptrParsing->picsDir[picsDirLenSave]='\0';

        //find elements description in the description file
        l=0;
        for(i=0;i<n;i++)
            setEntryDescription(entries[i],albumDescr);
        //done with finding descriptions
        checkAndFreeAlbumEntries(albumDescr);
    }

    //re-sort the pictures with date, if all have timezone info and we have at least 2 timezones
    if(nrPicsWithoutTimezone==0 && nrTimezones>1 && nrPicsWithDate>0){
        LOG0("Re-sorting pictures, %d timezones detected:",nrTimezones);
        for(i=0;i<nrTimezones;i++)
            LOG0("Timezone %d: %f",i,(float)timezones[i]/4.0f);

        //bubble-sort
        int newN,currentN=nrPicsWithDate;
        struct mdh_album_entry *temp;
        do{
            newN=0;
            //LOG0("Current N: %d/%d",currentN,nrPicsWithDate);
            for(i=1;i<currentN;i++){
                struct mdh_album_entry *ci_1=entries[i-1];
                struct mdh_album_entry *ci=entries[i];
                ph_assert(!(ci_1->flags&FlagIsAlbum) && !(ci_1->flags&FlagIsVideo),NULL);
                ph_assert(!(ci->flags&FlagIsAlbum) && !(ci->flags&FlagIsVideo),NULL);
                int32_t ct_1=ci_1->captureTime,ct=ci->captureTime;
                //LOG0("TZ offset (%2d): %d",i-1,(int32_t)(((struct mdh_album_entry_picture*)(ci_1+1))->timezone)*900);
                //LOG0("TZ offset (%2d): %d",i,(int32_t)(((struct mdh_album_entry_picture*)(ci+1))->timezone)*900);
                ct_1-=(int32_t)(((struct mdh_album_entry_picture*)(ci_1+1))->timezone)*900;
                ct-=(int32_t)(((struct mdh_album_entry_picture*)(ci+1))->timezone)*900;
                //LOG0("  Comparing %d with %d (diff: %d)",ct_1,ct,ct_1-ct);
                //compare
                if(ct_1>ct){
                    //swap
                    temp=entries[i-1];
                    entries[i-1]=entries[i];
                    entries[i]=temp;
                    newN=i;
                    //LOG0("Swapped %d with %d",i-1,i);
                }
            }//for
            currentN=newN;
        }while(newN);
    } else LOG0c(nrTimezones>1 && nrPicsWithDate>0,"%d timezones detected, but %d pictures lack a timezone. Most probably the picture order is wrong.",nrTimezones,nrPicsWithoutTimezone);

    //sort the pictures without date
    ph_assert(nrPicsWithDate+nrPicsWithoutDate==n,NULL);
    if(nrPicsWithoutDate>0){
        LOG0("Sorting pictures in %s",ptrParsing->picsDir);
        /*
        LOG0("Pics with date:");
        for(i=0;i<nrPicsWithDate;i++){
            //get the current entry
            struct mdh_album_entry *ci=&(entries[i]);
            char *nameI=getFilenameFromEntry(ci);

            LOG0("%s",nameI);
        }*/


        //we should sort the pictures without date by name
        for(i=nrPicsWithDate;i<n;i++){
            //get the current entry
            struct mdh_album_entry *ci=entries[i];
            char *nameI=getFilenameFromEntry(ci);
            int j;
            if(ci->flags&FlagIsAlbum)
                LOG0("Sorting %s, having %d elements",nameI,ci->entriesIndex);

            for(j=0;j<i;j++){
                struct mdh_album_entry *cCurrent=entries[j];
                char *nameCurrent=getFilenameFromEntry(cCurrent);

                if(strcasecmp(nameI,nameCurrent)<0){
                    //we insert i between prev and current
                    LOG2("Inserting %s before %s",nameI,nameCurrent);
                    struct mdh_album_entry *temp=ci;
                    memmove(entries+j+1,entries+j,(i-j)*sizeof(void*));
                    *(entries+j)=temp;

                    //we are done for this element
                    break;
                }
            }//for(j=...
        }//for(i=...

        //lets print what we have sorted
        LOG0("We sorted %d pictures here.",nrPicsWithoutDate);
    }
    //here we have the pictures sorted
    nrPicsWithDate=nrPicsWithoutDate=-1; //because their values do not make sense anymore


    //create and save the md_album_entry structure
    tempMdAlbumEntry.album=createTempMdAlbumEntry((struct mdh_album_entry const * const * const)entries,n,nameStringsLen,descriptionStringsLen,
                                                  nrTotalUnknowns,sizeOfUnknownsInAlbum);
    //here we can assign the album to the parent album, just in case a client will request some data
    releaseMdPics(&ptrParsing->mdPics);
    mdPicsW=getMdPicsWritable();
    struct md_album_entry *oldAlbum=parentMdAlbumEntry->album;
    uint16_t oldN=parentMdAlbumEntry->entriesIndex;
    parentMdAlbumEntry->album=tempMdAlbumEntry.album;
    parentMdAlbumEntry->entriesIndex=n;
    //update dates
    if(tempMdAlbumEntry.earliestCaptureTime<parentMdAlbumEntry->earliestCaptureTime)
        parentMdAlbumEntry->earliestCaptureTime=tempMdAlbumEntry.earliestCaptureTime;
    if(tempMdAlbumEntry.latestCaptureTime!=DATE_MAX && (tempMdAlbumEntry.latestCaptureTime>parentMdAlbumEntry->latestCaptureTime || parentMdAlbumEntry->latestCaptureTime==DATE_MAX))
        parentMdAlbumEntry->latestCaptureTime=tempMdAlbumEntry.latestCaptureTime;

    uint16_t oldCounter=parentMdAlbumEntryX->modificationCounter;
    propagateModification(mdPicsW);
    ph_assert(parentMdAlbumEntryX->modificationCounter==oldCounter+1,NULL);
    releaseMdPicsW(&mdPicsW);
    tempMdAlbumEntry.album=NULL;
    sendStatusToConnectedClients(&ptrParsing->currentAlbum);
    ptrParsing->mdPics=getMdPicsRd();

    for(i=0;i<n;i++)
        free(entries[i]);
    free(entries);entries=NULL;

    if(albumDescriptor){
        freeAlbumDescriptionStruct(albumDescriptor);
        free(albumDescriptor);
        albumDescriptor=NULL;
    }

    //we can now free the old album, but first we must NULL-ify the reused albums
    if(oldAlbum)
        for(i=0;i<oldN;i++)
            if(oldAlbum[i].flags&FlagIsAlbum && oldAlbum[i].album){
                //try to find this entry/album in the new album (where it was transfered)
                //we should find it
                uint32_t oldNameCRC=oldAlbum[i].nameCRC&0xFFFFFFFC;
                char *oldName=getNameFromAlbum(oldAlbum,i);
                for(j=0;j<parentMdAlbumEntry->entriesIndex;j++)
                    if(oldNameCRC==(parentMdAlbumEntry->album[j].nameCRC&0xFFFFFFFC) && !strcmp(oldName,getNameFromAlbum(parentMdAlbumEntry->album,j))){
                        oldAlbum[i].album=NULL;
                        break;
                    }
                LOG0c(j>=parentMdAlbumEntry->entriesIndex,"INFO: oldAlbum entry %s was not found in the new album.",oldName);
            }
    //now we can free the old album
    freeAlbums(&oldAlbum,&oldN);





    //now process folders/albums
    ph_assert(parentMdAlbumEntry->album,NULL);
    struct mdh_album_data_by_permission *albumDataByPermission=getAlbumDataByPermission(parentMdAlbumEntry);
    do{for(i=0;i<n;i++)
        if(parentMdAlbumEntry->album[i].flags&FlagIsAlbum && parentMdAlbumEntry->album[i].flags&FlagElmIsBeingScanned){
            struct md_album_entry *currentMdAlbumEntry=&parentMdAlbumEntry->album[i];
            struct mdx_album_entry *currentMdAlbumEntryX=getExtraEntry(parentMdAlbumEntry->album,parentMdAlbumEntry->entriesIndex,i);;
            char const * const d_name=getNameFromAlbum(parentMdAlbumEntry->album,i);
            int32_t oldEarliestCaptureTime=currentMdAlbumEntry->earliestCaptureTime;


            //add our dir name to picsDir and dataDir
            l=strlen(d_name);
            memcpy(ptrParsing->picsDir+picsDirLenSave,d_name,l+1);
            ptrParsing->picsDirLen=picsDirLenSave+l;
            memcpy(ptrParsing->dataDir+dataDirLenSave,d_name,l+1);
            ptrParsing->dataDirLen=dataDirLenSave+l;

            //remove the current albumDataByPermission from this album's current albumDataByPermission.
            //It will be re-added later, after scanning
            if(currentMdAlbumEntry->album){
                struct mdh_album_data_by_permission const *adbpEntry=getAlbumDataByPermission(currentMdAlbumEntry);
                ph_assert(adbpEntry,NULL);
                uint8_t permissions=currentMdAlbumEntry->permissions;

                releaseMdPics(&ptrParsing->mdPics);
                mdPicsW=getMdPicsWritable();

                for(j=0;j<ptrParsing->nrPermissions+1;j++){
                    if(j==0 || permissions&1){
                        //this entry is considered in the current permision class
                        struct mdh_album_data_by_permission * __restrict adbp=&(albumDataByPermission[j]);
                        ph_assert(currentMdAlbumEntry->flags&FlagIsAlbum,NULL);

                        struct mdh_album_data_by_permission const * const __restrict adbpE=&(adbpEntry[j]);
                        if(j){
                            //change "regular" entries
                            adbp->nrEntries-=adbpE->nrEntries;
                        } else {
                            //change "admin" entries
                            adbp->sizeOfUnknownsInAlbumHi-=adbpE->sizeOfUnknownsInAlbumHi;
                            adbp->sizeOfUnknownsInAlbumLo-=adbpE->sizeOfUnknownsInAlbumLo;
                            adbp->nrTotalUnknowns-=adbpE->nrTotalUnknowns;
                        }
                        //common entries
                        adbp->nrTotalPictrs-=adbpE->nrTotalPictrs;
                        adbp->nrTotalVideos-=adbpE->nrTotalVideos;
                        adbp->nrTotalAlbums-=adbpE->nrTotalAlbums;
                        //LOG0c(j==0,"nrTotalAlbums for current album increased to %d because we added albums for %s which is %d+1",adbp->nrTotalAlbums,t,adbpEntry[j].nrTotalAlbums);
                        adbp->sizeOfPictrsInAlbum-=adbpE->sizeOfPictrsInAlbum;
                        adbp->sizeOfVideosInAlbum-=adbpE->sizeOfVideosInAlbum;

                        LOG0c(j==0,"nrPics=%d, nrVids=%d, nrAlbums=%d. n=%d",adbp->nrTotalPictrs,adbp->nrTotalVideos,adbp->nrTotalAlbums,n);
                    }
                    permissions>>=1;
                }//for(j...

                releaseMdPicsW(&mdPicsW);
                ptrParsing->mdPics=getMdPicsRd();
            }


            //do the scanning
            r=scanPicsFolder(level+1,parentMdAlbumEntry->album,i,n,changedEntry,!fastScan);
            ptrParsing->currentAlbum.level=level;
            LOG0return(r==-1,-1,"Processing a subalbum failed. Forcing full rescan.");
            LOG0return(shouldExit,-1,"Photostovis exiting (aborting picture scanning).");

            //if we are here it does not matter the returned value, we are scanning this album anyway (this album is the parent of the album scanned above)
            ph_assert(currentMdAlbumEntry->album,"%s",ptrParsing->picsDir);
            ph_assert(!(currentMdAlbumEntry->flags&FlagElmIsBeingScanned),NULL);
            ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';

            //this is the computed offset
            if(currentMdAlbumEntry->flags&FlagHasNameOffset){
                ph_assert(currentMdAlbumEntryX->nameOffset>0,NULL);
            }else{
                ph_assert(currentMdAlbumEntryX->nameOffset==0,"album: <<%s>>, offset: %d",ptrParsing->picsDir,currentMdAlbumEntryX->nameOffset);
            }

            nrTotalUnknowns+=ptrParsing->nrTotalUnknowns;
            sizeOfUnknownsInAlbum+=ptrParsing->sizeOfUnknownsInAlbum;

            //check for pictures without date
            if(currentMdAlbumEntry->flags&FlagAlbumHasUndatedEntries)
                tempMdAlbumEntry.flags|=FlagAlbumHasUndatedEntries;
            if(currentMdAlbumEntry->flags&FlagHasExifThumbnail)
                tempMdAlbumEntry.flags|=FlagHasExifThumbnail;


            //set earliestCaptureTime and latest capture time
            if(tempMdAlbumEntry.earliestCaptureTime>currentMdAlbumEntry->earliestCaptureTime)
                tempMdAlbumEntry.earliestCaptureTime=currentMdAlbumEntry->earliestCaptureTime;
            if(currentMdAlbumEntry->latestCaptureTime!=DATE_MAX &&
                    (tempMdAlbumEntry.latestCaptureTime==DATE_MAX || tempMdAlbumEntry.latestCaptureTime<currentMdAlbumEntry->latestCaptureTime))
                tempMdAlbumEntry.latestCaptureTime=currentMdAlbumEntry->latestCaptureTime;

            releaseMdPics(&ptrParsing->mdPics);
            mdPicsW=getMdPicsWritable();

            //update the adbp data
            if(currentMdAlbumEntry->album){
                struct mdh_album_data_by_permission const *adbpEntry=getAlbumDataByPermission(currentMdAlbumEntry);
                ph_assert(adbpEntry,NULL);
                uint8_t permissions=currentMdAlbumEntry->permissions;
                for(j=0;j<ptrParsing->nrPermissions+1;j++){
                    if(j==0 || permissions&1){
                        //this entry is considered in the current permision class
                        struct mdh_album_data_by_permission * __restrict adbp=&(albumDataByPermission[j]);
                        ph_assert(currentMdAlbumEntry->flags&FlagIsAlbum,NULL);

                        struct mdh_album_data_by_permission const * const __restrict adbpE=&(adbpEntry[j]);
                        if(j){
                            //change "regular" entries
                            adbp->nrEntries+=adbpE->nrEntries;
                        } else {
                            //change "admin" entries
                            adbp->sizeOfUnknownsInAlbumHi+=adbpE->sizeOfUnknownsInAlbumHi;
                            adbp->sizeOfUnknownsInAlbumLo+=adbpE->sizeOfUnknownsInAlbumLo;
                            adbp->nrTotalUnknowns+=adbpE->nrTotalUnknowns;
                        }
                        //common entries
                        adbp->nrTotalPictrs+=adbpE->nrTotalPictrs;
                        adbp->nrTotalVideos+=adbpE->nrTotalVideos;
                        adbp->nrTotalAlbums+=adbpE->nrTotalAlbums;
                        //LOG0c(j==0,"nrTotalAlbums for current album increased to %d because we added albums for %s which is %d+1",adbp->nrTotalAlbums,t,adbpEntry[j].nrTotalAlbums);
                        adbp->sizeOfPictrsInAlbum+=adbpE->sizeOfPictrsInAlbum;
                        adbp->sizeOfVideosInAlbum+=adbpE->sizeOfVideosInAlbum;
                        LOG0c(j==0,"nrPics=%d, nrVids=%d, nrAlbums=%d. n=%d",adbp->nrTotalPictrs,adbp->nrTotalVideos,adbp->nrTotalAlbums,n);
                    }
                    permissions>>=1;
                }//for(j...
            }

            //rearange (sort) this last element, but only if its earliest date is different
            if(oldEarliestCaptureTime!=currentMdAlbumEntry->earliestCaptureTime){
#ifndef NDEBUG
                //if(level==0){
                    LOG0(NULL);
                    LOG0("DEBUG: >>>>> Before rearranging %d/%d (%s), new time: %d (old time: %d)",
                         i+1,n,getNameFromAlbum(parentMdAlbumEntry->album,i),currentMdAlbumEntry->earliestCaptureTime,oldEarliestCaptureTime);
                    //verify the sort
                    int32_t previousTime=-DATE_MAX;
                    LOG0("DEBUG: >>>>> Before the sort:");
                    for(j=0;j<n;j++)
                        if(parentMdAlbumEntry->album[j].captureTime!=DATE_MAX){
                            LOG0("DEBUG: >>>>> 1Verifying  With  Date  (%2d) %10d/%s against previous valid time (%d)",
                                 j+1,parentMdAlbumEntry->album[j].captureTime,getNameFromAlbum(parentMdAlbumEntry->album,j),previousTime);
                            previousTime=parentMdAlbumEntry->album[j].captureTime;
                        } else
                            LOG0("DEBUG: >>>>> 1Verifying Without Date (%2d) %10X/%s against previous valid time (%d)",
                                 j+1,parentMdAlbumEntry->album[j].captureTime,getNameFromAlbum(parentMdAlbumEntry->album,j),previousTime);
                //}
#endif

                //we need to rearange (sort) this element, and also its X counterpart
                int32_t currentEarliestCaptureTime=currentMdAlbumEntry->earliestCaptureTime;
                char *currentName=getNameFromAlbum(parentMdAlbumEntry->album,i);

                for(j=0;j<n;j++){
                    if(i==j)continue;
                    if(currentEarliestCaptureTime==DATE_MAX){
                        //we only sort by name
                        char *jName=getNameFromAlbum(parentMdAlbumEntry->album,j);
                        if(strcasecmp(currentName,jName)<0){
                            //we insert it before j
                            swapElementBefore(parentMdAlbumEntry->album,i,j,n);
                            break;//done
                        }
                    } else {
                        //we have a date and things are a bit more complicated.
                        if(parentMdAlbumEntry->album[j].captureTime==DATE_MAX){
                            //we need to find the next element with a date, and then decide if the current element falls within this interval
                            for(k=j+1;k<n;k++)
                                if(parentMdAlbumEntry->album[k].captureTime!=DATE_MAX)
                                    break;
                            if(k<n && currentEarliestCaptureTime>=parentMdAlbumEntry->album[k].captureTime){
                                //LOG0("Element falls outside this interval [%d-%d]",j+1,k-1+1);
                                continue;
                            }

                            //if here, the element is inside the interval
                            //LOG0("Element inside the interval [%d-%d]",j+1,k-1+1);

                            //if we are here it means we have to sort by name in the interval j..k-1 (inclusive)
                            for(l=j;l<k;l++){
                                char *lName=getNameFromAlbum(parentMdAlbumEntry->album,l);
                                if(strcasecmp(currentName,lName)<0){
                                    //we insert it before l
                                    //LOG0("Inserting1 element at position %d",l+1);
                                    swapElementBefore(parentMdAlbumEntry->album,i,l,n);
                                    break;//done
                                }
                            }
                            if(l==k){
                                //insert at position k
                                //LOG0("Inserting2 element after position %d",k-1);
                                swapElementAfter(parentMdAlbumEntry->album,i,k-1,n);
                            }
                            break; //from the j loop
                        } else {
                            //we just sort by date
                            if(currentEarliestCaptureTime<parentMdAlbumEntry->album[j].captureTime){
                                //LOG0("Inserting3 element at position %d (i=%d). Times: %d<%d",j+1,i+1,currentEarliestCaptureTime,parentMdAlbumEntry->album[j].captureTime);
                                swapElementBefore(parentMdAlbumEntry->album,i,j,n);
                                break;//done
                            }
                        }//sorting by date
                    }
                }//for(j=0;..
                if(j==n && i!=n-1)//we need to have the current element as the last element
                    swapElementAfter(parentMdAlbumEntry->album,i,n-1,n);
            }//if(oldEarliestCaptureTime!=currentMdAlbumEntry->earliestCaptureTime)
#ifndef NDEBUG
            //if(level==0){
                LOG0("DEBUG: >>>>> Rearange done.");
                //verify the sort
                LOG0("DEBUG: >>>>> After the sort:");
                int32_t previousTime=-DATE_MAX;

                for(j=0;j<n;j++)
                    if(parentMdAlbumEntry->album[j].captureTime!=DATE_MAX){
                        ph_assert(parentMdAlbumEntry->album[j].captureTime>=previousTime,"%d<=%d",parentMdAlbumEntry->album[j].captureTime,previousTime);
                        LOG0("DEBUG: >>>>> 2Verifying  With  Date  (%2d) %10d/%s against previous valid time (%d)",
                             j+1,parentMdAlbumEntry->album[j].captureTime,getNameFromAlbum(parentMdAlbumEntry->album,j),previousTime);
                        previousTime=parentMdAlbumEntry->album[j].captureTime;
                    } else
                        LOG0("DEBUG: >>>>> 2Verifying Without Date (%2d) %10X/%s against previous valid time (%d)",
                             j+1,parentMdAlbumEntry->album[j].captureTime,getNameFromAlbum(parentMdAlbumEntry->album,j),previousTime);
            //}
#endif

            //change the modification time
            uint16_t oldCounter=parentMdAlbumEntryX->modificationCounter;
            propagateModification(mdPicsW);
            ph_assert(parentMdAlbumEntryX->modificationCounter==oldCounter+1,NULL);
            releaseMdPicsW(&mdPicsW);
            //signal that the album has changed
            sendStatusToConnectedClients(&ptrParsing->currentAlbum);
            ptrParsing->mdPics=getMdPicsRd();

            //we need to break the loop and look for the next unscanned album
            break;
        }
    } while(i<n);
    //end of the "for" loop


    //verify the sort
    /* no point in verifying because it DOES NOT take into account the timezones
    int32_t earlierTime=DATE_MAX;
    for(i=0;i<n;i++)
        if(parentMdAlbumEntry->album[i].captureTime!=DATE_MAX){
            //LOG0("VerifyingX (%2d) %d/%s against earlier time (%d)",i+1,parentMdAlbumEntry->album[i].captureTime,getNameFromAlbum(parentMdAlbumEntry->album,i),earlierTime);
            if(earlierTime==DATE_MAX)earlierTime=parentMdAlbumEntry->album[i].captureTime;
            else {
                ph_assert(earlierTime<=parentMdAlbumEntry->album[i].captureTime,NULL);
                earlierTime=parentMdAlbumEntry->album[i].captureTime;
            }
        } else if(i>0){
            char *nameI=getNameFromAlbum(parentMdAlbumEntry->album,i);
            char *nameIprev=getNameFromAlbum(parentMdAlbumEntry->album,i-1);
            //LOG0("VerifyingY (%2d) %X/%s against earlier name (%s)",i+1,parentMdAlbumEntry->album[i].captureTime,nameI,nameIprev);
            ph_assert(strcasecmp(nameIprev,nameI)<0,NULL);

        }
        */

    ptrParsing->picsDirLen=picsDirLenSave;
    ptrParsing->dataDirLen=dataDirLenSave;
    ptrParsing->picsDir[picsDirLenSave]='\0';
    ptrParsing->dataDir[dataDirLenSave]='\0';

    //setup ptrParsing->nrTotalUnknowns and sizeOfUnknownsInAlbum
    ptrParsing->nrTotalUnknowns=nrTotalUnknowns;
    ptrParsing->sizeOfUnknownsInAlbum=sizeOfUnknownsInAlbum;

    //check if we have a date in the name that we could use
    if(tempMdAlbumEntry.earliestCaptureTime==DATE_MAX){
        LOG0("NO DATE for this album, but perhaps we can get one from the name!");
        tempMdAlbumEntry.earliestCaptureTime=getDateTimeFromString(parentMdAlbumName,0,0);
        if(tempMdAlbumEntry.earliestCaptureTime!=DATE_MAX && tempMdAlbumEntry.latestCaptureTime==DATE_MAX)
            tempMdAlbumEntry.latestCaptureTime=tempMdAlbumEntry.earliestCaptureTime;
    }

    //compute the memory sizes (for saving data into the file)
    uint32_t strAlbumPos=sizeof(struct md_album_entry)*n+sizeof(struct mdx_album_entry*)+4+16+
            sizeof(struct mdh_album_data_by_permission)*(ptrParsing->nrPermissions+1); //16 comes from the md5 sum, which will be stored right after the entries
    const uint32_t albumDataLen=strAlbumPos+nameStringsLen;
    uint32_t strAlbumXPos=sizeof(struct mdx_album_entry)*n;
    const uint32_t albumExtraLen=strAlbumXPos+descriptionStringsLen;


    //at this point the album data is almost fully computed.The only thing to do is to compute its md5
    l=computeAlbumMD5(parentMdAlbumEntry->album,n,albumDataLen,ptrParsing->nrPermissions,0);
    ph_assert(!l,NULL); //the only possible return value is zero

    /*****************************
     *  Writing "album.dat" file
     *****************************/

    memcpy(ptrParsing->dataDir+ptrParsing->dataDirLen,KAlbumFilename,strlen(KAlbumFilename)+1);
    FILE *f=fopen(ptrParsing->dataDir,"w");
    setOwnership(ptrParsing->dataDir,0);
    ph_assert(f,"ERROR: Opening file %s for writing failed with error: %s",ptrParsing->dataDir,strerror(errno));
    ptrParsing->dataDir[ptrParsing->dataDirLen]='\0'; //restore dataDir
    //TODO: handle f==NULL

    /* File structure:
     * Header:
     * struct md_albumdat_header {
            uint8_t fileVersion;
            uint8_t nrPermissions;
            uint16_t nrEntries;
            uint32_t lengthOfAlbumData; //album data starts immediately after this structure
            uint32_t lengthOfExtraData;
        };
     * album data (album)
     * extra data (albumX)
     *  ...
    */

    //varaibles for writing into the file
    struct md_albumdat_header albumDatHeader={
        CURRENT_ALBUMDAT_FILE_VERSION,ptrParsing->nrPermissions,n,albumDataLen,albumExtraLen
    };


    //write into the file the header
    fwrite(&albumDatHeader,sizeof(struct md_albumdat_header),1,f);
    fwrite(parentMdAlbumEntry->album,albumDataLen,1,f);
    fwrite(*(struct mdx_album_entry**)(parentMdAlbumEntry->album+n),albumExtraLen,1,f);
    fclose(f);//done writing
    tempMdAlbumEntry.modificationTime=time(NULL);
    LOG0("Wrote album %s. albumDataLen=%d, albumExtraLen=%d",ptrParsing->picsDir,albumDataLen,albumExtraLen);

#if 0
    //do we have albumname-based date and not picture-based? If yes, change albumDataByPermission[].earliestCaptureTime
    if(thisMdAlbumEntry->earliestCaptureTime==DATE_MAX && parentMdAlbumEntry->earliestCaptureTime!=DATE_MAX){
        //is this possible at all???
        LOG0("thisMdAlbumEntry.earliestCaptureTime=%x, parent.earliestCaptureTime=%x",thisMdAlbumEntry->earliestCaptureTime,parentMdAlbumEntry->earliestCaptureTime);
        ph_assert(0,NULL);
        /*
        thisMdAlbumEntry.earliestCaptureTime=parentMdAlbumEntry->earliestCaptureTime;
        for(j=1;j<ptrParsing->nrPermissions+1;j++)
            albumDataByPermission[j].earliestCaptureTime=parentMdAlbumEntry->earliestCaptureTime;
            */
    }
#endif

    ph_assert(tempMdAlbumEntry.permissions==parentMdAlbumEntry->permissions,NULL);
    ph_assert(!tempMdAlbumEntry.album,NULL);

    //check the number of pictures
    int debugCountedPicsAndVids=0;
    for(i=0;i<n;i++){
        if(!(parentMdAlbumEntry->album[i].flags&FlagIsAlbum))
            debugCountedPicsAndVids++;
        //else LOG0("%d/%d is album",i+1,n);
    }
    ph_assert(debugCountedPicsAndVids==debugNrPicturesCamera,"%d!=%d total: %d (%s)",debugCountedPicsAndVids,debugNrPicturesCamera,n,ptrParsing->picsDir);

    //check timings
    ph_assert((tempMdAlbumEntry.earliestCaptureTime==DATE_MAX && tempMdAlbumEntry.latestCaptureTime==DATE_MAX) ||
              (tempMdAlbumEntry.earliestCaptureTime!=DATE_MAX && tempMdAlbumEntry.latestCaptureTime!=DATE_MAX),
              "%x %x",tempMdAlbumEntry.earliestCaptureTime,tempMdAlbumEntry.latestCaptureTime);

    //changing the parentMdAlbum requires write lock to mdPics
    ph_assert(!tempMdAlbumEntry.album,NULL);
    releaseMdPics(&ptrParsing->mdPics);
    mdPicsW=getMdPicsWritable();
    tempMdAlbumEntry.album=parentMdAlbumEntry->album; //so we can use memcpy
    memcpy(parentMdAlbumEntry,&tempMdAlbumEntry,sizeof(struct md_album_entry));
    memcpy(parentMdAlbumEntryX,&tempMdAlbumEntryX,sizeof(struct mdx_album_entry));
    tempMdAlbumEntry.album=NULL;

    //we will return from this function, so mark our album that it is not being scaned anymore
    parentMdAlbumEntry->flags&=~FlagElmIsBeingScanned;
    mdPicsW->scannedEntries+=parentMdAlbumEntry->entriesIndex;
    oldCounter=parentMdAlbumEntryX->modificationCounter;
    propagateModification(mdPicsW);
    ph_assert(parentMdAlbumEntryX->modificationCounter==oldCounter+1,NULL);

#ifdef DEBUG_PICS_FOLDER
    if(!statusFlagsCheck(StatusFlagDoARescan)){
        //find the current folder
        for(i=0;i<dpsNr;i++)
            if(dps[i].name && !strcmp(dps[i].name,ptrParsing->picsDir)){//found it
                ph_assert(parentMdAlbumEntry->entriesIndex==dps[i].count,
                          "Mismatch %d vs %d for %s",parentMdAlbumEntry->entriesIndex,dps[i].count,dps[i].name);
                dps[i].count=-dps[i].count;
                break;
            }
        ph_assert(!parentMdAlbumEntry->entriesIndex || i<dpsNr,"Not found: %s, level: %d",ptrParsing->picsDir,level);
    }
#endif

    releaseMdPicsW(&mdPicsW);
    sendStatusToConnectedClients(&ptrParsing->currentAlbum);
    ptrParsing->mdPics=getMdPicsRd();

    //one more thing to do: delete those folders in our dataDir that have their modification time before startTime (updated in the beginning of this function)
    deleteUnusedFolders(startTime);
    LOG0("scanPicsFolder-- (level %d), album: %s returning 1 (because it was parsed)",level,ptrParsing->picsDir);

    LOG_FLUSH;
    return 1;
}//scanPicsFolder


int lookForPictures(int *forcePicturesFullRescan, struct md_entry_id *changedFolderId){
    struct timeval ts,te;
    long int tdiff;
    int i,r;

#ifndef NDEBUG
    LOG0("DEBUG: sizeof(struct mdh_album_entry)=%zu sizeof(struct md_album_entry)=%zu",sizeof(struct mdh_album_entry),sizeof(struct md_album_entry));
    ph_assert(sizeof(void*)==4 || sizeof(void*)==8,NULL);
    ph_assert(sizeof(struct mdh_album_entry)==20,NULL);

    ph_assert(sizeof(struct md_album_entry)==32,"sizeof(struct md_album_entry) is not 32 but %zu",sizeof(struct md_album_entry));

    ph_assert(sizeof(struct mdh_album_data_by_permission)==40,
              "sizeof(struct mdh_album_data_by_permission) is not 40 but %zu",sizeof(struct mdh_album_data_by_permission));
#endif

    //create and allocate the parsing structure
    ph_assert(!ptrParsing,"ptrParsing not NULL");
    ptrParsing=(struct mdh_parsing*)ph_malloc(sizeof(struct mdh_parsing));
    memset(ptrParsing,0,sizeof(struct mdh_parsing));
    ptrParsing->tempBufSize=2*TEMP_BUF_MIN_SIZE;
    ptrParsing->tempBuf=(char*)ph_malloc(ptrParsing->tempBufSize);
    //allocated data
    ptrParsing->nrAllocatedChunks=1;
    ptrParsing->forcePicturesFullScan=*forcePicturesFullRescan; //basically, if we cannot find the cameras file or we cannot read it, we have to force a complete picture scan
    LOG0("forcePicturesFullRescan=%d",ptrParsing->forcePicturesFullScan);
    //compile the date parsing structure
    r=regcomp(&ptrParsing->rxd,KDatePattern,REG_EXTENDED);
    ph_assert(!r,NULL);
    //testing - these should match
    r=regexec(&ptrParsing->rxd,"2013-04-17 Cucu",1,&ptrParsing->rxdm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxdm.rm_so==0 && ptrParsing->rxdm.rm_eo==11,NULL);
    r=regexec(&ptrParsing->rxd,"2014-04-20-1 Cucu",1,&ptrParsing->rxdm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxdm.rm_so==0 && ptrParsing->rxdm.rm_eo==13,NULL);
    r=regexec(&ptrParsing->rxd,"2013-04-17-3 Cucu",1,&ptrParsing->rxdm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxdm.rm_so==0 && ptrParsing->rxdm.rm_eo==13,NULL);
    r=regexec(&ptrParsing->rxd,"20130417 Cucu",1,&ptrParsing->rxdm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxdm.rm_so==0 && ptrParsing->rxdm.rm_eo==9,NULL);
    r=regexec(&ptrParsing->rxd,"2013 04 17-Cucu",1,&ptrParsing->rxdm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxdm.rm_so==0 && ptrParsing->rxdm.rm_eo==11,NULL);
    r=regexec(&ptrParsing->rxd,"04-17 Cucu",1,&ptrParsing->rxdm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxdm.rm_so==0 && ptrParsing->rxdm.rm_eo==6,NULL);
    r=regexec(&ptrParsing->rxd,"0417 Cucu",1,&ptrParsing->rxdm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxdm.rm_so==0 && ptrParsing->rxdm.rm_eo==5,NULL);
    r=regexec(&ptrParsing->rxd,"04 17-Cucu",1,&ptrParsing->rxdm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxdm.rm_so==0 && ptrParsing->rxdm.rm_eo==6,NULL);
    r=regexec(&ptrParsing->rxd,"2014-04-20-1 De Paste",1,&ptrParsing->rxdm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxdm.rm_so==0 && ptrParsing->rxdm.rm_eo==13,NULL);
    r=regexec(&ptrParsing->rxd,"VID_20150420_",1,&ptrParsing->rxdm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxdm.rm_so==0 && ptrParsing->rxdm.rm_eo==13,NULL);
    //testing - these should NOT match
    r=regexec(&ptrParsing->rxd,"3113-04-17 cucu",0,NULL,0);
    ph_assert(r==REG_NOMATCH,NULL);
    r=regexec(&ptrParsing->rxd,"2013-14-17",0,NULL,0);
    ph_assert(r==REG_NOMATCH,NULL);
    r=regexec(&ptrParsing->rxd,"2013-04-37",0,NULL,0);
    ph_assert(r==REG_NOMATCH,NULL);
    r=regexec(&ptrParsing->rxd,"-04-17 cucu",0,NULL,0);
    ph_assert(r==REG_NOMATCH,NULL);
    r=regexec(&ptrParsing->rxd,"14-17 cucu",0,NULL,0);
    ph_assert(r==REG_NOMATCH,NULL);
    r=regexec(&ptrParsing->rxd,"04-37 cucu",0,NULL,0);
    ph_assert(r==REG_NOMATCH,NULL);

    //compile the time parsing structure
    r=regcomp(&ptrParsing->rxt,KTimePattern,REG_EXTENDED);
    ph_assert(!r,NULL);
    //testing - these should match
    r=regexec(&ptrParsing->rxt,"08-20-17",1,&ptrParsing->rxtm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxtm.rm_so==0 && ptrParsing->rxtm.rm_eo==8,NULL);
    r=regexec(&ptrParsing->rxt,"082017",1,&ptrParsing->rxtm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxtm.rm_so==0 && ptrParsing->rxtm.rm_eo==6,NULL);
    r=regexec(&ptrParsing->rxt,"10 59 00",1,&ptrParsing->rxtm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxtm.rm_so==0 && ptrParsing->rxtm.rm_eo==8,NULL);
    r=regexec(&ptrParsing->rxt,"23.59.59",1,&ptrParsing->rxtm,0);
    ph_assert(!r,NULL);
    ph_assert(ptrParsing->rxtm.rm_so==0 && ptrParsing->rxtm.rm_eo==8,NULL);
    //testing - these should NOT match
    r=regexec(&ptrParsing->rxt,"24-00-00",0,NULL,0);
    ph_assert(r==REG_NOMATCH,NULL);
    r=regexec(&ptrParsing->rxt,"23-60-00",0,NULL,0);
    ph_assert(r==REG_NOMATCH,NULL);
    r=regexec(&ptrParsing->rxt,"22-00-72",0,NULL,0);
    ph_assert(r==REG_NOMATCH,NULL);
    r=regexec(&ptrParsing->rxt,"334455",0,NULL,0);
    ph_assert(r==REG_NOMATCH,NULL);
    r=regexec(&ptrParsing->rxt,"234-34-44",0,NULL,0);
    ph_assert(r==REG_NOMATCH,NULL);
    r=regexec(&ptrParsing->rxt,"12-66-77",0,NULL,0);
    ph_assert(r==REG_NOMATCH,NULL);

    //prepare picsDir and dataDir
    struct ps_md_config const * const mdConfig=getMdConfigRd();
    int l=strlen(mdConfig->rootFolder);
    memcpy(ptrParsing->picsDir,mdConfig->rootFolder,l);
    memcpy(ptrParsing->dataDir,mdConfig->rootFolder,l);
    r=strlen(KPicturesFolder);
    memcpy(ptrParsing->picsDir+l,KPicturesFolder,r+1);
    ptrParsing->picsDirLen=l+r;
    r=strlen(KPhotostovisData);
    memcpy(ptrParsing->dataDir+l,KPhotostovisData,r+1);
    ptrParsing->dataDirLen=l+r;

    
    LOG0("Counting pictures in folder %s...",mdConfig->rootFolder);
    int totalEntries=0,tryLoadingCamerasFromFile=0;
    countPicsFolder(0,mdConfig->inotifyFd,&totalEntries);
    const uint32_t strAlbumPos=sizeof(struct md_album_entry)+sizeof(struct mdx_album_entry*)+4+16+
            sizeof(struct mdh_album_data_by_permission)*(mdConfig->nrViewingPermissions+1);

    releaseMdConfig(&mdConfig);


    struct ps_md_pics *mdPicsW=getMdPicsWritable();
    ph_assert(mdPicsW->rootAlbum->dataOffsetLo==strAlbumPos,NULL);
    ph_assert(mdPicsW->rootAlbum->dataOffsetHi==0,NULL);
    mdPicsW->totalEntries=totalEntries;
    mdPicsW->scannedEntries=0;
    if(mdPicsW->nrCameras==0 && totalEntries>0)
        tryLoadingCamerasFromFile=1;
    releaseMdPicsW(&mdPicsW);

    LOG0("Counted pictures & videos: %d",totalEntries);
    //end of counting the pictures

    if(statusFlagsCheck(StatusFlagFirstScanDone)){
        if(!changedFolderId){
            //we sleep one second to allow incoming inotifies to hit us and request a picture scan
            sleep(1);
        }
        //check if we have rescan requests
        LOG0close_return(statusFlagsCheck(StatusFlagDoARescan),1,
                         *forcePicturesFullRescan=ptrParsing->forcePicturesFullScan;regfree(&ptrParsing->rxd);regfree(&ptrParsing->rxt);
                         free(ptrParsing->tempBuf);free(ptrParsing);ptrParsing=NULL,
                         "Changes happened while counting the pictures. We restart the picture scanning");
    }

    //scanning pictures
    gettimeofday(&ts,NULL);

    if(tryLoadingCamerasFromFile){
        //load camera models from disk
        //first, save the current length of the dataDir
        l=ptrParsing->dataDirLen;
        if(ptrParsing->dataDir[l-1]!='/')
            ptrParsing->dataDir[l++]='/';
        memcpy(ptrParsing->dataDir+l,KCamerasFilename,strlen(KCamerasFilename)+1);
        FILE *f=fopen(ptrParsing->dataDir,"r");
        ptrParsing->dataDir[ptrParsing->dataDirLen]='\0';//reset the dataDir string
        mdPicsW=getMdPicsWritable();
        if(f){
            struct md_cameras_header camerasFileHeader;
            r=fread(&camerasFileHeader,sizeof(struct md_albumdat_header),1,f);
            if(r!=1)ptrParsing->forcePicturesFullScan=1;

            if(!ptrParsing->forcePicturesFullScan)
                LOG0c_do(camerasFileHeader.fileVersion!=CURRENT_CAMERAS_FILE_VERSION,ptrParsing->forcePicturesFullScan=1,
                         "WARNING: cameras file version is different, this will force a complete rescan.");

            if(!ptrParsing->forcePicturesFullScan){
                mdPicsW->nrCameras=camerasFileHeader.nrCameras;
                mdPicsW->fullMakeModelStringsLen=camerasFileHeader.fullMakeModelStringsLen;
                mdPicsW->shortenedMakeModelStringsLen=camerasFileHeader.shortenedMakeModelStringsLen;
                LOG0("Found %u cameras (full string len: %u, shortened string len: %u) in %s file.",
                     (unsigned int)mdPicsW->nrCameras,mdPicsW->fullMakeModelStringsLen,mdPicsW->shortenedMakeModelStringsLen,KCamerasFilename);
                //allocate space and read the data from the file
                if(mdPicsW->nrCameras){
                    mdPicsW->cameras=(struct ps_camera *)ph_malloc(mdPicsW->nrCameras*sizeof(struct ps_camera));
                    r=fread(mdPicsW->cameras,mdPicsW->nrCameras*sizeof(struct ps_camera),1,f);
                    LOG0c_do(r!=1,ptrParsing->forcePicturesFullScan=2,"WARNING: could not read cameras from file.");
                }
                if(mdPicsW->fullMakeModelStringsLen>0){
                    mdPicsW->fullMakeModelStrings=(char*)ph_malloc(mdPicsW->fullMakeModelStringsLen+1);
                    r=fread(mdPicsW->fullMakeModelStrings,mdPicsW->fullMakeModelStringsLen,1,f);
                    LOG0c_do(r!=1,ptrParsing->forcePicturesFullScan=2,"WARNING: could not read camera models full string from file.");
                    mdPicsW->fullMakeModelStrings[mdPicsW->fullMakeModelStringsLen]='\0';
                }
                if(mdPicsW->shortenedMakeModelStringsLen>0){
                    mdPicsW->shortenedMakeModelStrings=(char*)ph_malloc(mdPicsW->shortenedMakeModelStringsLen+1);
                    r=fread(mdPicsW->shortenedMakeModelStrings,mdPicsW->shortenedMakeModelStringsLen,1,f);
                    LOG0c_do(r!=1,ptrParsing->forcePicturesFullScan=2,"WARNING: could not read camera models shortened string from file.");
                    mdPicsW->shortenedMakeModelStrings[mdPicsW->shortenedMakeModelStringsLen]='\0';
                }
            }            
            fclose(f);
        } else ptrParsing->forcePicturesFullScan=1;
    }//if(tryLoadingCamerasFromFile)
    else mdPicsW=getMdPicsWritable();

    if(ptrParsing->forcePicturesFullScan==2){
        //we need to free and reset stuff
        free(mdPicsW->cameras);
        mdPicsW->cameras=NULL;
        mdPicsW->nrCameras=0;
        free(mdPicsW->fullMakeModelStrings);
        mdPicsW->fullMakeModelStrings=NULL;
        mdPicsW->fullMakeModelStringsLen=0;
        free(mdPicsW->shortenedMakeModelStrings);
        mdPicsW->shortenedMakeModelStrings=NULL;
        mdPicsW->shortenedMakeModelStringsLen=0;
    } else //reset the number of pictures for each camera
        for(i=0;i<mdPicsW->nrCameras;i++)
            mdPicsW->cameras[i].nrPictures=0;
    if(ptrParsing->forcePicturesFullScan){
        //freeing albums
        freeAlbums(&mdPicsW->rootAlbum->album,&mdPicsW->rootAlbum->entriesIndex);
    }

    printAlbums(mdPicsW->rootAlbum,1,0);
    //mark all elements as "being scanned"
    mdPicsW->rootAlbum->flags|=FlagElmIsBeingScanned;
    markAsBeingScanned(mdPicsW->rootAlbum,1,0);
    releaseMdPicsW(&mdPicsW);
    ph_assert(!ptrParsing->mdPics,NULL);

    LOG0("Scanning pictures in folder (%s)...",ptrParsing->picsDir);
    ptrParsing->mdPics=getMdPicsRd();
    r=scanPicsFolder(0,ptrParsing->mdPics->rootAlbum,0,1,changedFolderId,changedFolderId?1:0);

    ph_assert(ptrParsing->mdPics->rootAlbum->dataOffsetLo==strAlbumPos,NULL);
    ph_assert(!ptrParsing->mdPics->rootAlbum->dataOffsetHi,NULL);

    releaseMdPics(&ptrParsing->mdPics);
    //r==-1 handled below


    //free part of the data
    regfree(&ptrParsing->rxd);
    regfree(&ptrParsing->rxt);
    free(ptrParsing->tempBuf);ptrParsing->tempBuf=NULL;
    //check for error returned by scanPicsFolder
    LOG0close_return(shouldExit,1,
                     mdPicsW=getMdPicsWritable();freeAlbums(&mdPicsW->rootAlbum->album,&mdPicsW->rootAlbum->entriesIndex);releaseMdPicsW(&mdPicsW);free(ptrParsing);ptrParsing=NULL;freeDpsData(),
            "Photostovis exiting (scanning pictures aborted).")
    LOG0close_return(r<0,1,*forcePicturesFullRescan=ptrParsing->forcePicturesFullScan;free(ptrParsing);ptrParsing=NULL;freeDpsData(),
                     "WARNING: scanning folders failed (probably changes happened during picture scanning). Will rescan.");

    //some statistics
    gettimeofday(&te,NULL);
    tdiff=(te.tv_sec-ts.tv_sec)*1000+(te.tv_usec-ts.tv_usec)/1000;

    ptrParsing->mdPics=getMdPicsRd();
    struct mdh_album_data_by_permission * __restrict adbp=getAlbumDataByPermission(ptrParsing->mdPics->rootAlbum);
    LOG0("Done looking for pictures. We found %u pictures and %u videos in %u albums, in total %uMB of pics and %uMB of videos. "
         "Parsing took %ldms",
         adbp[0].nrTotalPictrs,adbp[0].nrTotalVideos,adbp[0].nrTotalAlbums,
         (unsigned)(adbp[0].sizeOfPictrsInAlbum>>20),(unsigned)(adbp[0].sizeOfVideosInAlbum>>20),tdiff);
    if(ptrParsing->nrTotalUnknowns>0)
        LOG0("We also found %u unknown items totalling %uMB.",ptrParsing->nrTotalUnknowns,(unsigned)(ptrParsing->sizeOfUnknownsInAlbum>>20));

    LOG0("We have allocated %d album structures totalling %dKB. On average, we are using %5.2f bytes/element (picture or video).",
         ptrParsing->nrAllocatedChunks,(ptrParsing->sizeOfAllocatedChunks>>10)+1,
         ptrParsing->sizeOfAllocatedChunks/(double)(adbp->nrTotalPictrs+adbp->nrTotalVideos));
    LOG0("Data cached in files totals %ukB.",(unsigned int)(ptrParsing->sizeOfCachedData>>10)+1);

    int nrTotalAlbums=adbp[0].nrTotalAlbums+1;
    int nrCameraPictures=0;
    LOG0("Parsed albums: %d/%d (%d%%), loaded albums: %d/%d (%d%%), reused albums: %d/%d (%d%%)",
         ptrParsing->nrParsedAlbums,nrTotalAlbums,ptrParsing->nrParsedAlbums*100/nrTotalAlbums,
            ptrParsing->nrLoadedAlbums,nrTotalAlbums,ptrParsing->nrLoadedAlbums*100/nrTotalAlbums,
            ptrParsing->nrReusedAlbums,nrTotalAlbums,ptrParsing->nrReusedAlbums*100/nrTotalAlbums);

    if(ptrParsing->mdPics->nrCameras>0){
        //save cameras to file. We do it here, before shortening the names, because otherwise it will be no match when we load it from file
        l=ptrParsing->dataDirLen;
        if(ptrParsing->dataDir[l-1]!='/')
            ptrParsing->dataDir[l++]='/';
        memcpy(ptrParsing->dataDir+l,KCamerasFilename,strlen(KCamerasFilename)+1);
        LOG0("Dumping camera info to file (%s)",ptrParsing->dataDir);
        FILE *f=fopen(ptrParsing->dataDir,"w");
        setOwnership(ptrParsing->dataDir,0);
        ptrParsing->dataDir[ptrParsing->dataDirLen]='\0';//reset the dataDir string
        if(f){
            struct md_cameras_header camerasFileHeader;
            camerasFileHeader.fileVersion=CURRENT_CAMERAS_FILE_VERSION;
            camerasFileHeader.nrCameras=ptrParsing->mdPics->nrCameras;
            camerasFileHeader.fullMakeModelStringsLen=ptrParsing->mdPics->fullMakeModelStringsLen;
            camerasFileHeader.shortenedMakeModelStringsLen=ptrParsing->mdPics->shortenedMakeModelStringsLen;

            r=fwrite(&camerasFileHeader,sizeof(struct md_cameras_header),1,f);
            r=fwrite(ptrParsing->mdPics->cameras,ptrParsing->mdPics->nrCameras*sizeof(struct ps_camera),1,f);
            r=fwrite(ptrParsing->mdPics->fullMakeModelStrings,ptrParsing->mdPics->fullMakeModelStringsLen,1,f);
            r=fwrite(ptrParsing->mdPics->shortenedMakeModelStrings,ptrParsing->mdPics->shortenedMakeModelStringsLen,1,f);
            fclose(f);
        }

        //print cameras
        LOG0("%d cameras used. Full names string length: %d. Shortened names string length: %d. Cameras:",
             ptrParsing->mdPics->nrCameras,ptrParsing->mdPics->fullMakeModelStringsLen,ptrParsing->mdPics->shortenedMakeModelStringsLen);
        for(i=0;i<ptrParsing->mdPics->nrCameras;i++){
            struct ps_camera * __restrict c=&(ptrParsing->mdPics->cameras[i]);
            char * __restrict cameraName=ptrParsing->mdPics->fullMakeModelStrings+c->fullMakeModelOffset;
            nrCameraPictures+=c->nrPictures;
            if(c->shortenedMakeModelOffset!=0xFFFF){
                //we have a shortened name
                char * __restrict shortCameraName=ptrParsing->mdPics->shortenedMakeModelStrings+c->shortenedMakeModelOffset;
                LOG0("%2d. [%4d pictures, min focal length: %5.2f]: %s (%s)",i+1,c->nrPictures,c->minFocalLength,shortCameraName,cameraName);
            } else {
                //we do NOT have a shortened name
                LOG0("%2d. [%4d pictures, min focal length: %5.2f]: %s",i+1,c->nrPictures,c->minFocalLength,cameraName);
            }
        }
        LOG0(NULL);
    }

    if(!statusFlagsCheck(StatusFlagDoARescan)){
#ifdef DEBUG_PICS_FOLDER
        checkAlbums(ptrParsing->mdPics->rootAlbum,1,0);
#endif
        //if we need to do a rescan, the assertions below do not make sense
        ph_assert(totalEntries==ptrParsing->mdPics->scannedEntries,
                  "totalEntries (%d) and scannedEntries (%d) do not match.",totalEntries,ptrParsing->mdPics->scannedEntries);

        ph_assert(nrCameraPictures+ptrParsing->nrPicturesWithUnknownCamera==adbp[0].nrTotalPictrs+adbp[0].nrTotalVideos,
                "Total nr pictures by camera (%d) plus nr pictures without camera info (%d) not equal with the counted nrTotalPics (%d) + nrTotalVideos (%d). Difference: %d",
                nrCameraPictures,ptrParsing->nrPicturesWithUnknownCamera,adbp[0].nrTotalPictrs,adbp[0].nrTotalVideos,
                nrCameraPictures+ptrParsing->nrPicturesWithUnknownCamera-adbp[0].nrTotalPictrs-adbp[0].nrTotalVideos);

        //check that no album has a "being scanned" status
        checkIfStillMarkedAsBeingScanned(ptrParsing->mdPics->rootAlbum,1);
    } else freeDpsData();
    releaseMdPics(&ptrParsing->mdPics);

    //freeing the parsing data (part of it was freed above)
    adbp=NULL;
    free(ptrParsing);
    ptrParsing=NULL;



    LOG0(NULL);
    ph_malloc_info();
    LOG0(NULL);

    LOG_FLUSH;
    *forcePicturesFullRescan=0; //if here, everything went well
    return 0;
}

void freeAlbums(struct md_album_entry **album, uint16_t *nrEntries){
    int i;
    if(!(*album))return;
    //LOG0("freeAlbums++");
    for(i=0;i<*nrEntries;i++)
        if((*album)[i].flags&FlagIsAlbum && (*album)[i].album)
            freeAlbums(&(*album)[i].album,&(*album)[i].entriesIndex);


    if(*nrEntries){
        ph_assert(*(struct mdx_album_entry**)(*album+(*nrEntries)),NULL);
        //LOG0("Freeing mdx %p",*(struct mdx_album_entry**)(*album+(*nrEntries)));
        free(*(struct mdx_album_entry**)(*album+(*nrEntries)));
    } else
        ph_assert(NULL==*(struct mdx_album_entry**)(*album+(*nrEntries)),NULL);
    //LOG0("Freeing %p",*album);
    free(*album);
    *album=NULL;
    *nrEntries=0;
    //LOG0("freeAlbums--");
}

void printAlbums(struct md_album_entry *album, int nrEntries, int level)
{
    int i;
    struct tm e,l;
    time_t te,tl;
    char be[20],bl[20];
    ph_assert(album,NULL);
    for(i=0;i<nrEntries;i++)
        if(album[i].flags&FlagIsAlbum && album[i].album){
            te=album[i].earliestCaptureTime;
            tl=album[i].latestCaptureTime;
            gmtime_r(&te,&e);
            gmtime_r(&tl,&l);
            strftime(be,20,"%F",&e);
            strftime(bl,20,"%F",&l);
            //tabPrint(level);
            LOG0("Album: %s (%d)    flags: %x (%d - %d) / (%s - %s)",getNameFromAlbum(album,i),album[i].entriesIndex,
                 album[i].flags,album[i].earliestCaptureTime,album[i].latestCaptureTime,be,bl);
            printAlbums(album[i].album,album[i].entriesIndex,level+1);
        }
        //else LOG0("Picture entry: %s/%x",getNameFromAlbum(album,i),album[i].flags);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// checkAndCreateThumbnails

struct ph_old_thumbnails {
    FILE *F1;
    FILE *F2;
    struct md_thumbnails *entries1;
    struct md_thumbnails *entries2;
    int n;
};

struct ph_old_thumbnails *openOldThumbnailFiles(char * const path);
int getThumbnailsWithETag(struct ph_old_thumbnails const * const thmbs, struct md_level_etag const * const etag,
                        char ** const b1, char ** const b2, int * const l1, int * const l2);
void closeOldThumbnailFiles(struct ph_old_thumbnails * const thmbs);


int convertThumbFileToV3(char * const path, struct md_album_entry const * const a){
    const int pathLen=strlen(path);
    int r,n,i,l;
    char *oldPath=ph_strdup(path);
    int goOn=1,oldThumbnailFileSize;

    LOG0("Converting thumbnail file from Version 2 to version 3: %s",path);
    //change the oldPath name
    ph_assert(pathLen>5,NULL); //should be significantly bigger than 5
    ph_assert(oldPath[pathLen-4]=='.',NULL);
    ph_assert(oldPath[pathLen-3]=='d',NULL);
    ph_assert(oldPath[pathLen-2]=='a',NULL);
    ph_assert(oldPath[pathLen-1]=='t',NULL);
    oldPath[pathLen-3]='o';
    oldPath[pathLen-2]='l';
    oldPath[pathLen-1]='d';
    //rename the current file to oldPath
    r=rename(path,oldPath);
    LOG0close_return(r,-1,free(oldPath),"WARNING: cannot rename %s to %s.",path,oldPath);

    //open the old file and read the header
    struct md_thmbfile_header thmbfileHeader;
    struct md_thumbnails *thmbEntries=NULL;
    struct md_thumbnails_v2 *thmbEntriesOld=NULL;
    FILE *fOld=fopen(oldPath,"r");
    LOG0c_do(!fOld,goOn=0,"WARNING: cannot open old thumbnail file %s. Error: %s",oldPath,strerror(errno));
    //rename the target file to ".new"
    ph_assert(path[pathLen-4]=='.',NULL);
    ph_assert(path[pathLen-3]=='d',NULL);
    ph_assert(path[pathLen-2]=='a',NULL);
    ph_assert(path[pathLen-1]=='t',NULL);
    path[pathLen-3]='n';
    path[pathLen-2]='e';
    path[pathLen-1]='w';
    FILE *fNew=fopen(path,"w");
    LOG0c_do(!fNew,goOn=0,"WARNING: cannot open new thumbnail file %s. Error: %s",path,strerror(errno));
    setOwnership(path,0);

    if(goOn){
        //fint the size of the old thumbnail file
        struct stat st;
        r=stat(oldPath,&st);
        LOG0c_do(r,goOn=0,"WARNING: stat failed for %s. Error: %s",oldPath,strerror(errno));
        if(!r)
            oldThumbnailFileSize=st.st_size;
    }

    if(goOn){
        r=fread(&thmbfileHeader,sizeof(struct md_thmbfile_header),1,fOld);
        LOG0c_do(r!=1 || thmbfileHeader.version!=2 || thmbfileHeader.nrEntries!=a->entriesIndex,goOn=0,"WARNING: error reading old thumbnail's file header.");
    }

    if(goOn){
        n=thmbfileHeader.nrEntries;
        thmbEntries=(struct md_thumbnails*)ph_malloc((n+1)*sizeof(struct md_thumbnails));
        thmbEntriesOld=(struct md_thumbnails_v2*)ph_malloc(n*sizeof(struct md_thumbnails_v2));
        LOG0c_do(!thmbEntries || !thmbEntriesOld,goOn=0,"WARNING: memory allocation failed.");
    }

    if(goOn){
        r=fread(thmbEntriesOld,n*sizeof(struct md_thumbnails_v2),1,fOld);
        LOG0c_do(r!=1,goOn=0,"WARNING: counld not read thumbnail data from old thumbnail file.");
    }
    //can we actually convert this file? Do the elements match?
    //at the same tine, build the new structure
    if(goOn){
        for(i=0;i<n;i++){
            struct md_thumbnails_v2 const * const __restrict t2=&thmbEntriesOld[i];
            struct md_thumbnails * const __restrict t3=&thmbEntries[i];
            struct md_album_entry const * const __restrict e=&a->album[i];
            if(!(e->flags&FlagIsAlbum) && !(e->flags&FlagIsVideo)){
                LOG0close_break(e->size!=t2->size,goOn=0,"WARNING: sizes do not match for element with index=%d (from %d)",i,n);
            }
            //if we are here, the sizes matches and we should (partially) build the second element
            t3->etag.nameCRC=e->nameCRC;
            t3->etag.captureTime=e->captureTime;
            t3->etag.sizeOrLatestCaptureTime=e->size;
            t3->etag.modificationTime=e->modificationTime;
        }
    }

    if(goOn){
        //seek over the beginning part in fNew
        r=fseek(fNew,sizeof(struct md_thmbfile_header)+(n+1)*sizeof(struct md_thumbnails),SEEK_SET);
        LOG0c_do(r,goOn=0,"WARNING: seek error: %s",strerror(errno));
    }

    int posNew=sizeof(struct md_thmbfile_header)+(n+1)*sizeof(struct md_thumbnails);
    if(goOn){
        //read thumbnail elements and write them into the second (new) file
        for(i=0;i<n-1;i++){
            l=thmbEntriesOld[i+1].offset-thmbEntriesOld[i].offset;
            LOG0close_break(l<0,goOn=0,"WARNING: negative size computed for element with index=%d (from %d)",i,n);

            thmbEntries[i].offset=posNew;
            if(l>0){
                char *b=malloc(l);
                LOG0close_break(!b,goOn=0,"WARNING: buffer could not be allocated (length=%d) for index=%d (from %d)",l,i,n);
                r=fread(b,l,1,fOld);
                LOG0close_break(r!=1,free(b);goOn=0,"WARNING: could not read thumbnail (length=%d) for index=%d (from %d)",l,i,n);
                r=fwrite(b,l,1,fNew);
                LOG0close_break(r!=1,free(b);goOn=0,"WARNING: could not write thumbnail (length=%d) for index=%d (from %d)",l,i,n);
                free(b);
                posNew+=l;
            }
        }
    }
    if(goOn){
        i=n-1;
        //handle the last element (part 1/2)
        l=oldThumbnailFileSize-thmbEntriesOld[i].offset;
        LOG0c_do(l<0,goOn=0,"WARNING: negative size computed for element with index=%d (from %d)",i,n);
        thmbEntries[i].offset=posNew;
    }
    if(goOn){
        //handle the last element (part 2/2)
        if(l>0){
            char *b=malloc(l);
            LOG0c_do(!b,goOn=0,"WARNING: buffer could not be allocated (length=%d) for index=%d (from %d)",l,i,n);
            if(goOn){
                r=fread(b,l,1,fOld);
                LOG0c_do(r!=1,free(b);goOn=0,"WARNING: could not read thumbnail (length=%d) for index=%d (from %d)",l,i,n);
            }
            if(goOn){
                r=fwrite(b,l,1,fNew);
                LOG0c_do(r!=1,free(b);goOn=0,"WARNING: could not write thumbnail (length=%d) for index=%d (from %d)",l,i,n);
            }
            if(goOn){
                free(b);
                posNew+=l;
            }
        }
        thmbEntries[i+1].offset=posNew;
        thmbEntries[i+1].etag.nameCRC=0;
        thmbEntries[i+1].etag.captureTime=DATE_MAX;
        thmbEntries[i+1].etag.sizeOrLatestCaptureTime=0;
        thmbEntries[i+1].etag.modificationTime=0;
    }

    if(goOn){
        //write the header into the new file
        thmbfileHeader.version=CURRENT_THUMBNAILS_FILE_VERSION;
        r=fseek(fNew,0,SEEK_SET);
        LOG0c_do(r,goOn=0,"WARNING: seek error: %s",strerror(errno));
        r=fwrite(&thmbfileHeader,sizeof(struct md_thmbfile_header),1,fNew);
        LOG0c_do(r!=1,goOn=0,"WARNING: header write error (new thumbnails file): %s",strerror(errno));
        r=fwrite(thmbEntries,(n+1)*sizeof(struct md_thumbnails),1,fNew);
        LOG0c_do(r!=1,goOn=0,"WARNING: thumbnails data write error (new thumbnails file): %s",strerror(errno));
    }

    //now we can close and free everything
    free(thmbEntries);
    free(thmbEntriesOld);
    if(fNew)fclose(fNew);
    if(fOld)fclose(fOld);
    //delete the old path file
    unlink(oldPath);

    //rename path back
    ph_assert(path[pathLen-4]=='.',NULL);
    ph_assert(path[pathLen-3]=='n',NULL);
    ph_assert(path[pathLen-2]=='e',NULL);
    ph_assert(path[pathLen-1]=='w',NULL);
    path[pathLen-3]='d';
    path[pathLen-2]='a';
    path[pathLen-1]='t';
    LOG0close_return(!goOn,-1,unlink(path);free(oldPath),"Converting thumbnail file (%s) from version 2 to version 3 failed.",path);
    //if here we were successful. Rename the ".new" file to ".dat"
    ph_assert(oldPath[pathLen-4]=='.',NULL);
    ph_assert(oldPath[pathLen-3]=='o',NULL);
    ph_assert(oldPath[pathLen-2]=='l',NULL);
    ph_assert(oldPath[pathLen-1]=='d',NULL);
    oldPath[pathLen-3]='n';
    oldPath[pathLen-2]='e';
    oldPath[pathLen-1]='w';
    r=rename(oldPath,path);
    LOG0close_return(r,-1,free(oldPath),"WARNING: renaming %s to %s failed.",oldPath,path);
    free(oldPath);

    LOG0("Conversion was successful!");
    return 0;
}

int ccThumbnailsInFolder(const int level, struct md_album_entry const * const mdAlbum, const int mdAlbumIndex){
    ph_assert(sizeof(uint32_t)==4,NULL); //otherwise some of the size assumptions below do not work
    if(shouldExit)return -1;

    int thumbnailsNeedCreation=0; //we suppose it is ok
    struct md_album_entry const * const __restrict a=&mdAlbum[mdAlbumIndex];
    int r,i;

    //make sure folders end with /
    if(ptrParsing->picsDir[ptrParsing->picsDirLen-1]!='/'){
        ptrParsing->picsDir[ptrParsing->picsDirLen++]='/';
        ptrParsing->picsDir[ptrParsing->picsDirLen]='\0';
    }
    if(ptrParsing->dataDir[ptrParsing->dataDirLen-1]!='/'){
        ptrParsing->dataDir[ptrParsing->dataDirLen++]='/';
        ptrParsing->dataDir[ptrParsing->dataDirLen]='\0';
    }


    //set ptrParsing->currentAlbum
    ptrParsing->currentAlbum.level=level;
    if(level)
        ptrParsing->currentAlbum.nameCRC[level-1]=a->nameCRC;


    memcpy(ptrParsing->dataDir+ptrParsing->dataDirLen,KThumbnailsFilename,strlen(KThumbnailsFilename)+1);
    //LOG0("Opening thumbnail file: %s",ptrParsing->dataDir);
    //check if there is an old thumbnail file and if we need to delete it
    FILE *thumbnailF1=fopen(ptrParsing->dataDir,"r");
    if(thumbnailF1){
        struct md_thmbfile_header thmbfileHeader;
        struct md_thumbnails *thmbEntries=NULL;
        int nrUntouchedThumbnails=0;
        r=fread(&thmbfileHeader,sizeof(struct md_thmbfile_header),1,thumbnailF1);
        if(r!=1){
            thumbnailsNeedCreation=2;
            LOG0("Recreating thumbnail files because r(%d)!=1",r);
        }
        if(!thumbnailsNeedCreation && thmbfileHeader.version!=CURRENT_THUMBNAILS_FILE_VERSION){
            if(thmbfileHeader.version==2 && CURRENT_THUMBNAILS_FILE_VERSION==3){
                //try to convert this file to version 3
                fclose(thumbnailF1);
                thumbnailF1=NULL;
                ph_assert(statusFlagsCheck(StatusFlagThumbnailScanActive),NULL);
                LOG0return(statusFlagsCheck(StatusFlagDoARescan),0,"Exiting thumbnail scanning (flag StatusFlagDoARescan raised)");
                ph_assert(a==&mdAlbum[mdAlbumIndex],NULL);
                r=convertThumbFileToV3(ptrParsing->dataDir,a);
                if(!r){
                    thumbnailF1=fopen(ptrParsing->dataDir,"r");
                    if(thumbnailF1){
                        r=fread(&thmbfileHeader,sizeof(struct md_thmbfile_header),1,thumbnailF1);
                        if(r!=1 || thmbfileHeader.version!=CURRENT_THUMBNAILS_FILE_VERSION)
                            thumbnailsNeedCreation=2;
                    } else thumbnailsNeedCreation=2;
                } else thumbnailsNeedCreation=2;
                //also convert the thumbnails2.dat file
                i=strlen(ptrParsing->dataDir);
                ph_assert(i>5 && ptrParsing->dataDir[i-5]=='1',NULL);
                ptrParsing->dataDir[i-5]='2';
                ph_assert(statusFlagsCheck(StatusFlagThumbnailScanActive),NULL);
                LOG0return(statusFlagsCheck(StatusFlagDoARescan),0,"Exiting thumbnail scanning (flag StatusFlagDoARescan raised)");
                ph_assert(a==&mdAlbum[mdAlbumIndex],NULL);
                r=convertThumbFileToV3(ptrParsing->dataDir,a);
                //put the letter back
                ptrParsing->dataDir[i-5]='1';
                LOG0c_do(r,thumbnailsNeedCreation=2,"Converting second thumbnail file was not successful.");
            } else thumbnailsNeedCreation=2;
            LOG0c(thumbnailsNeedCreation,"Recreating thumbnail files because file version (%d)!=current version (%d) and conversion failed.",
                 thmbfileHeader.version,CURRENT_THUMBNAILS_FILE_VERSION);
        }

        if(!thumbnailsNeedCreation){//we can check further
            if(thmbfileHeader.nrEntries!=a->entriesIndex){
                thumbnailsNeedCreation=1;
                LOG0("Recreating thumbnail files because nrThumbnails(%d)!=nrEntries(%d)",
                     thmbfileHeader.nrEntries,a->entriesIndex);
            }
        }

        if(!thumbnailsNeedCreation){//we can check further
            thmbEntries=ph_malloc((a->entriesIndex+1)*sizeof(struct md_thumbnails));//+1 for the size
            r=fread(thmbEntries,(a->entriesIndex+1)*sizeof(struct md_thumbnails),1,thumbnailF1);
            if(r!=1){
                thumbnailsNeedCreation=2;
                LOG0("Recreating thumbnail files (%s) file because r(%d)!=1",ptrParsing->dataDir,r);
            }
        }
        if(thumbnailF1)
            fclose(thumbnailF1);//not needed any more

        ph_assert(statusFlagsCheck(StatusFlagThumbnailScanActive),NULL);
        LOG0close_return(statusFlagsCheck(StatusFlagDoARescan),0,free(thmbEntries),"Exiting thumbnail scanning (flag StatusFlagDoARescan raised)");

        ph_assert(a==&mdAlbum[mdAlbumIndex],NULL);
        if(!thumbnailsNeedCreation)//we can check further
            for(i=0;i<a->entriesIndex;i++){
                struct md_album_entry const * const __restrict e=&a->album[i];
                struct md_thumbnails * const __restrict t=&thmbEntries[i];

                //is this an album? If yes, we do not care about it
                if(e->flags&FlagIsAlbum)
                    continue;

                //is it the same id?
                if(e->nameCRC!=t->etag.nameCRC || e->captureTime!=t->etag.captureTime || e->size!=t->etag.sizeOrLatestCaptureTime ||
                        e->modificationTime!=t->etag.modificationTime){
                    thumbnailsNeedCreation=1;
                    LOG0("Recreating thumbnail files because ETAGs are different for i=%d",i);
                    break;
                } else {
                    //does this entry actually have a thumbnail?
                    if(thmbEntries[i+1].offset-t->offset>0){
                        //yes, it does
                        //we need a write lock to change the flag
                        releaseMdPics(&ptrParsing->mdPics);
                        struct ps_md_pics *mdPicsW=getMdPicsWritable();
                        *(uint16_t*)(&e->flags)|=FlagHasScaledThumbnail;
                        releaseMdPicsW(&mdPicsW);
                        ptrParsing->mdPics=getMdPicsRd();
                        nrUntouchedThumbnails++;
                    } else {
                        //entry does NOT have a thumbnail
                        if(e->flags&FlagIsVideo){
                            thumbnailsNeedCreation=1;
                            LOG0("Recreating thumbnail files because a video does NOT have a thumbnail.");
                            break;
                        }
                    }
                    //
                }
            }

        //here we should have an answer
        if(!thumbnailsNeedCreation){
            if(nrUntouchedThumbnails){
                ptrParsing->nrUntouchedThumbnails+=nrUntouchedThumbnails;
                //mark the parent album as having scaled thumbnail (s)
                //we need a write lock to change the flag
                releaseMdPics(&ptrParsing->mdPics);
                struct ps_md_pics *mdPicsW=getMdPicsWritable();
                *(uint16_t*)(&a->flags)|=FlagHasScaledThumbnail;
                releaseMdPicsW(&mdPicsW);
                ptrParsing->mdPics=getMdPicsRd();
            }
            ptrParsing->nrUntouchedThumbnailFiles++;
            ptrParsing->sizeOfUntouchedThumbnailFiles+=thmbEntries[a->entriesIndex].offset;
            //add the size of thumbnails2.dat to sizeOfUntouchedThumbnailFiles
            i=strlen(ptrParsing->dataDir);
            ph_assert(i>5 && ptrParsing->dataDir[i-5]=='1',NULL);
            ptrParsing->dataDir[i-5]='2';
            r=stat(ptrParsing->dataDir,&ptrParsing->statBuf);
            if(!r)
                ptrParsing->sizeOfUntouchedThumbnailFiles+=ptrParsing->statBuf.st_size;
            ptrParsing->dataDir[i-5]='1';
            //done
            //LOG0("Thumbnails file ok:     %s",ptrParsing->dataDir);
        } else LOG0("Thumbnail files will be (re)created");
        free(thmbEntries);
        thmbEntries=NULL;

        if(!thumbnailsNeedCreation){
            //go recursively into the sub-albums
            int savedPicsDirLen=ptrParsing->picsDirLen;
            int savedDataDirLen=ptrParsing->dataDirLen;

            ph_assert(statusFlagsCheck(StatusFlagThumbnailScanActive),NULL);
            LOG0return(statusFlagsCheck(StatusFlagDoARescan),0,"Exiting thumbnail scanning (flag StatusFlagDoARescan raised)");
            ph_assert(a==&mdAlbum[mdAlbumIndex],NULL);

            for(i=0;i<a->entriesIndex;i++)
                if(a->album[i].flags&FlagIsAlbum){
                    char *filename=getNameFromAlbum(a->album,i);
                    r=strlen(filename);
                    memcpy(ptrParsing->picsDir+savedPicsDirLen,filename,r+1);
                    ptrParsing->picsDirLen=savedPicsDirLen+r;
                    memcpy(ptrParsing->dataDir+savedDataDirLen,filename,r+1);
                    ptrParsing->dataDirLen=savedDataDirLen+r;
                    ccThumbnailsInFolder(level+1,a->album,i);
                    ptrParsing->currentAlbum.level=level;
                    if(shouldExit)return -1;
                    //things could have changed meanwhile, so check if we should exit
                    LOG0return(statusFlagsCheck(StatusFlagDoARescan),0,"Exiting thumbnail scanning (flag StatusFlagDoARescan raised)");
                    sendStatusToConnectedClients(&ptrParsing->currentAlbum);
                }
            //change the album modification time
            releaseMdPics(&ptrParsing->mdPics);
            struct ps_md_pics *mdPicsW=getMdPicsWritable();
            propagateModification(mdPicsW);
            releaseMdPicsW(&mdPicsW);
            sendStatusToConnectedClients(&ptrParsing->currentAlbum);
            ptrParsing->mdPics=getMdPicsRd();//we enter this function holding the mdPics mutex, we exit the same way
            LOG_FLUSH;
            return 0;
        }
    } else {
        LOG0("File %s could not be opened: %s",ptrParsing->dataDir,strerror(errno));
        thumbnailsNeedCreation=2;
    }

    //if here, we need to recreate the thumbnails
    ptrParsing->currentAlbum.level=level;
    ph_assert(thumbnailsNeedCreation,NULL);
    //try to reuse thumbnails if we can
    struct ph_old_thumbnails *oldThumbs=NULL;
    if(thumbnailsNeedCreation==1)//if it equals 2, the thumbnail file(s) do not exist or they are corrupted
        oldThumbs=openOldThumbnailFiles(ptrParsing->dataDir);

    //create thumbnail files as .new
    i=strlen(ptrParsing->dataDir);
    ph_assert(i>5,NULL); //should be significantly bigger than 5
    ph_assert(ptrParsing->dataDir[i-4]=='.',NULL);
    ph_assert(ptrParsing->dataDir[i-3]=='d',NULL);
    ph_assert(ptrParsing->dataDir[i-2]=='a',NULL);
    ph_assert(ptrParsing->dataDir[i-1]=='t',NULL);
    ptrParsing->dataDir[i-3]='n';
    ptrParsing->dataDir[i-2]='e';
    ptrParsing->dataDir[i-1]='w';


    thumbnailF1=fopen(ptrParsing->dataDir,"w");
    LOG0return(!thumbnailF1,-1,"ERROR opening the thumbnails1 file (%s): %s",ptrParsing->dataDir,strerror(errno));
    setOwnership(ptrParsing->dataDir,0);
    //open the second (retina) thumbnail file
    //first, get the name right
    ph_assert(ptrParsing->dataDir[i-5]=='1',"%s",ptrParsing->dataDir);
    ptrParsing->dataDir[i-5]='2';
    FILE *thumbnailF2=fopen(ptrParsing->dataDir,"w");
    LOG0return(!thumbnailF2,-1,"ERROR opening the thumbnails2 file (%s): %s",ptrParsing->dataDir,strerror(errno));
    setOwnership(ptrParsing->dataDir,0);

    //fill in the metadata part
    struct md_thmbfile_header thumbHeader;
    long thmbEntriesSize=(a->entriesIndex+1)*sizeof(struct md_thumbnails),pos1,pos2;
    long thmbEntriesTotalSize=sizeof(struct md_thmbfile_header)+thmbEntriesSize;
    struct md_thumbnails *thumbEntries1=(struct md_thumbnails *)ph_malloc(thmbEntriesSize);
    struct md_thumbnails *thumbEntries2=(struct md_thumbnails *)ph_malloc(thmbEntriesSize);
    //fill in the header
    thumbHeader.version=CURRENT_THUMBNAILS_FILE_VERSION;
    thumbHeader.nrEntries=a->entriesIndex;

    //we will write the metadata part at the end, jump over it for the moment
    r=fseek(thumbnailF1,thmbEntriesTotalSize,SEEK_SET);
    r=fseek(thumbnailF2,thmbEntriesTotalSize,SEEK_SET);
    pos1=pos2=thmbEntriesTotalSize;
    ph_assert(pos1=ftell(thumbnailF1),NULL);
    ph_assert(pos2=ftell(thumbnailF2),NULL);
    ph_assert(ptrParsing->picsDir[ptrParsing->picsDirLen-1]=='/',"%s",ptrParsing->picsDir);

    ph_assert(statusFlagsCheck(StatusFlagThumbnailScanActive),NULL);
    LOG0close_return(statusFlagsCheck(StatusFlagDoARescan),0,
                     closeOldThumbnailFiles(oldThumbs);free(thumbEntries1);free(thumbEntries2);fclose(thumbnailF1);fclose(thumbnailF2),
            "Exiting thumbnail scanning (flag StatusFlagDoARescan raised)");

    ph_assert(a==&mdAlbum[mdAlbumIndex],NULL);

    int parentAlbumHasScaledThumbnail=0;
    for(i=0;i<a->entriesIndex;i++){
        //shortcuts
        struct md_album_entry const * const __restrict e=&a->album[i];
        struct md_thumbnails * const __restrict t1=&thumbEntries1[i];
        struct md_thumbnails * const __restrict t2=&thumbEntries2[i];

        //fill-in the id and the offset
        t1->etag.nameCRC=t2->etag.nameCRC=e->nameCRC;
        t1->etag.captureTime=t2->etag.captureTime=e->captureTime;
        t1->etag.sizeOrLatestCaptureTime=t2->etag.sizeOrLatestCaptureTime=e->size;
        t1->etag.modificationTime=t2->etag.modificationTime=e->modificationTime;
        t1->offset=pos1;
        t2->offset=pos2;

        //LOG0("POSITIONS for i=%d: %d and %d",i,pos1,pos2);
        if(!(e->flags&FlagIsAlbum)){
            //check if the thumbnail exists in the old file, and if yes, copy it
            char *b1=NULL,*b2=NULL;
            int l1,l2,r2,hasScaledThumbnail=0;
            struct md_level_etag etag={e->nameCRC,e->captureTime,e->size,e->modificationTime};
            //prepare picsDir for the case where we need to scale the picture
            char *filename=getNameFromAlbum(a->album,i);
            memcpy(ptrParsing->picsDir+ptrParsing->picsDirLen,filename,strlen(filename)+1);

            getThumbnailsWithETag(oldThumbs,&etag,&b1,&b2,&l1,&l2);
            if(b1 && b2){
                LOG0("We have thumbnails from the old file: l1=%d, l2=%d",l1,l2);
                //we have our thumbnails, from the old file.
                r =fwrite(b1,l1,1,thumbnailF1);
                r2=fwrite(b2,l2,1,thumbnailF2);
                free(b1);
                free(b2);
                if(r!=1 || r2!=1){
                    LOG0("Writing thumbnails to file (l1=%d and l2=%d) failed for index=%d",l1,l2,i);
                    pos1=ftell(thumbnailF1);
                    pos2=ftell(thumbnailF2);
                } else {
                    //writing to file succeeded
                    hasScaledThumbnail=1;
                    parentAlbumHasScaledThumbnail=1;

                    pos1+=l1;
                    pos2+=l2;
                    ptrParsing->nrReusedThumbnails++;
                    LOG0("Thumbnails reused (from old thumbnail file) for index=%d. Current positions: pos1=%ld, pos2=%ld",i,pos1,pos2);
                }
            } else {
                //add the thumbnail at the end
                unsigned int resizeTimeMS,resizeTimeMSperMpixel;

                if(e->flags&FlagIsVideo)
                    r=createVideoThumbnail(ptrParsing->picsDir,thumbnailF1,thumbnailF2,&resizeTimeMS,&resizeTimeMSperMpixel);
                else {
                    //this is image, but we need to find out what type
                    char *ext=strrchr(ptrParsing->picsDir,'.');
                    if(ext && (!strcasecmp(ext,".jpeg") || !strcasecmp(ext,".jpg")))
                        r=createJpegThumbnail(ptrParsing->picsDir,thumbnailF1,thumbnailF2,&resizeTimeMS,&resizeTimeMSperMpixel);
                    else if(ext && !strcasecmp(ext,".heic")){
                        char pathTran[PATH_MAX];
                        getTranscodedPathForMedia(ptrParsing->picsDir,"jpeg",pathTran);
                        r=createJpegThumbnail(pathTran,thumbnailF1,thumbnailF2,&resizeTimeMS,&resizeTimeMSperMpixel);
                    } else {
                        r=-1;
                        LOG0("WARNING: we do not how to create a thumbnail for this file/extension: %s",ptrParsing->picsDir);
                    }
                }
                LOG0c(r,"WARNING: Thumbnail creation failed for %s",ptrParsing->picsDir);
                if(!r){
                    //thumbnail creation succeeded!
                    hasScaledThumbnail=1;
                    parentAlbumHasScaledThumbnail=1;

                    pos1=ftell(thumbnailF1);
                    pos2=ftell(thumbnailF2);
                    ptrParsing->nrCreatedThumbnails++;
                    ptrParsing->thumbnailsCreationTimeMS+=resizeTimeMS;
                    ptrParsing->thumbnailsCreationTimePerMPixelMS+=resizeTimeMSperMpixel;
                }
            }
            LOG_FLUSH;
            ph_assert(statusFlagsCheck(StatusFlagThumbnailScanActive),NULL);
            LOG0close_return(statusFlagsCheck(StatusFlagDoARescan),0,
                             closeOldThumbnailFiles(oldThumbs);free(thumbEntries1);free(thumbEntries2);fclose(thumbnailF1);fclose(thumbnailF2),
                    "Exiting thumbnail scanning (flag StatusFlagDoARescan raised)");

            ph_assert(a==&mdAlbum[mdAlbumIndex],NULL);
            ph_assert(e==&a->album[i],NULL);
            if(hasScaledThumbnail){
                //we need a write lock to change the flag
                releaseMdPics(&ptrParsing->mdPics);
                struct ps_md_pics *mdPicsW=getMdPicsWritable();
                *(uint16_t*)(&e->flags)|=FlagHasScaledThumbnail;
                releaseMdPicsW(&mdPicsW);
                ptrParsing->mdPics=getMdPicsRd();
            }
        }
    }//for

    //do the last entry
    thumbEntries1[a->entriesIndex].etag.nameCRC=thumbEntries2[a->entriesIndex].etag.nameCRC=0;
    thumbEntries1[a->entriesIndex].etag.captureTime=thumbEntries2[a->entriesIndex].etag.captureTime=DATE_MAX;
    thumbEntries1[a->entriesIndex].etag.sizeOrLatestCaptureTime=thumbEntries2[a->entriesIndex].etag.sizeOrLatestCaptureTime=0;
    thumbEntries1[a->entriesIndex].etag.modificationTime=thumbEntries2[a->entriesIndex].etag.modificationTime=0;
    thumbEntries1[a->entriesIndex].offset=pos1;
    thumbEntries2[a->entriesIndex].offset=pos2;
    if(parentAlbumHasScaledThumbnail){
        //we need a write lock to change the flag
        releaseMdPics(&ptrParsing->mdPics);
        struct ps_md_pics *mdPicsW=getMdPicsWritable();
        *(uint16_t*)(&a->flags)|=FlagHasScaledThumbnail;
        releaseMdPicsW(&mdPicsW);
        ptrParsing->mdPics=getMdPicsRd();
    }

    //First is F2 because we check F1 for rightness. If something fails here (power loss), F1 is incomplete.
    //for thumbnailF2:
    r=fseek(thumbnailF2,0,SEEK_SET);
    ph_assert(!r,NULL);//TODO
    r=fwrite(&thumbHeader,sizeof(struct md_thmbfile_header),1,thumbnailF2);
    r=fwrite(thumbEntries2,thmbEntriesSize,1,thumbnailF2);
    free(thumbEntries2);
    fclose(thumbnailF2);
    //not too much point in checking for errors, there is not much we can do (except perhaps marking the data that scaled thumbnail is actually missing)
    LOG0("Thumbnail2 file has %ld KBytes (%ld bytes)",pos2>>10,pos2);
    ptrParsing->nrCreatedThumbnailFiles++;
    ptrParsing->sizeOfCreatedThumbnailFiles+=pos2;

    //for thumbnailF1:
    r=fseek(thumbnailF1,0,SEEK_SET);
    ph_assert(!r,NULL);//TODO
    r=fwrite(&thumbHeader,sizeof(struct md_thmbfile_header),1,thumbnailF1);
    r=fwrite(thumbEntries1,thmbEntriesSize,1,thumbnailF1);
    free(thumbEntries1);
    fclose(thumbnailF1);
    closeOldThumbnailFiles(oldThumbs);
    //not too much point in checking for errors, there is not much we can do (except perhaps marking the data that scaled thumbnail is actually missing)
    LOG0("Thumbnail1 file has %ld KBytes (%ld bytes)",pos1>>10,pos1);
    //ptrParsing->nrCreatedThumbnailFiles++; -> counting it only once
    ptrParsing->sizeOfCreatedThumbnailFiles+=pos1;

    //rename files from .new to .dat
    {
        char path[PATH_MAX];
        strcpy(path,ptrParsing->dataDir);
        i=strlen(path);
        //first we do the thumbnail2.dat/new, because we have it set already
        ph_assert(i>5,NULL); //should be significantly bigger than 5
        ph_assert(path[i-5]=='2',NULL);
        ph_assert(path[i-4]=='.',NULL);
        ph_assert(path[i-3]=='n',NULL);
        ph_assert(path[i-2]=='e',NULL);
        ph_assert(path[i-1]=='w',NULL);
        path[i-3]='d';
        path[i-2]='a';
        path[i-1]='t';
        r=rename(ptrParsing->dataDir,path);//rename also deletes the old/existing thumbnail2.dat
        LOG0c(r,"ERROR: renaming thumbnail file from %s to %s failed: %s",ptrParsing->dataDir,path,strerror(errno));
        //and for thumbnail1.dat now
        path[i-5]='1';
        ptrParsing->dataDir[i-5]='1';
        r=rename(ptrParsing->dataDir,path);//rename also deletes the old/existing thumbnail1.dat
        LOG0c(r,"ERROR: renaming thumbnail file from %s to %s failed: %s",ptrParsing->dataDir,path,strerror(errno));
    }

    //go recursively into the sub-albums
    int savedPicsDirLen=ptrParsing->picsDirLen;
    int savedDataDirLen=ptrParsing->dataDirLen;

    ph_assert(statusFlagsCheck(StatusFlagThumbnailScanActive),NULL);
    LOG0return(statusFlagsCheck(StatusFlagDoARescan),0,"Exiting thumbnail scanning (flag StatusFlagDoARescan raised)");
    ph_assert(a==&mdAlbum[mdAlbumIndex],NULL);

    for(i=0;i<a->entriesIndex;i++)
        if(a->album[i].flags&FlagIsAlbum){
            char *filename=getNameFromAlbum(a->album,i);
            r=strlen(filename);
            memcpy(ptrParsing->picsDir+savedPicsDirLen,filename,r+1);
            ptrParsing->picsDirLen=savedPicsDirLen+r;
            memcpy(ptrParsing->dataDir+savedDataDirLen,filename,r+1);
            ptrParsing->dataDirLen=savedDataDirLen+r;
            ccThumbnailsInFolder(level+1,a->album,i);
            ptrParsing->currentAlbum.level=level;
            if(shouldExit)return -1;
            sendStatusToConnectedClients(&ptrParsing->currentAlbum);
        }
    //change the album modification time
    releaseMdPics(&ptrParsing->mdPics);
    struct ps_md_pics *mdPicsW=getMdPicsWritable();
    propagateModification(mdPicsW);
    releaseMdPicsW(&mdPicsW);
    sendStatusToConnectedClients(&ptrParsing->currentAlbum);
    ptrParsing->mdPics=getMdPicsRd();
    LOG_FLUSH;
    return 0;
}

int checkAndCreateThumbnails(){
    struct timeval ts,te;
    int tdiff;
    int r,l;

    //we will reuse ptrParsing
    ptrParsing=(struct mdh_parsing*)ph_malloc(sizeof(struct mdh_parsing));
    memset(ptrParsing,0,sizeof(struct mdh_parsing));

    //prepare picsDir and dataDir
    struct ps_md_config const * const mdConfig=getMdConfigRd();
    l=strlen(mdConfig->rootFolder);
    memcpy(ptrParsing->picsDir,mdConfig->rootFolder,l);
    memcpy(ptrParsing->dataDir,mdConfig->rootFolder,l);
    releaseMdConfig(&mdConfig);
    r=strlen(KPicturesFolder);
    memcpy(ptrParsing->picsDir+l,KPicturesFolder,r+1);
    ptrParsing->picsDirLen=l+r;
    r=strlen(KPhotostovisData);
    memcpy(ptrParsing->dataDir+l,KPhotostovisData,r+1);
    ptrParsing->dataDirLen=l+r;

    //starting
    gettimeofday(&ts,NULL);

    statusFlagsAdd(StatusFlagThumbnailScanActive);
    ptrParsing->mdPics=getMdPicsRd();
    r=ccThumbnailsInFolder(0,ptrParsing->mdPics->rootAlbum,0);
    releaseMdPics(&ptrParsing->mdPics);

    statusFlagsRemove(StatusFlagThumbnailScanActive);

    //ending
    //some statistics
    gettimeofday(&te,NULL);
    tdiff=(te.tv_sec-ts.tv_sec)*1000+(te.tv_usec-ts.tv_usec)/1000;

    //changing sizes
    const char *KBytes="Bytes";
    const char *KKB="KB";
    const char *KMB="MB";
    char const *bkmUntouched, *bkmCreated;
    if(ptrParsing->sizeOfUntouchedThumbnailFiles>10*1024){
        ptrParsing->sizeOfUntouchedThumbnailFiles>>=10;
        bkmUntouched=KKB;
        if(ptrParsing->sizeOfUntouchedThumbnailFiles>10*1024){
            ptrParsing->sizeOfUntouchedThumbnailFiles>>=10;
            bkmUntouched=KMB;
        }
    } else bkmUntouched=KBytes;
    if(ptrParsing->sizeOfCreatedThumbnailFiles>10*1024){
        ptrParsing->sizeOfCreatedThumbnailFiles>>=10;
        bkmCreated=KKB;
        if(ptrParsing->sizeOfCreatedThumbnailFiles>10*1024){
            ptrParsing->sizeOfCreatedThumbnailFiles>>=10;
            bkmCreated=KMB;
        }
    } else bkmCreated=KBytes;


    LOG0("Done checking and creating thumbnails. Operation took %dms. Out of this time, effective thumbnail creation was %dms. Other time was %dms.",
         tdiff,ptrParsing->thumbnailsCreationTimeMS,tdiff-ptrParsing->thumbnailsCreationTimeMS);
    LOG0("Already created thumbnails: %d (+%d) in %d (+%d) files, totalling %d %s.",
         ptrParsing->nrUntouchedThumbnails,ptrParsing->nrUntouchedThumbnails,ptrParsing->nrUntouchedThumbnailFiles,ptrParsing->nrUntouchedThumbnailFiles,
         (int)(ptrParsing->sizeOfUntouchedThumbnailFiles),bkmUntouched);
    LOG0("Created thumbnails: %d (+%d), reused thumbnails: %d (+%d). Totalling %d (+%d) thumbnails in %d (+%d) files having %d %s.",
         ptrParsing->nrCreatedThumbnails,ptrParsing->nrCreatedThumbnails,ptrParsing->nrReusedThumbnails,ptrParsing->nrReusedThumbnails,
         ptrParsing->nrCreatedThumbnails+ptrParsing->nrReusedThumbnails,ptrParsing->nrCreatedThumbnails+ptrParsing->nrReusedThumbnails,
         ptrParsing->nrCreatedThumbnailFiles,ptrParsing->nrCreatedThumbnailFiles,
         (int)(ptrParsing->sizeOfCreatedThumbnailFiles),bkmCreated);
    //average thumbnail creation time. Average per megapixel:
    if(ptrParsing->nrCreatedThumbnails>0)
        LOG0("Average thumbnail(s) creation time: %dms. Average thumbnails creation time per MPixel: %dms.",
             ptrParsing->thumbnailsCreationTimeMS/ptrParsing->nrCreatedThumbnails,ptrParsing->thumbnailsCreationTimePerMPixelMS/ptrParsing->nrCreatedThumbnails);

    free(ptrParsing);
    ptrParsing=NULL;

    LOG_FLUSH;
    return 0;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// handeling previous thumbnail files

struct ph_old_thumbnails *openOldThumbnailFiles(char * const path){
    struct ph_old_thumbnails *thmbs=(struct ph_old_thumbnails*)malloc(sizeof(struct ph_old_thumbnails));
    if(!thmbs)return NULL;
    memset(thmbs,0,sizeof(struct ph_old_thumbnails));
    struct md_thmbfile_header hdr1,hdr2;

    //rename the old thumbnail files
    int goOn=1,l=strlen(path),r;

    if(goOn){
        //open thumbnails1.dat
        ph_assert(path[l-5]=='1',NULL);
        thmbs->F1=fopen(path,"r");
        LOG0c_do(!thmbs->F1,goOn=0,"WARNING: could not open thumbnails1.dat file %s. Error: %s",path,strerror(errno));
    }
    if(goOn){
        path[l-5]='2';
        //open thumbnails2.dat
        thmbs->F2=fopen(path,"r");
        LOG0c_do(!thmbs->F2,goOn=0,"WARNING: could not open thumbnails2.dat file %s. Error: %s",path,strerror(errno));
        //put the letter back
        path[l-5]='1';
    }

    if(goOn){
        //read the header from thumbnails1.old
        r=fread(&hdr1,sizeof(struct md_thmbfile_header),1,thmbs->F1);
        LOG0c_do(r!=1 || hdr1.version!=CURRENT_THUMBNAILS_FILE_VERSION,goOn=0,"WARNING: error reading header or wrong thumbnail file version");
    }
    if(goOn){
        //read the header from thumbnails2.old
        r=fread(&hdr2,sizeof(struct md_thmbfile_header),1,thmbs->F2);
        LOG0c_do(r!=1 || hdr2.version!=CURRENT_THUMBNAILS_FILE_VERSION,goOn=0,"WARNING: error reading header or wrong thumbnail file version");
    }
    if(goOn){
        ph_assert(hdr1.nrEntries==hdr2.nrEntries,NULL);
        thmbs->n=hdr1.nrEntries;
    }
    if(goOn){
        //allocate data
        l=(thmbs->n+1)*sizeof(struct md_thumbnails);
        thmbs->entries1=(struct md_thumbnails*)malloc(l);
        thmbs->entries2=(struct md_thumbnails*)malloc(l);
        LOG0c_do(!thmbs->entries1 || !thmbs->entries2,goOn=0,"WARNING: memory allocation problem");
    }
    if(goOn){
        //read the data from F1
        r=fread(thmbs->entries1,l,1,thmbs->F1);
        LOG0c_do(r!=1,goOn=0,"WARNING: error reading thumbnail data from thumbnails1.old");
    }
    if(goOn){
        //read the data from F2
        r=fread(thmbs->entries2,l,1,thmbs->F2);
        LOG0c_do(r!=1,goOn=0,"WARNING: error reading thumbnail data from thumbnails2.old");
    }

    //if everything is right, we can return
    if(goOn)
        return thmbs;

    //if here, something went wrong. Free all data and return NULL
    if(thmbs->entries1)free(thmbs->entries1);
    if(thmbs->entries2)free(thmbs->entries2);
    if(thmbs->F1)fclose(thmbs->F1);
    if(thmbs->F2)fclose(thmbs->F2);
    free(thmbs);
    return NULL;
}

int getThumbnailsWithETag(struct ph_old_thumbnails const * const thmbs, struct md_level_etag const * const etag,
                        char ** const b1, char ** const b2, int * const l1, int * const l2){
    //init with NULL/0 so we can return easily
    *b1=*b2=NULL;
    *l1=*l2=0;
    if(!thmbs)return -1;
    int i,r1,r2;
    for(i=0;i<thmbs->n;i++){
        struct md_thumbnails * __restrict e1=&thmbs->entries1[i];
        struct md_thumbnails * __restrict e2=&thmbs->entries2[i];
        if(etag->nameCRC==e1->etag.nameCRC && etag->captureTime==e1->etag.captureTime &&
                etag->sizeOrLatestCaptureTime==e1->etag.sizeOrLatestCaptureTime && etag->modificationTime==e1->etag.modificationTime &&
                etag->nameCRC==e2->etag.nameCRC && etag->captureTime==e2->etag.captureTime &&
                etag->sizeOrLatestCaptureTime==e2->etag.sizeOrLatestCaptureTime && etag->modificationTime==e2->etag.modificationTime){
            *l1=thmbs->entries1[i+1].offset-e1->offset;
            *l2=thmbs->entries2[i+1].offset-e2->offset;
            if(*l1==0 || *l2==0){
                ph_assert(*l1==0,NULL);
                ph_assert(*l2==0,NULL);
                return -1; //TODO: should it be 0??
            }
            //allocate data
            *b1=malloc(*l1);
            *b2=malloc(*l2);
            LOG0close_return(*b1==NULL || *b2==NULL,-1,free(*b1);free(*b2);*b1=*b2=NULL;*l1=*l2=0,"ERROR: memory allocation failed");
            //seek in files
            r1=fseek(thmbs->F1,e1->offset,SEEK_SET);
            r2=fseek(thmbs->F2,e2->offset,SEEK_SET);
            LOG0close_return(r1 || r2,-1,free(*b1);free(*b2);*b1=*b2=NULL;*l1=*l2=0,"ERROR: seek in old thumbnail file(s) failed");
            //read data
            r1=fread(*b1,*l1,1,thmbs->F1);
            r2=fread(*b2,*l2,1,thmbs->F2);
            LOG0close_return(r1!=1 || r2!=1,-1,free(*b1);free(*b2);*b1=*b2=NULL;*l1=*l2=0,"ERROR: reading old thumbnails from file(s) failed");
            //success!
            return 0;
        }
    }
    //if here, we found nothing
    return -1;
}

void closeOldThumbnailFiles(struct ph_old_thumbnails * const thmbs){
    if(!thmbs)return; //we do not check in the client

    ph_assert(thmbs,NULL);
    ph_assert(thmbs->entries1,NULL);
    ph_assert(thmbs->entries2,NULL);
    ph_assert(thmbs->F1,NULL);
    ph_assert(thmbs->F2,NULL);

    free(thmbs->entries1);
    free(thmbs->entries2);
    fclose(thmbs->F1);
    fclose(thmbs->F2);
    free(thmbs);
}




int changePictureName(const char *path){
    //we change the picture name from something like .../image00012.jpeg to .../image_20160306_184323_00.jpeg

    //can we get a date from this image?
    ExifData *ed=exif_data_new_from_file(path);
    LOG0return(!ed,-1,"changePictureName: cannot get EXIF info from file (%s)",path);

    int8_t timezone;
    char tempBuf[TEMP_BUF_MIN_SIZE*4];
    time_t dateTime=getExifCaptureDate(tempBuf,ed,NULL,&timezone);
    exif_data_unref(ed);

    LOG0return(dateTime==DATE_MAX,-1,"changePictureName: cannot get date from EXIF info for file (%s)",path);

    char *extensionStart=strrchr(path,'.');
    int pathLen=strlen(path);
    int extensionStartLen=strlen(extensionStart);
    LOG0return(!extensionStart,-1,"changePictureName: cannot get extension for file (%s)",path);
    LOG0return(pathLen-extensionStartLen-5<=0,-1,"changePictureName: filename too funny (%s)",path);

    char newPath[PATH_MAX];
    memcpy(newPath,path,pathLen+1);
    char *nameEnd=newPath+pathLen-extensionStartLen-5;

    //add the new name starting with nameEnd
    struct tm t;
    gmtime_r(&dateTime,&t);
    int dtLen=strftime(nameEnd,17,"_%Y%m%d_%H%M%S",&t);
    nameEnd+=dtLen;

    //add extension
    memcpy(nameEnd,extensionStart,extensionStartLen+1);
    struct stat st;
    LOG0("changePictureName: trying %s  ->  %s",path,newPath);

    if(!stat(newPath,&st)){
        //we need to add some number at the end
        int i;
        for(i=1;i<99;i++){
            sprintf(nameEnd,"_%02d",i);
            memcpy(nameEnd+3,extensionStart,extensionStartLen+1);
            LOG0("Trying %s",newPath);
            if(stat(newPath,&st) && errno==ENOENT)
                break;
        }
    }

    //rename
    LOG0("changePictureName: %s  -->  %s",path,newPath);
    rename(path,newPath);

    return 0;
}




static int _scanFoldersMoveTranscoded(char *pathPics, int lenPics, char *pathTran, int lenTran){
    struct dirent **namelist=NULL;
    int i,r;
    //LOG0("Path here: %s",pathPics);
    int n=scandir(pathPics,&namelist,NULL,NULL);
    ph_assert(n>=0,NULL);

    //add / at the end of pathPics and pathTran
    if(pathPics[lenPics-1]!='/')
        pathPics[lenPics++]='/';
    if(pathTran[lenTran-1]!='/')
        pathTran[lenTran++]='/';

    for(i=0;i<n;i++){
        if(!strcmp(namelist[i]->d_name,".") || !strcmp(namelist[i]->d_name,"..")){
            free(namelist[i]);
            continue;
        }

        //add name to paths
        const int l=strlen(namelist[i]->d_name);
        memcpy(pathPics+lenPics,namelist[i]->d_name,l+1);
        lenPics+=l;
        memcpy(pathTran+lenTran,namelist[i]->d_name,l+1);
        lenTran+=l;

        //is this a folder?
        if(namelist[i]->d_type==DT_DIR){
            _scanFoldersMoveTranscoded(pathPics,lenPics,pathTran,lenTran);
        } else if(namelist[i]->d_type==DT_REG){
            //does the current name ends with _l.mp4?
            if(l>7 && !strcmp(namelist[i]->d_name+l-6,"_l.mp4")){
                //do we also have a _h.mp4 version?
                ph_assert(pathPics[lenPics-5]=='l',NULL);
                pathPics[lenPics-5]='h';
                struct stat st;
                if(!stat(pathPics,&st)){
                    //yes, we do have a _h version
                    char path2[PATH_MAX];

                    pathPics[lenPics-5]='l';

                    //make the tran path
                    memcpy(path2,pathTran,lenTran-l);
                    path2[lenTran-l]='\0';
                    r=dirService(path2,1);
                    ph_assert(!r,NULL);

                    //remove the _l from the target name
                    memmove(pathTran+lenTran-6,".mp4",5);
                    LOG0("Moving %s to %s",pathPics,pathTran);
                    r=rename(pathPics,pathTran);
                    ph_assert(!r,NULL);

                    //rename the _h.mp4 into .mp4
                    memcpy(path2,pathPics,lenPics+1);
                    memmove(path2+lenPics-6,".mp4",5);
                    pathPics[lenPics-5]='h';

                    LOG0("Renaming %s to %s",pathPics,path2);
                    rename(pathPics,path2);
                    ph_assert(!r,NULL);
                } else {
                    pathPics[lenPics-5]='l';
                    LOG0("Found a _l.mp4 file (%s) without a corresponding _h.mp4",pathPics);
                }
            }
        } else ph_assert(namelist[i]->d_type==DT_UNKNOWN,NULL);

        //remove name
        lenPics-=l;
        lenTran-=l;
    }//for


    //LOG0("Here: %s -> %s",pathPics,pathTran);

    return 0;
}

int moveTranscodedFiles(){
    char pathPics[PATH_MAX];
    char pathTran[PATH_MAX];
    int l,lenPics,lenTran;

    //initialize the path
    struct ps_md_config const * const mdConfig=getMdConfigRd();
    l=lenPics=lenTran=strlen(mdConfig->rootFolder);
    memcpy(pathPics,mdConfig->rootFolder,l);
    memcpy(pathTran,mdConfig->rootFolder,l);
    releaseMdConfig(&mdConfig);

    l=strlen(KPicturesFolder);
    memcpy(pathPics+lenPics,KPicturesFolder,l+1);
    lenPics+=l;

    l=strlen(KTranscodedFolder);
    memcpy(pathTran+lenTran,KTranscodedFolder,l+1);
    lenTran+=l;

    //r=dirService(pathTran,1);

    //scan the pictures path for *_l.mp4 files that have a corresponding *_h.mp4 file
    _scanFoldersMoveTranscoded(pathPics,lenPics,pathTran,lenTran);


    return 0;
}

int deleteMedia(struct md_entry_id *mediaId){


    return 0;
}


