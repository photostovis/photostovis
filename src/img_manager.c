#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <fcntl.h>
#include <limits.h> //PATH_MAX

#include "log.h"
#include "photostovis.h"

struct ps_img_prefetch {
    char *filename;
    char *buf;
    size_t bufLen;
    pthread_t threadId; //do we need this?
    uint32_t fileSize;
    //uint32_t targetSize;
    uint8_t priority;
    uint8_t scaleNum;
    uint8_t processingStatus; //0=waiting (for a thread to become free), 1=being created, 2=ready, 3=creation failed
    uint8_t waitingStatus;    //0=finish, detach & exit, 1=somebody waiting, do not detach
};

static struct ps_img_prefetch *images=NULL;
static int nrImages=0;
static pthread_mutex_t imagesMutex=PTHREAD_MUTEX_INITIALIZER;
static uint8_t nrCores=0;
static uint8_t nrThreadsInUse=0;

void imgManagerInit(){
    nrCores=sysconf(_SC_NPROCESSORS_ONLN);
    if(nrCores>0)LOG0("Detected number of processors/cores: %d",nrCores);
    else LOG0c_do(1,nrCores=2,"WARNING: detecting the number of processors/cores failed (%s). Assumed to be 2.",strerror(errno));
}

void imgManagerClose(){
    int i;
    pthread_mutex_lock(&imagesMutex);
    for(i=0;i<nrImages;i++){
        free(images[i].buf);
        free(images[i].filename);
    }
    free(images);
    images=NULL;
    nrImages=0;
    pthread_mutex_unlock(&imagesMutex);
    pthread_mutex_destroy(&imagesMutex);
}

void computeTargetImgParameters(const uint32_t fileSize, uint16_t imgWidth, uint16_t imgHeight, int requestedWidth, int requestedHeight,
                                struct ps_session const * const mySession, uint32_t * const targetSize, uint8_t * const scaleFactorNum){
    uint32_t estimatedTransferTimeMS=MAX_AVERAGE_VIEW_TIME; //consider rather large if we cannot estimate
    uint32_t estimatedRenderingTimeMS=(uint32_t)imgWidth*(uint32_t)imgHeight/1000000*mySession->averageRenderingTimePerMPixel;
    uint8_t minScaleFactorNum=0;

    if(estimatedRenderingTimeMS<1)estimatedRenderingTimeMS=1;
    //do we have resolution constrains?
    if(requestedWidth>0){
        ph_assert(requestedHeight>0,NULL);
        if(requestedWidth==imgWidth)minScaleFactorNum=8;
        else {
            int i;
            for(i=1;i<8;i++)
                if(requestedWidth<=(i*imgWidth)>>3)
                    break;
            minScaleFactorNum=i;
        }
        ph_assert(minScaleFactorNum>=1 && minScaleFactorNum<=8,NULL);
    }


    if(mySession->bitrate>0)
        estimatedTransferTimeMS=(fileSize<<3)/mySession->bitrate;
    if(estimatedTransferTimeMS>mySession->averageViewTime || estimatedRenderingTimeMS>MAX_ACCEPTABLE_RENDERING_TIME){
        *targetSize=(mySession->averageViewTime*mySession->bitrate)>>3;
        if(*targetSize==0)
            *targetSize=fileSize>>4; // 1/16
        float scaleRatio=*targetSize/(float)fileSize;
        float renderingRatio=MAX_ACCEPTABLE_RENDERING_TIME/(float)estimatedRenderingTimeMS;
        LOG0("->->->->->->->->->->->->->->->->->->Estimated transfer time: %ums (bitrate: %u kbps). Scale ratio: %f for a target size of %uKB (original: %uKB)",
             estimatedTransferTimeMS,mySession->bitrate,scaleRatio,*targetSize>>10,fileSize>>10);
        LOG0("->->->->->->->->->->->->->->->->->->Estimated rendering time: %ums. Rendering ratio: %f",estimatedRenderingTimeMS,renderingRatio);
        if(scaleRatio>=0.5 && renderingRatio>=0.5)*scaleFactorNum=6;
        else if(scaleRatio>=0.39 && renderingRatio>=0.39)*scaleFactorNum=5;
        else if(scaleRatio>=0.25 && renderingRatio>=0.25)*scaleFactorNum=4;
        else if(scaleRatio>=0.14 && renderingRatio>=0.14)*scaleFactorNum=3;
        else if(scaleRatio>=0.06 && renderingRatio>=0.06)*scaleFactorNum=2;
        else *scaleFactorNum=1;

        //if we have a "smallDevice", we should not send too big images because it cannot handle them properly
        if(mySession->flags&FlagSession_FromSmallDevice){
            //is this resolution too big for our screen?
            uint8_t scaleFactorNumBeforeAdjustment=*scaleFactorNum;
            uint16_t screenMax=(mySession->screenWidth>mySession->screenHeight?mySession->screenWidth:mySession->screenHeight)*mySession->pixelRatio;
            uint16_t imgMax8x2=imgWidth>imgHeight?(imgWidth>>2):(imgHeight>>2); // /8*2
            while(*scaleFactorNum>1 && imgMax8x2*(*scaleFactorNum)>screenMax)
                (*scaleFactorNum)--;
            LOG0c(*scaleFactorNum!=scaleFactorNumBeforeAdjustment,"smallDevice scale factor adjustment: from %u to %u.",
                  (unsigned)scaleFactorNumBeforeAdjustment,(unsigned)(*scaleFactorNum));
        }



        //our scaled imge should be bigger than the image thumbnail (otherwise it does not make sense to have/send it)
        uint16_t minDimension=imgWidth<imgHeight?imgWidth:imgHeight;
        uint16_t minDimension8=minDimension>>3;
        if(mySession->pixelRatio>1){
            //minimum thumbnail dimension is at least 300
            while(minDimension8*(*scaleFactorNum)<600)
                (*scaleFactorNum)++;
        } else {
            //minimum dimension is at least 150
            while(minDimension8*(*scaleFactorNum)<300)
                (*scaleFactorNum)++;
        }
        if(*scaleFactorNum>8)
            *scaleFactorNum=8;
        if(minScaleFactorNum>0 && *scaleFactorNum<minScaleFactorNum){
            LOG0("Adjusting the scaleFactor from %u to minScaleFactor of %u",(unsigned int)*scaleFactorNum,(unsigned int)minScaleFactorNum);
            *scaleFactorNum=minScaleFactorNum;
        }
    } else {
        *targetSize=fileSize;
        *scaleFactorNum=8;
        LOG0("->->->->->->->->->->->->->->->->->->Estimated transfer time: %ums (bitrate: %u kbps), rendering time: %ums. NO scaling.",
             estimatedTransferTimeMS,mySession->bitrate,estimatedRenderingTimeMS);
    }
}



static void removeImage(const int index, const struct ps_img_prefetch *ourImage){
    ph_assert(ourImage-images==index,NULL);
    ph_assert(ourImage->buf==NULL,NULL);
    ph_assert(index>=0 && index<nrImages,NULL);
    free(ourImage->filename);

    //remove our image
    memmove(images+index,images+index+1,(nrImages-index-1)*sizeof(struct ps_img_prefetch));
    nrImages--;
    if(nrImages)
        images=ph_realloc(images,nrImages*sizeof(struct ps_img_prefetch));
    else {
        //if we do not do this trick, the ph_realloc will "fail" when allocating 0 bytes
        free(images);
        images=NULL;
    }
    LOG0("####>>>> Remaining images in prefetch DB: %d",nrImages);
}

static char *doCreateImage(const char *path, size_t *bufLen, uint8_t scaleNum, uint32_t originalFileSize){
    char *buf=NULL;
    LOG0("####>>>> doCreateImage++ (%s)",path);
    unsigned int startTime=getRelativeTimeMs();

    //open the file
    int l,fd=open(path,O_RDONLY);
    LOG0return(fd==-1,NULL,"####>>>> WARNING: Unable to open image file (%s).",path);

    //allocate and fill in the buffer
    buf=(char*)ph_malloc(originalFileSize);
    //read the file into the buffer
    l=read(fd,buf,originalFileSize);
    close(fd);
    LOG0close_return(l<0,NULL,free(buf);buf=NULL;*bufLen=0,"####>>>> Error reading image file (%s). Error: %s",path,strerror(errno));
    LOG0close_return(l>=0 && l!=originalFileSize,NULL,free(buf);buf=NULL;*bufLen=0,"####>>>> WARNING: Image file (%s) partially read. Returning NULL.",path);

    unsigned int readEndTime=getRelativeTimeMs();
    if(scaleNum!=8){
        char *buf2;
        //if(scaleNum<5)scaleNum=5;
        if(scaleNum<2)//for scaleNum==1 it is not worth having multithreading
            buf2=scaleJpeg1Thr(buf,l,bufLen,scaleNum,originalFileSize);
        else
            buf2=scaleJpeg2Thr(buf,l,bufLen,scaleNum,originalFileSize);
        unsigned int resizeTime=getRelativeTimeMs()-readEndTime;
        if(buf2){
            free(buf);
            //rescaling succeeded
            LOG0("####>>>> doCreateImage-- (image was read in %ums and rescaled in %ums, size=%u)",readEndTime-startTime,resizeTime,(unsigned int)*bufLen);

            //unsigned int resizedVsEstimate=(*bufLen)*100/(unsigned long int)targetSize;
            //LOG0c(resizedVsEstimate<50 || resizedVsEstimate>200,"WARNING: estimate was too far away from the actual result: %u%%",resizedVsEstimate);
            //LOG0("####>>>> doCreateImage-- (image was read in %ums and rescaled in %ums, size=%u (%u%% from estimate))",readEndTime-startTime,resizeTime,(unsigned int)*bufLen,resizedVsEstimate);
            return buf2;
        } else LOG0("####>>>> WARNING: UNSUCCESSFUL image resize took %ums",resizeTime);
    }
    ph_assert(buf,NULL);

    //if we are here, resizing either failed or there was no need for resizing.
    *bufLen=originalFileSize;//assume everything went ok
    LOG0("####>>>> doCreateImage-- (image was buffered from file. read took %ums)",readEndTime-startTime);
    return buf;
}

void *prefetchThread(void *data){
    //first, make a duplicate of our data
    struct ps_img_prefetch *threadData=(struct ps_img_prefetch *)data;
    ph_assert(threadData->processingStatus==1,NULL);
    ph_assert(threadData->buf==NULL,NULL);

    //scale/read the image
    //LOG0("####>>>> prefetchThread++ (%s scale:%d/8)",threadData->filename,(int)threadData->scaleNum);
    threadData->buf=doCreateImage(threadData->filename,&threadData->bufLen,threadData->scaleNum,threadData->fileSize);

    //find our entry and update it, then free and exit
    pthread_mutex_lock(&imagesMutex);
    struct ps_img_prefetch *ourImage=NULL;
    int i,r;
    //uint8_t waitStatus=0;
    for(i=0;i<nrImages;i++)
        if(threadData->scaleNum==images[i].scaleNum && !strcmp(threadData->filename,images[i].filename)){
            //we found our image
            ourImage=&images[i];
            ph_assert(ourImage->processingStatus==1,"%s processingStatus=%d",ourImage->filename,(int)ourImage->processingStatus);
            ph_assert(ourImage->buf==NULL,NULL);
            ph_assert(ourImage->threadId==pthread_self(),NULL);
            //update data
            if(threadData->buf)ourImage->processingStatus=2; //success
            else ourImage->processingStatus=3; //failure
            ourImage->buf=threadData->buf;
            ourImage->bufLen=threadData->bufLen;
            if(ourImage->waitingStatus==0)//we detach
                pthread_detach(ourImage->threadId);
            //
            threadData->buf=NULL;
            threadData->bufLen=0;
            break;
        }
    nrThreadsInUse--;
    ph_assert(nrThreadsInUse>=0,NULL);
    pthread_mutex_unlock(&imagesMutex);
    //free our data and exit
    //LOG0("####>>>> prefetchThread-- (%s scale=%d/8)",threadData->filename,(int)threadData->scaleNum);
    free(threadData->filename);
    LOG0c_do(threadData->buf,free(threadData->buf),"####>>>> INFO: prefetchThread: freeing the image we just processed because it was canceled.");
    free(threadData);

    //check if there are images waiting for free threads
    pthread_mutex_lock(&imagesMutex);
    for(i=0;i<nrImages;i++)
        if(images[i].processingStatus==0){
            //we found an image waiting to be processed. Launch a new thread
            ourImage=&images[i];
            ourImage->processingStatus=1;
            //create the data that will be sent to the thread
            ph_assert(!ourImage->buf,NULL);
            threadData=(struct ps_img_prefetch *)ph_malloc(sizeof(struct ps_img_prefetch));
            *threadData=*ourImage;
            threadData->filename=ph_strdup(ourImage->filename);
            //create the thread
            r=pthread_create(&ourImage->threadId,NULL,&prefetchThread,threadData);
            LOG0close_return(r,NULL,free(threadData->filename);free(threadData);pthread_mutex_unlock(&imagesMutex),
                    "####>>>> ERROR (%d) creating a thread for prefetching an image",r);
            nrThreadsInUse++;
            break;
        }
    pthread_mutex_unlock(&imagesMutex);


    return NULL;
}

int prefetchImage(struct md_entry_id const * const id, const int priority, struct ps_session const * const mySession){
    char path[PATH_MAX];
    struct md_album_entry const *e;
    struct mdx_album_entry const *ex;
    int i,r=getEntryAndPath(id,KPicturesFolder,path,NULL,&e,&ex);
    //we MUST call releaseMdPics(NULL) before returning from this function
    LOG0close_return(!e,-1,releaseMdPics(NULL),"prefetchImage: WARNING: requested image not found.");
    LOG0close_return(e->flags&FlagIsAlbum,-1,releaseMdPics(NULL),"prefetchImage: WARNING: requested entry is an album, not an image!");
    LOG0close_return(e->flags&FlagIsVideo,-1,releaseMdPics(NULL),"prefetchImage: WARNING: requested entry is a video, not an image!");

    //LOG0("####>>>> prefetchImage++ (%s)",path);

    //get the file size & dimensions
    uint32_t targetSize,fileSize=e->size;
    uint16_t imgWidth=ex->width,imgHeight=ex->height;
    ph_assert(!(e->flags&FlagSizeIsInMB),NULL);
    releaseMdPics(NULL); //locked inside getEntryAndMD5
    e=NULL;
    ex=NULL;

    //ph_assert(displayWidth>=0 && displayWidth<=MAX_SCREEN_ACCEPTED_WIDTH,NULL);
    //ph_assert(displayHeight>=0 && displayHeight<=MAX_SCREEN_ACCEPTED_WIDTH,NULL);
    ph_assert(mySession->averageViewTime>=MIN_AVERAGE_VIEW_TIME,NULL);
    ph_assert(mySession->averageViewTime<=MAX_AVERAGE_VIEW_TIME,NULL);

    uint8_t scaleFactorNum;
    computeTargetImgParameters(fileSize,imgWidth,imgHeight,0,0,mySession,&targetSize,&scaleFactorNum);

    //do we already have this image prefetched or prefetching?
    struct ps_img_prefetch *ourImage=NULL;
    pthread_mutex_lock(&imagesMutex);
    for(i=0;i<nrImages;i++)
        if(scaleFactorNum==images[i].scaleNum && !strcmp(path,images[i].filename)){
            //we found our image
            ourImage=&images[i];
            ph_assert(ourImage->fileSize==fileSize,NULL);
            break;
        }
    if(!ourImage){
        //we have to add our archive
        //but first, check if we have too many
        if(nrImages>MAX_PREFETCHED_IMAGES){
            LOG0("####>>>> prefetchImage: we have too many prefetched images in the buffers (%d). Remove some (%d)",nrImages,nrImages-MAX_PREFETCHED_IMAGES);
            while(nrImages>MAX_PREFETCHED_IMAGES){
                free(images[0].buf);
                images[0].buf=NULL;
                removeImage(0,images);
            }
        }

        //adding to archive
        images=ph_realloc(images,(nrImages+1)*sizeof(struct ps_img_prefetch));
        i=nrImages;//used as an index, below
        ourImage=&images[nrImages++];
        ourImage->processingStatus=0;
        ourImage->waitingStatus=0;
        ourImage->filename=ph_strdup(path);
        ourImage->buf=NULL;
        ourImage->bufLen=0;
        ourImage->fileSize=fileSize;
        //ourImage->targetSize=targetSize;
        ourImage->scaleNum=scaleFactorNum;
    }
    ourImage->priority=priority;


    //do we have free thread slots and a waiting status?
    if(!ourImage->processingStatus && nrThreadsInUse<nrCores-1){
        //yes, we do. Launch a new thread
        ourImage->processingStatus=1;
        //create the data that will be sent to the thread
        ph_assert(!ourImage->buf,NULL);
        struct ps_img_prefetch *threadData=(struct ps_img_prefetch *)ph_malloc(sizeof(struct ps_img_prefetch));
        *threadData=*ourImage;
        threadData->filename=ph_strdup(ourImage->filename);
        //create the thread
        r=pthread_create(&ourImage->threadId,NULL,&prefetchThread,threadData);
        LOG0close_return(r,-1,removeImage(i,ourImage);free(threadData->filename);free(threadData);pthread_mutex_unlock(&imagesMutex),
                "####>>>> ERROR (%d) creating a thread for prefetching an image",r);
        nrThreadsInUse++;
    }

    pthread_mutex_unlock(&imagesMutex);
    //LOG0("####>>>> prefetchImage-- (%s)",path);
    return 0;
}


char *getPrefetchedImage(const char *path, size_t *bufLen, uint8_t scaleNum, uint32_t originalFileSize){
    //first, check if we find this image in the "database"
    LOG0("####>>>> getPrefetchedImage++ (%s, scale=%d/8)",path,(int)scaleNum);
    int i,createOrWait=1; //1=create, 2=wait, 0 means neither. We need 0 for the case when creation failed and we need to wait for the thread to join
                        //by default, if we do not find it, we create it
    char *buf=NULL;
    pthread_t threadToWait;
    struct ps_img_prefetch *ourImage=NULL;
    *bufLen=0;//just in case
    pthread_mutex_lock(&imagesMutex);
    for(i=0;i<nrImages;i++){
        LOG0("getPrefetchedImage: testing against %s, scale=%d/8",images[i].filename,images[i].scaleNum);
        if(scaleNum==images[i].scaleNum && !strcmp(path,images[i].filename)){
            //we found our archive
            ourImage=&images[i];
            ph_assert(originalFileSize==ourImage->fileSize,"%s ()%d/8: %u %u",ourImage->filename,ourImage->scaleNum,ourImage->fileSize,originalFileSize);
            //check the status: (0=waiting (for a thread to become free), 1=being created, 2=ready, 3=creation failed)
            if(!ourImage->processingStatus){
                //image still waiting for a thread to become available.
                //Remove the image from the database and create it ourselves in this thread
                ph_assert(!ourImage->buf,NULL);
                createOrWait=1;
                removeImage(i,ourImage);
                ourImage=NULL;
            } else if(ourImage->processingStatus==1){
                //image is being created. Announce we will wait for it, so that the thread does not detach itself
                ourImage->waitingStatus=1;
                createOrWait=2;
                threadToWait=ourImage->threadId;
            } else {
                //the image is ready. Get it and remove the data.
                createOrWait=0; //neither create or wait, we just get it
                buf=ourImage->buf;
                ourImage->buf=NULL;
                *bufLen=ourImage->bufLen;
                removeImage(i,ourImage);
                ourImage=NULL;
            }
            break;
        }
    }
    pthread_mutex_unlock(&imagesMutex);
    ourImage=NULL; //it may be invalidated after we lose the lock

    if(createOrWait==1){
        //create our image here
        unsigned int creationTime=LOG0("####>>>> getPrefetchedImage: WARNING: we have to create our image on the spot!");
        buf=doCreateImage(path,bufLen,scaleNum,originalFileSize);
        creationTime=getRelativeTimeMs()-creationTime;
        LOG0("####>>>> getPrefetchedImage: image was ready in %ums",creationTime);
    } else if(createOrWait==2){
        unsigned int creationTime=LOG0("####>>>> getPrefetchedImage: WARNING: we have to wait for the prefetch thread!");
        //wait for the thread
        pthread_join(threadToWait,NULL); //TODO: can it be that there are several thread waiting for the same thread? I guess it can be
        pthread_mutex_lock(&imagesMutex);
        //find our image again
        for(i=0;i<nrImages;i++)
            if(scaleNum==images[i].scaleNum && !strcmp(path,images[i].filename)){
                //we found our archive
                ourImage=&images[i];
                ph_assert(originalFileSize==ourImage->fileSize,"%u %u",originalFileSize,ourImage->fileSize);
                ph_assert(threadToWait==ourImage->threadId,NULL);
                buf=ourImage->buf;
                ourImage->buf=NULL;
                *bufLen=ourImage->bufLen;
                removeImage(i,ourImage);
                break;
            }
        pthread_mutex_unlock(&imagesMutex);
        if(ourImage){
            creationTime=getRelativeTimeMs()-creationTime;
            LOG0("####>>>> getPrefetchedImage: we had to wait for %ums",creationTime);
        } else
            LOG0("####>>>> getPrefetchedImage: WARNING: image not found after thread joined.");
    }

    //LOG0("####>>>> getPrefetchedImage--");
    return buf;
}

void cancelPrefetchedImage(const char *filename, uint8_t scaleNum){
    //LOG0("####>>>> cancelPrefetchedImage++ (%s scale=%d/8)",filename,(int)scaleNum);
    //find the image and remove it, unless there is a thread waiting for it
    int i;
    struct ps_img_prefetch *ourImage=NULL;
    pthread_mutex_lock(&imagesMutex);
    for(i=0;i<nrImages;i++)
        if(scaleNum==images[i].scaleNum && !strcmp(filename,images[i].filename)){
            //we found our archive
            ourImage=&images[i];
            if(ourImage->waitingStatus)
                break;
            if(ourImage->buf){
                free(ourImage->buf);
                ourImage->buf=NULL;
            }
            removeImage(i,ourImage);
        }
    pthread_mutex_unlock(&imagesMutex);
    //LOG0("####>>>> cancelPrefetchedImage-- (%s)",filename);
}


void clearPrefetchedImagesInAlbum(const char *path){
    //TODO

}

void getTranscodedPathForMedia(char const * const path, char const * const newExtension, char * const pathTrans){
    memcpy(pathTrans,path,strlen(path)+1);
    //try replacing the pictures folder with transcoded folder
    char *start=strstr(pathTrans,KPicturesFolder);
    ph_assert(start,NULL);
    int l=strlen(KTranscodedFolder)-strlen(KPicturesFolder);
    ph_assert(l>=0,NULL);
    //shift what is after start with l
    memmove(start+l,start,strlen(start)+1);
    memcpy(start,KTranscodedFolder,strlen(KTranscodedFolder));
    if(newExtension){
        char *dot=strrchr(pathTrans,'.');
        if(dot)strcpy(dot+1,newExtension);
    }
}
