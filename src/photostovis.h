#ifndef PHOTOSTOVIS_H
#define PHOTOSTOVIS_H

#include <stdio.h>
#include <stdint.h>
#include <pthread.h>

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

//No need to change the version anywhere, anymore
#define KPhotostovisVersionMajor 3
#define KPhotostovisVersionMinor 29
#define KPhotostovisVersionDate "2023-04-08"

//outside this interval we wait 2 minutes for the system time to change
#define KMinTime 1609372800 //2020-12-31 00:00:00
#define KMaxTime 1703980800 //2023-12-31 00:00:00

#define CURRENT_THUMBNAILS_FILE_VERSION 3
#define CURRENT_ALBUMDAT_FILE_VERSION 6
#define CURRENT_CAMERAS_FILE_VERSION 2
#define CURRENT_CONFIG_FILE_VERSION 1

#ifdef NDEBUG
#define KPhotostovisServerString "Photostovis/" STR(KPhotostovisVersionMajor) "." STR(KPhotostovisVersionMinor) " from " STR(KPhotostovisVersionDate)
#define KSrvDefaultPort 17424
#define KSrvDefaultHttpsPort 17434
#else
#define KPhotostovisServerString "Photostovis/" STR(KPhotostovisVersionMajor) "." STR(KPhotostovisVersionMinor) " from " STR(KPhotostovisVersionDate) " Debug"
#define KSrvDefaultPort 17464
#define KSrvDefaultHttpsPort 17474

//#define DEBUG_PICS_FOLDER
#define DEBUG_THUMBNAILS_PROCESSING_TIME

#endif

#define DEFAULT_SSH_PORT_INT 22 //the default port on which the ssh command runs on this machine.
#define DEFAULT_SSH_PORT_EXT 17422 //the default port that is exposed to outside, through the firewall

#define DEFAULT_PERMISSIONS_ANONYMOUS 0b10101010 //can do everything from inside the LAN, and nothing from outside the LAN
#define DEFAULT_PERMISSIONS_AUTHWITHKEY 0b11000000 //can only view pictures, from inside and outside the LAN
#define DEFAULT_PERMISSIONS_USER 0b11111111 //by default, if no permissions are given/configured by the user through the UI

//#define WEBSOCKET_SCAN_SEND_STEP 1000 //we send feedback information roughly 1000 times during one scan

#define USE_HTTP_WITH_PHONET 0 //0: NO, 1=fallback to http if https fails, 2=use http directly (do not try https)
#define RESIZE_LAN_LIMIT 3*1048576 //in MB
#define RESIZE_WAN_LIMIT 1*1048576 //in MB
#define NR_PARALLEL_SESSIONS 5 //for how many connections we allocate space to calculate the bitrate, etc
//#define NR_BW_ITEMS 50 //how many items we keep in order to calculate the bitrate
#define MIN_DATA_TRANSFERED_FOR_BITRATE_CALCULATION 131072 //1048576/8: for 1Mbit/s this many bytes are transfered in 1 second

#define MAX_WEB_THREADS 50 //how many parallel web threads we create (threads created when we accept a new connection)
#define MAX_THREADS_PER_SESSION 100 //max nr of threads for the same session. If this is bigger than MAX_WEB_THREADS, it will never matter
#define SLEEP_IF_REGISTRATION_FAILED 10 //the number of seconds to sleep before trying again if the server registration failed
#define SLEEP_IF_REGISTRATION_SUCCESSFUL 60000 //(in miliseconds) the number of seconds to sleep before checking the IP addresses for modifications and trying to register again if changes detected
#define NET_RESTART_AFTER 300 //in seconds: restart the networking if we do not have a successful registration after these many seconds
#define PHONET_ERRORS_MIN 10 //Errors returned by photostovis.net start from this value
#define PHONET_PING_INTERVAL 600 //ping photostovis.net every [this seconds] interval

#define SPLIT_LOG_TIME 3600 //in seconds: split log if more than SPLIT_LOG_SIZE (see below) and no activity in this interval
#define SPLIT_LOG_SIZE 1048576 //in bytes: split log if no activity (as above) and current log file larger than this ammount
#define VIDEO_BUF_LEN 1048576
#define ZIP_BUF_SIZE 1048576
#define MAX_ZIP_DOWNLOAD_SIZE_MB 5*1024 //5GB
#define ZIP_DOWNLOAD_TIMEOUT 30 //30 seconds should be sufficient for the client browser to initiate the download
#define ZIP_NOTIFY_EVERY_MB 10 //notify the client every 10MB with a websocket message

#define MAX_LENGTH_USERNAME 25 //same as in the database
#define BCRYPT_HASH_LENGTH 60
#define DATE_MAX 0x7FFFFFFF

#define MIN_AVERAGE_VIEW_TIME 1000
#define MAX_AVERAGE_VIEW_TIME 5000
#define MAX_ACCEPTABLE_RENDERING_TIME 300

#define MAX_PREFETCHED_IMAGES 12 // 6x2, enough for 2 simultaneous clients
#define NR_THUMBNAILS_PROCESSING_TIME_ALLOC_AT_ONCE 1000 //growing the array of thumbnail processed time by this ammount (should be 100-1000)

#define MIN_ROOT_MEMORY_BEFORE_CRITICAL 1024 //in MB: if the free space in the root (/) partition gets below this value, we report it
#define MIN_DATA_MEMORY_BEFORE_CRITICAL 1024 //in MB: same as above. If the free space in the data (usually /data) partition gets below this value, we report it
#define MIN_DATA_FREE_MEMORY_PERCENTAGE_BEFORE_CRITICAL 5 //if free memory gets below 5% in the data partition we report it

#define THUMBNAILS_CACHE_MAX_MB 200
#define THUMBNAILS_CACHE_NR_ENTRIES_PER_ALBUM 50

#ifdef __APPLE__
#define MAX_BITRATE 10485760 //10Gbps
#else
#define MAX_BITRATE 1048576 //1Gbps
#endif


#ifdef NDEBUG
#define BITRATE_LIMITED_TO 0 //For production this should always be zero
#else
#define BITRATE_LIMITED_TO 0 //in Mbps. For testing. For production this should always be zero
#endif


#define PHOTOSTOVIS_ETAGF_SIZE 41 //16+2+2 Bytes: 32+4+4 Bytes in HEX string + 1 Byte for \0
#define DEFAULT_HTTP_BUFFER_SIZE 16*1024+1
#define HTTP_RESPONSE_BUFFER_SIZE 1024 //our response should not be bigger than this

#define PHOTOSTOVIS_INOTIFY_WATCH_MASK IN_CLOSE_WRITE|IN_CREATE|IN_DELETE|IN_DELETE_SELF|IN_MOVE_SELF|IN_MOVED_FROM|IN_MOVED_TO

#if defined __WORDSIZE && __WORDSIZE == 64 && !defined __APPLE__
#define UINT64_FMT "lu"
#define UINT64X_FMT "lx"
#else
#define UINT64_FMT "llu"
#define UINT64X_FMT "llx"
#endif

#define KPhoNetIP "34.253.196.45" //photostovis.net expected address
#define KPhoOrgIP "34.243.74.72" //photostovis.org expected address

#define BYTETOBINARYPATTERN "%d%d%d%d%d%d%d%d"
#define BYTETOBINARY(byte)  \
  (byte & 0x80 ? 1 : 0), \
  (byte & 0x40 ? 1 : 0), \
  (byte & 0x20 ? 1 : 0), \
  (byte & 0x10 ? 1 : 0), \
  (byte & 0x08 ? 1 : 0), \
  (byte & 0x04 ? 1 : 0), \
  (byte & 0x02 ? 1 : 0), \
  (byte & 0x01 ? 1 : 0)

//Usage: printf ("Leading text "BYTETOBINARYPATTERN, BYTETOBINARY(byte));

//exit codes
#define EXIT_FOR_UPGRADE    1
#define EXIT_RESTART        2
#define EXIT_DO_NOT_RESTART 3
#define EXIT_SHUTDOWN       4

extern int shouldExit;
extern uint32_t myHardware;
extern uint32_t phonetIp;
extern uint32_t phoorgIp;
extern const int KWebsocketBufferOffset;
extern const char *activeNetworkInterface;

extern const char *KPicturesFolder;
extern const char *KUploadsFolder;
extern const char *KTranscodedFolder;
//extern const char *KDuplicatesFolder;
extern const char *KPhotostovisData;
//extern const char *KAlbumFilename;
//extern const char *KDescriptionFilename;
//extern const char *KThumbnailsFilename;
//extern const char *KConfigFilenameLocal;
extern const char *KConfigFilenameSystem;
extern const char *KPhotostovisDaemonName;
extern const char *KDefaultDocRootSystem;
extern const char *KDefaultDocRootLocal;

extern const char *KSSLExtraPath;
extern const char *KSSLCertificateFile;
extern const char *KSSLPrivateKeyFile;

/* these are only needed in websrv.c
extern const char *KJsonAnswerUnknown;
extern const char *KJsonAnswerTrue;
extern const char *KJsonAnswerFalse;
*/

extern const char *KJpegExtension;
extern const char *KMp4Extension;
extern const char *KM4vExtension;
extern const char *KMovExtension;

extern const char * const KOptionsRootFolder;

extern char const * KFFmpegBin;
extern char const * KFFprobeBin;

enum { //8bits
    DaemonFlagFlushLog=1 //flush log buffer after each message
};

enum {
    FlagIsAlbum=1,
    FlagIsVideo=2,
    FlagPhotoHasGPSInfo=4,
    FlagHasExifThumbnail=8,
    FlagRotatePlus90=0x10,       //16
    FlagRotatePlus180=0x20,      //32
    //FlagFullFileMD5=0x40,        //64
    FlagAlbumHasUndatedEntries=0x80, //128
    FlagSizeIsInMB=0x100,             //256
    FlagHasDescription=0x200,         //512
    FlagHasUserComment=0x400,         //1024
    FlagHasDescriptionTxtEntry=0x800, //2048
    FlagHasNameOffset=0x1000,         //4096
    //runtime flags
    FlagHasScaledThumbnail=0x2000,    //8192
    FlagHasCachedThumbnail=0x4000,    //16384
    FlagElmIsBeingScanned=0x8000,     //32768   (last bit)
    RuntimeFlags=0xE000
};

/* data field:
 * -if FlagHasDescriptionTxtEntry, next sizeof(void*)+1 bytes are the pointer in the description file of our entry (without the first line) and permissions byte
 * -if FlagHasNameOffset next byte is offset [0-~15]
 * -next comes the filename
 * -if FlagHasDescription, then we have description until \0
 * -if FlagHasUserComment, then we have user comment until \0
 */
#define getDataFromEntry(e) ((char*)e+sizeof(struct mdh_album_entry)+(e->flags&FlagIsAlbum?sizeof(struct mdh_album_entry_album):(e->flags&FlagIsVideo?sizeof(struct mdh_album_entry_video):sizeof(struct mdh_album_entry_picture))))

/*
#define getFilenameFromEntry(e) ((char*)e+sizeof(struct mdh_album_entry)+(e->flags&FlagIsAlbum?sizeof(struct mdh_album_entry_album):(e->flags&FlagIsVideo?sizeof(struct mdh_album_entry_video):sizeof(struct mdh_album_entry_picture)))+(e->flags&FlagHasDescriptionTxtEntry?sizeof(void*)+1:0)+(e->flags&FlagHasNameOffset?1:0))*/

#define getFilenameFromEntry(e) (getDataFromEntry(e)+(e->flags&FlagHasDescriptionTxtEntry?sizeof(void*)+1:0)+(e->flags&FlagHasNameOffset?1:0))

#define getNameFromAlbum(album,i) ((char*)album+((uint32_t)album[i].dataOffsetHi<<16)+album[i].dataOffsetLo)

#define getExtraEntry(album,n,i) (&(*(struct mdx_album_entry**)((struct md_album_entry*)album+n))[i])
#define getExtraEntryData(album,n,i) (char*)(*(struct mdx_album_entry**)((struct md_album_entry*)album+n))+(getExtraEntry(album,n,i))->stringsOffset


#define ASSERT_SESSION_LOCKED(s) if(!(s->flags&FlagSession_IsLocked))ph_assertion_failed(__FILE__,__LINE__,"Session is NOT locked!")


/**********************************************
 * Limitations:
 * -no more than 65535 entries in an album
 * -no more than 65535 cameras supported
 * -no more than 7 levels of embedded albums
 *********************************************/

struct mdh_album_entry {
    uint32_t size; //for albums: original size of all pictures, videos and other supported files from this album and all subalbums
    int32_t captureTime; //for albums: time of the earliest photo captured from this album
    int32_t modificationTime;
    uint32_t nameCRC;
    uint16_t flags;
    uint16_t entriesIndex; //for Photos & Videos: index in makeModel array, 0xFFFF if no info. For albums: nr of direct entries in the album
};//size: 16 bytes
struct mdh_album_entry_picture {
    float focalLength;  //4 bytes
    float exposureTime; //4 bytes
    float fNumber; //4 bytes
    int32_t lat;
    int32_t lng;
    uint16_t width;
    uint16_t height;
    uint16_t focalLength35;
    uint16_t iso;

    uint8_t exposureProgram;
    int8_t timezone; //TIMEZONE_UNAVAILABLE (127) is "unknown/unavailable"
}; //size: 30 bytes (aligned to 32?)
struct mdh_album_entry_video {
    uint32_t sizeLo;
    int32_t lat;
    int32_t lng;
    uint16_t width;
    uint16_t height;
    uint16_t duration; //max: 65535s -> approx 18h
    int8_t timezone; //TIMEZONE_UNAVAILABLE (127) is "unknown/unavailable"
};
struct mdh_album_entry_album {
    struct md_album_entry *mdAlbum;
    int32_t latestCaptureTime;
};

struct mdh_file_info {
    off_t size;
    time_t mtime;
};

/* structure for parsing description.txt files
 */
struct mdh_album_description {
    uint32_t nrEntries; //this could be uint16
    uint32_t dataLen;
    char *data;
    int32_t *entries; //the position of each entry. If negative, it has been "used"
};

//////////////////////////

enum { //8bits
    //first 4 bits: pixel ratio, at the moment 1 or 2
    FlagThumbnailETag_Scaled=0x10 //as opposed to "EXIF" thumbnail
};

//pictures, thumbnails & videos:
//eTag:  nameCRC+captureTime+size+modificationTime (16 Bytes) -> size is the size of the original picture, not the size of the thumbnail or the scaled picture
//eTagF: thumbnails: eTag+thumb_flags (16+1 Bytes), images: eTag+scaling num (values: 1-8, 16+1 bytes)
//albums:
//eTag:  nameCRC+earliestCaptureTime+latestCaptureTime+modificationTime (16 bytes)
//eTagF: eTag+flags+modificationCounter (16+2+2 bytes)
struct md_level_etag {
    uint32_t nameCRC;
    int32_t captureTime; //or earliestCaptureTime for albums
    int32_t sizeOrLatestCaptureTime; //for pics & videos: file size, in bytes. If flag FlagSizeIsInMB is set, size is in KB. For albums: latestCaptureTime
    int32_t modificationTime;
};
struct md_level_etagf {
    uint32_t nameCRC;
    int32_t captureTime; //or earliestCaptureTime for albums
    int32_t sizeOrLatestCaptureTime; //for pics & videos: file size, in bytes. If flag FlagSizeIsInMB is set, size is in KB. For albums: latestCaptureTime
    int32_t modificationTime;
    uint16_t modificationCounter;
    uint16_t flags;
};

struct md_entry_id {
    uint8_t reqPerm;
    uint8_t level;
    uint32_t nameCRC[7];
};

struct md_album_entry {
    uint16_t flags;
    uint16_t entriesIndex; //for Photos & Videos: index in makeModel array, 0xFFFF if no info. For albums: nr of direct entries in the album
    uint8_t  permissions;
    uint8_t  dataOffsetHi; //data offset relative to the beginning of the allocated structure
    uint16_t dataOffsetLo;
    uint32_t nameCRC;
    int32_t modificationTime; //file modification time (st_mtime)
    union {
        struct {
            //pictures & videos
            int32_t captureTime;
            uint32_t size; //file size, in bytes. If flag FlagSizeIsInMB is set, size is in MB. Lower bits are in the mdx structure
            int32_t lat;
            int32_t lng;
        };
        struct {
            //albums
            int32_t earliestCaptureTime;
            int32_t latestCaptureTime;
            struct md_album_entry *album;
        };
    };
};//sizeof: 16+8+8 (32)

struct mdx_album_entry {
    uint32_t stringsOffset; //0 if no strings
    union {
        struct {
            //common part for pictures and videos
            uint16_t width;
            uint16_t height;

            uint8_t exposureProgram; //we do not know this for videos, but it is here for alignmnent
            int8_t timezone; //TIMEZONE_UNAVAILABLE (127) is "unknown/unavailable"

            union {
                struct {
                    //pictures
                    float focalLength;  //4 bytes
                    float exposureTime; //4 bytes
                    float fNumber; //4 bytes

                    uint16_t focalLength35;
                    uint16_t iso;
                };
                struct {
                    //videos
                    uint32_t sizeLo; //If flag FlagSizeIsInMB is set, this is the least significant 20 bits
                    uint16_t duration;
                };
            };
        };//size: 22(+2 bytes alignment)
        struct {
            //albums
            uint16_t modificationCounter;
            uint8_t nameOffset;
        };
    };
};//size: 28

struct mdh_album_data_by_permission {
    uint64_t sizeOfPictrsInAlbum;
    uint64_t sizeOfVideosInAlbum;
    union {
        struct { //"regular" entries
            uint32_t nrEntries;
            int32_t earliestCaptureTime;
            int32_t latestCaptureTime;
        };
        struct { //"admin" entries
            uint32_t nrTotalUnknowns;
            uint32_t sizeOfUnknownsInAlbumHi;
            uint32_t sizeOfUnknownsInAlbumLo;
        };
    };
    uint32_t nrTotalPictrs;//number of all pictures in this album and all sub-albums
    uint32_t nrTotalVideos;//number of all videos in this album and all sub-albums
    uint32_t nrTotalAlbums;//number of all sub-albums
};//sizeof: 5*8=40 bytes

struct mdh_album_data_by_permission *getAlbumDataByPermission(struct md_album_entry const *parentEntry);

/* In data: the filename (length+1 (\0))
 * Use getNameFromAlbum(album,i) defined above to get the filename
*/

struct md_thmbfile_header {
    uint16_t version;
    uint16_t nrEntries;
};

// old version
struct md_thumbnails_v2 {
    uint32_t size; //size of the original image file for this thumbnail
    uint32_t offset; //offset of the thumbnail data (from the beginning of the file)
    uint16_t width; //original image width
    uint16_t height; //original image height
};

struct md_thumbnails {
    //first we have the picture etag fields
    struct md_level_etag etag;
    //offset
    uint32_t offset; //offset of the thumbnail data (from the beginning of the file)
};


struct md_albumdat_header {
    uint8_t fileVersion;
    uint8_t nrPermissions;
    uint16_t nrEntries;
    uint32_t lengthOfAlbumData; //album data starts immediately after this structure
    uint32_t lengthOfExtraData;
};


struct md_cameras_header {
    uint8_t fileVersion;
    uint8_t padding;
    uint16_t nrCameras;
    uint32_t fullMakeModelStringsLen;
    uint32_t shortenedMakeModelStringsLen;
}; //size: 12 bytes

//////////////////////////

/*
struct ps_websocket_packet {
    char *data;//owned
    uint32_t dataLen;
    uint32_t dataOffset;
};*/

enum { //ps_session flags
    FlagSession_FromSmallDevice=1, //client is a "small device"
    FlagSession_ComingFromLAN=2,
    FlagSession_IsLocked=0x80
};
struct ps_session {
    pthread_mutex_t sessionMutex;

    uint16_t nrConnectedThreads;
    uint16_t nrCamerasSent;
    int32_t lastSentPacketTime;

    int websocketPipe;

    //bandwidth items
    uint32_t bitrate; //kbits/s, uplink

    uint32_t uploadRecvStart;
    uint32_t uploadLastEnd; //the time when the last upload has ended
    uint64_t uploadBytesRecv;
    uint32_t uploadBitrate; //kbps, downlink

    uint32_t thumbsTransferStart;
    uint32_t thumbsBytesTransfering;
    uint32_t thumbsBytesTransfered;
    uint16_t thumbsNrTransfering;
    uint16_t thumbsNrTransfered;

    uint32_t averageViewTime; //only counted between 1s and 5s (<1 counted as 1, >5 counted as 5) - see constants above
    uint32_t imageTransferStart;
    uint32_t imageProcessingTime;
    uint64_t imageBytes;
    uint32_t imageNameCRC; //to have some way of verifying the image
    //end of bandwidth items
    uint32_t clientIp;
    uint64_t sessionId;
    //until here we have all fields nicely packed in 64bits groups

    struct md_entry_id lastRequestedAlbumID;
    struct md_level_etagf lastRequestedAlbumETag;
    // -> not needed char *lastRequestedAlbumKey; //owned

#ifdef DEBUG_THUMBNAILS_PROCESSING_TIME
    int16_t *thumbnailsProcessingTime; //owned
    uint32_t thumbnailsProcessingTime_allocatedSize; //in sizeof(int16_t)
#endif
    uint16_t averageRenderingTimePerMPixel;

    //uint16_t status; //TODO: not used yet

    uint8_t flags;
    uint8_t pixelRatio;
    uint8_t lastRequestedAlbumNrThumbnailsPerRow;
    uint8_t userIndex;
    uint16_t screenWidth;
    uint16_t screenHeight;
};

struct ps_camera {
    uint32_t fullMakeModelOffset;
    uint32_t nrPictures; //nr of pictures and videos taken with this camera
    float minFocalLength;
    uint16_t shortenedMakeModelOffset; //0xFFFF if none
    uint16_t modelOffset; //this can be uint8_t
};

struct ps_auth {
    char *username;
    char *password;
    uint8_t permissions;
};

struct ps_partition {
    char name[5];
    char type[10];
    char uuid[36+1];
    char *mountPoint;
};

struct ps_device {
    char *id;
    char *device;
    int nrPartitions;
    struct ps_partition *partitions;
};

enum {//request types. These values correspond to the number of bits we need to shift to get the proper permission bit.
    RequestView=6,
    RequestUpload=4,
    RequestShare=2,
    RequestAdmin=0
};
//metadata structures
struct ps_md_config { //TODO: this structure needs memory re-alignment, too many pointers
    char *srvDocRoot;
    char *phonetUsername;//user registration: login
    char *phonetPassword;   //user registration: password
    struct ps_auth *authUsers;
    struct ps_device *devices;
    char *rootFolder;
    char *salt;
    uint8_t nrAuthUsers;
    uint8_t nrDevices;
    uint8_t nrViewingPermissions;
    uint8_t cfgNumber; //usually zero, but can be >0 if there are several virtual servers
    uint8_t permissionsAnonymous;
    uint8_t permissionsAuthWithKey;
    uint8_t nrTotalConfigurations;
    uint16_t cfgFlags;
    uint16_t httpPort;
    uint16_t httpsPort;
    uint16_t sshPortInt;
    uint16_t sshPortExt;
    //next items are not really config items, but here because they have the same value for the entire duration of the process)
    int inotifyFd;
    union {
        struct {
            int httpSocket;
            int httpsSocket;
        };

        pid_t pidPhotostovis;
    };
    //
    unsigned int rootDataSizeMB,rootFreeSizeMB,dataDataSizeMB,dataFreeSizeMB;
    //unsigned int nrPictures,nrVideos,nrAlbums;
};
struct ps_md_config_received {
    char *phonetUsername;
    struct ps_auth *authUsers;
    uint16_t cfgFlags;
    uint8_t nrAuthUsers;
    uint8_t permissionsAnonymous;
    uint8_t permissionsAuthWithKey;
};

struct ps_md_pics {
    struct ps_camera *cameras;
    char *fullMakeModelStrings;
    char *shortenedMakeModelStrings;
    struct md_album_entry *rootAlbum;
    uint32_t fullMakeModelStringsLen;
    uint32_t shortenedMakeModelStringsLen;
    uint32_t totalEntries;
    uint32_t scannedEntries;
    uint16_t nrCameras;
    //uint16_t statusFlagsPics;
};
struct ps_md_net {
    struct ps_session sessions[NR_PARALLEL_SESSIONS];
    char *phonetCfgCurlErrorString;
    uint32_t lanIP;
    uint32_t externalIP;

    //status bytes
    uint8_t gatewayCfgStatus;
    uint8_t phonetCfgStatus;
    //uint16_t statusFlagsNet;
};

//!!! If any of these change, changes are needed in common.php
enum { //cfgFlags (16 bits)
    CfgFlagIsDaemon=0x01,
    CfgFlagConfigurationRequired=0x02,
    CfgFlagUPnPWorks=0x04,
    CfgFlagCertificateIsNeeded=0x08,

    //SrvFlagDNSNameMismatch=0x80, //placeholder: on phonet side this is set when the name does not match the IP address

    //SECOND BYTE
    //these are "enhancing" flags. Used for verifying the configuration
    CfgFlagLanAuthRequired=0x0100, //authentication (signing in) is required for LAN requests
    CfgFlagLanAuthHttpNotAllowed=0x0200,

    CfgFlagInetNotAllowed=0x0400, //Internet requests not allowed
    CfgFlagInetAuthNotRequired=0x0800, //Internet requests do not require authentication
    CfgFlagInetAuthHttpAllowed=0x1000, //Allow authentication over http. If not set, only authentication over https work
    CfgFlagInetAuthHttpAllowedAutoadded=0x2000, //this is set when the server itself added the above flag, and not the user. Do not save to config if this is set.
    CfgFlagShutdownOnCableOut=0x4000
};

enum { //statusFlags
    //pics (first byte)
    StatusFlagPictureScanThreadActive=1,
    StatusFlagDoARescan=2, //after the current picture scan the system should do a rescan because there were modifications meanwhile
    StatusFlagFirstScanDone=4,
    StatusFlagThumbnailScanActive=8,
    StatusFlagIgnoreFirstRescan=16, //if we just created a new folder

    //net (second byte)
    //StatusFlagGatewayCfgInProgress=0x100,
    //StatusFlagGatewayCfgDone=0x200,
    //StatusFlagPhonetCfgInProgress=0x400,
    //StatusFlagPhonetCfgDone=0x800,
    StatusFlagRegistrationThreadActive=0x1000
};

struct md_thmbstrip_entry {
    uint32_t nameCRC;
    uint16_t flags;
    uint16_t clientIndex;

    //uint16_t sizeInBytes; //should be less than 64k. There is no (easy) way to obtain this, the way the code is currently written

    //next fields are only used for albums (flags&FlagIsAlbum)
    uint16_t nrTotalThumbnails;
    uint16_t nrEntries;
    uint16_t currentIndex;


    struct md_thmbstrip_entry *entries;
};

enum md_scanning_status {
    StatusScanning_Done = 0,
    StatusScanning_Pictures = 1,
    StatusScanning_Thumbnails = 2,
    StatusScanning_ThumbnailsCache = 3
    //there will be some more
};

uint32_t ph_crc32b(char const * const message);
char *base64_encode(const unsigned char *data, size_t input_length, size_t *output_length);
void printIP(const uint32_t ip, char *ipStr);
uint32_t ipToUInt32(char const * const ip);
char *escape_str(char const * const src);

void trim_string(char **s);
void trim_string_right(char *s);
int dirService(const char *folder, int create);
int setOwnership(const char *folder, int recursive);
int changePictureName(const char *path);
int lookForPictures(int *forcePicturesFullRescan, struct md_entry_id *changedFolderId);
int checkAndCreateThumbnails();
void *scanPicturesThread(void *ptr);
int moveUploadedContent(char *strAlbum);
int moveTranscodedFiles();
int deleteMedia(struct md_entry_id *mediaId);
int createNewSubalbum(char *strAlbum, char *strSubAlbum, uint32_t *subAlbumNameCRC);
int cancelUploadedContent();
void sendStatusToConnectedClients(struct md_entry_id const * const __restrict impactedAlbum);
void sendScanningStatusToConnectedClients(enum md_scanning_status status);
void freeAlbums(struct md_album_entry **album, uint16_t *nrEntries);
void printAlbums(struct md_album_entry *album, int nrEntries, int level);

int getEntryAndPath(struct md_entry_id const * const id, const char *KFolder, char *path, char const **name,
                    struct md_album_entry const **e, struct mdx_album_entry const **ex);

int hasAlbumDescription(const char *path);
struct mdh_album_description *getAlbumDescription(const char *path);
void setEntryDescription(struct mdh_album_entry *entry, struct mdh_album_description *albumDescr);
void checkAndFreeAlbumEntries(struct mdh_album_description *albumDescr);
void writeImageDescriptionAndTitle(FILE *f, struct mdh_album_entry const * const entry, char const * const filename, char const * const extraStr);
void freeAlbumDescriptionStruct(struct mdh_album_description *albumDescr);

char *getAlbumJsonHttp(struct md_entry_id const * const reqId, const char *reqKey, const char *reqETag, const int reqNrThumbnails, const int pixelRatio,
                       const int bufferOffset, char *eTag, int eTagSize, struct md_level_etagf * const albumETag, size_t *bufLen);
char *getImageHttp    (struct md_entry_id const * const id, size_t *bufLen, char *eTag, int eTagSize, const char *reqETag, int requestedWidth, int requestedHeight,
                       struct ps_session const * const mySession);
/*char *getThumbnailStripJsonHttp(struct md_entry_id const * const id, size_t *bufLen,
                                char *eTag, int eTagSize, const char *reqETag, int nrThumbnails, const char *reqUrl);*/
char *getAlbumOgProperties(struct md_entry_id const * const id, const char *reqKey, size_t *bufLen, const char *reqUrl);
char *getImageOgProperties(struct md_entry_id const * const id, const char *reqKey, size_t *bufLen, const char *reqUrl);

int getZipDownloadInfo(struct md_entry_id const * const id, uint64_t *contentSize, unsigned int *nrPics, unsigned int *nrVideos, unsigned int *nrAlbums);
int getZipDownload(struct md_entry_id const * const id, const int httpKeepAlive, const char *dateStr, int socket, void *ssl);
int prepareZipFile(struct md_entry_id const * const id, const uint8_t visibleLayer, uint64_t contentSize, struct ps_session * const mySession);
void deleteAllZipArchives();

int thmbManagerInit();
int thmbManagerClose();
int getThumbnailUrlFromCache(const uint32_t nameCRC, struct md_thmbstrip_entry *tse, int permissionIndex, const int reqNrThumbnails);
char *getThumbnailFromCache(struct md_entry_id const * const id, const int pixelRatio, size_t *bufLen, int *returnedDataShouldBeFreed,
                            char *eTag, int eTagSize, const char *reqETag, int * const isVideo);
//int cacheThumbnails(struct md_entry_id const * const id, struct md_thmbstrip_entry const * const tse, const int pixelRatio, const int keepInCache);

int createWebsocketTextPacket(char * const buffer, uint32_t * const bufferLen, uint32_t * const bufferOffset);
int enqueueWebsocketPacket(struct ps_session *s, char * const buffer, uint32_t bufferLen, uint32_t bufferOffset,
                           const int lockSession, const int createWebsocketPacket);
//int srvWriteWebsocket(struct ps_session * const s, void const * const  __restrict buf, const int bufLen);
int srvWriteGeneric(int c, void *ssl, void const * const  __restrict buf, const int bufLen, const int isWebsocket);

struct ps_session *getSession(uint64_t sessionId, uint32_t clientIp, int expectedToFind);

/* JSON format:
 * {"a":{...album_data...},"e":[{...entry_data...},{...entry_data...}, ... ]}
 * album_data: {
 *      "n":nrEntriesInAlbum,
 *      "p":nrPicturesInAlbumAndSubalbums,
 *      "a":nrSubalbums,
 *      "u":nrUnknownsInAlbumAndSubalbums,
 *      "ec":earliestCaptureTime_as_int32,
 *      "lc":latestCaptureTime_as_int32,
 *      "sp":sizeOfAllPicturesInAlbumAndSubalbums,
 *      "su":sizeOfAllUnknownsInAlbumAndSubalbums
 * }
 * entry_data: { //for pictures
 *      "c":captureTimeAs_int32,
 *      "fn":"filename",
 *      "fl":flags,
 *      "lat":latitude,
 *      "lng":longitude,
 *      "m": makeModelIndex,
 *      "d":"description",
 *      "uc":"user comments",
 *      "sz":size_in_bytes,
 *
 *      "tz":timezone,
 *      "no":name offset
 * }
 *
 * entry_data: { //for albums
 *      -this is the same as album_data above-
 * }
 */

enum ClientCommands {
    CCGetAlbum='a',          //gets the album.json file for an album
    CCSetAlbum='e',          //sets the current album (notifies the server that the current album has changed)
    CCGetThumbnail='t',      //gets the thumbnail for a picture or album
    CCGetImage='i',          //gets a picture/image
    CCGetVideo='v',          //gets a video
    CCGetSrvMetadata='m',    //POST: gets server metadata
    CCConfigure='c',         //the client sent configuration data
    CCPing='p',              //photostovis.net pings the server. The server should answer with the phonet username (or empty string)
    CCFacebook='f',          //photostovis.net asks the server to provide open graph info about a page
    CCShutdown='z',          //the client asks the server to shutdown itself
    CCUpload='u',            //the client uploads a file to the server
    CCCancelUpload='x',      //the client cancels all uploaded files that are not committed
    CCGetUrlToAlbum='k',     //gets a URL to specified album
    CCDownloadAsZip='d',     //download album as zip
    CCDownladAsZipInfo='n',  //download the JSON info: number of pics & vids, the album size
    CCDelete='b',
    //for debugging purpose
    CCGetLog='l'             //retrieves the current log file
};

enum ClientErrors {
    CEThumbNotFound=-1
};


//jpegtransform
char *scaleJpeg1Thr(char *srcBuf, size_t srcBufLen, size_t *bufLen, uint8_t scaleNum, uint32_t originalFileSize);
char *scaleJpeg2Thr(char *srcBuf, size_t srcBufLen, size_t *bufLen, uint8_t scaleNum, uint32_t originalFileSize);
int createJpegThumbnail(const char *path, FILE *thumbnailFile1, FILE *thumbnailFile2, unsigned int *resizeTimeMS, unsigned int *resizeTimeMSperMpixel);
int createVideoThumbnail(const char *path, FILE *thumbnailFile1, FILE *thumbnailFile2, unsigned int *resizeTimeMS, unsigned int *resizeTimeMSperMpixel);
int getJpegDimensions(const char *path, uint16_t *width, uint16_t *height);

//files, image and thumbnail managers
void imgManagerInit();
void imgManagerClose();
void computeTargetImgParameters(const uint32_t fileSize, uint16_t imgWidth, uint16_t imgHeight, int requestedWidth, int requestedHeight,
                                struct ps_session const * const mySession, uint32_t * const targetSize, uint8_t * const scaleFactorNum);
int prefetchImage(struct md_entry_id const * const id, const int priority, struct ps_session const * const mySession);
void cancelPrefetchedImage(const char *filename, uint8_t scaleNum);
char *getPrefetchedImage(const char *path, size_t *bufLen, uint8_t scaleNum, uint32_t originalFileSize);
void getTranscodedPathForMedia(char const * const path, char const * const newExtension, char * const pathTrans);

enum FileManagerFileFlags {
    FlagIsGZipped=1,
    FlagFreeData=2
};

struct ps_fm_file {
    char *buf;
    int bufLen;
    int flags;
    char eTag[17]; //16+1 (\0)
};
void fileManagerInit();
void fileManagerClose();
//freeBuffer-> if 1, the calling functuon should free the returned buffer.
int getFile(const char *url, const int urlLen, const char *reqETag, const int acceptsGzip, struct ps_fm_file * const fmFile);
int CreateIndexAllHtmlGz(void);
//utils: semaphores

#ifdef __APPLE__
typedef struct {
    int v;
    pthread_mutex_t m;
    pthread_cond_t c;
} ph_sem_t;
int ph_sem_init(ph_sem_t *s, const int pshared, const int v);
int ph_sem_destroy(ph_sem_t *s);
int ph_sem_wait(ph_sem_t *s);
int ph_sem_post(ph_sem_t *s);
int ph_sem_getvalue(ph_sem_t *s, int *v);
#else
#define ph_sem_t sem_t
#define ph_sem_init(s,share,v) sem_init(s,share,v)
#define ph_sem_destroy(s) sem_destroy(s)
#define ph_sem_wait(s) sem_wait(s)
#define ph_sem_post(s) sem_post(s)
#define ph_sem_getvalue(s,v) sem_getvalue(s,v)
#endif



//leds (HW dependent: only works on Cubietruck)
//Flash 10%: Cannot connect to photostovis.net
//Flash 50%: Scanning in progress
//Flash 90%: Unreachable from internet
enum LedCommands {
    LCOff=0,
    LCOn=1,
    LCFlash10PercentOn=2,
    LCFlash10PercentOff=3,
    LCFlash50PercentOn=4,
    LCFlash50PercentOff=5,
    LCFlash90PercentOn=6,
    LCFlash90PercentOff=7
};
int ledCommand(enum LedCommands lc, int force);
int ledClientActivity(enum LedCommands lc, int force);

//main process
int photostovis(struct ps_md_config *config);

//web server functions
int httpStart(uint16_t port);
//int httpAcceptConnection();

int httpsStart(uint16_t port);
int srvAcceptConnection(int isSSL);

int srvStop(); //stops both
//
//void saveSettingsFile();
const char *verifyUserSubmittedConfig(struct ps_md_config_received * const __restrict cfg);
int saveToConfigurationFile(struct ps_md_config_received const * const __restrict cfg);

//mdAll functions
void initMd(struct ps_md_config *config);
void destroyMd();
void resetMdPics();

struct ps_md_config const * const getMdConfigRd();
struct ps_md_config * const getMdConfigWritable();
void releaseMdConfig(struct ps_md_config const * const *ptr);
void releaseMdConfigW(struct ps_md_config **ptr);

struct ps_md_pics const * const getMdPicsRd();
struct ps_md_pics * const getMdPicsWritable();
void releaseMdPics(struct ps_md_pics const * const *ptr);
void releaseMdPicsW(struct ps_md_pics **ptr);

struct ps_md_net const * const getMdNetRd();
struct ps_md_net * const getMdNetWritable();
void releaseMdNet(struct ps_md_net const * const *ptr);
void releaseMdNetW(struct ps_md_net **ptr);

void statusFlagsAdd(uint32_t f);
void statusFlagsRemove(uint32_t f);
int statusFlagsCheck(uint32_t f);

//
void newAlbumRequested(struct ps_session *session, struct md_entry_id const * const id, const int nrThumbnails,
                       struct md_level_etagf const * const lastRequestedAlbumETag);
uint32_t addThumbTransferStart(struct ps_session *session, uint32_t dataSize, uint32_t *transferStart);
void thumbTransferFailed(struct ps_session *session, uint32_t dataSize, uint32_t transferStart);
void thumbTransferSucceeded(struct ps_session *session, uint32_t dataSize, uint32_t transferStart, uint32_t processingTime);
uint32_t thumbnailsTransferEnded(struct ps_session *session);

void addImageTransferStart(struct ps_session *session, uint64_t dataSize, uint32_t processingTime, uint32_t nameCRC, uint32_t *transferStart);
void imageTransferFailed(struct ps_session *session, uint64_t dataSize, uint32_t processingTime, uint32_t nameCRC, uint32_t transferStart);
uint32_t imageTransferConfirmed(struct ps_session *session, int transferTime, uint32_t nameCRC);

uint32_t addUploadedData(struct ps_session *session, const uint32_t startTime, const uint32_t endTime, const uint64_t dataSize, const int finished);

//bcrypt
int bcrypt_checkpass(const char *pass, const char *goodhash);

#ifndef CONFIG_NO_PHONET
//phonet
enum PhonetCfgStatus {
    PhonetCfgSuccess=0,
    PhonetCfgErrCurlInitFailed=1,
    PhonetCfgErrCurlConnectionFailed=2,
    PhonetCfgErrAnswerParsingFailed=3,
    PhonetCfgErrAnswerOutsideRange=4,
    PhonetCfgNeverTriedToRegister=0xFF
};
//const long int phonetTimeoutInSeconds=60; //this is defined in phonet.c
//#define PHONET_ERRORS_MIN 10 //Errors returned by photostovis.net start from this value

//enum PhonetCfgStatus phonetRequest(const char *url, const char *params, char **answer, size_t *answerLen);
int phonetServerStart();
void phonetServerStop();
int phonetServerRegister(const char *servername, const char *password);
int checkCable();

//we need these here because we use them in several places
enum PhonetCfgStatus phonetRequestString(const char *baseUrl, char *url, char **string);
enum PhonetCfgStatus phonetRequestFile(const char *baseUrl, char *url, FILE *f);
int phonetSendLog(const char *baseUrl, const char *logText, int logTextLen);
void getUniqueID(char md5str[33], uint8_t cfgNumber);
int updateSizes(unsigned *rootDataSizeMB, unsigned *rootFreeSizeMB, unsigned *dataDataSizeMB, unsigned *dataFreeSizeMB);

//upnp igd
enum GatewayCfgStatus {
    GatewayCfgSuccess=0,
    GatewayCfgErrUpnpDiscover=1,
    GatewayCfgErrUpnpNoIGD=2,
    GatewayCfgErrUpnpGetDescriptionFailed=3,
    GatewayCfgErrCannotGetLanIP=4,
    GatewayCfgErrAddPortMappingFailed=5,
    GatewayCfgErrGetSpecificPortMappingFailed=6,
    GatewayCfgNeverTriedToConfigure=0xFF
};
enum GatewayCfgStatus configureGateway(int httpPort, int httpsPort, int unregisterHttpxPorts, int sshPortInt, int sshPortExt,
                                       uint32_t *lanIP, uint32_t *externalIP, int *upnpWorks);
enum GatewayCfgStatus closeGateway(int httpPort, int httpsPort, uint32_t lanIP);

void setUpgradeCheckAlarm(int *firstRun);
int checkForUpgrade();
char *getUpgradePackageCommand();
char *getExtraUpgradePackageCommand();
//const char * const getDeleteUpgradePackagesCommand();
#endif //CONFIG_NO_PHONET

#endif // PHOTOSTOVIS_H
