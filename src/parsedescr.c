#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "photostovis.h"
#include "log.h"


int hasAlbumDescription(const char *path)
{
    //either yes (1) or no (0)
    int r;
    struct stat statBuf;
    r=stat(path,&statBuf);
    if(!r && S_ISREG(statBuf.st_mode))return 1; //we have a description file
    return 0;

}

struct mdh_album_description *getAlbumDescription(const char *path)
{
    FILE *f=fopen(path,"r");
    if(!f)return NULL; //no album description, not a problem

    LOG0("We have a description file!");
    struct mdh_album_description *albumDescr;
    int l,l2,i;
    char *cur,*eol;

    //get the file length
    fseek(f,0,SEEK_END);
    l=ftell(f);
    fseek(f,0,SEEK_SET);

    //allocate data & read the file
    albumDescr=(struct mdh_album_description *)ph_malloc(sizeof(struct mdh_album_description));
    albumDescr->data=ph_malloc(l+3); //+3: we add at the end 3 characters: \0, @, \0 (for easier parsing)

    l2=fread(albumDescr->data,l,1,f);
    ph_assert(l2==1,"Less data read than available");
    albumDescr->dataLen=l;
    fclose(f);f=NULL; //not needed any more
    albumDescr->data[l]='\0';
    albumDescr->data[l+1]='@';
    albumDescr->data[l+2]='\0';

    //count the images
    albumDescr->nrEntries=0;
    cur=albumDescr->data;
    while((eol=strchr(cur,'\n'))){
        l=eol-cur;
        if(l && cur[0]=='@')albumDescr->nrEntries++;
        cur=eol+1;
    }
    LOG1("We found descriptions for %d images in the description file.",albumDescr->nrEntries);

    //allocate image indexes
    albumDescr->entries=ph_malloc(albumDescr->nrEntries*sizeof(uint32_t));

    //create entries
    i=0;
    cur=albumDescr->data;
    while((eol=strchr(cur,'\n'))){
        *eol='\0';
        if(eol>cur && *(eol-1)=='\r')
            *(eol-1)='\0';
        l=eol-cur;
        if(l && cur[0]=='@'){
            albumDescr->entries[i]=cur-albumDescr->data+1; //+1 is needed to skip the @ character
            i++;
        }
        cur=eol+1;
    }
    ph_assert(i==albumDescr->nrEntries,NULL);
    return albumDescr;
}

void setEntryDescription(struct mdh_album_entry *entry, struct mdh_album_description *albumDescr)
{
    char const *filename;
    char const**descriptionPtr;
    uint8_t *permission;
    int pos=0,i;
    char *entryData=getDataFromEntry(entry);

    //get the filename from the entry
    descriptionPtr=(char const**)entryData;
    ph_assert(*descriptionPtr==NULL,"%p %p",descriptionPtr,*descriptionPtr);
    pos+=sizeof(void*);
    permission=(uint8_t*)(entryData+pos);
    pos++;
    filename=entryData+pos;

    //find the entry for filename

    for(i=0;i<albumDescr->nrEntries;i++)
        if(albumDescr->entries[i]>0 && !strcmp(filename,albumDescr->data+albumDescr->entries[i])){
            //found our description
            pos=albumDescr->entries[i]+strlen(filename)+1;
            while(albumDescr->data[pos]=='\0')pos++;
            LOG0c_do(albumDescr->data[pos]=='@',break,"WARNING: empty entry found for %s",filename);

            if(!strncmp(albumDescr->data+pos,":p:",3)){
                pos+=3;
                *permission=(uint8_t)strtol(albumDescr->data+pos,NULL,10);
                while(albumDescr->data[pos]!='\0')pos++;
                while(albumDescr->data[pos]=='\0')pos++;
            }
            *descriptionPtr=albumDescr->data+pos;
            //LOG0("Found description for %s: %s (first string)",filename,*descriptionPtr);
            albumDescr->entries[i]=-albumDescr->entries[i];
            break;
        }
    LOG0c(i>=albumDescr->nrEntries,"WARNING: description not found for %s",filename);

    entry->flags|=FlagHasDescriptionTxtEntry;
}

void checkAndFreeAlbumEntries(struct mdh_album_description *albumDescr)
{
    //first, check if any of the entries is unused
    int i,nrUnused=0;
    for(i=0;i<albumDescr->nrEntries;i++)
        if(albumDescr->entries[i]>0)nrUnused++;
    LOG0c(nrUnused>0,"WARNING: Albun description contains %d unused entries!",nrUnused);

    //free entries
    free(albumDescr->entries);
    albumDescr->entries=NULL;
    albumDescr->nrEntries=0;
}

void writeImageDescriptionAndTitle(FILE *f, struct mdh_album_entry const * const entry, char const * const filename, char const * const extraStr)
{
    ph_assert(entry->flags&FlagHasDescriptionTxtEntry,NULL);
    //if here, we have a description and we can get it
    int l,wroteDobj=0,wroteFirst=0;
    char *entryData=getDataFromEntry(entry);
    char *cur,*descr=*(char**)(entryData);
    if(extraStr){
        fprintf(f,",\"d\":{\"xx\":\"%s\"",extraStr);
        wroteDobj=wroteFirst=1;
    }
    if(!descr){
        //no description, actually
        if(wroteDobj)fprintf(f,"}");
        return;
    }

    LOG0("We have a description for \"%s\" (%s) (%p)",filename,descr,descr);

    //parse each line for a description line
    cur=descr;
    while(cur[0]!='@'){
        l=strlen(cur);
        if(l>6 && !strncmp(cur,":d:",3) && cur[5]==':'){
            LOG0("Description[%c%c]: %s",cur[3],cur[4],cur+6);
            if(!wroteDobj){fprintf(f,",\"d\":{");wroteDobj=1;}
            if(wroteFirst)fprintf(f,",");
            else wroteFirst=1;
            fprintf(f,"\"%c%c\":\"%s\"",cur[3],cur[4],cur+6);
        }
        cur+=strlen(cur);
        while(cur[0]=='\0')cur++;
    }
    if(wroteDobj)fprintf(f,"}");

    //parse each line for a title line
    wroteDobj=wroteFirst=0;
    cur=descr;
    while(cur[0]!='@'){
        l=strlen(cur);
        if(l>6 && !strncmp(cur,":t:",3) && cur[5]==':'){
            LOG0("Title[%c%c]: %s",cur[3],cur[4],cur+6);
            if(!wroteDobj){fprintf(f,",\"tt\":{");wroteDobj=1;}
            if(wroteFirst)fprintf(f,",");
            else wroteFirst=1;
            fprintf(f,"\"%c%c\":\"%s\"",cur[3],cur[4],cur+6);
        }
        cur+=strlen(cur);
        while(cur[0]=='\0')cur++;
    }
    if(wroteDobj)fprintf(f,"}");
}

void freeAlbumDescriptionStruct(struct mdh_album_description *albumDescr)
{
    if(!albumDescr)return;
    ph_assert(!albumDescr->entries,NULL); //they should have been freed already by the checkAndFreeAlbumEntries function
    free(albumDescr->data);
    //memset(albumDescr,0,sizeof(struct mdh_album_description)); -> not needed
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////



/*
int parseAlbumDescription(FILE *f, struct mdh_album_description *albumDescr, struct mdh_album_entry *entries, int nrEntries)
{
    int l,l2,i,j,scannedPermission;
    char *cur,*eol,*prms,*filename;
    uint32_t *posPtr;
    uint8_t *permissionsPtr,currentPermission;
    ph_assert(albumDescr,NULL);
    ph_assert(f,NULL);

    //get the file length
    fseek(f,0,SEEK_END);
    l=ftell(f);
    fseek(f,0,SEEK_SET);

    //allocate data & read the file
    albumDescr->data=ph_malloc(l);
    if(!albumDescr->data)return -1;

    l2=fread(albumDescr->data,l,1,f);
    ph_assert(l2==1,"Less data read than available");
    albumDescr->dataLen=l;

    //count the images
    cur=albumDescr->data;
    while((eol=strchr(cur,'\n'))){
        l=eol-cur;
        if(l && cur[0]=='@')albumDescr->nrEntries++;
        cur=eol+1;
    }
    LOG1("We found descriptions for %d images in the description file.",albumDescr->nrEntries);

    //allocate image indexes
    albumDescr->entries=ph_malloc(albumDescr->nrEntries*sizeof(uint32_t));
    if(!albumDescr->entries){
        freeAlbumDescriptionStruct(albumDescr);
        return -1;
    }

    //create entries
    i=0;
    cur=albumDescr->data;
    while((eol=strchr(cur,'\n'))){
        *eol='\0';
        if(eol>cur && *(eol-1)=='\r')
            *(eol-1)='\0';
        l=eol-cur;
        if(l && cur[0]=='@'){
            albumDescr->entries[i]=cur-albumDescr->data;
            //get the permissions
            currentPermission=0;//default value
            if(prms=strchr(cur,'[')){
                if(sscanf(prms+1,"%i",&scannedPermission))
                    if(scannedPermission>=0 && scannedPermission<256)
                        currentPermission=scannedPermission;
                    else LOG0("WARNING: Scanned permissions has value outside expected interval [0-255]: %d",scannedPermission);
                l=prms-cur;
            }
            //scan for the corresponding entry
            l--; //-- comes from the @ char in the beginning
            for(j=0;j<nrEntries;j++){
                filename=entries[j].data+(entries[j].flags&FlagIsAlbum?32+sizeof(void*)+sizeof(void*):0)+(entries[j].flags&FlagPhotoHasGPSInfo?8:0)+5;

                l2=strlen(filename);
                //LOG0("Checking %s (l=%d) with %s (l=%d)",filename,l2,cur+1,l);
                if(l==l2 && !strncmp(filename,cur+1,l)){
                    posPtr=(uint32_t*)(filename-5);
                    permissionsPtr=(uint8_t*)(filename-1);
                    *posPtr=eol+1-albumDescr->data;
                    *permissionsPtr=currentPermission;
                    LOG0("Found a match: %s and %s. Permission: %x",filename,cur+1,*permissionsPtr);
                    break;
                }
            }
            if(j>=nrEntries){
                LOG0("WARNING: %s not found among the files in the folder!!!",cur+1);
            }
            //done with this entry
            i++;
        }
        cur=eol+1;

    }
    ph_assert(i==albumDescr->nrEntries,NULL);

    return 0;
}

void freeAlbumDescriptionStruct(struct mdh_album_description *albumDescr)
{
    free(albumDescr->data);
    free(albumDescr->entries);
    memset(albumDescr,0,sizeof(struct mdh_album_description));
}

int writeImageDescriptionAndTitle(FILE *f, struct mdh_album_description *albumDescr, const char *filename, char *extraStr)
{
    //check if we can find our image
    int i,l=strlen(filename),l2,pos;
    int wroteDobj=0,wroteFirst=0;
    char *imgName=NULL,*cur;
    for(i=0;i<albumDescr->nrEntries;i++){
        imgName=albumDescr->data+albumDescr->entries[i]+1; //+1 is to get rid of the starting @ char
        l2=strlen(imgName);
        if((l2==l && strcmp(imgName,filename)==0) || (l2>l && imgName[l]=='[' && strncmp(imgName,filename,l)==0))
            break;
    }
    if(extraStr){
        fprintf(f,",\"d\":{\"xx\":\"%s\"",extraStr);
        wroteDobj=wroteFirst=1;
    }
    if(i<albumDescr->nrEntries){
        LOG0("We have a description for %s",filename);
        if(i==albumDescr->nrEntries-1)l2=albumDescr->dataLen-albumDescr->entries[i];
        else l2=albumDescr->entries[i+1]-albumDescr->entries[i];
        //parse each line for a description line
        cur=albumDescr->data+albumDescr->entries[i];
        pos=0;
        while(pos<l2){
            l=strlen(cur);
            if(l>6 && !strncmp(cur,":d:",3) && cur[5]==':'){
                LOG0("Description: %s",cur+3);
                if(!wroteDobj){fprintf(f,",\"d\":{");wroteDobj=1;}
                if(wroteFirst)fprintf(f,",");
                else wroteFirst=1;
                fprintf(f,"\"%c%c\":\"%s\"",cur[3],cur[4],cur+6);
            }
            pos+=l+1;
            cur+=l+1;
        }
        //parse and add titles
        cur=albumDescr->data+albumDescr->entries[i];
        pos=0;
        if(wroteDobj)fprintf(f,"}");
        wroteDobj=wroteFirst=0;
        while(pos<l2){
            l=strlen(cur);
            if(l>6 && !strncmp(cur,":t:",3) && cur[5]==':'){
                LOG0("Title: %s",cur+3);
                if(!wroteDobj){fprintf(f,",\"tt\":{");wroteDobj=1;}
                if(wroteFirst)fprintf(f,",");
                else wroteFirst=1;
                fprintf(f,"\"%c%c\":\"%s\"",cur[3],cur[4],cur+6);
            }
            pos+=l+1;
            cur+=l+1;
        }
    }
    if(wroteDobj)fprintf(f,"}");
}
*/
