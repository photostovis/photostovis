#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/resource.h> //setrlimit
#include <sys/time.h>
#include <sys/stat.h>
#include <signal.h>
#include <poll.h>
#include <ctype.h> //to upper
#ifndef CONFIG_NO_PHONET
#include <curl/curl.h>
#endif
//#include <execinfo.h>
#define UNW_LOCAL_ONLY
#include <libunwind.h>
#include <locale.h>

#ifdef WITH_BUTTON
#include <wiringPi.h>
#endif

#ifndef __APPLE__
#include <sys/inotify.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#endif

#include <openssl/md5.h>

#include <limits.h>
#include <pthread.h>
#include "photostovis.h"
#include "log.h"

#ifdef __APPLE__
const char * const KOptionsRootFolder="/Users/lohanf/Documents/photostovis";
static const char * const KRestartNetworkingTemplate=NULL;
#else
const char * const KOptionsRootFolder="/data/photostovis";
static const char * const KRestartNetworkingTemplate="sudo nohup sh -c \"ifdown %s && ifup %s\"";
#endif
//static const unsigned int KMaxMemory2Use=128*1048576;

const char *KPhotostovisDataOld="/.private";
const char *KPhotostovisData="/.photostovis_cache";
const char *KGeneratedCfgFilename="generated.cfg";

char const * KFFmpegBin=NULL;
char const * KFFprobeBin=NULL;

char const * const KFFmpegBin1="/usr/bin/photostovis_ffmpeg";
char const * const KFFmpegBin2="/usr/local/bin/ffmpeg";
char const * const KFFmpegBin3="/usr/bin/ffmpeg";
char const * const KFFprobeBin1="/usr/bin/photostovis_ffprobe";
char const * const KFFprobeBin2="/usr/local/bin/ffprobe";
char const * const KFFprobeBin3="/usr/bin/ffprobe";

int shouldExit=0; //uses exit codes from photostovis.h

void sigUSR1Function(int s){
    LOG0("Upgrade available. Exit when appropriate.");
    shouldExit=EXIT_FOR_UPGRADE;
}

void sigIntFunction(int s){
    LOG0("Signal caught (sigIntFunction): %s",strsignal(s));
    shouldExit=EXIT_DO_NOT_RESTART; //if we were "ordered" to stop, we stop. This might come during upgrade, and would mess things up!
}

static void sigAbortFunction(int s){
  LOG0("Signal caught (sigAbortFunction): %s. Will print stack and exit.",strsignal(s));

  //print trace
  unw_cursor_t cursor;
  unw_context_t context;

  // Initialize cursor to current frame for local unwinding.
  unw_getcontext(&context);
  unw_init_local(&cursor, &context);

  // Unwind frames one by one, going up the frame stack.
  while (unw_step(&cursor) > 0) {
    unw_word_t offset, pc;
    unw_get_reg(&cursor, UNW_REG_IP, &pc);
    if (pc == 0) {
      break;
    }
    printf("0x%zx:", pc);

    char sym[256];
    if (unw_get_proc_name(&cursor, sym, sizeof(sym), &offset) == 0) {
      LOG0("0x%zx: (%s+0x%zx)", pc, sym, offset);
    } else {
      LOG0("0x%zx: -- (? ? ?) --", pc);
    }
  }


    /*old backtrace code
    const int KArraySize=20;
    void *array[KArraySize];
    size_t size,i;
    char **strings;

    size=backtrace(array,KArraySize);
    strings=backtrace_symbols(array,size);

    LOG0("Obtained %zu stack frames.",size);
    for(i=0;i<size;i++)
        LOG0("%s",strings[i]);
    free(strings);
    */
    srvStop();
    closelog(1);
    exit(EXIT_RESTART);
}

static void sigPrintFunction(int s){
    LOG0("Signal caught (sigPrintFunction): %s. Ignoring.",strsignal(s));
}
/*
static uint16_t xtoys(uint16_t x){
    return ((x&0xFF)<<8)+(x>>8);
}*/

void trim_string(char **s){
    while(**s==' ')(*s)++;
    int l=strlen(*s)-1;
    if(l<0)return;
    while((*s)[l]==' ' || (*s)[l]=='\r' || (*s)[l]=='\n')
        (*s)[l--]='\0';
}
void trim_string_right(char *s){
    int l=strlen(s)-1;
    if(l<0)return;
    while(s[l]==' ' || s[l]=='\r' || s[l]=='\n')
        s[l--]='\0';
}

void renameOldDataFolder(const char *picsFolder){
    //first, create the old-style filename
    char pathOld[PATH_MAX],pathNew[PATH_MAX];
    int l=strlen(picsFolder),l2,err;
    memcpy(pathOld,picsFolder,l);
    memcpy(pathNew,picsFolder,l);
    
    l2=strlen(KPhotostovisDataOld);
    memcpy(pathOld+l,KPhotostovisDataOld,l2+1);
    l2=strlen(KPhotostovisData);
    memcpy(pathNew+l,KPhotostovisData,l2+1);

    //check if it exists
    err=rename(pathOld,pathNew);

    if(err){
        if(errno==ENOENT)return; //no old data folder, everything ok

        //if here, we have a real error!
        LOG0("ERROR changing from old-style folder (%s) to new-style (%s): %s.",pathOld,pathNew,strerror(errno));
        exit(0); //do not restart
    }
}

static FILE *openConfigFile(const char *picsFolder, const char *mode, const int expectExisting){
    //create a potential generated config filename based on the current path
    LOG0("openConfigFile: path=%s, mode=%s, expectExisting=%d",picsFolder,mode,expectExisting);
    char path[PATH_MAX];
    int l=strlen(picsFolder),l2;
    memcpy(path,picsFolder,l);
    l2=strlen(KPhotostovisData);
    memcpy(path+l,KPhotostovisData,l2);
    l+=l2;
    if(path[l-1]!='/')path[l++]='/';
    path[l]='\0';
    l2=dirService(path,0);
    LOG0("dirService for %s returned %d",path,l2);
    if(l2)return NULL; //cannot stat or create the folder

    l2=strlen(KGeneratedCfgFilename);
    memcpy(path+l,KGeneratedCfgFilename,l2+1);
    if(expectExisting){
        //check if the generated config file exists
        struct stat st;
        l2=stat(path,&st);
        if(l2 || !S_ISREG(st.st_mode))return NULL;
    }
    FILE *f=fopen(path,mode);
    return f;
}
/*
void saveSettingsFile(){
    struct ps_md_config const * const mdConfig=getMdConfigRd();
    FILE *f=openConfigFile(mdConfig->rootFolder,"w",0);
    ph_assert(f,NULL);

    //save to config file
    if(mdConfig->phonetUsername && mdConfig->phonetPassword)
        fprintf(f,"username=%s\npassword=%s\n",mdConfig->phonetUsername,mdConfig->phonetPassword);
    if(mdConfig->nrViewingPermissions>0)
        fprintf(f,"nrViewingPermissions=%d\n",mdConfig->nrViewingPermissions);
    fclose(f);
    releaseMdConfig(&mdConfig);
    LOG0("configuration file saved!");
}*/

#ifndef CONFIG_NO_PHONET
void *registerServerThread(void *ptr){
    //TODO: block SIGALRM
    time_t netUnreachableSince=-1,tNow,tThreadStart=time(NULL);
    LOG0("registerServerThread created and running.");
    int shutdownOnCableOut=0,cableConnected;
    struct ps_md_config const * const mdConfig=getMdConfigRd();
    if(mdConfig->cfgFlags&CfgFlagShutdownOnCableOut)
        shutdownOnCableOut=1;
    releaseMdConfig(&mdConfig);

    phonetIp=ipToUInt32(KPhoNetIP);
    phoorgIp=ipToUInt32(KPhoOrgIP);

    while(1){
        LOG0close_return(shouldExit,NULL,statusFlagsRemove(StatusFlagRegistrationThreadActive),"shouldExit active, exiting the registration thread");
        phonetServerStart();//at the moment this function always returns 0
        LOG0close_return(shouldExit,NULL,statusFlagsRemove(StatusFlagRegistrationThreadActive),"shouldExit active, exiting the registration thread");

        struct ps_md_net const * const mdNet=getMdNetRd();
        uint8_t phonetCfgStatus=mdNet->phonetCfgStatus;
        releaseMdNet(&mdNet);

        LOG0("phonetCfgStatus: %d",(int)phonetCfgStatus);
        if(phonetCfgStatus==PhonetCfgSuccess){
            //we succeeded to register and there are no errors whatsoever
            ledCommand(LCFlash10PercentOff,0);
            ledCommand(LCFlash90PercentOff,0);
            break;
        }
        if(phonetCfgStatus>=PHONET_ERRORS_MIN){
            //we reached the photostovis.net, but there is no registration
            ledCommand(LCFlash10PercentOff,0);
            ledCommand(LCFlash90PercentOn,0);
            break;
        }

        //if we are here, registration is unsuccessful. Wait few moments and try again

        ledCommand(LCFlash10PercentOn,0);
        //should we perhaps restart the network?
        if(netUnreachableSince!=-1 && KRestartNetworkingTemplate){
            tNow=time(NULL);
            if(tNow-netUnreachableSince>NET_RESTART_AFTER){
                netUnreachableSince=-1;
                //restart the networking
                LOG0("Attempting to restart the networking ...");
                char restartCmd[1000];
                snprintf(restartCmd,1000,KRestartNetworkingTemplate,activeNetworkInterface,activeNetworkInterface);
                int r=system(restartCmd);
                LOG0("Networking restart attempt returned %d",r);
            }
        } else
            netUnreachableSince=time(NULL);

        cableConnected=checkCable();
        if(shutdownOnCableOut && cableConnected==1)
            shutdownOnCableOut=2;
        if(!cableConnected && shutdownOnCableOut==2){
            shouldExit=EXIT_SHUTDOWN;
            shutdownOnCableOut=0; //so we can break and exit
            break;
        }
        sleep(SLEEP_IF_REGISTRATION_FAILED);
    }//while(1)
    if(shutdownOnCableOut){
        while(1){
            cableConnected=checkCable();
            if(shutdownOnCableOut && cableConnected==1)
                shutdownOnCableOut=2;
            if(!cableConnected && shutdownOnCableOut==2){
                shouldExit=EXIT_SHUTDOWN;
                break;
            }
            if( (time(NULL)-tThreadStart+SLEEP_IF_REGISTRATION_FAILED+5)*1000 > SLEEP_IF_REGISTRATION_SUCCESSFUL )
                break;
            sleep(SLEEP_IF_REGISTRATION_FAILED);
        }//while(1)
    }
    statusFlagsRemove(StatusFlagRegistrationThreadActive);
    LOG0("Exiting registerServerThread");

    return NULL;
}
int registerServer(){
    LOG0("registerServer++");
    //should we start registering or not?
    LOG0return(statusFlagsCheck(StatusFlagRegistrationThreadActive),0,"WARNING: Registration in progress, not starting another one.");
    statusFlagsAdd(StatusFlagRegistrationThreadActive);

    //here we create a separate thread from where we register to photostovis.net
    int r;
    pthread_t t;
    pthread_attr_t attr;
    r=pthread_attr_init(&attr);
    ph_assert(!r,NULL);//the above should not fail
    r=pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
    ph_assert(!r,NULL);//the above should not fail
    LOG0("Creating registerServerThread...");
    r=pthread_create(&t,&attr,&registerServerThread,NULL);//transfering ownership of data
    LOG0close_return(r,-1,statusFlagsRemove(StatusFlagRegistrationThreadActive),"ERROR (%d) creating the thread for server registration (in registerServer).",r);

    LOG0("registerServer--");
    return 0;
}
#endif

void *scanPicturesThread(void *ptr){
    int r,forcePicturesFullRescan=0;
    ledCommand(LCFlash50PercentOn,0);
    LOG0("scanPicturesThread created and running. id: %p",ptr);
    struct md_entry_id *id=(struct md_entry_id *)ptr;

    while(1){
        if(statusFlagsCheck(StatusFlagFirstScanDone) && !id){
            sleep(2); //we start with a 2 seconds break, maybe there are other modifications. Also to increase the time difference between a potential folder delete and the change of the modification time. Basically to not delete parsed folders data by mistake
            if(statusFlagsCheck(StatusFlagDoARescan)){
                statusFlagsRemove(StatusFlagDoARescan);
                continue;
            }
        }

        if(!id){
            LOG0("Closing thmbManager during picture scan");
            thmbManagerClose(); //we should not cache thumbs during picture scan
        }

        sendScanningStatusToConnectedClients(StatusScanning_Pictures);
        r=lookForPictures(&forcePicturesFullRescan,id); //includes counting the pictures
        ph_assert(r==0 || r==1,NULL);
        if(shouldExit)
            break;

        ph_assert(statusFlagsCheck(StatusFlagPictureScanThreadActive),NULL);
        statusFlagsAdd(StatusFlagFirstScanDone);

        if(r){//returned by lookForPictures()
            statusFlagsAdd(StatusFlagDoARescan);
        }

        if(statusFlagsCheck(StatusFlagDoARescan)){
            statusFlagsRemove(StatusFlagDoARescan);
            id=NULL;
            continue;
        }

        ledCommand(LCFlash50PercentOff,0);
        LOG_FLUSH;

        if(id)
            break;

        //scanning pictures done, now we go to checking thumbnails part
        sendScanningStatusToConnectedClients(StatusScanning_Thumbnails);
        r=checkAndCreateThumbnails();
        ph_assert(r==0,NULL);
        if(shouldExit)
            break;

        if(statusFlagsCheck(StatusFlagDoARescan)){
            statusFlagsRemove(StatusFlagDoARescan);
            continue;
        }

        if(shouldExit)
            break;
        if(statusFlagsCheck(StatusFlagDoARescan)){
            statusFlagsRemove(StatusFlagDoARescan);
            continue;
        }
        //if here, we can exit this while.
        break;
    }//while(1)
    sendScanningStatusToConnectedClients(StatusScanning_ThumbnailsCache);

    if(!id)
        thmbManagerInit();

    statusFlagsRemove(StatusFlagIgnoreFirstRescan); //...if not removed already
    statusFlagsRemove(StatusFlagPictureScanThreadActive);
    //send a final notification to clients
    sendScanningStatusToConnectedClients(StatusScanning_Done);
    LOG0("Exiting scanPicturesThread");
    return NULL;
}

int scanPictures(){
    //should we start scanning or not?
    if(statusFlagsCheck(StatusFlagPictureScanThreadActive)){
        if(statusFlagsCheck(StatusFlagIgnoreFirstRescan)){
            statusFlagsRemove(StatusFlagIgnoreFirstRescan);
            LOG0("Picture scan (fast) already ongoing, ignoring this request.");
        } else {
            statusFlagsAdd(StatusFlagDoARescan);
            LOG0("Picture scan in progress. Will restart scanning after current scan ends.");
        }
        return 0;
    }

    //if here, we will do a scan
    statusFlagsAdd(StatusFlagPictureScanThreadActive);

    //here we create a separate thread from where we register to photostovis.net, and then start looking for pictures
    int r;
    pthread_t t;
    pthread_attr_t attr;
    r=pthread_attr_init(&attr);
    ph_assert(!r,NULL);//the above should not fail
    r=pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
    ph_assert(!r,NULL);//the above should not fail
    LOG0("Creating scanPicturesThread...");
    r=pthread_create(&t,&attr,&scanPicturesThread,NULL);
    pthread_attr_destroy(&attr);
    LOG0close_return(r,-1,statusFlagsRemove(StatusFlagPictureScanThreadActive),"ERROR (%d) creating THE thread for pictures scan.",r);

    return 0;
}




void closeAndExit(int shouldExitValue){
    LOG0(NULL);
    LOG0("Photostovis server is terminating...");

    if(shouldExitValue>=0)
        shouldExit=shouldExitValue;
    srvStop();

    ledCommand(LCOff,1);
    ledClientActivity(LCOff,1);

    LOG0("Server stopped.");


    struct ps_md_config *mdConfigW=getMdConfigWritable();

#ifndef __APPLE__
    close(mdConfigW->inotifyFd);
#endif
    if(mdConfigW->srvDocRoot!=KDefaultDocRootLocal && mdConfigW->srvDocRoot!=KDefaultDocRootSystem){
        free(mdConfigW->srvDocRoot);
    }
    mdConfigW->srvDocRoot=NULL;
    releaseMdConfigW(&mdConfigW);

#ifndef CONFIG_NO_PHONET
    //unregister server in photostovis.net
    phonetServerStop();
    curl_global_cleanup(); //TODO: this is not thread safe. Do something to ensure this somehow.
#endif

    struct ps_md_config const * const mdConfig=getMdConfigRd();
    if(mdConfig->cfgFlags&CfgFlagIsDaemon){
        switch(shouldExit){
        case 0:
            LOG0("Photostovis exiting with success. Will NOT be automatically restarted.");
            break;
        case EXIT_FOR_UPGRADE:
            LOG0("Photostovis exiting, indicating readiness for upgrade.");
            break;
        case EXIT_RESTART:
            LOG0("Photostovis exiting, will be automatically restarted.");
            break;
        case EXIT_DO_NOT_RESTART:
            LOG0("Photostovis exiting, will NOT be automatically restarted.");
            break;
        }
    } else LOG0("Photostovis exiting");
    releaseMdConfig(&mdConfig);

    //free data
    deleteAllZipArchives();
    thmbManagerClose();
    imgManagerClose();
    fileManagerClose();
    destroyMd();

    ph_malloc_info();
    closelog(0);
    exit(shouldExit);
}


#ifdef WITH_BUTTON
void buttonHandler(void){
    if(shouldExit==EXIT_SHUTDOWN)return;
    closeAndExit(EXIT_SHUTDOWN);
}
#endif



int photostovis(struct ps_md_config *config) {
    int r,r2;
    //catch sigusr1
    struct sigaction sigHandler;
    sigHandler.sa_handler=sigUSR1Function;
    sigemptyset(&sigHandler.sa_mask);
    sigHandler.sa_flags=0;
    sigaction(SIGUSR1,&sigHandler,NULL);

    //catch sigint and exit nicely
    sigHandler.sa_handler=sigIntFunction;
    sigemptyset(&sigHandler.sa_mask);
    sigHandler.sa_flags=0;
    sigaction(SIGINT,&sigHandler,NULL);

    //catch sigabrt, sigsegv, sigfpe
    sigHandler.sa_handler=sigAbortFunction;
    sigemptyset(&sigHandler.sa_mask);
    sigHandler.sa_flags=0;
    sigaction(SIGABRT,&sigHandler,NULL);
    sigaction(SIGSEGV,&sigHandler,NULL);
    sigaction(SIGFPE,&sigHandler,NULL);

    //ignore sigpipe
    sigHandler.sa_handler=sigPrintFunction;
    sigaction(SIGPIPE,&sigHandler,NULL);
    //signal(SIGPIPE, SIG_IGN);

    LOG0c(config->cfgFlags&CfgFlagIsDaemon,KPhotostovisServerString); //if it is not daemon, the daemon part already wrote this.

#ifdef WITH_BUTTON
    LOG0("Using wiringPi and button!");
    wiringPiSetup();
    wiringPiISR(9,INT_EDGE_RISING,&buttonHandler);
#endif

    /* Not needed, because we are using RAND_bytes from openssl
    //initialize the random generator
    {
        struct timeval tv;
        gettimeofday(&tv,NULL);
        unsigned char r[MD5_DIGEST_LENGTH];
        MD5((unsigned char *)&tv,sizeof(struct timeval),r);
        srandom((unsigned int)r);
    }*/

    //set locale
    /*
    LOG0("Current LC_CTYPE locale: %s",setlocale(LC_CTYPE,NULL));
    LOG0("Locale changed to: %s",setlocale(LC_CTYPE,"C.UTF-8"));*/

    //Initialize md structures.
    initMd(config);
    free(config);config=NULL;

#ifndef NDEBUG
    LOG0("DEBUG: Running in debug mode.");

    ph_assert(sizeof(int)==4,NULL);
    LOG0("DEBUG: Size of pointers is: %d. Size of float is: %d",(int)sizeof(void*),(int)sizeof(float));

    /*
    struct rlimit rlt;
    r=getrlimit(RLIMIT_AS,&rlt);
    assert(!r);
    if(rlt.rlim_cur>=KMaxMemory2Use)
    {
        if(RLIM_INFINITY==rlt.rlim_cur)LOG0("Currently allowed memory use (no limit) is higher than what we want, lowering it to %u MB",KMaxMemory2Use>>20);
        else LOG0("Currently allowed memory use (%lu MB) is higher than what we want, lowering it to %u MB",rlt.rlim_cur>>20,KMaxMemory2Use>>20);
        rlt.rlim_cur=KMaxMemory2Use;
        r=setrlimit(RLIMIT_AS,&rlt);
        assert(!r);
    } else LOG0("Current allowed memory limit (%lu bytes) is lower than our limit (%u bytes)",rlt.rlim_cur,KMaxMemory2Use);
    */

#endif

    struct ps_md_config *mdConfigW=getMdConfigWritable();


    //if we have old-style data folder change it to new-style
    renameOldDataFolder(mdConfigW->rootFolder);

    //if we already have phonet username and password, we delete the generated settings file (becoming obsolete)
    if(mdConfigW->phonetUsername && mdConfigW->phonetPassword){
        char path[PATH_MAX];
        int l=strlen(mdConfigW->rootFolder),l2;
        memcpy(path,mdConfigW->rootFolder,l);
        l2=strlen(KPhotostovisData);
        memcpy(path+l,KPhotostovisData,l2);
        l+=l2;
        if(path[l-1]!='/')path[l++]='/';
        path[l]='\0';
        l2=dirService(path,0);
        LOG0("dirService for %s returned %d",path,l2);
        if(!l2){
            l2=strlen(KGeneratedCfgFilename);
            memcpy(path+l,KGeneratedCfgFilename,l2+1);

            r=unlink(path);
            if(r){
                LOG0c(errno!=ENOENT,"Could NOT delete obsolete configuration file <<%s>>. Error: %s",path,strerror(errno));
            } else
                LOG0("Obsolete configuration file %s deleted.",path);
        }
    } else {
        //try to open a potential generated settings filename based on the current path
        FILE *f=openConfigFile(mdConfigW->rootFolder,"r",1);
        if(f){
            LOG0("Settings file found in %s/%s",mdConfigW->rootFolder,KPhotostovisData);

            //we found a settings file. Use it
            char line[1024],*ret,*cmd,*arg;
            while(1){
                ret=fgets(line,sizeof(line),f);
                if(!ret)break;

                //is this a comment?
                if(line[0]=='#')continue;
                //remove the EOL
                trim_string_right(line);
                r=strlen(line);
                //parse our line
                arg=strchr(line,'=');
                if(!arg){
                    LOG0("WARNING: Skipping invalid settings line: %s",line);
                    continue;
                }

                arg[0]='\0';
                arg++;
                cmd=line;

                //trim cmd and arg
                trim_string(&cmd);
                trim_string(&arg);

                //do stuff
                if(!strcasecmp(cmd,"username")){
                    mdConfigW->phonetUsername=ph_malloc(strlen(arg)+1);
                    strcpy(mdConfigW->phonetUsername,arg);
                    LOG0("Configured photostovis.net username: %s",mdConfigW->phonetUsername);
                    continue;
                }
                if(!strcasecmp(cmd,"password")){
                    mdConfigW->phonetPassword=ph_malloc(strlen(arg)+1);
                    strcpy(mdConfigW->phonetPassword,arg);
                    int i;
                    for(i=0;mdConfigW->phonetPassword[i];i++)
                        mdConfigW->phonetPassword[i]=toupper(mdConfigW->phonetPassword[i]);
                    LOG0("Configured photostovis.net SHA256 password: %s",mdConfigW->phonetPassword);
                    continue;
                }

                if(!strcasecmp(cmd,"nrViewingPermissions")){
                    int nrViewingPermissions=strtol(arg,NULL,10);
                    LOG0return(nrViewingPermissions<=0 || nrViewingPermissions>8,EXIT_DO_NOT_RESTART,
                               "Invalid nrPermissions parsed from settings file: %d. Exiting. Photostovis will NOT be automatically restarted.",nrViewingPermissions);
                    mdConfigW->nrViewingPermissions=nrViewingPermissions;
                    LOG0("Configured the number of permissions: %d",mdConfigW->nrViewingPermissions);
                    continue;
                }

                //if we are here, we found a command that is not understood
                LOG0return(1,EXIT_DO_NOT_RESTART,
                           "Command not understood (%s) in settings line (%s). Exiting. Photostovis will NOT be automatically restarted.",cmd,line);
            }
            fclose(f);
        }//end of generated config filename parsing
    }


    //does the root folder exist?
    {
        char path[PATH_MAX];
        int l;
        r=dirService(mdConfigW->rootFolder,0);
        //TODO: we should not return, but display an error to the user. E.g. a different page

        LOG0return(r,EXIT_DO_NOT_RESTART,"ERROR: Root Folder %s supposedly containing pictures cound not be found.",mdConfigW->rootFolder);
        //add it to our folder list
        l=strlen(mdConfigW->rootFolder);
        memcpy(path,mdConfigW->rootFolder,l+1);

        //make sure pictures dir exists
        r=strlen(KPicturesFolder);
        memcpy(path+l,KPicturesFolder,r+1);
        r=dirService(path,1);
        LOG0return(r,EXIT_DO_NOT_RESTART,"ERROR: Picture folder (%s) does not exist and cannot be created!",path);

        //make sure data dir exists
        r=strlen(KPhotostovisData);
        memcpy(path+l,KPhotostovisData,r+1);
        r=dirService(path,1);
        LOG0return(r,EXIT_DO_NOT_RESTART,"ERROR: Data folder (%s) does not exist and cannot be created!",path);
    }
    LOG_FLUSH;

    /*******************************
     * End of configuration-related things
     * Start of initializing things
     *******************************/
    //prepare mdPictures->rootAlbum
    struct ps_md_pics *mdPicsW=getMdPicsWritable();
    r=strlen(mdConfigW->rootFolder);
    const uint32_t strAlbumPos=sizeof(struct md_album_entry)+sizeof(struct mdx_album_entry*)+4+16+sizeof(struct mdh_album_data_by_permission)*(mdConfigW->nrViewingPermissions+1);
    const uint32_t albumDataLen=strAlbumPos+r+1;
    const uint32_t albumExtraLen=sizeof(struct mdx_album_entry);

    mdPicsW->rootAlbum=ph_malloc(albumDataLen);
    //initialize rootAlbum as empty, root album
    memset(mdPicsW->rootAlbum,0,sizeof(struct md_album_entry));
    mdPicsW->rootAlbum->flags=FlagIsAlbum;
    mdPicsW->rootAlbum->dataOffsetLo=strAlbumPos;
    mdPicsW->rootAlbum->earliestCaptureTime=mdPicsW->rootAlbum->latestCaptureTime=DATE_MAX;
    //copy rootFolder at the end
    memcpy((char*)mdPicsW->rootAlbum+strAlbumPos,mdConfigW->rootFolder,r+1);
    //compute the nameCRC for the root element
    mdPicsW->rootAlbum->nameCRC=ph_crc32b(mdConfigW->rootFolder)&0xFFFFFFFC;
    //create and initialize albumX
    struct mdx_album_entry *albumX=(struct mdx_album_entry *)ph_malloc(albumExtraLen);
    memset(albumX,0,albumExtraLen);
    *(struct mdx_album_entry **)(mdPicsW->rootAlbum+1)=albumX;
    *(uint32_t*)((struct mdx_album_entry**)(mdPicsW->rootAlbum+1)+1)=albumExtraLen;
    releaseMdPicsW(&mdPicsW);

#ifndef CONFIG_NO_PHONET
    //initialize curl (before creating other threads
    curl_global_init(CURL_GLOBAL_DEFAULT);
#endif
#ifndef __APPLE__
    //initialize inotify
    mdConfigW->inotifyFd=inotify_init();
    LOG0return(mdConfigW->inotifyFd==-1,EXIT_DO_NOT_RESTART,"inotify_init() failed: %s",strerror(errno));
#endif
    uint16_t httpPort=mdConfigW->httpPort;
    uint16_t httpsPort=mdConfigW->httpsPort;

    //do we have ffmpeg?
    LOG0("Testing for ffmpeg ...");
    r=system(KFFmpegBin1);
    if(!r || r==256){
        LOG0("ffmpeg (%s) works!",KFFmpegBin1);
        KFFmpegBin=KFFmpegBin1;
    } else {
        r=system(KFFmpegBin2);
        if(!r || r==256){
            LOG0("ffmpeg (%s) works!",KFFmpegBin2);
            KFFmpegBin=KFFmpegBin2;
        } else {
            r=system(KFFmpegBin3);
            if(!r || r==256){
                LOG0("ffmpeg (/%s) works!",KFFmpegBin3);
                KFFmpegBin=KFFmpegBin3;
            } else {
                LOG0("ffmpeg does NOT work! system returned %d",r);
            }
        }
    }


    //do we have ffprobe?
    LOG0("Testing for ffprobe ...");
    r=system(KFFprobeBin1);
    if(!r || r==256){
        LOG0("ffprobe (%s) works!",KFFprobeBin1);
        KFFprobeBin=KFFmpegBin1;
    } else {
        r=system(KFFprobeBin2);
        if(!r || r==256){
            LOG0("ffprobe (%s) works!",KFFprobeBin2);
            KFFprobeBin=KFFprobeBin2;
        } else {
            r=system(KFFprobeBin3);
            if(!r || r==256){
                LOG0("ffprobe (/%s) works!",KFFprobeBin3);
                KFFprobeBin=KFFprobeBin3;
            } else {
                LOG0("ffprobe does NOT work! system returned %d",r);
                KFFprobeBin=NULL;
            }
        }
    }

    releaseMdConfigW(&mdConfigW);

    //initialize the image manager
    imgManagerInit();
#ifdef NDEBUG
    fileManagerInit();
#endif

    //move transcoded video files in "/transcoded"
    moveTranscodedFiles();

    LOG0("Starting the http server...");
    r=httpStart(httpPort);
    LOG0return(shouldExit,shouldExit,"Photostovis exiting (due to received signal).");
    LOG0return(r<0,EXIT_RESTART,"Photostovis exiting with error (%d) returned by srvStart().",r);

    if(httpsPort>0){
        LOG0("Attempting to start the https server...");
        r2=httpsStart(httpsPort);
        LOG0return(shouldExit,shouldExit,"Photostovis exiting (due to received signal).");

        LOG0c(r2==-1,"WARNING: Starting the web/https server failed because certificates & key could not be found.")
        LOG0c(r2<-1,"ERROR: Starting the web/https server failed.");
    } else r2=-10; //which means that we do not need https

    ledCommand(LCOn,1);

    mdConfigW=getMdConfigWritable();
    mdConfigW->httpSocket=r;
    mdConfigW->httpsSocket=r2;
    if(r2==-1){
        if(mdConfigW->phonetUsername){
            mdConfigW->cfgFlags|=CfgFlagCertificateIsNeeded;
            LOG0("A SSL Certificate is needed (flag added)");
        }
        //temporary allow authentication over http when coming from Internet. This is not to break the service
        //TODO: remove this, or do something
        if(!(mdConfigW->cfgFlags&CfgFlagInetAuthHttpAllowed)){
            mdConfigW->cfgFlags|=CfgFlagInetAuthHttpAllowed|CfgFlagInetAuthHttpAllowedAutoadded;
            LOG0("Inet Authentication over HTTP auto-allowed: flag added.");
        }
    }

    //initialize the poll structure
    //first: inotify
    //second: udev or diskutil
    //3rd: http
    //4th: https
    struct pollfd pfd[4];
#ifdef __APPLE__
    pfd[0].fd=-1; //no inotify for apple
    //open diskutil activity monitor
    FILE *diskUtilityMonitor=popen("diskutil activity","r");
    pfd[1].fd=fileno(diskUtilityMonitor);

    //TODO: this overrides the diskUtility (obviously)
    pfd[1].fd=-1;
#else
    pfd[0].fd=mdConfigW->inotifyFd;
    pfd[1].fd=-1; //udev not yet implemented
#endif

    pfd[2].fd=mdConfigW->httpSocket;
    if(mdConfigW->httpsSocket>=0)
        pfd[3].fd=mdConfigW->httpsSocket;
    else
        pfd[3].fd=-1;

    pfd[0].events=pfd[1].events=pfd[2].events=pfd[3].events=POLLIN;


    releaseMdConfigW(&mdConfigW);
#ifndef CONFIG_NO_PHONET
    r=registerServer();
    LOG_FLUSH;
    LOG0c_do(r,closeAndExit(EXIT_RESTART),"Photostovis exiting with error (%d) returned by registerServer().",r);
#endif

    r=scanPictures();
    LOG0c_do(r,closeAndExit(EXIT_RESTART),"Photostovis exiting with error (%d) returned by scanPictures().",r);

    while(1){
        r=poll(pfd,4,SLEEP_IF_REGISTRATION_SUCCESSFUL);
        //LOG0c(r,"poll returned %d",r);
        LOG0("poll returned %d",r);
        if(shouldExit)break;
        if(r<0)LOG0("poll returned an error: %s",strerror(errno));
        else if(r==0){
#ifndef CONFIG_NO_PHONET
            registerServer(); //check if we have different IP addresses than those we registered
#endif
        } else {
            ph_assert(r>0,NULL);
            if(pfd[0].revents){
#ifdef __APPLE__
                ph_assert(0,"we have an event for pfd[0] on Mac");
#else
                if(pfd[0].revents&POLLIN){
                    //clear the read

                    char buf[sizeof(struct inotify_event)+NAME_MAX+1];
                    r=read(pfd[0].fd,buf,sizeof(struct inotify_event)+NAME_MAX+1);

                    //scan pictures
                    r=scanPictures();
                    LOG0c_do(r,closeAndExit(EXIT_RESTART),"Photostovis exiting with error (%d) returned by scanPictures().",r);
                    if(shouldExit)break;
                } else ph_assert(0,"inotify revents (%d) is different from POLLIN.",(int)pfd[0].revents);
#endif
            }
            if(pfd[1].revents){
                if(pfd[1].revents&POLLIN){
#ifdef __APPLE__
                LOG0("Reading from diskutil activity");
                //disk add constants
                const char *KDiskAppeared="***DiskAppeared ";
                const char *KDiskDescriptionChanged="***DiskDescriptionChanged ";
                const char *KNullVolumePath="DAVolumePath = '<null>'";
                const char *KVolumePathStart="DAVolumePath = 'file://";
                //const char *KVolumeType="DAVolumeKind = '";
                const char *KPartitionNameStart="('disk";
                const int KDiskAppearedLen=strlen(KDiskAppeared);
                const int KDiskDescriptionChangedLen=strlen(KDiskDescriptionChanged);
                //partition UUID
                const char *KVolumeUUID="Volume UUID:";

                //disk remove constants
                const char *KDiskDisappeared="***DiskDisappeared ";
                const char *KDiskUnmountAproval="***DiskUnmountApproval ";
                const int KDiskDisappearedLen=strlen(KDiskDisappeared);
                const int KDiskUnmountAprovalLen=strlen(KDiskUnmountAproval);


                const int KLineSize=1000;
                char line[KLineSize];
                char *start,*end;

                fgets(line,KLineSize,diskUtilityMonitor);
                if((!strncmp(line,KDiskAppeared,KDiskAppearedLen) || !strncmp(line,KDiskDescriptionChanged,KDiskDescriptionChangedLen)) &&
                        //disk appeared/changed. Do we have a disk and a mount?
                        ((start=strstr(line,KPartitionNameStart)) && !strstr(line,KNullVolumePath))){
                    //we have a mounted partition
                    char partitionName[50];
                    char mountPoint[50];
                    char uuid[36+1];
                    //char type[10];

                    //find & save the partition
                    start+=2;
                    end=strchr(start,'\'');
                    ph_assert(end,NULL);
                    int l=end-start;
                    memcpy(partitionName,start,l);
                    partitionName[l]='\0';

                    //find & save the mount point
                    start=strstr(line,KVolumePathStart);
                    ph_assert(start,"We do not know the VolumePath from this line: %s",line);
                    start+=strlen(KVolumePathStart);
                    end=strchr(start,'\'');
                    ph_assert(end,NULL);
                    l=end-start;
                    memcpy(mountPoint,start,l);
                    mountPoint[l]='\0';

                    /*
                    //find & save the partition type
                    start=strstr(line,KVolumeType);
                    ph_assert(start,"We do not know the VolumeKind from this line: %s",line);
                    start+=strlen(KVolumeType);
                    end=strchr(start,'\'');
                    ph_assert(end,NULL);
                    l=end-start;
                    ph_assert(l<10,NULL);
                    memcpy(type,start,l);
                    type[l]='\0';*/

                    //find the partition UUID
                    sprintf(line,"diskutil info %s",partitionName);
                    FILE *f=popen(line,"r");
                    ph_assert(f,NULL);
                    while(fgets(line,KLineSize,f)){
                        if((start=strstr(line,KVolumeUUID))){
                            start+=strlen(KVolumeUUID);
                            while(isblank(*start))start++;
                            end=start;
                            while(isalnum(*end) || *end=='-')end++;
                            l=end-start;
                            ph_assert(l==36,"l=%d",l);
                            memcpy(uuid,start,l);
                            uuid[l]='\0';
                        }
                    }
                    pclose(f);
                    ph_assert(strlen(uuid)==36,NULL);

                    //save this partition
                    int deviceNr=99999;
                    if(sscanf(partitionName,"disk%d",&deviceNr) && deviceNr>1){
                        char deviceStr[10];
                        snprintf(deviceStr,10,"disk%d",deviceNr);
                        mdConfigW=getMdConfigWritable();
                        //find our device
                        struct ps_device *d=NULL;
                        int i;

                        for(i=0;i<mdConfigW->nrDevices;i++)
                            if(!strcmp(mdConfigW->devices[i].device,deviceStr)){
                                d=&(mdConfigW->devices[i]);
                                break;
                            }
                        if(!d){
                            mdConfigW->devices=ph_realloc(mdConfigW->devices,++(mdConfigW->nrDevices)*sizeof(struct ps_device));
                            ph_assert(mdConfigW->devices,NULL);
                            d=&(mdConfigW->devices[mdConfigW->nrDevices-1]);
                            memset(d,0,sizeof(struct ps_device));
                        }
                        d->id=NULL;
                        d->device=ph_strdup(deviceStr);
                        //create a new partition
                        d->partitions=ph_realloc(d->partitions,++d->nrPartitions*sizeof(struct ps_partition));
                        struct ps_partition * __restrict p=&(d->partitions[d->nrPartitions-1]);
                        strcpy(p->name,partitionName+strlen(deviceStr));
                        //strcpy(p->type,type);
                        p->type[0]='\0';
                        strcpy(p->uuid,uuid);
                        p->mountPoint=ph_strdup(mountPoint);
                        LOG0("*** New disk/partition %s%s mounted in %s. UUID: %s. Nr devices: %d",d->device,p->name,p->mountPoint,p->uuid,mdConfigW->nrDevices);
                        releaseMdConfigW(&mdConfigW);

                        //TODO: call partition_added or something
                    } //else this is not an external disk
                }

                if((!strncmp(line,KDiskDisappeared,KDiskDisappearedLen) || !strncmp(line,KDiskUnmountAproval,KDiskUnmountAprovalLen)) &&
                        //disk removed/unmounted. Do we have a disk and a mount?
                        ((start=strstr(line,KPartitionNameStart)) && !strstr(line,KNullVolumePath))){
                    //we have a removed partition
                    char partitionName[50];
                    char mountPoint[50];

                    //find & save the partition
                    start+=2;
                    end=strchr(start,'\'');
                    ph_assert(end,NULL);
                    int l=end-start;
                    memcpy(partitionName,start,l);
                    partitionName[l]='\0';

                    //find & save the mount point
                    start=strstr(line,KVolumePathStart);
                    ph_assert(start,"We do not know the VolumePath from this line: %s",line);
                    start+=strlen(KVolumePathStart);
                    end=strchr(start,'\'');
                    ph_assert(end,NULL);
                    l=end-start;
                    memcpy(mountPoint,start,l);
                    mountPoint[l]='\0';

                    //get the device string
                    int deviceNr=99999;
                    sscanf(partitionName,"disk%d",&deviceNr);
                    char deviceStr[10];
                    snprintf(deviceStr,10,"disk%d",deviceNr);
                    mdConfigW=getMdConfigWritable();
                    //find our device
                    struct ps_device *d=NULL;
                    int i,j;
                    for(i=0;i<mdConfigW->nrDevices;i++)
                        if(!strcmp(mdConfigW->devices[i].device,deviceStr)){
                            d=&(mdConfigW->devices[i]);

                            //find our partition
                            struct ps_partition *p=NULL;
                            memmove(partitionName,partitionName+strlen(deviceStr),strlen(partitionName)-strlen(deviceStr)+1);
                            for(j=0;j<d->nrPartitions;j++)
                                if(!strcmp(d->partitions[j].name,partitionName)){
                                    //we found our partition
                                    p=&d->partitions[j];
                                    ph_assert(!strcmp(p->mountPoint,mountPoint),NULL);
                                    //delete this partition
                                    free(p->mountPoint);
                                    memmove(d->partitions+j,d->partitions+j+1,(d->nrPartitions-j-1)*sizeof(struct ps_partition));
                                    d->nrPartitions--;
                                    if(d->nrPartitions)
                                        d->partitions=ph_realloc(d->partitions,d->nrPartitions*sizeof(struct ps_partition));
                                    else {
                                        free(d->partitions);
                                        d->partitions=NULL;
                                    }
                                }
                            ph_assert(p,NULL); //we assert that we found the partition
                            //if we have no more partitions, remove this device (this is usually the case)
                            if(!d->nrPartitions){
                                free(d->id);
                                free(d->device);
                                ph_assert(!d->partitions,NULL);
                                memmove(mdConfigW->devices+i,mdConfigW->devices+i+1,(mdConfigW->nrDevices-i-1)*sizeof(struct ps_device));
                                mdConfigW->nrDevices--;
                                if(mdConfigW->nrDevices)
                                    mdConfigW->devices=ph_realloc(mdConfigW->devices,mdConfigW->nrDevices*sizeof(struct ps_device));
                                else {
                                    free(mdConfigW->devices);
                                    mdConfigW->devices=NULL;
                                }
                                if(!mdConfigW->nrDevices)
                                    ph_assert(!mdConfigW->devices,NULL);
                            }
                            LOG0("*** Disk/partition %s%s removed from %s. Nr devices remaining: %d",deviceStr,partitionName,mountPoint,mdConfigW->nrDevices);
                        }
                    releaseMdConfigW(&mdConfigW);
                    //ph_assert(d,NULL); -> I do not remember why d should be non-NULL
                }
#else
                //udev handling not implemented
#endif
                } else ph_assert(0,"udev/diskutil socket revents (%d) is different from POLLIN.",(int)pfd[1].revents);
            }
            if(pfd[2].revents){
                if(pfd[2].revents&POLLIN){
                    r=srvAcceptConnection(0);
                    //if(r==1)break;
                    if(shouldExit)break;
                } else ph_assert(pfd[2].revents&POLLNVAL,"Server (http) socket revents (%d) is different from POLLIN (and POLLNVAL).",(int)pfd[2].revents);
            }
            if(pfd[3].revents){
                if(pfd[3].revents&POLLIN){
                    r=srvAcceptConnection(1);
                    //if(r==1)break;
                    if(shouldExit)break;
                } else ph_assert(pfd[3].revents&POLLNVAL,"Server (https) socket revents (%d) is different from POLLIN (and POLLNVAL).",(int)pfd[3].revents);
            }
        }//r>0
    }
#ifdef __APPLE__
    pclose(diskUtilityMonitor);
#endif
    closeAndExit(-1); //-1 means we do not change the shouldExit value
    //we should not reach the return below. It is there just to avoid a warning
    return shouldExit;
}
