#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <limits.h>
#include <ctype.h>

#include <openssl/rand.h>

#ifndef __USE_XOPEN_EXTENDED
#define __USE_XOPEN_EXTENDED
#endif
#include <ftw.h>

#include "photostovis.h"
#include "log.h"

struct mdh_moving {
    char picsDir[PATH_MAX];
    char uploadsDir[PATH_MAX];
    int picsDirLen;
    int uploadsDirLen;
};
static struct mdh_moving *ptrMoving=NULL;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Helper functions, pretty generic
///

static char *escapeString(const char *s){
    int i=0,nrCharsToEscape=0;

    //we need to convert to unsigned, otherwise on Intel unicode chars are converted.
    const unsigned char *ss=(unsigned char *)s;
    //LOG0("################################################## <<%s>> len=%zu",s,strlen(s));
    while(ss[i]!='\0'){
        if(ss[i]=='\\' || ss[i]=='"' || ss[i]<32){ //until 32 we have control characters
            nrCharsToEscape++;
            //LOG0("Escaping char %2d: %2X",i,ss[i]);
        }
        i++;
    }

    if(!nrCharsToEscape)return (char*)s; //no characters to escape
    //LOG0("################################################## We have %d chars to escape.",nrCharsToEscape);

    //if here, we have characters to escape
    int sExcapedLen=i+nrCharsToEscape,pos=0;
    unsigned char *sEscaped=ph_malloc(sExcapedLen+1);
    i=0;
    while(ss[i]!='\0'){
        if(ss[i]=='\\' || ss[i]=='"'){
            sEscaped[pos++]='\\';
            sEscaped[pos++]=ss[i];
        } else if(ss[i]>=32)
            sEscaped[pos++]=ss[i];
        //else we do nothing, because we have a control character
        i++;
    }
    sEscaped[pos]='\0';
    return (char *)sEscaped;
}

#define FREE_ESCAPED_STRING(es,s) if(es!=s)free(es);es=NULL

int filesAreTheSame(const char *fn1, const char *fn2){
    const int KBufLen=1024;
    //returns 1 if the two files are the same, 0 otherwise
    FILE *f1=fopen(fn1,"r");
    if(!f1)return -1;
    FILE *f2=fopen(fn2,"r");
    if(!f2){
        fclose(f1);
        return -1;
    }

    //check the lengths
    fseek(f1,0,SEEK_END);
    int l1=ftell(f1);
    fseek(f2,0,SEEK_END);
    int l2=ftell(f2);

    if(l1!=l2){
        fclose(f1);
        fclose(f2);
        return 0;
    }
    //rewind
    fseek(f1,0,SEEK_SET);
    fseek(f2,0,SEEK_SET);

    int p=0,r1,r2;
    uint8_t b1[KBufLen],b2[KBufLen];
    while(p<l1){
        //LOG0("Here p=%d and l1=%d",p,l1);
        r1=fread(b1,1,KBufLen,f1);
        r2=fread(b2,1,KBufLen,f2);
        ph_assert(r1==r2,NULL);
        p+=r1;
        if(memcmp(b1,b2,r1)){
            //found differences
            fclose(f1);
            fclose(f2);
            return 0;
        }
    }
    //if we are here, we did not find any difference
    fclose(f1);
    fclose(f2);
    return 1;
}


int _unlink_cb_all_in(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf)
{
    /*LOG0return(!ftwbuf->level,0,"rmrfAllIn: skipping %s (path: %s, len: %d, base: %d)",fpath+ftwbuf->base,
               fpath,strlen(fpath),ftwbuf->base);*/
    if(!ftwbuf->level)
        return 0; //skipping "uploads"

    int r=remove(fpath);
    //if(r)perror(fpath);
    LOG0c(r,"Removing %s returned %d",fpath,r);
    return 0;
}

int rmrfAllIn(const char *folder){
    return nftw(folder,&_unlink_cb_all_in,5,FTW_DEPTH|FTW_PHYS);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Helper functions (specific)
///

static void index2string(struct md_entry_id const * const id, char *path, const int pathSize){
    int i,l=0;
    for(i=0;i<id->level/*-1*/;i++){
        l+=snprintf(path+l,pathSize-l,"/%08X",id->nameCRC[i]);
        if(l>pathSize)return;
    }
}

/*
int compareElement(struct md_album_entry const * const __restrict albumElm, const struct md_level_id id){
    //returns -1 if albumElm is earlier than id,
    //0 if the dates are the same
    //1 if albumElm is later than id

    if(albumElm->flags&FlagIsAlbum){
        if(albumElm->earliestCaptureTime<id.captureTime)return -1;
        else if(albumElm->earliestCaptureTime>id.captureTime)return 1;
    } else {
        if(albumElm->captureTime<id.captureTime)return -1;
        else if(albumElm->captureTime>id.captureTime)return 1;
    }
    return 0;
}

int binarySearch(struct md_album_entry const * const __restrict album, const int n, const struct md_level_id id, const int strict){
    int iMin=0,iMax=n-1,iMid,c;
    while(iMax>=iMin){
        iMid=(iMax+iMin)>>1;
        //LOG0("%d %d %d",iMin,iMid,iMax);
        c=compareElement(&album[iMid],id);
        //LOG0("c=%d, %d %d",c,album[iMid].earliestCaptureTime,id.captureTime);
        if(c==0){
            if(isThisElement(&album[iMid],id,strict))return iMid;
            else return -1;
        } else if(c<0)iMin=iMid+1;
        else iMax=iMid-1;
    }
    return -1;
}

int simpleSearch(struct md_album_entry const * const __restrict album, const int n, const uint32_t nameCRC){
    int i;
    for(i=0;i<n;i++)
        if(isThisElement(&album[i],id,strict))return i;
    return -1;
}*/

int getEntryAndPath(struct md_entry_id const * const id, const char *KFolder, char *path, char const ** name,
                    struct md_album_entry const **e, struct mdx_album_entry const **ex)
{
    ph_assert(id,NULL);
    struct ps_md_pics const * const mdPics=getMdPicsRd();
    int pathLen=0;
    int i,j,l,index,currentN=mdPics->rootAlbum->entriesIndex;
    struct md_album_entry *currentAlbum=mdPics->rootAlbum->album,*currentEntry=NULL;
    struct mdx_album_entry *currentEntryX=NULL;
    char *cname=NULL;

    if(e)*e=NULL; //in case we find nothing
    if(ex)*ex=NULL; //in case we find nothing

    for(i=0;i<id->level;i++){
        //we should not trust data coming from the user
        LOG0return(!currentN,-1,"Wrong client data (currentN==0 for level=%d)",i);
        LOG0return(!currentAlbum,-1,"Wrong client data (currentAlbum==NULL for level=%d)",i);

        for(j=0;j<currentN;j++)
            if(currentAlbum[j].nameCRC==id->nameCRC[i])
                break;
        LOG0return(j>=currentN,-1,"Wrong client data: element not found for level %d",i);
        index=j;

        //if here, the index is valid. Check permission
        currentEntry=&currentAlbum[index];
        currentEntryX=&(*(struct mdx_album_entry**)(currentAlbum+currentN))[index];
        LOG0return(id->reqPerm && !((currentEntry->permissions>>(id->reqPerm-1))&1),-1,
                   "The client does NOT have permission to access this album/picture.");

        if(path){
            if(!i){
                //first, we need to copy the rootFolders
                ph_assert(!pathLen,NULL);
                if(KFolder){
                    cname=getNameFromAlbum(mdPics->rootAlbum,0);
                    pathLen=strlen(cname);
                    memcpy(path,cname,pathLen);
                    //add KFolder (starts with /)
                    l=strlen(KFolder);
                    memcpy(path+pathLen,KFolder,l);
                    pathLen+=l;
                    path[pathLen++]='/';
                }
                //else we just get the name of the album
            }
            cname=getNameFromAlbum(currentAlbum,index);
            l=strlen(cname);
            memcpy(path+pathLen,cname,l);
            pathLen+=l;

            path[pathLen++]='/';
        } else if(name)
            cname=getNameFromAlbum(currentAlbum,index);
        if(currentEntry->flags&FlagIsAlbum){
            currentAlbum=currentEntry->album;
            currentN=currentEntry->entriesIndex;
        } else {
            currentAlbum=NULL;
            currentN=0;
        }
    }
    if(id->level==0){
        currentEntry=mdPics->rootAlbum;
        currentEntryX=*(struct mdx_album_entry**)(mdPics->rootAlbum+1);
        if(path){
            ph_assert(!pathLen,NULL);

            if(KFolder){
                cname=getNameFromAlbum(mdPics->rootAlbum,0);
                pathLen=strlen(cname);
                memcpy(path,cname,pathLen);
                //add KFolder
                l=strlen(KFolder);
                memcpy(path+pathLen,KFolder,l);
                pathLen+=l;
                path[pathLen++]='/';
            }
        }
    }
    if(path)
        path[pathLen-1]='\0'; //overwrite last /
    //releaseMdPics(&mdPics); -> we do not release mdPics because the calling function does something with what we return. The calling function should call releaseMdPics
    ph_assert(currentEntry,NULL);
    if(e)*e=currentEntry;
    if(ex)*ex=currentEntryX;
    if(name)*name=cname;
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static int getThumbnailUrl(struct md_album_entry const *e, struct md_thmbstrip_entry *tse, int permissionIndex)
{
    int i,j=-1,k,r=0;
    struct md_album_entry *currentEntry;
    ph_assert(tse->flags&FlagIsAlbum,NULL);
    const uint16_t KFlagHasAThumbnail=FlagHasExifThumbnail|FlagHasScaledThumbnail|FlagIsAlbum;

    ph_assert(e->entriesIndex>0 && e->album,NULL);
    for(i=0;i<e->entriesIndex;i++){
        currentEntry=&(e->album[i]);
        if(permissionIndex){
            uint8_t prm=currentEntry->permissions;
            prm=(prm>>(permissionIndex-1))&1;
            if(!prm)//the user does not see this picture/album, we ignore it
                continue;
        }
        j++;//this is the index the client sees
        if(!(currentEntry->flags&KFlagHasAThumbnail))continue; //this entry does not have a thumbnail
        if(i<tse->currentIndex)continue; //we skip all indexes before currentIndex
        //check if we already added this entry
        for(k=0;k<tse->nrEntries;k++)
            if(j==tse->entries[k].clientIndex){
                if(currentEntry->flags&FlagIsAlbum){
                    if(currentEntry->entriesIndex>0 && currentEntry->album)//go into this album
                        r=getThumbnailUrl(currentEntry,&(tse->entries[k]),permissionIndex);
                } else //skip this picture
                    r=-1;
                break;
            }
        if(k<tse->nrEntries){
            if(r<0)continue; //means we did not find a suitable entry
            //if here, we, perhaps, found a suitable entry
            break;
        }

        //if we are here we can add a thumbnail for this entry
        if(currentEntry->flags&FlagIsAlbum){
            if(currentEntry->entriesIndex>0 && currentEntry->album){
                struct md_thmbstrip_entry thmbEntry;
                memset(&thmbEntry,0,sizeof(struct md_thmbstrip_entry));
                thmbEntry.flags=currentEntry->flags;
                thmbEntry.clientIndex=j;
                thmbEntry.nameCRC=currentEntry->nameCRC;
                r=getThumbnailUrl(currentEntry,&thmbEntry,permissionIndex);
                if(r<0)continue; //means we did not find a suitable entry
                //if here, we found a suitable entry. Add thmbEntry to our list of entries
                tse->entries=ph_realloc(tse->entries,(tse->nrEntries+1)*sizeof(struct md_thmbstrip_entry));
                struct md_thmbstrip_entry *c=&(tse->entries[tse->nrEntries]);
                memcpy(c,&thmbEntry,sizeof(struct md_thmbstrip_entry));
            } else continue; //means we did not find a suitable entry
        } else {
            //this is a picture
            tse->entries=ph_realloc(tse->entries,(tse->nrEntries+1)*sizeof(struct md_thmbstrip_entry));
            struct md_thmbstrip_entry *c=&(tse->entries[tse->nrEntries]);
            c->flags=currentEntry->flags;
            c->clientIndex=j;
            c->nameCRC=currentEntry->nameCRC;
            r=1;
        }
        //if we are here, we found an entry, we can return
        tse->nrEntries++;
        break;
    }
    if(i>=e->entriesIndex){ //we did not find an entry
        if(tse->currentIndex>0){
            tse->currentIndex=0;
            return 0; //we can start again and perhaps we find something
        } else return -1;
    }
    if(r==0){
        return 0; //we can start again and perhaps we find something
    }
    //if here, we found a suitable entry
    tse->nrTotalThumbnails++;
    tse->currentIndex=i+1;
    if(tse->currentIndex==e->entriesIndex)
        tse->currentIndex=0;
    return 1;
}

struct md_thmbstrip_entry *getThmbEntry(struct md_thmbstrip_entry *tse, struct md_album_entry const *e, const int index){
    ph_assert(index>=0 && index<e->entriesIndex,NULL);
    struct md_album_entry *c=&e->album[index];
    uint32_t nameCRC=c->nameCRC;
    int i,place=0;

    //LOG0("getThmbEntry for %08X",nameCRC);
    //is our nameCRC in the album already?
    if(c->flags&FlagIsAlbum)
        for(i=0;i<tse->nrEntries;i++)
            if(tse->entries[i].nameCRC==nameCRC)
                return &tse->entries[i];
    //if here, our nameCRC is NOT in the album already

    //find the place where we should put it
    if(tse->nrEntries>0)
        for(i=0;i<index;i++)
            if(e->album[i].nameCRC==tse->entries[place].nameCRC){
                place++;
                if(place>=tse->nrEntries)
                    break; //means we put it at the end
            }
    //we found the place
    ph_assert(place<=tse->nrEntries,"place=%d nrEntries: %u",place,(unsigned)tse->nrEntries);

    tse->nrEntries++;
    tse->entries=(struct md_thmbstrip_entry *)ph_realloc(tse->entries,tse->nrEntries*sizeof(struct md_thmbstrip_entry));
    //LOG0("getThmbEntry: place=%d out of %d elements. Moving %d elements.",place,tse->nrEntries,tse->nrEntries-1-place);
    memmove(tse->entries+place+1,tse->entries+place,(tse->nrEntries-1-place)*sizeof(struct md_thmbstrip_entry));
    //set it up!
    struct md_thmbstrip_entry *currentTse=&tse->entries[place];
    currentTse->flags=c->flags;
    currentTse->clientIndex=index;
    currentTse->nrTotalThumbnails=0; //??? what to put here?
    currentTse->nrEntries=0;
    currentTse->currentIndex=0; //??? what to put here?
    currentTse->nameCRC=nameCRC;
    currentTse->entries=NULL;
    return currentTse;
}

static int getThumbnailByIndex(struct md_album_entry const *e, struct mdh_album_data_by_permission const *adbp, int index, struct md_thmbstrip_entry *tse){
    int i,current=0,notParsedElement=0;
    int availableNrThumbnails=adbp->nrTotalPictrs+adbp->nrTotalVideos;
    LOG0("getThumbnailByIndex: looking for element %d out of %d",index,availableNrThumbnails);
    ph_assert(index>=0 && index<availableNrThumbnails,NULL);


    for(i=0;i<e->entriesIndex;i++){
        struct md_album_entry *c=&e->album[i];
        ph_assert(current<=index,NULL);
        if(c->flags&FlagIsAlbum){
            //LOG0("Element %d is an album.",i);
            LOG0close_continue(!c->album,notParsedElement=1,"Album element not parsed yet, skipping.");
            struct mdh_album_data_by_permission *adbpCurrent=getAlbumDataByPermission(c);
            int availableNrThumbnailsInCurrentAlbum=adbpCurrent->nrTotalPictrs+adbpCurrent->nrTotalVideos;
            if(current+availableNrThumbnailsInCurrentAlbum<=index){
                //the searched thumbnail is not in this sub-album
                current+=availableNrThumbnailsInCurrentAlbum;
                continue;
            }

            //if we are here, the searched thumbnail can be found in this subalbum
            return getThumbnailByIndex(c,adbpCurrent,index-current,getThmbEntry(tse,e,i));
        } else {
            //LOG0("Element %d is a picture or a video.",i);
            if(current!=index){
                //it is not this one!
                current++;
                continue;
            }

            //we found it! Is it suitable?
            if(!(c->flags&(FlagHasScaledThumbnail+FlagHasExifThumbnail))){
                LOG0("Element %d is not suitable (does not have a thumbnail)",i);
                return 0; //not suitable because it does not have a thumbnail
            }

            //if we are here, this is the one and it is suitable!
            getThmbEntry(tse,e,i);

            return 1;
        }
    }
    ph_assert(notParsedElement,NULL); //we can get here only if some element is not yet parsed
    return 0;
}

static int getThumbnailUrl2(struct md_album_entry const *e, struct md_thmbstrip_entry *tse, int permissionIndex, const int reqNrThumbnails){
    int i,j=-1,r;
    ph_assert(tse->flags&FlagIsAlbum,NULL);
    struct mdh_album_data_by_permission *adbp=getAlbumDataByPermission(e);
    int nrThumbnailsToGet=reqNrThumbnails,availableNrThumbnails=adbp->nrTotalPictrs+adbp->nrTotalVideos;
    unsigned int *thumbnailIndexes;

    //TODO: permission index not used!

    LOG0("getThumbnailUrl2: Number of all thumbnails (pictures+videos) in this album: %d",availableNrThumbnails);
    if(availableNrThumbnails<nrThumbnailsToGet)nrThumbnailsToGet=availableNrThumbnails; //cannot get more thumbnails than we have in the album
    thumbnailIndexes=(unsigned int*)malloc(nrThumbnailsToGet*sizeof(unsigned int));

    //get the pictures
    LOG0("getThumbnailUrl2: nrThumbnailsToGet=%d",nrThumbnailsToGet);
    for(i=0;i<nrThumbnailsToGet;i++){
        //generate a random number
        r=RAND_bytes((unsigned char *)(thumbnailIndexes+i),4);
        thumbnailIndexes[i]%=availableNrThumbnails;
        LOG0("Index %u/%u",thumbnailIndexes[i],availableNrThumbnails);
        //is it a duplicate?
        for(j=0;j<i;j++)
            if(thumbnailIndexes[i]==thumbnailIndexes[j])
                break;//duplicate found, retry
        if(j<i){
            i--;
            continue;
        }

        //get this thumbnail
        r=getThumbnailByIndex(e,adbp,thumbnailIndexes[i],tse);
        if(r==0){
            LOG0("ERROR getting thumbnail with index %d. Skipping.",thumbnailIndexes[i]);
            if(availableNrThumbnails==nrThumbnailsToGet)
                nrThumbnailsToGet--;
            availableNrThumbnails--;
            //TODO: make a list with unsuitable elements (that failed)
            //try to get another thumbnail
            i--;
            continue;
        }
        ph_assert(r==1,NULL);
    }
    //if here we got all the thumbnails!
    free(thumbnailIndexes);
    //LOG0("Retrieved thumbnails: %d (requested: %d. Available: %d)",nrThumbnailsToGet,reqNrThumbnails,availableNrThumbnails);

    return nrThumbnailsToGet;
}

static void printAndFreeThumbnailUrl(char *thmBuf, int *thmBufLen, const int thmBufSize, struct md_thmbstrip_entry *tse, int comaBefore,
                                     char *reqUrl, int reqUrlLen, const int reqUrlSize){
    int r=snprintf(reqUrl+reqUrlLen,reqUrlSize-reqUrlLen,"/%08X",tse->nameCRC);
    ph_assert(r>=0,NULL);
    ph_assert(r<reqUrlSize-reqUrlLen,NULL);
    if(comaBefore)thmBuf[(*thmBufLen)++]=',';

    if(tse->flags&FlagIsAlbum){
        int i;
        thmBuf[(*thmBufLen)++]='[';

        for(i=0;i<tse->nrEntries;i++)
            printAndFreeThumbnailUrl(thmBuf,thmBufLen,thmBufSize,&(tse->entries[i]),i,reqUrl,reqUrlLen+r,reqUrlSize);

        thmBuf[(*thmBufLen)++]=']';
        thmBuf[(*thmBufLen)]='\0';

        free(tse->entries);
        tse->entries=NULL;
    } else {
        //picture
        //we need to send the flags because they contain info about rotation
        r=snprintf(thmBuf+(*thmBufLen),thmBufSize-(*thmBufLen),"{\"fl\":%u,\"t\":\"%s\"}",(unsigned int)tse->flags,reqUrl);
        //LOG0c(tse->flags&FlagHasScaledThumbnail,"!!!!!!!!!!>>>>>>>>>>>>>>>>> Picture %s has scaled thumbnail!",reqUrl)
        ph_assert(r>=0,NULL);
        ph_assert(r<thmBufSize-(*thmBufLen),"r=%d thmBufSize=%d thmBufLen=%d",r,thmBufSize,*thmBufLen);
        *thmBufLen+=r;
    }

    reqUrl[reqUrlLen]='\0';
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

char *getAlbumJsonHttp(struct md_entry_id const * const reqId, const char *reqKey, const char *reqETag, const int reqNrThumbnails, const int pixelRatio,
                       const int bufferOffset, char *eTag, int eTagSize, struct md_level_etagf * const albumETag, size_t *bufLen){
    int i,j,r,n;
    struct md_album_entry const *e;
    struct mdx_album_entry const *ex;
    char const *albumName=NULL;
    char *tEscaped;
    const char *KNothing="";
    char albumPath[PATH_MAX];
    albumPath[0]='\0';

    r=getEntryAndPath(reqId,NULL,albumPath,&albumName,&e,&ex); //e and ex are part of mdAll, we do not free it
    //we MUST call releaseMdPics(NULL) before returning from this function
    LOG0close_return(r,NULL,releaseMdPics(NULL),"WARNING: getAlbumJsonHttp: We were unable to find the requested ID.");
    LOG0close_return(!(e->flags&FlagIsAlbum),NULL,releaseMdPics(NULL),"WARNING: getAlbumJsonHttp: requested element is not an album");
    LOG0close_return(!e->album,NULL,releaseMdPics(NULL),"WARNING: getAlbumJsonHttp: requested element has no album part");
    LOG0(">>>>>>>>>> REQUESTED ALBUM: %s",albumPath);

    //write the eTag
    albumETag->nameCRC=e->nameCRC;
    albumETag->captureTime=e->earliestCaptureTime;
    albumETag->sizeOrLatestCaptureTime=e->latestCaptureTime;
    albumETag->modificationTime=e->modificationTime;
    albumETag->flags=e->flags;
    albumETag->modificationCounter=ex->modificationCounter;
    r=snprintf(eTag,eTagSize,"%08X%08X%08X%08X%04X%04X",e->nameCRC,(unsigned int)e->earliestCaptureTime,(unsigned int)e->latestCaptureTime,
               (unsigned int)e->modificationTime,(unsigned int)e->flags,(unsigned int)ex->modificationCounter);
    ph_assert(r<eTagSize,NULL);
    //LOG0close_return(reqETag && !strcmp(eTag,reqETag),(char*)reqETag,releaseMdPics(NULL),"getAlbumJsonHttp: Requested album has the same ETAG.");

    n=e->entriesIndex;

    ph_assert(reqId->reqPerm==0,NULL); //TODO: implement this for other permissions
    struct mdh_album_data_by_permission const *adbp=&(getAlbumDataByPermission(e)[reqId->reqPerm]);

    //max URL obj can be: {"fl":65535,"t":"/t
    // /FF00FF00 x 7 times "}, : 22+7*9=approx 100 characters. We can have [ and ] for each level, in total 14 additional chars.
    // Lets be on the safe side and put 100+14+safety=128
    const int KThmEntrySize=128;
    const int thmBufSize=reqNrThumbnails*KThmEntrySize;

    int bSize=bufferOffset+256+(128+thmBufSize)*n+1024;
    int bPos=0,l;
    char *bWithoutOffset=(char*)ph_malloc(bSize);
    char *b=bWithoutOffset+bufferOffset;

    //do we have an album with a key? If yes, find the base layer
    int baseLayer=0;
    if(reqKey){
        LOG0("key (%s) length: %zu (-40): %zd",reqKey,strlen(reqKey),(ssize_t)strlen(reqKey)-40);
        ph_assert((strlen(reqKey)-40)%8==0,NULL);
        baseLayer=(strlen(reqKey)-40)/8; // /8
    }
    ph_assert(baseLayer<=reqId->level,"%d<=%d",baseLayer,(int)reqId->level);

    if(albumName)tEscaped=escapeString(albumName);
    else tEscaped=(char*)KNothing;
    //write the album specific part
    LOG0("Components of l: level(%d)-baseLayer(%d)",(int)reqId->level,baseLayer);
    if(reqId->reqPerm)
        l=snprintf(b,bSize,"{\"a\":{\"id\":\"%08X\",\"t\":\"%s\",\"no\":%u,\"l\":%d,\"n\":%d,\"fl\":%u,\"p\":%d,\"v\":%d,\"a\":%d,\"ec\":%d,\"lc\":%d,"
                "\"sp\":%"UINT64_FMT",\"sv\":%"UINT64_FMT"},\n\"e\":[\n",
                e->nameCRC,tEscaped,e->flags&FlagHasNameOffset?ex->nameOffset:0,reqId->level-baseLayer,adbp->nrEntries,(unsigned int)e->flags,
                adbp->nrTotalPictrs,adbp->nrTotalVideos,adbp->nrTotalAlbums,
                adbp->earliestCaptureTime,adbp->latestCaptureTime,
                adbp->sizeOfPictrsInAlbum,adbp->sizeOfVideosInAlbum);
    else {
        ph_assert(adbp->nrTotalPictrs+adbp->nrTotalVideos+adbp->nrTotalAlbums>=n,NULL);
        uint64_t sz=((uint64_t)adbp->sizeOfUnknownsInAlbumHi<<32)+adbp->sizeOfUnknownsInAlbumLo;
        l=snprintf(b,bSize,"{\"a\":{\"id\":\"%08X\",\"t\":\"%s\",\"no\":%u,\"l\":%d,\"n\":%d,\"fl\":%u,\"p\":%d,\"v\":%d,\"a\":%d,\"u\":%d,\"ec\":%d,\"lc\":%d,"
                "\"sp\":%"UINT64_FMT",\"sv\":%"UINT64_FMT",\"su\":%"UINT64_FMT"},\n\"e\":[\n",
                e->nameCRC,tEscaped,e->flags&FlagHasNameOffset?ex->nameOffset:0,reqId->level-baseLayer,n,(unsigned int)e->flags,
                adbp->nrTotalPictrs,adbp->nrTotalVideos,adbp->nrTotalAlbums,adbp->nrTotalUnknowns,
                e->earliestCaptureTime,e->latestCaptureTime,
                adbp->sizeOfPictrsInAlbum,adbp->sizeOfVideosInAlbum,sz);
    }
    if(albumName){
        FREE_ESCAPED_STRING(tEscaped,albumName);
    }

    bPos+=l;
    if(bSize-bPos<1024){
        bWithoutOffset=ph_realloc(bWithoutOffset,(bSize+=1024));
        b=bWithoutOffset+bufferOffset;
    }

    //write album elements
    if(e->album && n>0)for(i=0;i<n;i++){
        //shortcuts
        struct md_album_entry const * const c=&e->album[i];
        struct mdx_album_entry const * const cx=getExtraEntry(e->album,e->entriesIndex,i);

        //should we add this element?
        if(reqId->reqPerm && !(c->permissions&(1<<(reqId->reqPerm-1)))){
            LOG0("Skipping element!");
            //TODO: this needs to be verified
            continue;
        }
        //if here we can add this element

        //more shortcuts
        char *t=getNameFromAlbum(e->album,i);
        //LOG0("Name, not escaped: %s",t);

        tEscaped=escapeString(t);
        //LOG0("Name,   escaped  : %s",tEscaped);

        if(c->flags&FlagIsAlbum){
            if(c->album){
                struct mdh_album_data_by_permission const *cAdbp=&(getAlbumDataByPermission(c)[reqId->reqPerm]);
                if(reqId->reqPerm)
                    l=snprintf(b+bPos,bSize-bPos,
                               "{\"id\":\"%08X\",\"t\":\"%s\",\"fl\":%u,\"mt\":%d,\"p\":%u,\"v\":%u,\"a\":%u,\"ec\":%d,\"lc\":%d,\"sp\":%"UINT64_FMT",\"sv\":%"UINT64_FMT",\"str\":",
                               c->nameCRC,tEscaped,(unsigned int)c->flags,c->modificationTime,
                               cAdbp->nrTotalPictrs,cAdbp->nrTotalVideos,cAdbp->nrTotalAlbums,
                               (int)c->earliestCaptureTime,(int)c->latestCaptureTime,cAdbp->sizeOfPictrsInAlbum,cAdbp->sizeOfVideosInAlbum);
                else {
                    uint64_t sizeOfUnknownsInAlbum=(((uint64_t)cAdbp->sizeOfUnknownsInAlbumHi)<<32)+cAdbp->sizeOfUnknownsInAlbumLo;
                    l=snprintf(b+bPos,bSize-bPos,
                               "{\"id\":\"%08X\",\"t\":\"%s\",\"fl\":%u,\"mt\":%d,\"p\":%u,\"v\":%u,\"a\":%u,\"u\":%u,\"ec\":%d,\"lc\":%d,\"sp\":%"UINT64_FMT",\"sv\":%"UINT64_FMT",\"su\":%"UINT64_FMT",\"str\":",
                               c->nameCRC,tEscaped,(unsigned int)c->flags,c->modificationTime,
                               cAdbp->nrTotalPictrs,cAdbp->nrTotalVideos,cAdbp->nrTotalAlbums,cAdbp->nrTotalUnknowns,
                               (int)c->earliestCaptureTime,(int)c->latestCaptureTime,
                               cAdbp->sizeOfPictrsInAlbum,cAdbp->sizeOfVideosInAlbum,sizeOfUnknownsInAlbum);
                }
                bPos+=l;

                //find the thumbnails for the thumbnail strip
                struct md_thmbstrip_entry tse;
                memset(&tse,0,sizeof(struct md_thmbstrip_entry));
                tse.flags=c->flags;
                if(c->entriesIndex>0){
                    if(statusFlagsCheck(StatusFlagPictureScanThreadActive)){
                        //picture scan in progress, we get the first thumbnails in each album and not some random thumbnails
                        while(tse.nrTotalThumbnails<reqNrThumbnails){
                            r=getThumbnailUrl(c,&tse,reqId->reqPerm);
                            if(r<0)break;
                        }
                        LOG0("Thumbnails found for element %s: %d/%d (during picture scanning)",t,tse.nrTotalThumbnails,reqNrThumbnails);
                    } else {
                        if(baseLayer+reqId->level)
                            tse.nrTotalThumbnails=getThumbnailUrl2(c,&tse,reqId->reqPerm,reqNrThumbnails);
                        else {
                            tse.nrTotalThumbnails=getThumbnailUrlFromCache(c->nameCRC,&tse,reqId->reqPerm,reqNrThumbnails);
                            if(tse.nrTotalThumbnails==(uint16_t)-1)
                                tse.nrTotalThumbnails=getThumbnailUrl2(c,&tse,reqId->reqPerm,reqNrThumbnails);
                        }
                        LOG0("Thumbnails found for element %s: %d/%d",t,tse.nrTotalThumbnails,reqNrThumbnails);
                    }
                }

                //we collected the thumbnail URLs, now lets generate the JSON data
                char requestUrl[KThmEntrySize];
                requestUrl[0]='/';
                requestUrl[1]='t';
                requestUrl[2]='\0'; //just in case reqId->level==0
                for(j=0;j<reqId->level-baseLayer;j++)
                    sprintf(requestUrl+2+j*9,"/%08X",reqId->nameCRC[j+baseLayer]);
                l=2+(reqId->level-baseLayer)*9;
                tse.nameCRC=c->nameCRC;
                //LOG0("REQUEST URL (%d): %s (length=%d)",reqId->level-baseLayer,requestUrl,l);

                //cacheThumbnails(reqId,&tse,pixelRatio,0);
                printAndFreeThumbnailUrl(b,&bPos,bSize,&tse,0,requestUrl,l,KThmEntrySize);
                ph_assert(tse.entries==NULL,NULL);
            } else {
                //we need a different approach, because if the album is NULL we do not have cAdbp
                if(reqId->reqPerm)
                    l=snprintf(b+bPos,bSize-bPos,
                               "{\"id\":\"%08X\",\"t\":\"%s\",\"fl\":%u,\"mt\":%d,\"p\":0,\"v\":0,\"a\":0,\"ec\":%d,\"lc\":%d,\"sp\":0,\"sv\":0",
                               c->nameCRC,tEscaped,(unsigned int)c->flags,c->modificationTime,(int)c->earliestCaptureTime,(int)c->latestCaptureTime);
                else {
                    l=snprintf(b+bPos,bSize-bPos,
                               "{\"id\":\"%08X\",\"t\":\"%s\",\"fl\":%u,\"mt\":%d,\"p\":0,\"v\":0,\"a\":0,\"u\":0,\"ec\":%d,\"lc\":%d,\"sp\":0,\"sv\":0,\"su\":0",
                               c->nameCRC,tEscaped,(unsigned int)c->flags,c->modificationTime,(int)c->earliestCaptureTime,(int)c->latestCaptureTime);
                }
                bPos+=l;
            }

        } else {
            //stuff common for video and pictures
            l=snprintf(b+bPos,bSize-bPos,"{\"id\":\"%08X\",\"c\":%d,\"fl\":%u,\"mt\":%d,\"m\":%u,\"fn\":\"%s\",\"sz\":%u,\"tz\":%d,\"w\":%u,\"h\":%u",
                       c->nameCRC,(int)c->captureTime,(unsigned int)c->flags,c->modificationTime,(unsigned int)c->entriesIndex,tEscaped,c->size,
                       (int)cx->timezone,(unsigned int)cx->width,(unsigned int)cx->height);
            bPos+=l;
            if(c->flags&FlagIsVideo){
                //video-specific: duration
                l=snprintf(b+bPos,bSize-bPos,",\"dur\":%u",cx->duration);
            } else {
                //picture-specific
                l=snprintf(b+bPos,bSize-bPos,",\"fln\":%f,\"fl35\":%u,\"et\":%f,\"ep\":%u,\"fnr\":%f,\"iso\":%u",
                           cx->focalLength,(unsigned int)cx->focalLength35,cx->exposureTime,(unsigned int)cx->exposureProgram,cx->fNumber,(unsigned int)cx->iso);
            }
            bPos+=l;
        }
        FREE_ESCAPED_STRING(tEscaped,t);
        if(c->flags&FlagPhotoHasGPSInfo){
            l=snprintf(b+bPos,bSize-bPos,",\"lat\":%d,\"lng\":%d",(int)c->lat,(int)c->lng);
            bPos+=l;
        }

        if(c->flags&FlagHasNameOffset){
            l=snprintf(b+bPos,bSize-bPos,",\"no\":%u",(unsigned int)cx->nameOffset);
            bPos+=l;
        }
        /*
        //write comments & stuff to JSON
        char *filename=t;
        //common for both files and albums/folders
        if(ce->flags&FlagHasDescriptionTxtEntry){
            if(ce->flags&FlagHasDescription){
                t+=strlen(t)+1;
                tEscaped=escapeString(t);
                writeImageDescriptionAndTitle(f,ce,filename,tEscaped);
                FREE_ESCAPED_STRING(tEscaped,t);
            } else
                writeImageDescriptionAndTitle(f,ce,filename,NULL);
        } else
            if(ce->flags&FlagHasDescription){
                t+=strlen(t)+1;
                tEscaped=escapeString(t);
                fprintf(f,",\"d\":{\"xx\":\"%s\"}",tEscaped);
                FREE_ESCAPED_STRING(tEscaped,t);
            }
            */
        if(c->flags&FlagHasDescription){
            t+=strlen(t)+1;
            tEscaped=escapeString(t);
            LOG0("Description: Escaped: <<%s>> Original: <<%s>>",tEscaped,t);
            l=snprintf(b+bPos,bSize-bPos,",\"uc\":\"%s\"",tEscaped);
            bPos+=l;
            FREE_ESCAPED_STRING(tEscaped,t);
        }
        if(c->flags&FlagHasUserComment){
            t+=strlen(t)+1;
            tEscaped=escapeString(t);
            LOG0("User comment: Escaped: <<%s>> Original: <<%s>>",tEscaped,t);
            l=snprintf(b+bPos,bSize-bPos,",\"uc\":\"%s\"",tEscaped);
            bPos+=l;
            FREE_ESCAPED_STRING(tEscaped,t);
        }
        if(i==n-1){
            //last element
            if(bufferOffset)
                l=snprintf(b+bPos,bSize-bPos,"}],\"type\":\"album\"}\n");
            else
                l=snprintf(b+bPos,bSize-bPos,"}]}\n");
        } else
            l=snprintf(b+bPos,bSize-bPos,"},\n");
        bPos+=l;

        //realloc our buffer if it becomes too small
        if(bSize-bPos<1024){
            bWithoutOffset=ph_realloc(bWithoutOffset,(bSize+=1024));
            b=bWithoutOffset+bufferOffset;
        }
    }//for(i...
    else {
        //terminate the buffer
        if(bufferOffset)
            l=snprintf(b+bPos,bSize-bPos,"],\"type\":\"album\"}\n");
        else
            l=snprintf(b+bPos,bSize-bPos,"]}\n");
        bPos+=l;
    }

    //we are done
    releaseMdPics(NULL);
    *bufLen=bPos;

    if(n>0)
        LOG0("INFO: JSON album buffer: allocated: %d, used: %d, unused: %d, nr elements:%d, used bytes/element: %d, UNUSED bytes/element: %d",
             bSize,bPos,bSize-bPos,n,bPos/n,(bSize-bPos)/n);
    else
        LOG0("INFO: JSON album buffer: allocated: %d, used: %d, unused: %d, nr elements:%d (ZERO)",
             bSize,bPos,bSize-bPos,n);
    LOG0("Album JSON: %s",b);
    return bWithoutOffset;
}


/*

char *getThumbnailStripJsonHttp(struct md_entry_id const * const id, size_t *bufLen, char *eTag, int eTagSize, const char *reqETag,
                                int nrThumbnails, const char *reqUrl)
{
    int r;
    struct md_album_entry const *e;
    struct mdx_album_entry const *ex;
    r=getEntryAndPath(id,NULL,NULL,&e,&ex); //e is part of mdAll, we do not free it
    //we MUST call releaseMdPics(NULL) before returning from this function
    LOG0close_return(!e,NULL,releaseMdPics(NULL),"WARNING: requested album not found.");
    LOG0close_return(!(e->flags&FlagIsAlbum),NULL,releaseMdPics(NULL),"WARNING: requested entry is not an album!");

    //write the eTag
    r=snprintf(eTag,eTagSize,"%08X%08X%08X%08X%04X%04X",e->nameCRC,(unsigned int)e->earliestCaptureTime,(unsigned int)e->latestCaptureTime,
               (unsigned int)e->modificationTime,(unsigned int)e->flags,(unsigned int)ex->modificationCounter);
    ph_assert(r<eTagSize,NULL);
    LOG0("€€€€€€€€€€€€€€€€€ getThumbnailStripJsonHttp: mt: %d, mc: %d (req etag: %s)",e->modificationTime,ex->modificationCounter,reqETag);

    LOG0close_return(reqETag && !strcmp(eTag,reqETag),(char*)reqETag,releaseMdPics(NULL),"getThumbnailStripJsonHttp: requested album has the same ETAG.");

    struct md_thmbstrip_entry tse;
    memset(&tse,0,sizeof(struct md_thmbstrip_entry));
    tse.flags=e->flags;
    if(e->entriesIndex>0 && e->album)
        while(tse.nrTotalThumbnails<nrThumbnails){
            r=getThumbnailUrl(e,&tse,id->reqPerm);
            if(r<0)break;
        }
    releaseMdPics(NULL);
    LOG0("Thumbnails found: %d/%d",tse.nrTotalThumbnails,nrThumbnails);

    //we collected the thumbnail URLs, now lets generate the JSON data
    //max URL obj can be: {"fl":65535,"t":"/t
    // /FF00FF00 x 7 times "}, : 22+7*9=approx 100 characters. We can have [ and ] for each level, in total 14 additional chars.
    // Lets be on the safe side and put 100+14+safety=128
    const int KThmEntrySize=128;
    const int thmBufSize=nrThumbnails*KThmEntrySize;
    char *thmBuf=ph_malloc(thmBufSize+1);
    int thmBufLen=0,l=strlen(reqUrl);
    char requestUrl[KThmEntrySize];
    char *slashS;
    memcpy(requestUrl,reqUrl,l+1);
    ph_assert(l>=3,NULL);
    ph_assert(requestUrl[1]=='s',NULL);

    //process the request URL a bit
    requestUrl[1]='t';
    if(requestUrl[l-1]=='/')
        requestUrl[--l]='\0';
    //remove the last id from the URL and put it into the tse.id
    slashS=strrchr(requestUrl,'/');
    LOG0close_return(!slashS,NULL,free(thmBuf),"WARNING: malformed request URL (%s)",requestUrl);
    *slashS='\0';
    slashS++;
    LOG0close_return(strlen(slashS)!=8,NULL,free(thmBuf),"WARNING: malformed request ID: (%s)",slashS);
    sscanf(slashS,"%08X",&tse.nameCRC);

    l=strlen(requestUrl);
    //LOG0("Our requestUrl: %s [%d] (%s)",requestUrl,l,reqUrl);
    printAndFreeThumbnailUrl(thmBuf,&thmBufLen,thmBufSize,&tse,0,requestUrl,l,KThmEntrySize);
    ph_assert(tse.entries==NULL,NULL);

    *bufLen=thmBufLen;
    //LOG0("Sending: %s",thmBuf);
    ph_assert(*bufLen==strlen(thmBuf),"%d!=%d",*bufLen,strlen(thmBuf));

    return thmBuf;
}*/



char *getImageHttp(struct md_entry_id const * const id, size_t *bufLen, char *eTag, int eTagSize, const char *reqETag, int requestedWidth, int requestedHeight,
                   struct ps_session const * const mySession){
    ph_assert(mySession->averageViewTime>=MIN_AVERAGE_VIEW_TIME,NULL);
    ph_assert(mySession->averageViewTime<=MAX_AVERAGE_VIEW_TIME,NULL);

    char path[PATH_MAX];
    struct md_album_entry const *e;
    struct mdx_album_entry const *ex;
    int r=getEntryAndPath(id,KPicturesFolder,path,NULL,&e,&ex);
    //we MUST call releaseMdPics(NULL) before returning from this function
    LOG0close_return(!e,NULL,releaseMdPics(NULL),"WARNING: requested image not found.");
    LOG0close_return(e->flags&FlagIsAlbum,NULL,releaseMdPics(NULL),"WARNING: requested entry is an album, not an image!");
    LOG0close_return(e->flags&FlagIsVideo,NULL,releaseMdPics(NULL),"WARNING: requested entry is a video, not an image!");

    //is this image JPEG or something else?
    char *ext=strrchr(path,'.');
    if(ext && !strcasecmp(ext,".heic")){
        //this is HEIC image, retarget to transcoded jpeg
        char path2[PATH_MAX];
        strcpy(path2,path);
        getTranscodedPathForMedia(path2,"jpeg",path);
        LOG0("REDIRECTING TO TRANSCODED JPEG");
    }

    //in how many seconds do we estimate this image will be transmitted?
    uint8_t scaleFactorNum=8; //(valid values: 1-8). Denominator is 8
    uint32_t fileSize=e->size;
    uint32_t targetSize=fileSize;
    ph_assert(!(e->flags&FlagSizeIsInMB),NULL);
    computeTargetImgParameters(fileSize,ex->width,ex->height,requestedWidth,requestedHeight,mySession,&targetSize,&scaleFactorNum);

    //write the eTag
    r=snprintf(eTag,eTagSize,"%08X%08X%08X%08X%02X",e->nameCRC,(unsigned int)e->captureTime,fileSize,(unsigned int)e->modificationTime,(unsigned int)scaleFactorNum);
    ph_assert(r<eTagSize,NULL);
    releaseMdPics(NULL); //locked inside getEntryAndMD5
    e=NULL;

    ph_assert(r==strlen(eTag),NULL);
    if(reqETag && r==strlen(reqETag) && !strncmp(eTag,reqETag,r-1)){
        //we COULD have the same/similar tag
        if(reqETag[r-1]>=eTag[r-1]){
            //if this is true it means that the cached image on the client size has a better quality than what we wouls provide
            cancelPrefetchedImage(path,scaleFactorNum);
            return (char*)reqETag;
        }
    }

    //if we are here, we need to get and return the image
    return getPrefetchedImage(path,bufLen,scaleFactorNum,fileSize);
}


//entries that are OK have a thumbnail that does NOT need rotation
static int entryOkForFacebookThumbnail(uint16_t f){
    if((f&FlagHasExifThumbnail || f&FlagHasScaledThumbnail) &&
            !(f&FlagRotatePlus90 || f&FlagRotatePlus180))return 1;
    return 0;
}

char *getAlbumOgProperties(struct md_entry_id const * const id, const char *reqKey, size_t *bufLen, const char *reqUrl){
    int l,i;
    char path[PATH_MAX];
    struct md_album_entry const *e;
    struct mdx_album_entry const *ex;
    getEntryAndPath(id,KPicturesFolder,path,NULL,&e,&ex);
    //we MUST call releaseMdPics(NULL) before returning from this function
    LOG0close_return(!e,NULL,releaseMdPics(NULL);index2string(id,path,PATH_MAX),
            "WARNING: getAlbumOgProperties: We were unable to find the path for this index: %s",path);
    LOG0close_return(!(e->flags&FlagIsAlbum),NULL,releaseMdPics(NULL),"WARNING: getAlbumOgProperties: requested element is not an album");
    LOG0close_return(!e->album,NULL,releaseMdPics(NULL),"WARNING: getAlbumOgProperties: requested element has no album part");

    LOG0("getAlbumOgProperties++");

    char *t=NULL;
    if(id->level>0){
        t=strrchr(path,'/');
        if(t && ex->nameOffset>0)
            t+=ex->nameOffset;
    }
    if(!t)
        t="Photostovis";
    else while(*t==' ')t++;
    LOG0("Album name: <<%s>>",t);

    const char *KFbOgStart=
            "<!DOCTYPE html><html><head>\r\n"
            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n"
            "<meta property=\"og:site_name\" content=\"Photostovis\" />\r\n"
            "<meta property=\"og:title\" content=\"%s\" />\r\n"
            "<meta property=\"og:type\" content=\"photostovis:photo_album\" />\r\n"
            "<meta property=\"og:url\" content=\"http://photostovis.net/%s%s%s\" />\r\n"
            "<meta property=\"og:description\" content=\"Photo album containing pictures and/or videos.\" />\r\n";
    const char *KFbOgEnd="</head></html>\r\n";
    const char *KFbPicture="<meta property=\"og:image\" content=\"http://photostovis.net/%s%s%%08X%s\" />\r\n";
    struct ps_md_config const * const mdConfig=getMdConfigRd();

    //do we have an album with a key? If yes, find the base layer
    //int baseLayer=0;
    char *keyQuery=NULL;
    size_t reqKeyLen=0,keyQueryLen=0;
    if(reqKey){
        reqKeyLen=strlen(reqKey);
        LOG0("key (%s) length: %zu (-40): %zd",reqKey,reqKeyLen,(ssize_t)reqKeyLen-40);
        ph_assert((reqKeyLen-40)%8==0,NULL);
        //baseLayer=(reqKeyLen-40)/8; // /8

        const char *KKQ="?key=";
        const size_t KKQlen=strlen(KKQ);
        keyQueryLen=KKQlen+reqKeyLen;
        keyQuery=ph_malloc(keyQueryLen+1);
        memcpy(keyQuery,KKQ,KKQlen);
        memcpy(keyQuery+KKQlen,reqKey,reqKeyLen+1);
    } else {
        keyQuery=ph_malloc(1);
        keyQuery[0]='\0';
    }
    //ph_assert(baseLayer<=id->level,NULL);


    //allocate the buffer
    int nrPics=0,nrAddedPics=0;
    //find pictures with thumbnails and which do NOT need rotation
    for(i=0;i<e->entriesIndex;i++)
        if(entryOkForFacebookThumbnail(e->album[i].flags)){
            nrPics++;
            if(nrPics>=3)break;
        }

    const char *reqUrl2=(strlen(reqUrl)>2)?reqUrl:"/a/";
    ph_assert(reqUrl2[0]=='/',"%s",reqUrl2);
    ph_assert(reqUrl2[1]=='a',"%s",reqUrl2);

    int picsUrlLen=strlen(KFbPicture)+strlen(mdConfig->phonetUsername)+strlen(reqUrl2)+8+10+keyQueryLen; //+8+10 for the integer part (last index)
    char *picsUrl=ph_malloc(picsUrlLen);
    snprintf(picsUrl,picsUrlLen,KFbPicture,mdConfig->phonetUsername,reqUrl2,keyQuery);

    l=strlen(KFbOgStart)+strlen(KFbOgEnd)+strlen(t)+strlen(mdConfig->phonetUsername)+strlen(reqUrl2)+keyQueryLen+nrPics*picsUrlLen+1;
    char *b=ph_malloc(l);

    //put the basic og metadata
    *bufLen=snprintf(b,l,KFbOgStart,t,mdConfig->phonetUsername,reqUrl2,keyQuery);
    releaseMdConfig(&mdConfig);
    free(keyQuery);

    //add picture URLs
    t=strstr(picsUrl,"/a/");
    ph_assert(t,NULL);
    t[1]='t';
    LOG0("PicsUrl: %s",picsUrl);
    for(i=0;i<e->entriesIndex;i++)
        if(entryOkForFacebookThumbnail(e->album[i].flags)){
            l=sprintf(b+(*bufLen),picsUrl,e->album[i].nameCRC);
            *bufLen+=l;
            nrAddedPics++;
            if(nrAddedPics==nrPics)break;
        }
    free(picsUrl);
    releaseMdPics(NULL);
    e=NULL;
    ex=NULL;

    //add end of page
    l=strlen(KFbOgEnd)+1;
    memcpy(b+(*bufLen),KFbOgEnd,l);
    *bufLen+=l;

    LOG0("Answering: %s",b);
    return b;
}

char *getImageOgProperties(struct md_entry_id const * const id, const char *reqKey, size_t *bufLen, const char *reqUrl){
    struct md_album_entry const *e;
    getEntryAndPath(id,NULL,NULL,NULL,&e,NULL);
    //we MUST call releaseMdPics(NULL) before returning from this function
    LOG0close_return(!e,NULL,releaseMdPics(NULL),"WARNING: We were unable to find the requested index");

    LOG0close_return(e->flags&FlagIsAlbum,NULL,releaseMdPics(NULL),"WARNING: requested entry is an album, not an image!");
    LOG0close_return(e->flags&FlagIsVideo,NULL,releaseMdPics(NULL),"WARNING: requested entry is a video, not an image!");

    releaseMdPics(NULL);
    e=NULL;

    const char *KFbOgPhoto=
            "<!DOCTYPE html><html><head>\r\n"
            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n"
            "<meta property=\"og:site_name\" content=\"Photostovis\" />\r\n"
            "<meta property=\"og:title\" content=\"Photo\" />\r\n"
            "<meta property=\"og:type\" content=\"photostovis:photo\" />\r\n"
            "<meta property=\"og:url\" content=\"http://photostovis.net/%s%s\" />\r\n"
            "<meta property=\"og:image\" content=\"http://photostovis.net/%s%s\" />\r\n"
            "</head></html>\r\n";
    struct ps_md_config const * const mdConfig=getMdConfigRd();

    //allocate the buffer
    int l=strlen(KFbOgPhoto)+strlen(mdConfig->phonetUsername)+strlen(reqUrl)+strlen(mdConfig->phonetUsername)+strlen(reqUrl)+1;
    char *b=ph_malloc(l);

    //put the basic og metadata
    *bufLen=snprintf(b,l,KFbOgPhoto,mdConfig->phonetUsername,reqUrl,mdConfig->phonetUsername,reqUrl);
    //TODO: change the og:image url to thumbnail from image (???)
    releaseMdConfig(&mdConfig);


    LOG0("Answering: %s",b);
    return b;
}

static int movePicsInFolder(int level){
    //we basically have to take all entries from ptrMoving->uploadsDir folder and move them to ptrMoving->picsDir
    //for each entry we need to check if it exists.
    //-if it does and it is a folder, them go inside recursively
    //-if it does and it is a file: is it the same file?
    //  -if yes, delete the upload (ignore)
    //  -if no, rename the file
    int i,n,r,l;
    struct dirent **namelist=NULL; //we must initialize it, in case of error (scandir returns -1) we "free" it.
    int savedPicsDirLen,savedUploadsDirLen;
    struct stat st;
    int picsDir_type=0; //to silence a warning

    //make sure picsDir and uploadsDir ends with /
    ph_assert(ptrMoving->picsDirLen>0,NULL);
    ph_assert(ptrMoving->picsDirLen==strlen(ptrMoving->picsDir),NULL);
    if(ptrMoving->picsDir[ptrMoving->picsDirLen-1]!='/'){
        ptrMoving->picsDir[ptrMoving->picsDirLen++]='/';
        ptrMoving->picsDir[ptrMoving->picsDirLen]='\0';
    }
    savedPicsDirLen=ptrMoving->picsDirLen;
    ph_assert(ptrMoving->uploadsDirLen>0,NULL);
    ph_assert(ptrMoving->uploadsDirLen==strlen(ptrMoving->uploadsDir),NULL);
    if(ptrMoving->uploadsDir[ptrMoving->uploadsDirLen-1]!='/'){
        ptrMoving->uploadsDir[ptrMoving->uploadsDirLen++]='/';
        ptrMoving->uploadsDir[ptrMoving->uploadsDirLen]='\0';
    }
    savedUploadsDirLen=ptrMoving->uploadsDirLen;

    LOG0("movePicsFolder++ in: %s -> %s",ptrMoving->uploadsDir,ptrMoving->picsDir);
    n=scandir(ptrMoving->uploadsDir, &namelist, NULL, NULL);
    for(i=0;i<n;i++){
        struct dirent * __restrict d=namelist[i];
        if(!strcmp(d->d_name,".") || !strcmp(d->d_name,"..") || !strcmp(d->d_name,".DS_Store")){
            free(d);
            continue;
        }

        //add current name to both paths
        l=strlen(d->d_name);//d->d_namlen is not available on ARM
        memcpy(ptrMoving->picsDir+savedPicsDirLen,d->d_name,l+1);
        ptrMoving->picsDirLen=strlen(ptrMoving->picsDir);
        memcpy(ptrMoving->uploadsDir+savedUploadsDirLen,d->d_name,l+1);
        ptrMoving->uploadsDirLen=strlen(ptrMoving->uploadsDir);
        LOG0("Processing %s (%s and %s)",d->d_name,ptrMoving->uploadsDir,ptrMoving->picsDir);

        //do we have a counterpart in pictures dir?
        if(stat(ptrMoving->picsDir,&st)){
            //stat failed, should be because the entry was not found
            ph_assert(errno==ENOENT,"stat failed with wrong reason: %s",strerror(errno));
            //move our entry
            rename(ptrMoving->uploadsDir,ptrMoving->picsDir);
            LOG0("Moved %s to %s",ptrMoving->uploadsDir,ptrMoving->picsDir);
            free(d);
            continue;
        }
        //if we are here, stat succeeded, so we have a counterpart

        if(S_ISREG(st.st_mode))picsDir_type=DT_REG;
        else if(S_ISDIR(st.st_mode))picsDir_type=DT_DIR;
        else ph_assert(0,"Cannot find entry type. ");


        //check the type of the uploads entry
        if(d->d_type==DT_UNKNOWN){
            //we must find the type
            if(!stat(ptrMoving->uploadsDir,&st)){
                //stat succeeded
                if(S_ISREG(st.st_mode))d->d_type=DT_REG;
                else if(S_ISDIR(st.st_mode))d->d_type=DT_DIR;
                else ph_assert(0,"Cannot find entry type. ");
            } else {
                //stat failing is not a good sign
                LOG0("ERROR: stat failed for: %s",ptrMoving->uploadsDir);
                free(d);
                continue;
            }
        }

        if(d->d_type==DT_DIR && picsDir_type==DT_DIR){
            movePicsInFolder(level+1);
            free(d);
            continue;
        }

        //if we are here, the 2 entries exist, but they are not BOTH folders
        if(d->d_type==DT_REG && picsDir_type==DT_REG){
            //next question is: are these files the same?
            r=filesAreTheSame(ptrMoving->picsDir,ptrMoving->uploadsDir);
            ph_assert(r==0 || r==1,NULL);
            if(r==1){
                LOG0("Deleting %s because we have EXACTLY the same file in target folder",ptrMoving->uploadsDir);
                unlink(ptrMoving->uploadsDir);
            } else {
                //they are not the same.
                //Policy: we overwrite the old file
                //TODO: prompt the user to choose something

                LOG0("%s and %s are NOT the same, even if they have the same name.",ptrMoving->picsDir,ptrMoving->uploadsDir);
                unlink(ptrMoving->picsDir);
                rename(ptrMoving->uploadsDir,ptrMoving->picsDir);

                /*
                composeNewName(ptrMoving->uploadsDir);
                if(filesAreTheSame(ptrMoving->picsDir,ptrMoving->uploadsDir)){
                    LOG0("Deleting %s because we have the same file in target folder",ptrMoving->uploadsDir);
                    unlink(ptrMoving->uploadsDir);
                } else {
                    //move our entry
                    rename(ptrMoving->uploadsDir,ptrMoving->picsDir);
                }*/
            }
            free(d);
            continue;
        }

        //if here, one is file, the other one is folder
        free(d);
        ph_assert(0,"NOT IMPLEMENTED");
    }//for
    free(namelist);
    ptrMoving->picsDirLen=savedPicsDirLen;
    ptrMoving->picsDir[ptrMoving->picsDirLen]='\0';
    ptrMoving->uploadsDirLen=savedUploadsDirLen;
    ptrMoving->uploadsDir[ptrMoving->uploadsDirLen]='\0';

    LOG0("movePicsFolder-- in: %s -> %s",ptrMoving->uploadsDir,ptrMoving->picsDir);

    return 0;
}

//TODO: move this somewhere else
void urldecode2(char *dst, const char *src){
    char a, b;
    while (*src) {
        if ((*src == '%') &&
                ((a = src[1]) && (b = src[2])) &&
                (isxdigit(a) && isxdigit(b))) {
            if (a >= 'a')
                a -= 'a'-'A';
            if (a >= 'A')
                a -= ('A' - 10);
            else
                a -= '0';
            if (b >= 'a')
                b -= 'a'-'A';
            if (b >= 'A')
                b -= ('A' - 10);
            else
                b -= '0';
            *dst++ = 16*a+b;
            src+=3;
        } else {
            *dst++ = *src++;
        }
    }
    *dst++ = '\0';
}

int createNewSubalbum(char *strAlbum, char *strSubAlbum, uint32_t *subAlbumNameCRC){
    LOG0("createNewSubalbum: in album: %s, name: %s",strAlbum,strSubAlbum);
    int r,l;
    //get the ID of the album
    struct md_entry_id id={0,0};
    if(strAlbum){
        char *ptrStart=strAlbum+1,*ptrEnd;
        while(1){
            ptrEnd=strchr(ptrStart,'/');
            if(ptrEnd)ptrEnd[0]='\0';//so we can extract the number
            l=strlen(ptrStart);
            if(l==0)break;
            LOG0return(l!=8,-1,"WARNING: wrong ID length: %d (ID: %s)",l,ptrStart);
            sscanf(ptrStart,"%08X",&(id.nameCRC[id.level]));

            id.level++;
            if(ptrEnd){
                ptrEnd[0]='/';//we put it back
                ptrStart=ptrEnd+1;
            } else break;
            LOG0return(id.level>=7,-1,"WARNING: Illegal request (too many levels: %d).",id.level);
        }
    }

    //prepare picsDir,
    char picsDir[PATH_MAX];
    r=getEntryAndPath(&id,KPicturesFolder,picsDir,NULL,NULL,NULL);
    releaseMdPics(NULL);
    LOG0return(r,-100,"createNewSubalbum: Something went wrong, the path was not found");
    ph_assert(strSubAlbum,NULL);
    int picsDirLen=strlen(picsDir);

    urldecode2(strSubAlbum,strSubAlbum);
    if(picsDir[picsDirLen-1]!='/')
        picsDir[picsDirLen++]='/';
    l=strlen(strSubAlbum);
    memcpy(picsDir+picsDirLen,strSubAlbum,l+1);
    picsDirLen+=l;

    //add nameCRC for our new sub-album
    *subAlbumNameCRC=id.nameCRC[id.level++]=ph_crc32b(strSubAlbum)&0xFFFFFFFC;

    //start a scan
    int weDoFastScan;
    if(statusFlagsCheck(StatusFlagPictureScanThreadActive)){
        statusFlagsAdd(StatusFlagDoARescan);
        weDoFastScan=0;
        LOG0("createNewSubalbum: scanning already ongoing. Queued");
    } else {
        statusFlagsAdd(StatusFlagPictureScanThreadActive|StatusFlagIgnoreFirstRescan);
        weDoFastScan=1;
    }
    r=dirService(picsDir,1);
    LOG0return(r,-101,"createNewSubalbum: Something went wrong, could not create the new folder");

    if(!weDoFastScan)
        return 1;

    //if here we do a fast scan
    pthread_t t;
    LOG0("createNewSubalbum: Creating scanPicturesThread...");
    r=pthread_create(&t,NULL,&scanPicturesThread,&id);
    LOG0close_return(r,-102,statusFlagsRemove(StatusFlagPictureScanThreadActive),"ERROR (%d) creating THE thread for pictures scan.",r);
    pthread_join(t,NULL);
    //statusFlagsRemove(StatusFlagPictureScanThreadActive); -> not needed here, the scan thread removes it

    LOG0("createNewSubalbum: scanPicturesThread joined!");
    return 0;
}

int moveUploadedContent(char *strAlbum){
    int r,l;
    //get the ID of the album
    struct md_entry_id id={0,0};
    if(strAlbum){
        char *ptrStart=strAlbum+1,*ptrEnd;
        while(1){
            ptrEnd=strchr(ptrStart,'/');
            if(ptrEnd)ptrEnd[0]='\0';//so we can extract the number
            l=strlen(ptrStart);
            if(l==0)break;
            LOG0return(l!=8,-1,"WARNING: wrong ID length: %d (ID: %s)",l,ptrStart);
            sscanf(ptrStart,"%08X",&(id.nameCRC[id.level]));

            id.level++;
            if(ptrEnd){
                ptrEnd[0]='/';//we put it back
                ptrStart=ptrEnd+1;
            } else break;
            LOG0return(id.level>=7,-1,"WARNING: Illegal request (too many levels: %d).",id.level);
        }
    }

    ph_assert(!ptrMoving,NULL);
    ptrMoving=ph_malloc(sizeof(struct mdh_moving));
    ptrMoving->picsDirLen=0;
    ptrMoving->uploadsDirLen=0;
    LOG0("Move uploaded content++");

    //prepare picsDir and uploadsDir
    getEntryAndPath(&id,KPicturesFolder,ptrMoving->picsDir,NULL,NULL,NULL);
    releaseMdPics(NULL);
    ptrMoving->picsDirLen=strlen(ptrMoving->picsDir);

    struct ps_md_config const * const mdConfig=getMdConfigRd();
    l=strlen(mdConfig->rootFolder);
    memcpy(ptrMoving->uploadsDir,mdConfig->rootFolder,l);
    releaseMdConfig(&mdConfig);
    r=strlen(KUploadsFolder);
    memcpy(ptrMoving->uploadsDir+l,KUploadsFolder,r+1);
    ptrMoving->uploadsDirLen=l+r;


    LOG0("Moving uploaded pictures from <<%s>> to <<%s>>",ptrMoving->uploadsDir,ptrMoving->picsDir);
    r=movePicsInFolder(0);
    //TODO: check r

    //delete everything in this uploads folder
    LOG0("Removing everything in %s",ptrMoving->uploadsDir);
    rmrfAllIn(ptrMoving->uploadsDir);

    free(ptrMoving);
    ptrMoving=NULL;
    LOG0("Move uploaded content--");
    return r;
}

int cancelUploadedContent(){
    int r,l;

    ph_assert(!ptrMoving,NULL);
    ptrMoving=ph_malloc(sizeof(struct mdh_moving));
    ptrMoving->picsDirLen=0;
    ptrMoving->uploadsDirLen=0;
    LOG0("Cancel uploaded content++");

    //prepare picsDir and uploadsDir
    struct ps_md_config const * const mdConfig=getMdConfigRd();
    l=strlen(mdConfig->rootFolder);

    memcpy(ptrMoving->picsDir,mdConfig->rootFolder,l);
    memcpy(ptrMoving->uploadsDir,mdConfig->rootFolder,l);
    releaseMdConfig(&mdConfig);
    r=strlen(KPicturesFolder);
    memcpy(ptrMoving->picsDir+l,KPicturesFolder,r+1);
    ptrMoving->picsDirLen=l+r;
    r=strlen(KUploadsFolder);
    memcpy(ptrMoving->uploadsDir+l,KUploadsFolder,r+1);
    ptrMoving->uploadsDirLen=l+r;

    //delete everything in this uploads folder
    LOG0("Removing everything in %s",ptrMoving->uploadsDir);
    rmrfAllIn(ptrMoving->uploadsDir);

    free(ptrMoving);
    ptrMoving=NULL;
    LOG0("Move uploaded content--");
    return 0;
}


