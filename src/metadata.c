#define _GNU_SOURCE
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <sys/types.h>
#include "photostovis.h"
#include "log.h"

static struct ps_md_config mdConfig;
static struct ps_md_pics mdPics;
static struct ps_md_net mdNet;
static uint32_t statusFlags=0;
static pthread_rwlock_t mdConfigLock;
static pthread_rwlock_t mdPicsLock;
static pthread_rwlock_t mdNetLock;
static pthread_rwlock_t mdStatusLock;

static const char *KSalt="HarryPotter17"; //used as a solt for authWithKey and phonet servername

extern int bcrypt_newhash(const char *pass, int log_rounds, char *hash, size_t hashlen);
//#define RWLOCK_DEBUG

#ifdef RWLOCK_DEBUG
static pthread_mutex_t mdMutexR;
int nrConfigR=0,nrPicsR=0,nrNetR=0,nrNetW=0;
#endif

void initMd(struct ps_md_config *config){
    //initialize the rwlock variables
    pthread_rwlock_init(&mdConfigLock,NULL);
    pthread_rwlock_init(&mdPicsLock,NULL);
    pthread_rwlock_init(&mdNetLock,NULL);
    pthread_rwlock_init(&mdStatusLock,NULL);

#ifdef RWLOCK_DEBUG
    pthread_mutex_init(&mdMutexR,NULL);
#endif

    //initialize mdConfig
    if(config){
        memcpy(&mdConfig,config,sizeof(mdConfig));
        memset(config,0,sizeof(struct ps_md_config));
    } else
        memset(&mdConfig,0,sizeof(mdConfig));
    if(!mdConfig.httpPort)
        mdConfig.httpPort=KSrvDefaultPort;
    if(!mdConfig.httpsPort && !(mdConfig.cfgFlags&CfgFlagInetNotAllowed))
        mdConfig.httpsPort=KSrvDefaultHttpsPort;
    mdConfig.httpSocket=-1;
    mdConfig.httpsSocket=-1;
    mdConfig.inotifyFd=0;
    if(!mdConfig.rootFolder)
        mdConfig.rootFolder=(char*)KOptionsRootFolder;
    if(!mdConfig.salt)
        mdConfig.salt=(char*)KSalt;

    //initialize mdPics
    memset(&mdPics,0,sizeof(mdPics));
    mdPics.totalEntries=0;
    mdPics.scannedEntries=-1;
    //initialize mdNet
    memset(&mdNet,0,sizeof(mdNet));
#ifndef CONFIG_NO_PHONET
    mdNet.phonetCfgStatus=PhonetCfgNeverTriedToRegister;
    mdNet.gatewayCfgStatus=GatewayCfgNeverTriedToConfigure;
#else
    mdNet.phonetCfgStatus=0xFF;
    mdNet.gatewayCfgStatus=0xFF;
#endif

    //initialize sessions
    int i;
    for(i=0;i<NR_PARALLEL_SESSIONS;i++){
        pthread_mutex_init(&mdNet.sessions[i].sessionMutex,NULL);
        mdNet.sessions[i].websocketPipe=-1;
    }
    LOG0("Sizeof mdNet: %zu",sizeof(mdNet));
}

void statusFlagsAdd(uint32_t f){
    int r=pthread_rwlock_wrlock(&mdStatusLock);
    ph_assert(r==0,"statusFlagsAdd() lock error: %d (%s)",r,strerror(r));
    statusFlags|=f;
    r=pthread_rwlock_unlock(&mdStatusLock);
    ph_assert(r==0,"statusFlagsAdd() unlock error: %d (%s)",r,strerror(r));
}

void statusFlagsRemove(uint32_t f){
    int r=pthread_rwlock_wrlock(&mdStatusLock);
    ph_assert(r==0,"statusFlagsRemove() lock error: %d (%s)",r,strerror(r));
    statusFlags&=~f;
    r=pthread_rwlock_unlock(&mdStatusLock);
    ph_assert(r==0,"statusFlagsRemove() unlock error: %d (%s)",r,strerror(r));
}

int statusFlagsCheck(uint32_t f){
    int r=pthread_rwlock_rdlock(&mdStatusLock);
    ph_assert(r==0,"statusFlagsCheck() lock error: %d (%s)",r,strerror(r));
    uint32_t x=statusFlags&f;
    r=pthread_rwlock_unlock(&mdStatusLock);
    ph_assert(r==0,"statusFlagsCheck() unlock error: %d (%s)",r,strerror(r));
    return x?1:0;
}

///////////////////////////////////////////
struct ps_md_config const * const getMdConfigRd(){
#ifdef RWLOCK_DEBUG
    pthread_mutex_lock(&mdMutexR);
    LOG0("getMdConfigRd++ (r: %d)",nrConfigR++);
    pthread_mutex_unlock(&mdMutexR);
#endif
    int r=pthread_rwlock_rdlock(&mdConfigLock);
    ph_assert(r==0,"getMdConfigRd() lock error: %d (%s)",r,strerror(r));
#ifdef RWLOCK_DEBUG
    LOG0("getMdConfigRd--");
#endif
    return &mdConfig;
}
struct ps_md_config * const getMdConfigWritable(){
#ifdef RWLOCK_DEBUG
    pthread_mutex_lock(&mdMutexR);
    LOG0("getMdConfigWritable++ (r: %d)",nrConfigR);
    pthread_mutex_unlock(&mdMutexR);
#endif
    int r=pthread_rwlock_wrlock(&mdConfigLock);
    ph_assert(r==0,"getMdConfigWritable() lock error: %d (%s)",r,strerror(r));
#ifdef RWLOCK_DEBUG
    LOG0("getMdConfigWritable--");
#endif
    return &mdConfig;
}
void releaseMdConfig(struct ps_md_config const * const *ptr){
#ifdef RWLOCK_DEBUG
    pthread_mutex_lock(&mdMutexR);
    ph_assert(nrConfigR>0,NULL);
    LOG0("releaseMdConfig++ (r: %d)",nrConfigR--);
    pthread_mutex_unlock(&mdMutexR);
#endif
    if(ptr)*(struct ps_md_config **)ptr=NULL;
    int r=pthread_rwlock_unlock(&mdConfigLock);
    ph_assert(r==0,"releaseMdConfig() unlock error: %d (%s)",r,strerror(r));
#ifdef RWLOCK_DEBUG
    LOG0("releaseMdConfig--");
#endif
}
void releaseMdConfigW(struct ps_md_config **ptr){
#ifdef RWLOCK_DEBUG
    pthread_mutex_lock(&mdMutexR);
    LOG0("releaseMdConfigW++ (r: %d)",nrConfigR);
    pthread_mutex_unlock(&mdMutexR);
#endif
    if(ptr)*ptr=NULL;
    int r=pthread_rwlock_unlock(&mdConfigLock);
    ph_assert(r==0,"releaseMdConfigW() unlock error: %d (%s)",r,strerror(r));
#ifdef RWLOCK_DEBUG
    LOG0("releaseMdConfigW--");
#endif
}


///////////////////////////////////////////
struct ps_md_pics const * const getMdPicsRd(){
#ifdef RWLOCK_DEBUG
    pthread_mutex_lock(&mdMutexR);
    LOG0("getMdPicsRd++ (r: %d)",nrPicsR++);
    pthread_mutex_unlock(&mdMutexR);
#endif
    int r=pthread_rwlock_rdlock(&mdPicsLock);
    ph_assert(r==0,"getMdPicsRd() lock error: %d (%s)",r,strerror(r));
#ifdef RWLOCK_DEBUG
    LOG0("getMdPicsRd--");
#endif
    return &mdPics;
}
struct ps_md_pics * const getMdPicsWritable(){
#ifdef RWLOCK_DEBUG
    pthread_mutex_lock(&mdMutexR);
    LOG0("getMdPicsWritable++ (r: %d)",nrPicsR);
    pthread_mutex_unlock(&mdMutexR);
#endif
    int r=pthread_rwlock_wrlock(&mdPicsLock);
    ph_assert(r==0,"getMdPicsWritable() lock error: %d (%s)",r,strerror(r));
#ifdef RWLOCK_DEBUG
    LOG0("getMdPicsWritable--");
#endif
    return &mdPics;
}
void releaseMdPics(struct ps_md_pics const * const *ptr){
#ifdef RWLOCK_DEBUG
    pthread_mutex_lock(&mdMutexR);
    ph_assert(nrPicsR>0,NULL);
    LOG0("releaseMdPics++ (r: %d)",nrPicsR--);
    pthread_mutex_unlock(&mdMutexR);
#endif
    if(ptr)*(struct ps_md_pics **)ptr=NULL;
    int r=pthread_rwlock_unlock(&mdPicsLock);
    ph_assert(r==0,"releaseMdPics() unlock error: %d (%s)",r,strerror(r));
#ifdef RWLOCK_DEBUG
    LOG0("releaseMdPics--");
#endif
}
void releaseMdPicsW(struct ps_md_pics **ptr){
#ifdef RWLOCK_DEBUG
    pthread_mutex_lock(&mdMutexR);
    LOG0("releaseMdPicsW++ (r: %d)",nrPicsR);
    pthread_mutex_unlock(&mdMutexR);
#endif
    if(ptr)*ptr=NULL;
    int r=pthread_rwlock_unlock(&mdPicsLock);
    ph_assert(r==0,"releaseMdPicsW() unlock error: %d (%s)",r,strerror(r));
#ifdef RWLOCK_DEBUG
    LOG0("releaseMdPicsW--");
#endif
}


///////////////////////////////////////////
struct ps_md_net const * const getMdNetRd(){
#ifdef RWLOCK_DEBUG
    pthread_mutex_lock(&mdMutexR);
    LOG0("getMdNetRd++ (r: %d)",nrNetR++);
    pthread_mutex_unlock(&mdMutexR);
#endif
    int r=pthread_rwlock_rdlock(&mdNetLock);
    ph_assert(r==0,"getMdNetRd() lock error: %d (%s)",r,strerror(r));
#ifdef RWLOCK_DEBUG
    LOG0("getMdNetRd--");
#endif
    return &mdNet;
}
struct ps_md_net * const getMdNetWritable(){
#ifdef RWLOCK_DEBUG
    pthread_mutex_lock(&mdMutexR);
    LOG0("getMdNetWritable++ (r: %d)",nrNetR);
    pthread_mutex_unlock(&mdMutexR);
#endif
    int r=pthread_rwlock_wrlock(&mdNetLock);
    ph_assert(r==0,"getMdNetWritable() lock error: %d (%s)",r,strerror(r));
#ifdef RWLOCK_DEBUG
    nrNetW++;
    ph_assert(nrNetR==0,"getMdNetWritable(): nr of readers is not 0 (WTF?)");
    ph_assert(nrNetW==1,"getMdNetWritable(): nr of writers is not 1 (WTF?)");
    LOG0("getMdNetWritable--");
#endif
    return &mdNet;
}
void releaseMdNet(struct ps_md_net const * const *ptr){
#ifdef RWLOCK_DEBUG
    pthread_mutex_lock(&mdMutexR);
    ph_assert(nrNetR>0,NULL);
    LOG0("releaseMdNet++ (r: %d)",nrNetR--);
    pthread_mutex_unlock(&mdMutexR);
#endif
    if(ptr)*(struct ps_md_net **)ptr=NULL;
    int r=pthread_rwlock_unlock(&mdNetLock);
    ph_assert(r==0,"releaseMdNet() unlock error: %d (%s)",r,strerror(r));
#ifdef RWLOCK_DEBUG
    LOG0("releaseMdNet--");
#endif
}
void releaseMdNetW(struct ps_md_net **ptr){
#ifdef RWLOCK_DEBUG
    pthread_mutex_lock(&mdMutexR);
    LOG0("releaseMdNetW++ (r: %d)",nrNetR);
    ph_assert(nrNetR==0,"releaseMdNetW(): nr of readers is not 0 (WTF?)");
    ph_assert(nrNetW==1,"releaseMdNetW(): nr of writers is not 1 (WTF?)");
    pthread_mutex_unlock(&mdMutexR);
#endif
    if(ptr)*ptr=NULL;
    int r=pthread_rwlock_unlock(&mdNetLock);
    ph_assert(r==0,"releaseMdNetW() unlock error: %d (%s)",r,strerror(r));
#ifdef RWLOCK_DEBUG
    LOG0("releaseMdNetW--");
#endif
}

#ifdef RWLOCK_DEBUG
#define MDNET_WR_LOCKED ph_assert(nrNetW==1,"MDNET_WR_LOCKED: nr of writers is not 1");
#else
#define MDNET_WR_LOCKED
#endif



///////////////////////////////////////////
void destroyMd(){
    int i,j;
    //mdConfig part
    getMdConfigWritable();
    //srvDocRoot should have been freed already in main()
    ph_assert(!mdConfig.srvDocRoot,NULL);
    if(mdConfig.rootFolder!=KOptionsRootFolder)
        free(mdConfig.rootFolder);
    if(mdConfig.salt!=KSalt)
        free(mdConfig.salt);
    free(mdConfig.phonetUsername);
    free(mdConfig.phonetPassword);
    free(mdConfig.authUsers);
    for(i=0;i<mdConfig.nrDevices;i++){
        struct ps_device * __restrict d=&mdConfig.devices[i];
        for(j=0;j<d->nrPartitions;j++)
            free(d->partitions[j].mountPoint);
        free(d->partitions);
        free(d->device);
        free(d->id);
    }
    memset(&mdConfig,0,sizeof(struct ps_md_config));
    releaseMdConfigW(NULL);
    pthread_rwlock_destroy(&mdConfigLock);

    //mdPics part
    getMdPicsWritable();
    freeAlbums(&mdPics.rootAlbum->album,&mdPics.rootAlbum->entriesIndex);
    free(*(struct mdx_album_entry**)(mdPics.rootAlbum+1));
    free(mdPics.rootAlbum);
    free(mdPics.cameras);
    free(mdPics.fullMakeModelStrings);
    free(mdPics.shortenedMakeModelStrings);
    memset(&mdPics,0,sizeof(struct ps_md_pics));
    releaseMdPicsW(NULL);
    pthread_rwlock_destroy(&mdPicsLock);

    //mdNet part
    getMdNetWritable();
    for(i=0;i<NR_PARALLEL_SESSIONS;i++){
        //free(mdNet.sessions[i].lastRequestedAlbumKey);
        pthread_mutex_destroy(&mdNet.sessions[i].sessionMutex);
    }
    free(mdNet.phonetCfgCurlErrorString);
    memset(&mdNet,0,sizeof(struct ps_md_net));
    releaseMdNetW(NULL);
    pthread_rwlock_destroy(&mdNetLock);
#ifdef RWLOCK_DEBUG
    pthread_mutex_destroy(&mdMutexR);
#endif
}

///////////// connection related

void newAlbumRequested(struct ps_session *session, struct md_entry_id const * const id, const int nrThumbnails,
                       struct md_level_etagf const * const lastRequestedAlbumETag){
    //the parent is responsible with locking the session
    ASSERT_SESSION_LOCKED(session);

    session->lastRequestedAlbumID=*id;
    session->lastRequestedAlbumNrThumbnailsPerRow=nrThumbnails;
    /*
    if(key){
        if(session->lastRequestedAlbumKey)
            free(session->lastRequestedAlbumKey);
        session->lastRequestedAlbumKey=ph_strdup(key);
    }*/
    if(lastRequestedAlbumETag)
        session->lastRequestedAlbumETag=*lastRequestedAlbumETag;
    //reset thumbnail transfer data
    session->thumbsTransferStart=0xFFFFFFFF;
    session->thumbsBytesTransfered=session->thumbsBytesTransfering=0;
    session->thumbsNrTransfered=session->thumbsNrTransfering=0;

    LOG0("->->->-> newAlbumRequested: reset everything");
}

uint32_t addThumbTransferStart(struct ps_session *session, uint32_t dataSize, uint32_t *transferStart){
    //the parent is responsible with locking the session
    ASSERT_SESSION_LOCKED(session);

    if(session->thumbsTransferStart==0xFFFFFFFF){
        ph_assert(session->thumbsNrTransfered==0,NULL);
        ph_assert(session->thumbsNrTransfering==0,NULL);
        ph_assert(session->thumbsBytesTransfered==0,NULL);
        ph_assert(session->thumbsBytesTransfering==0,NULL);
        session->thumbsTransferStart=getRelativeTimeMs();
    }
    //
    uint32_t bitrate=0xFFFFFFFF;
    //can we compute the bitrate with the data we have?
    if(session->thumbsNrTransfered>=10 && !(session->thumbsNrTransfered%10) && session->thumbsBytesTransfered>=MIN_DATA_TRANSFERED_FOR_BITRATE_CALCULATION){
        //yes, we can compute the bitrate
        uint32_t endTime=getRelativeTimeMs();
        if(endTime>session->thumbsTransferStart){
            bitrate=(session->thumbsBytesTransfered<<3)/(endTime-session->thumbsTransferStart);
            if(session->bitrate)
                bitrate=session->bitrate=(session->bitrate+bitrate)>>1;
            else
                session->bitrate=bitrate;
        }
    }

    //update data
    session->thumbsNrTransfering++;
    session->thumbsBytesTransfering+=dataSize;

    //done
    LOG0("->->->-> addThumbTransferStart: %u bytes (total: %u+%u), start time (hex): %x. Transfering/ed nr: %u+%u, Bitrate: %u",
         dataSize,session->thumbsBytesTransfering,session->thumbsBytesTransfered,session->thumbsTransferStart,
         session->thumbsNrTransfering,session->thumbsNrTransfered,bitrate);
    if(transferStart)
        *transferStart=session->thumbsTransferStart;
    return bitrate;
}

void thumbTransferFailed(struct ps_session *session, uint32_t dataSize, uint32_t transferStart){
    //the parent is responsible with locking the session
    ASSERT_SESSION_LOCKED(session);

    if(session->thumbsTransferStart==transferStart){
        //if here it means that the data has not been reset since this image transfer started
        ph_assert(session->thumbsNrTransfering>0,NULL);
        session->thumbsNrTransfering--;
        ph_assert(session->thumbsBytesTransfering>=dataSize,NULL);
        session->thumbsBytesTransfering-=dataSize;
        LOG0("->->->-> thumbTransferFailed: %u+%u",session->thumbsNrTransfering,session->thumbsNrTransfered);
    } else LOG0("->->->-> thumbTransferFailed: data was reset already");
}

void thumbTransferSucceeded(struct ps_session *session, uint32_t dataSize, uint32_t transferStart, uint32_t processingTime){
  //the parent is responsible with locking the session
  ASSERT_SESSION_LOCKED(session);

  if(session->thumbsTransferStart==transferStart){
    //if here it means that the data has not been reset since this image transfer started
    ph_assert(session->thumbsNrTransfering>0,NULL);
#ifdef DEBUG_THUMBNAILS_PROCESSING_TIME
    if(!session->thumbnailsProcessingTime){
      LOG0("Allocating thumbnailsProcessingTime");
      session->thumbnailsProcessingTime_allocatedSize=NR_THUMBNAILS_PROCESSING_TIME_ALLOC_AT_ONCE;
      session->thumbnailsProcessingTime=(int16_t*)ph_malloc(session->thumbnailsProcessingTime_allocatedSize*sizeof(int16_t));
      
    } else if(session->thumbsNrTransfered+1>session->thumbnailsProcessingTime_allocatedSize){
      LOG0("(Re)allocating thumbnailsProcessingTime (thumbsNrTransfered=%u)",(unsigned)session->thumbsNrTransfered);
      session->thumbnailsProcessingTime_allocatedSize+=NR_THUMBNAILS_PROCESSING_TIME_ALLOC_AT_ONCE;
      session->thumbnailsProcessingTime=(int16_t*)ph_realloc(session->thumbnailsProcessingTime,
              session->thumbnailsProcessingTime_allocatedSize*sizeof(int16_t));
    }
#endif
    if(processingTime>0x7FFF)
        processingTime=0x7FFF; //because we store it on int16_t. The sign is to differentiate "NotModified" thumbnails: they have negative values
    if(!dataSize){
        //this is a "NotModified" thumbnail. Store the processing time as -processingTime-1. The -1 is to differentiate it from zero values
        processingTime=-processingTime-1;
    }
#ifdef DEBUG_THUMBNAILS_PROCESSING_TIME
    session->thumbnailsProcessingTime[session->thumbsNrTransfered]=processingTime;
#endif
    session->thumbsNrTransfering--;
    session->thumbsNrTransfered++;
    ph_assert(session->thumbsBytesTransfering>=dataSize,"%u>=%u",session->thumbsBytesTransfering,dataSize);
    session->thumbsBytesTransfering-=dataSize;
    session->thumbsBytesTransfered+=dataSize;
    LOG0("->->->-> thumbTransferSucceeded: %u+%u",session->thumbsNrTransfering,session->thumbsNrTransfered);
  } else LOG0("->->->-> thumbTransferSucceeded: data was reset already");
}

uint32_t thumbnailsTransferEnded(struct ps_session *session){
    //the parent is responsible with locking the session
    ASSERT_SESSION_LOCKED(session);

    uint32_t bitrate=0xFFFFFFFF,endTime=getRelativeTimeMs();
    //compute the bitrate out of transfered and transfering items
    //(due to multithreading it is possible that we have not cleared yet some of the transfering items
    if(endTime>session->thumbsTransferStart){
        bitrate=((session->thumbsBytesTransfered+session->thumbsBytesTransfering)<<3)/(endTime-session->thumbsTransferStart);
        if(bitrate){
            if(session->bitrate)
                session->bitrate=(session->bitrate+bitrate)>>1;
            else
                session->bitrate=bitrate;
        } else bitrate=0xFFFFFFFF;
        LOG0("->->->-> thumbnailsTransferEnded: %u+%u bytes transfered in %u ms. Bitrate: %u (for thumbnails: %u)",
             session->thumbsBytesTransfering,session->thumbsBytesTransfered,endTime-session->thumbsTransferStart,session->bitrate,bitrate);
    }// else the bitrate stays the same;

#ifdef DEBUG_THUMBNAILS_PROCESSING_TIME
    //print processing time statistics
    LOG0("Thumbnails processing time (first 5):");
    float averageProcessingTime,averageProcessingTimeTransfered,averageProcessingTimeNotModified;
    int16_t maxProcessingTime=0,currentProcessingTime;
    uint32_t totalProcessingTime=0;
    uint32_t totalProcessingTimeTransfered=0;
    uint32_t totalProcessingTimeNotModified=0;
    uint16_t nrThumbnailTransfered=0,nrThumbnailsNotModified=0;

    int i,nrPrints=session->thumbsNrTransfered;
    if(nrPrints>5)nrPrints=5;
    for(i=0;i<session->thumbsNrTransfered;i++){
        currentProcessingTime=session->thumbnailsProcessingTime[i];
        if(currentProcessingTime<0){
            //"NotModified" thumbnail
            currentProcessingTime=-currentProcessingTime-1;
            LOG0c(i<nrPrints,"i=%03d t=%2d (Not Modified)",i,(int)currentProcessingTime);
            totalProcessingTimeNotModified+=currentProcessingTime;
            nrThumbnailsNotModified++;
        } else {
            //transfered thumbnail
            LOG0c(i<nrPrints,"i=%03d t=%2d",i,(int)currentProcessingTime);
            totalProcessingTimeTransfered+=currentProcessingTime;
            nrThumbnailTransfered++;
        }

        totalProcessingTime+=currentProcessingTime;
        if(currentProcessingTime>maxProcessingTime)
            maxProcessingTime=currentProcessingTime;
    }
    if(session->thumbsNrTransfered)
        averageProcessingTime=(float)totalProcessingTime/(float)session->thumbsNrTransfered;
    else averageProcessingTime=0;
    LOG0("AVERAGE THUMBNAIL PROCESSING TIME: %5.2fms. MAX PROCESSING TIME: %ums. TOTAL PROCESSING TIME: %ums. Transfered: %uKB",
         averageProcessingTime,(unsigned)maxProcessingTime,(unsigned)totalProcessingTime,(unsigned)session->thumbsBytesTransfered>>10);

    if(nrThumbnailTransfered)
        averageProcessingTimeTransfered=(float)totalProcessingTimeTransfered/(float)nrThumbnailTransfered;
    else averageProcessingTimeTransfered=0;
    LOG0("Average thumbnail processing time for transfered  thumbnails (nr=%d): %5.2fms.",(int)nrThumbnailTransfered,averageProcessingTimeTransfered);

    if(nrThumbnailsNotModified)
        averageProcessingTimeNotModified=(float)totalProcessingTimeNotModified/(float)nrThumbnailsNotModified;
    else averageProcessingTimeNotModified=0;
    LOG0("Average thumbnail processing time for NotModified thumbnails (nr=%d): %5.2fms.",(int)nrThumbnailsNotModified,averageProcessingTimeNotModified);
#endif

    //clear everything
    session->thumbsTransferStart=0xFFFFFFFF;
    session->thumbsBytesTransfered=session->thumbsBytesTransfering=0;
    session->thumbsNrTransfered=session->thumbsNrTransfering=0;
#ifdef DEBUG_THUMBNAILS_PROCESSING_TIME
    free(session->thumbnailsProcessingTime);
    session->thumbnailsProcessingTime=NULL;
    session->thumbnailsProcessingTime_allocatedSize=0;
#endif
    //done
    if(bitrate!=0xFFFFFFFF)
        bitrate=session->bitrate;
    return bitrate;
}

void addImageTransferStart(struct ps_session *session, uint64_t dataSize, uint32_t processingTime, uint32_t nameCRC, uint32_t *transferStart){
    //the parent is responsible with locking the session
    ASSERT_SESSION_LOCKED(session);

    if(session->imageTransferStart==0xFFFFFFFF){
        //this is the "right" case, when the previous image has been confirmed
        ph_assert(session->imageBytes==0,NULL);
        ph_assert(session->imageNameCRC==0,NULL);
    } else {
        LOG0("!>!>!>!> addImageTransferStart: previous image (%08X), %"UINT64_FMT" bytes not confirmed!! Overwriting!",session->imageNameCRC,session->imageBytes);
    }

    //(over)write image data
    session->imageTransferStart=getRelativeTimeMs();
    session->imageProcessingTime=processingTime;
    session->imageBytes=dataSize;
    session->imageNameCRC=nameCRC;
    if(transferStart)
        *transferStart=session->imageTransferStart;
    //reset thumbnail transfer data
    session->thumbsTransferStart=0xFFFFFFFF;
    session->thumbsBytesTransfered=session->thumbsBytesTransfering=0;
    session->thumbsNrTransfered=session->thumbsNrTransfering=0;

    //done
    LOG0("->->->-> addImageTransferStart: ID=%08X, %"UINT64_FMT" bytes. Processing time: %ums. Start time: %u",
         nameCRC,dataSize,processingTime,session->imageTransferStart);
}

void imageTransferFailed(struct ps_session *session, uint64_t dataSize, uint32_t processingTime, uint32_t nameCRC, uint32_t transferStart){
    LOG0("imageTransferFailed++");
    //the parent is responsible with locking the session
    ASSERT_SESSION_LOCKED(session);

    if(session->imageTransferStart==transferStart){
        //if here it means that the data has NOT been reset since this image transfer started
        ph_assert(session->imageProcessingTime==processingTime,"session->imageProcessingTime=%u, processingTime=%u",session->imageProcessingTime,processingTime);
        ph_assert(session->imageBytes==dataSize,NULL);
        ph_assert(session->imageNameCRC==nameCRC,NULL);
        //do the reset
        session->imageTransferStart=0xFFFFFFFF;
        session->imageProcessingTime=0;
        session->imageBytes=0;
        session->imageNameCRC=0;
        LOG0("!>!>!>!> imageTransferFailed: ID=%08X, %"UINT64_FMT" bytes.",nameCRC,dataSize);
    } else {
        LOG0("!>!>!>!> imageTransferFailed: data was reset already: ID=%08X, %"UINT64_FMT" bytes. Processing time: %ums. Start time: %u",
             nameCRC,dataSize,processingTime,session->imageTransferStart);
    }
    LOG0("imageTransferFailed--");
}

uint32_t imageTransferConfirmed(struct ps_session *session, int transferTime, uint32_t nameCRC){
    //the parent is responsible with locking the session
    ASSERT_SESSION_LOCKED(session);

    uint32_t bitrate=0xFFFFFFFF;
    int ourTransferTime=getRelativeTimeMs()-session->imageTransferStart;
    int transferTimeDifference;
    //we expect to have here the element identified by the nameCRC
    LOG0return(session->imageNameCRC!=nameCRC || session->imageTransferStart==0xFFFFFFFF,bitrate,
                     "!>!>!>!> imageTransferConfirmed: this is not our image. Image ID to confirm: %08X, stored ID: %08X",nameCRC,session->imageNameCRC);

    //remote transfer time contains processing time, substract it
    transferTime-=session->imageProcessingTime;
    transferTimeDifference=transferTime-ourTransferTime;

    LOG0("->->->-> imageTransferConfirmed: our transferTime: %d, remote (JS) transferTime: %d. Difference: %d. Processing time: %ums",
         ourTransferTime,transferTime,transferTimeDifference,session->imageProcessingTime);
    //we will be using ourTransferTime

    LOG0c_do(ourTransferTime<=0 || transferTime<=0,bitrate=0,
             "!>!>!>!> imageTransferConfirmed: ourTransferTime (%d) too small. Skipping computing the bitrate.",ourTransferTime);
    LOG0c_do((ourTransferTime>>3)<transferTimeDifference || (transferTime>>3)<transferTimeDifference,bitrate=0,
             "!>!>!>!> imageTransferConfirmed: transferTimeDifference (%d) is too big compared with server transfer time (%d) and/or client transfer time (%d). "
             "Skipping computing bitrate.",transferTimeDifference,ourTransferTime,transferTime);

    if(bitrate)
        bitrate=(session->imageBytes<<3)/ourTransferTime;
    if(bitrate){
        if(session->bitrate)
            session->bitrate=(session->bitrate+bitrate)>>1;
        else
            session->bitrate=bitrate;
    } else bitrate=0xFFFFFFFF;
    LOG0("->->->-> imageTransferConfirmed: ID=%08X, %"UINT64_FMT" bytes. Transfer time: %dms (remote: %dms). Bitrate: %u (%u)",
         nameCRC,session->imageBytes,ourTransferTime,transferTime,session->bitrate,bitrate);

    //clear everything
    session->imageTransferStart=0xFFFFFFFF;
    session->imageProcessingTime=0;
    session->imageBytes=0;
    session->imageNameCRC=0;

    //done
    if(bitrate!=0xFFFFFFFF)
        bitrate=session->bitrate;
    return bitrate;
}


uint32_t addUploadedData(struct ps_session *session, const uint32_t startTime, const uint32_t endTime, const uint64_t dataSize, const int finished){
    //the parent is responsible with locking the session
    ASSERT_SESSION_LOCKED(session);

    uint32_t bitrate=0xFFFFFFFF;
    if(session->uploadRecvStart==0xFFFFFFFF || (session->uploadLastEnd!=0xFFFFFFFF && startTime>session->uploadLastEnd && startTime-session->uploadLastEnd>1000)){
        //we are starting a brand new connection series
        session->uploadRecvStart=startTime;
        session->uploadBytesRecv=dataSize;
        //LOG0("addUploadedData: started a new transfer with %u bytes",(unsigned int)dataSize);
    } else {
        //we are adding on top of existing data
        session->uploadBytesRecv+=dataSize;
        //LOG0("addUploadedData: adding (%u bytes) to existing data. Total: %u bytes",(unsigned int)dataSize,(unsigned int)session->uploadBytesRecv);
    }
    if(finished)
        session->uploadLastEnd=endTime;

    //compute the bitrate
    if(endTime>session->uploadRecvStart && endTime-session->uploadRecvStart>1000 && session->uploadBytesRecv>MIN_DATA_TRANSFERED_FOR_BITRATE_CALCULATION){
        bitrate=(session->uploadBytesRecv<<3)/(endTime-session->uploadRecvStart);
        if(session->uploadBitrate)
            session->uploadBitrate=(session->uploadBitrate+bitrate)>>1;
        else
            session->uploadBitrate=bitrate;
        //LOG0("addUploadedData: upload bitrate computed: %u (%u)",session->uploadBitrate,bitrate);
        bitrate=session->uploadBitrate;
    }
    //done
    return bitrate;
}


const char *verifyUserSubmittedConfig(struct ps_md_config_received * const __restrict cfg){
    int j;
    const unsigned int KAccessFromInetMask=0b01010101;
    const unsigned int KAccessFromLANMask =0b10101010;
    const unsigned int KViewPermissionLANMask   =0b10000000;
    const unsigned int KOtherPermissionsLANMask =0b00101010;
    const unsigned int KViewPermissionInetMask  =0b01000000;
    const unsigned int KOtherPermissionsInetMask=0b00010101;

    //verify if permissions make sense (any other permission requires viewing permission)
    if(!(cfg->permissionsAnonymous&KViewPermissionLANMask) && cfg->permissionsAnonymous&KOtherPermissionsLANMask)
        return "Anonymous, LAN: having other permissions but not viewing permissions";
    if(!(cfg->permissionsAnonymous&KViewPermissionInetMask) && cfg->permissionsAnonymous&KOtherPermissionsInetMask)
        return "Anonymous, Inet: having other permissions but not viewing permissions";

    if(!(cfg->permissionsAuthWithKey&KViewPermissionLANMask) && cfg->permissionsAuthWithKey&KOtherPermissionsLANMask)
        return "AuthWithKey, LAN: having other permissions but not viewing permissions";
    if(!(cfg->permissionsAuthWithKey&KViewPermissionInetMask) && cfg->permissionsAuthWithKey&KOtherPermissionsInetMask)
        return "AuthWithKey, Inet: having other permissions but not viewing permissions";

    for(j=0;j<cfg->nrAuthUsers;j++){
        if(!(cfg->authUsers[j].permissions&KViewPermissionLANMask) && cfg->authUsers[j].permissions&KOtherPermissionsLANMask)
            return "Found user without view permission but having other permissions (for LAN)";
        if(!(cfg->authUsers[j].permissions&KViewPermissionInetMask) && cfg->authUsers[j].permissions&KOtherPermissionsInetMask)
            return "Found user without view permission but having other permissions (for Internet)";
    }

    //verify the configuration flags. Add them if they make sense.
    if(cfg->cfgFlags&CfgFlagLanAuthRequired){
        //the anonymous user should have no LAN permissions
        if(cfg->permissionsAnonymous&KAccessFromLANMask)
            return "LAN Authentication required, but Anonymous user still has some permissions.";
    } else {
        if(!(cfg->permissionsAnonymous&KAccessFromLANMask)){
            cfg->cfgFlags|=CfgFlagLanAuthRequired;
            LOG0("CfgFlagLanAuthRequired flag added to the user-submitted configuration.");
        }
    }
    if(cfg->cfgFlags&CfgFlagInetAuthNotRequired){
        //the anonymous user should have at least viewing permissions for Inet
        if(!(cfg->permissionsAnonymous&KViewPermissionInetMask))
            return "Inet Authentication not required, but Anonymous user has no Inet viewing permissions.";
    } else {
        if(cfg->permissionsAnonymous&KViewPermissionInetMask){
            cfg->cfgFlags|=CfgFlagInetAuthNotRequired;
            LOG0("CfgFlagInetAuthRequired flag added to the user-submitted configuration.");
        }
    }
    if(cfg->cfgFlags&CfgFlagInetNotAllowed){
        //this is more complicated. Basically, nobody should have any permissios when coming from Inet
        if(cfg->permissionsAnonymous&KAccessFromInetMask)
            return "Inet access forbidden, but Anonymous user still has some permissions.";
        if(cfg->permissionsAuthWithKey&KAccessFromInetMask)
            cfg->permissionsAuthWithKey&=~KAccessFromInetMask;
        for(j=0;j<cfg->nrAuthUsers;j++)
            if(cfg->authUsers[j].permissions&KAccessFromInetMask)
                cfg->authUsers[j].permissions&=~KAccessFromInetMask;
    } else {
        //should we actually add this flag to our configuration?
        int shouldAdd=1;
        if(cfg->permissionsAnonymous&KAccessFromInetMask || cfg->permissionsAuthWithKey&KAccessFromInetMask)
            shouldAdd=0; //because either anonymous or auht with key users can access photos from outside
        if(shouldAdd)
            for(j=0;j<cfg->nrAuthUsers;j++)
                if(cfg->authUsers[j].permissions&KAccessFromInetMask){
                    shouldAdd=0; //because this user can access photos from outside (Inet)
                    break;
                }
        if(shouldAdd){
            cfg->cfgFlags|=CfgFlagInetNotAllowed;
            LOG0("CfgFlagInetNotAllowed flag added to the user-submitted configuration.");
        }
    }

    //if we are here everything is good!
    return NULL;
}

int saveToConfigurationFile(struct ps_md_config_received const * const __restrict cfg){
    struct ps_md_config const * const mdConfig=getMdConfigRd();
    ph_assert(mdConfig->nrTotalConfigurations<=1,"There are several servers (%u) running on this HW. Cannot use UI Config!",
              (unsigned int)mdConfig->nrTotalConfigurations);

    //open a new configuration file. We will write the configuration here.
    FILE *f;
    const char *newFilename=NULL;
    const char *oldFilename=NULL;
    const char *existingFilename=NULL;
    if(mdConfig->cfgFlags&CfgFlagIsDaemon){
        newFilename="/etc/photostovis.conf.new";
        oldFilename="/etc/photostovis.conf.old";
        existingFilename=KConfigFilenameSystem;
    } else {
        newFilename="./photostovis.conf.new";
        oldFilename="./photostovis.conf.old";
        existingFilename="./photostovis.conf";
    }

    f=fopen(newFilename,"w");
    LOG0return(!f,-1,"Could not open configuration file for writing (%s): %s",newFilename,strerror(errno));

    //write the version
    fprintf(f,"# This configuration file is autogenerated by Photostovis\nversion=%d\n\n",CURRENT_CONFIG_FILE_VERSION);

    //should we write the root_folder, or is it the default?
    if(mdConfig->rootFolder!=KOptionsRootFolder)
        fprintf(f,"root_folder=%s\n\n",mdConfig->rootFolder);

    //do we have different ports, should we write them?
    if(mdConfig->httpPort!=KSrvDefaultPort)
        fprintf(f,"port=%u\n",(unsigned int)mdConfig->httpPort);
    if(mdConfig->httpsPort!=KSrvDefaultHttpsPort)
        fprintf(f,"port_https=%u\n",(unsigned int)mdConfig->httpsPort);

    //do we have ssh port forwarding?
    if(mdConfig->sshPortExt && mdConfig->sshPortInt)
        fprintf(f,"\nregister_ssh_port_int=%u\nregister_ssh_port_ext=%u\n",(unsigned int)mdConfig->sshPortInt,(unsigned int)mdConfig->sshPortExt);

    //users
    fprintf(f,"\n");
    int i,j;
    for(i=0;i<cfg->nrAuthUsers;i++){
        if(!cfg->authUsers[i].password){
            //this user was unchanged. Copy the password from mdConfig
            for(j=0;j<mdConfig->nrAuthUsers;j++)
                if(!strcmp(cfg->authUsers[i].username,mdConfig->authUsers[j].username)){
                    cfg->authUsers[i].password=ph_strdup(mdConfig->authUsers[j].password);
                    break;
                }
        } else {
            //the password that we have is SHA256. We have to bcrypt it!
            ph_assert(strlen(cfg->authUsers[i].password)==64,NULL);
            char *pwBcryptStr=ph_malloc(BCRYPT_HASH_LENGTH+1);
            bcrypt_newhash(cfg->authUsers[i].password,12,pwBcryptStr,BCRYPT_HASH_LENGTH+1);
            free(cfg->authUsers[i].password);
            cfg->authUsers[i].password=pwBcryptStr;
        }
        LOG0close_return(!cfg->authUsers[i].password,-1,fclose(f);unlink(newFilename),"No password for username %s",cfg->authUsers[i].username);

        //do we have permissions? If not, fetch them from the old config, and if not found, have default permissions
        if(!cfg->authUsers[i].permissions){
            for(j=0;j<mdConfig->nrAuthUsers;j++)
                if(!strcmp(cfg->authUsers[i].username,mdConfig->authUsers[j].username)){
                    cfg->authUsers[i].permissions=mdConfig->authUsers[j].permissions;
                    break;
                }
            if(!cfg->authUsers[i].permissions)
                cfg->authUsers[i].permissions=DEFAULT_PERMISSIONS_USER;
        }

        //here we have to printf this line into the file
        fprintf(f,"auth_user=%s:%s:"BYTETOBINARYPATTERN"\n",cfg->authUsers[i].username,cfg->authUsers[i].password,BYTETOBINARY(cfg->authUsers[i].permissions));
    }//for(...nrAuthUsers
    fprintf(f,"\n");

    //anonymous & authWithKey users
    if(cfg->permissionsAnonymous!=DEFAULT_PERMISSIONS_ANONYMOUS)
        fprintf(f,"auth_user_anonymous="BYTETOBINARYPATTERN"\n",BYTETOBINARY(cfg->permissionsAnonymous));
    if(cfg->permissionsAuthWithKey!=DEFAULT_PERMISSIONS_AUTHWITHKEY)
        fprintf(f,"auth_user_authwithkey="BYTETOBINARYPATTERN"\n",BYTETOBINARY(cfg->permissionsAuthWithKey));
    fprintf(f,"\n");

    //LAN and Inet authentications
    if(cfg->cfgFlags&CfgFlagLanAuthRequired)
        fprintf(f,"auth_lan_access=2\n");
    if(cfg->cfgFlags&CfgFlagLanAuthHttpNotAllowed)
        fprintf(f,"auth_lan_auth_over_http=false\n");
    if(cfg->cfgFlags&CfgFlagInetAuthNotRequired)
        fprintf(f,"auth_inet_access=1\n");
    if(cfg->cfgFlags&CfgFlagInetNotAllowed)
        fprintf(f,"auth_inet_access=3\n");
    if((cfg->cfgFlags&CfgFlagInetAuthHttpAllowed) && !(cfg->cfgFlags&CfgFlagInetAuthHttpAllowedAutoadded))
        fprintf(f,"auth_inet_auth_over_http=true\n");
    fprintf(f,"\n");

    //server name
    if(mdConfig->phonetUsername){
        ph_assert(mdConfig->phonetPassword,NULL);
        ph_assert(!cfg->phonetUsername,NULL);
        fprintf(f,"phonet_username=%s\n",mdConfig->phonetUsername);
        fprintf(f,"phonet_password=%s\n",mdConfig->phonetPassword);
    } else if(cfg->phonetUsername)
        fprintf(f,"phonet_attempted_username=%s\n",cfg->phonetUsername); //TODO: this is not parsed yet. Not used anywhere

    //nr of viewing permissions
    //TODO: nr_viewing permissions not yet used anywhere
    if(mdConfig->nrViewingPermissions)
        fprintf(f,"nr_viewing_permissions=%u\n",(unsigned int)mdConfig->nrViewingPermissions);

    releaseMdConfig(&mdConfig);
    fclose(f);

    //replace the files
    int r=rename(existingFilename,oldFilename);
    //-> ok to fail if no config file LOG0return(r,-1,"Renaming the old configuration file from %s to %s failed: %s",existingFilename,oldFilename,strerror(errno));
    r=rename(newFilename,existingFilename);
    LOG0return(r,-1,"Renaming the new configuration file from %s to %s failed: %s",newFilename,existingFilename,strerror(errno));

    //now save the info from cfg to the configuration
    struct ps_md_config *mdConfigW=getMdConfigWritable();
    mdConfigW->cfgFlags=cfg->cfgFlags;
    mdConfigW->permissionsAnonymous=cfg->permissionsAnonymous;
    mdConfigW->permissionsAuthWithKey=cfg->permissionsAuthWithKey;
    for(i=0;i<mdConfigW->nrAuthUsers;i++){
        free(mdConfigW->authUsers[i].username);
        free(mdConfigW->authUsers[i].password);
    }
    free(mdConfigW->authUsers);
    mdConfigW->authUsers=cfg->authUsers;
    mdConfigW->nrAuthUsers=cfg->nrAuthUsers;
    releaseMdConfigW(&mdConfigW);

    return 0;
}


