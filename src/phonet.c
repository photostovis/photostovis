#ifdef CONFIG_NO_PHONET
#error "This file should not be compiled in this build type"
#endif

#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include <curl/curl.h>
#include <openssl/md5.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <netdb.h>

#include <net/if.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <errno.h>

#include "photostovis.h"
#include "log.h"


//#ifdef NDEBUG
const char *KPhotostovisNetBaseUrl="https://photostovis.net/";
const char *KPhotostovisNetBaseUrlBackup="https://photostovis.org/net/";

const char *activeNetworkInterface=NULL;

/* this was too complicated to maintain
#else
const char *KPhotostovisNetBaseUrl="https://photostovis.net/net_dbg/";
const char *KPhotostovisNetBaseUrlBackup="https://photostovis.org/net_dbg/";
#endif*/

#define PHONET_URL_LEN 512
const long int phonetTimeoutInSeconds=60;

#ifdef __linux__
#define HAVE_SIOCGIFHWADDR
#include <sys/ioctl.h>
#include <net/if.h>

#include <sys/vfs.h> //statfs()
#endif
#ifdef __APPLE__
#define HAVE_GETIFADDRS
#include <net/if_dl.h>

#include <sys/mount.h> //statfs()
#endif

static time_t lastPhonetPing=0;
uint32_t phonetIp=0;
uint32_t phoorgIp=0;

static int getActiveInterfaceName(char* const if_name){
    struct ifaddrs *addrs, *tmp;
    int found=0;
    getifaddrs(&addrs);
    tmp=addrs;
    while(tmp){
        if(tmp->ifa_addr && tmp->ifa_addr->sa_family==AF_INET && (strcmp(tmp->ifa_name,"lo") && strcmp(tmp->ifa_name,"lo0"))){
            LOG0("getActiveInterfaceName: Selected interface [1try]: %s",tmp->ifa_name);
            strcpy(if_name, tmp->ifa_name);
            found=1;
            break;
        } else LOG0("getActiveInterfaceName: Skipped interface [1try]: %s (ifa_addr: %p, family: %d [%d])",tmp->ifa_name,tmp->ifa_addr,tmp->ifa_addr?tmp->ifa_addr->sa_family:0,AF_INET);
        tmp=tmp->ifa_next;
    }
    if(!found){
        //try finding eth0 or enpXsY
        while(tmp){
            int X,Y;
            int rx=sscanf(tmp->ifa_name,"enp%ds%d",&X,&Y);
            if(tmp->ifa_addr && (!strcmp(tmp->ifa_name,"eth0") || (rx==2 && X>=0 && X<256 && Y>=0 && Y<256))){
                LOG0("getActiveInterfaceName: Selected interface [2try]: %s",tmp->ifa_name);
                strcpy(if_name, tmp->ifa_name);
                found=1;
                break;
            } else LOG0("getActiveInterfaceName: Skipped interface [2try]: %s (ifa_addr: %p, family: %d [%d])",tmp->ifa_name,tmp->ifa_addr,tmp->ifa_addr?tmp->ifa_addr->sa_family:0,AF_INET);
            tmp=tmp->ifa_next;
        }
    }
    freeifaddrs(addrs);
    return found-1; //0-> interface found, -1->interface not found
}

#if defined(HAVE_SIOCGIFHWADDR)

static int get_mac_address(char* mac_addr, const char* if_name){
    struct ifreq ifinfo;
    if(!if_name){
        int r=getActiveInterfaceName(ifinfo.ifr_name);
        LOG0return(r,r,"get_mac_address: No suitable interface found.");
    } else strcpy(ifinfo.ifr_name, if_name);
    int sd = socket(AF_INET, SOCK_DGRAM, 0);
    int result = ioctl(sd, SIOCGIFHWADDR, &ifinfo);
    close(sd);

    if ((result == 0) && (ifinfo.ifr_hwaddr.sa_family == 1)) {
        memcpy(mac_addr, ifinfo.ifr_hwaddr.sa_data, IFHWADDRLEN);
        return 0;//found
    } else
        return -1; //not found
}
static uint32_t getInterfaceAddress(const char *if_name){
    struct ifreq ifinfo;
    if(!if_name){
        int r=getActiveInterfaceName(ifinfo.ifr_name);
        LOG0return(r,r,"getInterfaceAddress: No suitable interface found.");
    } else strcpy(ifinfo.ifr_name, if_name);
    int sd = socket(AF_INET, SOCK_DGRAM, 0);
    /*int result = */ioctl(sd, SIOCGIFADDR, &ifinfo);
    close(sd);

    uint32_t ip=htonl( ((struct sockaddr_in *)&ifinfo.ifr_addr)->sin_addr.s_addr );
    //printf("%s\n", inet_ntoa(((struct sockaddr_in *)&ifinfo.ifr_addr)->sin_addr));
    return ip;
}
#elif defined(HAVE_GETIFADDRS)

static int get_mac_address(char* mac_addr, char* if_name){
    struct ifaddrs *iflist, *cur;
    int found=0;
    if(!if_name)if_name="en0";
    if(getifaddrs(&iflist)==0){
        for(cur=iflist; cur; cur=cur->ifa_next){
            if((cur->ifa_addr->sa_family==AF_LINK) && !strcmp(cur->ifa_name, if_name) && cur->ifa_addr){
                struct sockaddr_dl *sdl=(struct sockaddr_dl*)cur->ifa_addr;
                ph_assert(sdl->sdl_alen==6,"MAC address len (%d) is not 6 as expected.",sdl->sdl_alen);
                memcpy(mac_addr, LLADDR(sdl), sdl->sdl_alen);
                found=1;
                break;
            }
        }
        freeifaddrs(iflist);
    }
    return found-1; //returns 0 for found, -1 for not found
}

/*
#if ! defined(IFT_ETHER)
#define IFT_ETHER 0x6 // Ethernet CSMACD
#endif

static int get_mac_address(char* mac_addr, char* if_name){
    struct ifreq *ifrp;
    struct ifconf ifc;
    char buffer[720];
    int socketfd,error,len,space=0;
    ifc.ifc_len=sizeof(buffer);
    len=ifc.ifc_len;
    ifc.ifc_buf=buffer;

    if(!if_name)if_name="en0";
    socketfd=socket(AF_INET,SOCK_DGRAM,0);

    LOG0("finding interface: <<%s>>",if_name);

    if((error=ioctl(socketfd,SIOCGIFCONF,&ifc))<0){
        perror("ioctl faild");
        exit(1);
    }
    if(ifc.ifc_len<=len){
        ifrp=ifc.ifc_req;
        do {
            struct sockaddr *sa=&ifrp->ifr_addr;

            if(((struct sockaddr_dl *)sa)->sdl_type==IFT_ETHER) {
                LOG0("Checking interface: <<%s>>",ifrp->ifr_name);
                if(!strcmp(if_name, ifrp->ifr_name)){
                    LOG0("HERE!!");
                    memcpy (mac_addr, LLADDR((struct sockaddr_dl *)&ifrp->ifr_addr), 6);
                    return 0;
                }
            }
            ifrp=(struct ifreq*)(sa->sa_len+(caddr_t)&ifrp->ifr_addr);
            space+=(int)sa->sa_len+sizeof(ifrp->ifr_name);
        }
        while(space<ifc.ifc_len);
    }
    return -1;
}*/

static uint32_t getInterfaceAddress(const char *if_name){
    struct ifaddrs *ifaddr, *ifa;
    int family,s,n;
    char ipAddr[NI_MAXHOST];
    uint32_t ip=0;

    if(getifaddrs(&ifaddr)==-1){
        LOG0("ERROR: could not get network interfaces. No local IP");
        exit(0);
    }

    //walk through the list
    for(ifa=ifaddr,n=0; ifa!=NULL; ifa=ifa->ifa_next, n++){
        if(ifa->ifa_addr==NULL)
            continue;

        family=ifa->ifa_addr->sa_family;
        if(family!=AF_INET)
            continue;

        //if here, we can get the IP address
        s=getnameinfo(ifa->ifa_addr,sizeof(struct sockaddr_in), ipAddr, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
        LOG0c(s,"getnameinfo() failed: %s\n", gai_strerror(s));
        if(s)exit(0);

        if(!strcmp(ipAddr,"127.0.0.1"))
            continue;

        LOG0("IP address: %s",ipAddr);
        if(!ip){
            //parse this address
            int i1,i2,i3,i4;
            int nrScanned=sscanf(ipAddr,"%d.%d.%d.%d",&i1,&i2,&i3,&i4);
            if(nrScanned!=4)continue;
            ip=(i1<<24)+(i2<<16)+(i3<<8)+i4;
        }

    }

    freeifaddrs(ifaddr);
    return ip;
}

#else
#error no definition for get_mac_address() on this platform!
#endif



void getUniqueID(char md5str[33], uint8_t cfgNumber){
    char macAddr[7];
    int i,nrz=0,r=get_mac_address(macAddr,(char*)activeNetworkInterface);
    macAddr[6]='\0';

    LOG0close_return(r,,shouldExit=EXIT_DO_NOT_RESTART,"We were unable to obtain a MAC address.");

    //check how many zeros we have
    for(i=0;i<6;i++)if(!macAddr[i])nrz++;
    ph_assert(nrz<4,"Too many zeros (%d) in MAC address.",nrz);

    //MD5 the mac address
    unsigned char md5Data[MD5_DIGEST_LENGTH];
    MD5((const unsigned char*)macAddr,6,md5Data);
    //last byte -> cfgNumber
    md5Data[MD5_DIGEST_LENGTH-1]=cfgNumber;
    //print
    r=0;
    for(i=0;i<MD5_DIGEST_LENGTH;i++)
        r+=snprintf(md5str+i+i,3,"%02X",md5Data[i]);
    ph_assert(r==32,NULL);
}

//some info about the filesystem
int updateSizes(unsigned *rootDataSizeMB, unsigned *rootFreeSizeMB, unsigned *dataDataSizeMB, unsigned *dataFreeSizeMB){
    int r,changesHappened=0;
    struct statfs sfs;
    r=statfs("/",&sfs);
    if(!r){
#ifdef __APPLE__
        LOG0("Root file system: type=%s, disk=%s",sfs.f_fstypename,sfs.f_mntfromname);
#endif
        *rootDataSizeMB=(sfs.f_bsize*sfs.f_blocks)>>20;
        *rootFreeSizeMB=(sfs.f_bsize*sfs.f_bavail)>>20;
        LOG0("Block size: %u bytes. Total blocks: %u (%u MB), free blocks: %u (%u MB), free available blocks: %u (%u MB).",(unsigned)sfs.f_bsize,
             (unsigned)sfs.f_blocks,*rootDataSizeMB,(unsigned)sfs.f_bfree,(unsigned)((sfs.f_bsize*sfs.f_bfree)>>30),(unsigned)sfs.f_bavail,*rootFreeSizeMB);
    } //else we suppose they are already set to 0

    struct ps_md_config const * const mdConfig=getMdConfigRd();
    r=statfs(mdConfig->rootFolder,&sfs);
    if(!r){
#ifdef __APPLE__
        LOG0("Data file system (%s): type=%s, disk=%s",mdConfig->rootFolder,sfs.f_fstypename,sfs.f_mntfromname);
#endif
        *dataDataSizeMB=(sfs.f_bsize*sfs.f_blocks)>>20;
        *dataFreeSizeMB=(sfs.f_bsize*sfs.f_bavail)>>20;
        LOG0("Block size: %u bytes. Total blocks: %u (%u MB), free blocks: %u (%u MB), free available blocks: %u (%u MB).",(unsigned)sfs.f_bsize,
             (unsigned)sfs.f_blocks,*dataDataSizeMB,(unsigned)sfs.f_bfree,(unsigned)((sfs.f_bsize*sfs.f_bfree)>>30),(unsigned)sfs.f_bavail,*dataFreeSizeMB);
    } //else we suppose they are already set to 0
    //check if they match our current values
    if(*rootDataSizeMB && *dataDataSizeMB && //so far we know we have valid data
            (*rootDataSizeMB!=mdConfig->rootDataSizeMB || *rootFreeSizeMB!=mdConfig->rootFreeSizeMB ||
             *dataDataSizeMB!=mdConfig->dataDataSizeMB || *dataFreeSizeMB!=mdConfig->dataFreeSizeMB)){
        //if here, we need to update stuff
        releaseMdConfig(&mdConfig);
        changesHappened=1;
        struct ps_md_config *mdConfigW=getMdConfigWritable();
        if(mdConfigW->rootDataSizeMB){
            ph_assert(mdConfigW->rootDataSizeMB==*rootDataSizeMB,"ERROR: root (/) data size changed from %u to %u. This cannot be.",mdConfigW->rootDataSizeMB,*rootDataSizeMB);
        } else mdConfigW->rootDataSizeMB=*rootDataSizeMB;
        if(mdConfigW->rootFreeSizeMB){
            LOG0c_do(mdConfigW->rootFreeSizeMB!=*rootFreeSizeMB,mdConfigW->rootFreeSizeMB=*rootFreeSizeMB,"INFO: root free space changed from %uMB to %uMB.",
                     mdConfigW->rootFreeSizeMB,*rootFreeSizeMB);
        } else mdConfigW->rootFreeSizeMB=*rootFreeSizeMB;

        if(mdConfigW->dataDataSizeMB){
            ph_assert(mdConfigW->dataDataSizeMB==*dataDataSizeMB,"ERROR: pictures partition (%s) data size changed from %u to %u. This cannot be.",mdConfigW->rootFolder,
                      mdConfigW->dataDataSizeMB,*dataDataSizeMB);
        } else mdConfigW->dataDataSizeMB=*dataDataSizeMB;
        if(mdConfigW->dataFreeSizeMB){
            LOG0c_do(mdConfigW->dataFreeSizeMB!=*dataFreeSizeMB,mdConfigW->dataFreeSizeMB=*dataFreeSizeMB,"INFO: pictures partition free space changed from %uMB to %uMB.",
                     mdConfigW->dataFreeSizeMB,*dataFreeSizeMB);
        } else mdConfigW->dataFreeSizeMB=*dataFreeSizeMB;
        releaseMdConfigW(&mdConfigW);
    } else
        releaseMdConfig(&mdConfig);
    //done
    return changesHappened;
}



int parseAndSaveCertificate(void const * const buf, const size_t len){
    FILE *f=NULL;
    int r,zipFilenameLen=0;
    char sslPath[PATH_MAX];
    struct ps_md_config const * const mdConfig=getMdConfigRd();
    //if(mdConfig->cfgFlags&CfgFlagCertificateIsNeeded){
        const char *KZipFilename="crtAndKey.zip";
        int l=strlen(mdConfig->rootFolder);
        ph_assert(mdConfig->httpsPort>0,"The https port is zero");

        memcpy(sslPath,mdConfig->rootFolder,l);
        memcpy(sslPath+l,KSSLExtraPath,strlen(KSSLExtraPath)+1);
        l=strlen(sslPath);
        releaseMdConfig(&mdConfig);

        mkdir(sslPath,0700); //can fail, no problem (e.g. already exists)
        zipFilenameLen=strlen(KZipFilename);
        memcpy(sslPath+l,KZipFilename,zipFilenameLen+1);

        f=fopen(sslPath,"w");
        LOG0return(!f,-1,"Unable to open the certificate zip file (%s): %s",sslPath,strerror(errno));
    //} else releaseMdConfig(&mdConfig);
    LOG0return(!f,-2,"We received a certificate but there is no need for one ....");

    r=fwrite(buf,len,1,f);
    fclose(f);
    LOG0return(r!=1,-3,"Could not write buffer to file (%s): %s",sslPath,strerror(errno));

    //if here, we need to unzip the file
    char command[PATH_MAX];
    snprintf(command,PATH_MAX,"unzip -o %s -d %s",sslPath,sslPath);
    //remove the zip filename from path
    command[strlen(command)-zipFilenameLen-1]='\0';
    LOG0("Command to execute: %s",command);
    r=system(command);
    LOG0return(r,-4,"The unzip command (%s) failed with code: %d",command,r);

    //if here we have the certificates
    unlink(sslPath);

    //we need to restart anyway, because we need to parse the new configuration and initialize the poll structure & everything
    shouldExit=EXIT_RESTART;

    struct ps_md_config *mdConfigW=getMdConfigWritable();
    mdConfigW->httpsSocket=r; //we do this so that it is closed nicely
    releaseMdConfigW(&mdConfigW);

    return 0;
}


////////////

static size_t write_data(void *buffer, size_t size, size_t nmemb, void *userp){
    char **answer=(char**)userp;
    int n=size*nmemb;
    //LOG0("write_data: %dx%d",size,nmemb);
    //TODO: the answer may be split in parts
    ph_assert(*answer==NULL,NULL);
    *answer=ph_malloc(n+1);
    memcpy(*answer,buffer,n);
    (*answer)[n]='\0';
    return n;
}

struct smart_answer {
    char *buf;
    size_t len;
};

static size_t write_data_smart(void *buffer, size_t size, size_t nmemb, void *userp){
    struct smart_answer *sa=(struct smart_answer*)userp;
    int n=size*nmemb;

    LOG0("write_data_smart: writing %d bytes",n);
    sa->buf=ph_realloc(sa->buf,sa->len+n+1);
    memcpy(sa->buf+sa->len,buffer,n);
    sa->len+=n;
    sa->buf[sa->len]='\0';
    return n;
}

enum PhonetCfgStatus phonetRequest(const char *baseUrl, const char *url, const char *params, char **curlError, char **answer, size_t *answerLen){
    char *reqUrl;
    int err,r,urlOffset=0,l,l2;
    struct smart_answer smartAnswer;
    memset(&smartAnswer,0,sizeof(struct smart_answer));
    char curlErrorString[CURL_ERROR_SIZE];

    CURL *curl=curl_easy_init();
    if(!curl){
        LOG0("ERROR: curl_easy_init() failed!");
        return PhonetCfgErrCurlInitFailed;
    }

    ph_assert(strstr(baseUrl,".net") || strstr(baseUrl,".org"),NULL);

    if(!baseUrl)
        baseUrl=KPhotostovisNetBaseUrl;
    ph_assert(url,NULL);
    l=strlen(url);
    l2=strlen(baseUrl);
    ph_assert(l>4,"url=<<%s>> l=%d",url,l); //smallest: ping.php
    reqUrl=ph_malloc(l2+l+1);
    memcpy(reqUrl,baseUrl,l2);
    memcpy(reqUrl+l2,url,l+1);

    ph_assert(!strncmp(reqUrl,"https://",8),NULL);
    if(USE_HTTP_WITH_PHONET==2){
        //replace https with http
        reqUrl[1]='h';reqUrl[2]='t';reqUrl[3]='t';reqUrl[4]='p';
        urlOffset=1;
        LOG0("Request URL changed from https to http.");
    }
    LOG0("Phonet request: %s, POST params: %s",reqUrl+urlOffset,params);

    curl_easy_setopt(curl,CURLOPT_URL,reqUrl+urlOffset);
    curl_easy_setopt(curl,CURLOPT_POSTFIELDS,params);
    curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION,write_data_smart);
    curl_easy_setopt(curl,CURLOPT_WRITEDATA,&smartAnswer);
    curl_easy_setopt(curl,CURLOPT_NOSIGNAL,1);
    curl_easy_setopt(curl,CURLOPT_TIMEOUT,phonetTimeoutInSeconds);
    curl_easy_setopt(curl,CURLOPT_ERRORBUFFER,curlErrorString);
    curlErrorString[0]='\0';

    LOG0("phonetRequest: performing...");
    CURLcode res=curl_easy_perform(curl);
    if(res!=CURLE_OK){
        LOG0("WARNING: curl_easy_perform(https://...) failed with err=%d: %s",res,curl_easy_strerror(res));
        if(USE_HTTP_WITH_PHONET==1){
            LOG0("Trying with http instead of https:");
            reqUrl[1]='h';reqUrl[2]='t';reqUrl[3]='t';reqUrl[4]='p';
            urlOffset=1;
            curl_easy_setopt(curl,CURLOPT_URL,reqUrl+urlOffset);
            res=curl_easy_perform(curl);
            if(res!=CURLE_OK)
                LOG0("WARNING: curl_easy_perform(http://...) failed with err=%d: %s",res,curl_easy_strerror(res));
        }

        if(res!=CURLE_OK){
            free(reqUrl);
            curl_easy_cleanup(curl);
            if(curlError && curlErrorString[0]!='\0')
                *curlError=ph_strdup(curlErrorString);
            return PhonetCfgErrCurlConnectionFailed;
        }
    }
    ph_assert(res==CURLE_OK,NULL);
    //get the IP of our connection
    char *ip;
    curl_easy_getinfo(curl,CURLINFO_PRIMARY_IP,&ip);
    if(ip){
        LOG0("phonetRequest: performing done. Remote IP: %s",ip);
        //make it a number
        if(strstr(baseUrl,".net"))
            phonetIp=ipToUInt32(ip);
        else
            phoorgIp=ipToUInt32(ip);
    } else LOG0("phonetRequest: performing done. NO REMOTE IP");
    curl_easy_cleanup(curl);

    //get the error code out of answer
    LOG0close_return(!smartAnswer.len,PhonetCfgErrAnswerParsingFailed,
      if(answerLen)*answerLen=0;
      if(answer && *answer)(*answer)[0]='\0',
      "WARNING: the server did not send an answer.");

    //if here, we have an answer
    ph_assert(smartAnswer.len,NULL);

    LOG0("phonet answer: %s",smartAnswer.buf);

    const char *KAnswerTrue="{\"a\":true";
    int answerParsed=1;
    if(strlen(KAnswerTrue)<=smartAnswer.len && !strncmp(smartAnswer.buf,KAnswerTrue,strlen(KAnswerTrue)))err=0; //no error
    else {
        //try parsing our error
        const char *KAnswerPartialFalse="{\"a\":false,\"err\":%d,";
        if(strlen(KAnswerPartialFalse)<=smartAnswer.len && !strncmp(smartAnswer.buf,KAnswerPartialFalse,11)){
            r=sscanf(smartAnswer.buf,KAnswerPartialFalse,&err);
            if(r!=1){
                //parsing failed
                err=PhonetCfgErrAnswerParsingFailed;
                LOG0("Parsing answer (%s) failed!",smartAnswer.buf);
            } else {
                //check answer range
                if(err!=0 && (err<PHONET_ERRORS_MIN || err>255)){
                    err=PhonetCfgErrAnswerOutsideRange;
                    LOG0("Answer err outside range. (%s)",smartAnswer.buf);
                }
            }
        } else {
            //the answer is not an object. Well, no problem, hopefully the calling party figures it out!
            err=0;
            answerParsed=0;
        }
    }
    if(!answerParsed){
        if(answer)
            *answer=smartAnswer.buf;
        else free(smartAnswer.buf);
        if(answerLen)
            *answerLen=smartAnswer.len;
    } else free(smartAnswer.buf);
    free(reqUrl);
    return err;
}

int phonetServerStart(){
    LOG0("Configuring the internet gateway to allow & redirect incoming connections to us.");
    if(!activeNetworkInterface){
        struct ifreq ifinfo;
        int r=getActiveInterfaceName(ifinfo.ifr_name);
        if(!r)
            activeNetworkInterface=ph_strdup(ifinfo.ifr_name);
    }

    struct ps_md_config const *mdConfig=getMdConfigRd();
    ph_assert(mdConfig->httpPort>0,NULL);
    uint16_t httpPort=mdConfig->httpPort,httpsPort=mdConfig->httpsPort,sshPortInt=mdConfig->sshPortInt,sshPortExt=mdConfig->sshPortExt,cfgFlags=mdConfig->cfgFlags;
    uint8_t cfgNumber=mdConfig->cfgNumber;
    int unregisterHttpxPorts=0;
    int upnpWorks=-1;
    int changes=0;
    if(mdConfig->cfgFlags&CfgFlagInetNotAllowed)
        unregisterHttpxPorts=1;
    if(mdConfig->httpsSocket<0)
        httpsPort=0;
    releaseMdConfig(&mdConfig);
    struct ps_md_net const *mdNet=getMdNetRd();
    uint32_t lanIP;
    uint32_t externalIP;
    releaseMdNet(&mdNet);
    enum GatewayCfgStatus gatewayCfgStatus;
    enum PhonetCfgStatus phonetCfgStatus;
    char *curlErrorStr=NULL;
    struct ps_md_net *mdNetW=NULL;

    //first, we need to configure our gateway to allow remote connection
    gatewayCfgStatus=configureGateway(httpPort,httpsPort,unregisterHttpxPorts,sshPortInt,sshPortExt,&lanIP,&externalIP,&upnpWorks);

    //do we have the "UPnP works" flag set correctly?
    if(upnpWorks && !(cfgFlags&CfgFlagUPnPWorks)){
        //we need to add the UPnP works flag
        struct ps_md_config *mdConfigW=getMdConfigWritable();
        mdConfigW->cfgFlags|=CfgFlagUPnPWorks;
        releaseMdConfigW(&mdConfigW);
        cfgFlags|=CfgFlagUPnPWorks;
        LOG0("Added UPnPworks flag. We will send a serverStart command");
        changes=1;
    }
    if(!upnpWorks && (cfgFlags&CfgFlagUPnPWorks)){
        //we need to REMOVE the UPnP works flag
        struct ps_md_config *mdConfigW=getMdConfigWritable();
        mdConfigW->cfgFlags&=~CfgFlagUPnPWorks;
        releaseMdConfigW(&mdConfigW);
        cfgFlags&=~CfgFlagUPnPWorks;
        LOG0("Removed UPnPworks flag. We will send a serverStart command");
        changes=1;
    }

    if(lanIP==INADDR_NONE){
        LOG0("Could not get LAN IP through UPnP. Get it from active net interface.");
        lanIP=getInterfaceAddress(activeNetworkInterface);
        char ipStr[16];
        printIP(lanIP,ipStr);
        LOG0("IP of active network interface is: %s",ipStr);
    }


    mdNet=getMdNetRd();
    if(mdNet->lanIP==lanIP && mdNet->externalIP==externalIP && mdNet->gatewayCfgStatus==gatewayCfgStatus && mdNet->phonetCfgStatus==0 && !changes){
        //everything is the same and we registered. No need to re-register
        LOG0("Internal & external IPs are the same, no need to re-register.");
        releaseMdNet(&mdNet);

        //when was the last time we pinged photostovis.net?
        time_t timeNow=time(NULL);
        if(timeNow-lastPhonetPing<PHONET_PING_INTERVAL){
            //LOG0(">>>>>>>>>> returning early");
            if(timeNow-lastPhonetPing<0)//time glitch
                lastPhonetPing=timeNow;
            return 0; //too early to ping photostovis.net
        }

        //if here, we should ping photostovis.net

        //create our URL and send the request
        LOG0("Pinging photostovis.net");
        char uid[33],reqParams[PHONET_URL_LEN];
        int reqParamsLen=0;
        getUniqueID(uid,cfgNumber);
        reqParamsLen=snprintf(reqParams,PHONET_URL_LEN,"uid=%s",uid);
        unsigned int rootDataSizeMB=0,rootFreeSizeMB=0,dataDataSizeMB=0,dataFreeSizeMB=0;
        if(updateSizes(&rootDataSizeMB,&rootFreeSizeMB,&dataDataSizeMB,&dataFreeSizeMB)){
            //is the system in a critical condition?
            if(rootFreeSizeMB<MIN_ROOT_MEMORY_BEFORE_CRITICAL)
                reqParamsLen+=snprintf(reqParams+reqParamsLen,PHONET_URL_LEN-reqParamsLen,"&rfs=%u",rootFreeSizeMB);
            if(dataFreeSizeMB<MIN_DATA_MEMORY_BEFORE_CRITICAL || dataFreeSizeMB*100/dataDataSizeMB<MIN_DATA_FREE_MEMORY_PERCENTAGE_BEFORE_CRITICAL)
                reqParamsLen+=snprintf(reqParams+reqParamsLen,PHONET_URL_LEN-reqParamsLen,"&dds=%u&dfs=%u",dataDataSizeMB,dataFreeSizeMB);
        };

        //LOG0(">>>>>>>>>> before phonetRequest (ping)");
        //this may take some time
        char *crtBuf=NULL;
        size_t crtBufLen=0;
        phonetCfgStatus=phonetRequest(KPhotostovisNetBaseUrl,"ping.php",reqParams,NULL,&crtBuf,&crtBufLen);
        if(crtBufLen>0){
            if(crtBufLen>500)
                parseAndSaveCertificate(crtBuf,crtBufLen);
            else LOG0("Interesting answer (length=%zu) received from server: %s",crtBufLen,crtBuf);
            free(crtBuf);
            crtBuf=NULL;
            crtBufLen=0;
        }
        //send request to backup
        phonetRequest(KPhotostovisNetBaseUrlBackup,"ping.php",reqParams,NULL,NULL,NULL);


        mdNetW=getMdNetWritable();
        mdNetW->phonetCfgStatus=phonetCfgStatus;
        releaseMdNetW(&mdNetW);
        if(phonetCfgStatus){
            if(phonetCfgStatus>=PHONET_ERRORS_MIN){

                if(phonetCfgStatus==19){
                    //external IP has changed
                }



                LOG0("Pinging photostovis.net failed. Will try again soon.");
            } else
                LOG0("Pinging photostovis.net partially failed.");
        } else {
            LOG0("Pinging photostovis.net completed successfully.");
            //fill in the time of reaching photostovis.net
            lastPhonetPing=timeNow;
        }
        return 0;
    }
    LOG0("LAN IP: %u==%u      INET IP: %u==%u      Gateway cfg: %u==%u      Phonet cfg: %d==0",mdNet->lanIP,lanIP,mdNet->externalIP,externalIP,
         (unsigned int)mdNet->gatewayCfgStatus,(unsigned int)gatewayCfgStatus,(int)mdNet->phonetCfgStatus);
    releaseMdNet(&mdNet);

    mdNetW=getMdNetWritable();
    mdNetW->lanIP=lanIP;
    mdNetW->externalIP=externalIP;
    mdNetW->gatewayCfgStatus=gatewayCfgStatus;
    releaseMdNetW(&mdNetW);
    //statusFlagsRemove(StatusFlagGatewayCfgInProgress);
    //statusFlagsAdd(StatusFlagGatewayCfgDone);

    //create our URL and send the request
    LOG0("Let photostovis.net know we are alive and running!");
    char uid[33],reqParams[PHONET_URL_LEN];
    int reqParamsLen=0;
    unsigned int version=(KPhotostovisVersionMajor<<8)+KPhotostovisVersionMinor;
    getUniqueID(uid,cfgNumber);
    //do we need to add filesystem size data?
    unsigned int rootDataSizeMB=0,rootFreeSizeMB=0,dataDataSizeMB=0,dataFreeSizeMB=0;
    updateSizes(&rootDataSizeMB,&rootFreeSizeMB,&dataDataSizeMB,&dataFreeSizeMB);

    mdConfig=getMdConfigRd();
    if(mdConfig->httpsSocket>=0)//we have SSL
        reqParamsLen=snprintf(reqParams,PHONET_URL_LEN,"uid=%s&ip=%u&port=%u&sslp=%u&v=%u&hw=%u&fl=%u",uid,lanIP,
                              (unsigned int)mdConfig->httpPort,(unsigned int)mdConfig->httpsPort,version,(unsigned int)myHardware,(unsigned int)cfgFlags);
    else
        reqParamsLen=snprintf(reqParams,PHONET_URL_LEN,"uid=%s&ip=%u&port=%u&v=%u&hw=%u&fl=%u",uid,lanIP,
                              (unsigned int)mdConfig->httpPort,version,(unsigned int)myHardware,(unsigned int)cfgFlags);

    if(mdConfig->phonetUsername){
        //we have username and password
        ph_assert(mdConfig->phonetPassword,NULL);
        reqParamsLen+=snprintf(reqParams+reqParamsLen,PHONET_URL_LEN-reqParamsLen,"&un=%s&pw=%s",mdConfig->phonetUsername,mdConfig->phonetPassword);
    }

    releaseMdConfig(&mdConfig);

    //is the system in a critical condition?
    if(rootFreeSizeMB<MIN_ROOT_MEMORY_BEFORE_CRITICAL)
        reqParamsLen+=snprintf(reqParams+reqParamsLen,PHONET_URL_LEN-reqParamsLen,"&rfs=%u",rootFreeSizeMB);

    if(dataFreeSizeMB<MIN_DATA_MEMORY_BEFORE_CRITICAL || dataFreeSizeMB*100/dataDataSizeMB<MIN_DATA_FREE_MEMORY_PERCENTAGE_BEFORE_CRITICAL)
        reqParamsLen+=snprintf(reqParams+reqParamsLen,PHONET_URL_LEN-reqParamsLen,"&dds=%u&dfs=%u",dataDataSizeMB,dataFreeSizeMB);

    //this may take some time
    //LOG0(">>>>>>>>>> before phonetRequest (serverStart)");
    phonetCfgStatus=phonetRequest(KPhotostovisNetBaseUrl,"serverStart.php",reqParams,&curlErrorStr,NULL,NULL);
    //TODO: we could return & save the certificate for serverStart as well
    phonetRequest(KPhotostovisNetBaseUrlBackup,"serverStart.php",reqParams,NULL,NULL,NULL);

    mdNetW=getMdNetWritable();
    mdNetW->phonetCfgStatus=phonetCfgStatus;
    if(mdNetW->phonetCfgCurlErrorString)
        free(mdNetW->phonetCfgCurlErrorString);
    mdNetW->phonetCfgCurlErrorString=curlErrorStr;
    releaseMdNetW(&mdNetW);

    if(phonetCfgStatus){
        if(phonetCfgStatus>=PHONET_ERRORS_MIN)
            LOG0("Registration to photostovis.net failed (%d). Will try again soon.",(int)phonetCfgStatus);
        else
            LOG0("Registration to photostovis.net partially failed (%d).",(int)phonetCfgStatus);
    } else {
        LOG0("Registration to photostovis.net completed successfully.");
        //fill in the time of reaching photostovis.net
        lastPhonetPing=time(NULL);
    }

    //sendlog();
    return 0;
}

void phonetServerStop(){
    //the username and password should be either both NULL or both with value
    struct ps_md_config const * const mdConfig=getMdConfigRd();
    ph_assert((mdConfig->phonetUsername && mdConfig->phonetPassword) || (!mdConfig->phonetUsername && !mdConfig->phonetPassword),NULL);

    //create our URL and send the request
    char uid[33],reqParams[PHONET_URL_LEN];
    getUniqueID(uid,mdConfig->cfgNumber);

    if(mdConfig->phonetUsername){
        //we have username and password
        ph_assert(mdConfig->phonetPassword,NULL);
        snprintf(reqParams,PHONET_URL_LEN,"uid=%s&un=%s&pw=%s",uid,mdConfig->phonetUsername,mdConfig->phonetPassword);
    } else {
        //we do NOT have username and password
        ph_assert(!mdConfig->phonetPassword,NULL);
        snprintf(reqParams,PHONET_URL_LEN,"uid=%s",uid);
    }
    releaseMdConfig(&mdConfig);

    phonetRequest(KPhotostovisNetBaseUrl,"serverStop.php",reqParams,NULL,NULL,NULL);
    phonetRequest(KPhotostovisNetBaseUrlBackup,"serverStop.php",reqParams,NULL,NULL,NULL);
    LOG0("Unregistered from photostovis.net");

    //close the gateway
    //closeGateway(httpPort,httpsPort,mdNet->lanIP);

}

int phonetServerRegister(const char *servername, const char *password){
    char uid[33],reqParams[PHONET_URL_LEN];
    //int reqParamsLen=0;

    struct ps_md_config const *mdConfig=getMdConfigRd();
    uint8_t cfgNumber=mdConfig->cfgNumber;
    releaseMdConfig(&mdConfig);


    getUniqueID(uid,cfgNumber);
    /*reqParamsLen=*/snprintf(reqParams,PHONET_URL_LEN,"uid=%s&un=%s&pw=%s",uid,servername,password);

    int r=phonetRequest(KPhotostovisNetBaseUrl,"registerun.php",reqParams,NULL,NULL,NULL);
    if(r)LOG0("WARNING: registering server (%s) failed: %d",servername,r);
    else LOG0("Server registration (%s) succeeded!",servername);
    phonetRequest(KPhotostovisNetBaseUrlBackup,"registerun.php",reqParams,NULL,NULL,NULL);

    return r;
}


enum PhonetCfgStatus phonetRequestString(const char *baseUrl, char *url, char **string){
    char *reqUrl;
    int urlOffset=0,l,l2;
    ph_assert(string,NULL);
    ph_assert(url,NULL);
    if(!baseUrl)
        baseUrl=KPhotostovisNetBaseUrl;

    CURL *curl=curl_easy_init();
    if(!curl)return PhonetCfgErrCurlInitFailed;

    l=strlen(url);
    l2=strlen(baseUrl);
    ph_assert(l>10,NULL);
    reqUrl=ph_malloc(l2+l+1);
    memcpy(reqUrl,baseUrl,l2);
    memcpy(reqUrl+l2,url,l+1);

    ph_assert(reqUrl[0]=='h' && reqUrl[1]=='t' && reqUrl[2]=='t' && reqUrl[3]=='p' && reqUrl[4]=='s',NULL);
    if(USE_HTTP_WITH_PHONET==2){
        //replace https with http
        reqUrl[1]='h';reqUrl[2]='t';reqUrl[3]='t';reqUrl[4]='p';
        urlOffset=1;
        //LOG0("Request URL changed from https to http.");
    }
    LOG0("Phonet request: %s",reqUrl+urlOffset);

    curl_easy_setopt(curl,CURLOPT_URL,reqUrl+urlOffset);
    curl_easy_setopt(curl,CURLOPT_WRITEFUNCTION,write_data);
    curl_easy_setopt(curl,CURLOPT_WRITEDATA,string);
    curl_easy_setopt(curl,CURLOPT_NOSIGNAL,1);
    curl_easy_setopt(curl,CURLOPT_TIMEOUT,phonetTimeoutInSeconds);

    CURLcode res=curl_easy_perform(curl);
    if(res!=CURLE_OK){
        LOG0("WARNING: curl_easy_perform(https://...) failed with err=%d: %s",res,curl_easy_strerror(res));
        if(USE_HTTP_WITH_PHONET==1){
            LOG0("Trying with http instead of https:");
            reqUrl[1]='h';reqUrl[2]='t';reqUrl[3]='t';reqUrl[4]='p';
            urlOffset=1;
            curl_easy_setopt(curl,CURLOPT_URL,reqUrl+urlOffset);
            res=curl_easy_perform(curl);
            LOG0c(res!=CURLE_OK,"WARNING: curl_easy_perform(http://...) failed with err=%d: %s",res,curl_easy_strerror(res));
        }
        free(reqUrl);

        if(res!=CURLE_OK){
            curl_easy_cleanup(curl);
            return PhonetCfgErrCurlConnectionFailed;
        }
    }
    ph_assert(res==CURLE_OK,NULL);

    curl_easy_cleanup(curl);

    return PhonetCfgSuccess;
}

enum PhonetCfgStatus phonetRequestFile(const char *baseUrl, char *url, FILE *f){
    char *reqUrl;
    int urlOffset=0,l,l2;
    ph_assert(f,NULL);
    ph_assert(url,NULL);
    if(!baseUrl)
        baseUrl=KPhotostovisNetBaseUrl;

    CURL *curl=curl_easy_init();
    if(!curl)return PhonetCfgErrCurlInitFailed;

    l=strlen(url);
    l2=strlen(baseUrl);
    ph_assert(l>10,NULL);
    reqUrl=ph_malloc(l2+l+1);
    memcpy(reqUrl,baseUrl,l2);
    memcpy(reqUrl+l2,url,l+1);

    ph_assert(reqUrl[0]=='h' && reqUrl[1]=='t' && reqUrl[2]=='t' && reqUrl[3]=='p' && reqUrl[4]=='s',NULL);
    if(USE_HTTP_WITH_PHONET==2){
        //replace https with http
        reqUrl[1]='h';reqUrl[2]='t';reqUrl[3]='t';reqUrl[4]='p';
        urlOffset=1;
        //LOG0("Request URL changed from https to http.");
    }
    LOG0("Phonet request: %s",reqUrl+urlOffset);

    curl_easy_setopt(curl,CURLOPT_URL,reqUrl+urlOffset);
    curl_easy_setopt(curl,CURLOPT_WRITEDATA,f);
    curl_easy_setopt(curl,CURLOPT_NOSIGNAL,1);
    curl_easy_setopt(curl,CURLOPT_TIMEOUT,phonetTimeoutInSeconds);

    CURLcode res=curl_easy_perform(curl);
    if(res!=CURLE_OK){
        LOG0("WARNING: curl_easy_perform(https://...) failed with err=%d: %s",res,curl_easy_strerror(res));
        if(USE_HTTP_WITH_PHONET==1){
            LOG0("Trying with http instead of https:");
            reqUrl[1]='h';reqUrl[2]='t';reqUrl[3]='t';reqUrl[4]='p';
            urlOffset=1;
            curl_easy_setopt(curl,CURLOPT_URL,reqUrl+urlOffset);
            res=curl_easy_perform(curl);
            LOG0c(res!=CURLE_OK,"WARNING: curl_easy_perform(http://...) failed with err=%d: %s",res,curl_easy_strerror(res));
        }
        free(reqUrl);

        if(res!=CURLE_OK){
            curl_easy_cleanup(curl);
            return PhonetCfgErrCurlConnectionFailed;
        }
    }
    ph_assert(res==CURLE_OK,NULL);

    curl_easy_cleanup(curl);

    return PhonetCfgSuccess;
}


struct ph_data2send {
    const char *logText;
    int pos;
    int remaining;
};

static size_t read_data(char *buffer, size_t size, size_t nitems, void *instream){
    size_t maxBytes=size*nitems;
    struct ph_data2send *data2read=(struct ph_data2send*)instream;

    LOG0("read_data++");
    if(maxBytes>=data2read->remaining){
        //we can send all our remaining data
        memcpy(buffer,data2read->logText+data2read->pos,data2read->remaining);
        maxBytes=data2read->remaining;
        data2read->pos+=data2read->remaining;
        data2read->remaining=0;
    } else {
        //we can only send part of the data
        memcpy(buffer,data2read->logText+data2read->pos,maxBytes);
        data2read->pos+=maxBytes;
        data2read->remaining-=maxBytes;
    }
    LOG0("read_data-- (returning: %d bytes)",(int)maxBytes);
    return maxBytes;
}

int phonetSendLog(const char *baseUrl, const char *logText, const int logTextLen){
    const char *url="somethingHappened2.php";
    char *reqUrl;
    int urlOffset=0,l,l2;
    if(!baseUrl)
        baseUrl=KPhotostovisNetBaseUrl;

    LOG0("phonetSendLog++");
    CURL *curl=curl_easy_init();
    LOG0return(!curl,-1,"phonetSendLog--: curl_easy_init ERROR!");

    struct ph_data2send data2read;

    l=strlen(url);
    l2=strlen(baseUrl);
    reqUrl=ph_malloc(l2+l+1);
    memcpy(reqUrl,baseUrl,l2);
    memcpy(reqUrl+l2,url,l+1);

    ph_assert(reqUrl[0]=='h' && reqUrl[1]=='t' && reqUrl[2]=='t' && reqUrl[3]=='p' && reqUrl[4]=='s',NULL);
    if(USE_HTTP_WITH_PHONET==2){
        //replace https with http
        reqUrl[1]='h';reqUrl[2]='t';reqUrl[3]='t';reqUrl[4]='p';
        urlOffset=1;
    }

    data2read.logText=logText;
    data2read.pos=0;
    data2read.remaining=logTextLen;

    curl_easy_setopt(curl,CURLOPT_URL,reqUrl+urlOffset);
    curl_easy_setopt(curl,CURLOPT_READFUNCTION,read_data);
    curl_easy_setopt(curl,CURLOPT_READDATA,&data2read);

    curl_easy_setopt(curl,CURLOPT_UPLOAD,1L);
    curl_easy_setopt(curl,CURLOPT_INFILESIZE,logTextLen);

    curl_easy_setopt(curl,CURLOPT_NOSIGNAL,1);
    curl_easy_setopt(curl,CURLOPT_TIMEOUT,phonetTimeoutInSeconds);

    LOG0("phonetSendLog: performing...");

    CURLcode res=curl_easy_perform(curl);
    if(res!=CURLE_OK){
        if(USE_HTTP_WITH_PHONET==1){
            LOG0("phonetSendLog: trying again");
            reqUrl[1]='h';reqUrl[2]='t';reqUrl[3]='t';reqUrl[4]='p';
            urlOffset=1;
            curl_easy_setopt(curl,CURLOPT_URL,reqUrl+urlOffset);
            res=curl_easy_perform(curl);
        }
        free(reqUrl);

        if(res!=CURLE_OK){
            curl_easy_cleanup(curl);
            LOG0("phonetSendLog-- (ERROR: %s)",curl_easy_strerror(res));
            return -1;
        }
    } else {
        free(reqUrl);
    }
    ph_assert(res==CURLE_OK,NULL);

    curl_easy_cleanup(curl);

    LOG0("phonetSendLog-- (OK)");
    return 0;
}

int checkCable(){
    struct ifaddrs *ifaddr, *ifa;
    int family,n,cableConnected=-1; //-1 -> we don't know
    //int s;
    //char ipAddr[NI_MAXHOST];

    if(getifaddrs(&ifaddr)==-1){
        LOG0("ERROR: could not get network interfaces.");
        return -1;
    }

    //walk through the list
    for(ifa=ifaddr,n=0; ifa!=NULL; ifa=ifa->ifa_next, n++){
        if(ifa->ifa_addr==NULL)
            continue;

        family=ifa->ifa_addr->sa_family;
        if(family!=AF_INET)
            continue;

        //if here, we can get the IP address
        /*
        s=getnameinfo(ifa->ifa_addr,sizeof(struct sockaddr_in), ipAddr, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
        LOG0c(s,"getnameinfo() failed: %s\n", gai_strerror(s));
        if(s)break;*/

        if(ifa->ifa_flags&IFF_LOOPBACK){
            LOG0("%s is a loopback, ignoring.",ifa->ifa_name);
            continue;
        }

        if(!(ifa->ifa_flags&IFF_UP)){
            LOG0("%s is NOT up, ignoring.",ifa->ifa_name);
            continue;
        }

        if(!activeNetworkInterface || !strcmp(ifa->ifa_name,activeNetworkInterface)){
            LOG0("%s is NOT the activeNetworkInterface, ignoring.",ifa->ifa_name);
            continue;
        }

#ifdef __linux__
#ifndef IFF_LOWER_UP
        continue; //basically, we don't know
#else
        if(ifa->ifa_flags&IFF_LOWER_UP){
            cableConnected=1;
            LOG0("%s: Cable connected",ifa->ifa_name);
        } else {
            cableConnected=0;
            LOG0("%s: Cable NOT connected",ifa->ifa_name);
        }
#endif
#endif

#ifdef __APPLE__
        if(ifa->ifa_flags&IFF_UP){
            cableConnected=1;
            LOG0("%s: Up",ifa->ifa_name);
        } else {
            cableConnected=0;
            LOG0("%s: Down",ifa->ifa_name);
        }
#endif
    }

    freeifaddrs(ifaddr);
    return cableConnected;
}
