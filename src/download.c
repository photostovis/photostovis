#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <pthread.h>
#include <fcntl.h>

#include <limits.h> //PATH_MAX

#include "log.h"
#include "photostovis.h"

#include "minizip/zip.h"

//there is a need to manage zip archives
struct ps_zip_archive {
    char *filename;
    uint8_t status; //0=being created, 1=created already, 2=creation failed (3=we create it)
    uint8_t counter; //how many threads/clients have a need to use this.
                     //After a thread posts a link to its client it starts a countdown of 10+1 minutes. When done, it decrements this and exits.
                     //when a download starts, it increments this, when it ends it decrements it.
                     //when an entity sets the countdown to zero it can remove the file and then remove this entry
};

static struct ps_zip_archive *archives=NULL;
static int nrArchives=0;
static pthread_mutex_t archivesMutex=PTHREAD_MUTEX_INITIALIZER;


void deleteAllZipArchives(){
    int i;
    pthread_mutex_lock(&archivesMutex);
    for(i=0;i<nrArchives;i++){
        ph_assert(archives[i].filename,NULL);
        unlink(archives[i].filename);
        free(archives[i].filename);
    }
    free(archives);
    nrArchives=0;
    pthread_mutex_unlock(&archivesMutex);
    //TODO: should we destroy this mutex?
}


static int scandir_known_files_filter(const struct dirent *d){
    //we filter out ., .. and all hidden folders and files. AND WE ALSO FILTER OUT SYNOLOGY-CREATED FOLDERS
    if(d->d_name[0]=='.' || d->d_name[0]=='@')
        return 0;

    int r,l=0,d_type=DT_UNKNOWN;
    //we return 1 for jpegs and folders
    if(d->d_type==DT_UNKNOWN)
        return 1; //will deal with it later
    if(d->d_type==DT_DIR || d_type==DT_DIR){
        return 1;
    }
    else if(d->d_type==DT_REG || d_type==DT_REG){
        //check extension
        if(!l)l=strlen(d->d_name);
        if(l>4){
            char *extension=(char*)d->d_name+l-4;
            if(!strcasecmp(extension,KJpegExtension) ||
                    !strcasecmp(extension,KMp4Extension) || !strcasecmp(extension,KM4vExtension) || !strcasecmp(extension,KMovExtension))
                r=1;
            else r=0;
        } else r=0;
    }
    else return 0;//skipping this file

    return r;
}

int addFolderToZipInfo(char *path, int pathLen, uint64_t *contentSize, unsigned int *nrPics, unsigned int *nrVideos, unsigned int *nrAlbums){
    struct dirent **namelist;
    int n=scandir(path,&namelist,scandir_known_files_filter,NULL);
    LOG0return(n<0,-1,"addFolderToZipInfo: scandir error in %s: %s",path,strerror(errno));
    int l,i,r;
    int statFailed=0;
    struct stat st;

    if(path[pathLen-1]!='/'){
        path[pathLen++]='/';
        path[pathLen]='\0';
    }

    for(i=0;i<n;i++){
        struct dirent *d=namelist[i];
        l=strlen(d->d_name);
        memcpy(path+pathLen,d->d_name,l+1);
        if(!stat(path,&st)){
            //stat succeeded
            //find the type if unknown
            if(d->d_type==DT_UNKNOWN){
                //we must find the type
                if(S_ISREG(st.st_mode))d->d_type=DT_REG;
                else if(S_ISDIR(st.st_mode))d->d_type=DT_DIR;
                else ph_assert(0,"Cannot find entry type. ");
            }

            //do we count this file?
            if(d->d_type==DT_REG){
                //check extension
                r=1;
                if(l>4){
                    char *extension=(char*)d->d_name+l-4;
                    if(!strcasecmp(extension,KJpegExtension))
                        (*nrPics)++;
                    else if(!strcasecmp(extension,KMp4Extension) || !strcasecmp(extension,KM4vExtension) || !strcasecmp(extension,KMovExtension))
                        (*nrVideos)++;
                    else r=0;
                } else r=0;
                if(r)
                    *contentSize+=st.st_size;
            } else if(d->d_type==DT_DIR){
                //go recursively
                (*nrAlbums)++;
                r=addFolderToZipInfo(path,pathLen+l,contentSize,nrPics,nrVideos,nrAlbums);
                if(r)statFailed++;
            } else ph_assert(0,"Unknown entry type.");
        } else {
            //stat failing can happen if the file was deleted meanwhile
            LOG0("stat failed for %s",path);
            statFailed++;
        }

        path[pathLen]='\0';
        free(d);
    }//for
    free(namelist);

    return 0;
}


int getZipDownloadInfo(struct md_entry_id const * const id, uint64_t *contentSize, unsigned int *nrPics, unsigned int *nrVideos, unsigned int *nrAlbums){
    *contentSize=0;
    *nrPics=*nrVideos=*nrAlbums=0;

    //first, create the path of this folder
    ph_assert(id,NULL);
    char path[PATH_MAX];
    struct md_album_entry const *e;
    int r=getEntryAndPath(id,KPicturesFolder,path,NULL,&e,NULL);

    LOG0close_return(r,-1,releaseMdPics(NULL),"WARNING: getZipDownloadInfo: We were unable to find the requested ID.");
    LOG0close_return(!(e->flags&FlagIsAlbum),-1,releaseMdPics(NULL),"WARNING: getZipDownloadInfo: requested element is not an album");
    releaseMdPics(NULL);e=NULL;

    int pathLen=strlen(path);

    //scan info in this folder and subfolders
    r=addFolderToZipInfo(path,pathLen,contentSize,nrPics,nrVideos,nrAlbums);

    //we are done with scanning the info
    LOG0("getZipDownloadInfo: %s has %"UINT64_FMT" MB of data, out of which %u pictures, %u videos and %u sub-albums.",path,(*contentSize)>>20,*nrPics,*nrVideos,*nrAlbums);

    return r;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

struct zipthread_data {
    struct md_entry_id id;
    uint64_t contentSize;
    uint64_t addedBytes;
    uint64_t bytesAtLastMsg;
    //char *key;
    uint8_t visibleLayer; //when we have a key, e.g. visibleLayer==2 means that we do NOT send the info about the first 2 layers
    //data about the network client
    struct ps_session *mySession;
    uint64_t sessionId;
};

static int replyIfSameSession(struct zipthread_data *ztd, char * const buffer, uint32_t bufferLen, uint32_t bufferOffset){
    int r=0;
    struct ps_md_net const * mdNet=getMdNetRd();
    if(ztd->sessionId==ztd->mySession->sessionId){
        //yes, it is the same
        enqueueWebsocketPacket(ztd->mySession,buffer,bufferLen,bufferOffset,1,1);
    } else //no, it is not
        r=-1;
    releaseMdNet(&mdNet);
    return r; //either 0 for success or -1 for error
}

void unregisterFromArchive(struct ps_zip_archive *ourArchive){
    if(!ourArchive)return;

    LOG0("unregisterFromArchive++ (counter: %d)",ourArchive->counter-1);
    int i;
    pthread_mutex_lock(&archivesMutex);
    ourArchive->counter--;
    if(ourArchive->counter){
        //unlock and return
        pthread_mutex_unlock(&archivesMutex);
        LOG0("unregisterFromArchive-- (counter NOT zero: %d)",ourArchive->counter);
        return;
    }
    //if here, we need to remove the current archive from the list
    LOG0("Deleting&unregistering zip archive: %s",ourArchive->filename);
    unlink(ourArchive->filename);
    free(ourArchive->filename);
    i=ourArchive-archives;
    LOG0("i=%d, ourArchive=%p, archives=%p",i,ourArchive,archives);
    ph_assert(i>=0 && i<nrArchives,"i=%d, nrArchives=%d",i,nrArchives);
    memmove(archives+i,archives+i+1,(nrArchives-i-1)*sizeof(struct ps_zip_archive));
    nrArchives--;
    if(nrArchives)
        archives=ph_realloc(archives,nrArchives*sizeof(struct ps_zip_archive));
    else {
        //if we do not do this trick, the ph_realloc will "fail" when allocating 0 bytes
        free(archives);
        archives=NULL;
    }
    LOG0("unregisterFromArchive--: Remaining zip archives: %d",nrArchives);
    pthread_mutex_unlock(&archivesMutex);
}

void answerZipError(struct zipthread_data *ztd, int err, struct ps_zip_archive *ourArchive){
    const char *KZipArchiveErrorTemplate="{\"type\":\"zipdld\",\"err\":%d}";

    char buf[50];
    int bufLen=sprintf(buf+KWebsocketBufferOffset,KZipArchiveErrorTemplate,err);

    LOG0("prepareZipFileThread: ERROR %d.",err);

    replyIfSameSession(ztd,buf,bufLen,KWebsocketBufferOffset);

    free(ztd);
    ztd=NULL;

    if(ourArchive)
        unregisterFromArchive(ourArchive);
}

void notifyZipClient(struct zipthread_data *ztd){
    const char *KZipArchiveMsgTemplate="{\"type\":\"zipdld\",\"prg\":%d}";
    int progress=(int)(ztd->addedBytes*100/ztd->contentSize);

    char buf[50];
    int bufLen=sprintf(buf+KWebsocketBufferOffset,KZipArchiveMsgTemplate,progress);

    LOG0("notifyZipClient: progress: %d.",progress);

    replyIfSameSession(ztd,buf,bufLen,KWebsocketBufferOffset);

    ztd->bytesAtLastMsg=ztd->addedBytes;
}

void ph_strreplace(char *dst, const char *src, const char *strReplaced, const char *strReplacing){
    char *pos=strstr(src,strReplaced);
    if(pos){
        int l=pos-src,lenReplacing=strlen(strReplacing),lenReplaced=strlen(strReplaced);
        memcpy(dst,src,l);
        memcpy(dst+l,strReplacing,lenReplacing);
        memcpy(dst+l+lenReplacing,pos+lenReplaced,strlen(src)-l-lenReplaced+1);
    } else ph_assert(0,NULL); //No usecase where this should fail

}

int addFilesToZip(struct zipthread_data *ztd, zipFile zf, char *path, int pathLen, char *zipPath, int zipPathLen){
    char *buf=NULL;
    zip_fileinfo zi;
    struct stat st;
    FILE *f=NULL;
    struct dirent **namelist;
    int i,l,r,bufLen,weHadErrors=0;
    int n=scandir(path,&namelist,&scandir_known_files_filter,NULL);
    LOG0return(n<0,-1,"addFilesToZip: scandir error in %s: %s",path,strerror(errno));

    LOG0("addFilesToZip: %d entries in folder %s",n,path);

    if(path[pathLen-1]!='/'){
        path[pathLen++]='/';
        path[pathLen]='\0';
    }
    if(zipPathLen>0 && zipPath[zipPathLen-1]!='/'){
        zipPath[zipPathLen++]='/';
        zipPath[zipPathLen]='\0';
    }

    //add files to the zip
    for(i=0;i<n;i++){
        struct dirent *d=namelist[i];
        l=strlen(d->d_name);
        memcpy(path+pathLen,d->d_name,l+1);

        if(!stat(path,&st)){
            //stat succeeded
            //find the type if unknown
            if(d->d_type==DT_UNKNOWN){
                //we must find the type
                if(S_ISREG(st.st_mode))d->d_type=DT_REG;
                else if(S_ISDIR(st.st_mode))d->d_type=DT_DIR;
                else ph_assert(0,"Cannot find entry type for %s.",path);
            }
        } else {
            LOG0("stat failed for %s",path);
            weHadErrors=1;
            continue;
        }

        if(d->d_type!=DT_REG)
            continue; //we skip folders for the moment

        //open the file to compress
        f=fopen(path,"r");
        LOG0close_continue(!f,weHadErrors=1,"WARNING: File %s could not be opened. Skipping.",path);

        //set the additional data and add the file to the zip
        memset(&zi,0,sizeof(zip_fileinfo));
        //fill in modification time (from stat)
        zi.dosDate=st.st_mtime;

        //build the zip filename
        memcpy(zipPath+zipPathLen,d->d_name,l+1);
        //LOG0("Adding to zip: %s as %s",path,zipPath);

        r=zipOpenNewFileInZip64(zf,zipPath,&zi,NULL,0,NULL,0,NULL,0,0,1);
        LOG0close_break(r!=ZIP_OK,weHadErrors=1;fclose(f);f=NULL,"ERROR opening %s in zipfile. Exiting zip creration.",zipPath);

        //read the file and add it to the zipfile
        buf=ph_malloc(ZIP_BUF_SIZE);
        while(1){
            errno=0;
            bufLen=fread(buf,1,ZIP_BUF_SIZE,f);
            LOG0close_break(!bufLen && ferror(f),weHadErrors=1,"Error reading from file (%s): %s",path,strerror(errno));
            if(!bufLen)break;

            //if here, we add the data to the zip file
            r=zipWriteInFileInZip (zf,buf,bufLen);
            LOG0close_break(r!=ZIP_OK,weHadErrors=1,"ERROR writing data into zipfile. Exiting zip creration.");
            //increase the number of processed bytes
            ztd->addedBytes+=bufLen;
            //LOG0("Wrote: %dMB",(ztd->addedBytes-ztd->bytesAtLastMsg)>>20);
            if(((ztd->addedBytes-ztd->bytesAtLastMsg)>>20)>ZIP_NOTIFY_EVERY_MB){
                notifyZipClient(ztd);
            }
        }
        free(buf);buf=NULL;
        //we are done with this file
        fclose(f);f=NULL;
        r=zipCloseFileInZip(zf);
        if(r!=ZIP_OK)
            weHadErrors=1;
    }

    //add folders to the zip
    for(i=0;i<n;i++){
        struct dirent *d=namelist[i];
        if(d->d_type!=DT_DIR || weHadErrors){
            free(d);
            continue;
        }
        //if here, this is a folder
        l=strlen(d->d_name);
        memcpy(path+pathLen,d->d_name,l+1);
        memcpy(zipPath+zipPathLen,d->d_name,l+1);
        r=addFilesToZip(ztd,zf,path,pathLen+l,zipPath,zipPathLen+l);
        if(r)
            weHadErrors=1;

        free(d);
    }
    free(namelist);
    free(buf);

    return weHadErrors;
}

void *prepareZipFileThread(void *data){
    struct zipthread_data *ztd=(struct zipthread_data *)data;

    //first, create the path of this folder
    ph_assert(ztd,NULL);
    char path[PATH_MAX];
    char zipPath[PATH_MAX];
    char const *lastName;
    struct md_album_entry const *e;
    int r=getEntryAndPath(&ztd->id,KPicturesFolder,path,&lastName,&e,NULL);

    LOG0close_return(r,NULL,releaseMdPics(NULL);answerZipError(ztd,-1,NULL),"WARNING: prepareZipFileThread: We were unable to find the requested ID.");
    LOG0close_return(!(e->flags&FlagIsAlbum),NULL,releaseMdPics(NULL);answerZipError(ztd,-2,NULL),"WARNING: prepareZipFileThread: requested element is not an album");
    releaseMdPics(NULL);e=NULL;

    //create the zip filename
    ph_strreplace(zipPath,path,KPicturesFolder,KPhotostovisData);
    if(lastName)
        sprintf(zipPath+strlen(zipPath),"/%s.zip",lastName);
    else
        strcat(zipPath,"/album.zip");

    //do we already have this zip archive?
    struct ps_zip_archive *ourArchive=NULL;
    int i,archiveStatus; //0=being created by somebody else (we wait for it), 1=already created, 2=we create it
    pthread_mutex_lock(&archivesMutex);
    for(i=0;i<nrArchives;i++)
        if(!strcmp(zipPath,archives[i].filename)){
            //we found our archive
            ourArchive=&archives[i];
            archiveStatus=ourArchive->status;
            if(archiveStatus!=2){
                ourArchive->counter++;
                ph_assert(archiveStatus==0 || archiveStatus==1,NULL);
            }
            break;
        }
    if(!ourArchive){
        //we have to add our archive
        archives=ph_realloc(archives,(nrArchives+1)*sizeof(struct ps_zip_archive));
        archives[nrArchives].counter=1;
        archives[nrArchives].status=0;
        archives[nrArchives].filename=ph_strdup(zipPath);
        ourArchive=&archives[nrArchives];
        nrArchives++;
        archiveStatus=3;
    }
    pthread_mutex_unlock(&archivesMutex);
    ph_assert(ourArchive,NULL);
    LOG0close_return(archiveStatus==2,NULL,answerZipError(ztd,-3,NULL),"ERROR: archive (%s) creation failed in some other thread.",zipPath);

    if(archiveStatus==3){
        LOG0("We have to create our archive.");
        //we have to create the archive (most common status)
        ztd->addedBytes=ztd->bytesAtLastMsg=0;
        //open the zip file
        zipFile zf=zipOpen64(zipPath,APPEND_STATUS_CREATE);
        LOG0close_return(!zf,NULL,answerZipError(ztd,-4,ourArchive),"ERROR: could not open the zip output file (%s)",zipPath);

        int pathLen=strlen(path);
        zipPath[0]='\0';
        r=addFilesToZip(ztd,zf,path,pathLen,zipPath,0);
        zipClose(zf,NULL);zf=NULL;
        LOG0close_return(r,NULL,answerZipError(ztd,-5,ourArchive),"WARNING: prepareZipFileThread: adding files to the zipfile was not entirely successfull.");

        //mark the zip as being ready
        pthread_mutex_lock(&archivesMutex);
        ph_assert(ourArchive->status==0,NULL);
        ourArchive->status=archiveStatus=1;
        pthread_mutex_unlock(&archivesMutex);
    } else if(archiveStatus==0){
        LOG0("Waiting for the archive to become available (created in some other thread");
        //wait for the archive to become available (or to fail)
        while(1){
            sleep(1);
            pthread_mutex_lock(&archivesMutex);
            if(ourArchive->status==1){
                //great, the archive is ready!
                archiveStatus=1;
                pthread_mutex_unlock(&archivesMutex);
                break;
            } else if(ourArchive->status==2){
                //archive creation in another thread failed
                pthread_mutex_unlock(&archivesMutex);
                LOG0close_return(1,NULL,answerZipError(ztd,-6,ourArchive),"ERROR: archive (%s) creation failed in some other thread.",zipPath);
            } else {
                ph_assert(ourArchive->status==0,NULL);
                pthread_mutex_unlock(&archivesMutex);
            }
        }//while
    }

    //if we are here it means success!
    ph_assert(archiveStatus==1,NULL);

    //answer "success"
    const char *KZipArchiveSuccessTemplate="{\"type\":\"zipdld\",\"link\":\"/d";     //\"}";
    int l=strlen(KZipArchiveSuccessTemplate);

    char buf[128];
    memcpy(buf+KWebsocketBufferOffset,KZipArchiveSuccessTemplate,l+1);
    int bufLen=l;

    for(i=ztd->visibleLayer;i<ztd->id.level;i++){
        sprintf(buf+KWebsocketBufferOffset+bufLen,"/%08X",ztd->id.nameCRC[i]);
        bufLen+=9;
    }

    memcpy(buf+KWebsocketBufferOffset+bufLen,"\"}",3);
    bufLen+=2;
    LOG0("prepareZipFileThread: success. Sending websocket packet: %s",buf+KWebsocketBufferOffset);

    replyIfSameSession(ztd,buf,bufLen,KWebsocketBufferOffset);


    free(ztd);
    data=ztd=NULL;

    //sleep the required time (timeout)
    //even if r==-1 perhaps we should still sleep, perhaps the user will ask again for the zip file
    r=ZIP_DOWNLOAD_TIMEOUT;
    while(r)
        r=sleep(r);

    //unregister ourselves from the archive
    unregisterFromArchive(ourArchive);
    LOG0("Zip Thread %x exiting.",(unsigned int)(intptr_t)pthread_self());

    return NULL;
}

int prepareZipFile(struct md_entry_id const * const id, const uint8_t visibleLayer, uint64_t contentSize, struct ps_session * const mySession){
    //this function creates a zipfile on disk and sends the progress over websocket every now and then
    int r;
    struct zipthread_data *ztd=ph_malloc(sizeof(struct zipthread_data));
    ztd->id=*id;
    ztd->contentSize=contentSize;
    ztd->mySession=mySession;
    ztd->sessionId=mySession->sessionId;
    ztd->visibleLayer=visibleLayer;

    //create a thread and handle the zipfile creation there
    pthread_t t;
    pthread_attr_t attr;
    r=pthread_attr_init(&attr);
    ph_assert(!r,NULL);//the above should not fail
    r=pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
    ph_assert(!r,NULL);//the above should not fail

    r=pthread_create(&t,&attr,&prepareZipFileThread,ztd);//transfering ownership of data
    LOG0close_return(r,-1,free(ztd),"ERROR (%d) creating a zip thread.",r);

    LOG0("Zip Thread %x created.",(unsigned int)(intptr_t)t);
    return 0;
}




int getZipDownload(struct md_entry_id const * const id, const int httpKeepAlive, const char *dateStr, int socket, void *ssl){
    //first, create the path of this folder
    ph_assert(id,NULL);
    char path[PATH_MAX];
    char const *lastName;
    struct md_album_entry const *e;
    int r=getEntryAndPath(id,KPhotostovisData,path,&lastName,&e,NULL);

    LOG0close_return(r,-1,releaseMdPics(NULL),"WARNING: getZipDownload: We were unable to find the requested ID (of the archive).");
    LOG0close_return(!(e->flags&FlagIsAlbum),-1,releaseMdPics(NULL),"WARNING: getZipDownload: requested archive is not an album");
    releaseMdPics(NULL);e=NULL;

    //create the zip filename
    int pathLen=strlen(path),nameStartLen;
    if(lastName)
        sprintf(path+pathLen,"/%s.zip",lastName);
    else
        strcat(path,"/album.zip");
    nameStartLen=pathLen+1;
    LOG0("Zip archive filename: %s",path);
    //pathLen=strlen(path); -> not needed

    //open the archive file
    int r2,fd=open(path,O_RDONLY);
    LOG0return(fd==-1,-1,"Unable to open archive (%s)",path);

    //register ourselves in the archive (manager)
    struct ps_zip_archive *ourArchive=NULL;
    int i;
    pthread_mutex_lock(&archivesMutex);
    for(i=0;i<nrArchives;i++)
        if(!strcmp(path,archives[i].filename)){
            //we found our archive
            ourArchive=&archives[i];
            LOG0close_return(ourArchive->status!=1,-1,pthread_mutex_unlock(&archivesMutex);close(fd),"WARNING: Archive not ready (status=%d)",ourArchive->status);
            ourArchive->counter++;
            break;
        }
    pthread_mutex_unlock(&archivesMutex);
    LOG0close_return(!ourArchive,-1,close(fd),"WARNING: Archive not found!");

    //find the file size
    int sentBytes;
    uint64_t remainingSize,fileSize=lseek(fd,0,SEEK_END);
    LOG0close_return(fileSize==(uint64_t)-1,-1,close(fd),"WARNING: lseek failed!");
    lseek(fd,0,SEEK_SET);

    //write and send the header
    pthread_t threadId=pthread_self();
    static const char *KConnectionKeepAlive="Keep-Alive";
    static const char *KConnectionClose="Close";
    static const char *KHTTPDownloadOk=
            "HTTP/1.1 200 OK\r\n"
            "Connection: %s\r\n"
            "Content-Type: application/octet-stream\r\n"
            "Content-Length: %"UINT64_FMT"\r\n"
            "Content-Disposition: attachment; filename=\"%s\"\r\n"
            "Server: " KPhotostovisServerString "\r\n"
            "Cache-Control: private\r\n"
            "Date: %s\r\n"
            "\r\n";
    char rspBuf[HTTP_RESPONSE_BUFFER_SIZE];
    r=snprintf(rspBuf,HTTP_RESPONSE_BUFFER_SIZE,KHTTPDownloadOk,httpKeepAlive?KConnectionKeepAlive:KConnectionClose,fileSize,path+nameStartLen,dateStr);
    LOG0return(r<0,-1,"ERROR error writing the HTTP response header to the buffer");
    LOG0return(r>=HTTP_RESPONSE_BUFFER_SIZE,-1,
            "ERROR: Insufficient buffer size (%d) for the HTTP response header",HTTP_RESPONSE_BUFFER_SIZE);
    ph_assert(r==strlen(rspBuf),"%d!=%zu",r,strlen(rspBuf));
    r2=srvWriteGeneric(socket,ssl,rspBuf,r,0);
    LOG0return(r2<0,-1,"ERROR (in thread: %x) writing the HTTP response header: %s",(unsigned int)(intptr_t)threadId,strerror(errno));
    LOG0return(r2!=r,-1,"ERROR (in thread: %x) writing the HTTP response header. Wrote %d bytes instead of %d",(unsigned int)(intptr_t)threadId,r2,r);

    //allocate the buffer
    char *b=NULL;
    if(fileSize<VIDEO_BUF_LEN)
        b=(char*)ph_malloc(fileSize);
    else
        b=(char*)ph_malloc(VIDEO_BUF_LEN);
    remainingSize=fileSize;

    //clear the timeout
    struct timeval timeout;
    timeout.tv_sec=0;
    timeout.tv_usec=0;
    setsockopt(socket,SOL_SOCKET,SO_SNDTIMEO,&timeout,sizeof(timeout));

    //read and send the file
    while(remainingSize>0){
        if(remainingSize<VIDEO_BUF_LEN)
            r=read(fd,b,remainingSize);
        else
            r=read(fd,b,VIDEO_BUF_LEN);
        //send
        //LOG0("Read %d bytes, remaining: %d/%d",r,remainingSize-r,fileSize);
        if(r<=0){
            if(r==0)ph_assert(0,"Error reading from zip file: zero/eof. What we know: (remaining/total): %"UINT64_FMT"/%"UINT64_FMT,remainingSize,fileSize);
            ph_assert(0,"Error (%d) reading from zip file: %s",r,strerror(errno));
        }
        //TODO: can we recover somehow?

        //LOG0("Sending %d bytes.",r);
        sentBytes=0;
        while(sentBytes<r){
            r2=srvWriteGeneric(socket,ssl,b+sentBytes,r-sentBytes,0);
            LOG0("Zip download: sent %d bytes (thread: %x)",r2,(unsigned int)(intptr_t)threadId);
            LOG0c(r2==-1,"Socket error: %s",strerror(errno));

            LOG0close_return(r2==-1,-2,free(b);close(fd),"Browser closed the socket. Sent %"UINT64_FMT" bytes. (thread: %x).",
                    fileSize-remainingSize-sentBytes,(unsigned int)(intptr_t)threadId);

            //if we are here it means thtat we managed to send something
            sentBytes+=r2;
            ph_assert(sentBytes<=r,NULL);
        }
        remainingSize-=sentBytes;
    }

    close(fd);
    free(b);
    unregisterFromArchive(ourArchive);

    return 0;
}
