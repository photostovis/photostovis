#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h> //PATH_MAX
#include <errno.h>
#include <sys/stat.h>

#include <openssl/rand.h>

#include "photostovis.h"
#include "log.h"

#include <libexif/exif-data.h>

extern const char *KThumbnailsFilename;

struct ps_thumb_cache {
    uint16_t flags;
    uint16_t nrEntries; //for albums: nr of entries in the structure. For pictures: 0xFFFF
    uint32_t nameCRC;
    union {
        struct {
            //pictures (& videos, maybe later)
            int32_t offset; //from the beginning of the structure
            int32_t size;
            int32_t captureTime;
            int32_t imageSize;
            int32_t modificationTime;
#ifndef NDEBUG
            int32_t zzz;
#endif
        };
        struct {
            //albums
            struct ps_thumb_cache *album;
            int32_t allocatedSize;
            uint32_t totalNrThumbnails;
        };
    };
};//4+4(8)+4




static unsigned int cacheMaxSize=0; //in bytes
static unsigned int cacheCurrentSize=0;
static unsigned int cacheNrThumbnails=0;
static unsigned int buildingCache=0;
static struct ps_thumb_cache *thumbsCachePR1=NULL;
static struct ps_thumb_cache *thumbsCachePR2=NULL;

static void index2string(struct md_entry_id const * const id, char *path, const int pathSize){
    int i,l=0;
    for(i=0;i<id->level;i++){
        l+=snprintf(path+l,pathSize-l,"/%08X",id->nameCRC[i]);
        if(l>pathSize)return;
    }
}

static char *getThumbnailFromFile(struct md_entry_id const * const id, const int pixelRatio, size_t *bufLen, char *eTag, int eTagSize, const char *reqETag,
                                  int * const isVideo){
    //LOG0("getThumbnailFromFile++");
    ph_assert(id,NULL);
    *bufLen=0; //in case we return NULL
    LOG0return(!id->level,NULL,"getThumbnailFromFile--: WARNING: Cannot get thumbnail for root level");

    char path[PATH_MAX];
    //the following code is based on getEntryAndPath
    struct ps_md_pics const * const mdPics=getMdPicsRd();
    int pathLen=0,posPicturesFolder=0,posLastSlash=0;
    int r,i,j,l,index=0,currentN=mdPics->rootAlbum->entriesIndex;
    struct md_album_entry *currentAlbum=mdPics->rootAlbum->album,*currentEntry=NULL;
    char const *cname;
    const uint16_t KFlagHasAThumbnail=FlagHasExifThumbnail|FlagHasScaledThumbnail|FlagIsAlbum;
    uint16_t entryFlags; //cached

    if(isVideo)*isVideo=-1; //we do not know, yet

    for(i=0;i<id->level;i++){
        //we should not trust data coming from the user
        LOG0close_return(!currentN,NULL,releaseMdPics(&mdPics),"getThumbnailFromFile--: Wrong client data (currentN==0 for level=%d)",i);
        LOG0close_return(!currentAlbum,NULL,releaseMdPics(&mdPics),
                         "getThumbnailFromFile--: Wrong client data (currentAlbum==NULL for level=%d, id=%08X (levels: %u))",i,id->nameCRC[i],(unsigned)id->level);

        for(j=0;j<currentN;j++)
            if(currentAlbum[j].nameCRC==id->nameCRC[i])
                break;
        LOG0close_return(j>=currentN,NULL,releaseMdPics(&mdPics),"getThumbnailFromFile--: Wrong client data: element not found for level %d",i);
        index=j;

        //if here, the index is valid. Check permission
        currentEntry=&currentAlbum[index];
        if(!(currentEntry->flags&FlagIsAlbum) && isVideo){
            if(currentEntry->flags&FlagIsVideo)*isVideo=1;
            else *isVideo=0;
        }
        LOG0close_return(id->reqPerm && !((currentEntry->permissions>>(id->reqPerm-1))&1),NULL,releaseMdPics(&mdPics),
                         "getThumbnailFromFile--: WARNING: The client does NOT have permission to access this album/picture.");
        LOG0close_return(!(currentEntry->flags&KFlagHasAThumbnail),NULL,releaseMdPics(&mdPics),
                         "getThumbnailFromFile--: WARNING: Requested element has no thumbnail.");

        if(!i){
            //first, we need to copy one of the rootFolders
            ph_assert(!pathLen,NULL);
            cname=getNameFromAlbum(mdPics->rootAlbum,0);
            posPicturesFolder=pathLen=strlen(cname);
            memcpy(path,cname,pathLen);
            //add KFolder (starts with /)
            l=strlen(KPicturesFolder);
            memcpy(path+pathLen,KPicturesFolder,l);
            pathLen+=l;
            path[pathLen++]='/';
            posLastSlash=pathLen;
            path[pathLen]='\0'; //terminate the path string, this is needed for the LOG below
        }
        cname=getNameFromAlbum(currentAlbum,index);
        l=strlen(cname);
        memcpy(path+pathLen,cname,l);
        pathLen+=l;

        path[pathLen]='\0'; //terminate the path string, this is needed for the LOG below

        if(currentEntry->flags&FlagIsAlbum){
            currentAlbum=currentEntry->album;
            currentN=currentEntry->entriesIndex;
            if(path[pathLen-1]!='/')//could be from KPicturesFolder
                path[pathLen++]='/';
            posLastSlash=pathLen;
        } else {
            currentAlbum=NULL;
            //currentN=0;
        }
        path[pathLen]='\0'; //terminate the path string, this is needed for the LOG below
    }

    path[pathLen]='\0'; //terminate the path string, this is needed for the LOG below
    //LOG0("getThumbnailFromFile: Path here : %s, currentAlbum=%p",path,currentAlbum);

    //i=id->entries[id->level-1];
    //in the end, we need to reach a picture with a thumbnail. We KNOW we have one, at least
    while(currentAlbum){
        //if here, we are looking for a picture in the current album
        for(i=0;i<currentN;i++){
            currentEntry=&(currentAlbum[i]);
            if(!(currentEntry->flags&FlagIsAlbum) && currentEntry->flags&KFlagHasAThumbnail){
                //this is a picture with a thumbnail, in the current album
                cname=getNameFromAlbum(currentAlbum,i);
                l=strlen(cname);
                memcpy(path+pathLen,cname,l);//we will terminate the string at the end
                pathLen+=l;
                //lets break from here
                currentAlbum=NULL;
                index=i;
                break;
            }
        }//for

        if(!currentAlbum)break; //we found our picture

        LOG0("Checking among subalbums.");
        //if here, check among the subalbums
        for(i=0;i<currentN;i++){
            currentEntry=&(currentAlbum[i]);
            if(currentEntry->flags&FlagIsAlbum && currentEntry->flags&KFlagHasAThumbnail){
                //go inside this album
                cname=getNameFromAlbum(currentAlbum,i);
                l=strlen(cname);
                memcpy(path+pathLen,cname,l);
                pathLen+=l;
                path[pathLen++]='/';
                posLastSlash=pathLen;
                //lets break from here
                currentAlbum=currentEntry->album;
                currentN=currentEntry->entriesIndex;
                index=i;
                i=-1; //so that we do not trigger the assert below with a modified currentN
                break;
            }
        }//for
        LOG0close_return(i>=currentN,NULL,releaseMdPics(&mdPics),"getThumbnailFromFile--: Thumbnail not found");
        //ph_assert(i<currentN,NULL);//otherwise we were not able to find a thumbnail, altough the flags said there should be one
    }
    path[pathLen]='\0'; //terminate the path string
    //LOG0("getThumbnailFromFile: Path here2: %s, currentAlbum=%p",path,currentAlbum);

    //here we know if this is a video or a picture (or an album)
    if(!(currentEntry->flags&FlagIsAlbum) && isVideo){
        if(currentEntry->flags&FlagIsVideo)*isVideo=1;
        else *isVideo=0;
    }

    if(statusFlagsCheck(StatusFlagPictureScanThreadActive)){
        //it may be that the thumbnail for this album is not reay yet
        LOG0close_return(!currentEntry || currentEntry->flags&FlagIsAlbum || !(currentEntry->flags&KFlagHasAThumbnail),NULL,releaseMdPics(&mdPics),
                         "getThumbnailFromFile--: Thumbnail not available during picture scan");
    } else {
        ph_assert(currentEntry,NULL);
        ph_assert(!(currentEntry->flags&FlagIsAlbum),"%u",currentEntry->flags); //it should be a picture
        ph_assert(currentEntry->flags&KFlagHasAThumbnail,NULL); //...with a thumbnail
        ph_assert(path[pathLen-1]!='/',NULL);
    }



    //compute the eTag. At the same time, get our ID
    struct md_level_etagf currentEntryETag;
    currentEntryETag.nameCRC=currentEntry->nameCRC;
    currentEntryETag.captureTime=currentEntry->captureTime;
    currentEntryETag.sizeOrLatestCaptureTime=currentEntry->size;
    currentEntryETag.modificationTime=currentEntry->modificationTime;
    currentEntryETag.flags=pixelRatio;
    entryFlags=currentEntry->flags;
    releaseMdPics(&mdPics); //not needed after this point
    currentEntry=currentAlbum=NULL;//for safety

    //add FlagThumbnailETag_Scaled to flags -> attempting scaled thumbnail
    currentEntryETag.flags|=FlagThumbnailETag_Scaled;
    ph_assert(currentEntryETag.flags<256,NULL);
    if(eTag){
        r=snprintf(eTag,eTagSize,"%08X%08X%08X%08X%02X",currentEntryETag.nameCRC,(unsigned int)currentEntryETag.captureTime,
                   (unsigned int)currentEntryETag.sizeOrLatestCaptureTime,(unsigned int)currentEntryETag.modificationTime,(unsigned int)currentEntryETag.flags);
        ph_assert(r<eTagSize,NULL);
        LOG0return(reqETag && !strcmp(eTag,reqETag),(char*)reqETag,"getThumbnailFromFile--: same ETag (for scaled thumbnail)");
    }

    //if here, we can and should send a thumbnail
    char *b=NULL;
    char *savedPath=NULL;

    if(entryFlags&FlagHasScaledThumbnail){//first try to get the scaled thumbnail
        //first, replace the KPicturesFolder
        const int lp=strlen(KPicturesFolder),ld=strlen(KPhotostovisData);
        int n,searchByID=0;
        if(entryFlags&FlagHasExifThumbnail)
            savedPath=ph_strdup(path);//just in case getting the scaled thumbnail fails and we need to open the
        if(lp!=ld){
            memmove(path+posPicturesFolder+ld,path+posPicturesFolder+lp,posLastSlash-posPicturesFolder-lp+1); //+1: the \0 char
            pathLen=pathLen-lp+ld;
            posLastSlash=posLastSlash-lp+ld;
        }
        memcpy(path+posPicturesFolder,KPhotostovisData,ld);
        //replace the picture name with the thumbnails filename
        r=strlen(KThumbnailsFilename);
        memcpy(path+posLastSlash,KThumbnailsFilename,r+1);
        if(pixelRatio>1){
            ph_assert((path+posLastSlash)[r-5]=='1',NULL);
            (path+posLastSlash)[r-5]='2';
            //LOG0("Switching to pixelRatio=2");
        }
        //open the thumbnails filename
        FILE *f=fopen(path,"r");
        LOG0c(!f,"ERROR: could not open thumbnail file (%s) for this album. Error: %s",path,strerror(errno));

        struct md_thumbnails *thumbnailsElm=NULL;
        if(f){
            //read the thumbnails metadata
            struct md_thmbfile_header thmbfileHeader;
            r=fread(&thmbfileHeader,sizeof(struct md_thmbfile_header),1,f);

            if(r!=1 || thmbfileHeader.version!=CURRENT_THUMBNAILS_FILE_VERSION){
                LOG0("ERROR reading thumbnail from file: header reading error.");
                fclose(f);
                f=NULL;
            }
            else {
                n=thmbfileHeader.nrEntries;
                LOG0c_do(n!=currentN,searchByID=1,"WARNING: thumbnail file: mismatched number of elements (%d vs %d). Will try matching by ID.",n,currentN);
            }
        }

        if(f){
            thumbnailsElm=ph_malloc((n+1)*sizeof(struct md_thumbnails));
            r=fread(thumbnailsElm,(n+1)*sizeof(struct md_thumbnails),1,f);
            LOG0c_do(r!=1,fclose(f);f=NULL,"ERROR reading thumbnail from file: reading elements failed."); //we will free thumbnailsElm later
        }

        if(f){
            struct md_thumbnails const * __restrict t=&thumbnailsElm[index];
            if(!searchByID){
                //does the index match?
                if(currentEntryETag.nameCRC!=t->etag.nameCRC)
                    searchByID=1;
            }
            if(searchByID){
                //we need to search for the right index
                for(i=0;i<n;i++){
                    t=&thumbnailsElm[i];
                    if(currentEntryETag.nameCRC!=t->etag.nameCRC){
                        index=i;
                        break;
                    }
                }
                if(i>=n){
                    //we did not find an index. close the file, will try exif thumbnail
                    fclose(f);f=NULL;
                    index=-1;
                    t=NULL;
                }
            }

            if(index>=0){
                ph_assert(t,NULL);
                *bufLen=l=thumbnailsElm[index+1].offset-t->offset;
                //allocate and fill in the buffer
                if(l>0){
                    b=(char*)ph_malloc(l);
                    //read the thumbnail data into the buffer
                    fseek(f,t->offset,SEEK_SET);
                    r=fread(b,l,1,f);
                    LOG0c_do(r!=1,fclose(f);f=NULL;free(b);b=NULL;*bufLen=0,"ERROR reading thumbnail from file: reading thumbnail failed."); //we will free thumbnailsElm later
                } else {
                    LOG0("WARNING: thumbnail has zero size. Ignoring.");
                    fclose(f);f=NULL;
                }
            }//if(index>=0)..
        }//if(f)...

        //not needed after this point, we free it here
        free(thumbnailsElm);

        if(f){
            ph_assert(b,NULL);
            ph_assert(*bufLen>0,NULL);
            fclose(f);
            free(savedPath);
            //LOG0("getThumbnailFromFile--: returning scaled thumbnail");
            return b;
        }

        //if here, we copy the saved path to path
        if(savedPath && (entryFlags&FlagHasExifThumbnail)){
            strcpy(path,savedPath);
            free(savedPath);
        } else
            LOG0return(1,NULL,"getThumbnailFromFile--: Scaled thumbnail not found and no path found (for EXIF thumbnail)");
    }

    LOG0return(!(entryFlags&FlagHasExifThumbnail),NULL,"getThumbnailFromFile--: entry does NOT have EXIF thumbnail");

    //if we are here, we should have EXIF thumbnail

    //remove the FlagThumbnailETag_Scaled from flags -> attempting EXIF thumbnail
    currentEntryETag.flags&=~(uint16_t)FlagThumbnailETag_Scaled;
    //also remove the pixel ratio completely because for EXIF thumbnail it does not matter
    currentEntryETag.flags&=0xF0;
    ph_assert(currentEntryETag.flags<256,NULL);
    ph_assert(currentEntryETag.flags==0,NULL); //this should be valid as long as we do not have other flags
    if(eTag){
        r=snprintf(eTag,eTagSize,"%08X%08X%08X%08X%02X",currentEntryETag.nameCRC,(unsigned int)currentEntryETag.captureTime,
                   (unsigned int)currentEntryETag.sizeOrLatestCaptureTime,(unsigned int)currentEntryETag.modificationTime,(unsigned int)currentEntryETag.flags);
        ph_assert(r<eTagSize,NULL);
        LOG0return(reqETag && !strcmp(eTag,reqETag),(char*)reqETag,"getThumbnailFromFile--: ETag is the same (for EXIF thumbnail)");
    }

    //check if the image file exists
    struct stat st;
    r=stat(path,&st);
    LOG0return(r,NULL,"getThumbnailFromFile--: WARNING: stat did not succeeded for this picture file (%s). Error: %s",path,strerror(errno));

    //open the file
    ExifData *ed=exif_data_new_from_file(path);
    LOG0return(!ed || !ed->data || !ed->size,NULL,"getThumbnailFromFile--: WARNING: We were unable to find a thumbnail in this file (%s). Strange ...",path);

    //if here, the thumbnail exists. Allocate a length for the buffer
    *bufLen=ed->size;
    LOG2("We have an EXIF thumbnail, size: %d",(int)*bufLen);

    //allocate and fill in the buffer
    b=(char*)ph_malloc(*bufLen);

    //read the exif data into the buffer
    memcpy(b,ed->data,ed->size);
    //free the exif data
    exif_data_unref(ed);
    LOG0("getThumbnailFromFile--: returning EXIF thumbnil");
    return b;
}

/*
static void cacheThisThumbnail(struct md_entry_id * currentId, struct md_thmbstrip_entry const * const tse, const int pixelRatio, const int keepInCache){
    currentId->nameCRC[currentId->level]=tse->nameCRC;
    currentId->level++;

    if(tse->flags&FlagIsAlbum){
        int i;
        for(i=0;i<tse->nrEntries;i++){
            cacheThisThumbnail(currentId,&(tse->entries[i]),pixelRatio,keepInCache);
        }
    } else {
        //picture
        char url[1024];
        index2string(currentId,url,1024);
        LOG0("Caching thumbnail with ID=/t%s (pr=%d, level: %d)",url,pixelRatio,currentId->level);
    }

    currentId->level--;
}

static struct ps_thumb_cache * getStructureForId(struct md_entry_id const * const id, int currentLevel, struct ps_thumb_cache *currentAlbum){
    int i;


    return NULL;
}*/

struct ps_thumb_cache *addOrGetThmbEntry(struct md_album_entry const *e, const int index, struct md_entry_id *id, const int pixelRatio, struct ps_thumb_cache *t){
    if(!e)return NULL; //this is possible
    ph_assert(index>=0 && index<e->entriesIndex,NULL);
    ph_assert(e->flags&FlagIsAlbum,NULL);
    ph_assert(t->nrEntries!=0xFFFF,NULL);
    struct md_album_entry *c=&e->album[index];
    uint32_t nameCRC=c->nameCRC;
    int i,place=0;

    //is our nameCRC in the album already?
    if(c->flags&FlagIsAlbum)
        for(i=0;i<t->nrEntries;i++)
            if(t->album[i].nameCRC==nameCRC)
                return &t->album[i];
    //if here, our nameCRC is NOT in the album already

    //find the place where we should put it
    if(t->nrEntries>0)
        for(i=0;i<index;i++)
            if(e->album[i].nameCRC==t->album[place].nameCRC){
                place++;
                if(place>=t->nrEntries)
                    break; //means we put it at the end
            }

    //if here, our index is NOT in the album already
    ph_assert(place<=t->nrEntries,"place=%d nrEntries: %u",place,(unsigned)t->nrEntries);

    size_t cntBufLen=0;
    char *cntBuf=NULL;
    if(!(c->flags&FlagIsAlbum)){
        cntBuf=getThumbnailFromFile(id,pixelRatio,&cntBufLen,NULL,0,NULL,NULL);
        ph_assert(cntBuf,NULL);
        ph_assert(cntBufLen,NULL);
    }

    //make space for our entry. Reallocate the structure
    t->album=(struct ps_thumb_cache*)ph_realloc(t->album,t->allocatedSize+sizeof(struct ps_thumb_cache)+cntBufLen);
    memmove(t->album+place+1,t->album+place,t->allocatedSize-place*sizeof(struct ps_thumb_cache));
    t->nrEntries++;
    t->allocatedSize+=sizeof(struct ps_thumb_cache)+cntBufLen;
    cacheCurrentSize+=sizeof(struct ps_thumb_cache)+cntBufLen;
    struct ps_thumb_cache* tNew=&t->album[place];
    //give values
    tNew->flags=c->flags;
    tNew->nameCRC=c->nameCRC;

    if(cntBuf){
#ifndef NDEBUG
        tNew->zzz=*(int32_t*)cntBuf;
#endif
        ph_assert(!(c->flags&FlagIsAlbum),NULL);
        ph_assert(cntBufLen>0,NULL);
        memcpy((char*)t->album+t->allocatedSize-cntBufLen,cntBuf,cntBufLen);
        free(cntBuf);
        cntBuf=NULL;
        cacheNrThumbnails++;

        //LOG0("FlagHasCachedThumbnail added!!!!");
        c->flags|=FlagHasCachedThumbnail;
        tNew->flags|=FlagHasCachedThumbnail;

        tNew->size=cntBufLen;
        tNew->offset=t->allocatedSize-cntBufLen-(t->nrEntries*sizeof(struct ps_thumb_cache));
        tNew->nrEntries=0xFFFF;
        tNew->captureTime=c->captureTime;
        tNew->imageSize=c->size;
        tNew->modificationTime=c->modificationTime;
    } else {
        ph_assert(c->flags&FlagIsAlbum,NULL);
        tNew->nrEntries=0;
        tNew->album=NULL;
        tNew->allocatedSize=0;
        tNew->totalNrThumbnails=0;
    }


    //verify (in debug version)
#ifndef NDEBUG
    //LOG0("DEBUG: Verifying ...");
    for(i=0;i<t->nrEntries;i++)
        if(t->album[i].nrEntries==0xFFFF)
            ph_assert(t->album[i].zzz == *(int32_t*)((char*)t->album+t->nrEntries*sizeof(struct ps_thumb_cache)+t->album[i].offset),NULL);
    //LOG0("DEBUG: Verification done.");
#endif
    return tNew;
}

static int addThumbnailByIndex(struct md_album_entry const *e, struct md_entry_id *id, struct mdh_album_data_by_permission const *adbp, int index,
                               struct ps_thumb_cache *tPR1, struct ps_thumb_cache *tPR2){
    int i,current=0;

    //LOG0("addThumbnailByIndex: looking for element %d out of %d",index+1,adbp->nrTotalPictrs+adbp->nrTotalVideos);
    ph_assert(index>=0 && index<(adbp->nrTotalPictrs+adbp->nrTotalVideos),"index: %d, nrPics: %u, nrVids: %u, total: %u",
              index,adbp->nrTotalPictrs,adbp->nrTotalVideos,adbp->nrTotalPictrs+adbp->nrTotalVideos);

    for(i=0;i<e->entriesIndex;i++){
        struct md_album_entry *c=&e->album[i];
        ph_assert(current<=index,NULL);
        if(c->flags&FlagIsAlbum){
            //LOG0("Element %d is an album.",i);
            struct mdh_album_data_by_permission *adbpCurrent=getAlbumDataByPermission(c);
            if(current+adbpCurrent->nrTotalPictrs+adbpCurrent->nrTotalVideos<=index){
                //the searched thumbnail is not in this sub-album
                current+=adbpCurrent->nrTotalPictrs+adbpCurrent->nrTotalVideos;
                continue;
            }

            //if we are here, the searched thumbnail can be found in this subalbum
            id->nameCRC[id->level]=c->nameCRC;
            id->level++;
            struct ps_thumb_cache *t1=addOrGetThmbEntry(e,i,id,1,tPR1);
            struct ps_thumb_cache *t2=addOrGetThmbEntry(e,i,id,2,tPR2);
            return addThumbnailByIndex(c,id,adbpCurrent,index-current,t1,t2);
        } else {
            //LOG0("Element %d is a picture or a video.",i);
            if(current!=index){
                //it is not this one!
                current++;
                continue;
            }

            //we found it! Is it suitable?
            if(!(c->flags&(FlagHasScaledThumbnail+FlagHasExifThumbnail))){
                LOG0("Element %d is not suitable (does not have a thumbnail)",i);
                return 0; //not suitable because it does not have a thumbnail
            }

            //if we are here, this is the one and it is suitable!
            id->nameCRC[id->level]=c->nameCRC;
            id->level++;
            addOrGetThmbEntry(e,i,id,1,tPR1);
            addOrGetThmbEntry(e,i,id,2,tPR2);

            return 1;
        }
    }
    ph_assert(0,NULL); //we should not get here
    return 0;
}


static int addThumbnailsToCache(struct md_album_entry const *e, struct md_entry_id *id, const int reqNrThumbnails,
                                struct ps_thumb_cache *tPR1, struct ps_thumb_cache *tPR2){
    int i,j=-1,r;
    ph_assert(e->flags&FlagIsAlbum,NULL);
    struct mdh_album_data_by_permission const * const adbp=getAlbumDataByPermission(e);
    int nrThumbnailsToGet=reqNrThumbnails,availableNrThumbnails=adbp->nrTotalPictrs+adbp->nrTotalVideos;
    uint32_t *thumbnailIndexes;

    //TODO: permission index not used!

    //LOG0("Number of all pictures+videos in this album: %d+%d",adbp->nrTotalPictrs,adbp->nrTotalVideos);
    if(availableNrThumbnails<nrThumbnailsToGet)nrThumbnailsToGet=availableNrThumbnails; //cannot get more pictures than we have in the album
    thumbnailIndexes=(uint32_t*)malloc(nrThumbnailsToGet*sizeof(uint32_t));

    //get the pictures
    for(i=0;i<nrThumbnailsToGet;i++){
        //generate a random number
        r=RAND_bytes((unsigned char *)(thumbnailIndexes+i),sizeof(uint32_t));
        thumbnailIndexes[i]%=availableNrThumbnails;
        //is it a duplicate?
        for(j=0;j<i;j++)
            if(thumbnailIndexes[i]==thumbnailIndexes[j])
                break;//duplicate found, retry
        if(j<i){
            i--;
            continue;
        }

        //get this thumbnail
        //LOG0("### Thumbnail %u from %d",thumbnailIndexes[i],availableNrThumbnails);
        id->level=1;
        r=addThumbnailByIndex(e,id,adbp,thumbnailIndexes[i],tPR1,tPR2);
        if(r==0){
            LOG0("ERROR getting thumbnail with index %u. Skipping.",thumbnailIndexes[i]);
            if(availableNrThumbnails==nrThumbnailsToGet)
                nrThumbnailsToGet--;
            availableNrThumbnails--;
            //TODO: make a list with unsuitable elements (that failed)
            //try to get another thumbnail
            i--;
            continue;
        }
        ph_assert(r==1,NULL);
    }
    //if here we got all the thumbnails!
    free(thumbnailIndexes);
    LOG0("Retrieved thumbnails: %d (requested: %d. Available: %d)",nrThumbnailsToGet,reqNrThumbnails,availableNrThumbnails);

    return nrThumbnailsToGet;
}

static void freeThumbCacheStructure(struct ps_thumb_cache *t){
    int i;
    ph_assert(t->nrEntries!=0xFFFF,NULL);
    for(i=0;i<t->nrEntries;i++)
        if(t->album[i].nrEntries!=0xFFFF)
            freeThumbCacheStructure(&t->album[i]);
        else {
            if(t->album[i].size>0)
                cacheNrThumbnails--;
        }
    free(t->album);
    cacheCurrentSize-=t->allocatedSize;
    t->album=0;
    t->allocatedSize=0;
    t->nrEntries=0;
    t->totalNrThumbnails=0;
}

static int computeTotalNrThumbnails(struct ps_thumb_cache *t){
    int i;
    ph_assert(t->nrEntries!=0xFFFF,NULL);
    for(i=0;i<t->nrEntries;i++)
        if(t->album[i].nrEntries!=0xFFFF)
            t->totalNrThumbnails+=computeTotalNrThumbnails(&t->album[i]);
        else {
            if(t->album[i].size>0)
                t->totalNrThumbnails++;
        }
    return t->totalNrThumbnails;
}


static struct md_thmbstrip_entry *getThmbEntryFromCache(struct md_thmbstrip_entry *tse, struct ps_thumb_cache const *t, const int index){
    ph_assert(t->nrEntries!=0xFFFF,NULL);
    ph_assert(index>=0 && index<t->nrEntries,NULL);
    struct ps_thumb_cache *c=&t->album[index];
    uint32_t nameCRC=c->nameCRC;
    int i,place=0;

    //LOG0("getThmbEntry for %08X",nameCRC);
    //is our nameCRC in the album already?
    if(c->nrEntries!=0xFFFF)
        for(i=0;i<tse->nrEntries;i++)
            if(tse->entries[i].nameCRC==nameCRC)
                return &tse->entries[i];
    //if here, our nameCRC is NOT in the album already

    //find the place where we should put it
    if(tse->nrEntries>0)
        for(i=0;i<index;i++)
            if(t->album[i].nameCRC==tse->entries[place].nameCRC){
                place++;
                if(place>=tse->nrEntries)
                    break; //means we put it at the end
            }
    //we found the place
    ph_assert(place<=tse->nrEntries,"place=%d nrEntries: %u",place,(unsigned)tse->nrEntries);

    tse->nrEntries++;
    tse->entries=(struct md_thmbstrip_entry *)ph_realloc(tse->entries,tse->nrEntries*sizeof(struct md_thmbstrip_entry));
    //LOG0("getThmbEntry: place=%d out of %d elements. Moving %d elements.",place,tse->nrEntries,tse->nrEntries-1-place);
    memmove(tse->entries+place+1,tse->entries+place,(tse->nrEntries-1-place)*sizeof(struct md_thmbstrip_entry));
    //set it up!
    struct md_thmbstrip_entry *currentTse=&tse->entries[place];
    if(c->nrEntries!=0xFFFF)currentTse->flags=FlagIsAlbum;
    else currentTse->flags=c->flags;
    currentTse->clientIndex=index;
    currentTse->nrTotalThumbnails=0; //??? what to put here?
    currentTse->nrEntries=0;
    currentTse->currentIndex=0; //??? what to put here?
    currentTse->nameCRC=nameCRC;
    currentTse->entries=NULL;
    return currentTse;
}

static int getThumbnailByIndexFromCache(struct ps_thumb_cache const *t, int index, struct md_thmbstrip_entry *tse){
    int i,current=0;
    ph_assert(index>=0 && index<t->totalNrThumbnails,NULL);
    //LOG0("getThumbnailByIndexFromCache: looking for element %d out of %d",index,adbp->nrTotalPictrs+adbp->nrTotalVideos);

    for(i=0;i<t->nrEntries;i++){
        struct ps_thumb_cache const *c=&t->album[i];
        ph_assert(current<=index,NULL);
        if(c->nrEntries!=0xFFFF){
            if(current+c->totalNrThumbnails<=index){
                //the searched thumbnail is not in this sub-album
                current+=c->totalNrThumbnails;
                continue;
            }

            //if we are here, the searched thumbnail can be found in this subalbum
            return getThumbnailByIndexFromCache(c,index-current,getThmbEntryFromCache(tse,t,i));
        } else {
            //LOG0("Element %d is a picture. Flags: %X",i,(unsigned)c->flags);
            if(current!=index){
                //it is not this one!
                current++;
                continue;
            }

            //we found it! Is it suitable?
            if(!(c->flags&(FlagHasScaledThumbnail+FlagHasExifThumbnail))){
                LOG0("Element %d is not suitable (does not have a thumbnail). Flags: %X",i,(unsigned)c->flags);
                return 0; //not suitable because it does not have a thumbnail
            }

            //if we are here, this is the one and it is suitable!
            getThmbEntryFromCache(tse,t,i);

            return 1;
        }
    }
    ph_assert(0,NULL); //we should not get here
    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int thmbManagerInit(){
    buildingCache=1;
    LOG0("thmbManagerInit++");
    if(thumbsCachePR1 || thumbsCachePR2)
        thmbManagerClose();
    ph_assert(thumbsCachePR1==NULL,NULL);
    ph_assert(thumbsCachePR2==NULL,NULL);
    ph_assert(cacheCurrentSize==0,NULL);
    ph_assert(cacheNrThumbnails==0,NULL);

    cacheMaxSize=THUMBNAILS_CACHE_MAX_MB<<20;

    struct ps_md_pics const * const mdPics=getMdPicsRd();
    int i,n=mdPics->rootAlbum->entriesIndex;
    struct md_album_entry const *album=mdPics->rootAlbum->album,*e;
    struct md_entry_id id;
    LOG0close_return(!n,0,releaseMdPics(&mdPics),"thmbManagerInit: no elements in main album.");

    thumbsCachePR1=(struct ps_thumb_cache*)ph_malloc(sizeof(struct ps_thumb_cache));
    memset(thumbsCachePR1,0,sizeof(struct ps_thumb_cache));
    thumbsCachePR2=(struct ps_thumb_cache*)ph_malloc(sizeof(struct ps_thumb_cache));
    memset(thumbsCachePR2,0,sizeof(struct ps_thumb_cache));




    LOG0("n=%d",n);
    unsigned int adjustedEntriesPerAlbum=THUMBNAILS_CACHE_NR_ENTRIES_PER_ALBUM;
    //can we increase this value?
    if(adjustedEntriesPerAlbum*100*n<(cacheMaxSize>>10)){
        //yes, we can
        adjustedEntriesPerAlbum=THUMBNAILS_CACHE_MAX_MB*10/n;
        LOG0("INFO: nr of thumbnails entries per album increased from %d to %d",THUMBNAILS_CACHE_NR_ENTRIES_PER_ALBUM,adjustedEntriesPerAlbum);
    }

    //fill in the root album
    thumbsCachePR1->flags=thumbsCachePR2->flags=FlagIsAlbum;
    thumbsCachePR1->nrEntries=thumbsCachePR2->nrEntries=n;
    thumbsCachePR1->nameCRC=thumbsCachePR2->nameCRC=0;

    thumbsCachePR1->album=(struct ps_thumb_cache*)ph_malloc(n*sizeof(struct ps_thumb_cache));
    thumbsCachePR2->album=(struct ps_thumb_cache*)ph_malloc(n*sizeof(struct ps_thumb_cache));
    thumbsCachePR1->allocatedSize=thumbsCachePR2->allocatedSize=n*sizeof(struct ps_thumb_cache);
    struct ps_thumb_cache *ta1,*ta2;
    cacheCurrentSize=(n+n)*sizeof(struct ps_thumb_cache);

    id.level=1;
    id.reqPerm=0; //TODO: this should be fixed at some point
    for(i=0;i<n;i++){
        //TODO: check for max cache limit. Implement after retrieval
        //LOG0c_break(cacheCurrentSize>cacheMaxSize,"WARNING: Max cache limit reached:");

        e=&album[i];
        ta1=&thumbsCachePR1->album[i];
        ta2=&thumbsCachePR2->album[i];
        ta1->flags=ta2->flags=e->flags;
        ta1->nameCRC=ta2->nameCRC=id.nameCRC[0]=e->nameCRC;
        id.level=1;

        if(e->flags&FlagIsAlbum){
            //LOG0("Entry %2d/%d is an album  (%s)",i+1,n,getNameFromAlbum(album,i));
            ta1->nrEntries=ta2->nrEntries=0;
            ta1->album=ta2->album=NULL;
            ta1->allocatedSize=ta2->allocatedSize=0;
            ta1->totalNrThumbnails=ta2->totalNrThumbnails=0;
            addThumbnailsToCache(e,&id,adjustedEntriesPerAlbum,ta1,ta2);
        } else {
            LOG0("Entry %2d/%d is a PICTURE (%s)",i+1,n,getNameFromAlbum(album,i));
            ta1->nrEntries=ta2->nrEntries=0xFFFF;
            ta1->captureTime=ta2->captureTime=e->captureTime;
            ta1->imageSize=ta2->imageSize=e->size;
            ta1->modificationTime=ta2->modificationTime=e->modificationTime;

            size_t cntBufLen;
            //for pixelRatio==1
            char *cntBuf=getThumbnailFromFile(&id,1,&cntBufLen,NULL,0,NULL,NULL);
            if(cntBuf){
                //fill in this entry. Reallocate the structure
                thumbsCachePR1->allocatedSize+=cntBufLen;
                thumbsCachePR1->album=(struct ps_thumb_cache*)ph_realloc(thumbsCachePR1->album,thumbsCachePR1->allocatedSize);
                cacheCurrentSize+=cntBufLen;
                cacheNrThumbnails++;

                ta1=&thumbsCachePR1->album[i]; //it changed

                memcpy((char*)thumbsCachePR1->album+thumbsCachePR1->allocatedSize-cntBufLen,cntBuf,cntBufLen);
                free(cntBuf);

                ta1->offset=thumbsCachePR1->allocatedSize-cntBufLen-thumbsCachePR1->nrEntries*sizeof(struct ps_thumb_cache);
                ta1->size=cntBufLen;
                ph_assert(cntBufLen>0,NULL);
            } else {
                //this is an entry without thumbnail
                ta1->offset=ta1->size=0;
            }

            //for pixelRatio==2
            cntBuf=getThumbnailFromFile(&id,2,&cntBufLen,NULL,0,NULL,NULL);
            if(cntBuf){
                //fill in this entry. Reallocate the structure
                thumbsCachePR2->allocatedSize+=cntBufLen;
                thumbsCachePR2->album=(struct ps_thumb_cache*)ph_realloc(thumbsCachePR2->album,thumbsCachePR2->allocatedSize);
                cacheCurrentSize+=cntBufLen;
                cacheNrThumbnails++;

                ta2=&thumbsCachePR2->album[i]; //it changed

                memcpy((char*)thumbsCachePR2->album+thumbsCachePR2->allocatedSize-cntBufLen,cntBuf,cntBufLen);
                free(cntBuf);


                ta2->offset=thumbsCachePR2->allocatedSize-cntBufLen-thumbsCachePR2->nrEntries*sizeof(struct ps_thumb_cache);
                ta2->size=cntBufLen;
                ph_assert(cntBufLen>0,NULL);
            } else {
                //this is an entry without thumbnail
                ta2->offset=ta2->size=0;
            }
        }

        if(cacheCurrentSize>cacheMaxSize){
            //breaking here
            LOG0("WARNING: thumbnails cache max size reached: side=%dMB, max=%dMB",cacheCurrentSize>>20,cacheMaxSize>>20);
            thumbsCachePR1->nrEntries=thumbsCachePR2->nrEntries=i+1;
            break;
        }
    }//for
    releaseMdPics(&mdPics);
    computeTotalNrThumbnails(thumbsCachePR1);
    computeTotalNrThumbnails(thumbsCachePR2);
    ph_assert(cacheNrThumbnails==thumbsCachePR1->totalNrThumbnails+thumbsCachePR2->totalNrThumbnails,
              "%u==%u+%u",cacheNrThumbnails,thumbsCachePR1->totalNrThumbnails,thumbsCachePR2->totalNrThumbnails);
    LOG0("Thumbnail manager: %u thumbnails loaded, total of %uMB (%u bytes).",cacheNrThumbnails,cacheCurrentSize>>20,cacheCurrentSize);
    LOG0c(THUMBNAILS_CACHE_NR_ENTRIES_PER_ALBUM!=adjustedEntriesPerAlbum,
          "Thumbnail manager: nr of thumbnails entries per album increased from %d to %d",THUMBNAILS_CACHE_NR_ENTRIES_PER_ALBUM,adjustedEntriesPerAlbum);
    LOG_FLUSH;

    buildingCache=0;
    return 0;
}


int thmbManagerClose(){
    if(thumbsCachePR1){
        freeThumbCacheStructure(thumbsCachePR1);
        free(thumbsCachePR1);
        thumbsCachePR1=NULL;
    }

    if(thumbsCachePR2){
        freeThumbCacheStructure(thumbsCachePR2);
        free(thumbsCachePR2);
        thumbsCachePR2=NULL;
    }

    ph_assert(!cacheNrThumbnails,"cache nr thumbnails: %u (%u)",cacheNrThumbnails,-cacheNrThumbnails);

    ph_assert(!cacheCurrentSize,"cache size: %u %u %zu %zu",cacheCurrentSize,-cacheCurrentSize,
              cacheCurrentSize/sizeof(struct ps_thumb_cache),-cacheCurrentSize/sizeof(struct ps_thumb_cache));

    return 0;
}


int getThumbnailUrlFromCache(const uint32_t nameCRC, struct md_thmbstrip_entry *tse, int permissionIndex, const int reqNrThumbnails){
    int i,j=-1,r;
    ph_assert(tse->flags&FlagIsAlbum,NULL);
    //LOG0return(buildingCache || !thumbsCachePR1 || !thumbsCachePR2,-1,"INFO: thumbnail cache not ready yet.");
    ph_assert(!buildingCache,NULL);
    ph_assert(thumbsCachePR1,NULL);
    ph_assert(thumbsCachePR2,NULL);
    ph_assert(thumbsCachePR1->nrEntries==thumbsCachePR2->nrEntries,NULL);

    //find the album element
    struct ps_thumb_cache const *t=NULL;
    for(i=0;i<thumbsCachePR1->nrEntries;i++)
        if(nameCRC==thumbsCachePR1->album[i].nameCRC){
            t=&thumbsCachePR1->album[i];
            break;
        }
    LOG0return(i>=thumbsCachePR1->nrEntries,-1,"INFO: requested album (nameCRC=%08X) not cached.",nameCRC);

    int nrThumbnailsToGet=reqNrThumbnails,availableNrThumbnails=t->totalNrThumbnails;
    int *thumbnailIndexes;

    //TODO: permission index not used!

    //LOG0("Number of all pictures in this album: %d",adbp->nrTotalPictrs+adbp->nrTotalVideos);
    if(availableNrThumbnails<nrThumbnailsToGet)nrThumbnailsToGet=availableNrThumbnails; //cannot get more pictures than we have in the album
    thumbnailIndexes=(int*)malloc(nrThumbnailsToGet*sizeof(int));

    //get the pictures
    for(i=0;i<nrThumbnailsToGet;i++){
        //generate a random number
        r=RAND_bytes((unsigned char *)(thumbnailIndexes+i),4);
        thumbnailIndexes[i]%=t->totalNrThumbnails;
        //is it a duplicate?
        for(j=0;j<i;j++)
            if(thumbnailIndexes[i]==thumbnailIndexes[j])
                break;//duplicate found, retry
        if(j<i){
            i--;
            continue;
        }

        //get this thumbnail
        r=getThumbnailByIndexFromCache(t,thumbnailIndexes[i],tse);
        if(r==0){
            LOG0("ERROR getting thumbnail with index %d. Skipping.",thumbnailIndexes[i]);
            if(availableNrThumbnails==nrThumbnailsToGet)
                nrThumbnailsToGet--;
            availableNrThumbnails--;
            //TODO: make a list with unsuitable elements (that failed)
            //try to get another thumbnail
            i--;
            continue;
        }
        ph_assert(r==1,NULL);
    }
    //if here we got all the thumbnails!
    free(thumbnailIndexes);
    //LOG0("Retrieved thumbnails: %d (requested: %d. Available: %d)",nrThumbnailsToGet,reqNrThumbnails,availableNrThumbnails);

    return nrThumbnailsToGet;
}


char *getThumbnailFromCache(struct md_entry_id const * const id, const int pixelRatio, size_t *bufLen, int *returnedDataShouldBeFreed,
                            char *eTag, int eTagSize, const char *reqETag, int * const isVideo){
    //look for the thumbnail in our cache
    ph_assert(id,NULL);
    LOG0return(!id->level,NULL,"getThumbnailFromCache: WARNING: Cannot get thumbnail for root level");
    if(buildingCache || !thumbsCachePR1 || !thumbsCachePR2){
        //cache not ready yet
        return getThumbnailFromFile(id,pixelRatio,bufLen,eTag,eTagSize,reqETag,isVideo);
    }
    struct ps_thumb_cache const *t=(pixelRatio==1?thumbsCachePR1:thumbsCachePR2);
    struct ps_thumb_cache const *tAlbum=NULL;
    uint32_t currentNameCRC;
    int i,j,r;

    LOG0("TTTTTTTT getThumbnailFromCache: level: %d",(int)id->level);

    for(i=0;i<id->level;i++){
        LOG0return(t->nrEntries==0xFFFF,NULL,"TTTTTTTT getThumbnailFromCache: WARNING: Invalid thumbnail URL!");

        ph_assert(t->nrEntries!=0xFFFF,NULL);
        currentNameCRC=id->nameCRC[i];

        r=0; //not found!
        for(j=0;j<t->nrEntries;j++)
            if(currentNameCRC==t->album[j].nameCRC){
                //we found the element at this level
                tAlbum=t;
                t=&t->album[j];
                r=1; //found
                break;
            }

        if(r==0){
            char tUrl[100];
            index2string(id,tUrl,100);
            LOG0("TTTTTTTT getThumbnailFromCache: Thumbnail NOT FOUND in cache: %s",tUrl);
            //this means that we have not found this thumbnail in cache
            *returnedDataShouldBeFreed=1;
            return getThumbnailFromFile(id,pixelRatio,bufLen,eTag,eTagSize,reqETag,isVideo);
        }
    }//for

    //if here, we should have found the thumbnail
    LOG0return(t->nrEntries!=0xFFFF,NULL,"TTTTTTTT getThumbnailFromCache: WARNING: Invalid thumbnail URL!");

    //LOG0("TTTTTTTT getThumbnailFromCache: nrEntries: %X, size: %u, nameCRC: %08X",(unsigned)t->nrEntries,(unsigned)t->size,(unsigned)t->nameCRC);

    //ETag stuff
    if(eTag){
        r=snprintf(eTag,eTagSize,"%08X%08X%08X%08X%02X",t->nameCRC,(unsigned int)t->captureTime,
                   (unsigned int)t->imageSize,(unsigned int)t->modificationTime,(unsigned int)(pixelRatio==1?1:2));
        ph_assert(r<eTagSize,NULL);
        LOG0return(reqETag && !strcmp(eTag,reqETag),(char*)reqETag,"getThumbnailFromCache--: same ETag (for scaled thumbnail)");
    }

    //if here, we should return the thumbnail data
    ph_assert(tAlbum,NULL);
    *bufLen=t->size;
    *returnedDataShouldBeFreed=0;


#ifndef NDEBUG
    //verify that the data we return is the same as the data from the file
    LOG0("DEBUG: verifying thumbnail data.");
    size_t fileLen=0;
    char *fileThumbnail=getThumbnailFromFile(id,pixelRatio,&fileLen,NULL,0,NULL,NULL);
    ph_assert(t->size==fileLen,"%d==%d",(int)t->size,(int)fileLen);
    int zz=memcmp((char*)tAlbum->album+tAlbum->nrEntries*sizeof(struct ps_thumb_cache)+t->offset,fileThumbnail,fileLen);
    ph_assert(!zz,NULL);
    LOG0("DEBUG: thumbnail data verification done!");
#endif

    return (char*)tAlbum->album+tAlbum->nrEntries*sizeof(struct ps_thumb_cache)+t->offset;
}
