#ifndef LOG_H
#define LOG_H

#define MAX_LOG_MESSAGE_SIZE 1024
#define MAX_LOG_BUFFER_SIZE 1048576
#define MAX_LOG_SIZE_SEND2SERVER 10240

//log commands: should be smaller than 0xFFFF but larger than MAX_LOG_MESSAGE_SIZE
#define LOG_CMD_FLUSH         0xFFFFFFF0
#define LOG_CMD_SEND2SERVER   0xFFFFFFF1
#define LOG_CMD_EXIT          0xFFFFFFFE
#define LOG_CMD_EXIT_AND_SEND 0xFFFFFFFF

enum log_tags {
    //we can define 14+2 tags
    LOG_PHONET=1,
    LOG_SCAN=2,
    LOG_SIGNAL=4,
    //
    LOG_WARN=0x4000,
    LOG_ERROR=0x8000
};

//daemon log functions
struct ph_log_data {
    int srvSocket;
    char *rootPath;
};

extern void readjustLogStartTime();
extern void* logServerThread(void *data);



//extern unsigned int printlogt(const uint16_t tags, const char *fmt, ...);
extern unsigned int printlog(const char *fmt, ...) __attribute__ ((format (printf, 1, 2)));
unsigned int printlogbin(const char *ptr, int ptrLen, int printTxt);
extern void ph_assertion_failed(const char *filename, const int line, const char *fmt, ...) __attribute__ ((format (printf, 3, 4)));
extern unsigned int getRelativeTimeMs();
extern void flushlog();
extern void sendlog();
extern void ph_malloc_info();
extern void closelog(int sendToServer);

//extern void openLogfile(const char * const logPath);
//extern void checkAndSplitLogfile();
//extern void freeLogResources();
extern void resetLog(int s);

//extern char* getLogFile(size_t *bufLen);

#define ph_assert(c,txt, ...) if(!(c))ph_assertion_failed(__FILE__,__LINE__,txt,##__VA_ARGS__)

#ifdef NDEBUG
#define ph_assert_debug(c,txt, ...)
#else
#define ph_assert_debug(c,txt, ...) if(!(c))ph_assertion_failed(__FILE__,__LINE__,txt,##__VA_ARGS__)
#endif

#ifndef LOG_VERBOSITY
#define LOG_VERBOSITY 1 //0,1,2
#endif

//#define LOG0t(t,x, ...) printlogt(t,x,##__VA_ARGS__)
#define LOG0(x, ...) printlog(x,##__VA_ARGS__)
#define LOG0bin(x,y,z) printlogbin(x,y,z)

#if LOG_VERBOSITY>=1
#define LOG1(x, ...) printlog(x,##__VA_ARGS__)
#else
#define LOG1(x, ...)
#endif

#if LOG_VERBOSITY>=2
#define LOG2(x, ...) printlog(x,##__VA_ARGS__)
#else
#define LOG2(x, ...)
#endif

#define assert_memory(x,n) if(!x){printlog("MEMORY ALLOCATION OF %d bytes failed in %s, line %d.",(int)(n),__FILE__,__LINE__);exit(EXIT_RESTART);}
#define ph_malloc(n) (malloc(n)?:(printlog("MEMORY ALLOCATION OF %d bytes failed in %s, line %d.",(int)(n),__FILE__,__LINE__),exit(EXIT_RESTART),NULL))
#define ph_realloc(p,n) (realloc(p,n)?:(printlog("MEMORY REALLOCATION OF %d bytes failed in %s, line %d.",(int)(n),__FILE__,__LINE__),exit(EXIT_RESTART),NULL))
#define ph_strdup(s) (strdup(s)?:(printlog("MEMORY ALLOCATION OF %d bytes failed in %s, line %d.",s!=NULL?(int)strlen(s):0,__FILE__,__LINE__),exit(EXIT_RESTART),NULL))

#define ERRgoto(g,x, ...) {printlog(x,##__VA_ARGS__);goto g;}
#define LOG0c(c,x, ...) if(c){printlog(x,##__VA_ARGS__);}
#define LOG0goto(c,g,x, ...) if(c){printlog(x,##__VA_ARGS__);goto g;}
#define LOG0return(c,r,x, ...) if(c){printlog(x,##__VA_ARGS__);return r;}
#define LOG0close_return(c,r,todo,x, ...) if(c){printlog(x,##__VA_ARGS__);todo;return r;}
#define LOG0close_break(c,todo,x, ...) if(c){printlog(x,##__VA_ARGS__);todo;break;}
#define LOG0close_continue(c,todo,x, ...) if(c){printlog(x,##__VA_ARGS__);todo;continue;}
#define LOG0c_break(c,x, ...) if(c){printlog(x,##__VA_ARGS__);break;}
#define LOG0c_continue(c,x, ...) if(c){printlog(x,##__VA_ARGS__);continue;}
#define LOG0c_do(c,todo,x, ...) if(c){printlog(x,##__VA_ARGS__);todo;}
#define LOG_FLUSH flushlog()

//#define LOG0tc(c,t,x, ...) if(c){printlogt(t,x,##__VA_ARGS__);}

#endif // LOG_H
