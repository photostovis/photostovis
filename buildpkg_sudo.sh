#!/bin/sh
#set -x

#get the version and architecture
v_major=$(cat "src/photostovis.h" | grep "#define KPhotostovisVersionMajor" | cut -d' ' -f3)
v_minor=$(cat "src/photostovis.h" | grep "#define KPhotostovisVersionMinor" | cut -d' ' -f3)
architecture=$(uname -m)
#remove old files
sudo rm -f photostovis_*.deb
sudo rm -f pkg/photostovis/usr/bin/photostovis
sudo rm -f pkg/photostovis/DEBIAN/md5sums
sudo rm -f pkg/photostovis/DEBIAN/control
sudo rm -rf pkg/photostovis/usr/share/photostovis/web/*
#copy the proper control file
cp pkg/photostovis_arch/control.$(echo $architecture) pkg/photostovis/DEBIAN/control
#append the version to the control file, e.g.: Version: 2.30-1
echo "Version: ${v_major}.${v_minor}-1" >> pkg/photostovis/DEBIAN/control

#copy web files
sudo mkdir -p pkg/photostovis/usr/share/photostovis/web
sudo cp -r web/* pkg/photostovis/usr/share/photostovis/web/
#clean & make the package
cd ../photostovis-build
#make clean
make VERBOSE=1
cd ../photostovis
#copy the photostovis binary
sudo mkdir -p pkg/photostovis/usr/bin
sudo cp ../photostovis-build/photostovis pkg/photostovis/usr/bin/
sudo cp ../photostovis-build/convertVideos pkg/photostovis/usr/bin/
#sudo cp ../photostovis-build/createPhotostovisUser pkg/photostovis/usr/bin/
#create the md5 sums
cd pkg/photostovis
sudo find . -type f ! -regex '.*.hg.*' ! -regex '.*?debian-binary.*' ! -regex '.*?DEBIAN.*' -printf '%P ' | xargs md5sum > DEBIAN/md5sums
sudo chown root:root DEBIAN/md5sums
sudo chmod 0644 DEBIAN/md5sums
cd ../..
#build the package
dpkg-deb --build pkg/photostovis photostovis_$(echo "${v_major}.${v_minor}-1_${architecture}").deb




