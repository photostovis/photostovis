cmake_minimum_required(VERSION 3.18)

#For cubieboard:
#cmake -DCMAKE_BUILD_TYPE=CB3 ../photostovis

#For RaspberryPi 2, Tinkerboard:
#cmake -DCMAKE_BUILD_TYPE=RPI2 ../photostovis

#For RaspberryPi 3 & 3b+:
#cmake -DCMAKE_BUILD_TYPE=RPI3 ../photostovis

#For RaspberryPi 3b+ with button:
#cmake -DCMAKE_BUILD_TYPE=RPI3button ../photostovis

#independent of photostovis.net:
#cmake -DCMAKE_BUILD_TYPE=NO_PHONET ../photostovis

# -rdynamic and -funwind-tables are needed for backtrace() and backtrace_symbols() to work

IF(DEFINED CMAKE_OSX_SYSROOT)
    SET(CMAKE_OSX_SYSROOT "/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk")
    message(STATUS "MacOS detected")
ENDIF()


IF(DEFINED CMAKE_BUILD_TYPE)
    SET(CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE} CACHE STRING "Choose the type of build, options are: None, NO_PHONET, RELEASE, RELEASE_STATIC_LIBJPEG32, DEBUG_STATIC_LIBJPEG32.")
ENDIF()

message(STATUS "Build type is: " ${CMAKE_BUILD_TYPE})

project(photostovis)


include_directories("/opt/libjpeg-turbo/include")
include_directories("/usr/local/ssl/include")
include_directories("/usr/local/include")
include_directories("/usr/include/libxml2")

IF(DEFINED CMAKE_OSX_SYSROOT)
    include_directories("${CMAKE_OSX_SYSROOT}/usr/include/libxml2")

    include_directories("/opt/homebrew/include")
    include_directories("/opt/homebrew/opt/openssl@3/include")
ENDIF()



#find_library(JPEG_TURBO NAMES libjpeg)

find_library(EXIF_LIBRARY NAMES exif)
find_library(HEIF_LIBRARY NAMES heif)
find_library(DE265_LIBRARY NAMES de265)
find_library(AOM_LIBRARY NAMES aom)
#find_library(JXL_LIBRARY NAMES jxl)
#find_library(DAV1D_LIBRARY NAMES dav1d)
#find_library(RAV1E_LIBRARY NAMES rav1e)
#find_library(VMAF_LIBRARY NAMES vmaf)
find_library(X265_LIBRARY NAMES x265)

find_library(MINIUPNPC_LIBRARY NAMES miniupnpc)
find_library(IPTC_LIBRARY NAMES iptcdata)
find_library(XML2_LIBRARY NAMES xml2)

find_library(OPENSSL_LIBRARY NAMES openssl)



if(EXIF_LIBRARY)
    message(STATUS ${EXIF_LIBRARY})
else()
    message(FATAL_ERROR "FATAL ERROR: missing EXIF library")
endif()

if(HEIF_LIBRARY)
    message(STATUS ${HEIF_LIBRARY})
else()
    message(FATAL_ERROR "FATAL ERROR: missing HEIF library")
endif()

if(DE265_LIBRARY)
    message(STATUS ${DE265_LIBRARY})
else()
    message(FATAL_ERROR "FATAL ERROR: missing DE265 library")
endif()

if(AOM_LIBRARY)
    message(STATUS ${AOM_LIBRARY})
else()
    message(FATAL_ERROR "FATAL ERROR: missing AOM library")
endif()

#if(JXL_LIBRARY)
#    message(STATUS ${JXL_LIBRARY})
#else()
#    message(FATAL_ERROR "FATAL ERROR: missing JXL library")
#endif()

#if(DAV1D_LIBRARY)
#    message(STATUS ${DAV1D_LIBRARY})
#else()
#    message(FATAL_ERROR "FATAL ERROR: missing DAV1D library")
#endif()

#if(RAV1E_LIBRARY)
#    message(STATUS ${RAV1E_LIBRARY})
#else()
#    message(FATAL_ERROR "FATAL ERROR: missing RAV1E library")
#endif()

#if(VMAF_LIBRARY)
#    message(STATUS ${VMAF_LIBRARY})
#else()
#    message(FATAL_ERROR "FATAL ERROR: missing VMAF library")
#endif()

if(X265_LIBRARY)
    message(STATUS ${X265_LIBRARY})
else()
    message(FATAL_ERROR "FATAL ERROR: missing X265 library")
endif()




if(IPTC_LIBRARY)
    message(STATUS ${IPTC_LIBRARY})
else()
    message(FATAL_ERROR "FATAL ERROR: missing IPTC library")
endif()

if(MINIUPNPC_LIBRARY)
    message(STATUS ${MINIUPNPC_LIBRARY})
else()
    message(FATAL_ERROR "FATAL ERROR: missing miniupnpc library")
endif()

if(XML2_LIBRARY)
    message(STATUS ${XML2_LIBRARY})
else()
    message(FATAL_ERROR "FATAL ERROR: missing libxml2 library")
endif()

#if(OPENSSL_LIBRARY)
#    message(STATUS ${OPENSSL_LIBRARY})
#else()
#    message(FATAL_ERROR "FATAL ERROR: missing openssl library")
#endif()



link_directories("/opt/libjpeg-turbo/lib")
link_directories("/opt/libjpeg-turbo/lib32")
link_directories("/opt/libjpeg-turbo/lib64")
link_directories("/usr/local/ssl/lib")
link_directories("/opt/homebrew/opt/openssl@3/lib")

#SET(HEIF_LIBS ${HEIF_LIBRARY} ${DE265_LIBRARY} ${AOM_LIBRARY} ${JXL_LIBRARY} ${DAV1D_LIBRARY} ${RAV1E_LIBRARY} ${VMAF_LIBRARY} ${X265_LIBRARY})
SET(HEIF_LIBS ${HEIF_LIBRARY} ${DE265_LIBRARY} ${AOM_LIBRARY} ${X265_LIBRARY})


SET(SOURCES src/main.c src/scanpix.c src/getmedia.c src/photostovis.c src/jpegtransform.c src/bcrypt.c src/blf.c
        src/log_server.c src/minizip/zip.c src/minizip/ioapi.c
        src/websrv.c src/parsedescr.c src/utils.c src/metadata.c src/download.c src/img_manager.c src/thmb_manager.c src/file_manager.c
        src/phonet.c src/upnp_igd.c src/upgrade.c src/cubietruck_leds.c)

SET(RELEASE_LIBS_wheezy ${EXIF_LIBRARY} ${HEIF_LIBRARY} /opt/libjpeg-turbo/lib32/libjpeg.a ${MINIUPNPC_LIBRARY} ${IPTC_LIBRARY}
        /usr/local/lib/libcurl.a /usr/local/ssl/lib/libssl.a /usr/local/ssl/lib/libcrypto.a
        rt z dl pthread m)
SET(RELEASE_LIBS_static_libjpeg ${EXIF_LIBRARY} ${HEIF_LIBRARY} /opt/libjpeg-turbo/lib32/libjpeg.a ${MINIUPNPC_LIBRARY} ${IPTC_LIBRARY}
        curl ssl crypto rt z dl pthread m)
SET(RELEASE_LIBS_static_libjpeg_wiringPi ${RELEASE_LIBS_static_libjpeg} wiringPi)

SET(RELEASE_LIBS_dynamic ${EXIF_LIBRARY} ${HEIF_LIBRARY} jpeg ${MINIUPNPC_LIBRARY} ${IPTC_LIBRARY}
        curl ssl crypto rt z dl pthread m)


IF(DEFINED CMAKE_OSX_SYSROOT)
    SET(RELEASE_LIBS_generic ${EXIF_LIBRARY} ${HEIF_LIBS} ${IPTC_LIBRARY} jpeg ${MINIUPNPC_LIBRARY} stdc++ curl ssl crypto    z dl pthread)
ELSE()
    SET(RELEASE_LIBS_generic ${EXIF_LIBRARY} ${HEIF_LIBS} ${IPTC_LIBRARY} jpeg ${MINIUPNPC_LIBRARY} curl ssl crypto rt z dl pthread m unwind)
ENDIF()

SET(SOURCES_ADD_USER xtra/util/add_user.c src/bcrypt.c src/blf.c)
SET(LIBS_ADD_USER crypto dl)

SET(SOURCES_CONVERT_VIDEOS xtra/util/convert_videos.c)
SET(LIBS_CONVERT_VIDEOS xml2)

FILE(GLOB SOURCES_DATESHIFT
    "xtra/util/dateshift/*.h"
    "xtra/util/dateshift/*.c"
)
SET(LIBS_DATESHIFT ${EXIF_LIBRARY} m)

SET(SOURCES_UDEV xtra/util/udev.c)
SET(LIBS_UDEV udev)


SET(SOURCES_DELETE_DUPLICATES xtra/util/delete_duplicates.c)
#SET(LIBS_DELETE_DUPLICATES xml2)
ADD_LINK_OPTIONS(-rdynamic)

#SET(CMAKE_ENABLE_EXPORTS TRUE)


IF(CMAKE_BUILD_TYPE STREQUAL "CB3")
    SET(CMAKE_C_FLAGS "-O3 -DNDEBUG -std=c99 -Wall -rdynamic -funwind-tables  -march=armv7-a -mfloat-abi=hard -mfpu=neon-vfpv4")

    add_executable(${PROJECT_NAME} ${SOURCES})
    target_link_libraries(${PROJECT_NAME} ${RELEASE_LIBS_wheezy})

    add_executable("convertVideos" ${SOURCES_CONVERT_VIDEOS})
    target_link_libraries("convertVideos" ${LIBS_CONVERT_VIDEOS})


ELSEIF(CMAKE_BUILD_TYPE STREQUAL "CB3_dbg")
    SET(CMAKE_C_FLAGS "-g -std=c99 -Wall -rdynamic -funwind-tables  -march=armv7-a -mfloat-abi=hard -mfpu=neon-vfpv4")

    add_executable(${PROJECT_NAME} ${SOURCES})
    target_link_libraries(${PROJECT_NAME} ${RELEASE_LIBS_static_libjpeg})

    add_executable("convertVideos" ${SOURCES_CONVERT_VIDEOS})
    target_link_libraries("convertVideos" ${LIBS_CONVERT_VIDEOS})


ELSEIF(CMAKE_BUILD_TYPE STREQUAL "RPI2")
    SET(CMAKE_C_FLAGS "-O3 -DNDEBUG -std=c99 -Wall -rdynamic -funwind-tables  -mcpu=cortex-a7 -mfloat-abi=hard -mfpu=neon-vfpv4")

    add_executable(${PROJECT_NAME} ${SOURCES})
    target_link_libraries(${PROJECT_NAME} ${RELEASE_LIBS_static_libjpeg})

    add_executable("convertVideos" ${SOURCES_CONVERT_VIDEOS})
    target_link_libraries("convertVideos" ${LIBS_CONVERT_VIDEOS})

ELSEIF(CMAKE_BUILD_TYPE STREQUAL "RPI3")
    SET(CMAKE_C_FLAGS "-O3 -DNDEBUG -std=c99 -Wall -rdynamic -funwind-tables  -march=armv8-a+crc -mtune=cortex-a53 -mcpu=cortex-a53 -mfloat-abi=hard -mfpu=crypto-neon-fp-armv8")

    add_executable(${PROJECT_NAME} ${SOURCES})
    target_link_libraries(${PROJECT_NAME} ${RELEASE_LIBS_dynamic})

    add_executable("convertVideos" ${SOURCES_CONVERT_VIDEOS})
    target_link_libraries("convertVideos" ${LIBS_CONVERT_VIDEOS})

ELSEIF(CMAKE_BUILD_TYPE STREQUAL "RPI3button")
        SET(CMAKE_C_FLAGS "-O3 -DNDEBUG -DWITH_BUTTON -std=c99 -Wall -rdynamic -funwind-tables  -march=armv8-a+crc -mtune=cortex-a53 -mcpu=cortex-a53 -mfloat-abi=hard -mfpu=crypto-neon-fp-armv8")

        add_executable(${PROJECT_NAME} ${SOURCES})
        target_link_libraries(${PROJECT_NAME} ${RELEASE_LIBS_static_libjpeg_wiringPi})

        add_executable("convertVideos" ${SOURCES_CONVERT_VIDEOS})
        target_link_libraries("convertVideos" ${LIBS_CONVERT_VIDEOS})


ELSEIF(CMAKE_BUILD_TYPE STREQUAL "NO_PHONET")
    SET(CMAKE_C_FLAGS "-O3 -DNDEBUG -DCONFIG_NO_PHONET -std=c99 -Wall -rdynamic -funwind-tables")


    add_executable(${PROJECT_NAME} src/main.c src/scanpix.c src/getmedia.c src/photostovis.c src/jpegtransform.c src/bcrypt.c
        src/log_server.c src/minizip/zip.c src/minizip/ioapi.c 
        src/websrv.c src/parsedescr.c src/utils.c src/metadata.c src/download.c src/img_manager.c src/thmb_manager.c src/file_manager.c
        src/cubietruck_leds.c)
    target_link_libraries(${PROJECT_NAME} dl pthread exif iptcdata jpeg z)



ELSEIF(CMAKE_BUILD_TYPE STREQUAL "RELEASE")
    SET(CMAKE_C_FLAGS "-O3 -DNDEBUG -std=c99 -Wall -rdynamic -funwind-tables")

    add_executable(${PROJECT_NAME} ${SOURCES})
    target_link_libraries(${PROJECT_NAME} ${RELEASE_LIBS_generic})

    add_executable("convertVideos" ${SOURCES_CONVERT_VIDEOS})
    target_link_libraries("convertVideos" ${LIBS_CONVERT_VIDEOS})


ELSEIF(CMAKE_BUILD_TYPE STREQUAL "DEBUG_STATIC_LIBJPEG32")
    SET(CMAKE_C_FLAGS "-g -std=c99 -Wall -rdynamic -funwind-tables")

    add_executable(${PROJECT_NAME} ${SOURCES})
    target_link_libraries(${PROJECT_NAME} ${RELEASE_LIBS_static_libjpeg})

    add_executable("convertVideos" ${SOURCES_CONVERT_VIDEOS})
    target_link_libraries("convertVideos" ${LIBS_CONVERT_VIDEOS})

    add_executable("dateShift" ${SOURCES_DATESHIFT})
    target_link_libraries("dateShift" PUBLIC ${LIBS_DATESHIFT})
ELSEIF(CMAKE_BUILD_TYPE STREQUAL "RELEASE_STATIC_LIBJPEG32")
    SET(CMAKE_C_FLAGS "-O3 -DNDEBUG -std=c99 -Wall -rdynamic -funwind-tables")

    add_executable(${PROJECT_NAME} ${SOURCES})
    target_link_libraries(${PROJECT_NAME} ${RELEASE_LIBS_static_libjpeg})

    add_executable("convertVideos" ${SOURCES_CONVERT_VIDEOS})
    target_link_libraries("convertVideos" ${LIBS_CONVERT_VIDEOS})

    add_executable("dateShift" ${SOURCES_DATESHIFT})
    target_link_libraries("dateShift" PUBLIC ${LIBS_DATESHIFT})
ELSE()
    SET(CMAKE_C_FLAGS "-g -std=c99 -Wall -funwind-tables")

    add_executable(${PROJECT_NAME} ${SOURCES})
    target_link_libraries(${PROJECT_NAME} ${RELEASE_LIBS_generic})

    add_executable("createPhotostovisUser" ${SOURCES_ADD_USER})
    target_link_libraries("createPhotostovisUser" ${LIBS_ADD_USER})

    add_executable("convertVideos" ${SOURCES_CONVERT_VIDEOS})
    target_link_libraries("convertVideos" ${LIBS_CONVERT_VIDEOS})

    add_executable("dateShift" ${SOURCES_DATESHIFT})
    target_link_libraries("dateShift" PUBLIC ${LIBS_DATESHIFT})

    add_executable("deldups" ${SOURCES_DELETE_DUPLICATES})
#    target_link_libraries("dateShift" PUBLIC ${LIBS_DATESHIFT})

#    add_executable("udevtest" ${SOURCES_UDEV})
#    target_link_libraries("udevtest" ${LIBS_UDEV})

#    add_executable("tests" tsrc/tests.c src/scanpix.c src/jpegtransform.c
#        src/parsedescr.c src/utils.c src/metadata.c)
#    target_link_libraries(tests dl pthread exif jpeg)

#    add_executable("tests" tsrc/tests.c src/utils.c
#        src/minizip/zip.c src/minizip/ioapi.c)
#    target_link_libraries(tests dl z)

#    add_executable("concat_index_html" xtra/util/concat.c)
#    target_link_libraries(concat_index_html z)
ENDIF()


#
