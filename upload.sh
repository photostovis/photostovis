#!/bin/sh
#set -x

#get the version and architecture
v_major=$(cat "src/photostovis.h" | grep "#define KPhotostovisVersionMajor" | cut -d' ' -f3)
v_minor=$(cat "src/photostovis.h" | grep "#define KPhotostovisVersionMinor" | cut -d' ' -f3)
architecture=$(uname -m)
distribution=$1
package="photostovis_"$(echo "${v_major}.${v_minor}-1_${architecture}")".deb"

curl -F "file=@"$package";filename="$package "https://photostovis.net/csc/uploadPkg.php?a="$architecture"&d="$distribution

