"use strict";
/* Ro characters:
 * ă î â ș ț
 * Ă Î Â Ș Ț
 */

PH.l10n=(function(){
    var l10n={
        txt_date_wwddmm:{en:"%w, %m %d",ro:"%w, %d %m"},
        txt_date_ddmmyy:{en:"%m %d, %y",ro:"%d %m %y"},
        txt_time_am:{en:"%h12:%m AM",ro:"%h24:%m"},
        txt_time_pm:{en:"%h12:%m PM",ro:"%h24:%m"},
        txt_time_am_tz:{en:"%h12:%m AM, GMT%z",ro:"%h24:%m, GMT%z"},
        txt_time_pm_tz:{en:"%h12:%m PM, GMT%z",ro:"%h24:%m, GMT%z"},
        txt_imghelp_left:{en:"Click in the left side to go to the previous picture",ro:"Click în partea dreaptă pentru poza precedentă"},
        txt_imghelp_right:{en:"Click in the right side to go to the next picture",ro:"Click în partea stângă pentru poza următoare"},
        txt_imghelp_center:{en:"Click in the center to exit picture mode",
            ro:"Click în partea centrală pentru a reveni la lista pozelor din album"},
        imghelp_gotit_btn:{en:"Ok, I got it!",ro:"Am înțeles!"},
        
        txt_0p_0v_0a:{en:"no pictures%band no albums",ro:"nicio poză%bși niciun album"},
        txt_0p_0v_1a:{en:"1 album",ro:"1 album"},
        txt_0p_0v_na:{en:"%a albums",ro:"%a albume"},
        txt_1p_0v_0a:{en:"1 picture",ro:"1 poză"},
        txt_1p_0v_1a:{en:"1 picture%band 1 album",ro:"1 poză%bși 1 album"},
        txt_1p_0v_na:{en:"1 picture%band %a albums",ro:"1 poză%bși %a albume"},
        txt_np_0v_0a:{en:"%p pictures",ro:"%p poze"},
        txt_np_0v_1a:{en:"%p pictures%band 1 album",ro:"%p poze%bși 1 album"},
        txt_np_0v_na:{en:"%p pictures%band %a albums",ro:"%p poze%bși %a albume"},
        
        txt_0p_1v_0a:{en:"1 video",ro:"1 film"},
        txt_0p_1v_1a:{en:"1 video%band 1 album",ro:"1 film%bși 1 album"},
        txt_0p_1v_na:{en:"1 video%band %a albums",ro:"1 film%bși %a albume"},
        txt_1p_1v_0a:{en:"1 picture and 1 video",ro:"1 poză și 1 film"},
        txt_1p_1v_1a:{en:"1 picture, 1 video%band 1 album",ro:"1 poză, 1 film%bși un album"},
        txt_1p_1v_na:{en:"1 picture, 1 video%band %a albums",ro:"1 poză, 1 film%bși %a albume"},
        txt_np_1v_0a:{en:"%p pictures and 1 video",ro:"%p poze și 1 film"},
        txt_np_1v_1a:{en:"%p pictures, 1 video%band 1 album",ro:"%p poze, 1 film%bși 1 album"},
        txt_np_1v_na:{en:"%p pictures, 1 video%band %a albums",ro:"%p poze, 1 film%bși %a albume"},
        
        txt_0p_nv_0a:{en:"%v videos",ro:"%v filme"},
        txt_0p_nv_1a:{en:"%v videos%band 1 album",ro:"%v filme%bși 1 album"},
        txt_0p_nv_na:{en:"%v videos%band %a albums",ro:"%v filme%bși %a albume"},
        txt_1p_nv_0a:{en:"1 picture and %v videos",ro:"1 poză și %v filme"},
        txt_1p_nv_1a:{en:"1 picture, %v videos%band 1 album",ro:"1 poză, %v filme%bși un album"},
        txt_1p_nv_na:{en:"1 picture, %v videos%band %a albums",ro:"1 poză, %v filme%bși %a albume"},
        txt_np_nv_0a:{en:"%p pictures and %v videos",ro:"%p poze și %v filme"},
        txt_np_nv_1a:{en:"%p pictures, %v videos%band 1 album",ro:"%p poze, %v filme%bși 1 album"},
        txt_np_nv_na:{en:"%p pictures, %v videos%band %a albums",ro:"%p poze, %v filme%bși %a albume"},
        
        
        txt_photostovis_root:{en:"PHOTOSTOVIS",ro:"PHOTOSTOVIS"},
        txt_photostovis_root_named:{en:"PHOTOSTOVIS@%s",ro:"PHOTOSTOVIS@%s"},
        txt_timeline_from:{en:"From: ",ro:"Din: "},
        txt_timeline_until:{en:"Until: ",ro:"Până în: "},
        txt_go_up:{en:"Back to the top of this album",ro:"Înapoi la începutul albumului"},
        txt_go_back:{en:"Back to this album",ro:"Înapoi la acest album"},
        txt_reg_pw_too_short1:{en:"Password is too short (1 character). Minimum required: %L characters.",
            ro:"Parola e prea scurtă (1 caracter). Parola trebuie să aibă cel puțin %L caractere."},
        txt_reg_pw_too_shortN:{en:"Password is too short (%l characters). Minimum required: %L characters.",
            ro:"Parola e prea scurtă (%l caractere). Parola trebuie să aibă cel puțin %L caractere."},
        txt_reg_un_too_short1:{en:"Username is too short (1 character). Minimum required: %L characters.",
            ro:"Username-ul e prea scurt (1 caracter). Trebuie să aibă cel puțin %L caractere."},
        txt_reg_un_too_shortN:{en:"Username is too short (%l characters). Minimum required: %L characters.",
            ro:"Usrname-ul e prea scurt (%l caractere). Trebuie să aibă cel puțin %L caractere."},
        txt_err_upnp:{en:"There was an error configuring your Internet Gateway:",ro:"A apărut o eroare la configurarea modemului:"},
        txt_err_phonet:{en:"There was an error when connecting to photostovis.net:",ro:"A apărut o eroare la conectarea la photostovis.net:"},
        
        txt_register_srv:{en:"register this server",ro:"înregistrați serverul"},
        txt_reregister_srv:{en:"re-register this server",ro:"reintroduceți"},
        
        //scanning pictures
        txt_spd_parsing:{en:"Photostovis is looking for new pictures. This may take a while.",
            ro:"Photostovis cauta poze noi. Acest lucru s-ar putea sa dureze cateva minute."},
        txt_spd_progress_data:{en:"%ss/%st",ro:"%ss/%st"},

        //authentication
        txt_auth_title_original:{en:"This Photostovis server requires authentication:",ro:"Acest server Photostovis necesită autentificare:"},
        txt_auth_wrong_unorpw:{en:"Wrong username or password. Please try again:",ro:"Nume utilizator sau parolă greșite. Vă rog să încercați din nou:"},
        txt_auth_title_signin:{en:"Sign in to enable additional priviledges:",ro:"Autentificați-vă pentru a obține privilegii suplimentare:"},
        //forbidden page/section
        txt_forbidden_nousers:{en:"This Photostovis server would theoretically allow you to authenticate and connect to it, but there are no users defined, so there is no way for you to authenticate. Please talk to the owner of the server to add some users. If you are the owner, please run the Configuration Wizzard from the menu.",
            ro:"Teoretic, acest server Photostovis acceptă să vă autentificați și să vă conectați la el, dar, din păcate, proprietarul serverului nu a definit niciun nume de utilizator. Vă sugerez să vorbiți cu proprietarul să adauge cel puțin un nume de utilizator. Dacă proprietarul sunteți chiar dumneavoastră, vă sugerez să rulați Configuratorul, din meniu."},
        txt_forbidden_noaccess:{en:"This Photostovis server forbids access from Internet. It only allows access from Local Network.",
            ro:"Acest server Photostovis nu poate fi accesat din Internet, ci doar din Rețeaua Locală"}
    };
    return function(id,lng){
        if(typeof(lng)==='undefined')lng=this.userPrefsGlobal.language;
        console.assert(lng==='en' || lng==='ro');

        console.assert(typeof(l10n[id])!=='undefined');
        console.assert(typeof(l10n[id][lng])!=='undefined');
        return l10n[id][lng];
    };
})();

PH.l10nGatewayConfigError=(function(){
    var err={
        1:{en:"Error discovering UPnP devices.",ro:"Eroare la cautarea dispozitivelor UPnP de conectare la internet."},
        2:{en:"No UPnP devices discovered.",ro:"Nu a fost gasit niciun dispozitiv de conectare la internet care sa suporte protocolul UPnP"},
        3:{en:"Error: Photostovis server cannot get the local (LAN) IP address, so it does not know where to instruct the Internet Gateway to redirect the incoming requests",
           ro:"Serverul Photostovis nu a gasit adresa IP locala, asa ca nu stie unde sa ii spuna modemului sa redirecteze cererile de acces venite din afara retelei locale."},
        4:{en:"Error: requesting the Internet Gateway device to redirect incomming traffic from outside the local network to Photostovis server failed",
           ro:"Cererea de redirectare a traficului extern catre serverul Photostovis a esuat."},
        5:{en:"There was a problem with redirecting incomming traffic from outside the local network to Photostovis server: verification failed",
           ro:"A aparut o problema la redirectarea traficului extern catre serverul Photostovis: verificarea a esuat."}
    };
    
    return function(id,lng){
        if(typeof(lng)==='undefined')lng=this.userPrefsGlobal.language;
        console.assert(lng==='en' || lng==='ro');

        console.assert(typeof(err[id])!=='undefined');
        console.assert(typeof(err[id][lng])!=='undefined');
        return err[id][lng];
    };
})();

PH.l10nPhonetStartError=(function(){
    var err={
         0:{en:"This server is not registered with photostovis.net, so it is not possible to connect to it from outside your local network. If you would like this, then please ",//register this server
             ro:"Acest server nu este inregistrat la photostovis.net, deci nu se poate accesa din afara retelei locale. Daca doriti acest lucru, "},//inregistrati serverul
         1:{en:"Error connecting to photostovis.net: initialization failed (curl).",
            ro:"Eroare la conectare la photostovis.net: initializare nereusita (curl)."},
         2:{en:"Error connecting to photostovis.net: connection failed (curl).",
            ro:"Eroare la conectare la photostovis.net: conexiune nereusita (curl)."},
         3:{en:"Error parsing the answer from photostovis.net.",ro:"Eroare la parsarea raspunsului primit de la photostovis.net"},
         4:{en:"Error parsing the answer from photostovis.net: error number outside the range",
            ro:"Eroare la parsarea raspunsului primit de la photostovis.net: numarul erorii este invalid."},
        
        10:{en:"This Photostovis server cannot be reached from the Internet.",ro:"Acest server Photostovis nu poate fi accesat din Internet"},
        11:{en:"Server registration with photostovis.net failed.",ro:"Inregistrarea serverului la photostovis.net nu a reusit."},
        12:{en:"photostovis.net experienced an internal failure. Registration failed.",
            ro:"Serverul photostovis.net are probleme interne. Inregistrarea serverului la photostovis.net nu a reusit."},
        13:{en:"photostovis.net complains about invalid parameters. Registration failed.",
            ro:"Serverul photostovis.net de plange de parametrii inregistrarii. Inregistrarea serverului la photostovis.net nu a reusit."},
        14:{en:"photostovis.net has found another server with the same username. Registration failed.",
            ro:"Serverul photostovis.net a gasit un alt server cu acelasi nume. Inregistrarea serverului la photostovis.net nu a reusit."},
        15:{en:"photostovis.net has found another server registered with the same username. Registration failed.",
            ro:"Serverul photostovis.net a gasit un alt server inregistrat cu acelasi nume. Inregistrarea serverului la photostovis.net nu a reusit."},
        16:{en:"Congratulations, you have the latest Photostovis version.",
            ro:"Felicitari, aveti cea mai recenta versiune de Photostovis."},
        17:{en:"The username or password you are using to register to photostovis.net are different from those previously used. Registration failed.",
            ro:"Numele sau parola folosite pentru inregistrarea la photostovis.net sunt diferite de cele folosite anterior. Inregistrarea serverului la photostovis.net nu a reusit."},
        18:{en:"The username or password for registering this server to photostovis.net are missing. Please re-register this server.",
            ro:"Numele si parola de inregistrare la photostovis.net lipsesc. Va rugam sa le reintroduceti."}
    };
    
    return function(id,lng){
        if(typeof(lng)==='undefined')lng=this.userPrefsGlobal.language;
        console.assert(lng==='en' || lng==='ro');

        console.assert(typeof(err[id])!=='undefined');
        console.assert(typeof(err[id][lng])!=='undefined');
        return err[id][lng];
    };
})();

PH.l10nWeekdaysShort=(function(){
    var txt_weekdays=[
        {en:"Sun",ro:"Du"},
        {en:"Mon",ro:"Lu"},
        {en:"Tue",ro:"Ma"},
        {en:"Wed",ro:"Mi"},
        {en:"Thu",ro:"Jo"},
        {en:"Fri",ro:"Vi"},
        {en:"Sat",ro:"Sâ"}
    ];
    return function(i,lng){
        if(typeof(lng)==='undefined')lng=this.userPrefsGlobal.language;
        console.assert(lng==='en' || lng==='ro');
        console.assert(typeof(i)==='number');
        console.assert(typeof(txt_weekdays[i])!=='undefined');
        console.assert(typeof(txt_weekdays[i][lng])!=='undefined');
        return txt_weekdays[i][lng];
    };
})();

PH.l10nMonthsShort=(function(){
    var txt_months=[
        {en:"Jan",ro:"ian"},
        {en:"Feb",ro:"feb"},
        {en:"Mar",ro:"mar"},
        {en:"Apr",ro:"apr"},
        {en:"May",ro:"mai"},
        {en:"Jun",ro:"iun"},
        {en:"Jul",ro:"iul"},
        {en:"Aug",ro:"aug"},
        {en:"Sep",ro:"sep"},
        {en:"Oct",ro:"oct"},
        {en:"Nov",ro:"noi"},
        {en:"Dec",ro:"dec"}
    ];
    return function(i,lng){
        if(typeof(lng)==='undefined')lng=this.userPrefsGlobal.language;
        console.assert(lng==='en' || lng==='ro');
        console.assert(typeof(i)==='number');
        console.assert(typeof(txt_months[i])!=='undefined');
        console.assert(typeof(txt_months[i][lng])!=='undefined');
        return txt_months[i][lng];
    };
})();

PH.l10nWeekdaysLong=(function(){
    var txt_weekdays=[
        {en:"Sunday",ro:"Duminică"},
        {en:"Monday",ro:"Luni"},
        {en:"Tuesday",ro:"Marți"},
        {en:"Wednesday",ro:"Miercuri"},
        {en:"Thursday",ro:"Joi"},
        {en:"Friday",ro:"Vineri"},
        {en:"Saturday",ro:"Sâmbătă"}
    ];
    return function(i,lng){
        if(typeof(lng)==='undefined')lng=this.userPrefsGlobal.language;
        console.assert(lng==='en' || lng==='ro');
        console.assert(typeof(i)==='number');
        console.assert(typeof(txt_weekdays[i])!=='undefined');
        console.assert(typeof(txt_weekdays[i][lng])!=='undefined');
        return txt_weekdays[i][lng];
    };
})();

PH.l10nMonthsLong=(function(){
    var txt_months=[
        {en:"January",ro:"ianuarie"},
        {en:"February",ro:"februarie"},
        {en:"March",ro:"martie"},
        {en:"April",ro:"aprilie"},
        {en:"May",ro:"mai"},
        {en:"June",ro:"iunie"},
        {en:"July",ro:"iulie"},
        {en:"August",ro:"august"},
        {en:"September",ro:"septembrie"},
        {en:"October",ro:"octombrie"},
        {en:"November",ro:"noiembrie"},
        {en:"December",ro:"decembrie"}
    ];
    return function(i,lng){
        if(typeof(lng)==='undefined')lng=this.userPrefsGlobal.language;
        console.assert(lng==='en' || lng==='ro');
        console.assert(typeof(i)==='number');
        console.assert(typeof(txt_months[i])!=='undefined');
        console.assert(typeof(txt_months[i][lng])!=='undefined');
        return txt_months[i][lng];
    };
})();
/*
PH.getExposureProgram=(function(){
    var txt_programs=[
        {en:"not defined",ro:"nedefinit"},
        {en:"Manual",ro:"Manual"},
        {en:"Normal program",ro:"Program normal"},
        {en:"Aperture priority",ro:"Prioritate deschidere (aperture)"},
        {en:"Shutter priority",ro:"Prioritate obturator (shutter)"},
        {en:"Creative program<br>(biased toward depth of field)",ro:"Program creativ<br>(prioritate dată profunzimii de câmp)"},
        {en:"Action program<br>(biased toward fast shutter speed)",ro:"Program de acțiune<br>(prioritate dată timpului de expunere scurt)"},
        {en:"Portrait mode<br>(closeup photos with the background out of focus)",ro:"Mod portret<br>(prim-plan focalizat, fundal neclar)"},
        {en:"Landscape mode<br>(background in focus)",ro:"Mod peisaj<br>(focalizare pe fundal)"}
    ];
    return function(i,lng){
        if(typeof(lng)==='undefined')lng=this.userPrefsGlobal.language;
        console.assert(lng==='en' || lng==='ro');
        console.assert(typeof(i)==='number');
        if(i<0 || i>=txt_programs.length)i=0;
        console.assert(typeof(txt_programs[i])!=='undefined');
        console.assert(typeof(txt_programs[i][lng])!=='undefined');
        return txt_programs[i][lng];
    };
})();*/
PH.getExposureProgram=(function(){
    var txt_programs=[
        {en:"not defined",ro:"nedefinit"},
        {en:"Manual",ro:"Manual"},
        {en:"Normal program",ro:"Program normal"},
        {en:"Aperture priority",ro:"Prioritate deschidere"},
        {en:"Shutter priority",ro:"Prioritate obturator"},
        {en:"Creative program",ro:"Program creativ"},
        {en:"Action program",ro:"Program de acțiune"},
        {en:"Portrait mode",ro:"Mod portret"},
        {en:"Landscape mode",ro:"Mod peisaj"}
    ];
    var txt_tooltips=[
        {en:"",ro:""},
        {en:"",ro:""},
        {en:"",ro:""},
        {en:"",ro:"Aperture priority"},
        {en:"",ro:"Shutter priority"},
        {en:"biased toward depth of field",ro:"prioritate dată profunzimii de câmp"},
        {en:"biased toward fast shutter speed",ro:"prioritate dată timpului de expunere scurt"},
        {en:"closeup photos with the background out of focus",ro:"prim-plan focalizat, fundal neclar"},
        {en:"background in focus",ro:"focalizare pe fundal"}
    ];
    return function(i,lng){
        var txt;
        if(typeof(lng)==='undefined')lng=this.userPrefsGlobal.language;
        console.assert(lng==='en' || lng==='ro');
        console.assert(typeof(i)==='number');
        if(i<0 || i>=txt_programs.length)i=0;
        console.assert(typeof(txt_programs[i])!=='undefined');
        console.assert(typeof(txt_programs[i][lng])!=='undefined');
        console.assert(typeof(txt_tooltips[i])!=='undefined');
        console.assert(typeof(txt_tooltips[i][lng])!=='undefined');
        if(txt_tooltips[i][lng].length>0)
            txt='<div title="'+txt_tooltips[i][lng]+'">';
        else
            txt='<div>';
        txt+=txt_programs[i][lng]+'</div>';
        return txt;
    };
})();

PH.getFormattedTime=function(d){
    var h24=d.getUTCHours(),m=d.getUTCMinutes(),mm,h12;
    //set minutes
    if(m<10)mm='0'+m;
    else mm=m;
    //set h12
    if(h24>12)h12=h24-12;
    else h12=h24;

    if(h24>11)//pm
        return this.l10n('txt_time_pm').replace('%h12',h12).replace('%h24',h24).replace('%m',mm);
    else
        return this.l10n('txt_time_am').replace('%h12',h12).replace('%h24',h24).replace('%m',mm);
};

PH.getFormattedShortDate=function(d,showYear){
    if(showYear)
        return this.l10n('txt_date_ddmmyy')
            .replace('%d',d.getUTCDate())
            .replace('%m',this.l10nMonthsShort(d.getUTCMonth()))
            .replace('%y',d.getUTCFullYear());
    else
        return this.l10n('txt_date_wwddmm')
            .replace('%w',this.l10nWeekdaysShort(d.getUTCDay()))
            .replace('%d',d.getUTCDate())
            .replace('%m',this.l10nMonthsShort(d.getUTCMonth()));
};

PH.getFormattedLongDate=function(d,showYear){
    if(showYear)
        return this.l10n('txt_date_ddmmyy')
            .replace('%d',d.getUTCDate())
            .replace('%m',this.l10nMonthsLong(d.getUTCMonth()))
            .replace('%y',d.getUTCFullYear());
    else
        return this.l10n('txt_date_wwddmm')
            .replace('%w',this.l10nWeekdaysLong(d.getUTCDay()))
            .replace('%d',d.getUTCDate())
            .replace('%m',this.l10nMonthsLong(d.getUTCMonth()));
};

PH.getFormattedLongOrShortDate=function(d,showYear,width){
    var dd=new Date(d*1000);
    if(width<768)
        return this.getFormattedShortDate(dd,showYear);
    else
        return this.getFormattedLongDate(dd,showYear);
};

PH.getFormattedDateTime=function(dx,tz){
    var d=new Date(dx*1000);
    var h24=d.getUTCHours(),m=d.getUTCMinutes(),mm,h12;
    var datetxt;
    var tzTxt=null;
    
    console.log("Hour: "+h24);
    
    datetxt=this.l10n('txt_date_ddmmyy')
            .replace('%d',d.getUTCDate())
            .replace('%m',this.l10nMonthsLong(d.getUTCMonth()))
            .replace('%y',d.getUTCFullYear());
    
    //set minutes
    if(m<10)mm='0'+m;
    else mm=m;
    //set h12
    if(h24>12)h12=h24-12;
    else h12=h24;
    //timezone?
    if(typeof(tz)==='number' && tz!==127){
        //we have a timezone
        if(!(tz%4))
            tzTxt=(tz/4).toFixed(0);
        else if(!(tz%2))
            tzTxt=(tz/4).toFixed(1);
        else
            tzTxt=(tz/4).toFixed(2);
        if(tz>0)tzTxt='+'+tzTxt;
        else if(tz===0)tzTxt='';
    }

    if(tzTxt!==null){
        if(h24>11)//pm
            return datetxt+', '+this.l10n('txt_time_pm_tz').replace('%h12',h12).replace('%h24',h24).replace('%m',mm).replace('%z',tzTxt);
        else
            return datetxt+', '+this.l10n('txt_time_am_tz').replace('%h12',h12).replace('%h24',h24).replace('%m',mm).replace('%z',tzTxt);
    } else {
        if(h24>11)//pm
            return datetxt+', '+this.l10n('txt_time_pm').replace('%h12',h12).replace('%h24',h24).replace('%m',mm);
        else
            return datetxt+', '+this.l10n('txt_time_am').replace('%h12',h12).replace('%h24',h24).replace('%m',mm);
    }
};

