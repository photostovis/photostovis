"use strict";

PH.getFullPath=function(tStart){
    if(!this.server.metadata)return; //we load the album(s) when we have the metadata

    var tDiff;
    var x=new XMLHttpRequest();
    var i,l,url='/a',query='?n='+this.strip.maxNrThumbnailsPerRow+this.platform.pixelRatioTxtThmbAm;
    var e,id;
    l=this.path.length;
    if(typeof(tStart)==='undefined')
        tStart=performance.now();
    console.assert(l<=this.hash.targetPath.length);
    if(l){
        for(i=0;i<l;i++)
            url+='/'+this.hash.targetPath[i];
    }

    //TODO: check the element that we are loading: do we find it? Is it an album, or something else? We should not trust targetPath too much
    
    console.log("getFullPath: loading album "+url+query+" as element "+l);

    x.onreadystatechange=function() {
        var pathElm;
        if(x.readyState===4){
            if(x.status===200){
                if(url!==PH.loadingAlbum){
                    console.warn("WARNING: We received an out-of-date album ("+url+"!=="+PH.loadingAlbum+").");
                    if(PH.loadingAlbum!=='')
                        console.warn("Expecting album: "+PH.loadingAlbum);
                    return;
                }
                PH.loadingAlbum='';//if here, we are loading the right album

                pathElm=parseJSON(x.responseText);
                //is this by any chance an authentication error?
                if(typeof(pathElm.forbidden)==='boolean' && pathElm.forbidden===true){
                    //this is authentication error
                    PH.handleReceivedForbidden(pathElm);
                    return;
                }

                pathElm.scrollTop=0;
                pathElm.index=-1;
                PH.path.push(pathElm);
                if(PH.path.length>1){
                    //find the current index in the previous path element
                    id=PH.hash.targetPath[l-1];
                    console.assert(id===PH.path[l].a.id,"id="+id+", l="+l+", PH.path.a.id="+PH.path[l].a.id);
                    e=PH.path[l-1].e;
                    for(i=0;i<e.length;i++)
                        if(e[i].id===id){
                            PH.path[l-1].index=i;
                            break;
                        }
                    console.assert(i<e.length);//assumes we found our index

                    if(i>=e.length){
                        //we reload the main album
                        PH.path=[];
                        PH.hash.targetPath=[];
                        PH.setHash();
                        return;
                    }
                } else {
	                //this was the first element in the path, the root album. Update the number of pictures/videos/albums in statistics
	                elmHTML('about_total',pathElm.a.p+' / '+pathElm.a.v+' / '+pathElm.a.a);
                }

                console.log("PH.hash.targetPath.length="+PH.hash.targetPath.length+", PH.path.length="+PH.path.length);
                if(PH.hash.targetPath.length+1===PH.path.length){
                    PH.hash.targetPath=[]; //reset targetPath, not needed anymore
                    tDiff=performance.now()-tStart;
                    console.log("getFullPath: path loaded in "+tDiff.toFixed(2)+" ms.");
                    //let the hash match the path
                    if(url.slice(1)!==PH.hash.hash.slice(1)){
                        console.log("url="+url.slice(1)+", hash="+PH.hash.hash.slice(1));
                        PH.setHash(); //this displays the album
                    } else if(PH.server.metadata){
                        console.log("Path:");
                        console.log(PH.path);
                        console.log("Displaying album");
                        PH.displayAlbum();
                    }
                } else if(PH.hash.targetPath.length===PH.path.length && !PH.hash.targetPathIsAlbum){
                    id=PH.hash.targetPath[l];
                    e=PH.path[l].e;
                    console.log("Looking for element with id="+id+", nr elements: "+e.length);
                    for(i=0;i<e.length;i++)
                        if(e[i].id===id){
                            PH.path[l].index=i;
                            break;
                        }
                    //TODO: what if we did not find the index?
                    console.assert(i<e.length);//assumes we found our index


                    //recreate the path as the target path
                    PH.hash.targetPath=[]; //reset targetPath, not needed anymore
                    tDiff=performance.now()-tStart;
                    console.log("getFullPath: path loaded in "+tDiff.toFixed(2)+" ms.");
                    //let the hash match the path

                    if(PH.server.metadata){
                        console.log("Path:");
                        console.log(PH.path);
                        console.log("Displaying image with index: "+i);
                        PH.albumCacheInitWithoutDisplay();//calls PH.albumCache.init(albumData.e,pathStr,albumTitle,pictureDisplayNr);
                        PH.path[PH.path.length-1].forceDisplay=true; //this will force a displayAlbum when exiting the picture mode

                        //enter full-screen mode (we can only do this from a user-generated event, such as click
                        elmShow('fullscreendiv');
                        PH.enterFullScreen();
                        setTimeout(function(){
                            elmShow('imgdiv');
                            if(!PH.userPrefsGlobal.skipImgHelp && !PH.userSession.showcase)
                                PH.showImgHelp();

                            //show the image
                            PH.albumCache.displayMedia(i);
                        },100);
                    }
                } else
                    PH.getFullPath(tStart);
            } else {
                if(x.status===403){
                    PH.ajaxError(403,"getFullPath");
                } else {
                    PH.loadingAlbum='';//because the album was not actually loaded
                    if(PH.path.length>0)
                    	PH.path[PH.path.length-1].index=-1;
                    console.warn("WARNING: XMLHttprequest failed: "+x.statusText);
                    console.warn(x);
                    //stop here
                    PH.setHash();
                }
            }
        }
    };//onreadystatechange
    if(this.loadingAlbum!==''){
        if(url===this.loadingAlbum){
            console.warn("WARNING: refusing to load the same album twice ("+url+")");
            return;
        } else
            console.warn("WARNING: loading a new album before receiving the previous one");
    }
    this.loadingAlbum=url;
    x.open('GET',this.createGetUrl(url,query,false),true);
    x.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    x.timeout=20000; //20 seconds
    x.send(null);

    PH.setupWebsocket();
};
PH.getAlbum=function(recreate){
    if(!this.server.metadata)return; //we load the album when we have the metadata

    var tStart=performance.now(),tDiff;
    var x=new XMLHttpRequest();
    var i,p='',q='?n='+this.strip.maxNrThumbnailsPerRow+this.platform.pixelRatioTxtThmbAm;
    console.log("getAlbum: path has "+this.path.length+" elements");
    for(i=0;i<this.path.length;i++){
        if(typeof(this.path[i].index)!=='number')
            throw new Error("typeof(this.path["+i+"].index) is "+typeof(this.path[i].index));
        if(this.path[i].index<0){
            if(i===this.path.length-1 && typeof(recreate)==='boolean' && recreate){
                //this is ok, we are recreating this album. We can stop here with building the path
                break;
            }
            throw new Error("this.path["+i+"].index="+this.path[i].index);
        }
        if(typeof(this.path[i].e[this.path[i].index])!=='object')
            throw new Error("typeof(this.path["+i+"].e["+this.path[i].index+"]) is "+typeof(this.path[i].e[this.path[i].index]));

        p+='/'+this.path[i].e[this.path[i].index].id;
    }

    if(typeof(recreate)!=='boolean' || !recreate)
        console.assert(('/t'+p+'/')!==this.albumCache.getPathStr());
    p=this.createGetUrl('/a'+p,q,false);
    console.log("getAlbum: loading album: "+p);

    x.onreadystatechange=function(){
        var pathElm,lastPathElm;
        if(x.readyState===4){
            if(x.status===200){
	            if(PH.userSession.dismissMessageOnAlbumLoad){
		            console.log("Here, dismissing the waiting dialog")
		            //we are waiting for a newly created album, probably this is it!
		            PH.userSession.dismissMessageOnAlbumLoad=false;
		            PH.dismissMessage();
		        }
		                            	
                if(p!==PH.loadingAlbum){
                    console.warn("WARNING: We received an out-of-date album ("+p+"!=="+PH.loadingAlbum+").");
                    if(PH.loadingAlbum!=='')
                        console.warn("Expecting album: "+PH.loadingAlbum);
                    return;
                }
                PH.loadingAlbum='';//if here, we are loading the right album
                pathElm=parseJSON(x.responseText);
                //is this by any chance an authentication error?
                if(typeof(pathElm.forbidden)==='boolean' && pathElm.forbidden===true){
                    //this is authentication error
                    PH.handleReceivedForbidden(pathElm);
                    return;
                }

                pathElm.scrollTop=0;
                pathElm.index=-1;
                console.assert(typeof(pathElm.a.l)==='number');
                console.assert(typeof(pathElm.a.id)==='string');
                
                if(PH.path.length){
                    //if we are scanning elements right now it may be that we already have this album in path (received through websocket)
                    lastPathElm=PH.path[PH.path.length-1];
                    console.assert(typeof(lastPathElm.a.l)==='number');
                    console.assert(typeof(lastPathElm.a.id)==='string');
                    if(lastPathElm.a.l===pathElm.a.l && lastPathElm.a.id===pathElm.a.id){
                        console.log("We have received the same element as in our path. Ignoring.");
                        return;
                    }
                } else {
	                //this was the first element in the path, the root album. Update the number of pictures/videos/albums in statistics
	                elmHTML('about_total',pathElm.a.p+'  /'+pathElm.a.v+' / '+pathElm.a.a);
                }
                //is this element right for this position in path?
                if(pathElm.a.l!==PH.path.length){
                    console.error("We received an element with wrong level ("+pathElm.a.l+"), our path has "+PH.path.length+
                            " elements. We ignore the received element (and verify the path).");
                    
                    for(i=0;i<PH.path.length;i++){
                        if(PH.path[i].a.l!==i){
                            console.error("Wrong element (level: "+PH.path[i].a.l+") in path at position "+i+". Purging the rest of the path.");
                            while(PH.path.length>i)PH.path.pop();
                            if(PH.path.length){
                                if(PH.server.metadata)
                                    PH.displayAlbum();
                            } else
                                PH.getAlbum();
                            break;
                        }//if
                    }//for
                    //if here, we just need to return
                    return;
                }
                PH.path.push(pathElm);
                tDiff=performance.now()-tStart;
                console.log("getAlbum: album ("+p+") loaded in "+tDiff.toFixed(2)+" ms.");
                if(PH.server.metadata)
                    PH.displayAlbum();
                //else the function will be called when we have metadata
            } else {
                if(x.status===403){
                    //we need to authenticate
                    PH.ajaxError(403,"getAlbum");
                } else {
                    PH.loadingAlbum='';//because the album was not actually loaded
                    if(PH.path.length)
                    	PH.path[PH.path.length-1].index=-1;
                    console.warn("WARNING: XMLHttprequest failed: "+x.statusText);
                    console.warn(x);
                    PH.setHash();
                }
            }
        }
    }; //onreadystatechange
    if(this.loadingAlbum!==''){
        if(p===this.loadingAlbum){
            console.warn("WARNING: refusing to load the same album twice ("+p+")");
            return;
        } else
            console.warn("WARNING: loading a new album before receiving the previous one");
    }
    this.loadingAlbum=p;
    x.open('GET',p,true);
    x.setRequestHeader("Content-Type", "application/json; charset=utf-8");
    x.timeout=20000; //20 seconds
    x.send(null);

    PH.setupWebsocket();
};


function countThumbnails(obj){
    var count=0,i;
    for(i=0;i<obj.length;i++)
        if(Array.isArray(obj[i]))
            count+=countThumbnails(obj[i]);
        else
            count++;
    return count;
}
function areTheSameStrip(s1,s2){
    var i;
    if(s1.length!==s2.length)return false;
    for(i=0;i<s1.length;i++)
        if(Array.isArray(s1[i])){
            if(Array.isArray(s2[i])){
                if(!areTheSameStrip(s1[i],s2[i]))
                    return false;
            } else return false; //s1 is array, s2 is not
        } else if(Array.isArray(s2[i])){
            return false; //s2 is array, s1 is not
        } else {
            //neither is array
            if(s1[i].fl!==s2[i].fl || s1[i].t!==s2[i].t)
                return false;
        }
    //we are at the end, we can return true
    return true;
}

function printThumbnails(obj){
    var nrThumbnails,thmbWidth,thmbWidthInt;
    var totalWidth=0,extraThmbWidth=0,nrPrintedThumbnails=0,txt='',queryBase;

    function printThumbnailsRecursive(obj,level){
        var i,c,tW,query,thumbClass;

        for(i=0;i<obj.length;i++){
            if(nrPrintedThumbnails===nrThumbnails)return; //we are done, no need to print more thumbnails
            
            if(Array.isArray(obj[i]))
                printThumbnailsRecursive(obj[i],level+1);
            else {
                c=obj[i];
                if(c.fl&PH.server.albumFlags.FlagRotatePlus90){
                    if(c.fl&PH.server.albumFlags.FlagRotatePlus180)c.rot=270;
                    else c.rot=90;
                } else
                    if(c.fl&PH.server.albumFlags.FlagRotatePlus180)c.rot=180;
                else c.rot=0;
                //console.log("Thumbnail URL: "+c.t);
                extraThmbWidth+=thmbWidth-thmbWidthInt;
                if(extraThmbWidth>=0.99){
                    tW=thmbWidthInt+1;
                    extraThmbWidth-=1;
                } else tW=thmbWidthInt;
                totalWidth+=1+tW;
                if(totalWidth>PH.strip.albmpictElmWidth-1){
                    tW-=(totalWidth-PH.strip.albmpictElmWidth+1);
                    console.warn("We had to adjust tW with "+(totalWidth-PH.strip.albmpictElmWidth+1)+" pixels.");
                }
                //console.log("tW="+tW+", extraThmbWidth="+extraThmbWidth);
                if(!(c.fl&PH.server.albumFlags.FlagHasScaledThumbnail)){
                    if(queryBase)query='&c=1';
                    else query='?c=1';
                } else query='';

                /* this is for checking the which thumbnails are cached and which not
                if(c.fl&PH.server.albumFlags.FlagHasCachedThumbnail)thumbClass='thmbboxCached';
                else*/
                thumbClass='thmbbox';

                txt+='<div class="'+thumbClass+'" data-bgurl="'+c.t+queryBase+query+'" style="width:'+
                        tW+'px;height:'+thmbWidthInt+'px;transform:rotate('+c.rot+'deg)">';
                if(c.fl&PH.server.albumFlags.FlagIsVideo)
                    txt+='<svg viewBox="0 0 276 276" class="overlay-videothumb"><use xlink:href="#overlay-videothumb-svg"></use></svg>';
                txt+='</div>';


                   
                nrPrintedThumbnails++;
            }
        }//for(
    }//printThumbnailsRecursive

    //create the queryBase (used for the src attribute of thumbnails)
    queryBase=PH.platform.pixelRatioTxt;
    if(PH.server.key)queryBase+='&key='+PH.server.key;
    if(PH.userSession.sessionId)queryBase+='&sid='+PH.userSession.sessionId;
    if(queryBase && queryBase[0]==='&')queryBase='?'+queryBase.slice(1);

    //console.log("queryBase="+queryBase);

    nrThumbnails=countThumbnails(obj);
    //console.log("Counted thumbnails: "+nrThumbnails);
    
    if(nrThumbnails>PH.strip.nrThumbnailsPerRow)
        nrThumbnails=PH.strip.nrThumbnailsPerRow; //we do not need all the available thumbnails
    //console.log("Adjusted nr thumbnails: "+nrThumbnails);

    if(nrThumbnails>PH.strip.nrThumbnailsPerRow*0.7)
        thmbWidth=(PH.strip.albmpictElmWidth-1)/nrThumbnails-1;
    else thmbWidth=(PH.strip.albmpictElmWidth-1)/PH.strip.nrThumbnailsPerRow/0.7-1;
    thmbWidthInt=Math.floor(thmbWidth);
    
    printThumbnailsRecursive(obj,0);
    
    //console.log("printThumbnails has printed "+nrPrintedThumbnails+" thumbnails: "+txt);
    return {txt:txt,totalWidth:totalWidth,thmbWidth:thmbWidth,thmbWidthInt:thmbWidthInt};
}


PH.showImgHelp=function(){
    console.assert(!this.userPrefsGlobal.skipImgHelp,null);
    if(!this.userSession.imgHelpConstructed){
        this.userSession.imgHelpConstructed=true;
        //show texts
        elm('imghelp_left').children[0].innerHTML=this.l10n('txt_imghelp_left');
        elm('imghelp_right').children[0].innerHTML=this.l10n('txt_imghelp_left');
        elm('imghelp_center').children[0].innerHTML=this.l10n('txt_imghelp_center');
        elm('imghelp_gotit_btn').innerHTML=this.l10n('imghelp_gotit_btn');
    }
    if(!this.userSession.imgHelpBinded){
        this.userSession.imgHelpBinded=true;
        //add bindings
        elm('imghelp').addEventListener(this.platform.clickEvent,function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            elmHide('imghelp');
        },false);
        elm('imghelp_gotit').addEventListener(this.platform.clickEvent,function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            PH.userPrefsGlobal.skipImgHelp=true;
            PH.saveGlobalPreferences();
            elmHide('imghelp');
        },false);
    }

    elmShow('imghelp');
    //if we show the imghelp we should NOT show the imginfo
    this.userPrefsGlobal.showPicInfo=0;
};

/* NOT USED
PH.getTimebar=function(parentEc,parentLc,ec,lc,showYear,width){
    var txt, l, s1, s2, s3, p1, p2, p3;
    if(parentEc===0x7FFFFFFF || parentLc===0x7FFFFFFF || ec===0x7FFFFFFF || lc===0x7FFFFFFF){
        console.log("getTimebar: some dates are missing. Cannot compute timebar.");
        return '<div class="albmpict-timebar"><div class="albmpict-timebar-out" style="width:100%;"></div></div>';
    }
    if(parentEc===parentLc){
        console.log("getTimebar: parent album has a single date, not an interval. Cannot compute timebar.");
        return '<div class="albmpict-timebar"><div class="albmpict-timebar-out" style="width:100%;"></div></div>';
    }
    console.log("getTimebar: parent between "+parentEc+" and "+parentLc+", child between "+ec+" and "+lc);
    console.assert(parentEc<parentLc);
    console.assert(ec<=lc);
    console.assert(parentEc<=ec);
    console.assert(parentLc>=lc);

    l=parentLc-parentEc;
    s1=ec-parentEc;
    s2=lc-ec;
    s3=parentLc-lc;
    console.assert(l===s1+s2+s3);
    p1=100*s1/l;
    p2=100*s2/l;
    p3=100*s3/l;
    if(p2<1){
        //minimim 1%. Substract from the bigger qualtity
        if(p1>p3)p1-=(1-p2);
        else p3-=(1-p2);
        p2=1;
    }
    //From text
    txt='<div class="albmpict-timebar"><div class="albmpict-timebar-txt-from" style="min-width:'+p1+'%">'+
            this.l10n('txt_timeline_from')+this.getFormattedLongOrShortDate(ec,showYear,width)+'</div></div>';

    //timebar
    txt+='<div class="albmpict-timebar"><div class="albmpict-timebar-out" style="width:'+p1+
            '%;"></div><div class="albmpict-timebar-in" style="width:'+p2+'%;"></div><div class="albmpict-timebar-out" style="width:'+p3+
            '%;"></div></div>';

    //Until text
    txt+='<div class="albmpict-timebar"><div class="albmpict-timebar-txt-until" style="min-width:'+p3+'%">'+
            this.l10n('txt_timeline_until')+this.getFormattedLongOrShortDate(lc,showYear,width)+'</div></div>';

    return txt;
};*/

PH.showTopTimebar=function(parentEc,parentLc,showYear,width){
    var txt, l;
    
    if(!document.getElementById('album-timebar'))return; //no timebar

    this.albumEc=parentEc;
    this.albumLc=parentLc;
    this.elementEc=null;
    this.elementLc=null;
    this.elementIndex=null;
    if(parentEc===0x7FFFFFFF || parentLc===0x7FFFFFFF){
        console.log("showTopTimebar: parent dates missing. Cannot compute timebar.");
        if(!this.platform.isTouch){
            elmHTML('album-timebar','<div class="albmpict-timebar"><div class="albmpict-timebar-txt-from">&nbsp</div></div>'+
            '<div class="albmpict-timebar"><div id="top-timebar" class="albmpict-timebar-out" style="width:100%;"></div></div>');
        } //else -> on touch we do nothing
        return;
    }
    if(parentEc===parentLc){
        console.log("showTopTimebar: parent album has a single date, not an interval. Cannot compute timebar.");
        if(!this.platform.isTouch){
            elmHTML('album-timebar','<div class="albmpict-timebar"><div class="albmpict-timebar-txt-from">&nbsp</div></div>'+
            '<div class="albmpict-timebar"><div id="top-timebar" class="albmpict-timebar-out" style="width:100%;"></div></div>');
        } //else -> on touch we do nothing
        return;
    }
    console.log("showTopTimebar: parent between "+parentEc+" and "+parentLc);
    console.assert(parentEc<parentLc);

    if(this.platform.isTouch){
        txt=this.getFormattedLongOrShortDate(parentEc,showYear,width)+' &rarr; '+this.getFormattedLongOrShortDate(parentLc,showYear,width);
    } else {

        l=parentLc-parentEc;
        //From text
        txt='<div class="albmpict-timebar"><div class="albmpict-timebar-txt-from">'+
                this.l10n('txt_timeline_from')+this.getFormattedLongOrShortDate(parentEc,showYear,width)+'</div></div>';
        //timebar
        txt+='<div class="albmpict-timebar"><div id="top-timebar" class="albmpict-timebar-out" style="width:100%;"></div></div>';

        //Until text
        txt+='<div class="albmpict-timebar"><div class="albmpict-timebar-txt-until">'+
                this.l10n('txt_timeline_until')+this.getFormattedLongOrShortDate(parentLc,showYear,width)+'</div></div>';
    }
    elmHTML('album-timebar',txt);
};

PH.showTimebarInterval=function(index,ec,lc){
    if(ec===0x7FFFFFFF || lc===0x7FFFFFFF)return;
    if(!document.getElementById('album-timebar'))return; //no timebar
    
    var l, s1, s2, s3, p1, p2, p3;
    var e=elm('top-timebar');
    var w=e.clientWidth;
    this.element2clearIndex=null;
    if(this.elementIndex===index)
        return;//already showing it

    this.elementIndex=index;
    this.elementEc=ec;
    this.elementLc=lc;

    l=this.albumLc-this.albumEc;
    s1=ec-this.albumEc;
    s2=lc-ec;
    s3=this.albumLc-lc;
    console.assert(l===s1+s2+s3);
    p1=w*s1/l;
    p2=w*s2/l;
    p3=w*s3/l;

    if(p2<8){
        //minimim 8px. Substract from the bigger quantity
        if(p1>p3)p1-=(8-p2);
        else p3-=(8-p2);
        p2=8;
    }
    if(p1<1){
        //minimim 1px. Substract from the bigger quantity
        if(p3>p2)p3-=(1-p1);
        else p2-=(1-p1);
        p1=1;
    }
    if(p3<1){
        //minimim 1px. Substract from the bigger quantity
        if(p1>p2)p1-=(1-p3);
        else p2-=(1-p3);
        p3=1;
    }

    //timebar
    e.innerHTML='<div class="albmpict-timebar-in" style="width:'+p2+'px;left:'+p1+'px"></div>';
};

PH.clearTimebarInterval=function(){
    if(this.element2clearIndex===null)return; //nothing to clear
    if(!document.getElementById('album-timebar'))return; //no timebar

    elmHTML('top-timebar','');
    this.elementIndex=null;
    this.element2clearIndex=null;
    this.elementEc=null;
    this.elementLc=null;
};

PH.getNrPicturesVideosAndAlbums=function(p,v,a,b){
    console.assert(b===' ' || b==='<br>');
    if(p===0){
        if(v===0){
            if(a===0)return this.l10n('txt_0p_0v_0a').replace('%b',b);
            else if(a===1)return this.l10n('txt_0p_0v_1a');
            else return this.l10n('txt_0p_0v_na').replace('%a',a);
        } else if(v===1){
            if(a===0)return this.l10n('txt_0p_1v_0a').replace('%b',b);
            else if(a===1)return this.l10n('txt_0p_1v_1a').replace('%b',b);
            else return this.l10n('txt_0p_1v_na').replace('%a',a).replace('%b',b);
        } else {
            if(a===0)return this.l10n('txt_0p_nv_0a').replace('%v',v).replace('%b',b);
            else if(a===1)return this.l10n('txt_0p_nv_1a').replace('%v',v).replace('%b',b);
            else return this.l10n('txt_0p_nv_na').replace('%v',v).replace('%a',a).replace('%b',b);
        }
    } else if(p===1){
        if(v===0){
            if(a===0)return this.l10n('txt_1p_0v_0a');
            else if(a===1)return this.l10n('txt_1p_0v_1a').replace('%b',b);
            else return this.l10n('txt_1p_0v_na').replace('%a',a).replace('%b',b);
        } else if(v===1){
            if(a===0)return this.l10n('txt_1p_1v_0a').replace('%b',b);
            else if(a===1)return this.l10n('txt_1p_1v_1a').replace('%b',b);
            else return this.l10n('txt_1p_1v_na').replace('%a',a).replace('%b',b);
        } else {
            if(a===0)return this.l10n('txt_1p_nv_0a').replace('%v',v).replace('%b',b);
            else if(a===1)return this.l10n('txt_1p_nv_1a').replace('%v',v).replace('%b',b);
            else return this.l10n('txt_1p_nv_na').replace('%v',v).replace('%a',a).replace('%b',b);
        }
    } else {
        if(v===0){
            if(a===0)return this.l10n('txt_np_0v_0a').replace('%p',p);
            else if(a===1)return this.l10n('txt_np_0v_1a').replace('%p',p).replace('%b',b);
            else return this.l10n('txt_np_0v_na').replace('%p',p).replace('%a',a).replace('%b',b);
        } else if(v===1){
            if(a===0)return this.l10n('txt_np_1v_0a').replace('%p',p).replace('%b',b);
            else if(a===1)return this.l10n('txt_np_1v_1a').replace('%p',p).replace('%b',b);
            else return this.l10n('txt_np_1v_na').replace('%p',p).replace('%a',a).replace('%b',b);
        } else {
            if(a===0)return this.l10n('txt_np_nv_0a').replace('%p',p).replace('%v',v).replace('%b',b);
            else if(a===1)return this.l10n('txt_np_nv_1a').replace('%p',p).replace('%v',v).replace('%b',b);
            else return this.l10n('txt_np_nv_na').replace('%p',p).replace('%v',v).replace('%a',a).replace('%b',b);
        }
    }
};

PH.displayAlbum=function(preservePosition) {
    //is there anything to show?
    if(this.path.length<1)return;
    var albumData=this.path[this.path.length-1];
    var cmdSent=false;
    var nontouchClass=this.platform.isTouch?'':' nontouch';
    var txt2fullrow='',txt2='',txt='',txtElm='',tooltip;
    var i,e,c,iniI=0,j,w,p,tbe; //tbe=thumbbar element
    var ec,lc,cd,tt,mb,mp,cm;
    var startPicIndex=-1,showYear;
    var pathStr='/t/';
    var extraThmbWidth=0,totalWidth=0,nrPicThumbsInThisRow=0,tW;
    //var emptySubalbums;
    var albumTitle,pictureDisplayNr;
    var thmbsObj;
    var query,queryBase;
    var preservePositionI=-1,preservePositionDiff=-1;
    var albmPictStyle;
    
    if(typeof(preservePosition)==='undefined')
        preservePosition=false;
    
    if(preservePosition){
        e=elm('cntnt');
        for(i=0;i<e.children.length;i++){
            if(i%2)continue;
            if(preservePositionI<0 && e.children[i].offsetTop>e.scrollTop){
                //console.log("This ("+i+") is the first visible element");
                preservePositionI=i;
                preservePositionDiff=e.children[i].offsetTop-e.scrollTop;
            }
        }
    }
    
    console.log(albumData);
    if(PH.path.length<1)
        throw new Error("PH.path.length==="+PH.path.length);

    //make sure we are displaying the right element
    console.assert(typeof(albumData)!=='undefined');
    console.assert(typeof(albumData.a)!=='undefined');
    console.assert(typeof(albumData.a.l)==='number');
    if(albumData.a.l!==this.path.length-1){
        console.error("displayAlbum: We are displaying an element with wrong level ("+albumData.a.l+"), our path has "+this.path.length+
                " elements. We ignore the element (and verify the path).");

        for(i=0;i<this.path.length;i++){
            if(this.path[i].a.l!==i){
                console.error("Wrong element (level: "+this.path[i].a.l+") in path at position "+i+". Purging the rest of the path.");
                while(this.path.length>i)this.path.pop();
                if(this.path.length){
                    if(this.server.metadata)
                        this.displayAlbum();
                } else
                    this.getAlbum();
                break;
            }//if
        }//for
        return; //nothing else should be done in this function
    }
                    
    console.log("displayAlbum: This album has "+albumData.e.length+" elements. Flags: "+albumData.a.fl+", Path length: "+this.path.length);
    //console.log(albumData);

    //is this an album in progress?
    if(albumData.a.fl&this.server.albumFlags.FlagAlbumIsBeingScanned){
        console.log("Album is scanned right now.");
        elm('menu').className='progress';
    } else 
        elm('menu').className='';
    
    //elmShow('albmdiv'); -> should not be needed. Always showing.  

    //build the path
    console.assert(this.path.length>0);
    tooltip=this.l10n('txt_go_back');
    if(this.platform.isTouch){
        //we want a single element here
        if(this.path.length>1)
            txt='<div class="path-elm-touch" data-index="'+(this.path.length-2)+'" title="'+tooltip+'">&lt;</div>';
    } else {
        for(i=0;i<this.path.length-1;i++){
            tt=this.path[i].a.t.substr(this.path[i].a.no);
            if(tt.length)
                txt+='<div class="path-elm-nontouch" data-index="'+i+'" title="'+tooltip+'">'+tt+'</div>';
            else //replace p[0] with something
                txt='<div class="path-elm-nontouch" data-index="0" title="'+tooltip+'">Photostovis</div>';
        }
    }

    elmHTML('path',txt);

    //bind the path
    function pathElmClicked(ev){
        if(cmdSent)return;//so we do not send a command twice
        ev.preventDefault();
        ev.stopPropagation();
        var pathLength=parseInt(this.attributes['data-index'].value,10);
        while(PH.path.length>pathLength+1){
            PH.path.pop();
        }
        console.assert(PH.path.length>0,PH.path.length);
        PH.path[PH.path.length-1].index=-1;
        PH.setHash();
    }

    e=elm('path');
    if(this.platform.isTouch){
        if(this.path.length>1)
            e.children[0].addEventListener(this.platform.clickEvent,pathElmClicked,false);
    } else {
        for(i=0;i<this.path.length-1;i++){
            e.children[i].addEventListener(this.platform.clickEvent,pathElmClicked,false);
        };
    }



    //album description
    ec=new Date(albumData.a.ec*1000);
    lc=new Date(albumData.a.lc*1000);
    console.assert(this.path.length>=1);

    //set showYear
    if(ec.getFullYear()===lc.getFullYear())showYear=false;
    else showYear=true;
    console.log("showYear: "+showYear);

    //album name
    console.assert(typeof(albumData.a.t)==='string');
    if(albumData.a.t.length){
        tt=albumData.a.t.substr(albumData.a.no);
        /*
        if(typeof(albumElm.tt)!=='undefined' && typeof(albumElm.tt[this.userPrefsGlobal.language])!=='undefined')
            tt=albumElm.tt[this.userPrefsGlobal.language]+' ('+tt+')';
         */
    } else {
        //root photostovis
        if(typeof(this.server.metadata.un)!=='undefined')
            tt=this.l10n('txt_photostovis_root_named').replace('%s',this.server.metadata.un);
        else
            tt=this.l10n('txt_photostovis_root');
    }

    //do we have empty sub-albums? If yes, how many?
    //emptySubalbums=0;
    for(i=0;i<albumData.e.length;i++){
        c=albumData.e[i];
        if(typeof(c.t)!=='undefined'){
            //this is an album
            console.assert(typeof(c.p)==='number',typeof(c.p));
            console.assert(typeof(c.v)==='number',typeof(c.v));
            //if(c.p+c.v===0)emptySubalbums++; //this is an empty sub-album
        }
    }
            

    elmHTML('album-title',tt);
    elmHTML('album-picsnalbms',this.getNrPicturesVideosAndAlbums(albumData.a.p,albumData.a.v,albumData.a.a/*-emptySubalbums*/,' '));
    //timebar
    w=elm('menu').clientWidth*0.9;
    this.showTopTimebar(albumData.a.ec,albumData.a.lc,true,w);

    //albums & pictures
    txt='';
    for(i=0;i<this.path.length-1;i++){
	    console.assert(typeof(this.path[i])==='object' && this.path[i]);
	    console.assert(typeof(this.path[i].index)==='number',this.path[i]);
	    console.assert(this.path[i].index>=0 && this.path[i].index<this.path[i].e.length,this.path[i]);
	    pathStr+=this.path[i].e[this.path[i].index].id+'/';
    }
        

    //create the queryBase (used for the src attribute of thumbnails)
    queryBase=this.platform.pixelRatioTxt;
    if(this.server.key)queryBase+='&key='+this.server.key;
    if(this.userSession.sessionId)queryBase+='&sid='+this.userSession.sessionId;
    if(queryBase && queryBase[0]==='&')queryBase='?'+queryBase.slice(1);

    albumTitle=tt;
    pictureDisplayNr=0;

    //create the albmPictStyle
    albmPictStyle='width:'+this.strip.albmpictElmWidth+'px';
    if(PH.server.embedded)
        albmPictStyle+=';margin:0';

    for(i=0;i<albumData.e.length;i++){
        c=albumData.e[i];

        if(typeof(c.t)!=='undefined'){
            //this is an album
            console.assert(typeof(c.p)==='number',typeof(c.p));
            console.assert(typeof(c.v)==='number',typeof(c.v));
            //if(c.p+c.v===0)continue; //this is an empty sub-album -> we show empty subalbums so that they can be created e.g. from Upload command

            if(startPicIndex>=0){
                txtElm='<div class="albmpict-elm" style="'+albmPictStyle+'">';
                if(txt2fullrow!=='')
                    txtElm+='<div class="thmbbar" style="background-size:'+(this.strip.picThmbWidthInt+2)+'px '+(this.strip.picThmbWidthInt+1)+'px">'+
                        txt2fullrow+'</div>';
                if(txt2!=='')
                    txtElm+='<div class="thmbbar" style="background-size:'+(this.strip.picThmbWidthInt+2)+'px '+(this.strip.picThmbWidthInt+1)+'px;width:'+
                        (totalWidth+1)+'px">'+txt2+'</div>';
                txtElm+='</div><div class="clearance"></div>';
                //add in front
                txt=txtElm+txt;
                
                
                txt2='';
                txt2fullrow='';
                startPicIndex=-1;
                extraThmbWidth=0;
                totalWidth=0;
                nrPicThumbsInThisRow=0;
            }
            txtElm='<div class="albmpict-elm albm-elm'+nontouchClass+'" data-index="'+i+
                    '" style="'+albmPictStyle+'"><div class="albmpict-header'+
                    (c.fl&this.server.albumFlags.FlagAlbumIsBeingScanned?' progress':'')+'">';


            //title bar
            //title creation first
            if(typeof(c.tt)!=='undefined' && typeof(c.tt[this.userPrefsGlobal.language])!=='undefined')tt=c.tt[this.userPrefsGlobal.language]+' ('+c.t+')';
            else {
                if(typeof(c.no)!=='undefined')tt=c.t.substr(c.no);
                else tt=c.t;
            }
            if(this.platform.isTouch){
                txtElm+='<div class="albmpict-title-touch">'+tt+'</div>';

                //from when till when
                if(c.ec!==0x7FFFFFFF)
                    txtElm+='<div class="albmpict-descr-left-touch">'+
                            this.getFormattedLongOrShortDate(c.ec,showYear,w)+' &rarr; '+this.getFormattedLongOrShortDate(c.lc,showYear,w)+'</div>\n';
                //nr of pictures and albums
                txtElm+='<div class="albmpict-descr-right-touch">'+this.getNrPicturesVideosAndAlbums(c.p,c.v,c.a,' ')+'</div>\n</div>\n';
            } else {
                txtElm+='<div class="albmpict-title">'+tt+'</div>';

                //from when till when
                if(c.ec!==0x7FFFFFFF)
                    txtElm+='<div class="albmpict-descr-left">'+
                            this.l10n('txt_timeline_from')+this.getFormattedLongOrShortDate(c.ec,showYear,w)+'<br>'+
                            this.l10n('txt_timeline_until')+this.getFormattedLongOrShortDate(c.lc,showYear,w)+'</div>\n';
                //nr of pictures and albums
                txtElm+='<div class="albmpict-descr-right">'+this.getNrPicturesVideosAndAlbums(c.p,c.v,c.a,'<br>')+'</div>\n</div>\n';
            }
           
            //thumbnails
            if(typeof(c.str)!=='object')c.str=[];
            thmbsObj=printThumbnails(c.str);
            txtElm+='<div class="thmbbar" style="width:'+(thmbsObj.totalWidth+1)+'px; background-size:'+
                    thmbsObj.thmbWidth+'px '+thmbsObj.thmbWidthInt+'px">'+thmbsObj.txt+'</div>\n';

            //do we have description? If yes, add it
            if(typeof(c.d)!=='undefined' && typeof(c.d[this.userPrefsGlobal.language])!=='undefined'){
                txtElm+='<div class="albmpict-descr">'+c.d[this.userPrefsGlobal.language]+'</div>';
            }
            
            //end of the element
            txtElm+='</div><div class="clearance"></div>\n';
            //add this element in front
            txt=txtElm+txt;
        } else {
            //this is a picture
            if(startPicIndex<0)startPicIndex=i;
            pictureDisplayNr++;
            c.displayNr=pictureDisplayNr;
            
            if(c.fl&this.server.albumFlags.FlagRotatePlus90){
                if(c.fl&this.server.albumFlags.FlagRotatePlus180)c.rot=270;
                else c.rot=90;
            } else
                if(c.fl&this.server.albumFlags.FlagRotatePlus180)c.rot=180;
            else c.rot=0;
        

            //prepare the thumbnail width
            extraThmbWidth+=this.strip.picThmbWidth-this.strip.picThmbWidthInt;
            if(extraThmbWidth>=0.99){
                tW=this.strip.picThmbWidthInt+1;
                extraThmbWidth-=1;
            } else tW=this.strip.picThmbWidthInt;
            totalWidth+=1+tW;
            if(totalWidth>this.strip.albmpictElmWidth-1){
                tW-=(totalWidth-this.strip.albmpictElmWidth+1);
                console.warn("We had to adjust tW with "+(totalWidth-this.strip.albmpictElmWidth+1)+" pixels.");
            }
            //thumbnail
            if(!(c.fl&this.server.albumFlags.FlagHasScaledThumbnail)){
                if(queryBase)query='&c=1';
                else query='?c=1';
            } else query='';

            txt2+='<div data-index="'+i+'" class="thmbbox pict-elm'+nontouchClass+'" data-bgurl="'+pathStr+c.id+queryBase+query+
                    '" style="transform:rotate('+c.rot+'deg);width:'+tW+'px;height:'+this.strip.picThmbWidthInt+'px"';

            if(!this.platform.isTouch)
                txt2+=' onmouseover="this.children[0].style.display=\'block\'" onmouseout="this.children[0].style.display=\'none\'">';
            else txt2+='>';

            //technical details
            cd=new Date(c.c*1000);
            
            //if we have a video, show duration, if we have a picture, show size
            if(c.fl&this.server.albumFlags.FlagIsVideo && c.dur>0){
		        //this is a video with duration
		        mb=getShortestTimeFormat(c.dur);
		    } else {
			    //this is a picture or video without duration
			    if(c.fl&PH.server.albumFlags.FlagSizeIsInMB){
	                if(c.sz>=10240)mb=(c.sz/1024).toFixed(1)+' GB';
	                else if(c.sz>=1024)mb=(c.sz/1024).toFixed(2)+' GB';
	                else mb=c.sz.toFixed(0)+' MB';
	            } else {
	                if(c.sz>=1073741824)//1GB
	                    mb=(c.sz/1073741824).toFixed(2)+' GB';
	                else if(c.sz>=104857600)//100MB
	                    mb=(c.sz/1048576).toFixed(0)+' MB';
	                else if(c.sz>=1048576)//1MB
	                    mb=(c.sz/1048576).toFixed(1)+' MB';
	                else //less than 1MB
	                    mb=(c.sz/1024).toFixed(0)+' KB';
	            }
		    }
            
            if(c.w>0 && c.h>0){
	            if(c.fl&this.server.albumFlags.FlagIsVideo){
		            //this is a video
		            if((c.w===3840 && c.h===2160) || (c.h===3840 && c.w===2160))
		            	mp='4K / ';
		            else if((c.w===1920 && c.h===1080) || (c.h===1920 && c.w===1080))
		            	mp='1080p / ';
		            else if((c.w===1280 && c.h===720) || (c.h===1280 && c.w===720))
		            	mp='720p / ';
		            else mp=c.w+'x'+c.h+' / ';
		            
	            } else {
		            //this is an image
		            mp=c.w*c.h/1000000;
					mp=mp.toFixed(1)+' Mpix / ';
	            }
            } else mp='';

            /*if(c.fl&this.server.albumFlags.FlagHasThumbnail)*/
                txt2+='<div class="techdet" style="transform:rotate('+(-c.rot)+'deg)">';
            /*else
                txt2+='<div class="techdet">';*/
            //capture date and time
            if(c.c!==0x7FFFFFFF){
                txt2+='<div class="techdet-txt techdet-txt-top">'+this.getFormattedShortDate(cd,showYear)+'</div>';
                txt2+='<div class="techdet-txt" style="top:18px">'+this.getFormattedTime(cd)+'</div>';
            }
            //size and Mpixels
            txt2+='<div class="techdet-txt" style="bottom:18px">'+mp+mb+'</div>';
            //camera model
            if(c.m!==0xFFFF && this.server.metadata.m.length>c.m) //good to check the length, during scanning it COULD be possible that we have not received info about this camera yet
                cm=this.server.metadata.m[c.m].c;
            else cm='';
            txt2+='<div class="techdet-txt techdet-txt-bottom">'+cm+'</div></div>';
            if(c.fl&this.server.albumFlags.FlagIsVideo){
                //add a "play button" overlay on top of the thumb
                txt2+='<svg viewBox="0 0 276 276" class="overlay-videothumb"><use xlink:href="#overlay-videothumb-svg"></use></svg>';
            }
            txt2+='</div>';
            
            nrPicThumbsInThisRow++;
            if(nrPicThumbsInThisRow===this.strip.nrThumbnailsPerRow){
                //reset some of the variables
                extraThmbWidth=0;
                totalWidth=0;
                nrPicThumbsInThisRow=0;
                //move txt2 to fullrow
                txt2fullrow+=txt2;
                txt2='';
            }
        }

    }
    if(startPicIndex>=0){
        txtElm='<div class="albmpict-elm" style="'+albmPictStyle+'">';
        if(txt2fullrow!=='')
            txtElm+='<div class="thmbbar" style="background-size:'+(this.strip.picThmbWidthInt+2)+'px '+(this.strip.picThmbWidthInt+1)+'px">'+txt2fullrow+'</div>';
        if(txt2!=='')
            txtElm+='<div class="thmbbar" style="background-size:'+(this.strip.picThmbWidthInt+2)+'px '+(this.strip.picThmbWidthInt+1)+'px;width:'+
                (totalWidth+1)+'px">'+txt2+'</div>';
        txtElm+='</div><div class="clearance"></div>';
        //add this element in front
        txt=txtElm+txt;
       
        txt2='';
        txt2fullrow='';
        startPicIndex=-1;
    }

    //assign
    e=elm('cntnt');
    /*
    if(preservePosition){
        deltaHeight=e.scrollHeight;
        this.path[this.path.length-1].scrollTop=e.scrollTop;
    }*/
    e.innerHTML=txt;
    /*
    if(preservePosition && this.path[this.path.length-1].scrollTop>50){
        deltaHeight=e.scrollHeight-deltaHeight;
    } else deltaHeight=0;
    e.scrollTop=this.path[this.path.length-1].scrollTop+deltaHeight;
    */
    if(!preservePosition)
        e.scrollTop=this.path[this.path.length-1].scrollTop;
    

    this.albumCache.init(albumData.e,pathStr,albumTitle,pictureDisplayNr);

    //bind stuff
    j=-1;
    //w=e.children[iniI].children[0].clientWidth;

    for(i=albumData.e.length-1;i>=0;i--){
        c=albumData.e[i];
        if(preservePosition && iniI===preservePositionI){
            //console.log("scrollTop is now "+e.scrollTop);
            e.scrollTop=e.children[iniI].offsetTop-preservePositionDiff;
            //console.log("scrollTop was set to "+e.scrollTop+" ("+e.children[iniI].offsetTop+"-"+preservePositionDiff+")");
        }
        //console.log(c);

        if(typeof(c.t)!=='undefined'){
            //this is an album
            //if(c.p+c.v===0)continue; //this is an empty sub-album

            e.children[iniI].addEventListener('click',function(ev){//-> if touchend then any page scroll ends with going into the album
                if(cmdSent)return;//so we do not send a command twice
                ev.preventDefault();
                ev.stopPropagation();
                //save current position
                if(PH.path.length){
	                PH.path[PH.path.length-1].scrollTop=elm('cntnt').scrollTop;
	                console.log("Saving Scroll top: "+PH.path[PH.path.length-1].scrollTop+" (for "+(PH.path.length-1)+")");
	                PH.path[PH.path.length-1].index=parseInt(this.attributes['data-index'].value,10);
                }
                PH.setHash();
            },false);
            if(!this.platform.isTouch){
                e.children[iniI].addEventListener('mouseover',function(){
                    var v=parseInt(this.attributes['data-index'].value,10);
                    if(PH.path.length>0 && v<PH.path[PH.path.length-1].e.length){
	                    var c=PH.path[PH.path.length-1].e[v];
						PH.showTimebarInterval(v,c.ec,c.lc);
                    }
                },false);

                e.children[iniI].addEventListener('mouseout',function(){
                    var v=parseInt(this.attributes['data-index'].value,10);
                    PH.element2clearIndex=v;
                    setTimeout(function(){
                        PH.clearTimebarInterval();
                    },100);
                },false);
            }

            iniI+=2;
        } else {
            //picture
            //identify the picture
            tbe=e.children[iniI].children[0].children;
            if(j<0){
                j=tbe.length;
                if(typeof(e.children[iniI].children[1])!=='undefined')
                    j+=e.children[iniI].children[1].children.length;
                j--;
            }
            if(j>=tbe.length)//we are in the second child
                p=e.children[iniI].children[1].children[j-tbe.length];
            else 
                p=tbe[j];
            console.assert(i===parseInt(p.attributes['data-index'].value,10));
            
            
           
            p.addEventListener('click',function(ev){//-> if touchend then any page scroll ends with going into the album
                ev.preventDefault();
                ev.stopPropagation();
                //save current position
                if(PH.path.length){
	                PH.path[PH.path.length-1].scrollTop=elm('cntnt').scrollTop;
	                console.log("Saving Scroll top: "+PH.path[PH.path.length-1].scrollTop+" (for "+(PH.path.length-1)+")");
	                PH.path[PH.path.length-1].index=parseInt(this.attributes['data-index'].value,10);
                }
                //enter full-screen mode (we can only do this from a user-generated event, such as click
                elmShow('fullscreendiv');
                PH.enterFullScreen();
                setTimeout(function(){
                    elmShow('imgdiv');
                    if(!PH.userPrefsGlobal.skipImgHelp && !PH.userSession.showcase)
                        PH.showImgHelp();

                    //show the image
                    PH.setHash();
                },100);
                
                //albumCache.displayMedia(parseInt(this.attributes['data-index'].value,10));
            },false);
            if(!this.platform.isTouch)
                p.addEventListener('mouseover',function(){
                    var v=parseInt(this.attributes['data-index'].value,10);
                    if(PH.path.length>0 && v<PH.path[PH.path.length-1].e.length){
	                    var c=PH.path[PH.path.length-1].e[v];
						PH.showTimebarInterval(v,c.c,c.c);
                    }
                },false);

            if(!this.platform.isTouch)
                p.addEventListener('mouseout',function(){
                    var v=parseInt(this.attributes['data-index'].value,10);
                    PH.element2clearIndex=v;
                    setTimeout(function(){
                        PH.clearTimebarInterval();
                    },100);
                },false);

            j--;
            if(j<0)
                iniI+=2;
        }
    }

    this.loadingThumbnails=[];
    this.skipInvisible=true;
    this.loadingThumbnailsStartTime=Date.now();
    this.currentScrollTop=e.scrollTop;
    this.loadThumbnails();
    //done
    console.log("displayAlbum: end");
};



PH.loadThisThumbnail=function(p){
    //load this thumbnail
    var img=new Image();
    var url=p.attributes['data-bgurl'].value;
    //console.log("Loading thumbnail: "+url);
    p.removeAttribute('data-bgurl');
    img.onload=function(){
        var index=PH.loadingThumbnails.indexOf(this);
        if(index!==-1){
            p.style.backgroundImage="url('"+url+"')";
            //p.removeAttribute('data-bgurl');
            PH.loadingThumbnails.splice(index,1);
            //if(PH.loadingThumbnails.length===0){
            if(PH.loadingThumbnails.length<PH.strip.nrThumbnailsToLoad){
                setTimeout(function(){
                    PH.loadThumbnails();
                },0);
            }
        }
    }//onload
    img.onerror=function(){
        console.warn("onerror triggered on thumbnail: "+url+", reloading this album.");
        PH.getAlbum(true);
        //remove this thumbnail from the queue
        var index=PH.loadingThumbnails.indexOf(this);
        if(index!==-1){
            PH.loadingThumbnails.splice(index,1);
            //if(PH.loadingThumbnails.length===0){
            if(PH.loadingThumbnails.length<PH.strip.nrThumbnailsToLoad){
                setTimeout(function(){
                    PH.loadThumbnails();
                },0);
            }
        }
    }//onerror

    this.loadingThumbnails.push(img);
    img.src=url;
};

PH.loadPicturesInThumbbar=function(thmbbar,e){
    var j,p;
    if(this.skipInvisible){
        //we need to check if this element is visible at all
        if(thmbbar.offsetTop+thmbbar.clientHeight<e.scrollTop){
            //console.log("thmbbar completely hidden in the upper part)");
            return false;
        }
        if(thmbbar.offsetTop>e.scrollTop+e.clientHeight){
            //console.log("thmbbar completely hidden in the lower part)");
            return false;
        }
    }

    for(j=0;j<thmbbar.children.length;j++){
        p=thmbbar.children[j];
        if(typeof(p.attributes['data-bgurl'])==='undefined')
            continue; //already loaded

        if(this.skipInvisible){
            //we need to check if this element is visible at all
            if(p.offsetTop+p.clientHeight<e.scrollTop){
                /*console.log("thumbnail "+(j+1)+"/"+thmbbar.children.length+" completely hidden in the upper part. offsetTop="+p.offsetTop+", clientHeight="+p.clientHeight+
                            ", e.scrollTop="+e.scrollTop);*/
                continue;
            }
            if(p.offsetTop>e.scrollTop+e.clientHeight){
                /*console.log("thumbnail "+(j+1)+"/"+thmbbar.children.length+" completely hidden in the lower part. offsetTop="+p.offsetTop+", e.clientHeight="+e.clientHeight);*/
                continue;
            }
        }

        //if here, we can load the thumbnail
        this.loadThisThumbnail(p);
        if(this.loadingThumbnails.length>=this.strip.nrThumbnailsToLoad)
            return true;
    }
    return false;
}

PH.loadThumbnails=function(){
    //console.log("loadThumbnails++ (skipping invisible: "+this.skipInvisible+")");
    var e=elm('cntnt');
    var i,ii;
    var t;

    if(elm('fullscreendiv').style.display==='block'){
        //we are in image mode, stop loading stuff
        console.log("We are now in image display mode, quit loading thumbnails");
        return;
    }

    if(e.scrollTop!==this.currentScrollTop && !this.skipInvisible){
        this.skipInvisible=true;
        this.currentScrollTop=e.scrollTop;
        console.log("switching back to skipInvisible=true");
    }

    for(i=0;i<e.children.length;i+=2){
        if(typeof(e.children[i].attributes['data-index'])!=='undefined'){
            //this is an album
            if(this.loadPicturesInThumbbar(e.children[i].children[1],e))
                return;

        } else {
            //this is picture area. It usually contains 2 thumbbars
            for(ii=0;ii<e.children[i].children.length;ii++){
                if(this.loadPicturesInThumbbar(e.children[i].children[ii],e))
                    return;
            }
        }
    }

    //console.log("loadThumbnails-- "+this.loadingThumbnails.length);

    if(this.loadingThumbnails.length===0 && this.loadingThumbnailsStartTime>0){
        if(this.skipInvisible){
            this.skipInvisible=false;
            setTimeout(function(){
                PH.loadThumbnails();
            },0);
        } else {
            //this.loadingThumbnailsStartTime=Date.now()-this.loadingThumbnailsStartTime;
            t=Date.now()-this.loadingThumbnailsStartTime;
            console.log("Thumbnails finished downloading in "+t+"ms");
            this.loadingThumbnailsStartTime=0;
            //we finished loaded thumbnails
            this.sendWebsocketMessage("thumbnails loaded");
        }
    }
}



PH.reDisplayAlbum=function(newAlbumData) {
    //is there anything to show?
    if(this.path.length<1)return;
    var oldAlbumData=this.path[this.path.length-1];
    var albumElm=null;
    var nontouchClass=this.platform.isTouch?'':' nontouch';
    var txt2='';
    var i,e,c,co,iniI=0,j,w,p,tbe; //tbe=thumbbar element
    var ec,lc,cd,tt,mb,mp,cm;
    var showYear;
    var pathStr='/t/';
    //var newEmptySubalbums,oldEmptySubalbums;
    var albumTitle,pictureDisplayNr;
    var thmbsObj;
    var counter,query,queryBase;

    //console.log(newAlbumData);
    console.log("reDisplayAlbum: This album has "+newAlbumData.e.length+" elements. Flags: "+newAlbumData.a.fl+", Path length: "+this.path.length);
    //is this an album in progress?
    if(newAlbumData.a.fl&this.server.albumFlags.FlagAlbumIsBeingScanned){
        console.log("Album is scanned right now.");
        elm('menu').className='progress';
    } else 
        elm('menu').className='';
    
    console.assert(newAlbumData.e.length===oldAlbumData.e.length,"new and old e.length do not match");
    
    //elmShow('albmdiv'); -> should not be needed. Always showing.  

    //THE PATH SHOULD BE THE SAME, NO NEED TO REBUILD IT

    //album description
    ec=new Date(newAlbumData.a.ec*1000);
    lc=new Date(newAlbumData.a.lc*1000);
    console.assert(this.path.length>=1);

    //set showYear
    if(ec.getFullYear()===lc.getFullYear())showYear=false;
    else showYear=true;

    if(this.path.length===1){
        //root photostovis 
        if(typeof(this.server.metadata.un)!=='undefined')
            tt=this.l10n('txt_photostovis_root_named').replace('%s',this.server.metadata.un);
        else
            tt=this.l10n('txt_photostovis_root');
    } else {
        console.assert(this.path.length>1);
        console.assert(typeof(newAlbumData.elm)==='undefined');
        console.assert(typeof(oldAlbumData.elm)==='undefined');
        i=this.path.length-1;
        console.log("i="+i+", index="+this.path[i-1].index);
        albumElm=this.path[i-1].e[this.path[i-1].index];

        if(typeof(albumElm.no)!=='undefined')
            tt=albumElm.t.substr(albumElm.no);
        else tt=albumElm.t;

        if(typeof(albumElm.tt)!=='undefined' && typeof(albumElm.tt[this.userPrefsGlobal.language])!=='undefined')
            tt=albumElm.tt[this.userPrefsGlobal.language]+' ('+tt+')';
    }
    
    //do we have empty sub-albums? If yes, how many?
    //newEmptySubalbums=0;
    //oldEmptySubalbums=0;
    
    for(i=0; i<newAlbumData.e.length; i++){
        c=newAlbumData.e[i];
        co=oldAlbumData.e[i];
        if(typeof(c.t)!=='undefined'){
            //this is an album
            console.assert(typeof(c.p)==='number',typeof(c.p));
            console.assert(typeof(c.v)==='number',typeof(c.v));
            //if(c.p+c.v===0)newEmptySubalbums++; //this is an empty sub-album
        }
        if(typeof(co.t)!=='undefined'){
            //this is an album
            console.assert(typeof(co.p)==='number',typeof(co.p));
            console.assert(typeof(co.v)==='number',typeof(co.v));
            //if(co.p+co.v===0)oldEmptySubalbums++; //this is an empty sub-album
        }
    }

    elmHTML('album-title',tt); //this could be the same, but we do not bother
    if(newAlbumData.a.p!==oldAlbumData.a.p || newAlbumData.a.v!==oldAlbumData.a.v || 
            (newAlbumData.a.a/*-newEmptySubalbums*/)!==(oldAlbumData.a.a/*-oldEmptySubalbums*/))
        elmHTML('album-picsnalbms',this.getNrPicturesVideosAndAlbums(newAlbumData.a.p,newAlbumData.a.v,newAlbumData.a.a/*-newEmptySubalbums*/,' '));
    //timebar
    w=elm('menu').clientWidth*0.9;
    if(newAlbumData.a.ec!==oldAlbumData.a.ec || newAlbumData.a.lc!==oldAlbumData.a.lc){
        console.log("albmdescr width="+w);
        this.showTopTimebar(newAlbumData.a.ec,newAlbumData.a.lc,true,w);
    }
    
    //albums & pictures
    for(i=0;i<this.path.length-1;i++)
        pathStr+=this.path[i].e[this.path[i].index].id+'/';

    //create the queryBase (used for the src attribute of thumbnails)
    queryBase=this.platform.pixelRatioTxt;
    if(this.server.key)queryBase+='&key='+this.server.key;
    if(this.userSession.sessionId)queryBase+='&sid='+this.userSession.sessionId;
    if(queryBase && queryBase[0]==='&')queryBase='?'+queryBase.slice(1);

    albumTitle=tt;
    pictureDisplayNr=0;
    
    e=elm('cntnt');

    //change stuff
    j=-1;
    for(i=newAlbumData.e.length-1;i>=0;i--){
        c=newAlbumData.e[i];
        co=oldAlbumData.e[i];
        
        if(typeof(c.t)!=='undefined'){
            //this is an album
            /*
            if(c.p+c.v===0){
                console.assert(co.p+co.v===0);
                continue;
            } //this is an empty sub-album
            */
            
            //if c and co are the same, we skip this child
            if(co.id===c.id && co.fl===c.fl && co.mt===c.mt && c.ec===co.ec && c.lc===co.lc){
                //they are the same
                iniI+=2;
                continue;
            }
            
            //!!! It seems there is a problem here
            if(typeof(e.children[iniI])==='undefined')
            	throw new Error('e.children[iniI] is undefined. iniI='+iniI+', e.children.length='+e.children.length);
            console.assert(typeof(e.children[iniI])!=='undefined');
            console.assert(typeof(e.children[iniI].children[0])!=='undefined');
            console.assert(typeof(e.children[iniI].children[1])!=='undefined');
            console.assert(i===parseInt(e.children[iniI].attributes['data-index'].value,10));
            
            //if here, we need to update this child (which is an album)
            //is the class name right?
            if(c.fl&this.server.albumFlags.FlagAlbumIsBeingScanned){
                if(e.children[iniI].children[0].className!=='albmpict-header progress')
                    e.children[iniI].children[0].className='albmpict-header progress';
            } else {
                if(e.children[iniI].children[0].className!=='albmpict-header')
                    e.children[iniI].children[0].className='albmpict-header';
            }
                
            //title bar (child 0)
            //title creation first
            if(typeof(c.tt)!=='undefined' && typeof(c.tt[this.userPrefsGlobal.language])!=='undefined')tt=c.tt[this.userPrefsGlobal.language]+' ('+c.t+')';
            else {
                if(typeof(c.no)!=='undefined')tt=c.t.substr(c.no);
                else tt=c.t;
            }
            if(this.platform.isTouch){
                txt2='<div class="albmpict-title-touch">'+tt+'</div>';

                //from when till when
                if(c.ec!==0x7FFFFFFF)
                    txt2+='<div class="albmpict-descr-left-touch">'+
                            this.getFormattedLongOrShortDate(c.ec,showYear,w)+' &rarr; '+this.getFormattedLongOrShortDate(c.lc,showYear,w)+'</div>\n';
                //nr of pictures and albums
                txt2+='<div class="albmpict-descr-right-touch">'+this.getNrPicturesVideosAndAlbums(c.p,c.v,c.a,' ')+'</div>\n</div>\n';
            } else {
                txt2='<div class="albmpict-title">'+tt+'</div>';

                //from when till when
                if(c.ec!==0x7FFFFFFF)
                    txt2+='<div class="albmpict-descr-left">'+
                          this.l10n('txt_timeline_from')+this.getFormattedLongOrShortDate(c.ec,showYear,w)+'<br>'+
                          this.l10n('txt_timeline_until')+this.getFormattedLongOrShortDate(c.lc,showYear,w)+'</div>\n';
                //nr of pictures and albums
                txt2+='<div class="albmpict-descr-right">'+this.getNrPicturesVideosAndAlbums(c.p,c.v,c.a,'<br>')+'</div>\n</div>\n';
            }
            e.children[iniI].children[0].innerHTML=txt2;
            txt2=null;

            //thumbnails
            if(typeof(c.str)==='object'){
                if(typeof(co.str)!=='object')co.str=[];
                if(!areTheSameStrip(co.str,c.str)){
                    //console.log("Strips are not the same for i="+i);
                    //console.log(c.str);
                    //console.log(co.str);
                    //strips are not the same. Replace the old strip
                    thmbsObj=printThumbnails(c.str);

                    e.children[iniI].children[1].style.width=(thmbsObj.totalWidth+1)+"px";
                    e.children[iniI].children[1].style.backgroundSize=thmbsObj.thmbWidth+"px "+thmbsObj.thmbWidthInt+"px";
                    e.children[iniI].children[1].innerHTML=thmbsObj.txt;
                }
            }

            iniI+=2;
        } else {
            //picture
            c.displayNr=co.displayNr;
            pictureDisplayNr++;
            
            //identify the picture
            tbe=e.children[iniI].children[0].children;
            if(j<0){
                j=tbe.length;
                if(typeof(e.children[iniI].children[1])!=='undefined')
                    j+=e.children[iniI].children[1].children.length;
                j--;
            }
            if(j>=tbe.length)//we are in the second child
                p=e.children[iniI].children[1].children[j-tbe.length];
            else 
                p=tbe[j];
            console.assert(i===parseInt(p.attributes['data-index'].value,10));
            
            //the only thing that can change is the flags
            console.assert(co.id===c.id && co.mt===c.mt && c.c===co.c && c.sz===co.sz);
            
            //the only thing that can change is the thumbnail URL
            if(c.fl!==co.fl){
                if(!(c.fl&this.server.albumFlags.FlagHasScaledThumbnail)){
                    if(queryBase)query='&c=1';
                    else query='?c=1';
                } else query='';
                p.style.backgroundImage='';
                p.setAttribute('data-bgurl',pathStr+c.id+queryBase+query);
            }
                
            j--;
            if(j<0)
                iniI+=2;
        }//picture
    }//loop

    //update the album/path object
    this.path[this.path.length-1].a=newAlbumData.a;
    this.path[this.path.length-1].e=newAlbumData.e;

    this.albumCache.init(newAlbumData.e,pathStr,albumTitle,pictureDisplayNr);

    //if not loading thumbnails already, start loading them
    if(!this.loadingThumbnailsStartTime){
        this.loadingThumbnails=[];
        this.skipInvisible=true;
        this.loadingThumbnailsStartTime=Date.now();
        this.currentScrollTop=e.scrollTop;
        this.loadThumbnails();
    }

    //done
    console.log("reDisplayAlbum: end");
};

PH.albumCacheInitWithoutDisplay=function(){
    var albumData=this.path[this.path.length-1];
    var pathStr='/t/',albumTitle,pictureDisplayNr=0;
    var i;

    //pathStr
    for(i=0;i<this.path.length-1;i++)
        pathStr+=this.path[i].e[this.path[i].index].id+'/';

    //albumTitle
    console.assert(typeof(albumData.a.t)==='string');
    if(albumData.a.t.length){
        albumTitle=albumData.a.t.substr(albumData.a.no);
    } else {
        //root photostovis
        if(typeof(this.server.metadata.un)!=='undefined')
            albumTitle=this.l10n('txt_photostovis_root_named').replace('%s',this.server.metadata.un);
        else
            albumTitle=this.l10n('txt_photostovis_root');
    }

    //pictureDisplayNr
    for(i=0;i<albumData.e.length;i++)
        if(!(albumData.e[i].fl&this.server.albumFlags.FlagIsAlbum)){
            pictureDisplayNr++;
            albumData.e[i].displayNr=pictureDisplayNr;
        }

    this.albumCache.init(albumData.e,pathStr,albumTitle,pictureDisplayNr);
}


PH.getHashFromPath=function(){
    var i,e,index=-1,hash='';
    for(i=0;i<this.path.length;i++){
        index=this.path[i].index;
        if(index===-1 ){
            //going back in path
            console.assert(i===this.path.length-1); //should be the last element
            break;
        }
        if(index<0 || index>=this.path[i].e.length)
        	sendErrorToServer(`Error in getHashFromPath, index=${index} for i=${i}. Full path: ${JSON.encode(this.path)}`,location.href,0,0,null);
        hash+='/'+this.path[i].e[index].id;
    }
       	
    //get the command
    if(index!==-1){
        e=this.path[this.path.length-1].e[index];
        if(e.fl&this.server.albumFlags.FlagIsAlbum)
            hash='#a'+hash;
        else if(e.fl&this.server.albumFlags.FlagIsVideo)
            hash='#v'+hash;
        else hash='#i'+hash;
    } else hash='#a'+hash;
     
    console.log("getHashFromPath: "+hash);
    return hash;
};

PH.enterFullScreen=function(fullApp){
    console.assert(this===PH);
    var fsDiv;
    
    //if we are in iOS this does not work, returning
    if(this.platform.name==='ios' || this.platform.pictureModeFullScreen)return;
    if(typeof(fullApp)==='undefined')fullApp=false;
    if(fullApp){
        fsDiv=document.body;
        this.platform.pictureModeFullScreen=true;
    } else
        fsDiv=elm('fullscreendiv');

    if(fsDiv.msRequestFullscreen)
        fsDiv.msRequestFullscreen();
    else if(fsDiv.mozRequestFullScreen)
        fsDiv.mozRequestFullScreen();
    else if(fsDiv.webkitRequestFullscreen)
        fsDiv.webkitRequestFullscreen();
    else if(fsDiv.requestFullscreen)
        fsDiv.requestFullscreen();
};

PH.exitFullScreen=function(fullApp){
    //if we are in iOS fullscreen does not work
    if(PH.platform.name==='ios')return;

    if(typeof(fullApp)==='undefined')fullApp=false;
    if(!fullApp  && PH.platform.pictureModeFullScreen)return;
    
    if(document.fullscreenElement && document.exitFullscreen)
        document.exitFullscreen();
    else if(document.msFullscreenElement && document.msExitFullscreen)
        document.msExitFullscreen();
    else if(document.mozFullScreenElement && document.mozCancelFullScreen)
        document.mozCancelFullScreen();
    else if(document.webkitFullscreenElement && document.webkitExitFullscreen)
        document.webkitExitFullscreen();

    PH.platform.pictureModeFullScreen=false;
};

PH.isFullScreen=function(){
    console.log("isFullScreen?");
    if(document.fullscreenElement || document.msFullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement)return true;
    console.log("No, it is not");
    return false;
}

    
    
    
