"use strict";

function elm(id){
    var e=document.getElementById(id);
    if(!e)throw new Error("ERROR in elm(): element with id="+id+" does not exist");
    return e;
}
function elmShow(id){
    var e=document.getElementById(id);
    if(!e)throw new Error("ERROR in elmShow(): element with id="+id+" does not exist");
    e.style.display='block';
}
function elmShowI(id){
    var e=document.getElementById(id);
    if(!e)throw new Error("ERROR in elmShowI(): element with id="+id+" does not exist");
    e.style.display='inline';
}
function elmHide(id){
    var e=document.getElementById(id);
    if(!e)throw new Error("ERROR in elmHide(): element with id="+id+" does not exist");
    e.style.display='none';
}
function elmClear(id){
    var e=document.getElementById(id);
    if(!e)throw new Error("ERROR in elmClear(): element with id="+id+" does not exist");
    e.style.display='';
}
function elmHTML(id,txt){
    var e=document.getElementById(id);
    if(!e)throw new Error("ERROR in elmHTML(): element with id="+id+" does not exist");
    e.innerHTML=txt;
}
function elmHTMLshow(id,txt){
    var e=document.getElementById(id);
    if(!e)throw new Error("ERROR in elmHTMLshow(): element with id="+id+" does not exist");
    e.innerHTML=txt;
    e.style.display='block';
}
function elmHTMLhide(id,txt){
    var e=document.getElementById(id);
    if(!e)throw new Error("ERROR in elmHTMLhide(): element with id="+id+" does not exist");
    e.innerHTML=txt;
    e.style.display='none';
}
function elmBindPdSp(id,f){
    document.getElementById(id).addEventListener('click',function(e){
        e.preventDefault();
        e.stopPropagation();
        f(e);
    },false);
}

function parseJSON(txt){
    var obj;
    try {
        obj=JSON.parse(txt);
        return obj;
    } catch (e) {
        console.log("JSON.parse returned an error for this string: "+txt);
        console.log("The error was:");
        console.log(e);
        return null;
    }
}
function ajaxJson(url,params,f,ferr){
    console.assert(typeof(url)==='string');
    console.assert(params===null || typeof(params)==='string');
    console.assert(f===null || typeof(f)==='function');

    var x=new XMLHttpRequest();
    x.ontimeout=function(){
        console.warn("WARNING: Request for "+url+" timed out.");
        if(typeof(ferr)==='function')
            ferr(-1);
    }

    x.onreadystatechange=function() {
        if(x.readyState===4){
            if(x.status===200){
                var obj=parseJSON(x.responseText);
                console.assert(typeof(obj)==='object');
                if(typeof(f)==='function')
                    f(obj);
            } else {
                if(x.status!==403){
                    console.warn("WARNING: XMLHttprequest failed: "+x.statusText);
                    console.warn(x);
                } //forbidden is ok, it means we need to show the authentication page
                if(typeof(ferr)==='function')
                    ferr(x.status);
            }
        }
    };

    if(params){
        x.open('POST',url,true);
        x.timeout=20000; //20 seconds. This has to be set between open() and send()
        x.send(params);
    } else {
        x.open('GET',url,true);
        x.timeout=20000; //20 seconds. This has to be set between open() and send()
        x.send(null);
    }
}

function getURLParam (oTarget, sVar) {
    return decodeURI(oTarget.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURI(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
    //how to use: alert(getURLParam(window.location, "name"));
}

function clone(obj) {
    var copy;

    // Handle the 3 simple types, and null or undefined
    if (null === obj || "object" !== typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}

function _pad(x){
	if(x<10)return '0'+x;
	return x;
}
function getShortestTimeFormat(s){
	if(s<60)return s+'s';
	
	let ss=s%60;
	let mm=(s-ss)/60;
	
	if(s<3600)
		return mm+':'+_pad(ss);
	
	let mx=mm;
	mm%=60;
	let hh=(mx-mm)/60;
	
	return hh+':'+_pad(mm)+':'+_pad(ss);
}

/*
function addListener(obj,ev,fnct){
    if(obj.addEventListener){//All browsers, except IE before version 9.
        obj.addEventListener(ev,fnct,false);
    } else if(obj.attachEvent){//IE before version 9.
        ev=(ev==='DOMContentLoaded')?'onreadystatechange':'on'+ev;
        obj.attachEvent(ev,fnct);
    } else console.error("addListener: no way of attaching things!!");
}*/

function loadScript(src,callback){
    var script = document.createElement("script");
    script.type = "text/javascript";
    if(callback)script.onload=callback;
    document.getElementsByTagName("head")[0].appendChild(script);
    script.src = src;
}

function fixedEncodedURIComponent(str){
    return encodeURIComponent(str).replace(/%20/g, '+');
}

function sendErrorToServer(msg,pageurl,line,col,errObj){
	if(msg==='ResizeObserver loop limit exceeded')return; 
	console.warn("ONERROR: msg="+msg+", pageurl="+pageurl+", line="+line+", column="+col);
	
	var url='https://photostovis.org/sme.php';
    var subject='[photostovis] JS Error!';
    
    var body='msg: '+msg+'\r\nurl='+pageurl+'\r\nline='+line+'\r\nUA='+navigator.userAgent;
    if(typeof(errObj)==='object' && errObj && typeof(errObj.stack)==='string')
        body+='\r\nstack='+errObj.stack;
        
    console.log('############################################### Announcing a JS error.');
    ajaxJson(url,JSON.stringify({s:subject,b:body}),function(obj){
	    if(obj && obj.a){
		    console.log("JS Error announced successfully!");
	    } else {
		    console.warn("Error sending JS error: ",obj);
	    }
    },function(status){
	    console.warn("Sending the JS error failed with status: "+status);
    });
}

window.onerror=sendErrorToServer;

