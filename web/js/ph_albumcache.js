"use strict";

PH.albumCache=(function(){ //functiuon returning an object
    var picsInAlbum=[];
    var mediaShadow=[];
    var pathStr=null;
    var albumTitle,nrDisplayPictures;
    var cPicAlbumIndex=-1; //current pic index in album (valid: 0-picsInAlbum.length-1)
    var cPicCacheIndex=-1; //current pic index in cache array (valid: 0-imgCache.length-1)
    var loadingImg=null; //the currently loading img object
    var __loadingImgNullReason="albumCache init";
    var widthOnEntry=-1;
    var showingImg=-1; //in the beginning there is no img showing
    var spinner=null;
    var map=null,photoMarker=null;
    var picWatchStartTime=null,thmbWatchStartTime=null;
    var direction=0; //1 if going forward, -1 if going backwords
    var zoomInElm=null,zoomOutElm=null,zoombox=null;
    var infobox=null; //=elm('img-info'); //caching this
    var resobox=null; //=elm('min-resolution'); //caching this
    var imgDiv=null;
    var reloadImageResolution=null,requestedMinResolution='n'; //can be: 'h', 'd', 'r', 'o', requestedMinResolution can also be 'n'
    var requestStartTime=null;

    var KCacheRadius=2;
    var KInfoboxIconOpacity="0.3";

    var statusFlags={
        FlagThumbnailLoading:1,
        FlagThumbnailLoaded:2,
        FlagMediaLoading:4,
        FlagMediaLoaded:8,
        FlagMediaPrefetchSent:16
    };
    var spinnerOpts={
        lines: 11, // The number of lines to draw
        length: 5, // The length of each line
        width: 3, // The line thickness
        radius: 7, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#FFFFFF', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent
        left: '50%' // Left position relative to parent
    };
    var _newElement=function(i){
        var p;
        console.assert(typeof(picsInAlbum[i].t)==='undefined');
        //create the div container
        p=document.createElement('div');
        p.id='media'+i;
        p.className='mediadiv';
        
        //create the elements inside the container
        if(!(picsInAlbum[i].fl&PH.server.albumFlags.FlagIsVideo))
            p.innerHTML='<img class="fsimg" src=""><img class="fsimg" src="" style="display:none">'; //image
        return p;
    };
    
    var _newShadowElement=function(i){
        var r={
            photostovisI:i,
            photostovisStatus:0,
            photostovisRot:'',
            photostovisZoom:0
        };
        return r;
    };
    
    var _clearElement=function(p){
        var r;
        console.assert(p.tagName==='DIV' || p.tagName==='div');
        //is there anything to remove?
        while(p.lastChild){
            r=p.lastChild;
            if(r.tagName==='IMG' || r.tagName==='img'){
                //this is an image
                r.removeEventListener('load',_imageLoaded);
                if(r===loadingImg){
                    loadingImg=null;
                    __loadingImgNullReason="clearElement";
                }
                r.style.cssText='';
                r.style.display='';
                r.style.transition='';
                r.style.top='';
                r.style.left='';
                //r.src='';
            } else if(r.tagName==='VIDEO' || r.tagName==='video'){
                //this is a video
                r.removeEventListener('loadedmetadata',_metadataLoaded);
                r.pause();
                r.src='';
                r.load();
            }
            delete p.removeChild(r);
        }//while
        r=null; //remove references to objects
        p.innerHTML='';
    }

    var _removeElement=function(i){
        //remove the DOM element(s). This function is important, otherwise memory leaks will kill the browser
        var p=imgDiv.children[i];
        mediaShadow.splice(i,1);//remove the mediaShadow element
        console.log("_removeElement++: nr children of elm2remove: "+p.children.length);

        _clearElement(p);
        delete imgDiv.removeChild(p);
        p=null;

        console.assert(imgDiv.children.length===mediaShadow.length);
        console.log("_removeElement--");
    };
    
    var _getNextIndex=function(i){
        //console.log("_getNextIndex")
        var n=i+1;
        while(n<picsInAlbum.length){
            if(typeof(picsInAlbum[n].t)==='undefined')return n;//this is an image
            else n++;//this is an album that we skip
        }
        return -1; //not found
    };
    var _getPrevIndex=function(i){
        var p=i-1;
        while(p>=0){
            if(typeof(picsInAlbum[p].t)==='undefined')return p;//this is an image
            else p--;//this is an album that we skip
        }
        return -1; //not found
    };
    var _sizeImg=function(i,childIndex){
        console.assert(i>=0 && i<imgDiv.children.length);
        var imgWidth,imgHeight; //we cannot trust r.width and r.height because they are not rotated
        var rot,dw,dh,hr,vr;
        var cssText='';
        var s=mediaShadow[i];
        var p=imgDiv.children[i];
        console.assert(p.children.length>childIndex);
        var imgNatWidth,imgNatHeight,imgNatTmp;
        var albumElm=picsInAlbum[s.photostovisI];
        var r=p.children[childIndex];

        console.log("");
        console.groupCollapsed("sizeImg with photostovisI="+s.photostovisI);
        console.assert(r.height>0 && r.width>0,"r.height="+r.height+", r.width="+r.width);
        rot=albumElm.rot;
        console.assert(typeof(albumElm.w)==='number' && typeof(albumElm.h)==='number');
        

        if(rot===90 || rot===270){
            //switch width and height
            console.log("Switching width and height ...");
            imgWidth=albumElm.h;
            imgHeight=albumElm.w;
        } else {
            imgWidth=albumElm.w;
            imgHeight=albumElm.h;
        }
        //moved here from below, see comments
        if(s.photostovisRot.length)
            cssText+='transform:'+s.photostovisRot+';image-orientation: none;';
        else rot=0;

        imgNatWidth=r.naturalWidth>0?r.naturalWidth:r.width;
        imgNatHeight=r.naturalHeight>0?r.naturalHeight:r.height;
        if((imgWidth>imgHeight && imgNatWidth<imgNatHeight) || (imgWidth<imgHeight && imgNatWidth>imgNatHeight)){
            //switch
            imgNatTmp=imgNatWidth;
            imgNatWidth=imgNatHeight;
            imgNatHeight=imgNatTmp;
        }

        //it is possible that the if(...)rot=0 line must be here below for ios6 and above for ios7. To be checked with an ios6 at some point
        //if(s.photostovisRot==='')rot=0;

        dw=document.documentElement.clientWidth;
        dh=document.documentElement.clientHeight;
        hr=dw/imgWidth;
        vr=dh/imgHeight;
        console.log("Image Width="+imgWidth+" Image Height="+imgHeight);
        console.log("Document Width="+document.documentElement.clientWidth+" Document Height="+document.documentElement.clientHeight);
        console.log("Horizontal ratio="+hr+" Vertical ratio="+vr);
        
        //console.log("JSON width="+albumElm.w+", JSON height="+albumElm.h);
        
        if(hr<vr){
            if(rot===90 || rot===270){
                //switch width and height
                s.photostovisWidth=imgHeight*hr;
                s.photostovisLeft=(dw-s.photostovisWidth)/2;
                s.photostovisHeight=dw;
                s.photostovisTop=(dh-s.photostovisWidth)/2;
                s.photostovisTop0=(s.photostovisWidth-s.photostovisHeight)/2;
                s.photostovisLeft0=(s.photostovisHeight-s.photostovisWidth)/2;
                //zoom dimensions
                s.photostovisZ1Width=dh;
                s.photostovisZ1Height=vr*imgWidth;
                s.photostovisZ1Top=(s.photostovisZ1Width-s.photostovisZ1Height)/2;;
                s.photostovisZ1Left=(s.photostovisZ1Height-s.photostovisZ1Width)/2;
                s.photostovisZ1ScrollLeft=(s.photostovisZ1Height-dw)/2;
            } else {
                s.photostovisWidth=dw;
                s.photostovisLeft=(dw-s.photostovisWidth)/2;
                s.photostovisHeight=imgHeight*hr;
                s.photostovisTop=(dh-s.photostovisHeight)/2;
                s.photostovisTop0=0;
                s.photostovisLeft0=0;
                //zoom dimensions
                s.photostovisZ1Height=dh;
                s.photostovisZ1Width=vr*imgWidth;
                s.photostovisZ1Top=0;
                s.photostovisZ1Left=0;
                s.photostovisZ1ScrollLeft=(s.photostovisZ1Width-dw)/2;
            }
            s.photostovisZ1ScrollTop=0;

            console.log("Full width. Scaled height="+s.photostovisHeight+" Distance to top="+s.photostovisTop);
        } else {
            if(rot===90 || rot===270){
                //switch width and height
                s.photostovisWidth=dh;
                s.photostovisLeft=(dw-s.photostovisWidth)/2;
                s.photostovisHeight=imgWidth*vr;
                s.photostovisTop0=s.photostovisTop=(s.photostovisWidth-s.photostovisHeight)/2;
                //if the black lateral margins are wider than 360+5 pixels, do NOT move the picture to the left
                if((dw-imgWidth*vr)/2>=365)
                    s.photostovisLeft0=s.photostovisLeft;
                else
                    s.photostovisLeft0=(s.photostovisHeight-s.photostovisWidth)/2;
                //zoom dimensions
                s.photostovisZ1Height=dw;
                s.photostovisZ1Width=hr*imgHeight;
                s.photostovisZ1Top=(s.photostovisZ1Width-s.photostovisZ1Height)/2;
                s.photostovisZ1Left=(s.photostovisZ1Height-s.photostovisZ1Width)/2;
                s.photostovisZ1ScrollTop=(s.photostovisZ1Width-dh)/2;
            } else {
                s.photostovisWidth=imgWidth*vr;
                s.photostovisLeft=(dw-s.photostovisWidth)/2;
                s.photostovisHeight=dh;
                s.photostovisTop0=s.photostovisTop=0;
                //if the black lateral margins are wider than 360+5 pixels, do NOT move the picture to the left
                if(s.photostovisLeft>=365)
                    s.photostovisLeft0=s.photostovisLeft;
                else
                    s.photostovisLeft0=0;

                //zoom1 dimensions
                s.photostovisZ1Width=dw;
                s.photostovisZ1Height=hr*imgHeight;
                s.photostovisZ1Top=0;
                s.photostovisZ1Left=0;
                s.photostovisZ1ScrollTop=(s.photostovisZ1Height-dh)/2;
            }
            s.photostovisZ1ScrollLeft=0;

            console.log("Full height. Scaled width="+s.photostovisWidth+" Distance to left="+s.photostovisLeft+" Distance to top="+s.photostovisTop);
        }

        //zoom2 dimensions
        if(rot===90 || rot===270){
            s.photostovisZ2Height=imgNatWidth/PH.platform.pixelRatio;
            s.photostovisZ2Width=imgNatHeight/PH.platform.pixelRatio;
            if(dw-s.photostovisZ2Height>0){
                s.photostovisZ2Left=(dw-s.photostovisZ2Width)/2;
                s.photostovisZ2ScrollLeft=0;
            } else {
                s.photostovisZ2Left=(s.photostovisZ2Height-s.photostovisZ2Width)/2;
                s.photostovisZ2ScrollLeft=(s.photostovisZ2Height-dw)/2;
            }
            if(dh-s.photostovisZ2Width>0){
                s.photostovisZ2Top=(dh-s.photostovisZ2Height)/2;
                s.photostovisZ2ScrollTop=0;
            } else {
                s.photostovisZ2Top=(s.photostovisZ2Width-s.photostovisZ2Height)/2;
                s.photostovisZ2ScrollTop=(s.photostovisZ2Width-dh)/2;
            }
        } else {
            s.photostovisZ2Width=imgNatWidth/PH.platform.pixelRatio;
            s.photostovisZ2Height=imgNatHeight/PH.platform.pixelRatio;
            if(dw-s.photostovisZ2Width>0){
                s.photostovisZ2Left=(dw-s.photostovisZ2Width)/2;
                s.photostovisZ2ScrollLeft=0;
            } else {
                s.photostovisZ2Left=0;
                s.photostovisZ2ScrollLeft=(s.photostovisZ2Width-dw)/2;
            }
            if(dh-s.photostovisZ2Height>0){
                s.photostovisZ2Top=(dh-s.photostovisZ2Height)/2;
                s.photostovisZ2ScrollTop=0;
            } else {
                s.photostovisZ2Top=0;
                s.photostovisZ2ScrollTop=(s.photostovisZ2Height-dh)/2;
            }
        }

        //new stuff: create the cssText
        switch(s.photostovisZoom){
        case 0:
            cssText+='width:'+s.photostovisWidth+'px; height:'+s.photostovisHeight+'px; top:';
            if(PH.userPrefsGlobal.showPicInfo===2)
                cssText+=s.photostovisTop0+'px; left:'+s.photostovisLeft0+'px;';
            else
                cssText+=s.photostovisTop+'px; left:'+s.photostovisLeft+'px;';
            break;
        case 1:
            cssText+='width:'+s.photostovisZ1Width+'px; height:'+s.photostovisZ1Height+'px; top:'+s.photostovisZ1Top+'px; left:'+s.photostovisZ1Left+'px;';
            break;
        case 2:
            cssText+='width:'+s.photostovisZ2Width+'px; height:'+s.photostovisZ2Height+'px; top:'+s.photostovisZ2Top+'px; left:'+s.photostovisZ2Left+'px;';
            break;
        }//switch
        r.style.cssText=cssText;
        console.groupEnd();
    };
    var _getInfoText=function(s,img){
        var camera,mp,mb,c;
        var sc_num,sc_denom;
        var txt,txtSmall,txtTmp;

        c=picsInAlbum[s.photostovisI];

        if(typeof(c.m)==='number' && c.m!==0xFFFF){
            camera=PH.server.metadata.m[c.m];
            console.assert(typeof(camera.c)==='string' && camera.c.length>=0);
        } else camera=null;
        console.log("Picture data:");
        console.log(c);
        console.log("CAMERA:");
        console.log(camera);

        //album title row
        txt='<h4>'+albumTitle+' ('+(c.displayNr)+'/'+nrDisplayPictures+')</h4>';
        txtSmall='<table><tr>';
        //title row
        txt+='<h1>'+c.fn+'</h1>';
        //date row
        if(c.c!==0x7FFFFFFF){
            txtTmp=PH.getFormattedDateTime(c.c,c.tz);
            txt+='<h2>'+txtTmp+'</h2>';
            txtSmall+='<td class="date">'+txtTmp+'</td>';
        } else txtSmall+='<td class="date"></td>';

        //album title row for small text
        txtSmall+='<td class="nr-pics">('+(c.displayNr)+'/'+nrDisplayPictures+')</td></tr>';

        //Original image data
        if(c.sz<1048576)mb=(c.sz/1024).toFixed(0)+' KB';
        else mb=(c.sz/1048576).toFixed(1)+' MB';
        console.assert(c.w>0 && c.h>0,"c.w="+c.w+", c.h="+c.h);
        mp=c.w*c.h/1000000;
        mp=mp.toFixed(1)+' Mpix';

        txt+='<table><tr><td>Original:</td><td class="right">'+c.w+'</td><td>x</td><td>'+c.h+'</td><td class="right">('+mp+
                '</td><td>/</td><td>'+mb+')</td></tr>';

        //transfered image data
        console.assert(img.width>0 && img.height>0);
        if(typeof(img.naturalWidth)!=='undefined'){
            if(img.naturalWidth===c.w && img.naturalHeight===c.h){
                sc_num=0;
            } else {
                //mp=(c.w/img.naturalWidth)*(c.h/img.naturalHeight);
                sc_num=Math.round(img.naturalWidth*8/c.w);
                console.log("Transfered image dimensions: "+img.naturalWidth+'x'+img.naturalHeight);
                console.log("Original/rounded sc_num: "+(img.naturalWidth*8/c.w)+'/'+sc_num);
                if(sc_num===0)sc_num=1; //TODO: this is temporary solution/hack
                console.assert(sc_num>0 && sc_num<8);
            }
        } else {
            if(img.width===c.w && img.height===c.h){
                sc_num=0;
            } else {
                //mp=(c.w/img.width)*(c.h/img.height);
                sc_num=Math.round(img.width*8/c.w);
                console.log("Transfered image dimensions: "+img.width+'x'+img.height);
                console.log("Original/rounded sc_num: "+(img.width*8/c.w)+'/'+sc_num);
                if(sc_num===0)sc_num=1; //TODO: this is temporary solution/hack
                console.assert(sc_num>0 && sc_num<8);
            }
        }
        if(sc_num>0){
            if(sc_num===4){
                sc_num=1;sc_denom=2;
            } else if(sc_num===2 || sc_num===6){
                sc_num/=2;sc_denom=4;
            } else sc_denom=8;
            mp=sc_num+'/'+sc_denom;
            txt+='<tr><td>On screen:</td><td class="right">'+img.naturalWidth+'</td><td>x</td><td>'+img.naturalHeight+
                '</td><td colspan="3" class="center">('+mp+'x'+mp+' from original)</td></tr>';
            if(camera)
                txtSmall+='<tr><td class="camera">'+camera.c+'</td><td class="scaling">('+mp+'x'+mp+')</td></tr></table>';
            else
                txtSmall+='<tr><td class="camera"></td><td class="scaling">('+mp+'x'+mp+')</td></tr></table>';
        } else {
            txt+='<tr><td>On screen:</td><td colspan="6" class="originalimg">original image</td></tr>';
            if(camera)
                txtSmall+='<tr><td class="camera">'+camera.c+'</td><td class="scaling">(original)</td></tr></table>';
            else
                txtSmall+='<tr><td class="camera"></td><td class="scaling">(original)</td></tr></table>';
        }


        txt+='</table>';
        //Camera & nr pictures
        if(camera)
            txt+='<h2>Camera: '+camera.c+'</h2><h3>('+camera.p+' pictures taken with this camera)</h3>';

        txt+='<table>';
        //Focal length
        if(c.fln)
            txt+='<tr><td class="nowrap">Focal length:</td><td>'+c.fln.toFixed(2)+'mm</td></tr>';
        //Focal length in 35mm
        if(c.fl35)
            txt+='<tr><td class="nowrap">35mm equivalent:</td><td>'+c.fl35+'mm</td></tr>';
        //Exposure
        if(c.et)
            txt+='<tr><td class="nowrap">Exposure time:</td><td>'+c.et.toPrecision()+'s</td></tr>';
        //Exposure program
        if(c.ep)
            txt+='<tr><td class="nowrap">Exposure program:</td><td>'+PH.getExposureProgram(c.ep)+'</td></tr>';
        //F number
        if(c.fnr)
            txt+='<tr><td class="nowrap">F Number:</td><td>f/'+c.fnr.toPrecision()+'</td></tr>';
        //ISO
        if(c.iso)
            txt+='<tr><td class="nowrap">ISO:</td><td>'+c.iso+'</td></tr>';
        //Zoom
        if(c.fl35>0)
            txt+='<tr><td class="nowrap">Zoom (35mm equiv):</td><td>'+(c.fl35/50).toFixed(1)+'</td></tr>';
        else if(c.fln>0 && camera && camera.mfl>0 && camera.p>=100) {
            //we only do this for more than 100 pictures, to increase the probability that at least one of these
            //has the minimum focus length of the camera
            txt+='<tr><td class="nowrap">Zoom (approx.):</td><td>'+(c.fln/camera.mfl).toFixed(1)+'</td></tr>';
        }
        txt+='</table>';
        s.photostovisImgText=txt;
        s.photostovisImgTextSmall=txtSmall;
    }
    var _getMinimumResolutionAsObject=function(albumElm,resolution){
        var requestedWidth,requestedHeight,requestedTmp;

        //get the resolution
        if(resolution===null && reloadImageResolution){
            resolution=reloadImageResolution;
            reloadImageResolution=null; //reset it
        }
        if(resolution===null && requestedMinResolution!=='n')
            resolution=requestedMinResolution;
        if(resolution===null)return null;

        //compute the resolution that we are asking
        switch(resolution){
        case 'h':requestedWidth=screen.width/2;requestedHeight=screen.height/2;break;
        case 'd':requestedWidth=screen.width;requestedHeight=screen.height;break;
        case 'r':requestedWidth=screen.width*PH.platform.pixelRatio;requestedHeight=screen.height*PH.platform.pixelRatio;break;
        case 'o':requestedWidth=albumElm.w;requestedHeight=albumElm.h;break;
        default:console.assert(0);
        }
        //do we need to swap width and height?
        if((requestedWidth>requestedWidth && albumElm.w<albumElm.h) || (requestedWidth<requestedWidth && albumElm.w>albumElm.h)){
            //we need to switch them
            requestedTmp=requestedWidth;
            requestedWidth=requestedHeight;
            requestedHeight=requestedTmp;
        }
        return {w:Math.floor(requestedWidth),h:Math.floor(requestedHeight)};
    };
    var _getMinimumResolutionString=function(albumElm,resolution){
        var obj=_getMinimumResolutionAsObject(albumElm,resolution);
        if(obj)
            return '&w='+obj.w+'&h='+obj.h;
        return '';
    };
    var _isBelowMinimumResolution=function(i){
        var s,p,r,rObj,albumElm;
        var imgNatWidth,imgNatHeight,imgNatTmp;
        if(requestedMinResolution==='n')return false; //image is NOT below the minimum resolution

        s=mediaShadow[i];
        if(!(s.photostovisStatus&statusFlags.FlagMediaLoaded))return false; //media not loaded
        albumElm=picsInAlbum[s.photostovisI];
        if(albumElm.fl&PH.server.albumFlags.FlagIsVideo)return false; //this is a video

        //check if the resolution is below the one required
        rObj=_getMinimumResolutionAsObject(albumElm,null);
        if(!rObj)return false;

        p=imgDiv.children[i];
        console.assert(p.children.length===1 || p.children.length===2);
        if(p.children.length===2)r=p.children[1];
        else r=p.children[0];

        imgNatWidth=r.naturalWidth>0?r.naturalWidth:r.width;
        imgNatHeight=r.naturalHeight>0?r.naturalHeight:r.height;
        if((rObj.w>rObj.h && imgNatWidth<imgNatHeight) || (rObj.w<rObj.h && imgNatWidth>imgNatHeight)){
            //switch
            imgNatTmp=imgNatWidth;
            imgNatWidth=imgNatHeight;
            imgNatHeight=imgNatTmp;
        }

        //if this is original image, return false
        if((albumElm.w===imgNatWidth && albumElm.h===imgNatHeight) || (albumElm.h===imgNatWidth && albumElm.w===imgNatHeight)){
            console.log("Image with ID="+s.photostovisID+" is already the original image.");
            return false;
        } else console.log("Image ("+imgNatWidth+"x"+imgNatHeight+") is NOT the original ("+albumElm.w+"x"+albumElm.h+")");

        if(imgNatWidth<rObj.w || imgNatHeight<rObj.h){
            console.log("Image with ID="+s.photostovisID+" is below the minimum resolution required. Img: "+imgNatWidth+"x"+imgNatHeight+", requested: "+rObj.w+"x"+rObj.h);
            return true;
        } else console.assert(imgNatHeight>=rObj.h);
        return false;
    };

    var _imageLoaded=function(){
        var p=this.parentNode,s,r;
        var i,index=-1;
        var picLoadTime,timeNow,thumbnailOrPicture;
        //find our loaded media element among imgDiv children
        for(i=0;i<imgDiv.children.length;i++)
            if(imgDiv.children[i]===p){
                //we found it!
                index=i;
                break;
            }
        if(index<0){
            console.warn("Stray image loaded. Ignoring.");
            return;
        }

        s=mediaShadow[index];

        console.log("Img loaded: "+s.photostovisI+" src="+this.src);

        //rotate the picture if needed. Rotating yelds correct width and height
        /*if(PH.platform.name==='ios'){
            //on ios the browser will rotate the picture by itself, based on the metadata, so we should NOT rotate it
            s.photostovisRot='';
            if(picsInAlbum[s.photostovisI].rot)
                console.log("This picture would have been rotated, but we are on iOS.");
        } else*/ if(picsInAlbum[s.photostovisI].rot){
            s.photostovisRot='rotate('+picsInAlbum[s.photostovisI].rot+'deg)';
            console.log("Rotating loaded picture: "+s.photostovisRot);
        }
        //mark image as loaded, resize it
        if(s.photostovisStatus&statusFlags.FlagThumbnailLoading){
            s.photostovisStatus|=statusFlags.FlagThumbnailLoaded;
            s.photostovisStatus&=~statusFlags.FlagThumbnailLoading;
            _sizeImg(index,0);
            thumbnailOrPicture='thumbnail';
        } else {
            if(loadingImg===null){
                throw new Error("loadingImg is null because: "+__loadingImgNullReason);
            }
            console.assert(loadingImg.src===this.src);
            console.assert(typeof(s.photostovisLoadStartTime)==='number',s.photostovisLoadStartTime);
            picLoadTime=Date.now()-s.photostovisLoadStartTime;
            console.log("Picture ("+s.photostovisID+") loaded in "+picLoadTime+"ms. startTime="+s.photostovisLoadStartTime);
            PH.sendWebsocketMessage("loadID="+s.photostovisID+" loadTime="+picLoadTime);
            delete s.photostovisLoadStartTime;
            loadingImg=null;
            __loadingImgNullReason="cleared in imageLoaded";
            console.assert(s.photostovisStatus&statusFlags.FlagMediaLoading);
            s.photostovisStatus&=~statusFlags.FlagMediaLoading;
            s.photostovisStatus|=statusFlags.FlagMediaLoaded;
            console.assert(p.children.length===2);
            _sizeImg(index,1);
            thumbnailOrPicture='picture';

            if((direction===-1 && cPicCacheIndex>0 && typeof(mediaShadow[cPicCacheIndex-1])!=='undefined' &&
                mediaShadow[cPicCacheIndex-1].photostovisStatus&statusFlags.FlagMediaLoaded) ||
               (direction>=0 && cPicCacheIndex<KCacheRadius+KCacheRadius && typeof(mediaShadow[cPicCacheIndex+1])!=='undefined' &&
                mediaShadow[cPicCacheIndex+1].photostovisStatus&statusFlags.FlagMediaLoaded))
                elm('next-pic-ready').style.visibility='visible';
        }

        //image metadata
        _getInfoText(s,this);

        if(s.photostovisPathStr===pathStr){
            if(s.photostovisI===cPicAlbumIndex){
                //show picture
                console.log("Current picture has been loaded, showing it!");
                PH.albumCache.showImg(index,PH.albumCache.loadNextMedia);
            } else //load next picture
                setTimeout(PH.albumCache.loadNextMedia,0);
        }
        delete s.photostovisPathStr;
        //if the image was loaded, delete the thumbnail or the first (lower resolution) image
        if(s.photostovisStatus&statusFlags.FlagMediaLoaded && p.children.length===2){
            r=p.children[0];
            r.style.cssText='';
            r.style.display='';
            r.style.transition='';
            r.style.top='';
            r.style.left='';
            p.removeChild(r);
        }


        timeNow=Date.now();
        console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ["+cPicAlbumIndex+"] Next image ("+s.photostovisI+", "+thumbnailOrPicture+
                    ") loaded after: "+(timeNow-requestStartTime)+"ms");
    };
    
    var _metadataLoaded=function(){
        var p=this.parentNode,s;
        var i,index=-1;
        //find our loaded media element among imgDiv children
        for(i=0;i<imgDiv.children.length;i++)
            if(imgDiv.children[i]===p){
                //we found it!
                index=i;
                break;
            }
        if(index<0){
            console.warn("Stray video metadata loaded. Ignoring.");
            return;
        }
        s=mediaShadow[index];
        console.log("Video Metadata loaded: "+s.photostovisI+". Video dimensions: "+this.videoWidth+"x"+this.videoHeight);
        console.log("Seekable: "+this.seekable+", readyState: "+this.readyState);
        console.log(s);
        //mark video as loaded
        console.assert(s.photostovisStatus&statusFlags.FlagMediaLoading);
        s.photostovisStatus|=statusFlags.FlagMediaLoaded;
        s.photostovisStatus&=~statusFlags.FlagMediaLoading;
        //size the video
        if(this.videoWidth/this.videoHeight>PH.platform.screenWidth/PH.platform.screenHeight){
            //the video is wider than the screen.
            this.style.width='100%';
            this.style.height='auto';
        } else {
            //the screen is wider than the video.
            this.style.height='100%';
            this.style.width='auto';
        }
        //show or load next (or both)
        if(s.photostovisPathStr===pathStr){
            if(s.photostovisI===cPicAlbumIndex){
                //show video
                console.log("Current video has been loaded, showing it!");
                PH.albumCache.showVideo(index,PH.albumCache.loadNextMedia);
            } else //load next picture
                setTimeout(PH.albumCache.loadNextMedia,0);
        }
        delete s.photostovisPathStr;
        //mark the shadow as having a "stopable" child
        s.photostovisStopable=0;
    };
    
    var _showSpinner=function(){
        if(spinner)spinner.stop();
        spinner=null;
        spinner=new Spinner(spinnerOpts).spin(elm('fsimg-spinner-box'));
    };
    var _hideSpinner=function(){
        if(spinner)spinner.stop();
        spinner=null;
    };
    var _getTransform=function(elm){
        return elm.style.transform;
    };
    var _setTransform=function(elm,txt){
        elm.style.transform=txt;
    };
    var _hideShowing=function(){
        if(showingImg<0)return;
        
        var pOld=imgDiv.children[showingImg];
        var sOld=mediaShadow[showingImg];
        //pOld.style.display='none';
        //->console.assert(!direction || pOld.style.transition!=='');
        console.assert(_getTransform(pOld)==='',_getTransform(pOld));
        if(direction){
            //->console.assert(pOld.style.transition!=='',"Transition: "+pOld.style.transition);
            _setTransform(pOld,'translate3d('+(-direction)+'00%,0,0)');
        } else {
            //->pOld.style.transition='opacity 0.2s';
            pOld.style.opacity='0';
        }

        if(typeof(sOld.photostovisStopable)==='number' && pOld.children[sOld.photostovisStopable])
            pOld.children[sOld.photostovisStopable].pause();
        showingImg=-1;
    };

    return {
        clear:function(){
            //clear imgDiv & media Shadow before clearing anything else
            console.log("albumCache::clear++");
            if(imgDiv){
                while(imgDiv.children.length)
                    _removeElement(imgDiv.children.length-1);
                console.assert(loadingImg===null); //because it was made null when the load event was removed, in _removeElement
                imgDiv.innerHTML='';
            }
            mediaShadow=[];
            
            picsInAlbum=null;
            pathStr=null;
            albumTitle=null;
            nrDisplayPictures=0;

            cPicAlbumIndex=-1; //current pic index in album (valid: 0-picsInAlbum.length-1)
            cPicCacheIndex=-1; //current pic index in cache array (valid: 0-imgCache.length-1)

			//clear these, should free some memory
			imgDiv=infobox=zoomInElm=zoomOutElm=zoombox=resobox=null;

            showingImg=-1; //in the beginning there is no img showing
            direction=0;
            reloadImageResolution=null;
            requestStartTime=null;
            console.log("albumCache::clear--");
        },
        init:function(picsAlbm,pathString,tt,nrdsp){
            console.log("albumCache::init++");
            this.clear();
            picsInAlbum=picsAlbm;
            pathStr=pathString;
            albumTitle=tt;
            nrDisplayPictures=nrdsp;
            //console.log(picsInAlbum);
            console.assert(typeof(picsInAlbum)==='object' && picsInAlbum);
            console.assert(Array.isArray(picsInAlbum));
            console.assert(typeof(pathStr)==='string' && pathStr.search('/t/')!==-1);
            //reset timings
            picWatchStartTime=thmbWatchStartTime=null;
            //elm shortcuts
            if(!imgDiv)imgDiv=elm('imgdiv');
            if(!infobox)infobox=elm('img-info');
            if(!zoomInElm)zoomInElm=elm('zoom-in');
            if(!zoomOutElm)zoomOutElm=elm('zoom-out');
            if(!zoombox)zoombox=elm('zoom');
            if(!resobox)resobox=elm('min-resolution');
            if(PH.userSession.showcase){
                PH.userSession.showcase=false;
                PH.userPrefsGlobal.showPicInfo=0;
            }
            if(typeof(PH.userPrefsGlobal.showPicInfo)==='boolean'){
                if(PH.userPrefsGlobal.showPicInfo)PH.userPrefsGlobal.showPicInfo=1;
                else PH.userPrefsGlobal.showPicInfo=0;
            }
            console.log("albumCache::init--");
        },
        reset:function(){
	        //this is a clear + init
	        console.log("albumCache::reset++");
	        if(imgDiv){
                while(imgDiv.children.length)
                    _removeElement(imgDiv.children.length-1);
                console.assert(loadingImg===null); //because it was made null when the load event was removed, in _removeElement
                imgDiv.innerHTML='';
            }
            mediaShadow=[];
            console.assert(typeof(picsInAlbum)==='object' && picsInAlbum);
            console.assert(Array.isArray(picsInAlbum));
            console.assert(typeof(pathStr)==='string' && pathStr.search('/t/')!==-1);
            
			picWatchStartTime=thmbWatchStartTime=null;
			cPicAlbumIndex=-1; //current pic index in album (valid: 0-picsInAlbum.length-1)
            cPicCacheIndex=-1; //current pic index in cache array (valid: 0-imgCache.length-1)

            showingImg=-1; //in the beginning there is no img showing
            direction=0;
            reloadImageResolution=null;
            requestStartTime=null;
	        
	        console.log("albumCache::reset--");
        },
        resize:function(){
            if(!imgDiv)return;
            console.log("resize++");
            var s;
            var j;
            console.assert(imgDiv.children.length===mediaShadow.length);
            requestStartTime=null;
            for(j=0;j<mediaShadow.length;j++){
                s=mediaShadow[j];
                s.photostovisHeight=-1;
                s.photostovisWidth=-1;
                s.photostovisZ1Height=-1;
                s.photostovisZ1Width=-1;
                s.photostovisZ2Height=-1;
                s.photostovisZ2Width=-1;
            }
            if(showingImg>=0 && cPicAlbumIndex>=0)
                if(!(picsInAlbum[cPicAlbumIndex].fl&PH.server.albumFlags.FlagIsVideo)){
                    requestStartTime=Date.now();
                    this.showImg(showingImg,null);
                }
            if(spinner){
                spinner.stop();
                spinner=null;
                spinner=new Spinner(spinnerOpts).spin(elm('fsimg-spinner-box'));
            }
            console.log("resize--");
        },
        loadNextMedia:function(){
            var i=0,index=-1;
            var s,media2load,shadow2load;
            var img2load,albumElm;
            var albumIndexNext,albumIndexPrev;
            var doPrefetching=false;
            var belowMinRes,newImg;
            
            console.groupCollapsed("loadNextMedia: current="+cPicAlbumIndex);
            console.assert(cPicAlbumIndex<picsInAlbum.length,"Current pic outside range: "+cPicAlbumIndex);
            console.assert(cPicCacheIndex>=0 && cPicCacheIndex<KCacheRadius+1+KCacheRadius,"cPicCacheIndex outside range");
            if(cPicAlbumIndex<0){
                console.log("returning (the user has exited the \"show pictures\" mode)");
                console.groupEnd();
                return;
            } //the user has exited the "show pictures" mode

            s=mediaShadow[cPicCacheIndex];
            if(s.photostovisStatus&statusFlags.FlagMediaLoading || s.photostovisStatus&statusFlags.FlagThumbnailLoading){
                //current thumb already loading, we will continue when this is ready
                console.log("loadNextMedia: current thumb or media already loading, will continue with cache loading when it is loaded");
                if(picsInAlbum[cPicAlbumIndex].fl&PH.server.albumFlags.FlagIsVideo){
                    //we show the video even if it is not fully loaded
                    PH.albumCache.showVideo(cPicCacheIndex);
                    console.log("Showing current video");
                }
                console.groupEnd();
                return;
            }
            
            if(s.photostovisStatus&statusFlags.FlagThumbnailLoaded ||
                    !(picsInAlbum[cPicAlbumIndex].fl&PH.server.albumFlags.FlagHasThumbnail) ||
                    picsInAlbum[cPicAlbumIndex].fl&PH.server.albumFlags.FlagIsVideo){
                //current thumb loaded (or not available), check the current image

                //BUT FIRST: do we have an already loading image? If yes, we should wait until it loads (because we cannot cancel it)
                if(loadingImg){
                    //yes, we have a loading image
                    console.log("!!! We already have a loading image! Wait until it loads.");
                    console.groupEnd();
                    return;
                }

                if(s.photostovisStatus&statusFlags.FlagMediaLoaded && !reloadImageResolution && !(belowMinRes=_isBelowMinimumResolution(cPicCacheIndex))){
                    //current image already loaded, we need a new image to load
                    //check first if all thumbnails are loaded
                    albumIndexNext=albumIndexPrev=cPicAlbumIndex;
                    for(i=1;i<KCacheRadius+1;i++){
                        index=cPicCacheIndex+i;
                        albumIndexNext=_getNextIndex(albumIndexNext);
                        if(index<imgDiv.children.length && albumIndexNext!==-1 && 
                                picsInAlbum[albumIndexNext].fl&PH.server.albumFlags.FlagHasThumbnail &&
                                !(picsInAlbum[albumIndexNext].fl&PH.server.albumFlags.FlagIsVideo)){
                            media2load=imgDiv.children[index];
                            shadow2load=mediaShadow[index];
                            if(!(shadow2load.photostovisStatus&statusFlags.FlagThumbnailLoaded))break;
                        }

                        index=cPicCacheIndex-i;
                        albumIndexPrev=_getPrevIndex(albumIndexPrev);
                        if(index>=0 && albumIndexPrev!==-1 && 
                                picsInAlbum[albumIndexPrev].fl&PH.server.albumFlags.FlagHasThumbnail &&
                                !(picsInAlbum[albumIndexPrev].fl&PH.server.albumFlags.FlagIsVideo)){
                            media2load=imgDiv.children[index];
                            shadow2load=mediaShadow[index];
                            if(!(shadow2load.photostovisStatus&statusFlags.FlagThumbnailLoaded))break;
                        }
                    }//for
                    if(i<KCacheRadius+1){
                        console.log("Loading a thumb (index="+index+", current="+cPicCacheIndex+")");
                        shadow2load.photostovisStatus|=statusFlags.FlagThumbnailLoading;
                    } else {
                        //try with an image
                        albumIndexNext=albumIndexPrev=cPicAlbumIndex;
                        for(i=1;i<KCacheRadius+1;i++){
                            //following if prevents videos to be loaded, if they are more than one step after or before the current element
                            albumIndexNext=_getNextIndex(albumIndexNext);
                            albumIndexPrev=_getPrevIndex(albumIndexPrev);
                            if(i===1 || 
                                !(albumIndexNext!==-1 && picsInAlbum[albumIndexNext].fl&PH.server.albumFlags.FlagIsVideo)
                                || !(albumIndexPrev!==-1 && picsInAlbum[albumIndexPrev].fl&PH.server.albumFlags.FlagIsVideo)){
                                index=cPicCacheIndex+i;
                                if(index<imgDiv.children.length){
                                    media2load=imgDiv.children[index];
                                    shadow2load=mediaShadow[index];
                                    if(!(shadow2load.photostovisStatus&statusFlags.FlagMediaLoaded))break;
                                    //perhaps this image needs reloading?
                                    if((belowMinRes=_isBelowMinimumResolution(index)))break;
                                }

                                index=cPicCacheIndex-i;
                                if(index>=0){
                                    media2load=imgDiv.children[index];
                                    shadow2load=mediaShadow[index];
                                    if(!(shadow2load.photostovisStatus&statusFlags.FlagMediaLoaded))break;
                                    //perhaps this image needs reloading?
                                    if((belowMinRes=_isBelowMinimumResolution(index)))break;
                                }
                            }// if (after for)
                        }//for
                        if(i<KCacheRadius+1){
                            console.log("Loading an image/video (not a thumbnail)");
                            shadow2load.photostovisStatus|=statusFlags.FlagMediaLoading;
                            if(belowMinRes){
                                console.assert(media2load.children.length===1);
                                newImg=new Image();
                                newImg.className='fsimg';
                                newImg.style.display='none';
                                media2load.appendChild(newImg);
                            }
                        } else {
                            //nothing to load
                            console.log("loadNextMedia: ALL IMAGES IN CACHE LOADED!");
                            //based on our direction prefetch an image
                            if(direction===1){
                                //going forward.
                                albumIndexNext=_getNextIndex(albumIndexNext);
                                if(albumIndexNext!==-1 && !(picsInAlbum[albumIndexNext].fl&PH.server.albumFlags.FlagIsVideo)){
                                    console.log("PREFETCHING image ID="+picsInAlbum[albumIndexNext].id+", priority="+(KCacheRadius+1));
                                    PH.sendWebsocketMessage("prefetchID="+picsInAlbum[albumIndexNext].id+" priority="+(KCacheRadius+1));
                                }
                            } else if(direction===-1){
                                //going backward.
                                albumIndexPrev=_getPrevIndex(albumIndexPrev);
                                if(albumIndexPrev!==-1 && !(picsInAlbum[albumIndexPrev].fl&PH.server.albumFlags.FlagIsVideo)){
                                    console.log("PREFETCHING image ID="+picsInAlbum[albumIndexPrev].id+", priority="+(KCacheRadius+1));
                                    PH.sendWebsocketMessage("prefetchID="+picsInAlbum[albumIndexPrev].id+" priority="+(KCacheRadius+1));
                                }
                            } else console.assert(direction===0);

                            console.groupEnd();
                            return;
                        }
                    }
                } else {
                    console.log("Loading the current media");
                    index=cPicCacheIndex;
                    media2load=imgDiv.children[index];
                    shadow2load=mediaShadow[index];
                    shadow2load.photostovisStatus|=statusFlags.FlagMediaLoading;
                    if(belowMinRes){
                        console.assert(media2load.children.length===1);
                        newImg=new Image();
                        newImg.className='fsimg';
                        newImg.style.display='none';
                        media2load.appendChild(newImg);
                    }
                }
            } else {
                console.log("Loading the current thumbnail");
                index=cPicCacheIndex;
                media2load=imgDiv.children[index];
                shadow2load=mediaShadow[index];
                shadow2load.photostovisStatus|=statusFlags.FlagThumbnailLoading;
            }

            //load the current media
            albumElm=picsInAlbum[shadow2load.photostovisI];
            shadow2load.photostovisPathStr=pathStr; //this is used to check if we have the same album
            shadow2load.photostovisID=albumElm.id;
            console.log("loadNextMedia: loading thumb/media with id="+shadow2load.photostovisID+", i="+shadow2load.photostovisI+" and index="+index);
            if(albumElm.fl&PH.server.albumFlags.FlagIsVideo){
                //loading the video
                console.log("Loading video: ");
                console.log(shadow2load);
                console.assert(shadow2load.photostovisStatus&statusFlags.FlagMediaLoading);

                media2load.innerHTML='<video controls class="fsvideo"><source src="'+
                        PH.createGetUrl(pathStr.replace('t','v')+shadow2load.photostovisID,null,false)+'"></video>';
                img2load=media2load.children[0];

                console.log("Video media state: "+img2load.readyState);
                //if(img2load.readyState)
                img2load.addEventListener('loadedmetadata',_metadataLoaded);
                img2load.preload='metadata';
                console.log("Metadata will be loaded for video with photostovisI: "+shadow2load.photostovisI);
            } else {
                //loading an image
                console.log("HHH media2load:");
                console.log(media2load);
                if(shadow2load.photostovisStatus&statusFlags.FlagMediaLoading)img2load=media2load.children[1];
                else if(shadow2load.photostovisStatus&statusFlags.FlagThumbnailLoading)img2load=media2load.children[0];
                else console.assert(0);
                
                img2load.addEventListener('load',_imageLoaded);
                 
                if(shadow2load.photostovisStatus&statusFlags.FlagMediaLoading){
                    img2load.src=PH.createGetUrl(pathStr.replace('t','i')+shadow2load.photostovisID,_getMinimumResolutionString(albumElm,null),false);
                    console.assert(typeof(shadow2load.photostovisLoadStartTime)==='undefined');
                    shadow2load.photostovisLoadStartTime=Date.now();
                    console.log("Starting loading picture ("+shadow2load.photostovisID+") at "+shadow2load.photostovisLoadStartTime);
                    loadingImg=img2load;
                    doPrefetching=true; //this will make the prefetch code below to run
                }else {                    
                    img2load.src=PH.createGetUrl(pathStr+shadow2load.photostovisID,PH.platform.pixelRatioTxt,false);
                }
            }

            if(doPrefetching){
                //prefetching next image
                albumIndexNext=albumIndexPrev=cPicAlbumIndex;
                shadow2load=null; //not really necessary
                for(i=1;i<KCacheRadius+1;i++){
                    //following if prevents videos to be loaded
                    albumIndexNext=_getNextIndex(albumIndexNext);
                    albumIndexPrev=_getPrevIndex(albumIndexPrev);
                    if(!(albumIndexNext!==-1 && picsInAlbum[albumIndexNext].fl&PH.server.albumFlags.FlagIsVideo) ||
                       !(albumIndexPrev!==-1 && picsInAlbum[albumIndexPrev].fl&PH.server.albumFlags.FlagIsVideo)){
                        index=cPicCacheIndex+i;
                        if(index<imgDiv.children.length){
                            shadow2load=mediaShadow[index];
                            if(!(shadow2load.photostovisStatus&statusFlags.FlagMediaLoaded) &&
                               !(shadow2load.photostovisStatus&statusFlags.FlagMediaLoading) &&
                               !(shadow2load.photostovisStatus&statusFlags.FlagMediaPrefetchSent))break;
                        }

                        index=cPicCacheIndex-i;
                        if(index>=0){
                            shadow2load=mediaShadow[index];
                            if(!(shadow2load.photostovisStatus&statusFlags.FlagMediaLoaded) &&
                               !(shadow2load.photostovisStatus&statusFlags.FlagMediaLoading) &&
                               !(shadow2load.photostovisStatus&statusFlags.FlagMediaPrefetchSent))break;
                        }
                    }// if (after for)
                }//for
                if(i<KCacheRadius+1){
                    shadow2load.photostovisStatus|=statusFlags.FlagMediaPrefetchSent;
                    if(typeof(shadow2load.photostovisID)!=='string')
                        shadow2load.photostovisID=picsInAlbum[shadow2load.photostovisI].id;
                    console.log("PREFETCHING image ID="+shadow2load.photostovisID+", priority="+i);
                    PH.sendWebsocketMessage("prefetchID="+shadow2load.photostovisID+" priority="+i);
                } else //nothing to prefetch
                    console.log("No image needs prefetching.");
            }//if(doPrefetching)

            console.groupEnd();
        },//loadNextMedia
        reloadThisPicture:function(resolution){
            var media2load,shadow2load,img2load;

            console.groupCollapsed("reloadThisPicture: current="+cPicAlbumIndex);
            console.assert(cPicAlbumIndex<picsInAlbum.length,"Current pic outside range: "+cPicAlbumIndex);
            console.assert(cPicCacheIndex>=0 && cPicCacheIndex<KCacheRadius+1+KCacheRadius,"cPicCacheIndex outside range");
            console.assert(cPicAlbumIndex>=0); //still in show pictures mode


            if(loadingImg){
                reloadImageResolution=resolution;
                console.log("!!! We already have a loading image! Wait until it loads.");
                console.groupEnd();
                return;
            }

            //if here no other image is loading, so we can load ours
            media2load=imgDiv.children[cPicCacheIndex];
            shadow2load=mediaShadow[cPicCacheIndex];
            console.assert(shadow2load.photostovisStatus&statusFlags.FlagMediaLoaded); //load buttons should only be enabled after the image is loaded
            shadow2load.photostovisStatus|=statusFlags.FlagMediaLoading;

            //load the current media
            shadow2load.photostovisPathStr=pathStr; //this is used to check if we have the same album
            console.assert(shadow2load.photostovisID===picsInAlbum[shadow2load.photostovisI].id);
            console.assert(!(picsInAlbum[shadow2load.photostovisI].fl&PH.server.albumFlags.FlagIsVideo));
            console.log("reloadThisPicture: reloading thumb/media with id="+shadow2load.photostovisID+", i="+shadow2load.photostovisI+" and resolution="+resolution);
            console.assert(media2load.children.length===1);

            //add a new child element
            media2load.appendChild(new Image());
            img2load=media2load.children[1];
            img2load.className='fsimg';
            img2load.style.display='none';

            img2load.addEventListener('load',_imageLoaded);

            img2load.src=PH.createGetUrl(pathStr.replace('t','i')+shadow2load.photostovisID,
                                         _getMinimumResolutionString(picsInAlbum[cPicAlbumIndex],resolution),false);
            console.assert(typeof(shadow2load.photostovisLoadStartTime)==='undefined');
            shadow2load.photostovisLoadStartTime=Date.now();
            console.log("Starting reloading picture ("+shadow2load.photostovisID+") at "+shadow2load.photostovisLoadStartTime);
            loadingImg=img2load;
            _showSpinner();
            this.toggleResolutionMenu();

            console.groupEnd();
        },//reloadThisPicture
        displayMedia:function(i){
            var j,ii;
            var p=null,r=null,s,targetLength;
            var timeNow=Date.now();

            console.assert(i>=0 && i<picsInAlbum.length,"i outside range: "+i);
            if(i<0 || i>=picsInAlbum.length)
        		sendErrorToServer(`Error in displayMedia, i=${i}, picsInAlbum.length=${picsInAlbum.length}.
picsInAlbum: ${JSON.encode(picsInAlbum)} `,location.href,0,0,null);
            
            console.assert(imgDiv.children.length<=KCacheRadius+1+KCacheRadius,"imgDiv.children.length is not less than "+
                           (KCacheRadius+1+KCacheRadius)+" but "+imgDiv.children.length);
            console.assert(typeof(picsInAlbum[i].t)==='undefined',"displayMedia: element is not picture!");
            console.assert(imgDiv.children.length===mediaShadow.length);
            console.log(" ");
            console.log("displayMedia++: current="+i);
            if(!requestStartTime)requestStartTime=timeNow;
            console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ["+i+"] Start displaying/processing after: "+(timeNow-requestStartTime)+"ms");
            PH.inPictureMode=true;
            
            if(widthOnEntry<0)
                widthOnEntry=document.body.clientWidth;

            //set cPicXXXIndex
            cPicAlbumIndex=i;
            cPicCacheIndex=-1; //we do not know it yet

            //find our element among the divs, if any
            for(j=0;j<mediaShadow.length;j++)
                if(mediaShadow[j].photostovisI===i){
                    cPicCacheIndex=j;
                    break;
                }
            if(cPicCacheIndex>=0){
                //remove too "small" elements from the front of the img array
                while(cPicCacheIndex>KCacheRadius){
                    //remove the media element
                    _removeElement(0);
                    //done with removing
                    if(showingImg>=0)showingImg--;
                    cPicCacheIndex--;
                }
                //remove all elements that are too "big"
                for(j=cPicCacheIndex+KCacheRadius+1;j<imgDiv.children.length;j++){
                    _removeElement(cPicCacheIndex+KCacheRadius+1);
                }
            } else {
                //array should be "empty", set the current element as the first and nullify the rest
                console.assert(imgDiv.children.length===0,imgDiv.children.length);
                console.assert(mediaShadow.length===0,mediaShadow.length);
                console.log("adding first element");
                imgDiv.appendChild(_newElement(i));
                mediaShadow.push(_newShadowElement(i));
                cPicCacheIndex=0;
            }
            console.assert(cPicCacheIndex>=0,"cPicCacheIndex is not >=0 but "+cPicCacheIndex);

            //adding elements in front
            while(cPicCacheIndex<KCacheRadius){
                //get the element index
                ii=_getPrevIndex(mediaShadow[0].photostovisI);
                if(ii<0)break; //we do not have a prev element

                imgDiv.insertBefore(_newElement(ii),imgDiv.children[0]);
                mediaShadow.unshift(_newShadowElement(ii));

                console.log("#### displayMedia: added to cache before current: "+ii);
                cPicCacheIndex++;
                if(showingImg>=0)showingImg++;
                console.log("Showing img: "+showingImg);
            }

            //change the elements after the current element
            console.assert(imgDiv.children.length-cPicCacheIndex<=KCacheRadius+1,"imgDiv.children.length="+imgDiv.children.length+", cPicCacheIndex="+cPicCacheIndex);
            targetLength=cPicCacheIndex+KCacheRadius+1; //KCacheRadius more elements after the current element (KCacheRadius+1, 1 is for current element)
            for(j=imgDiv.children.length;j<targetLength;j++){
                ii=_getNextIndex(mediaShadow[j-1].photostovisI);
                if(ii<0)break; //we do not have a next element
                imgDiv.appendChild(_newElement(ii));
                mediaShadow.push(_newShadowElement(ii));
                console.log("#### displayMedia: added to cache after current: "+ii);
            }
            console.assert(imgDiv.children.length<=KCacheRadius+1+KCacheRadius,imgDiv.children.length);
            console.assert(imgDiv.children.length===mediaShadow.length);

            //check if the IDs are in order AND remove video elements for elements that are too far
            for(j=0;j<imgDiv.children.length;j++){
                p=imgDiv.children[j];
                s=mediaShadow[j];
                console.assert(s.photostovisI>=0);
                if(j>0)
                    console.assert(mediaShadow[j-1].photostovisI<s.photostovisI,"not consecutive");
                //nullify the transition and assign a transform
                if(j!==cPicCacheIndex && j!==showingImg){
                    //->p.style.transition='';
                    _setTransform(p,'translate3d('+(j-cPicCacheIndex)+'00%,0,0)');
                }
                if(direction===0 && j===cPicCacheIndex){
                    console.assert(showingImg===-1,"showingImg="+showingImg);
                    p.style.opacity='0';
                    console.log("SETTING OPACITY TO ZERO for first element (child index: "+cPicCacheIndex+")");
                    console.assert(p.style.opacity==='0');
                }

                //is the current element a video elelemt?
                if((picsInAlbum[s.photostovisI].fl&PH.server.albumFlags.FlagIsVideo) && !(j-cPicCacheIndex<=1 && j-cPicCacheIndex>=-1)){
                    console.log("Clearing video.");
                    //console.log(p);
                    //console.log(s);
                    _clearElement(p);
                    s.photostovisStatus=0;
                }
                        
            }
            console.log("Current showingImg="+showingImg+", current picture index="+cPicCacheIndex+", cPicAlbumIndex="+cPicAlbumIndex);

            //check first the status of the current element
            if(picsInAlbum[cPicAlbumIndex].fl&PH.server.albumFlags.FlagIsVideo)
                this.showVideo(cPicCacheIndex,this.loadNextMedia);
            else
                this.showImg(cPicCacheIndex,this.loadNextMedia); 

            console.log("displayMedia--");
        },
        showImg:function(i,callback){
            console.assert(i>=0);
            console.assert(i===cPicCacheIndex);
            var r,c;
            var mapOptions,photoPos;
            var imgChildIndex=0;
            var imgNatWidth,imgNatHeight,imgNatTmp;
            var s=mediaShadow[i];
            var albumElm=picsInAlbum[s.photostovisI];
            var p=imgDiv.children[i];
            var timeNow,thumbnailOrPicture,renderingStartTime,loadAfterRendering;

            //check if we have something to show
            if(!(s.photostovisStatus&statusFlags.FlagThumbnailLoaded || s.photostovisStatus&statusFlags.FlagMediaLoaded)){
                //nothing to show, we should return.
                console.log("showImg: nothing to show, returning.");
                //but first, call the callback, if any
                if(callback)
                    setTimeout(callback,0);
                return;
            }
            console.assert(cPicCacheIndex>=0);
            
            //find out r
            if(s.photostovisStatus&statusFlags.FlagMediaLoaded){
                if(p.children.length===1){
                    //thumbnail was removed
                    r=p.children[0];
                } else {
                    //thumbnail was not removed YET
                    console.assert(p.children.length===2);
                    r=p.children[1];
                    imgChildIndex=1;
                    p.children[0].style.display='none';
                    p.children[1].style.display='';
                }
                thumbnailOrPicture='picture';
            } else {
                r=p.children[0];
                thumbnailOrPicture='thumbnail';
            }
            //we need this here because the screen size might have been changed since the picture was loaded, and a resize was triggered
            if(s.photostovisHeight<=0 || s.photostovisWidth<=0){
                console.log("Looks like we need to resize. s: ("+s.photostovisHeight+" "+s.photostovisWidth+")");
                console.log(s);
                _sizeImg(i,imgChildIndex);
                console.log("Resize done: s: ("+s.photostovisHeight+" "+s.photostovisWidth+")");
            }
            console.assert(r);

            console.log("showImg: photostovisI="+s.photostovisI+", current showImg="+showingImg);
            console.log("Status: "+s.photostovisStatus);
            console.log("Status medialoaded: "+(s.photostovisStatus&statusFlags.FlagMediaLoaded?'true':'false'));

            //show/hide things
            if(s.photostovisStatus&statusFlags.FlagMediaLoaded){
                _hideSpinner();
                picWatchStartTime=Date.now();
            } else {
                _showSpinner();
                thmbWatchStartTime=Date.now();
            }
            //hide video nav controls
            elmClear('video_nav_ctrls');

            //check what "improve resolution" buttons to disable
            if(s.photostovisStatus&statusFlags.FlagMediaLoaded){
                //this is the image
                imgNatWidth=r.naturalWidth>0?r.naturalWidth:r.width;
                imgNatHeight=r.naturalHeight>0?r.naturalHeight:r.height;
                if((albumElm.w>albumElm.h && imgNatWidth<imgNatHeight) || (albumElm.w<albumElm.h && imgNatWidth>imgNatHeight)){
                    //switch
                    imgNatTmp=imgNatWidth;
                    imgNatWidth=imgNatHeight;
                    imgNatHeight=imgNatTmp;
                }
                if(imgNatWidth===albumElm.w){
                    console.assert(imgNatHeight===albumElm.h);
                    //disable all buttons because this is the original image
                    elm('min-res-this-h').disabled=true;
                    elm('min-res-this-d').disabled=true;
                    elm('min-res-this-r').disabled=true;
                    elm('min-res-this-o').disabled=true;
                    //elm('min-res-this-o').disabled=false;
                    console.log("This is original image!");
                } else {
                    //not the original image, so scaled.
                    console.log("This is scaled image!");
                    if((screen.width>screen.height && imgNatWidth<imgNatHeight) || (screen.width<screen.height && imgNatWidth>imgNatHeight)){
                        //switch
                        imgNatTmp=imgNatWidth;
                        imgNatWidth=imgNatHeight;
                        imgNatHeight=imgNatTmp;
                    }

                    if(imgNatWidth>screen.width/2)
                        elm('min-res-this-h').disabled=true;
                    else
                        elm('min-res-this-h').disabled=false;
                    if(imgNatWidth>screen.width)
                        elm('min-res-this-d').disabled=true;
                    else
                        elm('min-res-this-d').disabled=false;
                    if(imgNatWidth>PH.platform.screenWidth)
                        elm('min-res-this-r').disabled=true;
                    else
                        elm('min-res-this-r').disabled=false;
                    elm('min-res-this-o').disabled=false;
                }
            } else {
                //this is the thumbnail. Disable all "improve" resolution buttons because we are getting our image
                elm('min-res-this-h').disabled=true;
                elm('min-res-this-d').disabled=true;
                elm('min-res-this-r').disabled=true;
                elm('min-res-this-o').disabled=true;
                console.log("This is thumbnail!");
            }

            if(showingImg===cPicCacheIndex){
                //we are replacing a thumbnail with an image, or a thumbnail/image with a rescaled thumbnail/image
                console.assert(typeof(s.photostovisImgText)==='string');
                console.assert(typeof(s.photostovisImgTextSmall)==='string');
                infobox.children[1].innerHTML=s.photostovisImgTextSmall;
                infobox.children[2].innerHTML=s.photostovisImgText;
                //nothing else needs to be done here
            } else {
                //hide the currently showing set of images, if any
                _hideShowing();
                showingImg=cPicCacheIndex;
                //show the right picture info
                console.assert(typeof(PH.userPrefsGlobal.showPicInfo)==='number',typeof(PH.userPrefsGlobal.showPicInfo));
                if(s.photostovisZoom!==2){
                    switch(PH.userPrefsGlobal.showPicInfo){
                    case 0:
                        //show the info icon
                        //we do not assign the info to children 1 and 2
                        infobox.children[0].style.display='';
                        infobox.children[1].style.display='none';
                        infobox.children[2].style.display='none';
                        infobox.children[3].style.display='none';
                        infobox.children[1].innerHTML='';
                        infobox.children[2].innerHTML='';
                        infobox.style.width="34px";
                        infobox.style.height="34px";
                        infobox.style.opacity=KInfoboxIconOpacity;
                        if(s.photostovisZoom===0){
                            r.style.top=s.photostovisTop+"px";
                            r.style.left=s.photostovisLeft+"px";
                        }
                        break;

                    case 1:
                        //show the mini-infobox
                        //update & show information
                        c=picsInAlbum[cPicAlbumIndex];
                        infobox.children[0].style.display='none';
                        infobox.children[1].style.display='';
                        infobox.children[2].style.display='none';
                        infobox.children[3].style.display='none';
                        console.assert(typeof(s.photostovisImgText)==='string');
                        console.assert(typeof(s.photostovisImgTextSmall)==='string');
                        infobox.children[1].innerHTML=s.photostovisImgTextSmall;
                        infobox.children[2].innerHTML=s.photostovisImgText;

                        //infobox.style.width="260px";
                        infobox.style.height="34px";
                        infobox.style.opacity="";
                        break;

                    case 2:
                        //update & show information
                        c=picsInAlbum[cPicAlbumIndex];
                        infobox.children[0].style.display='none';
                        infobox.children[1].style.display='none';
                        infobox.children[2].style.display='';
                        console.assert(typeof(s.photostovisImgText)==='string');
                        console.assert(typeof(s.photostovisImgTextSmall)==='string');
                        infobox.children[1].innerHTML=s.photostovisImgTextSmall;
                        infobox.children[2].innerHTML=s.photostovisImgText;
                        //map
                        if(typeof(c.lat)!=='undefined' && typeof(google)==='undefined')
                        	console.warn("We have latitude and longitude, bug Google Maps failed to load");
                        if(typeof(c.lat)!=='undefined' && typeof(google)!=='undefined' && !PH.platform.smallDevice){
                            console.assert(typeof(c.lng)!=='undefined');
                            console.log("We have GPS pos: lat="+(c.lat/1000000)+", lng="+c.lng/1000000);
                            if(!map){
                                mapOptions={
                                    center: {lat:c.lat/1000000, lng:c.lng/1000000},
                                    zoom: 15
                                };
                                map=new google.maps.Map(infobox.children[3],mapOptions);
                                photoMarker=new google.maps.Marker({map: map});
                            }
                            //display the photomap
                            infobox.children[3].style.display='';
                            google.maps.event.trigger(map,'resize');
                            //center the map
                            photoPos=new google.maps.LatLng(c.lat/1000000,c.lng/1000000);
                            map.setCenter(photoPos);
                            photoMarker.setPosition(photoPos);
                        } else infobox.children[3].style.display='none';
                        //infobox.style.width='';
                        infobox.style.width="360px";
                        infobox.style.height='';
                        infobox.style.opacity="";
                        if(s.photostovisZoom===0){
                            r.style.top=s.photostovisTop0+"px";
                            r.style.left=s.photostovisLeft0+"px";
                        }

                    }//switch
                    infobox.style.display='';
                } else
                    infobox.style.display='none';


                //set the right zoom image
                switch(s.photostovisZoom){
                case 0:
                    zoomOutElm.style.display='none';
                    zoomInElm.style.display='block';
                    break;
                case 1:
                    if(s.photostovisZ2Width>s.photostovisZ1Width){
                        zoomOutElm.style.display='none';
                        zoomInElm.style.display='block';
                    } else {
                        zoomInElm.style.display='none';
                        zoomOutElm.style.display='block';
                    }
                    break;
                case 2:
                    if(s.photostovisWidth>s.photostovisZ2Width){
                        zoomOutElm.style.display='none';
                        zoomInElm.style.display='block';
                    } else {
                        zoomInElm.style.display='none';
                        zoomOutElm.style.display='block';
                    }
                    break;
                }



                zoombox.style.display='';
                resobox.style.display='';

                //p.style.display='';//unhide the "master" element
                if(direction){
                    console.assert(_getTransform(p)!=='',_getTransform(p));
                    //->p.style.transition='transform 0.3s';
                    _setTransform(p,'');
                } else {
                    console.assert(p.style.opacity==='0',p.style.opacity);
                    //->p.style.transition='opacity 0.3s';
                    p.style.opacity='';
                }
            }

            //rendering part
            loadAfterRendering=true; //-> set to true to wait for full rendering before loading the next picture

            renderingStartTime=timeNow=Date.now();
            console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ["+cPicAlbumIndex+"] Start rendering ("+thumbnailOrPicture+") after: "+
                        (timeNow-requestStartTime)+"ms");

            function rendered(){
                //Render complete
                var renderingTime,renderingAverage,MPixels,oldAverage;
                timeNow=Date.now();
                renderingTime=timeNow-renderingStartTime;
                //renderingTime*=10; //-> for testing
                console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ["+cPicAlbumIndex+"] Rendering COMPLETE ("+thumbnailOrPicture+") after: "+
                            (timeNow-requestStartTime)+"ms. Rendering took "+renderingTime+"ms.");
                //do we show the green circle?
                if((direction===-1 && cPicCacheIndex>0 && typeof(mediaShadow[cPicCacheIndex-1])!=='undefined' &&
                    mediaShadow[cPicCacheIndex-1].photostovisStatus&statusFlags.FlagMediaLoaded) ||
                   (direction>=0 && cPicCacheIndex<KCacheRadius+KCacheRadius && typeof(mediaShadow[cPicCacheIndex+1])!=='undefined' &&
                    mediaShadow[cPicCacheIndex+1].photostovisStatus&statusFlags.FlagMediaLoaded))
                    elm('next-pic-ready').style.visibility='visible';
                if(thumbnailOrPicture==='picture' && direction){
                    //so we skip this for direction==0 because that is the first picture and it usually takes longer time to render
                    MPixels=imgNatWidth*imgNatHeight/1000000;
                    renderingAverage=renderingTime/MPixels;
                    console.log("Rendering time per MPixel for this image: "+renderingAverage+"ms. Image had "+MPixels.toFixed(2)+" MPixels");
                    oldAverage=PH.platform.renderingTimePerMPixel;
                    if(PH.platform.renderingTimePerMPixel)
                        PH.platform.renderingTimePerMPixel=(7*PH.platform.renderingTimePerMPixel+renderingAverage)/8;
                    else
                        PH.platform.renderingTimePerMPixel=renderingAverage;
                    console.log("New average: "+PH.platform.renderingTimePerMPixel.toFixed(2)+"ms. Old average: "+oldAverage.toFixed(2)+"ms.");

                    //if we have a 10% +/- difference from the last value sent, we send the new value to the server
                    if(PH.platform.renderingTimePerMPixel*1.1>PH.platform.lastSentRenderingTimePerMPixel ||
                            PH.platform.renderingTimePerMPixel*0.9<PH.platform.lastSentRenderingTimePerMPixel){
                        //send a new average to the server
                        console.log("Sending a new average to the server: "+PH.platform.renderingTimePerMPixel);
                        PH.platform.lastSentRenderingTimePerMPixel=PH.platform.renderingTimePerMPixel;
                        PH.sendWebsocketMessage("renderingTimePerMPixel="+PH.platform.lastSentRenderingTimePerMPixel.toFixed(0));
                    }
                }

                if(loadAfterRendering && callback)
                    setTimeout(callback,0);
            }//rendered
            function startRender(){
                //Rendering start
                requestAnimationFrame(rendered);
            }

            requestAnimationFrame(startRender);
            if(!loadAfterRendering && callback)
                setTimeout(callback,0);
        },
        showVideo:function(i,callback){
            console.assert(i>=0);
            var p,s,e;

            p=imgDiv.children[i];
            s=mediaShadow[i];
            
            //hide the currently showing set of images, if any
            _hideShowing();
            //hide zoombox
            zoombox.style.display='none';
            //hide infobox
            infobox.style.display='none';
            //hide resobox
            resobox.style.display='none';

            //check if we have something to show
            if(!(s.photostovisStatus&statusFlags.FlagMediaLoaded)){
                //nothing to show yet
                console.log("showVideo: nothing to show, yet.");
                _showSpinner();
            } else {
                showingImg=cPicCacheIndex;
                _hideSpinner();

                //p.style.display='';//unhide it
                if(direction){
                    console.assert(_getTransform(p)!=='',_getTransform(p));
                    //->p.style.transition='transform 0.3s';
                    _setTransform(p,'');
                } else {
                    console.assert(p.style.opacity==='0',p.style.opacity);
                    //->p.style.transition='opacity 0.3s';
                    p.style.opacity='';
                }
            }
            console.assert(cPicCacheIndex>=0);
            console.log("showVideo: photostovisI="+s.photostovisI);     

            //show video nav controls, but only if this is NOT touch device. For touch we show them only on click
            if(PH.platform.isTouch){
                e=elm('video_nav_prev');
                e.style.transition='';
                e.style.opacity='0';
                e=elm('video_nav_next');
                e.style.transition='';
                e.style.opacity='0';
                e=elm('video_nav_close');
                e.style.transition='';
                e.style.opacity='0';

                PH.videoctrls.showing=false;
                PH.videoctrls.active=false;
            } else {
                PH.showVideoControls();
            }
            console.log("showing/enabling video controls.");
            elmShow('video_nav_ctrls');

            if(callback)
                setTimeout(callback,0);
        },
        getOut:function(){
            direction=0;
            _hideShowing();
            _hideSpinner();

            console.log("albumCache::getOut++");
            cPicAlbumIndex=-1;
            requestStartTime=null;
            PH.path[PH.path.length-1].index=-1;
            PH.inPictureMode=false;
            
            //exit full screeen, then continue
            setTimeout(function(){
                PH.exitFullScreen();
                //hide the picture mode div
                elm('imgdiv').style.display='';
                elm('fullscreendiv').style.display='';
                //do we have the same width on exit as on entry?
                if(widthOnEntry!==document.body.clientWidth){
                    //no, we don't. We need to redisplay the album
                    PH.computeThumbnailStrip();
                    PH.displayAlbum(true); //"true" preserves the position
                } else {
                    //display the album, if needed
                    if(typeof(PH.path[PH.path.length-1].forceDisplay)==='boolean' && PH.path[PH.path.length-1].forceDisplay){
                        delete PH.path[PH.path.length-1].forceDisplay;
                        PH.displayAlbum();
                    } else {
	                    PH.albumCache.reset(); //this is clear+init but more efficient
	                    if(PH.loadingThumbnailsStartTime){
	                        //if non-zero it means that thumbnails did not finish loading
	                        PH.loadingThumbnails=[];
	                        PH.skipInvisible=true;
	                        PH.loadingThumbnailsStartTime=Date.now();
	                        PH.currentScrollTop=elm('cntnt').scrollTop;
	                        PH.loadThumbnails();
	                    }
	                }
                }
            },100);
            console.log("albumCache::getOut--");
        },
        showNext:function(){
            if(cPicAlbumIndex<0)return;
            var picWatchTime=-1,thmbWatchTime=-1;
            requestStartTime=Date.now();
            if(picWatchStartTime)picWatchTime=requestStartTime-picWatchStartTime;
            if(thmbWatchStartTime){
                if(picWatchStartTime)thmbWatchTime=picWatchStartTime-thmbWatchStartTime;
                else thmbWatchTime=requestStartTime-thmbWatchStartTime; //the user watched the thumbnail all the time (picture was not loading)
            }
            if(showingImg>=0)
                PH.sendWebsocketMessage("watchedID="+mediaShadow[showingImg].photostovisID+" picWT="+picWatchTime+" thmbWT="+thmbWatchTime);
            picWatchStartTime=thmbWatchStartTime=null;
            direction=1;
            PH.path[PH.path.length-1].index=_getNextIndex(cPicAlbumIndex); //returned result can be -1
            PH.setHash();
            //hide the green circle
            elm('next-pic-ready').style.visibility='';
        },
        showPrev:function(){
            if(cPicAlbumIndex<0)return;
            var picWatchTime=-1,thmbWatchTime=-1;
            requestStartTime=Date.now();
            if(picWatchStartTime)picWatchTime=requestStartTime-picWatchStartTime;
            if(thmbWatchStartTime){
                if(picWatchStartTime)thmbWatchTime=picWatchStartTime-thmbWatchStartTime;
                else thmbWatchTime=requestStartTime-thmbWatchStartTime; //the user watched the thumbnail all the time (picture was not loading)
            }
            if(showingImg>=0)
                PH.sendWebsocketMessage("watchedID="+mediaShadow[showingImg].photostovisID+" picWT="+picWatchTime+" thmbWT="+thmbWatchTime);
            picWatchStartTime=thmbWatchStartTime=null;
            direction=-1;
            PH.path[PH.path.length-1].index=_getPrevIndex(cPicAlbumIndex); //returned result can be -1
            PH.setHash();
            //hide the green circle
            elm('next-pic-ready').style.visibility='';
        },
        getPathStr:function(){
            return pathStr;
        },
        currentIsVideo:function(){
            if(cPicAlbumIndex>=0 && (picsInAlbum[cPicAlbumIndex].fl&PH.server.albumFlags.FlagIsVideo))
                return true;
            return false;
        },
        currentElmZoom:function(){
            if(cPicCacheIndex>=0)
                return mediaShadow[cPicCacheIndex].photostovisZoom;
            return -1;
        },
        toggleInfobox:function(){
            if(cPicCacheIndex<0)return false; //no toggle
            var p=imgDiv.children[cPicCacheIndex],s=mediaShadow[cPicCacheIndex],r,c;
            console.assert(s.photostovisZoom!==2);
            console.assert(cPicAlbumIndex>=0 && !(picsInAlbum[cPicAlbumIndex].fl&PH.server.albumFlags.FlagIsVideo));
            var btm,rgt;
            var mapOptions,photoPos;

            //if here, we are inside the box
            console.assert(typeof(PH.userPrefsGlobal.showPicInfo)==='number',typeof(PH.userPrefsGlobal.showPicInfo));
            switch(PH.userPrefsGlobal.showPicInfo){
            case 0:
                //show mini-infobox
                PH.userPrefsGlobal.showPicInfo=1;
                PH.saveGlobalPreferences();
                //do we have the texts set?
                if(!infobox.children[1].innerHTML){
                    infobox.children[1].innerHTML=s.photostovisImgTextSmall;
                    infobox.children[2].innerHTML=s.photostovisImgText;
                }

                //move the infobox outside of our screen
                infobox.style.transition='';
                infobox.style.right="-400px";
                //"show" the infobox
                infobox.children[0].style.display="none";
                infobox.children[1].style.display="";
                //infobox.style.width="260px";
                infobox.style.width="";
                infobox.style.height="34px";
                //compute the starting right and bottom values
                btm=5+34-infobox.clientHeight;
                rgt=5+34-infobox.clientWidth;
                infobox.style.right=rgt+"px";
                infobox.style.bottom=btm+"px";
                infobox.style.opacity="";

                setTimeout(function(){
                    //prepare the transition
                    infobox.style.transition="bottom 0.5s ease-out, right 0.5s ease-out";
                    //apply changes
                    infobox.style.bottom="5px";
                    infobox.style.right="5px";

                    setTimeout(function(){
                        infobox.style.transition="";
                    },550);
                },0);


                break;

            case 1:
                //show the full infobox
                PH.userPrefsGlobal.showPicInfo=2;
                PH.saveGlobalPreferences();
                //we should have the right info
                console.assert(infobox.children[1].innerHTML!=='');
                infobox.style.transition="height 0.5s ease-out";

                //do we show a map?
                c=picsInAlbum[cPicAlbumIndex];
                //TODO: attention: for small devices we do NOT show a map
                if(typeof(c.lat)!=='undefined' && typeof(google)!=='undefined' && !PH.platform.smallDevice){
                    console.assert(typeof(c.lng)!=='undefined');
                    console.log("We have GPS pos: lat="+(c.lat/1000000)+", lng="+c.lng/1000000);
                    if(!map){
                        mapOptions={
                            center: {lat:c.lat/1000000, lng:c.lng/1000000},
                            zoom: 15
                        };
                        map=new google.maps.Map(infobox.children[3],mapOptions);
                        photoMarker=new google.maps.Marker({map: map});
                    }
                    //display the photomap
                    infobox.children[3].style.display='';
                    google.maps.event.trigger(map,'resize');
                    //center the map
                    photoPos=new google.maps.LatLng(c.lat/1000000,c.lng/1000000);
                    map.setCenter(photoPos);
                    photoMarker.setPosition(photoPos);
                } else infobox.children[3].style.display='none';

                //"show" the infobox
                infobox.style.width="360px";
                infobox.children[0].style.display="none";
                infobox.children[1].style.display="none";
                infobox.children[2].style.display="";

                //we need a px value, otherwise the transition does not work.
                //we have +16 because 16 is the table margin
                infobox.style.height=(infobox.children[2].clientHeight+infobox.children[3].clientHeight+16)+'px';
				infobox.style.opacity="";


                setTimeout(function(){
                    if(s.photostovisZoom===0){
                        r=p.children[0];
                        r.style.transition="top 0.5s ease-out, left 0.5s ease-out";
                        r.style.top=s.photostovisTop0+"px";
                        r.style.left=s.photostovisLeft0+"px";
                    }

                    setTimeout(function(){
                        infobox.style.transition="";
                        infobox.style.height="";
                        p.children[0].style.transition="";
                    },550);
                },0);
                break;

            case 2:
                //hide the infobox
                PH.userPrefsGlobal.showPicInfo=0;
                PH.saveGlobalPreferences();
                //prepare the transition
                infobox.style.transition="bottom 0.5s ease-out, right 0.5s ease-out";
                btm=5+34-infobox.clientHeight;
                rgt=5+34-infobox.clientWidth;
                //apply changes
                infobox.style.bottom=btm+"px";
                infobox.style.right=rgt+"px";
                infobox.style.opacity=KInfoboxIconOpacity;

                if(s.photostovisZoom===0){
                    r=p.children[0];
                    r.style.transition="top 0.5s ease-out, left 0.5s ease-out";
                    r.style.top=s.photostovisTop+"px";
                    r.style.left=s.photostovisLeft+"px";
                } //else we do not change the position

                setTimeout(function(){
                    infobox.children[0].style.display="";
                    infobox.children[1].style.display="none";
                    infobox.children[2].style.display="none";
                    infobox.children[3].style.display="none";

                    infobox.style.width="34px";
                    infobox.style.height="34px";
                    infobox.style.bottom="5px";
                    infobox.style.right="5px";
                    infobox.style.transition="";
                    p.children[0].style.transition="";
                },500);
                break;

            }//switch

            return true;
        },//togleInfobox
        toggleZoom:function(){
            if(cPicCacheIndex<0)return false; //no toggle
            var p=imgDiv.children[cPicCacheIndex],s=mediaShadow[cPicCacheIndex],r,c;
            var cssText;
            console.assert(cPicAlbumIndex>=0 && !(picsInAlbum[cPicAlbumIndex].fl&PH.server.albumFlags.FlagIsVideo));

            if(typeof(s.photostovisWidth)!=='number'){
                console.assert(typeof(s.photostovisHeight)!=='number');
                return false; //no toggle, there is no image
            } else {
                console.assert(typeof(s.photostovisHeight)==='number');
                console.assert(typeof(s.photostovisZ1Width)==='number');
                console.assert(typeof(s.photostovisZ1Height)==='number');
                console.assert(typeof(s.photostovisZ2Width)==='number');
                console.assert(typeof(s.photostovisZ2Height)==='number');
            }
            if(s.photostovisWidth<=0){
                console.assert(s.photostovisHeight<=0);
                return false; //no toggle, there is no image
            } else {
                console.assert(s.photostovisHeight>0);
                console.assert(s.photostovisZ1Width>0);
                console.assert(s.photostovisZ1Height>0);
                console.assert(s.photostovisZ2Width>0);
                console.assert(s.photostovisZ2Height>0);
            }

            if(s.photostovisRot.length)
                cssText='transform:'+s.photostovisRot+';image-orientation: none;';
            else cssText='';

            switch(s.photostovisZoom){
            case 0:
                p.children[0].style.cssText=cssText+'width:'+s.photostovisZ1Width+'px; height:'+s.photostovisZ1Height+
                        'px; top:'+s.photostovisZ1Top+'px; left:'+s.photostovisZ1Left+'px;';
                p.scrollTop=s.photostovisZ1ScrollTop;
                p.scrollLeft=s.photostovisZ1ScrollLeft;
                s.photostovisZoom=1;

                //we are showing zoom-in, right?
                console.assert(s.photostovisZ1Width>=s.photostovisWidth);
                //Should we change to zoom out?
                if(s.photostovisZ2Width<s.photostovisZ1Width){
                    //change to zoom-out
                    zoomInElm.style.display='none';
                    zoomOutElm.style.display='block';
                }

                break;
            case 1:
                p.children[0].style.cssText=cssText+'width:'+s.photostovisZ2Width+'px; height:'+s.photostovisZ2Height+
                        'px; top:'+s.photostovisZ2Top+'px; left:'+s.photostovisZ2Left+'px;';
                p.scrollTop=s.photostovisZ2ScrollTop;
                p.scrollLeft=s.photostovisZ2ScrollLeft;
                s.photostovisZoom=2;

                //change what zoom is showing?
                if(s.photostovisZ2Width<s.photostovisZ1Width && s.photostovisZ2Width<s.photostovisWidth){
                    zoomOutElm.style.display='none';
                    zoomInElm.style.display='block';
                }
                if(s.photostovisZ2Width>s.photostovisZ1Width && s.photostovisZ2Width>s.photostovisWidth){
                    zoomInElm.style.display='none';
                    zoomOutElm.style.display='block';
                }
                //hide the infobox
                infobox.style.display='none';
                break;
            case 2:
                if(PH.userPrefsGlobal.showPicInfo===2)
                    cssText+='top:'+s.photostovisTop0+'px;left:'+s.photostovisLeft0+'px;';
                else
                    cssText+='top:'+s.photostovisTop+'px;left:'+s.photostovisLeft+'px;';
                p.children[0].style.cssText=cssText+'width:'+s.photostovisWidth+'px; height:'+s.photostovisHeight+'px;';
                s.photostovisZoom=0;

                //change what zoom is showing?
                console.assert(s.photostovisZ1Width>=s.photostovisWidth);
                if(s.photostovisZ2Width>s.photostovisWidth){
                    zoomOutElm.style.display='none';
                    zoomInElm.style.display='block';
                }

                //show the infobox
                infobox.style.display='';
                break;
            }//switch

            return true;
        },//togleZoom
        toggleResolutionMenu:function(){
            if(cPicCacheIndex<0)return false; //no toggle
            var minResContainerElm=elm('min-res-container');

            if(minResContainerElm.style.display!=='block')
                minResContainerElm.style.display='block';
            else
                minResContainerElm.style.display='';
        },//openResolutionMenu
        setMinimumResolution:function(resolution){
            requestedMinResolution=resolution;

            if(resolution!=='n')
                this.loadNextMedia();
            //this.toggleResolutionMenu();
        }//setMinimumResolution
    };//returned object    
})();
