"use strict";

PH.configWizard=function(canCancel){
    console.assert(typeof(PH.userSession.config)==='object' && PH.userSession.config!==null,typeof(PH.userSession.config));
    console.assert(typeof(canCancel)==='boolean');
    elmShow('config-p1');
    elmShow('config_bg');
    setTimeout(function(){
	    elm('config_bg').style.backgroundColor='rgba(0,0,0,0.8)';
	    elm('config-box').style.opacity='1';
	},100);
	
    PH.userSession.config.canCancel=canCancel;
    console.log(PH.userSession.config);
    // -> for testing PH.server.metadata.un=undefined;
    if(canCancel){
        //show all the cancel buttons
        elmClear('config-p1-cancel');
        elmClear('config-p2s-cancel');
        elmClear('config-p2a-cancel');
        elmClear('config-p3a-cancel');
        elmClear('config-users-cancel');
        elmClear('config-register-cancel');
        elmClear('config-summary-cancel');
    } else {
        //hide all cancel buttons
        elmHide('config-p1-cancel');
        elmHide('config-p2s-cancel');
        elmHide('config-p2a-cancel');
        elmHide('config-p3a-cancel');
        elmHide('config-users-cancel');
        elmHide('config-register-cancel');
        elmHide('config-summary-cancel');
    }

    //populate users table
    var i,nrUsers;
    var authUsers;

    if(typeof(PH.userSession.config.authUsers)!=='object' || !PH.userSession.config.authUsers)
        PH.userSession.config.authUsers=[];

    authUsers=PH.userSession.config.authUsers;
    if(authUsers.length>PH.KConfigNrUserEntries)nrUsers=PH.KConfigNrUserEntries;
    else nrUsers=authUsers.length;

    for(i=0;i<nrUsers;i++){
        elm('config-users-un'+i).value=authUsers[i].username;
        //give some fake values to the passwords so that they do not look empty
        elm('config-users-pw'+i).value=elm('config-users-pwa'+i).value=PH.KFakeOriginalPw;
        authUsers[i].pwWasChanged=false;
    }
    for(i=nrUsers;i<PH.KConfigNrUserEntries;i++){
        elm('config-users-del'+i).disabled=true;
        elm('config-users-perm'+i).disabled=true;
    }

    //select the proper values for a2 screen
    if(PH.userSession.config.cfgFlags&PH.server.configFlags.CfgFlagLanAuthRequired)
        elm('config-p2a-lan-signed').checked=true;
    else
        elm('config-p2a-lan-anybody').checked=true;

    if(PH.userSession.config.cfgFlags&PH.server.configFlags.CfgFlagInetNotAllowed)
        elm('config-p2a-inet-nobody').checked=true;
    else if(PH.userSession.config.cfgFlags&PH.server.configFlags.CfgFlagInetAuthNotRequired)
        elm('config-p2a-inet-anybody').checked=true;
    else
        elm('config-p2a-inet-signed').checked=true;
}

PH.configWizardPasswordChanged=function(elm){
    var n=elm.attributes['id'].value.slice(-1);
    if(PH.userSession.config.authUsers.length>n)
        PH.userSession.config.authUsers[n].pwWasChanged=true;
}
PH.configWizardServerNameChanged=function(){
    var r;
    var txt=elm('config-register-un').value;
    var uidQuery='';
    elmHTML('config-register-srv',txt);

    if(txt.length>=PH.KMinServernameLength){
        if(!txt.search(/^\w{5,}$/)){
            if(typeof(PH.userSession.config.lastCheckedName)!=='string' || PH.userSession.config.lastCheckedName!==txt){
                elm('config-register-next').disabled=true;
                if(typeof(PH.userSession.config.uid)==='string' && PH.userSession.config.uid.length===32)
                    uidQuery='&uid='+PH.userSession.config.uid;
                ajaxJson('https://photostovis.net/checkun.php?un='+txt+uidQuery,null,function(obj){
                    console.assert(typeof(obj)==='object' && typeof(obj.a)==='boolean');
                    //do we match the current value? If not, do nothing
                    if(txt!==elm('config-register-un').value)return;
                    PH.userSession.config.lastCheckedName=txt;
                    if(obj.a){
                        console.log('ServerName '+txt+' is available.');
                        elmHTML('config-register-error','&nbsp;');
                        elm('config-register-next').disabled=false; //we can continue

                    } else {
                        console.log('ServerName '+txt+' is NOT available.');
                        elmHTML('config-register-error','This server name ('+txt+') is not available. Please choose something else.');
                        elm('config-register-error').style.color='red';
                    }
                });
            }
        } else {
            elmHTML('config-register-error','Only letters, numbers and the underscore (_) characters are allowed.');
            elm('config-register-error').style.color='red';
            elm('config-register-next').disabled=true;
        }
    } else {
        //text too short
        elmHTML('config-register-error','The server name should have at least 5 characters.');
        elm('config-register-error').style.color='#A0A0A0';
        elm('config-register-next').disabled=true;
    }
};

PH.configWizardShowLanUpShCoScreen=function(){
    if(elm('config-p2a-lan-signed').checked)return false;
    return true;
}

PH.configWizardShowUsersScreen=function(){
    if(elm('config-p2a-lan-anybody').checked &&
       elm('config-p2a-inet-nobody').checked &&
       elm('config-p3a-operations-anybody').checked &&
       PH.userSession.config.authUsers.length===0){
        //if here, there is no need to show the "add users" dialog and we can go to the next thing
        return false;
    }
    return true;
}
PH.configWizardCanLeaveUsersScreen=function(checkNumber){
    var i,j,un,pw,pwa,t=elm('config-users-title');
    var nrValidUsers=0;

    //verify usernames & passwords:
    for(i=0;i<PH.KConfigNrUserEntries;i++){
        un=elm('config-users-un'+i);
        pw=elm('config-users-pw'+i);
        pwa=elm('config-users-pwa'+i);
        if(!un.value.length && !pw.value.length && !pwa.value.length)continue;

        //are there passwords without usernames?
        if(un.value.length===0){
            t.innerHTML='Missing username';
            t.style.color='red';
            un.focus();
            return false;
        }

        //are there usernames without passwords?
        if(!pw.value.length){
            t.innerHTML='Missing password';
            t.style.color='red';
            pw.focus();
            return false;
        }

        //are there non-matching passwords?
        if(pw.value!==pwa.value){
            t.innerHTML='Passwords do not match';
            t.style.color='red';
            pwa.focus();
            return false;
        }

        //are there too short usernames? Minimum length: PH.KMinUsernameLength
        if(un.value.length<PH.KMinUsernameLength){
            t.innerHTML='Username too short (minimum length: '+PH.KMinUsernameLength+')';
            t.style.color='red';
            un.focus();
            return false;
        }

        //are there identical usernames?
        for(j=i+1;j<PH.KConfigNrUserEntries;j++)
            if(elm('config-users-un'+j).value===un.value){
                t.innerHTML='Duplicate username';
                t.style.color='red';
                elm('config-users-un'+j).focus();
                return false;
            }

        //are there too short passwords? Minimum length: PH.KMinPasswordLength
        if(pw.value.length<PH.KMinPasswordLength){
            t.innerHTML='Password too short (minimum length: '+PH.KMinPasswordLength+')';
            t.style.color='red';
            pw.focus();
            return false;
        }

        //are there usernames with illegal characters?
        if(un.value.search(/^\w{5,}$/)){
            t.innerHTML='Only letters, numbers and the underscore (_) characters are allowed in usernames.';
            t.style.color='red';
            un.focus();
            return false;
        }

        //this is a valid user
        nrValidUsers++;
    }

    if(checkNumber && !nrValidUsers && (elm('config-p2a-lan-signed').checked || elm('config-p2a-inet-signed').checked)){
        //we need to have at least one registered user
        if(elm('config-p2a-lan-signed').checked)
            t.innerHTML='At least one user has to be created, because access from LAN is only possible for signed users.';
        else
            t.innerHTML='At least one user has to be created, because access from Internet is only possible for signed users.';
        t.style.color='red';
        return false;
    }

    //if here, everything is ok
    t.innerHTML='Registered users';
    t.style.color='';
    return true; //everything is ok
}

PH.configWizardShowRegisterScreen=function(){
    if(typeof(PH.server.metadata.un)==='string' && PH.server.metadata.un.length)
        return false;//already registered

    //do we need to register? If the user said "No access from outside" we do not register.
    if(!PH.userSession.config.simple && elm('config-p2a-inet-nobody').checked)
        return false; //no access from outside

    return true;
}

PH.configWizardDisplaySummaryPage=function(){
    var txt,i;
    if(PH.userSession.config.simple){
        elmClear('config-summary-lan-anybody1');
        elmClear('config-summary-lan-anybody2');
        elmHide('config-summary-lan-mixed1');
        elmHide('config-summary-lan-mixed2');
        elmHide('config-summary-lan-mixed3');
        elmHide('config-summary-lan-onlysigned');

        elmHide('config-summary-inet-mixed1');
        elmHide('config-summary-inet-mixed2');
        elmHide('config-summary-inet-mixed3');
        elmClear('config-summary-inet-onlysigned');
        elmHide('config-summary-inet-noaccess');
    } else {
        //what should we show for the LAN users?
        if(elm('config-p2a-lan-anybody').checked && elm('config-p3a-operations-anybody').checked){
            elmClear('config-summary-lan-anybody1');
            elmClear('config-summary-lan-anybody2');
            elmHide('config-summary-lan-mixed1');
            elmHide('config-summary-lan-mixed2');
            elmHide('config-summary-lan-mixed3');
            elmHide('config-summary-lan-onlysigned');
        } else if(elm('config-p2a-lan-anybody').checked && elm('config-p3a-operations-signed').checked){
            elmHide('config-summary-lan-anybody1');
            elmHide('config-summary-lan-anybody2');
            elmClear('config-summary-lan-mixed1');
            elmClear('config-summary-lan-mixed2');
            elmClear('config-summary-lan-mixed3');
            elmHide('config-summary-lan-onlysigned');
        } else {
            console.assert(!elm('config-p2a-lan-anybody').checked);
            elmHide('config-summary-lan-anybody1');
            elmHide('config-summary-lan-anybody2');
            elmHide('config-summary-lan-mixed1');
            elmHide('config-summary-lan-mixed2');
            elmHide('config-summary-lan-mixed3');
            elmClear('config-summary-lan-onlysigned');
        }

        //what should we show for the Internet users?
        if(elm('config-p2a-inet-anybody').checked){
            elmClear('config-summary-inet-mixed1');
            elmClear('config-summary-inet-mixed2');
            elmClear('config-summary-inet-mixed3');
            elmHide('config-summary-inet-onlysigned');
            elmHide('config-summary-inet-noaccess');
        } else if(elm('config-p2a-inet-signed').checked){
            elmHide('config-summary-inet-mixed1');
            elmHide('config-summary-inet-mixed2');
            elmHide('config-summary-inet-mixed3');
            elmClear('config-summary-inet-onlysigned');
            elmHide('config-summary-inet-noaccess');
        } else {
            console.assert(elm('config-p2a-inet-nobody').checked)
            elmHide('config-summary-inet-mixed1');
            elmHide('config-summary-inet-mixed2');
            elmHide('config-summary-inet-mixed3');
            elmHide('config-summary-inet-onlysigned');
            elmClear('config-summary-inet-noaccess');
        }
    }

    //users
    txt=null;
    for(i=0;i<PH.KConfigNrUserEntries;i++){
        if(elm('config-users-un'+i).value.length>0){
            if(txt)txt+=', '+elm('config-users-un'+i).value;
            else txt=elm('config-users-un'+i).value;
        }
    }
    if(txt){
        elmHTML('config-summary-users-list',txt);
        elmClear('config-summary-users');
        elmClear('config-summary-users-list-p');
    } else {
        elmHide('config-summary-users');
        elmHide('config-summary-users-list-p');
    }

    //registration name
    txt=null;
    if(typeof(PH.server.metadata.un)!=='string' && elm('config-register-un').value.length>=PH.KMinServernameLength)
        txt=elm('config-register-un').value;
    if(txt){
        elmHTML('config-summary-register-servername',txt);
        elmClear('config-summary-register');
        elmClear('config-summary-register-servername-p');
    } else {
        elmHide('config-summary-register');
        elmHide('config-summary-register-servername-p');
    }


    elmShow('config-summary');
}

PH.bindConfig=function(){
    var i,e;
    //page 1: next & cancel
    elm('config-p1-next').addEventListener(this.platform.clickEvent,function(){
        var e=elm('config-p1-title');
        console.assert(typeof(PH.userSession.config)==='object' && PH.userSession.config!==null);
        if(elm('config-p1-simple').checked){
            //show simple configuration
            elmHide('config-p1');
            elmShow('config-p2s');
            e.innerHTML='Welcome to Photostovis!';
            e.style.color='black';
            PH.userSession.config.simple=true;
        } else if(elm('config-p1-advanced').checked){
            //show advanced configuration
            elmHide('config-p1');
            elmShow('config-p2a');
            e.innerHTML='Welcome to Photostovis!';
            e.style.color='black';
            PH.userSession.config.simple=false;
        } else {
            //nothing is checked
            e.innerHTML='Please check one!';
            e.style.color='red';
        }
    },false);
    elm('config-p1-cancel').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-p1');
        elmHide('config_bg');
    },false);

    //page 2a: prev, cancel & next
    elm('config-p2a-prev').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-p2a');
        elmShow('config-p1');
    },false);
    elm('config-p2a-cancel').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-p2a');
        elmHide('config_bg');
    },false);
    elm('config-p2a-next').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-p2a');
        if(PH.configWizardShowLanUpShCoScreen())
            elmShow('config-p3a');
        else {
            console.assert(PH.configWizardShowUsersScreen());
            elmShow('config-users');
        }
    },false);
    //page 3a: prev, cancel & next
    elm('config-p3a-prev').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-p3a');
        elmShow('config-p2a');
    },false);
    elm('config-p3a-cancel').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-p3a');
        elmHide('config_bg');
    },false);
    elm('config-p3a-next').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-p3a');

        //are we going to show the "add users" screen?
        if(PH.configWizardShowUsersScreen())
            elmShow('config-users');
        else if(PH.configWizardShowRegisterScreen())
            elmShow('config-register');
        else
            PH.configWizardDisplaySummaryPage();
    },false);

    //page 2s: prev, cancel & next
    elm('config-p2s-prev').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-p2s');
        elmShow('config-p1');
    },false);
    elm('config-p2s-cancel').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-p2s');
        elmHide('config_bg');
    },false);
    elm('config-p2s-next').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-p2s');
        elmShow('config-users');
    },false);

    //page users: prev, cancel & next
    elm('config-users-prev').addEventListener(this.platform.clickEvent,function(){
        if(!PH.configWizardCanLeaveUsersScreen(false))
            return;//data inconsistent

        elmHide('config-users');
        if(PH.userSession.config.simple)
            elmShow('config-p2s');
        else if(PH.configWizardShowLanUpShCoScreen())
            elmShow('config-p3a');
        else
            elmShow('config-p2a');
    },false);
    elm('config-users-cancel').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-users');
        elmHide('config_bg');
    },false);
    elm('config-users-next').addEventListener(this.platform.clickEvent,function(){
        if(!PH.configWizardCanLeaveUsersScreen(true))
            return;//data inconsistent

        elmHide('config-users');
        //are we registered? Do we show the register page or jump directly to summary
        if(PH.configWizardShowRegisterScreen())
            elmShow('config-register');
        else
            PH.configWizardDisplaySummaryPage();
    },false);
    //page users: buttons
    for(i=0;i<PH.KConfigNrUserEntries;i++){
        //disable/enable the buttons if the username is empty or not
        elm('config-users-un'+i).addEventListener('blur',function(ev){
            var n=this.attributes['id'].value.slice(-1);
            var authUsers;
            if(this.value.length>0)
                elm('config-users-del'+n).disabled=elm('config-users-perm'+n).disabled=false;
            else
                elm('config-users-del'+n).disabled=elm('config-users-perm'+n).disabled=true;

            //if the username is NOT the same and the password has not been changed, reset the password
            if(PH.userSession.config.authUsers.length>n){
                authUsers=PH.userSession.config.authUsers;
                if(this.value!==authUsers[n].username && !authUsers[n].pwWasChanged){
                    elm('config-users-pw'+n).value=elm('config-users-pwa'+n).value='';
                    authUsers[n].pwWasChanged=true;
                }
            }
        },false);

        //reset the password if we have "original" password
        e=elm('config-users-pw'+i);
        e.addEventListener('focus',function(ev){
            var n=this.attributes['id'].value.slice(-1);
            if(this.value===PH.KFakeOriginalPw)
                this.value=elm('config-users-pwa'+n).value='';
        },false);
        //detect password change
        e.addEventListener('change',  function(){PH.configWizardPasswordChanged(this)},false);
        e.addEventListener('keypress',function(){PH.configWizardPasswordChanged(this)},false);
        e.addEventListener('paste',   function(){PH.configWizardPasswordChanged(this)},false);
        e.addEventListener('input',   function(){PH.configWizardPasswordChanged(this)},false);
        //put back the "original" password if there was no pw change
        elm('config-users-pw'+i).addEventListener('blur',function(ev){
            var n=this.attributes['id'].value.slice(-1);
            if(this.value==='' && elm('config-users-pwa'+n).value===''){
                if(PH.userSession.config.authUsers.length>n && !PH.userSession.config.authUsers[n].pwWasChanged)
                    this.value=elm('config-users-pwa'+n).value=PH.KFakeOriginalPw;
            }
        },false);

        //do the same for pwa
        e=elm('config-users-pwa'+i);
        e.addEventListener('focus',function(ev){
            if(this.value===PH.KFakeOriginalPw)
                this.value='';
        },false);
        //detect password change
        e.addEventListener('change',  function(){PH.configWizardPasswordChanged(this)},false);
        e.addEventListener('keypress',function(){PH.configWizardPasswordChanged(this)},false);
        e.addEventListener('paste',   function(){PH.configWizardPasswordChanged(this)},false);
        e.addEventListener('input',   function(){PH.configWizardPasswordChanged(this)},false);
        //put back the "original" password if there was no pw change
        e.addEventListener('blur',function(ev){
            var n=this.attributes['id'].value.slice(-1);
            if(this.value===''){
                if(PH.userSession.config.authUsers.length>n && !PH.userSession.config.authUsers[n].pwWasChanged)
                    this.value=PH.KFakeOriginalPw;
            }
        },false);


        //Delete buttons clicked
        elm('config-users-del'+i).addEventListener(this.platform.clickEvent,function(){
            var n=this.attributes['id'].value.slice(-1);
            if(confirm('Deleting user "'+elm('config-users-un'+n).value+'". Are you sure?"')){
                elm('config-users-un'+n).value='';
                elm('config-users-pw'+n).value=elm('config-users-pwa'+n).value='';
                elm('config-users-del'+n).disabled=elm('config-users-perm'+n).disabled=true;
                if(PH.userSession.config.authUsers.length>n)
                    PH.userSession.config.authUsers[n].pwWasChanged=true;
            }
        },false);
        //Permissions button clicked
        elm('config-users-perm'+i).addEventListener(this.platform.clickEvent,function(){
            var n=this.attributes['id'].value.slice(-1);
            alert("Sorry, not implemented yet...");
        },false);
    }

    //page register: prev, cancel & next
    elm('config-register-prev').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-register');

        //going back to users page or something else?
        if(PH.userSession.config.simple)
            elmShow('config-users');
        else if(PH.configWizardShowUsersScreen())
            elmShow('config-users');
        else
            elmShow('config-p3a');
    },false);
    elm('config-register-cancel').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-register');
        elmHide('config_bg');
    },false);
    elm('config-register-next').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-register');
        PH.configWizardDisplaySummaryPage();
    },false);
    //page register: on change
    e=elm('config-register-un');
    e.addEventListener('change',  PH.configWizardServerNameChanged,false);
    e.addEventListener('keypress',PH.configWizardServerNameChanged,false);
    e.addEventListener('paste',   PH.configWizardServerNameChanged,false);
    e.addEventListener('input',   PH.configWizardServerNameChanged,false);

    //page summary: prev, cancel & save
    elm('config-summary-prev').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-summary');
        //this is complicated ...

        //going back to register page?
        if(PH.configWizardShowRegisterScreen())
            elmShow('config-register');
        else if(PH.userSession.config.simple)
            elmShow('config-users');
        else if(PH.configWizardShowUsersScreen())
            elmShow('config-users');
        else
            elmShow('config-p3a');
    },false);
    elm('config-summary-cancel').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-summary');
        elmHide('config_bg');
    },false);
    elm('config-summary-next').addEventListener(this.platform.clickEvent,function(){
        elmHide('config-summary');
        elmHide('config_bg');

        //save stuff
        var i,anon=0;
        var configString;
        var showRegistrationWaiting=false;

        if(PH.userSession.config.simple){
            configString='anonymous=170&inet=2'; //10101010
        } else {
            if(elm('config-p2a-lan-anybody').checked){
                anon=128; //10000000, LAN viewing permission
                if(elm('config-p3a-operations-anybody').checked)
                    anon+=42; //101010, LAN operations permissions
            }
            if(elm('config-p2a-inet-anybody').checked){
                anon+=64;
                configString='anonymous='+anon+'&inet=1';
            } else if(elm('config-p2a-inet-signed').checked)
                configString='anonymous='+anon+'&inet=2';
            else
                configString='anonymous='+anon+'&inet=3';
        }

        //save servername
        if(typeof(PH.server.metadata.un)!=='string' && elm('config-register-un').value.length>=PH.KMinServernameLength){
            configString+='&srv='+elm('config-register-un').value;
            showRegistrationWaiting=true;
        }

        //save users
        for(i=0;i<PH.KConfigNrUserEntries;i++){
            if(elm('config-users-un'+i).value.length>0){
                console.log("Username  "+(i+1)+" has a value");
                console.assert(elm('config-users-un'+i).value.length>=PH.KMinUsernameLength);
                configString+='&un='+elm('config-users-un'+i).value;

                console.assert(elm('config-users-pw'+i).value.length>=PH.KMinPasswordLength);
                console.assert(elm('config-users-pw'+i).value===elm('config-users-pwa'+i).value);

                if(elm('config-users-pw'+i).value!==PH.KFakeOriginalPw){
                    configString+='&pw='+CryptoJS.SHA256(PH.KSalt+elm('config-users-un'+i).value.toLowerCase()+elm('config-users-pw'+i).value).toString(CryptoJS.enc.Hex);
                }
            }
        }//save users

        console.log("Sending configuration to server: "+configString);
        //send the configuration to server
        ajaxJson('/c',configString,function(obj){
            console.log("The answer from server:"+obj.a);
            console.assert(typeof(obj)==='object');
            console.assert(typeof(obj.a)==='boolean');

            if(obj.a){
                console.log("Configuration succeeded!");
                if(showRegistrationWaiting){
                    PH.showMessage("Registration succeeded!",false);
                    //remove the attention sign
                    elmHide('attention-btn');
                    PH.getServerMetadata();
                }
            } else {
                //the only thing that can fail is the registration
                console.assert(showRegistrationWaiting);
                console.assert(typeof(obj.msg)==='string');
                console.log("Registration failed: "+obj.msg);
                PH.showMessage("Registration failed: "+obj.msg,false);
            }
        });

        if(showRegistrationWaiting)
            PH.showMessage("Registering your Photostovis server...",true);
    },false);
}

