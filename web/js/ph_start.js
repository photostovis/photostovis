"use strict";

Dropzone.autoDiscover=false;
Dropzone.options.phDropzone={
    parallelUploads:1,
    addRemoveLinks:false,
    maxFilesize:20480, //20GB
    init: function(){
        var that=this;
        this.on('success', function(file){
            PH.server.completedUploads++;
            PH.server.currentCompletedUploads++;
            var progress=PH.server.currentCompletedUploads*100/PH.server.queuedUploads;
            console.assert(progress<=100,"Completed uploads: "+PH.server.currentCompletedUploads+", Queued uploads: "+PH.server.queuedUploads);  
            elm('upl_progress_bar').style.width=progress+'%';
            //console.log("Progress: "+progress);
            
            setTimeout(function(){
                //console.log(file.previewElement);
                that.removeFile(file);
            },3000);
            setTimeout(function(){
                if(typeof(file.previewElement)==='object'){
                    file.previewElement.style.transition='opacity 1s linear';
                    file.previewElement.style.opacity='0.1';
                }
            },2000);
            //add 10 minutes to websocket timeout
            PH.increaseWebsocketTimeout(10);
        });
        
        this.on('addedfile', function(file){
	        if(!PH.server.queuedUploads){
		        //show the cancel button and the progress bar
				elm('uplCancel').style.opacity='1';
				elm('upl_progress').style.opacity='1';
	        }
            PH.server.queuedUploads++;
        });
        
        /*
        this.on('totaluploadprogress', function(totalUploadProgress, totalBytes, totalBytesSent){
            console.log("totalUploadProgress: "+totalUploadProgress+" "+totalBytesSent+"/"+totalBytes);
            elm('upl_progress_bar').style.width=totalUploadProgress+'%';
            if(totalUploadProgress!==100)
                elm('settings_btn_next').disabled=true;
            else 
                elm('settings_btn_next').disabled=false;
        });*/
        
        this.on('reset', function(){
            console.log("Dropzone queue reset!");
            PH.server.currentCompletedUploads=0;
            PH.server.queuedUploads=0;
            elm('upl_progress_bar').style.width='0';
            elm('upl_progress').style.opacity='0';
            elm('uplCancel').style.opacity='0';
            PH.increaseWebsocketTimeout(10);
        });
        this.on('queuecomplete', function(){
	        console.log("Dropzone queue complete!");
            PH.server.currentCompletedUploads=0;
            PH.server.queuedUploads=0;
            elm('uplCancel').style.opacity='0';
            setTimeout(function(){
	            elm('upl_progress_bar').style.width='0';
	            elm('upl_progress').style.opacity='0';
            },3000);
            PH.increaseWebsocketTimeout(20);
            PH.photosFinishedUploading();
        });
        
        this.on('error',function(file,errorMsg,xhr){
	        console.warn("Dropzone: Some ERROR happened: "+errorMsg);
	        if(typeof(xhr)==='object' && xhr){
		        console.log(xhr);
	        }
        });
        
        /* This is how it should be done
        this.on('sending', function(file, xhr, formData) {
            formData.append("path", file.fullPath);
        });
        */

    }
};


PH.onDOMContentLoaded=function(){
    var imgdiv=elm('imgdiv'),infobox=elm('img-info');
    //vars used with the hash event
    var someElm,someTxt;
    var path2,hashValid,hashQuery,i;
    
    this.platform={
        name:'',
        version:0,
        pixelRatio:1,
        pixelRatioTxt:'',
        pixelRatioTxtThmbAm:'', //with &
        screenWidth:screen.width,
        screenHeight:screen.height,
        isTouch:false,
        pictureModeFullScreen:false, //this is set to true if we go fullscreen in the beginning. We need this var to prevent going fullscreen in picture mode
        smallDevice:false,
        clickEvent:'',
        renderingTimePerMPixel:0,
        lastSentRenderingTimePerMPixel:0
    };
    this.videoctrls={
        showing:false,
        active:false,
        lastMouseMove:null,
        showingTime:2500
    };

    this.userPrefsGlobal={ //these are saved to localStorage at photostovis.net
        language:'en',
        skipImgHelp:false,
        showPicInfo:1
    };
    this.userPrefsLocal={ //these are saved to localStorage at photostovis.net, prefixed by the server name
        showFullscreenDlg:true,
        authUsername:null,
        authPassword:null
    };
    this.userSession={//these should NOT be saved!
        showcase:false,
        authTimeout:null,
        authAttempts:0,
        authUsername:null,
        authPassword:null,
        authUser:0xFF, //FF: anonymous, FE: key-based auth, >=0: authenticated
        sessionId:0,
        canUpload:false,
        canShare:false,
        canAdmin:false,
        canSignin:false,
        canSignout:false,
        
        headerHeight:3, //in em, when a header is shown, this increases
        scanningStatus:0,
        
        dismissMessageOnAlbumLoad:false,
        creatingNewAlbum:false
    };

    this.hash={
        hash:'',
        lastHash:'nothing',
        targetPath:[],
        targetPathIsAlbum:true
    };
    this.server={
        metadata:null,
        websocket:null,
        websocketQueue:[],
        websocketKeepAlive:null, //keep the websocket alive until this time. That is, if the websocket closes, reopen it.
        completedUploads:0,
        currentCompletedUploads:0,
        queuedUploads:0,
        key:null,
        embedded:false,
        phonetCfgStatus:{
            PhonetCfgCurlInitFailes:1,
            PhonetCfgErrCurlConnectionFailed:2,
            PhonetCfgErrAnswerParsingFailed:3,
            PhonetCfgErrAnswerOutsideRange:4,
            PhonetCfgNeverTriedToRegister:0xFF
        },
        albumFlags:{
            FlagIsAlbum:1,
            FlagIsVideo:2,
            FlagPhotoHasGPSInfo:4,
            FlagHasExifThumbnail:8,
            FlagRotatePlus90:16,
            FlagRotatePlus180:32,
            FlagFullFileMD5:64,
            FlagAlbumHasUndatedEntries:128,
            FlagSizeIsInMB:256,
            FlagHasDescription:512,
            FlagHasUserComment:1024,
            FlagHasDescriptionTxtEntry:2048,
            FlagHasNameOffset:4096,
            //runtime flags
            FlagHasScaledThumbnail:8192,
            FlagHasCachedThumbnail:16384,
            FlagAlbumIsBeingScanned:32768,
            //extra defines
            FlagHasThumbnail:8200 //8+8192
        },
        configFlags:{
            CfgFlagIsDaemon:0x01,
            CfgFlagConfigurationRequired:0x02,
            CfgFlagUPnPWorks:0x04,
            CfgFlagCertificateIsNeeded:0x08,
            CfgFlagFfmpegWorks:0x10,
            CfgFlagFfmpegDoesNotWork:0x20,

            CfgFlagLanAuthRequired:0x0100, //authentication (signing in) is required for LAN requests
            CfgFlagLanAuthHttpNotAllowed:0x0200,

            CfgFlagInetNotAllowed:0x0400, //Internet requests not allowed
            CfgFlagInetAuthNotRequired:0x0800, //Internet requests do not require authentication
            CfgFlagInetAuthHttpAllowed:0x1000, //Allow authentication over http. If not set, only authentication over https work
            CfgFlagInetAuthHttpAllowedAutoadded:0x2000 //this is set when the server itself added the above flag, and not the user. Do not save to config if this is set.
        }

        /*statusFlagsPics:{
            StatusFlagPictureScanThreadActive:1,
            StatusFlagDoARescan:2, //after the current picture scan the system should do a rescan because there were modifications meanwhile
            StatusFlagFirstScanDone:4
        }*/
    };
    this.strip={
        bodyWidth:0,
        albmpictElmWidth:0,
        nrThumbnailsPerRow:0,
        maxNrThumbnailsPerRow:0,
        picThmbWidth:0,
        picThmbWidthInt:0,
        nrThumbnailsToLoad:10 //how many thumbnails to load at once. Should be bitrate dependent (min:1, max: 20. 1 for each Mbps)
    };

    /* Each path element has these properties:
     * .a (object): info about current album (including its id)
     * .e (array of objects): elements of the album. Each element has its own id
     * .index (number): the current index. This is -1 if the path element is top-most, and between 0 and e.length otherwise
     * .scrollTop (number): position in page
     */
    this.path=[];
    
    this.loadingAlbum='';
    this.inPictureMode=false;
    
    this.settingAllowDismiss=true;
    this.zipDownload=false;
    this.dropzone=null;
    this.KConfigNrUserEntries=5; //depends on how many we have in the index.html config page
    this.KFakeOriginalPw='**********'; //10 stars
    this.KMinUsernameLength=3; //joe is Ok, jo is not
    this.KMinPasswordLength=5;
    this.KMinServernameLength=5;
    this.KSalt='Photostovis';
    this.KAlbmpicElmWidthPercent=0.98;
    //create the "action" Dropzone form attribute
    //Dropzone.options.phDropzone.url='/u';
    
    console.log("DocumentLoaded++");
    console.assert(this===PH);


    //document.body.addEventListener('touchmove',function(e){ e.preventDefault(); });
    //detect the platform
    if(typeof(navigator.userAgent)==='undefined'){
        console.warn("WARNING: navigator.userAgent not defined");
    } else if(navigator.userAgent.indexOf('iPhone')>0 || navigator.userAgent.indexOf('iPad')>0 || navigator.userAgent.indexOf('iPod')>0){
        this.platform.name='ios';
    } else if(navigator.userAgent.indexOf('Android')>=0){
        this.platform.name='android';
        if(navigator.userAgent.indexOf('Android 2')>=0){
            this.platform.version=2;
            //this.imgExt='.png';//Android 2 does not support svg
        }
    } else if(navigator.userAgent.indexOf('IEMobile/')>=0){
        this.platform.name='wp';
        if(navigator.userAgent.indexOf('IEMobile/9')>=0){
            this.platform.version=9;
        }
        else if(navigator.userAgent.indexOf('IEMobile/10')>=0)
            this.platform.version=10;
    } else if(navigator.userAgent.indexOf('Symbian')>=0)
        this.platform.name='symbian';
    if(typeof(window.devicePixelRatio)!=='undefined'){
        this.platform.pixelRatio=window.devicePixelRatio;
        this.platform.screenWidth=screen.width*window.devicePixelRatio;
        this.platform.screenHeight=screen.height*window.devicePixelRatio;
        console.assert(this.platform.pixelRatio===1 || this.platform.pixelRatio===2 || this.platform.pixelRatio===3);
        if(this.platform.pixelRatio>1)
            this.platform.pixelRatioTxt='?pr='+this.platform.pixelRatio;
    };
    this.platform.isTouch=!!('ontouchstart' in window) || !!('msmaxtouchpoints' in window.navigator);
    if(this.platform.pixelRatio===1){
        //disable the "retina" resolution "get image"
        elm('min-res-this-r').style.display='none';
        elm('min-res-all-r').parentNode.style.display='none';
    }
    
    console.log("Platform: ",this.platform);

    //get our preferences (from localStorage)
    PH.getPreferences();

    if(this.platform.isTouch){
        //touch device
        this.platform.clickEvent='touchend';
        console.log("touch device detected");

		/*
        elm('album-title').style.margin="0";
        elm('album-picsnalbms').style.lineHeight="1em";
        elm('album-timebar').style.fontSize="0.7em"; //same as album-picsnalbms
        elm('album-timebar').style.lineHeight="1em";
        */

        //toolbar elements:
        //elmHide('fb_iframe');//hide the share button (for the moment)
/*
        someElm=elm('attention-btn');
        
        someElm.style.height=someElm.style.width="1.2em";
        //someElm.style.display='block';
        someElm.style.position='absolute';
        someElm.style.top=someElm.style.right='1.4em';
        someElm.style.zIndex='10';*/

        //show full-screen dialog
        if(this.platform.name!=='ios' && this.userPrefsLocal.showFullscreenDlg)
            this._showSettings('settings_efs','Show full screen?',false);
        //TODO: on ios show a "add to homescreen" dialog
    } else {
        //non-touch device
        this.platform.clickEvent='click';
        //add nontouch class to menu button
        elm('menu-btn').setAttribute('class','nontouch');
        elm('attention-btn').setAttribute('class','nontouch');
        elm('album-picsnalbms').setAttribute('class','nontouch');
        //TODO: to other buttons as well
        console.log("non-touch device detected");
    }

    console.assert(this.platform.name===PH.platform.name,"Variable scope is screwed-up!");

    this.server.key=getURLParam(window.location,"key");
    this.server.embedded=getURLParam(window.location,"embedded");
    if(this.server.embedded){
        console.log("showing album embedded");
        this.server.embedded=true;
        elmHide('header');
        someElm=elm('cntnt');
        someElm.style.top="0";
    }
    if(this.server.key){
        console.log("Server key: "+this.server.key);
        this.userSession.authUser=0xFE; //means key-based authentication
        //hide some menu elements, those that should not be available to guests
        elmHide('menu_signin');
        elmHide('menu_upload');
        elmHide('menu_linkurl');
        elmHide('menu_shutdown');
    }
    if(parent!==self)parent.postMessage('alive',"*");
    
    this.computeThumbnailStrip(); //-> platform.smallDevice is set here
    console.assert(this.strip.nrThumbnailsPerRow>1,"nrThumbnailsPerRow="+this.strip.nrThumbnailsPerRow);
    
    //scrollfix
    someElm=elm('cntnt');
    someElm.scrollTop=1;
    someElm.addEventListener('scroll',function(){
        if(this.scrollTop<=0)
            this.scrollTop=1;
    },true);

    //load and initialize Google maps

    loadScript('https://maps.googleapis.com/maps/api/js?key=AIzaSyDyK67TTcukbOw8twxt4bkD8i8ZCt_2uRI&callback=PH.GoogleMapsInitialized',
    	function(){console.log("Google Maps script loaded.");});


    //console.log(location.host);
    
    //////////////////////////////////////////////////////////////////////////
    //bindings
    ////////////////////////////////////////////////////////////////////////////
    function showMenu(){
        //are we in full-screen mode?
        if(PH.platform.name==='ios'){
            //there is no full-screen in ios
            elmHide('menu_enter_fs');
            elmHide('menu_exit_fs');
        } else {
            if(PH.isFullScreen()){
                elmHide('menu_enter_fs');
                elmClear('menu_exit_fs');
            } else {
                elmClear('menu_enter_fs');
                elmHide('menu_exit_fs');
            }
        }
        elmShow('menu_bg');
        PH.setupWebsocket();
    }
    
    addEventListener('message',function(e){
        console.log("Message received from share iframe or parent:");
        //console.log(e); -> lots of errors in Safari/iOS
        var domainLen=e.origin.length;
        if(domainLen<20){
            console.warn("Ignoring message from unknown origin: "+e.origin);
            return;
        }
        var domain=e.origin.substring(domainLen-15);
        if(domain!=='photostovis.com' && domain!=='photostovis.net' && domain!=='photostovis.org'){
            console.warn("Ignoring message from unknown origin: "+e.origin);
            return;
        }
        if(typeof(e.data)==='string'){
            if(e.data==="sendme"){
                if(!PH.userSession.canShare)
                    PH.sendMessageToFbFrame('photostovis_disable');
                return;
            }
            if(e.data==="sharing_successfull"){
                //TODO:
                return;
            }
            if(e.data.slice(0,15)==="sharing_failed:"){
                //TODO: failed because of the Photostovis server
                console.warn("Sharing on FB failed: "+e.data.slice(15));
                return;
            }

            if(e.data==="get_fb_share_link"){
                //send the request to the server
                var url='/k',i;
                var fbIFrame=null; /*document.getElementById("fb_iframe");*/
                if(!fbIFrame || !fbIFrame.contentWindow || !PH.server.metadata || !PH.server.metadata.un){
                    console.warn("Cannot send link to FB frame: some data is missing.");
                    return;
                }
                console.log("Sending link to FB frame.");

                for(i=1;i<PH.path.length;i++)
                    url+='/'+PH.path[i].a.id;
                ajaxJson(PH.createGetUrl(url,null,true),null,function(obj){
                    var fbUrl;
                    if(typeof(obj)==='object'){
                        if(typeof(obj.link)==='string'){
                            fbUrl=obj.link.replace('/?key=','/a/?key=');
                            console.log("Sending link to FB frame: "+fbUrl);
                            fbIFrame.contentWindow.postMessage("photostovis_link="+fbUrl,"*");
                        } else if(typeof(obj.err)==='number')
                            console.warn("Cannot send link to FB frame: Server is not registered to phonet.");
                        else
                            console.warn("Cannot send link to FB frame: Unspecified error retrieving link from the server.");
                    } else
                        console.warn("Cannot send link to FB frame: Unspecified error retrieving link from the server (returned data not understood).");
                },PH.ajaxError);
                return;
            }
        }      
        
        if(typeof(e.data.photostovis_top_hash)==='string' && parent!==self){
            console.log("New hash received from parent: "+e.data.photostovis_top_hash);
            PH.hash.hash=e.data.photostovis_top_hash;
            //check if we have received user preferences
            if(typeof(e.data.localPreferencesTxt)==='string'){
                console.log("localPreferencesTxt: "+e.data.localPreferencesTxt);
                let preferencesObj=JSON.parse(e.data.localPreferencesTxt);
                PH.userPrefsLocal=preferencesObj;
            }
            if(typeof(e.data.globalPreferencesTxt)==='string'){
                console.log("globalPreferencesTxt: "+e.data.globalPreferencesTxt);
                let preferencesObj=JSON.parse(e.data.globalPreferencesTxt);

                if(typeof(preferencesObj.showPicInfo)==='boolean'){
                    if(preferencesObj.showPicInfo)preferencesObj.showPicInfo=1;
                    else preferencesObj.showPicInfo=0;
                }
                PH.userPrefsGlobal=preferencesObj;
            }
            //done checking for stored user preference
            //now check if we have server object
            if(typeof(e.data.server)==='object'){
                PH.server.ip=e.data.server.ip;
                PH.server.port=e.data.server.port;
                PH.server.ipNum=e.data.server.ipNum;
                PH.server.name=e.data.server.name;
                if(PH.server.name)
                    elmHTML('about_server',PH.server.name+'.photostovis.net:'+PH.server.port+'<br>IP: '+PH.server.ip+':'+PH.server.port);
                else
                    elmHTML('about_server',PH.server.ip+':'+PH.server.port);
            }
            
            //do we have a language?
            //console.log("UserX language: "+PH.userPrefsGlobal.language+" of type: "+typeof(PH.userPrefsGlobal.language));
            if(typeof(PH.userPrefsGlobal.language)!=='string' || PH.userPrefsGlobal.language.length!==2){
                //PH.showLanguageSelector(false);
                PH.userPrefsGlobal.language='en';
            }
            console.assert(typeof(PH.userPrefsGlobal.language)==='string' && PH.userPrefsGlobal.language.length===2);
    
            PH.onHashChanged();
            return;
        }
        
        console.error("What exactly is going on here??");
    },false);
    
    //bind the hash stuff. We keep it here to have access to some vars from this (onDOMContentLoaded) function
    if(self===parent || this.server.embedded){
        addEventListener('hashchange',function(){
            console.log("Received hashchanged event");
            PH.hash.hash=location.hash;
            PH.onHashChanged();
        },false);
        
        //check the initial hash
        this.path=[];
        if(location.hash===''){
            this.setHash('#a');
        } else if(location.hash==='#a'){
            console.log("location.hash===#a, PH.hash.hash="+PH.hash.hash);
            PH.hash.lastHash=PH.hash.hash='#a';
            this.getServerMetadata();
        } else {
            //we have a path, parse it
            hashValid=true;                

            if(location.hash.length<=3 || location.hash[0]!=='#' || (location.hash[1]!=='a' && location.hash[1]!=='i') || location.hash[2]!=='/')
                hashValid=false;
            else {
                hashQuery=location.hash.split('?');
                if(typeof(hashQuery[1])!=='undefined')
                    console.log("Query: "+hashQuery[1]);
                console.log("Hash (without query): "+hashQuery[0]);

                if(typeof(hashQuery[1])!=='undefined'){
                    if(hashQuery[1]==='showcase'){
                        //we showcase this picture
                        this.userSession.showcase=true;
                    } else console.warn("Unknown hash query: "+hashQuery[1]);
                }

                path2=hashQuery[0].slice(3).split('/');
                console.assert(path2.length>0);
                
                for(i=0;i<path2.length;i++)
                    this.hash.targetPath.push(path2[i]);
                if(hashQuery[0][1]==='a')
                    this.hash.targetPathIsAlbum=true;
                else
                    this.hash.targetPathIsAlbum=false;
            }

            if(!hashValid){
                console.warn("The hash ("+location.hash+") is not valid, loading the root album");
                this.setHash('#a');
            } else {
                console.log("Hash contains an apparently valid path");
                //this.setHash(hashQuery[0]); -> no point in setting the hash because it will not trigger any change
                PH.hash.lastHash=PH.hash.hash=hashQuery[0];
                this.getServerMetadata();
            }
        }
        
    } else {
        addEventListener('hashchange',function(){
            console.error("hashchange event detected in iframe");
        },false);
        
        //tell the parent to send us the initial hash
        parent.postMessage("sendme","*");
    }

    imgdiv.addEventListener(this.platform.clickEvent,function(ev){PH.handleClicks(ev,this)},false);
    if(this.platform.clickEvent!=='click')
        imgdiv.addEventListener('click',function(ev){PH.handleClicks(ev,this)},false); //we need real click for videos and zoomed pictures
    if(!this.platform.isTouch){
        //non-touch device
        imgdiv.addEventListener('mousemove',function(ev){
            console.assert(PH.albumCache);
            if(!PH.albumCache.currentIsVideo())return; //only interested in videos
            PH.showVideoControls();
        },false);//mousemove
    } //else for touch device we do a similar thing in handleClicks

    elm('video_nav_prev').addEventListener(this.platform.clickEvent,function(ev){
	    console.log("Video: prev");
        console.assert(PH.albumCache);
        if(PH.videoctrls.active)
            PH.albumCache.showPrev();
        else
            PH.showVideoControls();
    },false);
    elm('video_nav_next').addEventListener(this.platform.clickEvent,function(ev){
	    console.log("Video: next");
        console.assert(PH.albumCache);
        if(PH.videoctrls.active)
            PH.albumCache.showNext();
        else
            PH.showVideoControls();
    },false);
    elm('video_nav_close').addEventListener(this.platform.clickEvent,function(ev){
	    console.log("Video: close");
        //if here, we need to return to our album
        if(PH.videoctrls.active){
            console.assert(PH.path.length>0);
            PH.path[PH.path.length-1].index=-1;
            PH.setHash();
        } else {
	        console.log("Video: videocontrols are NOT active");
	        PH.showVideoControls();
        }
    },false);


    elm('zoom').addEventListener(this.platform.clickEvent,function(ev){
        if(typeof(ev.button)==='number' && ev.button!==0)return; //mouse click, other than left button
        if(PH.albumCache.toggleZoom())
            ev.preventDefault(); //we toggled the zoom
        ev.stopPropagation();
    },false);
    elm('min-resolution').addEventListener(this.platform.clickEvent,function(ev){
        if(typeof(ev.button)==='number' && ev.button!==0)return; //mouse click, other than left button
        if(PH.albumCache.toggleResolutionMenu())
            ev.preventDefault(); //we opened the menu
        ev.stopPropagation();
    },false);
    elm('min-res-this-h').addEventListener(this.platform.clickEvent,function(ev){
        if(typeof(ev.button)==='number' && ev.button!==0)return; //mouse click, other than left button
        PH.albumCache.reloadThisPicture('h');
        ev.preventDefault();
        ev.stopPropagation();
    },false);
    elm('min-res-this-d').addEventListener(this.platform.clickEvent,function(ev){
        if(typeof(ev.button)==='number' && ev.button!==0)return; //mouse click, other than left button
        PH.albumCache.reloadThisPicture('d');
        ev.preventDefault();
        ev.stopPropagation();
    },false);
    elm('min-res-this-r').addEventListener(this.platform.clickEvent,function(ev){
        if(typeof(ev.button)==='number' && ev.button!==0)return; //mouse click, other than left button
        PH.albumCache.reloadThisPicture('r');
        ev.preventDefault();
        ev.stopPropagation();
    },false);
    elm('min-res-this-o').addEventListener(this.platform.clickEvent,function(ev){
        if(typeof(ev.button)==='number' && ev.button!==0)return; //mouse click, other than left button
        PH.albumCache.reloadThisPicture('o');
        ev.preventDefault();
        ev.stopPropagation();
    },false);
    elm('min-res-all-n').addEventListener(this.platform.clickEvent,function(ev){
        if(typeof(ev.button)==='number' && ev.button!==0)return; //mouse click, other than left button
        PH.albumCache.setMinimumResolution('n');
    },false);
    elm('min-res-all-h').addEventListener(this.platform.clickEvent,function(ev){
        if(typeof(ev.button)==='number' && ev.button!==0)return; //mouse click, other than left button
        PH.albumCache.setMinimumResolution('h');
    },false);
    elm('min-res-all-d').addEventListener(this.platform.clickEvent,function(ev){
        if(typeof(ev.button)==='number' && ev.button!==0)return; //mouse click, other than left button
        PH.albumCache.setMinimumResolution('d');
    },false);
    elm('min-res-all-r').addEventListener(this.platform.clickEvent,function(ev){
        if(typeof(ev.button)==='number' && ev.button!==0)return; //mouse click, other than left button
        PH.albumCache.setMinimumResolution('r');
    },false);
    elm('min-res-all-o').addEventListener(this.platform.clickEvent,function(ev){
        if(typeof(ev.button)==='number' && ev.button!==0)return; //mouse click, other than left button
        PH.albumCache.setMinimumResolution('o');
    },false);

    infobox.addEventListener(this.platform.clickEvent,function(ev){
        if(typeof(ev.button)==='number' && ev.button!==0)return; //mouse click, other than left button
        if(PH.albumCache.toggleInfobox())
            ev.preventDefault(); //we toggled the infobox
        ev.stopPropagation();
    },false);
    infobox.children[3].addEventListener(this.platform.clickEvent,function(ev){ev.stopPropagation()},false);

    document.addEventListener('keydown',function(ev){
        if(!PH.albumCache)return;
        if(ev.keyCode===37)PH.albumCache.showPrev();
        if(ev.keyCode===39)PH.albumCache.showNext();
        if(ev.keyCode>=16 && ev.keyCode<=18)//shift, ctrl, alt
            PH.albumCache.toggleZoom();
    },false);
    
    window.addEventListener('resize',function(){
        console.log("In resize");
        if(PH.albumCache)
            PH.albumCache.resize();
        if(PH.path.length>0 && PH.path[PH.path.length-1].index===-1 && document.body.clientWidth!==PH.strip.bodyWidth){
            console.log("computing thumbnail strip");
            //TODO: these could be optimized (how?)
            PH.computeThumbnailStrip();
            PH.displayAlbum(true); //"true" preserves the position
        }
    },false);
    
    //menu related bindings
    //bind the menu button
    elm('menu-btn').addEventListener(this.platform.clickEvent,showMenu,false);
    elm('menu_bg').addEventListener(this.platform.clickEvent,function(){
        elmHide('menu_bg');
    },false);
    
    //bind the menu elements
    //"Login" menu item
    elm('menu_signin').addEventListener(this.platform.clickEvent,function(){
        elmHide('menu_bg');
        PH.showAuthPage('signin');
    },false);
    //"Signout" menu item
    elm('menu_signout').addEventListener(this.platform.clickEvent,function(){
        elmHide('menu_bg');
        PH._showSettings('settings_sgo','Sign Out?',true);
    },false);

    //"Enter fullscreen" menu item
    elm('menu_enter_fs').addEventListener(this.platform.clickEvent,function(){
        elmHide('menu_bg');
        //if we are here we are not in full-screen mode
        console.assert(!PH.isFullScreen());
        PH.platform.pictureModeFullScreen=false;
        if(!PH.userPrefsLocal.showFullscreenDlg){
            PH.userPrefsLocal.showFullscreenDlg=true;
            PH.saveLocalPreferences();
        }
        PH.enterFullScreen(true);
    },false);

    //"Exit fullscreen" menu item
    elm('menu_exit_fs').addEventListener(this.platform.clickEvent,function(){
        elmHide('menu_bg');
        //if we are here we ARE in full-screen mode
        console.assert(PH.isFullScreen());
        PH.platform.pictureModeFullScreen=true;
        PH.exitFullScreen(true);
    },false);
    //"Upload" menu item
    elm('menu_upload').addEventListener(this.platform.clickEvent,function(){
        if(!PH.userSession.canUpload){
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
        PH.showUploadToolbar();
    },false);
    //"Share link" menu item
    elm('menu_linkurl').addEventListener(this.platform.clickEvent,function(){
        if(!PH.userSession.canShare){
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
        PH.showLink();
    },false);
    //"Download as zip" menu link
    elm('menu_downloadzip').addEventListener(this.platform.clickEvent,function(){
        PH.showDownloadZip();
    },false);
    //"Language" menu item
    elm('menu_language').addEventListener(this.platform.clickEvent,function(){
        //elmHide('menu_bg');
        PH.showLanguageSelector(true);
    },false);
    //"Config Wizard" menu item
    elm('menu_config').addEventListener(this.platform.clickEvent,function(e){
        PH.configWizard(true);
    },false);
    //"Shutdown" menu item
    elm('menu_shutdown').addEventListener(this.platform.clickEvent,function(e){
        PH._showSettings('settings_off','Shut down?',true);
    },false);
    //"Help" menu item
    elm('menu_help').addEventListener(this.platform.clickEvent,function(){
        window.open("//photostovis.net/PhotostovisHelp.html","_blank");
    },false);
    //"About" menu item
    elm('menu_about').addEventListener(this.platform.clickEvent,function(){
        PH._showSettings('settings_abt','Statistics',true);
    },false);
    
    //bind settings background and settings box
    elm('settings_bg').addEventListener(this.platform.clickEvent,function(ev){
        //can we cancel the settings dialog?
        if(!PH.settingAllowDismiss)
            return; //dismissing the settings dialog is not allowed

        ev.stopPropagation();ev.preventDefault();
        PH._hideSettings();
    },false);
    
    elm('settings_box').addEventListener(this.platform.clickEvent,function(e){
        e.stopPropagation();
        //e.preventDefault();
    },false);
    
    //bind elements from the settings "dialog"
    elm('lngOk').addEventListener(this.platform.clickEvent,function(e){
        PH.validateLanguage();
    },false);
    elm('sgoSignOut').addEventListener(this.platform.clickEvent,function(e){
        PH.validateSignout();
    },false);
    elm('offShutdown').addEventListener(this.platform.clickEvent,function(e){
        PH.validateShutdown();
    },false);
    
    
    
    //bind the attention button
    elm('attention-btn').addEventListener(this.platform.clickEvent,function(){
        PH.showRegistrationErr();
    },false);

    elm('settings_dismiss').addEventListener(this.platform.clickEvent,function(ev){
        ev.stopPropagation();ev.preventDefault();
        PH._hideSettings();
    },false);
    
    
    //bind the fsToolbar buttons
    elm('tbEditClose').addEventListener(this.platform.clickEvent,function(ev){
        ev.stopPropagation();ev.preventDefault();
        PH.onEditToolbarClose();
    },false);
    elm('tbEditSubalbum').addEventListener(this.platform.clickEvent,function(ev){
        ev.stopPropagation();ev.preventDefault();
        PH.showCreateNewSubalbum();
    },false);
    elm('tbEditUpload').addEventListener(this.platform.clickEvent,function(ev){
        ev.stopPropagation();ev.preventDefault();
        PH.showUploadMedia();
    },false);

	//bind the "Create" button from NEw Sub-album settings item
	elm('csaCreate').addEventListener(this.platform.clickEvent,function(ev){
        ev.stopPropagation();ev.preventDefault();
        PH.validateCreateNewSubalbum();
    },false);
    elm('csaName').addEventListener('keypress',function(ev){
	    if(ev.which===13){
		    ev.stopPropagation();ev.preventDefault();
			PH.validateCreateNewSubalbum();
	    }
	},false);    

    //bind the buttons from the "enter full screen" dialog
    if(this.platform.isTouch && this.platform.name!=='ios'){
        console.log("binding buttons");
        elm('settings_efs_yes').addEventListener(this.platform.clickEvent,function(ev){
            ev.stopPropagation();ev.preventDefault();
            PH._hideSettings();
            PH.enterFullScreen(true);
        },false);
        elm('settings_efs_no1').addEventListener(this.platform.clickEvent,function(ev){
            ev.stopPropagation();ev.preventDefault();
            PH._hideSettings();
        },false);
        elm('settings_efs_no2').addEventListener(this.platform.clickEvent,function(ev){
            ev.stopPropagation();ev.preventDefault();
            PH._hideSettings();
            //save "do not show this"
            PH.userPrefsLocal.showFullscreenDlg=false;
            PH.saveLocalPreferences();
        },false);
    }

    //bind the authentication page buttons (sign in)
    elm('authSignIn').addEventListener(this.platform.clickEvent,function(){
        PH.submitAuthForm();
    },false);
    elm('authCancel').addEventListener(this.platform.clickEvent,function(){
        PH.hideAuthPage();
    },false);
    //bind keypress in the signin form
    elm('authUsername').addEventListener('keypress',function(e){
        if(e.which!==13)return;

        //if here, this is enter
        elm('authPassword').focus();
        e.preventDefault();
        e.stopPropagation();
    },false);
    elm('authPassword').addEventListener('keypress',function(e){
        if(e.which!==13)return;

        //if here, this is enter
        if(elm('authUsername').value.length>0){
            PH.submitAuthForm();
        } else {
            elm('authUsername').focus();
        }
    },false);

    //bind the config page buttons
    PH.bindConfig();

    console.log("DocumentLoaded--");
};

PH.submitAuthForm=function(){
    var un=elm('authUsername').value;
    var pwHashed=CryptoJS.SHA256(PH.KSalt+un.toLowerCase()+elm('authPassword').value).toString(CryptoJS.enc.Hex);
    if(elm('authRemember').checked){
        //save them
        PH.userPrefsLocal.authUsername=un;
        PH.userPrefsLocal.authPassword=pwHashed;
        PH.saveLocalPreferences();
    }
    PH.userSession.authAttempts++;
    PH.getServerMetadata(un,pwHashed); //-> this should also hide the authentication page
    PH.userSession.authTimeout=setTimeout(function(){
        //hide the Submit button and show a spinner
        var spinnerOpts={
            lines: 11, // The number of lines to draw
            length: 5, // The length of each line
            width: 3, // The line thickness
            radius: 7, // The radius of the inner circle
            corners: 1, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            direction: 1, // 1: clockwise, -1: counterclockwise
            color: '#000', // #rgb or #rrggbb or array of colors
            speed: 1, // Rounds per second
            trail: 60, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: '50%', // Top position relative to parent
            left: '50%' // Left position relative to parent
        };
        var spinner=new Spinner(spinnerOpts).spin(elm('authSpinnerBox'));

        //hide & show
        elmHide('authSignIn');
        elmHide('authCancel');
        elmShow('authSpinnerBox');
        PH.userSession.authTimeout=null;
    },500);//set timeout
}

PH.computeThumbnailStrip=function(){
    var albmpictElmHeight,idealWidth;
    //is this a small device (e.g. mobile phone?)
    if(document.body.clientWidth<=736 && document.body.clientHeight<=736){
        this.platform.smallDevice=true;
        idealWidth=70;
    } else {
        this.platform.smallDevice=false;
        idealWidth=142;
    }
    if(this.platform.pixelRatio>1 && !this.platform.smallDevice){
        console.assert(this.platform.pixelRatioTxt.length>3);
        this.platform.pixelRatioTxtThmbAm='&'+this.platform.pixelRatioTxt.slice(1);
    } else {
        this.platform.pixelRatioTxtThmbAm='';
    }

    if(this.platform.smallDevice)
        console.log("This is a small device with thumbnail pixel ratio: "+this.platform.pixelRatioTxtThmbAm);
    else
        console.log("This is NOT a small device. Width: "+document.body.clientWidth);

    //do we use the full width, or just a part?
    this.strip.bodyWidth=document.body.clientWidth;
    if(this.server.embedded)
        this.strip.albmpictElmWidth=document.body.clientWidth-2;
    else
        this.strip.albmpictElmWidth=Math.floor(document.body.clientWidth*this.KAlbmpicElmWidthPercent);
    this.strip.nrThumbnailsPerRow=Math.floor((this.strip.albmpictElmWidth-1)/idealWidth);
    this.strip.picThmbWidth=(this.strip.albmpictElmWidth-1)/this.strip.nrThumbnailsPerRow-1;
    this.strip.picThmbWidthInt=Math.floor(this.strip.picThmbWidth);
    if(document.body.clientWidth<document.body.clientHeight){
        if(this.server.embedded)
            albmpictElmHeight=document.body.clientHeight-2;
        else
            albmpictElmHeight=Math.floor(document.body.clientHeight*this.KAlbmpicElmWidthPercent);
        this.strip.maxNrThumbnailsPerRow=Math.floor((albmpictElmHeight-1)/idealWidth);
        console.assert(this.strip.maxNrThumbnailsPerRow>=this.strip.nrThumbnailsPerRow);
    } else this.strip.maxNrThumbnailsPerRow=this.strip.nrThumbnailsPerRow;
    console.log("computeThumbnailStrip: maxThumbnails: "+this.strip.maxNrThumbnailsPerRow+", per row: "+this.strip.nrThumbnailsPerRow);
};

PH.handleVideoControlsTimeout=function(){
    var e,now=Date.now();

    console.log("handleVideoControlsTimeout++");
    //how much time since the last mouse movement?
    console.assert(PH.videoctrls.lastMouseMove);
    if(now-PH.videoctrls.lastMouseMove>PH.videoctrls.showingTime-200){
        //hide the controls
        console.log("hide the controls");
        //elmClear('video_nav_ctrls');
        e=elm('video_nav_prev');
        e.style.transition='opacity 1s linear';
        e.style.opacity='0';
        e=elm('video_nav_next');
        e.style.transition='opacity 1s linear';
        e.style.opacity='0';
        e=elm('video_nav_close');
        e.style.transition='opacity 1s linear';
        e.style.opacity='0';

        PH.videoctrls.showing=false;
        setTimeout(function(){
            PH.videoctrls.active=false;
        },1000);
        PH.videoctrls.lastMouseMove=null;
    } else {
        //we need to sleep some more
        console.log("We need to sleep some more");
        setTimeout(PH.handleVideoControlsTimeout,PH.videoctrls.showingTime-(now-PH.videoctrls.lastMouseMove));
    }
    console.log("handleVideoControlsTimeout--");
}

PH.showVideoControls=function(){
    PH.videoctrls.lastMouseMove=Date.now();
    PH.videoctrls.active=true;
    if(PH.videoctrls.showing)
    	return;
    PH.videoctrls.showing=true;

    var e;
    //elmShow('video_nav_ctrls');
    e=elm('video_nav_prev');
    e.style.transition='opacity 0.1s linear';
    e.style.opacity='1';
    e=elm('video_nav_next');
    e.style.transition='opacity 0.1s linear';
    e.style.opacity='1';
    e=elm('video_nav_close');
    e.style.transition='opacity 0.1s linear';
    e.style.opacity='1';

    console.log("setting videocontrols timeout");
    setTimeout(PH.handleVideoControlsTimeout,PH.videoctrls.showingTime);
}


PH.handleClicks=function(ev,div){
    //returns true if we are to stopPropagation, false otherwise
    var clientX,currentZoom,isVideo;
    if(!this.albumCache)return false;

    if(typeof(ev.button)==='number' && ev.button!==0)return; //mouse click, other than left button
    if(typeof(ev.changedTouches)!=='undefined')clientX=ev.changedTouches[0].pageX;
    else clientX=ev.clientX;
    console.assert(typeof(clientX)==='number');

    currentZoom=this.albumCache.currentElmZoom();
    isVideo=this.albumCache.currentIsVideo();
    if((currentZoom!==0 || isVideo) && ev.type!=='click'){
        if(isVideo && this.platform.isTouch && ev.type==='touchend'){
            console.log("handleClicks: Video in touch device, showing video controls");
            this.showVideoControls();
        } else
            console.log("Non-click event on zoomed element or video element, ignoring.");
        return;
    }

    if(currentZoom===0 && ev.type==='click' && 'click'!==this.platform.clickEvent && !isVideo){
        console.log("Click event on non-zoomed, non-video element, ignoring.");
        return;
    }

    if(isVideo){
        console.log("handleClicks: video, event: "+ev.type+", ignoring. (only click handled for videos)");
        //ev.preventDefault();
        //ev.stopPropagation();
        return;
    }
    console.log("In handleClicks, handling the event. Type: "+ev.type);


    //if here we are handling the event
    ev.preventDefault();
    ev.stopPropagation();


    if(elm('min-res-container').style.display==='block'){
        this.albumCache.toggleResolutionMenu();
        return;
    }

    if(clientX<div.clientWidth/3 ){
        //go to previous image
        this.albumCache.showPrev();
        return;
    }
    if(clientX>div.clientWidth*2/3){
        //go to next image
        this.albumCache.showNext();
        return;
    }
    //if here, we need to return to our album
    this.path[this.path.length-1].index=-1;
    this.setHash();
};

PH.goOneLevelUp=function(){
    console.log("goOneLevelUp++");
    PH.path.pop();
    PH.albumCache.clear(true);//true -> total
    if(PH.path.length)
    	PH.path[PH.path.length-1].index=-1;
    PH.setHash();
    console.log("goOneLevelUp--");
};

PH.sendMessageToFbFrame=function(msg){
    console.warn("sendMessageToFbFrame: disabled!");
    /*
    console.log("sendMessageToFbFrame: "+msg);
    //send our path to the fb_iframe
    
    var fbIFrame=document.getElementById("fb_iframe");
    if(fbIFrame && fbIFrame.contentWindow){
        console.log("Posting message. Type: "+typeof(msg));
        fbIFrame.contentWindow.postMessage(msg,"*");
    }*/
};

PH.setHash=function(hash){
    console.log("setHash: "+hash);
    if(typeof(hash)==='undefined'){
        console.log("setHash: getting hash from path");
        hash=this.getHashFromPath();
    }
    console.log("setHash: "+hash);
    if(parent!==self && !this.server.embedded){
        console.log("Sending hash to parent: "+hash);
        parent.postMessage({photostovis_hash:hash},"*");
    } else {
        location.hash=hash;
        console.log("Setting hash: "+hash);
    }
};

PH.onHashChanged=function(){
    var i,v,index;
    var path2,hashQuery,p;
    var albumElements,id2find;
    var recreatePath=false;
    console.assert(this===PH);
    console.log("onHashChanged++: "+this.hash.hash+" (lastHash: "+this.hash.lastHash+") path length="+this.path.length);
    if(this.hash.hash===this.hash.lastHash){
        console.log("lastHash is the same, ignoring");
        return;
    }
    if(this.hash.hash==='#a' && this.hash.lastHash===''){
        console.log("Initial hash change, ignoring.");
        return;
    }
    if(this.hash.hash===''){
        this.setHash('#a');
        return;
    }
    
    //verify the path object
    for(i=0;i<this.path.length;i++){
        console.assert(typeof(this.path[i])==='object','Path element '+i+' is not an object.');
        v=this.path[i];
        console.assert(typeof(v.a)==='object','path['+i+'].a is not an object');
        console.assert(typeof(v.e)==='object','path['+i+'].e is not an object/array');
        console.assert(typeof(v.scrollTop)==='number','path['+i+'].scrollTop is not a number');
        console.assert(typeof(v.index)==='number','path['+i+'].index is not a number');
        console.assert(v.index===-1 || (v.index>=0 && v.index<v.e.length),'path['+i+'].index ('+v.index+') has improper value');
        console.assert(typeof(v.id)==='undefined','path['+i+'].id exists but it should not');
        console.assert(typeof(v.nr)==='undefined','path['+i+'].nr exists but it should not');
    }

    //LETS NOT FORGET: hash (path2) IS THE MASTER. this.path obeys and it is modeled by path2
    
    hashQuery=this.hash.hash.split('?');
    if(typeof(hashQuery[1])!=='undefined')
        console.log("Query: "+hashQuery[1]);
    console.log("Hash (without query): "+hashQuery[0]);
    //update the path array from the this.hash.hash  (hashQuery[0)
    if(hashQuery[0].length>3){
        path2=hashQuery[0].slice(3).split('/');
    } else path2=[];
    console.log("path.length="+this.path.length+" path2.length="+path2.length+". path2:");
    console.log(path2);
    
    if(typeof(hashQuery[1])!=='undefined'){
        if(hashQuery[1]==='showcase'){
            //we showcase this picture
            this.userSession.showcase=true;
        } else console.warn("Unknown hash query: "+hashQuery[1]);
    }
    /*if(typeof(hashQuery[1])!=='undefined'){
        var keyPos=hashQuery[1].search('key=');
        if(keyPos>=0){
            this.hash.key=hashQuery[1].slice(keyPos+4);
            console.log("key: "+this.hash.key);
        }
    }*/
    
    //this.path should be consistent with path2, otherwise we should recreate everything
    //we throw away from path all extra elements
    while(path2.length+1<this.path.length){
        console.log("Popping last element ("+(this.path.length-1)+") from path.");
        this.path.pop();
    }
    if(path2.length>this.path.length)
        recreatePath=true;
    else {
        console.assert((path2.length===this.path.length || path2.length+1===this.path.length));
        if(hashQuery[0][1]==='a'){
            //this is an album URL
            console.log("THIS IS an album URL");
            //Check first if the paths match
            for(i=0;i<path2.length;i++){
                index=this.path[i].index;
                console.log("index for i="+i+": "+index);
                if(index===-1 || path2[i]!==this.path[i].e[index].id){
                    recreatePath=true;
                    console.log("Recreating the path");
                }
            }
            if(!recreatePath){
                if(path2.length===this.path.length)
                    this.getAlbum();//we need to load the album.
                else {
                    //reset the index of the last path element
                    this.path[this.path.length-1].index=-1;
                    //show the last album
                    if(this.inPictureMode){
                        //if we were in picture mode we do not need to load anything!
                        //this.inPictureMode=false; -> changed inside getOut()
                        this.albumCache.getOut();
                    } else {
                        if(this.server.metadata){
                            this.displayAlbum();
                            PH.sendToServerCurrentAlbumId();
                        }
                        //else we display it when we get the metadata
                    }
                }
            }
        } else {
            //this is an image, video or thumbnail URL
            //if the path lengths are different, pop the last album
            if(path2.length!==this.path.length)this.path.pop();
            
            //Check first if the paths match
            for(i=0;i<path2.length-1;i++){
                index=this.path[i].index;
                if(index===-1 || path2[i]!==this.path[i].e[index].id)
                    recreatePath=true;
            }
            if(!recreatePath){
                //show pictures
                index=this.path[this.path.length-1].index;
                if(index===-1){
                    //we need to find the index
                    id2find=path2[path2.length-1];
                    albumElements=this.path[this.path.length-1].e;
                    for(i=0;i<albumElements.length;i++)
                        if(albumElements[i].id===id2find){
                            index=i;
                            break;
                        }
                    //we also need to go fullscreen
                    elmShow('fullscreendiv');
                    this.enterFullScreen();
                    elmShow('imgdiv');
                    if(!this.userPrefsGlobal.skipImgHelp && !this.userSession.showcase)
                        this.showImgHelp();
                }
                console.assert(index>=0,"index="+index);

                //this.inPictureMode=true; -> changed inside displayMedia
                this.albumCache.displayMedia(index);
            }
        }
    }
    
    
    if(this.hash.lastHash==='nothing'){
        console.log("calling getServerMetadata() because lastHash is \"nothing\"");
        this.getServerMetadata();
    }
    this.hash.lastHash=hashQuery[0];
        
    if(!recreatePath){
        //this.hash.lastHash=hashQuery[0];
        return;
    } //we are done
    
    console.log("onHashChanged: Recreating the path: "+this.hash.hash);
    console.assert(path2.length+1>=this.path.length,"Lenths mismatch: "+path2.length+"+1>="+this.path.length);
    
    for(i=0;i<this.path.length;i++){
        index=this.path[i].index;
        if(index===-1 || this.path[i].e[index].id!==path2[i])
            break;
    }
    while(this.path.length>i)
        this.path.pop();
    this.hash.targetPath=path2;
    if(hashQuery[0][1]==='a')
        this.hash.targetPathIsAlbum=true;
    else
        this.hash.targetPathIsAlbum=false;
    console.log("Recreating the path from truncated path:");
    console.log(this.path);
    this.getFullPath();
};//onHashChange

PH.sendToServerCurrentAlbumId=function(){
	var p='/e',i;
    for(i=0;i<PH.path.length-1;i++)
        p+='/'+PH.path[i].e[PH.path[i].index].id;
    ajaxJson(PH.createGetUrl(p,null,false),null,null,PH.ajaxError);
    return;
};

PH.setupWebsocket=function(){
    var protocol,query='';
    if(typeof(window.WebSocket)==='undefined'){
        console.log("This platform does NOT have WebSockets");
        return;
    }

    if(this.server.websocket && typeof(this.server.websocket)==='object' &&
            (PH.server.websocket.readyState===WebSocket.OPEN || PH.server.websocket.readyState===WebSocket.CONNECTING)){
        console.log("WebSocket already setup. State: "+(PH.server.websocket.readyState?'OPEN':'CONNECTING'));
        return;
    }

    if(location.protocol==='https:')protocol='wss://';
    else protocol='ws://';
    console.log("Websocket protocol: "+protocol);
    if(PH.userSession.sessionId)
        query='?sid='+PH.userSession.sessionId;
    if(PH.server.key){
        if(query)query+='&key='+PH.server.key;
        else query+='?key='+PH.server.key;
    }

    this.server.websocket=new WebSocket(protocol+location.host+query, "status.photostovis.net");
    this.server.websocket.onerror=function(e){
        console.warn("Websocket error:");
        console.log(e);
    }
    this.server.websocket.onclose=function(e){
        var tNow;
        console.warn("Websocket closing because:");
        console.log(e);
        if(PH.server.websocketKeepAlive){
            tNow=Date.now();
            if(tNow<PH.server.websocketKeepAlive){
                setTimeout(function(){
                    PH.setupWebsocket();
                },0);
                console.log("Reopening the websocket because it was closed and we keep it alive another "+((PH.server.websocketKeepAlive-tNow)/1000)+" seconds.");
                return;
            } else PH.server.websocketKeepAlive=null;
        }
        if(!PH.server.websocketKeepAlive){
            elmHTML('sessions','');
            elmHTML('fs_sessions','');
        }
    }
    this.server.websocket.onopen=function(e){
        var x;
        console.log("Websocket opened successfully.");

        //sending all the messages from the queue
        if(PH.server.websocketQueue.length){
            console.log("websocket queue containing "+PH.server.websocketQueue.length+" elements. Sending them:");
            while(PH.server.websocketQueue.length){
                x=PH.server.websocketQueue.shift();
                console.log("Sending over websocket: "+x);
                PH.server.websocket.send(x);
            }
        }
    }

    this.server.websocket.onmessage=function(e){
	    if(e.data.length<200)console.log("New WEBSOCKET data received: <<"+e.data.length+">>");
        else console.log("New WEBSOCKET data received, length: "+e.data.length);
        
        var obj=JSON.parse(e.data);
        var p,i,txt,j;
        var s,sfs;
        var oldIndexId,oldElm,newElm;
        
        console.assert(typeof(obj)==='object');
        console.log(obj);
        if(typeof(obj.type)!=='string'){
            console.error("Unhandled websocket message (does not have a type)");
            return;
        }
        //PH.increaseWebsocketTimeout(20); -> only for some messages, not for all

        switch(obj.type){
        case 'br':
            console.log("WEBSOCKET message type: bitrate. Bitrate: "+obj.b);

            PH.showBitrate(obj.b);
            //recompute the number of nrThumbnailsToLoad
            p=obj.b>>=10;
            if(p<1)p=1;
            if(p>20)p=20;
            if(p!==PH.strip.nrThumbnailsToLoad){
                console.log("Updating nrThumbnailsToLoad from "+PH.strip.nrThumbnailsToLoad+" to "+p);
                PH.strip.nrThumbnailsToLoad=p;
            }
            PH.increaseWebsocketTimeout(5);
            break;
        case 'ss':
            console.log("WEBSOCKET message type: sessions. Nr sessions: "+obj.ss);

            s=elm('sessions');sfs=elm('fs_sessions');
            if(obj.ss>1)
                s.innerHTML=sfs.innerHTML=obj.ss+" users";
            else
                s.innerHTML=sfs.innerHTML="1 user (you)";
            break;

        case 'md':
            console.log("WEBSOCKET message type: metadata.");
            PH.applyServerMetadata(obj,true);
            PH.increaseWebsocketTimeout(10);
            break;
            
        case 'scanning':
        	console.log("WEBSOCKET message type: scanning. current status: "+obj.status);
        	PH.scanningStatusChanged(obj.status);
        	break;

        case 'album':
            console.log("WEBSOCKET message type: ALBUM UPDATE.");
            PH.increaseWebsocketTimeout(10);
            
            if(!PH.path.length){
	            console.warn("Received an album update but our path has ZERO elements. Ignoring update");
	            return;
            }
            if(!PH.server.metadata){
	            console.warn("Received an album update but we have NO metadata yet. Ignoring update");
	            return;
            }
            
            if(obj.a.l<PH.path.length-1){
	            if(PH.path[obj.a.l].a.id!==obj.a.id){
		            console.warn("Received a different previous/path album than what we currently have. Ignoring");
		            PH.sendToServerCurrentAlbumId();
		            return;
   	            }
	            console.log("Received album is previous album (from the path)");
	            
	            i=obj.a.l;
	            if(PH.path[i].index>=0)
	            	oldIndexId=PH.path[i].e[PH.path[i].index].id;
	            else oldIndexId=null;
	            
	            PH.path[i].a=obj.a;
                PH.path[i].e=obj.e;
                
                if(oldIndexId && oldIndexId!==PH.path[i].e[PH.path[i].index].id){
	                console.log("NEED TO COMPUTE NEW INDEX...");
	                for(j=0;j<PH.path[i].e.length;j++)
	                	if(oldIndexId===PH.path[i].e[j].id){
		                	PH.path[i].index=j;
		                	console.log("New index found at position: "+j);
		                	break;
	                	}
	                if(j>=PH.path[i].e.length)
	                	PH.path[i].index=0;
                }
                return;
            }
            
            if(obj.a.l>PH.path.length-1){
	            console.warn("Received an album with a level above what we have. Ignoring");
	            PH.sendToServerCurrentAlbumId();
		        return;
            }
            
			console.assert(obj.a.l===PH.path.length-1);
			
			if(PH.path[PH.path.length-1].a.id!==obj.a.id){
                console.warn("Received a different album than what we are currently displaying. Ignoring");
                PH.sendToServerCurrentAlbumId();
                return;
            }
            console.log("Received album is current album");

            //do we display or redisplay album?
            oldElm=PH.path[PH.path.length-1].e;
            newElm=obj.e;
            i=-1;
            if(oldElm.length===newElm.length){
                for(i=0;i<newElm.length;i++){
                    //was the album re-sorted?
                    if(oldElm[i].id!==newElm[i].id){
                        console.log("Will display the received/current album (IDs do not match for i="+i+"), probably re-sorted");
                        break;
                    }
                    //will a new element be displayed (because pictures+videos !== zero anymore)?
                    if((newElm[i].p+newElm[i].v===0 && oldElm[i].p+oldElm[i].v!==0) ||
                            (newElm[i].p+newElm[i].v!==0 && oldElm[i].p+oldElm[i].v===0)){
                        console.log("Will display the received/current album (new elements will be displayed this time)");
                        break;
                    }
                    //something has a different size now?
                    if(oldElm[i].sz!==newElm[i].sz){
                        console.log("Will display the received/current album (sizes do not match for i="+i+")");
                        break;
                    }
                }//for(...
                if(i===newElm.length)
                	console.log("Will reDisplay the received/current album.");
            } else console.log("Will display the received album (has different length than existing data)");

			//console.log("path length: "+PH.path.length);
			//console.log("Index: "+PH.path[PH.path.length-1].index);
			
			i=PH.path.length-1;
			if(PH.path[i].index>=0)
				oldIndexId=PH.path[i].e[PH.path[i].index].id;
			else oldIndexId=null;
			
			PH.path[i].a=obj.a;
            PH.path[i].e=obj.e;
            
            if(oldIndexId && oldIndexId!==PH.path[i].e[PH.path[i].index].id){
                console.log("NEED TO COMPUTE NEW INDEX FOR CURRENT ALBUM...");
                for(j=0;j<PH.path[i].e.length;j++)
                	if(oldIndexId===PH.path[i].e[j].id){
	                	PH.path[i].index=j;
	                	console.log("New index found at position: "+j);
	                	break;
                	}
                if(j>=PH.path[i].e.length)
                	PH.path[i].index=0;
            }
             
            if(PH.path[PH.path.length-1].index===-1){
                //we are NOT in picture showing mode
                if(i===newElm.length)
                    PH.reDisplayAlbum(obj);
                else 
                    PH.displayAlbum(true); //"true" preserves the position
            } else {
                console.log("Currently showing a picture. Will display the album on exit from picture mode.");
                PH.path[PH.path.length-1].forceDisplay=true;
            } 
            break;

        case 'zipdld':
            console.log("WEBSOCKET message type: zip download.");
            if(typeof(PH.zipDownload)==='boolean' && PH.zipDownload===false)
                return; //we ignore the message, we do not need it (download either ongoing or cancelled)

            if(typeof(obj.prg)==='number'){
                elm('zip_progress_bar').style.width=obj.prg+'%';
            } else if(typeof(obj.link)==='string'){
                elm('zip_progress_bar').style.width='100%';
                //start the download
                console.log("Download link: "+obj.link);
                window.location=PH.createGetUrl(obj.link,null,false);
                //clear the zip object
                PH.zipDownload=false;
                //dismiss the settings window
                PH._hideSettings();
            } else console.error("Unrecognized zipdownload message");
            PH.increaseWebsocketTimeout(10);
            break;

        default:
            console.error("Unhandled message, type: "+obj.type);
        }//switch
    };//websocket.onmessage
};

PH.sendWebsocketMessage=function(msg){
    setTimeout(function(){
        var err;
        if(typeof(window.WebSocket)==='undefined'){
            console.warn("This platform does not implement WebSockets, and no alternative method is implemented!");
            //TODO: send the message as e.g. PUT
            return;
        }

        console.log("sendWebsocketMessage. State="+PH.server.websocket.readyState);
        if(PH.server.websocket.readyState!==WebSocket.OPEN){
            PH.server.websocketQueue.push(msg);
            PH.setupWebsocket();
            return;
        }

        try{
            PH.server.websocket.send(msg);
        } catch(err){
            console.warn("Sending websocket message failed with error: "+err.message+". Will try re-opening the socket");
            PH.server.websocketQueue.push(msg);
            PH.setupWebsocket();
        }
    },0);
};
PH.increaseWebsocketTimeout=function(min){
    var tNow=Date.now();
    var newKeepAlive=tNow+min*60000;

    if(typeof(this.server.websocketKeepAlive)!=='number' || this.server.websocketKeepAlive<newKeepAlive)
        this.server.websocketKeepAlive=newKeepAlive;
};

PH.GoogleMapsInitialized=function(){
    console.log("Google Maps initialized and ready to use.");
}

//start
document.addEventListener('DOMContentLoaded',function(){
    PH.onDOMContentLoaded();
},false);

