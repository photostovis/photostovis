"use strict";

PH.ajaxError=function(err,txt){
    var e;
    console.assert(typeof(err)==='number');
    console.log("ajaxError: err="+err+(typeof(txt)==='string'?' ('+txt+')':''));
    if(err===403){
        PH.showAuthPage('auth_required');
    }
}

PH.showAuthPage=function(role){
    var e;
    /*we have 2 possible roles so far:
        auth_required: needed to view pictures. No Cancel button.
        signin: to Upload pictures, Admin, etc. Can cel button that restores the current state
    */
    if(role==='auth_required'){
        PH.userSession.sessionId=0;
        PH.userSession.authUser=0xFF; //anonymous
        //show the authentication page
        e=elm('authTitle');
        console.log("Authentication attempts: "+PH.userSession.authAttempts);
        if(PH.userSession.authAttempts>0){
            //replace the text with something else
            e.innerHTML=PH.l10n('txt_auth_wrong_unorpw');
            e.style.color='red';
            //we also delete the saved credentials, if any
            PH.userPrefsLocal.authUsername=null;
            PH.userPrefsLocal.authPassword=null;
            PH.saveLocalPreferences();
            PH.userSession.authUsername=null;
            PH.userSession.authPassword=null;
        } else {
            e.innerHTML=PH.l10n('txt_auth_title_original');
            e.style.color='black';
        }
        elm('authPassword').value='';
        elmHide('authCancel');
    } else if(role==='signin'){
        //show the authentication page
        e=elm('authTitle');
        e.innerHTML=PH.l10n('txt_auth_title_signin');
        e.style.color='black';
        elm('authPassword').value='';
        elmClear('authCancel');
    } else console.assert(0);

    if(PH.server.metadata && typeof(PH.server.metadata.un)==='string')
        elmHTML('authServerName','Photostovis@'+PH.server.metadata.un);

    elmShow('authBg');
    elmShow('authHeader');
    elmHide('forbiddenBox');
    elmShow('authBox');
    elmShow('authWrapper');
    //just in case: show the sign in button, hide the timeout
    if(PH.userSession.authTimeout){
        console.log("ajaxError: Clearing auth timeout");
        clearTimeout(PH.userSession.authTimeout);
        PH.userSession.authTimeout=null;
    } else {
        //the timeout triggered already
        elmHTMLhide('authSpinnerBox','');
        elmShowI('authSignIn');
        console.log("ajaxError: Auth timeout already fired.");
    }
    //show the auth thingy
    setTimeout(function(){
        elm('authWrapper').style.opacity=0.9;
        elm('authUsername').focus();
    },100);
}
PH.hideAuthPage=function(){
    elmClear('authBg');
    elmClear('authHeader');
    elmClear('authWrapper');
    elm('authWrapper').style.opacity='';
    if(PH.userSession.authTimeout){
        console.log("Clearing auth timeout");
        clearTimeout(PH.userSession.authTimeout);
        PH.userSession.authTimeout=null;
    } else {
        //the timeout triggered already
        elmHTMLhide('authSpinnerBox','');
        elmShowI('authSignIn');
        console.log("auth timeout already fired (or never started).");
    }
}

PH.showForbiddenPage=function(err){
    if(PH.server.metadata && typeof(PH.server.metadata.un)==='string')
        elmHTML('authServerName','Photostovis@'+PH.server.metadata.un);

    elmShow('authBg');
    elmShow('authHeader');
    elmHide('authBox');
    elmShow('forbiddenBox');

    switch(err){
    case -10:elmHTML('forbiddenExplanation',PH.l10n('txt_forbidden_noaccess'));break;
    case -11:elmHTML('forbiddenExplanation',PH.l10n('txt_forbidden_nousers'));break;
    default:console.assert(0);
    }

    elmShow('authWrapper');

    //show the auth thingy
    setTimeout(function(){
        elm('authWrapper').style.opacity=0.9;
    },100);
}

PH.createGetUrl=function(command,query,isPriviledged){
    console.assert(typeof(command)==='string' && command.length>1);
    console.assert(query===null || typeof(query)==='string');
    console.assert(typeof(isPriviledged)==='boolean');
    console.assert(command.indexOf('?')===-1,'createGetUrl: the supplied command <<'+command+'>> already contains a query');

    if(isPriviledged && PH.server.key){
        console.error("Refusing to create a URL for a priviledged command for key-based authentication user!");
        return null;
    }

    if(query===null)query='';
    if(PH.server.key)
        query+='&key='+PH.server.key;

    if(PH.userSession.sessionId)
        query+='&sid='+PH.userSession.sessionId;
    if(query && query[0]==='&')
        query=query.slice(1);
    if(query && query[0]==='?')
        return command+query;
    return command+'?'+query;
}

PH.handleReceivedForbidden=function(obj){
    var newUrl,objToSend;
    console.assert(typeof(obj.forbidden)==='boolean' && obj.forbidden===true && typeof(obj.reason)==='number');
    console.log("We got a FORBIDDEN answer with reason="+obj.reason);
    if(typeof(obj.un)==='string' && obj.un.length>0){
        if(!PH.server.metadata)PH.server.metadata={};
        PH.server.metadata.un=obj.un;
    }

    /* Possible answers:
    -10: client not allowed to connect from Internet
    -11: Client allowed in theory to connect, but there are no configured users to allow this.
    -12: authentication requires https
    */

    //lets see what the reason is
    if(obj.reason===-10 || obj.reason===-11){
        //we need to show the forbidden page
        PH.showForbiddenPage(obj.reason);
    } else if(obj.reason===-12){
        console.assert(location.protocol ==='http:');
        console.assert(typeof(obj.port)==='number' && obj.port>0);
        console.assert(typeof(obj.fromLAN)==='number' && (obj.fromLAN===0 || obj.fromLAN===1));

        newUrl='https://'+location.hostname+':'+obj.port+location.pathname+location.search+location.hash;

        if(parent===self || this.server.embedded){
            console.log("Reloading the page over https. New URL: "+newUrl);
            location.href=newUrl; //location.replace(newUrl) does not work if the server is insecure
        } else {
            if(obj.fromLAN){
                console.log("Ask the parent to reload us directly. New URL: "+newUrl);
                objToSend={reloadOverHttps:newUrl};
            } else {
                console.log("Ask the parent to reload us over https.");
                objToSend={reloadOverHttps:''};
            }
            parent.postMessage(objToSend,"*");
        }
    } else {
        console.assert(obj.reason===-1);
        PH.showAuthPage('auth_required');
    }
}

PH.applyServerMetadata=function(obj,fromWebsocket){
    var showAttention=false;
    var oldMetadata=PH.server.metadata;
    var i;

	PH.dismissMessage();
    //is this an authentication error?
    if(typeof(obj.forbidden)==='boolean' && obj.forbidden===true){
        //this is authentication error
        PH.handleReceivedForbidden(obj);
        return;
    }

    //we received real metadata
    PH.server.metadata=obj;
    console.log("Metadata received: totalElements: "+obj.te+", scannedElements: "+obj.se+", received cameras: "+
            ((typeof(obj.m)!=='undefined')?obj.m.length:"none"));
    //console.log(obj);
    //apply server version
    var versionTxt=''+(PH.server.metadata.v>>8)+'.'+(PH.server.metadata.v%256);
    elmHTML('about_version',versionTxt);
    if(typeof(PH.server.metadata.n)==='number')elmHTML('about_nr_cameras',PH.server.metadata.n);
    else elmHTML('about_nr_cameras','unknown'); //TODO: translate this
    //do we have info about space?
    //console.log(PH.server.metadata);
    
    if(typeof(PH.server.metadata.tds)==='number' && typeof(PH.server.metadata.fds)==='number'){
	    elmHTML('about_space',((PH.server.metadata.tds-PH.server.metadata.fds)>>10)+' / '+(PH.server.metadata.fds>>10)+' / '+(PH.server.metadata.tds>>10));
	    elm('about_space').parentNode.style.display='';
    } else
    	elm('about_space').parentNode.style.display='none';   
    
    //check the status flags: are we reachable from outside?
    console.assert(typeof(PH.server.metadata.pcs)==='number');
    if(PH.server.metadata.pcs!==PH.server.phonetCfgStatus.PhonetCfgNeverTriedToRegister){
        //registration to photostovis.net done.
        if(PH.server.metadata.pcs!==0) //0 means success
            showAttention=true;//there was a problem with the server start command
        else if(typeof(PH.server.metadata.un)!=='string')
            showAttention=true;//this server is not registered to photostovis.net
    } //else registration in progress. Show something? Probably not

    if(showAttention && !PH.server.key){
        //if this is a guest link we do not show the attention button
        elmClear('attention-btn');
    }

    //do we have sessionId and permissions?
    if(typeof(PH.server.metadata.sid)==='string'){
        console.log("We have a session ID!");
        PH.userSession.sessionId=PH.server.metadata.sid;
        //we should close the existing websocket, if any
        PH.server.websocketKeepAlive=null;
        if(PH.server.websocket){
            PH.server.websocket.close();
            PH.server.websocket=null;
        }

        //we should also have permissions
        console.assert(typeof(PH.server.metadata.canUpload)==='number');
        console.assert(typeof(PH.server.metadata.canShare)==='number');
        console.assert(typeof(PH.server.metadata.canAdmin)==='number');

        if(PH.server.metadata.canUpload){
            PH.userSession.canUpload=true;
            //show uplod menu
            elm('menu_upload').className='menu-item';
        } else {
            PH.userSession.canUpload=false;
            //hide upload menu
            elm('menu_upload').className='menu-item-disabled';
        }

        if(PH.server.metadata.canShare){
            PH.userSession.canShare=true;
            //show share menus
            elm('menu_linkurl').className='menu-item';
            PH.sendMessageToFbFrame('photostovis_enable');
        } else {
            PH.userSession.canShare=false;
            //hide share menus
            elm('menu_linkurl').className='menu-item-disabled';
            PH.sendMessageToFbFrame('photostovis_disable');
        }

        if(PH.server.metadata.canAdmin){
            PH.userSession.canAdmin=true;
            //show shutdown menu
            elm('menu_shutdown').className='menu-item';
        } else {
            PH.userSession.canAdmin=false;
            //hide shutdown menu
            elm('menu_shutdown').className='menu-item-disabled';
        }

        if(PH.server.metadata.canSignin){
            PH.userSession.canSignin=true;
            //show signin menu
            elmShow('menu_signin');
        } else {
            PH.userSession.canSignin=false;
            //hide signin menu
            elmHide('menu_signin');
        }
        if(PH.server.metadata.canSignout){
            PH.userSession.canSignout=true;
            //console.log("Can signout!!!"+PH.userSession.authUsername);
            //show signout menu
            if(typeof(PH.userSession.authUsername)==='string' && PH.userSession.authUsername.length>0){
                elmHTML('signoutUsername',' ('+PH.userSession.authUsername+')');
            }
            elmShow('menu_signout');
        } else {
            PH.userSession.canSignout=false;
            //hide signout menu
            elmHide('menu_signout');
        }

        //if(PH.server.metadata.fromLAN)

        //do we need to hide the authentication page?
        PH.userSession.authAttempts=0; //reset it
        PH.hideAuthPage();

        //do we have a config?
        if(typeof(PH.server.metadata.config)==='object' && PH.userSession.canAdmin){
            PH.userSession.config=PH.server.metadata.config;
            if(PH.server.metadata.config.cfgFlags&PH.server.configFlags.CfgFlagConfigurationRequired){
                console.log("We should show the configuration wizzard!");
                PH.configWizard(false);
            }
        } else elmHide('menu_config');
    }//if(we have sessionId)

    //do we need to show the header?
    if(document.getElementById('header').style.top!=='0')
        setTimeout(function(){document.getElementById('header').style.top="0";},0);

    if(!oldMetadata || typeof(fromWebsocket)!=='boolean' || !fromWebsocket){
        if(!PH.hash.targetPath.length && PH.path.length>0){
            console.log("Building targetPath from path");

            PH.hash.targetPath=[];
            for(i=0;i<PH.path.length;i++){
                console.assert(typeof(PH.path[i].index)==='number');
                if(PH.path[i].index<0 || typeof(PH.path[i].e[PH.path[i].index])!=='object')
                    break;

                PH.hash.targetPath.push(PH.path[i].e[PH.path[i].index].id);
            }
        }
        PH.path=[]; //reseting path
        PH.getFullPath();
    }
    
    //do we have scanning?
    if(typeof(PH.server.metadata.scst)==='number')
	    PH.scanningStatusChanged(PH.server.metadata.scst);

    //check for websocket support
    PH.setupWebsocket();
};


PH.getServerMetadata=function(username,password){
    console.log("getServerMetadata++");
    var params='smallDevice='+(PH.platform.smallDevice?'true':'false')+'&screenWidth='+PH.platform.screenWidth+'&screenHeight='+PH.platform.screenHeight+
            '&pixelRatio='+PH.platform.pixelRatio;
    var url='/m';
    
    if(PH.server.key){
        url+='?key='+PH.server.key;
        PH.userSession.authUser=0xFE;
    } else {
        //if we have a key, we do not send username & password
        if(typeof(username)==='string' && typeof(password)==='string'){
            params+='&un='+username+'&pw='+password;
            PH.userSession.authUser=0;
            PH.userSession.authUsername=username;
            PH.userSession.authPassword=password;
        } else {
            //check if we have them in localPreferences
            if(typeof(PH.userPrefsLocal.authUsername)==='string' && typeof(PH.userPrefsLocal.authPassword)==='string'){
                params+='&un='+PH.userPrefsLocal.authUsername+'&pw='+PH.userPrefsLocal.authPassword;
                PH.userSession.authUser=0;
                PH.userSession.authUsername=PH.userPrefsLocal.authUsername;
                PH.userSession.authPassword=PH.userPrefsLocal.authPassword;
                console.log("Username & password found in userPrefsLocal");
                //show an auth message
                PH.showMessage("Authenticating...",true);
            }
        }
    }
    if(PH.userSession.sessionId){//we have a session already
        if(url.length===2)url+='?sid='+PH.userSession.sessionId;
        else url+='&sid='+PH.userSession.sessionId;
    }

	ajaxJson(url,params,PH.applyServerMetadata,PH.ajaxError);

    console.log("getServerMetadata--");
};

PH.showBitrate=function(bitrate){
    if(bitrate<=0)return;
    this.currentBitrate=bitrate;
    this.lastBitrateUpdate=Date.now();
    var e=elm('bitrate'),efs=elm('fs_bitrate');
    efs.style.opacity=e.style.opacity='1';
    if(bitrate>1024){
        bitrate/=1024;
        if(bitrate>=10)
            efs.innerHTML=e.innerHTML=bitrate.toFixed(0)+"Mbps";
        else
            efs.innerHTML=e.innerHTML=bitrate.toFixed(1)+"Mbps";
    } else
        efs.innerHTML=e.innerHTML=bitrate+"Kbps";

    setTimeout(function(){
        //do we start fading out?
        var tNow=Date.now();
        if(tNow-PH.lastBitrateUpdate<10000)return;

        efs.style.transition=e.style.transition='opacity 3s linear';
        efs.style.opacity=e.style.opacity='0.01';
        setTimeout(function(){
            efs.style.transition=e.style.transition='';
        },3000);
    },10000);
};

PH.saveGlobalPreferences=function(){
    var jsonTxt=JSON.stringify(PH.userPrefsGlobal);
    if(parent===self){
	    //save preferences locally because there is no parent
	    try {
	        if(typeof(localStorage)==='object' && localStorage){
	            localStorage.setItem('photostovisPreferencesGlobal',jsonTxt);
	        } else
	            console.warn("localStorage not defined!");
	    } catch(e) {
	        console.warn("localStorage exception catched: "+e.message+". Private mode?");
	    }
    } else
        parent.postMessage({globalPreferencesTxt:jsonTxt},"*");
};
PH.saveLocalPreferences=function(){
	var jsonTxt=JSON.stringify(PH.userPrefsLocal);
    if(parent===self){
	    //save preferences locally because there is no parent
	    try {
	        if(typeof(localStorage)==='object' && localStorage){
	            localStorage.setItem('photostovisPreferencesLocal',jsonTxt);
	        } else
	            console.warn("localStorage not defined!");
	    } catch(e) {
	        console.warn("localStorage exception catched: "+e.message+". Private mode?");
	    }
	} else
        parent.postMessage({localPreferencesTxt:jsonTxt},"*");
};
PH.getPreferences=function(){
	if(parent!==self)return; //preferences are coming from parent

	let preferencesObj;
    try {
        if(typeof(localStorage)==='object' && localStorage){
            preferencesObj=localStorage.getItem('photostovisPreferencesGlobal');
            if(preferencesObj)this.userPrefsGlobal=JSON.parse(preferencesObj);

            preferencesObj=localStorage.getItem('photostovisPreferencesLocal');
            if(preferencesObj)this.userPrefsLocal=JSON.parse(preferencesObj);

            if(parent!==self && !this.server.embedded)
                this.saveGlobalPreferences(false);
        } else
            console.warn("localStorage not defined!");
    } catch(e) {
        console.warn("localStorage exception catched: "+e.message+". Private mode?");
    }
    console.log("User preferences (global, local):");
    console.log(this.userPrefsGlobal);
    console.log(this.userPrefsLocal);
}

/////////////// Toolbar: Upload & Edit ////////////////////////////////////////////////////

PH.showUploadToolbar=function(){
	//shift everything down
	PH.userSession.headerHeight+=2;
	elm('cntnt').style.top=elm('header').style.height=PH.userSession.headerHeight+'em';
	//show the toolbar after 0.3s
	elmShow('tbEdit');
	setTimeout(function(){
		elm('tbEdit').style.opacity='1';
	},300);
}
PH.onEditToolbarClose=function(){
	elm('tbEdit').style.opacity='0';
	setTimeout(function(){
		elmClear('tbEdit');
		PH.userSession.headerHeight-=2;
		elm('cntnt').style.top=elm('header').style.height=PH.userSession.headerHeight+'em';
	},300);
}

/////////////// Settings ////////////////////////////////////////////////////

PH._showSettings=function(itemId,title,allowDismiss){
	elmHTML('settings_title',title);
	elmShow(itemId);
    elmShow('settings_bg');
    setTimeout(function(){
	    elm('settings_bg').style.backgroundColor='rgba(0,0,0,0.8)';
	    elm('settings_box').style.opacity='1';
	},100);
    this.settingShows=itemId;
    this.settingAllowDismiss=allowDismiss;
}
PH._hideSettings=function(){
	console.log("settingsShows: "+this.settingShows);
	
	if(this.settingShows==='settings_zip' && this.zipDownload){
		//confirm it!
		if(!confirm("Cancel zip archive preparation?"))
			return; //user pressed cancel, so we continue
		this.zipDownload=false;
	}
	
	elm('settings_box').style.opacity='0';
	elm('settings_bg').style.backgroundColor="rgba(0,0,0,0)";
	setTimeout(function(){
		elmClear('settings_bg');
		elmClear(PH.settingShows);
		PH.settingsShow=null;
	},300);
}

PH.showLanguageSelector=function(allowDismiss){
    //pre-select the language, if any
    if(this.userPrefsGlobal.language){
        var i;
        var sel=document.getElementsByName('settings_lang');
        for(i=0;i<sel.length;i++)
            if(this.userPrefsGlobal.language===sel[i].value){
                sel[i].checked=true;
                break;
            }
    }    
    this._showSettings('settings_lng','Language:',allowDismiss);
};



PH.showCreateNewSubalbum=function(){
	elm('csaName').value='';
    this._showSettings('settings_csa','Create a new sub-album:',true);
    elm('csaName').focus();
}

PH.showUploadMedia=function(){
	if(PH.userSession.sessionId){
        elm('ph_dropzone').action='/u?sid='+PH.userSession.sessionId;
        if(this.dropzone){
            console.assert(typeof(this.dropzone.options.url)==='string');
            this.dropzone.options.url='/u?sid='+PH.userSession.sessionId;
        }
    } else {
        elm('ph_dropzone').action='/u';
        if(this.dropzone){
            console.assert(typeof(this.dropzone.options.url)==='string');
            this.dropzone.options.url='/u';
        }
    }
    //create dropzone, if none created
    if(!this.dropzone)
        this.dropzone=new Dropzone("#ph_dropzone");
    elm('settings_upl').style.width=(document.body.clientWidth/2)+'px';
    
    this._showSettings('settings_upl','Upload photos & videos',false); 
}
PH.photosFinishedUploading=function(){
	var i,query='?a=';
	//we need to create the upload url
    for(i=1;i<this.path.length;i++)
        query+='/'+this.path[i].a.id;
    if(query.length===3)query+='/';    
        
    ajaxJson(this.createGetUrl('/u',query,true),null,function(obj){
        console.assert(typeof(obj)==='object');
        console.assert(typeof(obj.a)==='boolean');
        
        if(obj.a){
            console.log("Uploading succeeded!");
        } else {
	        console.assert(typeof(obj.err)==='number');
            console.log("Uploading failed with error: "+obj.err);

            PH.showMessage("Uploading photos & videos failed with error: "+obj.err,false);
        }        
    },PH.ajaxError);
}

PH.showLink=function(){
	elm('settings_lnk_link').value='';
    this._showSettings('settings_lnk','Link to album:',true);
    
    //send the request to the server 
    var url='/k',i;
    for(i=1;i<this.path.length;i++)
        url+='/'+this.path[i].a.id;
    ajaxJson(this.createGetUrl(url,null,true),null,function(obj){
        var e=elm('settings_lnk_link');
        if(typeof(obj)==='object'){
            if(typeof(obj.link)==='string'){
                e.value=obj.link;
                e.focus();
                e.select();
            } else if(typeof(obj.err)==='number')
                e.value="Server must be registered and have a name to generate a link.";
            else 
                e.value="Unspecified error retrieving link from the server.";
        } else
            e.value="Unspecified error retrieving link from the server (returned data not understood).";
    },PH.ajaxError);
};

PH.showDownloadZip=function(){
	elm('zip_progress_bar').style.width='0';
    this._showSettings('settings_zip','Download album as zip',true);
    
    this.zipDownload=true;
    
    //send the request for size to the server 
    var url='/n',i;
    for(i=1;i<this.path.length;i++)
        url+='/'+this.path[i].a.id;
    ajaxJson(this.createGetUrl(url,null,false),null,function(obj){
        var e=elm('settings_zip_txt');
        if(typeof(obj)==='object'){
            if(typeof(obj.sz)==='number' && typeof(obj.p)==='number' && typeof(obj.v)==='number'){
                if(typeof(PH.zipDownload)==='boolean' && PH.zipDownload===true){
                    e.innerHTML='The zip file is being prepared. This will take some time.<br>(The download will have approx '+
                            obj.sz+'MB and will contain '+obj.p+' pictures and '+obj.v+' videos.';
                    PH.zipDownload=obj;
                }// else we should be downloading the file already, or there was a cancel, or something
            } else if(typeof(obj.err)==='number'){
                if(obj.err===-10)
                    e.innerHTML='This album is too big to be downloaded as zip. Album size: '+obj.sz+'MB.';
                else 
                    e.innerHTML='Preparing the album zip file failed. Error: '+obj.err;
            } else 
                e.innerHTML='Preparing the album zip file failed: unknown error.';
        } else
            e.innerHTML='Preparing the album zip file failed: Unspecified error (returned data not understood).';
    },PH.ajaxError);
};


PH.showRegistrationErr=function(){
    var notificationTxt='';
    console.log("showRegistrationErr");
    console.assert(typeof(this.server.metadata.pcs)==='number');

    if(this.server.metadata.pcs!==this.server.phonetCfgStatus.PhonetCfgNeverTriedToRegister){
        if(this.server.metadata.pcs>0){
            //there were problems with the start server command
            if(this.server.metadata.gcs!==0){
                //there was a problem with the internet gateway configuration
                notificationTxt+='<p>'+this.l10n('txt_err_upnp')+'</p><p>'+this.l10nGatewayConfigError(this.server.metadata.gcs)+'</p>';
            }
            if(this.server.metadata.pcs!==0){
                //there was a problem with phonet registration
                notificationTxt+='<p>'+this.l10n('txt_err_phonet')+'</p><p>'+this.l10nPhonetStartError(this.server.metadata.pcs)+'</p>';
                if(typeof(this.server.metadata.pcsErrStr)==='string')
                	notificationTxt+='<p>'+this.server.metadata.pcsErrStr+'</p>';
            }
        } /*else {
            //server start was successfull
            if(typeof(this.server.metadata.un)!=='string'){
                notificationTxt+='<p>'+this.l10nPhonetStartError(this.server.metadata.pcs)+
                        '<button type="button" id="message-btn-register" onclick="PH.showRegistration()">'+
                        (this.server.metadata.pe?this.l10n('txt_reregister_srv'):this.l10n('txt_register_srv'))+'</button></p>';
            }
        }*/
    }
    console.assert(notificationTxt!=='',"this.server.metadata.pcs="+this.server.metadata.pcs); //we have something to show

    //display the notifications
    elmHTML('settings_rer',notificationTxt);
	this._showSettings('settings_rer','Registration error',true);
};

 
 
/*
PH.validateUploads=function(){
    var continueCancel=true;
    var i,query=null,album='',subAlbum=elm('upl_subfolder').value.trim();
    console.log("subAlbum: <<"+subAlbum+">>");

    if(this.server.completedUploads===0){
        console.assert(subAlbum.length>0); //otherwise the Ok button should not be enabled
        continueCancel=confirm("Do you want to create \""+subAlbum+"\" as an empty subalbum? (You can add pictures or other albums to it later) Press Ok if yes.");
        if(!continueCancel)return;
    }

    //we need to create the upload url
    for(i=1;i<this.path.length;i++)
        album+='/'+this.path[i].a.id;
    
    if(album.length>0)
        query='?a='+album;
    if(subAlbum.length>0){
        if(query)query+='&s='+encodeURIComponent(subAlbum);
        else query='?s='+encodeURIComponent(subAlbum);
    }
    
    ajaxJson(this.createGetUrl('/u',query,true),null,function(obj){
        console.log("The answer from server:"+obj.a);
        console.assert(typeof(obj)==='object');
        console.assert(typeof(obj.a)!=='unknown');
        if(typeof(obj.a)==='string'){
            console.log("Upload failed: "+obj.a);
            PH.showMessage("Upload failed: "+obj.a,false);
            return;
        }
        //if we are here, the answer should be true or false
        console.assert(typeof(obj.a)==='boolean');
        if(obj.a){
            console.log("Upload succeeded!");
            //clear the subAlbum
            //elm('upl_subfolder').value='';
        } else {
            console.log("Upload failed!");
            PH.showMessage("Upload failed! error: "+obj.err,false);
        }
    },PH.ajaxError);
    this.server.completedUploads=0; //we reset this
    
    elmClear('settings_bg');
    elmClear('settings_upl');
};

PH.cancelUploads=function(){
    var continueCancel=true;
    
    if(this.server.completedUploads>0)
        continueCancel=confirm("Do you really want cancel and delete from Photostovis all "+
            this.server.completedUploads+" uploaded files? Press Ok if yes.");
        
    if(!continueCancel)return;
    
    Dropzone.forElement("#ph_dropzone").removeAllFiles(true);
    console.log("Dropzone: removed all files");
    
    ajaxJson(this.createGetUrl('/x',null,true),null,function(obj){
        console.log("The answer from server:"+obj.a);
        console.assert(typeof(obj)==='object');
        //if we are here, the answer should be true or false
        console.assert(typeof(obj.a)==='boolean');
        if(obj.a){
            console.log("Cancel upload succeeded!");
        } else {
            console.log("Cancel upload failed! err="+obj.err);
        }
    },PH.ajaxError);
    this.server.completedUploads=0; //we reset this
    
    elmClear('settings_bg');
    elmClear('settings_upl');
};*/


PH.validateCreateNewSubalbum=function(){
    var i,query=null,album='',subAlbum=elm('csaName').value.trim();
    var hash=PH.hash.hash;
    console.assert(this===PH);
    
    //hide the dialog
    this._hideSettings();

	if(!subAlbum.length)
		return; //nothing to create
	console.log("subAlbum: <<"+subAlbum+">>");
	query='?s='+encodeURIComponent(subAlbum);
	
    //we need to create the upload url
    for(i=1;i<this.path.length;i++)
        album+='/'+this.path[i].a.id;
        
    if(album.length>0)
        query+='&a='+album;
        
    PH.showMessage("Creating new subalbum ...", true);
    PH.userSession.creatingNewAlbum=true; //this is used so that we do not reload the full path when scanning ends.
            
    ajaxJson(this.createGetUrl('/u',query,true),null,function(obj){
        console.assert(typeof(obj)==='object');
        console.assert(typeof(obj.a)==='boolean');
        PH.userSession.creatingNewAlbum=false;
        
        if(obj.a){
            console.log("Creating a new sub-album succeeded!");
            console.assert(typeof(obj.id)==='string');
            PH.setHash(hash+'/'+obj.id);
            //we dismiss the "new album" message when it gets loaded!
            PH.userSession.dismissMessageOnAlbumLoad=true;              
        } else {
	        console.assert(typeof(obj.err)==='number');
	        if(obj.err===1){
		        //1 means that the server was scanning pictures and we need to wait a bit
		    	//TODO    
		    } else {
		        console.log("Creating a new sub-album failed with error: "+obj.err);
				PH.showMessage("Creating a new sub-album failed with error: "+obj.err,false);
	        }
	        
	        //rescan everything
	        this.hash.targetPath=[];
	        this.hash.targetPathIsAlbum=true;
	        for(i=0;i<this.path.length-1;i++)
	        	if(this.path[i].index>=0)
	            	this.hash.targetPath.push(this.path[i].e[this.path[i].index].id);
	        this.path=[];
	        this.getFullPath();
	        
        }        
    },PH.ajaxError);
}


PH.validateLanguage=function(){
    var sel,i,lng=null;
    sel=document.getElementsByName('settings_lang');
    for(i=0;i<sel.length;i++)
        if(sel[i].checked){
            lng=sel[i].value;
            break;
        }
        
    //hide the dialog
    this._hideSettings();

    if(lng!==this.userPrefsGlobal.language){
        this.userPrefsGlobal.language=lng;
        if(this.path.length===0){
            this.getServerMetadata();
            this.getAlbum(); //will retrieve the first album
        }
        else this.displayAlbum(); //we show again our album
    }
    console.assert(typeof(lng)==='string' && lng.length===2);
    this.saveGlobalPreferences(true);
};

PH.validateShutdown=function(){
    ajaxJson(this.createGetUrl('/z',null,true),null,function(obj){
	    if(obj.a)
	        PH.showMessage("Your Photostovis server is shutting down. Please wait untill all lights on the box are off.",false);
	    else 
	        PH.showMessage("You do not have the permission to shut down this Photostovis server.",false);
	},PH.ajaxError);
	
	//hide the dialog
    this._hideSettings();
    //...and display a waiting message
    PH.showMessage("Sending the shutdown message to your photostovis server.",true);
};

PH.validateSignout=function(){
    var url;
    //clear and save the authUsername and authPassword
    PH.userPrefsLocal.authUsername=null;
    PH.userPrefsLocal.authPassword=null;
    PH.saveLocalPreferences();
    PH.userSession.authUsername=null;
    PH.userSession.authPassword=null;
    //close the websocket
    console.log("validateSignout: closing the websocket");
    PH.server.websocketKeepAlive=null;
    PH.server.websocket.close();
    //clear the username & password fields
    elm('authUsername').value='';
    elm('authPassword').value='';

    //reload the current location
    console.log("server.key: "+PH.server.key);
    if(PH.server.key){
        //reload without query and hash

        if(parent!==self){
            console.log("Asking the parent to reload us.");
            parent.postMessage({photostovis_reload:'noquery'},"*");
        } else {
            url=location.protocol+'//'+location.host+location.pathname;
            console.log("Loading location: "+url);
            location.href=url;
        }
    } else {
        console.log("reloading location");
        location.reload(true);
    }
    PH=null;//in case it fails
}

/////////////// Toolbar: Scanning status ////////////////////////////////////////////////////


PH.scanningStatusChanged=function(status){
	var i;
	console.assert(status===0 || status===1 || status===2 || status===3,status);
	if(status===PH.userSession.scanningStatus)return;
	console.log("scanningStatusChanged: "+status);
	PH.userSession.scanningStatus=status;
	
	if((status===1 || status===2 || status===3) && elm('tbScanning').style.display!=='block'){
		//show the scanning toolbar
		//increase header height and display the scanning "toolbar"
		PH.userSession.headerHeight+=0.6;
		elm('cntnt').style.top=elm('header').style.height=PH.userSession.headerHeight+'em';
		//show the toolbar after 0.3s
		elmShow('tbScanning');
		setTimeout(function(){
			elm('tbScanning').style.opacity='1';
		},300);
	}
	
	if(status===1){
		elmHTML('tbScanning','Scanning photos & videos');
		elm('tbScanning').style.animation='animate-scanning-stripes 0.3s linear infinite';
	}
		
	if(status===2){
		elmHTML('tbScanning','Creating thumbnails');
		elm('tbScanning').style.animation='animate-scanning-stripes 1s linear infinite';
	}
	
	if(status===3){
		elmHTML('tbScanning','Caching thumbnails');
		elm('tbScanning').style.animation='animate-scanning-stripes 1s linear infinite';
	}
		
		
	if(status===0){
		if(elm('tbScanning').style.display==='block'){
			//hide the scanning toolbar
			elm('tbScanning').style.opacity='0';
			setTimeout(function(){
				elmClear('tbScanning');
				PH.userSession.headerHeight-=0.6;
				elm('cntnt').style.top=elm('header').style.height=PH.userSession.headerHeight+'em';
			},300);
		}
		

		//Invalidate the current data, get everything again and redisplay
		if(!PH.userSession.creatingNewAlbum){
			this.hash.targetPath=[];
	        this.hash.targetPathIsAlbum=true;
	        for(i=0;i<this.path.length-1;i++)
	        	if(this.path[i].index>=0)
	            	this.hash.targetPath.push(this.path[i].e[this.path[i].index].id);
	        this.path=[];
	        this.getFullPath();
		}
	}
}



/////////////// Show/Dismiss message ////////////////////////////////////////////////////

PH.showMessage=function(txt, waiting){
    elmHTML('message-box-msg',txt);
    
    if(waiting){
        if(!this.spinner){
            var opts={
              lines: 11, // The number of lines to draw
              length: 10, // The length of each line
              width: 6, // The line thickness
              radius: 15, // The radius of the inner circle
              corners: 1, // Corner roundness (0..1)
              rotate: 0, // The rotation offset
              direction: 1, // 1: clockwise, -1: counterclockwise
              color: '#000', // #rgb or #rrggbb or array of colors
              speed: 1, // Rounds per second
              trail: 60, // Afterglow percentage
              shadow: false, // Whether to render a shadow
              hwaccel: false, // Whether to use hardware acceleration
              className: 'spinner', // The CSS class to assign to the spinner
              zIndex: 2e9, // The z-index (defaults to 2000000000)
              top: '50%', // Top position relative to parent
              left: '50%' // Left position relative to parent
            };
            this.spinner=new Spinner(opts).spin(elm('message-box-spinner'));
        } else this.spinner.spin(elm('message-box-spinner'));
        elmClear('message-box-spinner');
        elmHide('message-box-btn-ok');
    } else {
	    if(this.spinner)this.spinner.stop();
        elmHide('message-box-spinner');
        elmClear('message-box-btn-ok');
    }
    
    elmShow('message_bg');
    setTimeout(function(){
	    elm('message_bg').style.backgroundColor='rgba(0,0,0,0.8)';
	    elm('message-box').style.opacity='1';
	},100);

};

PH.dismissMessage=function(){
    if(this.spinner)this.spinner.stop();
    
    elm('message-box').style.opacity='0';
	elm('message_bg').style.backgroundColor="rgba(0,0,0,0)";
	setTimeout(function(){
		elmClear('message_bg');
	},300);
};


