# PHOTOSTOVIS #


## What is this? ##

* Photostovis lets you store your photos and videos at home and access them from any device, anywhere.
* It is for people who value their privacy and do not want to store their personal photos and videos in the cloud ...
* ... but at the same time would like to have a cloud-like experience: access their photos from anywhere, anytime, being able to share them with others.

## Demo: ##

* Demo running on a Cubietruck, from my appartment: [https://photostovis.net/demo1](https://photostovis.net/demo1) It may be slow or unavailable sometimes, if there are too many people connected to it.
* Demo running in Amazon Cloud (AWS): [https://photostovis.net/demoaws](https://photostovis.net/demoaws)


## How is Photostovis different from other similar solutions: ##

* Allows controlled access to photos and videos, differentiates from connections coming from the local network and connections coming from Internet. For example it allows free access to all the photos for anybody connecting from the local network, but requires a password from anybody connecting from Internet.
* Photos and videos are organized in files and folders. Folders are albums & sub-albums, up to 7 levels. Photos are sorted by their date, taken from EXIF information or from the filename.
* The server can rescale photos in real-time, adapting to the bandwidth between itself and the client. The client can also control the quality, e.g. can request the original photo.
* Albums can be shared to friends by sending them a link (URL) with a key. This link gives access only to the shared album.

## Some technical highlights: ##
* Photostovis is a client-server solution
* The server part is written in C and requires very little resources. Works well on ARM single board computers, such as Raspberry Pi.
* The client is written in HTML5/CSS/Javascript and runs in any modern browser, including mobile and tablets.
* By default, connections from Internet are over HTTPS. Each server gets its own certificate & key.

## How do I get set up? ##

The Photostovis server runs on Linux (or Mac). Other BSD-like environment should work fine. There are detailed instructions and binaries for 3 environments:

* Cubieboard/Cubietruck with Cubian (a Linux distribution for Cubieboards based on Debian Wheezy)
* Raspberry Pi 2 with latest Raspbian
* Raspberry Pi 3 with latest Raspbian

### Compiling and installing on Cubian:

* [Install Cubian](http://cubian.org/downloads)
Choose the appropriate download based on your board and display set up
* Configure wireless and other settings: https://github.com/cubieplayer/Cubian/wiki/Get-started-with-Cubian
* Prerequisites: libjpeg-turbo, openssl, curl, miniupnpc
* Optional: x264, ffmpeg
* Before compiling:

        export CFLAGS="-O3 -march=armv7-a -mfloat-abi=hard -mfpu=neon-vfpv4"

#### Prerequisites:

* Download and compile libjpeg-turbo: ./configure, make, sudo make install
* Download and compile openssl: ./config, make depend, make, sudo make install_sw
* Download and compile curl:
    1. open the "configure" file in a text editor, find the next 2 lines (each of them is separate):

            return HMAC_Update ();
            return HMAC_Init_ex ();

        and then comment them so they become:

            //return HMAC_Update ();
            //return HMAC_Init_ex ();

    2. Run configure:

            ./configure --disable-shared --disable-ftp --disable-file --disable-ldap --disable-ldaps --disable-rtsp --disable-dict --disable-telnet --disable-tftp --disable-pop3 --disable-imap --disable-smb --disable-smtp --disable-gopher --with-ssl=/usr/local/ssl/ --without-libssh2 --without-librtmp --without-libidn --without-nghttp2 --without-winidn

        It is important that curl gets configured so that it supports HTTP and HTTPS

    3. make & install:

            make
            sudo make install

#### Optionals:

* Download and compile x264:

        ./configure --disable-cli --enable-static --disable-interlaced
        make -j2
        sudo make install

* Download and compile ffmpeg:

        ./configure --disable-ffplay --disable-ffprobe --disable-ffserver --disable-doc --disable-avdevice --enable-libx264 --enable-gpl
        make
        sudo make install

#### Photostovis:

* Unzip photostovis in a folder called photostovis.
* Create a folder (in the same parent folder) called photostovis-build. Go into it.
* Run:

        cmake -DCMAKE_BUILD_TYPE=CB3 ../photostovis

* Follow the post-installation section below


### Compiling and installing on Raspberry Pi

* It is recommended to use the latest Raspbian

#### Prerequisites:

* Install several important prerequisite packages:

        sudo apt-get install cmake libexif-dev libssl-dev libcurl4-openssl-dev

* Install prerequisite 1/2: libjpeg-turbo.
    * Download the tar ball from: [https://sourceforge.net/projects/libjpeg-turbo/files/](https://sourceforge.net/projects/libjpeg-turbo/files/)
    * Unpack: tar xf libjpeg-turbo...
    * Compile in the usual way:

            ./configure
            make
            sudo make install

* Install prerequisite 2/2: miniupnpc.
    * Download the tar ball from: http://miniupnp.free.fr/files/
    * Please double check that you have downloaded the right package: it should end with a C (from client) and NOT with a D (from Daemon).
    * Unpack: tar xf miniupnpc-2.0.tar.gz
    * Compile:

            make
            sudo make install


#### Optionals:

* Download and compile x264:

        ./configure --disable-cli --enable-static --disable-interlaced
        make -j2
        sudo make install

* Download and compile ffmpeg:

        ./configure --disable-ffplay --disable-ffprobe --disable-ffserver --disable-doc --disable-avdevice --enable-libx264 --enable-gpl
        make
        sudo make install

#### Photostovis:

* git clone photostovis:

        git clone https://bitbucket.org/photostovis/photostovis.git

* Create a folder (in the same parent folder) called photostovis-build. Go into it:

        mkdir photostovis-build
        cd photostovis-build

* run cmake:

        cmake -DCMAKE_BUILD_TYPE=RPI2AND3 ../photostovis

* run make:

        make

* Follow the post-installation section below


### Post Installation

Now that you have successfully built Photostovis you can try if the build has succeeded by running it:

    ./photostovis

It will probably complain that the folder */data/photostovis* does not exist and then will exit. But it proves that the built has succeeded.

Next step is to create this folder:

    sudo mkdir /data
    sudo mkdir /data/photostovis

You can change the ownership of the */data/photostovis* folder to the current user.

You may wish to create another folder called */data/photostovis/pictures* and pre-populate it with your pictures.

You can run photostovis from the build directory, or you can create and install it as a package that will start photostovis at runtime, as a service. To create this package: go to the photostovis folder (from the photostovis-build folder) and run:

    ./buildpkg_sudo.sh

This will create a package called photostovis_X.Y-1_armv7l.deb. You can install this package with the following command:

    sudo dpkg -i photostovis_X.Y-1_armv7l.deb

## Connecting to your photostovis server

* Go with a browser to [http://photostovis.net](http://photostovis.net) *from the same network as your server*. This address should redirect you to your Photostovis server.
* More details: [http://photostovis.net/PhotostovisHelp.html](http://photostovis.net/PhotostovisHelp.html)

### Questions and Feedback ###

If you run into problems or would like to send feedback, please [contact me.](https://photostovis.org/#contact)

