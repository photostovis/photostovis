#define  _GNU_SOURCE

#include <stdio.h>
#include <dirent.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <dirent.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <ftw.h>

#ifndef PATH_MAX
#define PATH_MAX 512
#endif

#define KBatchLen 65536
#define GET_PATH(f) (pathBatches[(f).pathBatch]+(f).pathOffset)

/* This is the basic CRC-32 calculation with some optimization but no
table lookup. The the byte reversal is avoided by shifting the crc reg
right instead of left and by using a reversed 32-bit word to represent
the polynomial.
   When compiled to Cyclops with GCC, this function executes in 8 + 72n
instructions, where n is the number of bytes in the input message. It
should be doable in 4 + 61n instructions.
   If the inner loop is strung out (approx. 5*8 = 40 instructions),
it would take about 6 + 46n instructions. */

uint32_t ph_crc32b(char const * const message, long long int l) {
   int i, j;
   uint32_t byte, crc, mask;

   i = 0;
   crc = 0xFFFFFFFF;
   for(i=0;i<l;i++){
      byte = message[i];            // Get next byte.
      crc = crc ^ byte;
      for (j = 7; j >= 0; j--) {    // Do eight times.
         mask = -(crc & 1);
         crc = (crc >> 1) ^ (0xEDB88320 & mask);
      }
   }
   return ~crc;
}

uint32_t getFileCRC(char const * const filename, int64_t fileSize){
    const int64_t KMaxCompare=10485760; //10 MB
    if(fileSize>KMaxCompare)
        fileSize=KMaxCompare;

    //printf("Computing CRC for %s [%lld]\n",filename,fileSize);

    char *buf=malloc(fileSize);
    if(!buf){
        printf("Could not allocate buffer: %"PRId64" bytes. Exiting.\n",fileSize);
        exit(-1);
    }
    FILE *f=fopen(filename,"r");
    if(!f){
        printf("Could not open file <<%s>>. Reason: %d (%s).\n Exiting.\n",filename,errno,strerror(errno));
        exit(-1);
    }
    assert(f);
    int n=fread(buf,fileSize,1,f);
    if(n!=1){
        printf("Could not read file: %s. Returning 0.\n",filename);
        return 0;
    }
    fclose(f);
    uint32_t crc=ph_crc32b(buf,fileSize);
    free(buf);
    //printf("CRC: %08X\n",crc);
    return crc;
}

struct file_struct {
    int64_t fileSize;
    uint32_t crc;
    uint16_t pathBatch;
    uint16_t pathOffset;
};
struct file_struct *files=NULL;
uint32_t nrFiles=0;

char **pathBatches=NULL;
uint16_t nrPathBatches=0;
uint16_t currentPathBatchOffset=KBatchLen-1; //so that we reallocate it first thing when we add the first path

int64_t totalBytesDeleted=0;
uint32_t totalFilesDeleted=0,totalFoldersDeleted=0;

int crcMatch(struct file_struct * const f, const uint32_t crc){
    if(!f->crc)
        f->crc=getFileCRC(GET_PATH(*f),f->fileSize);
    if(f->crc==crc)return 1;
    return 0;
}

void addFile(int64_t fileSize, uint32_t crc, char *path, int pathLen, uint32_t posInFiles){
    assert(posInFiles<=nrFiles);
    if(!(nrFiles%1024)){
        //we need to (re)alloc memory first
        files=realloc(files,(nrFiles+1024)*sizeof(struct file_struct));
        assert(files);
    }
    //move stuff
    memmove(files+posInFiles+1,files+posInFiles,(nrFiles-posInFiles)*sizeof(struct file_struct));
    nrFiles++;

    struct file_struct * restrict fs=files+posInFiles;
    fs->fileSize=fileSize;
    fs->crc=crc;

    if(currentPathBatchOffset+pathLen+1>=KBatchLen){
        pathBatches=realloc(pathBatches,++nrPathBatches*KBatchLen);
        assert(pathBatches);
        pathBatches[nrPathBatches-1]=malloc(KBatchLen);
        assert(pathBatches[nrPathBatches-1]);
        currentPathBatchOffset=0;
    }
    fs->pathBatch=nrPathBatches-1;
    fs->pathOffset=currentPathBatchOffset;
    memcpy(pathBatches[nrPathBatches-1]+currentPathBatchOffset,path,pathLen+1);
    currentPathBatchOffset+=pathLen+1;
    //printf("Added file: %s, pathlen: %d\ncurrentBatchOffset: %d\n",path,pathLen,currentPathBatchOffset);
}

char originalPath[PATH_MAX];
int originalPathLen=0;
char deletedPath[PATH_MAX];
int deletedPathLen=0;

int completelyDeleteFile(char const * const filename){
    unlink(filename);
    return 0;
}


int _unlink_cb(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf)
{
    int r=remove(fpath);
    //if(r)perror(fpath);
    return r;
}
int completelyDeleteFolder(char const * const foldername){
    return nftw(foldername,&_unlink_cb,5,FTW_DEPTH|FTW_PHYS);
}


int deleteMoveFile(char const * const filename){
    int r;
    if(!deletedPathLen){
        //we need to create the deletedPath
        strcpy(deletedPath,originalPath);
        deletedPathLen=originalPathLen;
        deletedPath[deletedPathLen++]='.';
        deletedPath[deletedPathLen++]='d';
        deletedPath[deletedPathLen++]='/';
        deletedPath[deletedPathLen]='\0';
        r=mkdir(deletedPath,0755);
        if(r && errno!=EEXIST){
            printf("Creating folder failed (%s): %s\n",deletedPath,strerror(errno));
            exit(-1);
        }
    }

    assert(!strncmp(filename,originalPath,originalPathLen));
    strcpy(deletedPath+deletedPathLen,filename+originalPathLen);

    r=rename(filename,deletedPath);
    if(r){

        //make sure all subfolders do exist and then try again
        int i,l=strlen(deletedPath);
        for(i=deletedPathLen;i<l;i++)
            if(deletedPath[i]=='/'){
                deletedPath[i]='\0';
                struct stat st;
                if(stat(deletedPath,&st)){
                    r=mkdir(deletedPath,0755);
                    if(r){
                        printf("Creating folder failed (%s): %s",deletedPath,strerror(errno));
                        exit(-1);
                    }
                }
                deletedPath[i]='/';
            }
        //do the move/rename
        r=rename(filename,deletedPath);
        if(r){
            printf("Renaming %s to %s failed: %s",filename,deletedPath,strerror(errno));
            exit(-1);
        }
    }
    deletedPath[deletedPathLen]='\0';
    return 0;
}






int processPath(char *path, int pathLen){
    struct dirent **namelist;
    int n=scandir(path,&namelist,NULL,alphasort);
    if(n<0){
        printf("scandir error for: %s. Error: %s\n",path,strerror(errno));
        return -1;
    }
    //printf("Folder: %s\n",path);

    int len,i,nrDeleted=0;
    struct stat st;

    if(path[pathLen-1]!='/'){
        path[pathLen++]='/';
        path[pathLen]='\0';
    }

    for(i=0;i<n;i++){
        struct dirent *d=namelist[i];

        //printf("Here %s\n",d->d_name);
        if(d->d_name[0]=='.'){
            if(!strcmp(d->d_name,".DS_Store") || !strcmp(d->d_name,"._.DS_Store")){
                len=strlen(d->d_name);
                memcpy(path+pathLen,d->d_name,len+1);
                printf("Completely deleting blabla file: %s\n",path);
                completelyDeleteFile(path);
            }
            free(d);
            continue;
        }

        len=strlen(d->d_name);
        memcpy(path+pathLen,d->d_name,len+1);

        if(!stat(path,&st)){
            //stat succeeded
            //find the type if unknown
            if(d->d_type==DT_UNKNOWN){
                //we must find the type
                if(S_ISREG(st.st_mode))d->d_type=DT_REG;
                else if(S_ISDIR(st.st_mode))d->d_type=DT_DIR;
                else assert(0);
            }

            if(d->d_type==DT_REG){
                //can we find a file with the same size?
                uint32_t crc=0;
                int64_t fileSize=st.st_size;

                if(!fileSize){
                    printf("Completely deleting file with zero size: %s\n",path);
                    completelyDeleteFile(path);
                    free(d);
                    continue;
                }


                //we can also delete .DS_Store and Thumbs.db
                if(!strcmp(d->d_name,"Thumbs.db")){
                    printf("Completely deleting blabla file: %s\n",path);
                    completelyDeleteFile(path);
                    free(d);
                    continue;
                }

                //binary search
                int32_t l=0,r=nrFiles-1,m,found=0,pos,i,foundIndex;

                while(l<=r){
                    m=l+((r-l)>>1);

                    //did we find it?
                    if(files[m].fileSize==fileSize){
                        //found file(s) with the same file size. Check them
                        crc=getFileCRC(path,fileSize);

                        if(crcMatch(&files[m],crc)){
                            found=1;
                            foundIndex=m;
                        } else {
                            i=1;
                            while( (m-i>=0 && files[m-i].fileSize==fileSize) || (m+i<nrFiles && files[m+i].fileSize==fileSize) ){
                                if(m-i>=0 && files[m-i].fileSize==fileSize && crcMatch(&files[m-i],crc)){
                                    found=1;
                                    foundIndex=m-i;
                                    break;
                                }
                                if(m+i<nrFiles && files[m+i].fileSize==fileSize && crcMatch(&files[m+i],crc)){
                                    found=1;
                                    foundIndex=m+i;
                                    break;
                                }
                                i++;
                            }
                        }
                        if(found){
                            if(crc){
                                printf("Deleting file: %s\n       Same as %s\n",path,GET_PATH(files[foundIndex]));
                                totalBytesDeleted+=fileSize;
                                totalFilesDeleted++;
                                deleteMoveFile(path);
                                nrDeleted++;
                            } else {
                                printf("Something is wrong, CRC is zero for %s and %s\n",path,GET_PATH(files[foundIndex]));
                                exit(-1);
                            }
                        }

                        break;
                    }

                    if(files[m].fileSize<fileSize)
                        l=m+1;
                    else r=m-1;
                }
                if(!found){
                    //not found, we need to insert this file
                    if(!files){
                        assert(!m);
                        pos=0;
                    } else if(files[m].fileSize<fileSize){
                        pos=m+1;
                        if(pos<nrFiles)
                            assert(files[pos].fileSize>fileSize);
                    } else {
                        pos=m;
                        assert(files[pos].fileSize>=fileSize);
                        if(pos>0)
                            assert(files[pos-1].fileSize<=fileSize);
                    }
                    addFile(fileSize,crc,path,pathLen+len,pos); //crc can be 0, we compute it later if needed (when we find a file with same length)
                }
            } else if(d->d_type==DT_DIR){
                //we can delete _PAlbTN folders
                if(!strcmp(d->d_name,"_PAlbTN")){
                    printf("Completely deleting blabla folder: %s\n",path);
                    completelyDeleteFolder(path);
                    free(d);
                    continue;
                }

                //go recursively
                nrDeleted+=processPath(path,pathLen+len);
            } else assert(0);
        } else {
            //stat failing can happen if the file was deleted meanwhile
            printf("stat failed for %s\n",path);
        }
        free(d);
    }//for
    free(namelist);
    path[pathLen]='\0';

    if(n-nrDeleted<=2){
        //we can delete this folder
        int r=rmdir(path);
        if(r){
            printf("Failed to remove folder: %s Error: %s",path,strerror(errno));
            return 0;
        }
        totalFoldersDeleted++;
        return 1; //successfully removed the folder
    }
    return 0;
}



int main(int argc, char **argv){
    char path[PATH_MAX];
    int pathLen;

    if(argc>2){
        printf("Usage: %s [path]\n",argv[0]);
    }
    if(argc==1)strcpy(path,".");
    else strcpy(path,argv[1]);
    pathLen=strlen(path);

    //copy to originalPath
    strcpy(originalPath,path);
    originalPathLen=pathLen;
    //make sure it ends with /
    if(originalPath[originalPathLen-1]!='/'){
        originalPath[originalPathLen++]='/';
        originalPath[originalPathLen]='\0';
    }

    processPath(path,pathLen);

    printf("Deleted %u files and %u folders, total: %"PRId64" bytes.\nTotal non-duplicate files: %u, pathBatches used: %u\n",
           totalFilesDeleted,totalFoldersDeleted,totalBytesDeleted,nrFiles,nrPathBatches);

    int i;
    for(i=1;i<nrFiles;i++)
        assert(files[i-1].fileSize<=files[i].fileSize);
    //before deleting, check that all file lengths are in order

    //free files & paths
    free(files);files=NULL;nrFiles=0;
    for(i=0;i<nrPathBatches;i++)
        free(pathBatches[i]);
    free(pathBatches);

    return 0;
}
