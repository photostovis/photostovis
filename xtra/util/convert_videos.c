#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <time.h>
#include <assert.h>
#include <sys/stat.h> //mkdir
#include <signal.h> //defines SIGINT
#include <libxml/xmlreader.h>

#define HASH_FILENAME ".HashTable.xml"

char *currentFilename=NULL;
char *currentKey=NULL;
int inFilenameKeyElm=0;
int inDataKeyElm=0;
char *currentDateCreated=NULL;
char *currentDateTaken=NULL;

int moveDeleteMeFiles=0;
int createLowBitrateVersion=1;
int recursive=0;

int transcodingFailed=0,filesNotFound=0,transcodedOk=0;


void _mkdir(const char *dir) {
    char tmp[PATH_MAX];
    char *p = NULL;
    size_t len;

    snprintf(tmp, sizeof(tmp),"%s",dir);
    len = strlen(tmp);
    if(tmp[len - 1] == '/')
        tmp[len - 1] = 0;
    for(p = tmp + 1; *p; p++)
        if(*p == '/') {
            *p = 0;
            mkdir(tmp, S_IRWXU);
            *p = '/';
        }
    mkdir(tmp, S_IRWXU);
}

int moveToDeleteme(const char *filename){
    char newFilename[100];

    if(!moveDeleteMeFiles){
        snprintf(newFilename,100,"delete_me_%s",filename);
        return rename(filename,newFilename);
    }

    //if here, we move it to a different place
    char path[PATH_MAX];
    getcwd(path,PATH_MAX);
    char *picturesStart=strstr(path,"/pictures/");
    assert(picturesStart);
    memcpy(picturesStart,"/deleteme/",10); //without the \0
    _mkdir(path);
    int l=strlen(path);
    snprintf(path+l,PATH_MAX-l,"/delete_me_%s",filename);
    return rename(filename,path);
}

int transcodeMovie(const char *filename, const char *dateString, int fileMustBeFound){
    int r;
    //can we open filename?
    FILE *f=fopen(filename,"r");
    if(f)fclose(f);
    else {
        printf("The file %s was not found!\n",filename);
        assert(!fileMustBeFound);
        return -1;
    }

    int filenameLen=strlen(filename);
    struct tm t;
    const size_t KNameSize=100;
    const size_t KCommandSize=1000;
    char *timezoneString;
    int weHaveDate=0;
    char newFilename[KNameSize];
    char transcodeCmd[KCommandSize];
    const char *KCmdTranscodeHigh="ffmpeg -loglevel error -i \"%s\" -y -map 0:0 -map 0:1 -map 0:1    -c:v copy -c:a:0 aac -b:a:0 128k -c:a:1 copy  -movflags +faststart \"%s_h.mp4\"";
    const char *KCmdTranscodeLow ="ffmpeg -loglevel error -i \"%s\" -tune zerolatency -x264opts bitrate=1500:vbv-maxrate=1500:vbv-bufsize=3000:nal-hrd=vbr -codec:v libx264 -profile:v high -level 3.1 -movflags +faststart -pix_fmt yuv420p  -s 1280x720 -r 25 -ac 2 -ar 48000 -codec:a aac -ab 64k -strict experimental -y \"%s_l.mp4\"";

    //parse the dateString
    if(dateString){
        timezoneString=strptime(dateString,"%Y:%m:%d %H:%M:%S",&t);
        if(!timezoneString)
            timezoneString=strptime(dateString,"%Y-%m-%dT%H:%M:%S",&t);
        if(!timezoneString){
            printf("Time conversion failed!!!\n");
            return -2;
        }
        //TODO: parse the timezone string
        weHaveDate=1;
    } else {
        //can we find a date in the filename?
        timezoneString=strptime(filename,"%Y%m%d%H%M%S",&t);
        if(!timezoneString)
            timezoneString=strptime(filename,"%Y%m%d_%H%M%S",&t);
        if(timezoneString)
            weHaveDate=1;
    }


    //create the new filename
    if(weHaveDate)
        strftime(newFilename,KNameSize,"%Y%m%d_%H%M%S",&t);
    else {
        //change the extension to mp4. Actually, remove the extension completely

        assert(filenameLen>4);
        assert(filename[filenameLen-4]=='.');
        assert(filename[filenameLen-3]=='M');
        assert(filename[filenameLen-2]=='T');
        assert(filename[filenameLen-1]=='S');
        strcpy(newFilename,filename);
        newFilename[filenameLen-4]='\0';
    }
    printf("New name: %s_x.mp4\n",newFilename);

    //transcode high
    snprintf(transcodeCmd,KCommandSize,KCmdTranscodeHigh,filename,newFilename);
    r=system(transcodeCmd);
    if(r){
        printf("Transcode high command (%s) returned an error: %d\n",transcodeCmd,r);
        if(WIFSIGNALED(r) && WTERMSIG(r)==SIGINT){
            printf("SIGINT, exiting.\n");
            exit(0);
        }
        return -3;
    }

    //transcode low
    if(createLowBitrateVersion){
        snprintf(transcodeCmd,KCommandSize,KCmdTranscodeLow,filename,newFilename);
        r=system(transcodeCmd);
        if(r){
            printf("Transcode low command (%s) returned an error: %d\n",transcodeCmd,r);
            if(WIFSIGNALED(r) && WTERMSIG(r)==SIGINT){
                printf("SIGINT, exiting.\n");
                exit(0);
            }
            return -4;
        }
    }

    //rename the source file
    moveToDeleteme(filename);

    //if this a .m2ts file and we have .cont, .iis and .tmp, rename them as well
    if(filenameLen>5){
        const char *extension=filename+filenameLen-5;
        if(!strcasecmp(extension,".m2ts")){
            strcpy(newFilename,filename);

            //create the .cont filename
            strcpy(newFilename+filenameLen-4,"cont");
            moveToDeleteme(newFilename);

            //create the .iis filename
            strcpy(newFilename+filenameLen-4,"iis");
            moveToDeleteme(newFilename);

            //create the .tmb filename
            strcpy(newFilename+filenameLen-4,"tmb");
            moveToDeleteme(newFilename);
        }
    }
    return 0;
}

void processNode(xmlTextReaderPtr reader){
    const xmlChar *name, *value;
    int depth,nodeType,isEmpty,hasValue;

    name=xmlTextReaderConstName(reader);
    if(!name)return;

    value=xmlTextReaderConstValue(reader);
    depth=xmlTextReaderDepth(reader);
    nodeType=xmlTextReaderNodeType(reader);

    if(depth==2 && !strcmp((char*)name,"key")){
        if(nodeType==XML_READER_TYPE_ELEMENT)
            inFilenameKeyElm=1;
        else if(nodeType==XML_READER_TYPE_END_ELEMENT)
            inFilenameKeyElm=0;
        return;
    }

    if(depth==3 && inFilenameKeyElm && !strcmp((char*)name,"#text")){
        isEmpty=xmlTextReaderIsEmptyElement(reader);
        hasValue=xmlTextReaderHasValue(reader);

        if(!isEmpty && hasValue && value){
            //this is a media file name. Lets check if it is an .MTS file
            int l=strlen((char*)value);
            if(l>4 && !strcmp((char*)value+l-4,".MTS")){
                //yes it is
                assert(!currentFilename);
                assert(!currentDateTaken);
                assert(!currentDateCreated);
                currentFilename=strdup((char*)value);
                printf("File: <<%s>>\n",currentFilename);
            }
        }
    }

    if(depth==2 && nodeType==XML_READER_TYPE_END_ELEMENT && !strcmp((char*)name,"dict")){
        //end of a dict element which is associated with a media file
        if(currentFilename){
            int r=0;

            if(currentDateTaken)
                r=transcodeMovie(currentFilename,currentDateTaken,0);
            else if(currentDateCreated)
                r=transcodeMovie(currentFilename,currentDateCreated,0);
            else {
                printf("ERROR: we have no date for this movie (%s). We could transcode it with the same name, nut this is not yet implemented.\n",currentFilename);
                assert(0);
            }

            if(r){
                if(r==-1)filesNotFound++;
                else transcodingFailed++;
            } else transcodedOk++;
            printf("\n\n\n\n");

            if(currentDateCreated){
                free(currentDateCreated);
                currentDateCreated=NULL;
            }
            if(currentDateTaken){
                free(currentDateTaken);
                currentDateTaken=NULL;
            }
            //free current filename
            free(currentFilename);
            currentFilename=NULL;
        }

    }


    //find a date associated with the current filename
    if(currentFilename){
        if(depth==3 && !strcmp((char*)name,"key")){
            if(nodeType==XML_READER_TYPE_ELEMENT)
                inDataKeyElm=1;
            else if(nodeType==XML_READER_TYPE_END_ELEMENT)
                inDataKeyElm=0;
        }

        if(depth==4 && inDataKeyElm && !strcmp((char*)name,"#text")){
            isEmpty=xmlTextReaderIsEmptyElement(reader);
            hasValue=xmlTextReaderHasValue(reader);

            if(!isEmpty && hasValue && value){
                if(currentKey)free(currentKey);
                currentKey=strdup((char*)value);
            }
        }

        //printf("here1: depth=%d, inDataKeyElm=%d, name==%s, currentKey=%s\n",depth,inDataKeyElm,name,currentKey);

        //we could parse the type, but we do not really need it
        if(depth==4 && !inDataKeyElm && !strcmp((char*)name,"#text") && currentKey && !strcmp(currentKey,"DateTaken")){
            //this is the date string
            //lets make sure it is not empty
            isEmpty=xmlTextReaderIsEmptyElement(reader);
            hasValue=xmlTextReaderHasValue(reader);

            if(!isEmpty && hasValue && value){
                printf("%s was taken at %s\n",currentFilename,value);
                if(currentDateTaken)free(currentDateTaken);
                currentDateTaken=strdup((char*)value);
            }
        }

        if(depth==4 && !inDataKeyElm && !strcmp((char*)name,"#text") && currentKey && !strcmp(currentKey,"DateCreated")){
            //this is the date string
            //lets make sure it is not empty
            isEmpty=xmlTextReaderIsEmptyElement(reader);
            hasValue=xmlTextReaderHasValue(reader);

            if(!isEmpty && hasValue && value){
                printf("%s was created at %s\n",currentFilename,value);
                if(currentDateCreated)free(currentDateTaken);
                currentDateCreated=strdup((char*)value);
            }
        }
    }
}


int ParseAndCovertHashFile(char *path){
    LIBXML_TEST_VERSION;
    xmlTextReaderPtr reader;
    int r=0;

    reader=xmlReaderForFile(HASH_FILENAME,NULL,0);
    if(!reader){
        //printf("Unable to open "HASH_FILENAME"\n");
        return -1;
    }

    printf("Hash XML file found in %s\n",path);

    while(1==(r=xmlTextReaderRead(reader))){
        processNode(reader);
    }
    xmlFreeTextReader(reader);
    if(r!=0) {
        printf("Failed to parse "HASH_FILENAME"\n");
        return -1;
    }
    xmlCleanupParser();
    assert(!currentFilename);
    if(currentKey)free(currentKey);
    currentKey=NULL;

    return 0;
}

int isM2tFilter(const struct dirent *d){
    assert(d->d_type==DT_DIR || d->d_type==DT_REG);

    if(d->d_name[0]=='.' || d->d_name[0]=='@')
        return 0;
    if(d->d_type==DT_DIR)return 1;

    int l=strlen(d->d_name);
    //check for .MTS
    if(l>4){
        char *extension=(char*)d->d_name+l-4;
        if(!strcasecmp(extension,".MTS") && strncmp(d->d_name,"delete_me_",10))
            return 1;
    }
    //check for .m2ts
    if(l>5){
        char *extension=(char*)d->d_name+l-5;
        if(!strcasecmp(extension,".m2ts") && strncmp(d->d_name,"delete_me_",10))
            return 1;
    }
    return 0;
}

int isMp4Filter(const struct dirent *d){
    assert(d->d_type==DT_DIR || d->d_type==DT_REG);

    if(d->d_name[0]=='.' || d->d_name[0]=='@')
        return 0;
    if(d->d_type==DT_DIR)return 1;

    int l=strlen(d->d_name);
    if(l>4){
        char *extension=(char*)d->d_name+l-4;
        if(!strcasecmp(extension,".mp4") && strncmp(d->d_name,"delete_me_",10))
            return 1;
    }
    return 0;
}

int processFolder(char *path, const int pathLen){
    struct dirent **namelist=NULL;
    int n,i,l;

    chdir(path);

    ParseAndCovertHashFile(path);

    assert(path[pathLen-1]!='/');

    //do we have more files?
    n=scandir(path,&namelist,isM2tFilter,NULL);
    path[pathLen]='/';

    for(i=0;i<n;i++){
        if(namelist[i]->d_type==DT_DIR)continue;

        l=strlen(namelist[i]->d_name);
        memcpy(path+pathLen+1,namelist[i]->d_name,l+1);

        printf("File: %s \n",path);
        transcodeMovie(namelist[i]->d_name,NULL,1);
    }


    for(i=0;i<n;i++){
        if(recursive && namelist[i]->d_type==DT_DIR){
            l=strlen(namelist[i]->d_name);
            memcpy(path+pathLen+1,namelist[i]->d_name,l+1);
            processFolder(path,pathLen+1+l);
        }
        free(namelist[i]);
    }
    free(namelist);

    return 0;
}


int listFolder(char *path, const int pathLen){
    struct dirent **namelist=NULL;
    int n,i,l;

    chdir(path);

    assert(path[pathLen-1]!='/');

    //do we have more files?
    n=scandir(path,&namelist,isMp4Filter,NULL);
    path[pathLen]='/';

    for(i=0;i<n;i++){
        if(namelist[i]->d_type==DT_DIR)continue;

        l=strlen(namelist[i]->d_name);
        memcpy(path+pathLen+1,namelist[i]->d_name,l+1);

        printf("File: %s \n",path);
    }


    for(i=0;i<n;i++){
        if(namelist[i]->d_type==DT_DIR){
            l=strlen(namelist[i]->d_name);
            memcpy(path+pathLen+1,namelist[i]->d_name,l+1);
            listFolder(path,pathLen+1+l);
        }
        free(namelist[i]);
    }
    free(namelist);

    return 0;
}


int main(int argc, char **argv){
    char path[PATH_MAX];

    if(argc!=1 && argc!=2){
        printf("Usage: %s [-r] | [-l] | [-h]\n",argv[0]);
        return 0;
    }
    if(argc==2 && !strcmp(argv[1],"-r")){
        printf("Recursive: on\n");
        recursive=1;
    } if(argc==2 && !strcmp(argv[1],"-h")){
        printf("Only high\n");
        recursive=1;
        createLowBitrateVersion=0;
    }else printf("Recursive: off\n");

    getcwd(path,PATH_MAX);

    if(argc==2 && !strcmp(argv[1],"-l")){
        //listing all .mp4 files
        listFolder(path,strlen(path));
        return 0;
    }

    //does our folder end in /pictures and do we have a /deleteme ?
    char *picturesStart=strstr(path,"/pictures");
    int pathLen=strlen(path);
    if(picturesStart && strlen(picturesStart)==9){
        char deletemePath[PATH_MAX];
        memcpy(deletemePath,path,pathLen-8);
        memcpy(deletemePath+pathLen-8,"deleteme",8);

        //does this deletemePath exist?
        if(access(deletemePath,R_OK|W_OK|X_OK)){
            printf("deleteme component exists, delete_me_* files will be moved there!\n");
            moveDeleteMeFiles=1;
        }
    }

    processFolder(path,strlen(path));


    //summary
    if(transcodingFailed)printf("!!!! Transcoding failed for %d movie(s)!\n",transcodingFailed);
    if(filesNotFound)printf("!!!! %d movie file(s) not found!\n",filesNotFound);
    if(transcodedOk)printf("%d movie file(s) transcoded OK.\n",transcodedOk);

    return 0;
}

