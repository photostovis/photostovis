#include <stdio.h>
#include <dirent.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <sys/stat.h>

/* This is the basic CRC-32 calculation with some optimization but no
table lookup. The the byte reversal is avoided by shifting the crc reg
right instead of left and by using a reversed 32-bit word to represent
the polynomial.
   When compiled to Cyclops with GCC, this function executes in 8 + 72n
instructions, where n is the number of bytes in the input message. It
should be doable in 4 + 61n instructions.
   If the inner loop is strung out (approx. 5*8 = 40 instructions),
it would take about 6 + 46n instructions. */

uint32_t ph_crc32b(char const * const message, long long int l) {
   int i, j;
   uint32_t byte, crc, mask;

   i = 0;
   crc = 0xFFFFFFFF;
   for(i=0;i<l;i++){
      byte = message[i];            // Get next byte.
      crc = crc ^ byte;
      for (j = 7; j >= 0; j--) {    // Do eight times.
         mask = -(crc & 1);
         crc = (crc >> 1) ^ (0xEDB88320 & mask);
      }
   }
   return ~crc;
}

uint32_t getFileCRC(char const * const filename, int64_t fileSize){
    const int64_t KMaxCompare=10485760; //10 MB
    if(fileSize>KMaxCompare)
        fileSize=KMaxCompare;

    //printf("Computing CRC for %s [%lld]\n",filename,fileSize);

    char *buf=malloc(fileSize);
    if(!buf){
        printf("Could not allocate buffer: %lld bytes. Exiting.\n",fileSize);
        exit(-1);
    }
    FILE *f=fopen(filename,"r");
    int n=fread(buf,fileSize,1,f);
    if(n!=1){
        printf("Could not read file: %s. Returning 0.\n",filename);
        return 0;
    }
    fclose(f);
    uint32_t crc=ph_crc32b(buf,fileSize);
    free(buf);
    return crc;
}

struct file_struct {
    int64_t fileSize;
    uint32_t crc;
    char *path;
    struct file_struct *next;
};
struct file_struct *files=NULL;
struct file_struct *lastFile=NULL;
int64_t totalBytesToDelete=0,totalFilesToDelete=0,totalFoldersToDelete=0;

int addFile(int64_t fileSize, uint32_t crc, char *path){
    struct file_struct *fs=malloc(sizeof(struct file_struct));
    fs->fileSize=fileSize;
    fs->crc=crc;
    fs->path=strdup(path);
    fs->next=NULL;

    if(!files){
        files=fs;
        lastFile=fs;
    } else {
        assert(lastFile);
        assert(!lastFile->next);
        lastFile->next=fs;
        lastFile=fs;
    }
    return 0;
}

char originalPath[PATH_MAX];
int originalPathLen=0;
char deletedPath[PATH_MAX];
int deletedPathLen=0;


int deleteMoveFile(char *filename){
    int r;
    if(!deletedPathLen){
        //we need to create the deletedPath
        strcpy(deletedPath,originalPath);
        deletedPathLen=originalPathLen;
        deletedPath[deletedPathLen++]='.';
        deletedPath[deletedPathLen++]='d';
        deletedPath[deletedPathLen++]='/';
        deletedPath[deletedPathLen]='\0';
        r=mkdir(deletedPath,0755);
        if(r && errno!=EEXIST){
            printf("Creating folder failed (%s): %s\n",deletedPath,strerror(errno));
            exit(-1);
        }
    }

    assert(!strncmp(filename,originalPath,originalPathLen));
    strcpy(deletedPath+deletedPathLen,filename+originalPathLen);

    r=rename(filename,deletedPath);
    if(r){

        //make sure all subfolders do exist and then try again
        int i,l=strlen(deletedPath);
        for(i=deletedPathLen;i<l;i++)
            if(deletedPath[i]=='/'){
                deletedPath[i]='\0';
                struct stat st;
                if(stat(deletedPath,&st)){
                    r=mkdir(deletedPath,0755);
                    if(r){
                        printf("Creating folder failed (%s): %s",deletedPath,strerror(errno));
                        exit(-1);
                    }
                }
                deletedPath[i]='/';
            }
        //do the move/rename
        r=rename(filename,deletedPath);
        if(r){
            printf("Renaming %s to %s failed: %s",filename,deletedPath,strerror(errno));
            exit(-1);
        }
    }
    deletedPath[deletedPathLen]='\0';
    return 0;
}






int processPath(char *path, int pathLen){
    struct dirent **namelist;
    int n=scandir(path,&namelist,NULL,alphasort);
    if(n<0){
        printf("scandir error for: %s. Error: %s\n",path,strerror(errno));
        return -1;
    }
    //printf("Folder: %s\n",path);

    int l,i,nrDeleted=0;
    struct stat st;

    if(path[pathLen-1]!='/'){
        path[pathLen++]='/';
        path[pathLen]='\0';
    }

    for(i=0;i<n;i++){
        struct dirent *d=namelist[i];
        l=strlen(d->d_name);
        memcpy(path+pathLen,d->d_name,l+1);

        if(d->d_name[0]=='.'){
            free(d);
            continue;
        }


        if(!stat(path,&st)){
            //stat succeeded
            //find the type if unknown
            if(d->d_type==DT_UNKNOWN){
                //we must find the type
                if(S_ISREG(st.st_mode))d->d_type=DT_REG;
                else if(S_ISDIR(st.st_mode))d->d_type=DT_DIR;
                else assert(0);
            }

            if(d->d_type==DT_REG){
                //can we find a file with the same size?
                uint32_t crc=0;
                int64_t fileSize=st.st_size;

                if(!fileSize){
                    printf("Deleting file with zero size: %s\n",path);
                    deleteMoveFile(path);
                    free(d);
                    continue;
                }


                struct file_struct *current=files;
                while(current){
                    if(current->fileSize==fileSize){
                        //we need to check the CRC
                        if(!current->crc)
                            current->crc=getFileCRC(current->path,current->fileSize);
                        crc=getFileCRC(path,fileSize);

                        if(crc==current->crc){
                            //the file is the same
                            if(crc){
                                printf("Deleting file: %s\n       Same as %s\n",path,current->path);
                                totalBytesToDelete+=fileSize;
                                totalFilesToDelete++;
                                deleteMoveFile(path);
                                nrDeleted++;
                            } else {
                                printf("Something is wrong, CRC is zero for %s and %s\n",path,current->path);
                                exit(-1);
                            }
                            break; //we found a file that is the same
                        }
                    }
                    //if here, we go to next try

                    current=current->next;
                }

                if(!current)
                    addFile(fileSize,crc,path); //crc can be 0, we compute it later if needed (when we find a file with same length)
            } else if(d->d_type==DT_DIR){
                //go recursively
                nrDeleted+=processPath(path,pathLen+l);
            } else assert(0);
        } else {
            //stat failing can happen if the file was deleted meanwhile
            printf("stat failed for %s\n",path);
        }
        free(d);
    }//for
    free(namelist);
    path[pathLen]='\0';

    if(n-nrDeleted<=2){
        //we can delete this folder
        int r=rmdir(path);
        if(r){
            printf("Failed to remove folder: %s Error: %s",path,strerror(errno));
            return 0;
        }
        totalFoldersToDelete++;
        return 1; //successfully removed the folder
    }
    return 0;
}



int main(int argc, char **argv){
    char path[PATH_MAX];
    int pathLen;

    if(argc>2){
        printf("Usage: %s [path]\n",argv[0]);
    }
    if(argc==1)strcpy(path,".");
    else strcpy(path,argv[1]);
    pathLen=strlen(path);

    //copy to originalPath
    strcpy(originalPath,path);
    originalPathLen=pathLen;
    //make sure it ends with /
    if(originalPath[originalPathLen-1]!='/'){
        originalPath[originalPathLen++]='/';
        originalPath[originalPathLen]='\0';
    }

    processPath(path,pathLen);

    //free files

    while(files){
        struct file_struct *next=files->next;
        free(files->path);
        free(files);
        files=next;
    }

    printf("Deleted %lld files and %lld folders, total: %lld bytes.\n",totalFilesToDelete,totalFoldersToDelete,totalBytesToDelete);

    return 0;
}




