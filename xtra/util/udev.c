#define _XOPEN_SOURCE 500
#include <libudev.h>
#include <stdio.h>
#include <poll.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/mount.h>

struct ph_partition {
    char name[5];
    char type[10];
    char uuid[36+1];
    char *mountPoint;
};

struct ph_device {
    char *id;
    char *device;
    int nrPartitions;
    struct ph_partition *partitions;
};
struct ph_device *phDevices=NULL;
int nrPhDevices=0;

int shouldExit=0;

void sigIntFunction(int s){
    printf("SigInt.\n");

    //unmount everything
    int nrUnmounted=0;
    int i,j;
    //is this device already "added"?
    for(i=0;i<nrPhDevices;i++)
        for(j=0;j<phDevices[i].nrPartitions;j++){
            struct ph_partition *p=&(phDevices[i].partitions[j]);

            //unmount this partition
            if(p->mountPoint){
                char partition[100];
                snprintf(partition,100,"%s%s",phDevices[i].device,p->name);
                printf("Unmounting \"%s\"\n",partition);
                int r=umount2(p->mountPoint,MNT_FORCE);
                if(r)printf("umount-ing %s error: %s\n",partition,strerror(errno));
                else {
                    printf("Unmounting \"%s\" was successful!\n",partition);
                    //free partition
                    free(p->mountPoint);
                    p->mountPoint=NULL;
                    nrUnmounted++;
                }
            }
        }
    if(!nrUnmounted)
        shouldExit=1;
}


char *copy_and_decode(const char *s, int l){
    char *r=malloc(l+1);
    int i,pos=0;
    for(i=0;i<l;i++){
        if(s[i]!='\\'){
            r[pos++]=s[i];
            continue;
        }

        if(i+3<l){
            if(s[i+1]=='0' && s[i+2]=='4' && s[i+3]=='0'){
                i+=3;
                r[pos++]=' ';
            }
        }
    }
    r[pos]='\0';
    return r;
}


void mount_partitions(struct ph_device *device, int waitXs){
    const int KLineSize=1000;
    char line[KLineSize];
    FILE *f=popen("blkid","r");
    const int len=strlen(device->device);

    assert(!device->nrPartitions);
    //printf("mount_partitions: %s (%d)\n",device,len);
    while(fgets(line,KLineSize,f)){
        //printf("Line: %s",line);
        if(strncmp(line,device->device,len))continue;

        //parse this line. We are interested in partition string, UUID string and fs type
        //printf("Parsing line: %s",line);
        device->partitions=realloc(device->partitions,(++device->nrPartitions)*sizeof(struct ph_partition));
        struct ph_partition *p=&device->partitions[device->nrPartitions-1];

        //partition name (end)
        char *start=line;
        char *end=strchr(start,':');
        assert(end);
        int l=end-start-len;
        assert(l<5);
        memcpy(p->name,start+len,l);
        p->name[l]='\0';

        //uuid
        start=strstr(line,"UUID=\"");
        assert(start);
        start+=6;
        end=strchr(start,'"');
        l=end-start;
        assert(l<=36);
        memcpy(p->uuid,start,l);
        p->uuid[l]='\0';

        //fs type
        start=strstr(line,"TYPE=\"");
        assert(start);
        start+=6;
        end=strchr(start,'"');
        l=end-start;
        assert(l<10);
        memcpy(p->type,start,l);
        p->type[l]='\0';

        //not mounted yet
        p->mountPoint=NULL;
    }
    pclose(f);


    if(!device->nrPartitions){
        printf("WARNING: blkid found ZERO partitions for this device. Are you running this as root?");
        return;
    }

    //if here, we have found partitions
    if(waitXs){
        printf("%d partitions found. Waiting 5 seconds, perhaps someone will mount them for us.\n",device->nrPartitions);
        sleep(5);
    }

    int i,l;
    f=fopen("/etc/mtab","r");
    while(fgets(line,KLineSize,f)){

        //is this one of our partitions?
        if(strncmp(device->device,line,len))continue; //no, it is not

        //yes, it is. Which partition?
        for(i=0;i<device->nrPartitions;i++){
            if(!strncmp(line+len,device->partitions[i].name,strlen(device->partitions[i].name))){
                //this partition is mounted. Find out where
                char *start=strchr(line+len,'/');
                assert(start);
                char *end=strchr(start,' ');
                assert(end);
                l=end-start;

                struct ph_partition *p=&device->partitions[i];
                assert(!p->mountPoint);

                p->mountPoint=copy_and_decode(start,l);

                printf("Partition %s%s already mounted in \"%s\"\n",device->device,p->name,p->mountPoint);
            }
        }

    }
    fclose(f);

    //for each partition that was not mounted: mount it
    int nrFailedMounts=0;
    for(i=0;i<device->nrPartitions;i++){
        if(device->partitions[i].mountPoint)continue;

        const char *KMountRootFolder="/data/photostovis/ext";
        mkdir(KMountRootFolder,0755);
        sprintf(line,"%s/%s",KMountRootFolder,device->partitions[i].uuid);
        mkdir(line,0755);
        char partition[100];
        snprintf(partition,100,"%s%s",device->device,device->partitions[i].name);
        int r=mount(partition,line,device->partitions[i].type,MS_RDONLY,NULL);
        if(r){
            nrFailedMounts++;
            printf("Mounting %s to %s failed: %s\n",partition,line,strerror(errno));
        } else {
            device->partitions[i].mountPoint=strdup(line);
            printf("Partition %s mounted successfully in %s\n",partition,line);
        }
    }
    printf("Done mounting partitions (success: %d, failed: %d)\n",device->nrPartitions-nrFailedMounts,nrFailedMounts);
}

void device_added(const char *id, const char *device, int atRuntime){
    int i;
    //is this device already "added"?
    for(i=0;i<nrPhDevices;i++)
        if(!strcmp(id,phDevices[i].id)){
            printf("WARNING: device %s already addded as %s\n",phDevices[i].id,phDevices[i].device);
            return;
        }
    //add the device
    i=nrPhDevices;
    phDevices=realloc(phDevices,++nrPhDevices*sizeof(struct ph_device));
    phDevices[i].id=strdup(id);
    phDevices[i].device=strdup(device);
    phDevices[i].nrPartitions=0;
    phDevices[i].partitions=NULL;
    printf("Device %s added (ID: %s)\n",device,id);
    mount_partitions(&phDevices[i],atRuntime);
}

void device_removed(const char *id){
    int i,j;
    //is this device already "added"?
    for(i=0;i<nrPhDevices;i++)
        if(!strcmp(id,phDevices[i].id)){
            printf("Device %s rmvd  (ID: %s) (mounted partitions: %d)\n",phDevices[i].device,phDevices[i].id,phDevices[i].nrPartitions);
            //free stuff
            for(j=0;j<phDevices[i].nrPartitions;j++){
                struct ph_partition *p=&(phDevices[i].partitions[j]);

                //unmount
                /* unmounting seems pretty useless at this point
                if(p->mountPoint){
                    char partition[100];
                    snprintf(partition,100,"%s%s",phDevices[i].device,p->name);
                    printf("Unmounting \"%s\"\n",partition);
                    int r=umount2(partition,MNT_FORCE);
                    if(r)printf("umount-ing %s error: %s\n",partition,strerror(errno));
                    else printf("Unmounting \"%s\" was successful!\n",partition);
                }*/

                //free partition
                free(p->mountPoint);
            }
            free(phDevices[i].partitions);
            free(phDevices[i].device);
            free(phDevices[i].id);
            //remove the device from the structure
            memmove(phDevices+i,phDevices+i+1,(nrPhDevices-i-1)*sizeof(struct ph_device));
            phDevices=realloc(phDevices,(--nrPhDevices)*sizeof(struct ph_device));

            return;
        }

    printf("WARNING: device %s not found!\n",id);
}


static struct udev_device* get_child(struct udev* udev, struct udev_device* parent, const char* subsystem) {
  struct udev_device* child = NULL;
  struct udev_enumerate *enumerate = udev_enumerate_new(udev);

  udev_enumerate_add_match_parent(enumerate, parent);
  udev_enumerate_add_match_subsystem(enumerate, subsystem);
  udev_enumerate_scan_devices(enumerate);

  struct udev_list_entry *devices = udev_enumerate_get_list_entry(enumerate);
  struct udev_list_entry *entry;

  udev_list_entry_foreach(entry, devices) {
      const char *path = udev_list_entry_get_name(entry);
      child = udev_device_new_from_syspath(udev, path);
      if(child)break;
  }

  udev_enumerate_unref(enumerate);
  return child;
}

static void enumerate_usb_mass_storage(struct udev* udev) {
  struct udev_enumerate* enumerate = udev_enumerate_new(udev);

  udev_enumerate_add_match_subsystem(enumerate, "scsi");
  udev_enumerate_add_match_property(enumerate, "DEVTYPE", "scsi_device");
  udev_enumerate_scan_devices(enumerate);

  struct udev_list_entry *devices = udev_enumerate_get_list_entry(enumerate);
  struct udev_list_entry *entry;

  udev_list_entry_foreach(entry, devices) {
      const char* path = udev_list_entry_get_name(entry);
      struct udev_device* scsi = udev_device_new_from_syspath(udev, path);

      struct udev_device* block = get_child(udev, scsi, "block");
      struct udev_device* scsi_disk = get_child(udev, scsi, "scsi_disk");

      struct udev_device* usb = udev_device_get_parent_with_subsystem_devtype(scsi, "usb", "usb_device");

      if (block && scsi_disk && usb) {
          /*
          printf("block = %s, usb = %s:%s, scsi = %s\n",
                 udev_device_get_devnode(block),
                 udev_device_get_sysattr_value(usb, "idVendor"),
                 udev_device_get_sysattr_value(usb, "idProduct"),
                 udev_device_get_sysattr_value(scsi, "vendor"));*/
          device_added(udev_device_get_sysname(scsi),udev_device_get_devnode(block),0);
      }

      if (block) {
          udev_device_unref(block);
      }

      if (scsi_disk) {
          udev_device_unref(scsi_disk);
      }

      udev_device_unref(scsi);
  }

  udev_enumerate_unref(enumerate);
}

int main() {
  struct udev* udev = udev_new();
  struct udev_monitor *mon;
  int fd;

  struct sigaction sigHandler;
  sigHandler.sa_handler=sigIntFunction;
  sigemptyset(&sigHandler.sa_mask);
  sigHandler.sa_flags=0;
  sigaction(SIGINT,&sigHandler,NULL);


  /* Set up a monitor to monitor scsi devices */
  mon = udev_monitor_new_from_netlink(udev, "udev");
  udev_monitor_filter_add_match_subsystem_devtype(mon, "scsi", "scsi_device");
  udev_monitor_enable_receiving(mon);
      /* Get the file descriptor (fd) for the monitor.
         This fd will get passed to select() */
  fd = udev_monitor_get_fd(mon);


  enumerate_usb_mass_storage(udev);

  struct pollfd pfd[1];
  pfd[0].fd=fd;
  pfd[0].events=POLLIN;

  int r;

  while (1) {
      r=poll(pfd,1,60000);


      if(r>0){
          assert(pfd[0].revents);

          if(pfd[0].revents&POLLIN){
              struct udev_device *scsi = udev_monitor_receive_device(mon);

              if (scsi) {

                  if(!strcmp(udev_device_get_action(scsi),"add")){
                      struct udev_device* scsi_disk = get_child(udev, scsi, "scsi_disk");
                      struct udev_device* usb = udev_device_get_parent_with_subsystem_devtype(scsi, "usb", "usb_device");
                      struct udev_device* block = get_child(udev, scsi, "block");
                      if(!block){
                          sleep(1);//otherwise the "block" device returned is sometimes NULL
                          block = get_child(udev, scsi, "block");
                      }

                      if (block && scsi_disk && usb) {
                          /*
                          printf("block = %s, usb = %s:%s, scsi = %s\n",
                                 udev_device_get_devnode(block),
                                 udev_device_get_sysattr_value(usb, "idVendor"),
                                 udev_device_get_sysattr_value(usb, "idProduct"),
                                 udev_device_get_sysattr_value(scsi, "vendor"));*/

                          assert(!strcmp(udev_device_get_action(scsi),"add"));
                          device_added(udev_device_get_sysname(scsi),udev_device_get_devnode(block),1);

                      } else {
                          if(!block)printf("Added device has no \"block\" child\n");
                          if(!scsi_disk)printf("Added device has no \"scsi_disk\" child\n");
                          if(!usb)printf("Added device has no \"usb\" parent\n");
                          /*else
                              printf("usb = %s:%s,\n",udev_device_get_sysattr_value(usb, "idVendor"),udev_device_get_sysattr_value(usb, "idProduct"));
                              */

                          printf("   Sysname: %s\n",udev_device_get_sysname(scsi));
                          printf("   Devpath: %s\n",udev_device_get_devpath(scsi));
                      }

                      if (block) {
                          udev_device_unref(block);
                      }

                      if (scsi_disk) {
                          udev_device_unref(scsi_disk);
                      }
                  } else if(!strcmp(udev_device_get_action(scsi),"remove")){
                      device_removed(udev_device_get_sysname(scsi));
                  } else {
                      printf("   Node: %s\n", udev_device_get_devnode(scsi));
                      printf("   Subsystem: %s\n", udev_device_get_subsystem(scsi));
                      printf("   Devtype: %s\n", udev_device_get_devtype(scsi));
                      printf("   Action: %s\n", udev_device_get_action(scsi));

                      printf("   Sysname: %s\n",udev_device_get_sysname(scsi));
                      printf("   Devpath: %s\n",udev_device_get_devpath(scsi));
                  }

                  /*

                  printf("   Node: %s\n", udev_device_get_devnode(scsi));
                  printf("   Subsystem: %s\n", udev_device_get_subsystem(scsi));
                  printf("   Devtype: %s\n", udev_device_get_devtype(scsi));
                  printf("   Action: %s\n", udev_device_get_action(scsi));

                  printf("   Sysname: %s\n",udev_device_get_sysname(scsi));
                  printf("   Devpath: %s\n",udev_device_get_devpath(scsi));
                  */

                  udev_device_unref(scsi);
              }
              else {
                  printf("No Device from receive_device(). An error occured.\n");
              }
          }

      }
      if(shouldExit){
          printf("\nExiting...\n");
          break;
      }
  }//while

  udev_unref(udev);
  return 0;
}
