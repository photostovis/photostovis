#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

#include <openssl/sha.h>

#define ECHOFLAGS (ECHO | ECHOE | ECHOK | ECHONL)
#define MAX_UN_PW_SIZE 50
#define BCRYPT_STR_LEN 60

const char *KSalt="Photostovis";

int bcrypt_newhash(const char *pass, int log_rounds, char *hash, size_t hashlen);

int set_disp_mode(int fd,int option){
   int err;
   struct termios term;
   if(tcgetattr(fd,&term)==-1){
     perror("Cannot get the attribution of the terminal");
     return 1;
   }
   if(option)
        term.c_lflag|=ECHOFLAGS;
   else
        term.c_lflag &=~ECHOFLAGS;
   err=tcsetattr(fd,TCSAFLUSH,&term);
   if(err==-1 && err==EINTR){
        perror("Cannot set the attribution of the terminal");
        return 1;
   }
   return 0;
}
int getpasswd(char* passwd, int size){
   int c,n=0;
   do{
      c=getchar();
      if(c!='\n' && c!='\r'){
         passwd[n++]=c;
      }
   }while(c!='\n' && c!='\r' && n<(size-1));
   passwd[n]='\0';
   return n;
}

int main(){
   char password1[MAX_UN_PW_SIZE],password2[MAX_UN_PW_SIZE],username[MAX_UN_PW_SIZE];
   printf("Username: ");
   scanf("%s",username);
   getchar();
   set_disp_mode(STDIN_FILENO,0);
   printf("Password: ");
   getpasswd(password1, MAX_UN_PW_SIZE);
   printf("\nPw again: ");
   getpasswd(password2, MAX_UN_PW_SIZE);
   set_disp_mode(STDIN_FILENO,1);

   printf("\n\n");
   if(strcmp(password1,password2)){
       printf("The 2 passwords do not match!\n");
       return -1;
   }

   //if here, passwords do match

   //create the pw string
   int i,l;
   char pwString[MAX_UN_PW_SIZE+MAX_UN_PW_SIZE+MAX_UN_PW_SIZE+1];
   sprintf(pwString,"%s%s%s",KSalt,username,password1);
   l=strlen(KSalt)+strlen(username);
   for(i=strlen(KSalt);i<l;i++)
       pwString[i]=tolower(pwString[i]);

   //SHA256
   unsigned char pwSha256[SHA256_DIGEST_LENGTH];
   char pwSha256Str[SHA256_DIGEST_LENGTH+SHA256_DIGEST_LENGTH+100];
   SHA256((unsigned char*)pwString,strlen(pwString),pwSha256);

   for(i=0;i<SHA256_DIGEST_LENGTH;i++)
       sprintf(pwSha256Str+i+i,"%02x",pwSha256[i]);
   printf("sha256: %s\n",pwSha256Str);

   //bcrypt
   char pwBcryptStr[BCRYPT_STR_LEN+1];
   bcrypt_newhash(pwSha256Str,12,pwBcryptStr,BCRYPT_STR_LEN+1);


   //done. Output
   printf("auth_user=%s:%s:11111111\n",username,pwBcryptStr);
   return 0;
}
