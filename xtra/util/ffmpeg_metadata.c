#include <stdio.h>

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>


int main(int argc, char **argv)
{
    AVFormatContext *fmt_ctx = NULL;
    AVDictionaryEntry *tag = NULL;
    int ret,i;
    AVStream *st=NULL;

    if (argc != 2) {
        printf("usage: %s <input_file>\n"
               "example program to demonstrate the use of the libavformat metadata API.\n"
               "\n", argv[0]);
        return 1;
    }

    av_register_all();
    printf("cucu1\n");
    if ((ret = avformat_open_input(&fmt_ctx, argv[1], NULL, NULL)))
        return ret;

    printf("Nr streams: %d\n",fmt_ctx->nb_streams);

    for(i=0;i<fmt_ctx->nb_streams; i++)
    {
        printf("Media type: %d codec: %d \n",fmt_ctx->streams[i]->codec->codec_type,fmt_ctx->streams[i]->codec->codec_id);
        if(fmt_ctx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO && fmt_ctx->streams[i]->disposition|AV_DISPOSITION_ATTACHED_PIC)
            st=fmt_ctx->streams[i];
    }

    if(st)
    {
        printf("Attached picture: data=%p, len=%d\n",st->attached_pic.data,st->attached_pic.size);
        if(st->metadata)
            while ((tag = av_dict_get(st->metadata, "", tag, AV_DICT_IGNORE_SUFFIX)))
                printf("Metadata %s=%s\n", tag->key, tag->value);

        //
        FILE *f;
        f=fopen("cucu.jpg","w");
        fwrite(st->attached_pic.data,st->attached_pic.size,1,f);
        fclose(f);

        //AACContext *ac = st->codec->priv_data;
    }

    /*
        if(fmt_ctx->streams[i]->codec->codec_type==AVMEDIA_TYPE_DATA)
        {
            audioStream=i;
            break;
        }
*/


    printf("cucu2\n");

    while ((tag = av_dict_get(fmt_ctx->metadata, "", tag, AV_DICT_IGNORE_SUFFIX)))
        printf("%s=%s\n", tag->key, tag->value);

    avformat_close_input(&fmt_ctx);
    return 0;
}

