#!/bin/sh

v_major=$(cat "src/photostovis.h" | grep KPhotostovisVersionMajor | cut -d' ' -f3)
v_minor=$(cat "src/photostovis.h" | grep KPhotostovisVersionMinor | cut -d' ' -f3)
architecture=$(uname -m)

x="photostovis_${v_major}.${v_minor}_amd64.deb"

echo $x $architecture
sudo cp debian_arch/control.$(echo $architecture) debian/DEBIAN/control
