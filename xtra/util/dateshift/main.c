#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>

#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <utime.h>

#include <errno.h>
#include <fcntl.h>
#include <assert.h>

#include <ftw.h>
#include <regex.h> //regular expressions
#include <arpa/inet.h> //ntohs
#ifdef __APPLE__
#include <sys/syslimits.h>
#else
#include <sys/inotify.h>
#endif

#include <libexif/exif-data.h>
#include "jpeg-data.h"

#define _GNU_SOURCE
#include <math.h> //defines NAN macro

#define DATE_MAX 0x7FFFFFFF
#define EXIF_DATETIME_SIZE 20
const char *KJpegExtension=".jpg";
const char *KJpegExtension2="jpeg";

const char *KShiftedExtension=".shifted";
const int KShiftedExtensionLen=8; //strlen(KShiftedExtension)
char pattern[100];
int patternLen=0;

static int scandir_jpeg_and_pattern_filter(const struct dirent *d){
    //we filter out ., .. and all hidden folders and files. AND WE ALSO FILTER OUT SYNOLOGY-CREATED FOLDERS
    if(d->d_name[0]=='.' || d->d_name[0]=='@')
        return 0;

    int l=0,d_type=DT_UNKNOWN;
    //we return 1 for jpegs
    if(d->d_type==DT_UNKNOWN){
        return 0;
    }
    if(d->d_type==DT_REG || d_type==DT_REG){
        //check extension
        if(!l)l=strlen(d->d_name);
        if(l>4){
            char *extension=(char*)d->d_name+l-4;
            if(!strcasecmp(extension,KJpegExtension) || !strcasecmp(extension,KJpegExtension2)){
                //do we have a pattern? does it match?
                if(patternLen>0 && strncmp(d->d_name,pattern,patternLen)){
                    return 0; //because we have a pattern and there is no match!
                }
                return 1;
            }
        }
    }

    return 0;
}

static void trim_spaces(char *buf){
    int l=strlen(buf)-1;
    while(l>=0 && buf[l]==' ')buf[l--]='\0';
}

static void get_tag(char *buf, int bufLen, ExifData *d, ExifIfd ifd, ExifTag tag){
    //ph_assert(buf,NULL);
    //ph_assert(bufLen>0,NULL);
    /* See if this tag exists */
    ExifEntry *entry = exif_content_get_entry(d->ifd[ifd],tag);
    if(entry)
    {
        /* Get the contents of the tag in human-readable form */
        exif_entry_get_value(entry, buf, bufLen);
        trim_spaces(buf);
        /*
        if(entry->format==EXIF_FORMAT_ASCII)
            printf("Entry: size=%u, data length: %u\n",entry->size,(unsigned)strlen(buf));
        else printf("Entry format: %d\n",entry->format);
        */
    }
    else *buf='\0';
}

static void set_string_tag(ExifIfd ifd, ExifTag tag, ExifData *d, const char *buf){
    //ph_assert(buf,NULL);
    //ph_assert(bufLen>0,NULL);
    unsigned int l=strlen(buf);
    /* See if this tag exists */
    ExifEntry *entry = exif_content_get_entry(d->ifd[ifd],tag);
    if(entry && entry->format==EXIF_FORMAT_ASCII && entry->size>l){
        memcpy(entry->data,buf,l+1);

    } else {
        if(!entry)printf("SOMETHING IS WRONG: entry not found!\n");
        else if(entry->format!=EXIF_FORMAT_ASCII)printf("SOMETHING IS WRONG: entry is not ASCII!\n");
        else if(entry->size<=l)printf("SOMETHING IS WRONG: entry size (%u) is too small (needed at least: %u)!\n",entry->size,l+1);
        else printf("SOMETHING IS WRONG: coding!!!!!\n");
    }
}

static int32_t exifDateTime2UnixTime(const char *dtStr){
    struct tm t;
    memset(&t, 0, sizeof(struct tm));

    char *r=NULL;
    r=strptime(dtStr,"%Y:%m:%d %T",&t);
    if(!r){
        //some old phones have a strange date format
        printf("WARNING: Date with strange format: %s\n",dtStr);
        r=strptime(dtStr,"%Y:%m:%d:%T",&t);
        if(!r)return DATE_MAX;
    }

    if(*r!='\0'){
        printf("Date NOT successfully converted!! r=%s\n",r);
        return DATE_MAX;
    }
    return (int32_t)timegm(&t);
}

static void unixTime2exifDateTime(const int32_t time, char *buf){
    time_t tt=time;
    struct tm t;
    gmtime_r(&tt,&t);

    strftime(buf,1000,"%Y:%m:%d %T",&t);
    //printf("NEW TIME: %s",buf);
}

/****************************************************************************/

int shiftDate(const char *filenameIn, const char *filenameOut, int nrSeconds){
    ExifData *ed=exif_data_new_from_file(filenameIn);
    if(!ed)return -1;

    int changed=0;
    char tempBuf[1024];

    int32_t dt,dt1,dt2,dt3;
    int32_t captureTime=DATE_MAX;
    get_tag(tempBuf,EXIF_DATETIME_SIZE,ed,EXIF_IFD_0,EXIF_TAG_DATE_TIME); //20 is the max length for date-time values
    get_tag(tempBuf+EXIF_DATETIME_SIZE,EXIF_DATETIME_SIZE,ed,EXIF_IFD_EXIF,EXIF_TAG_DATE_TIME_ORIGINAL);
    get_tag(tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE,EXIF_DATETIME_SIZE,ed,EXIF_IFD_EXIF,EXIF_TAG_DATE_TIME_DIGITIZED);
    if(strlen(tempBuf)==EXIF_DATETIME_SIZE-1 &&
            strlen(tempBuf+EXIF_DATETIME_SIZE)==EXIF_DATETIME_SIZE-1 &&
            strlen(tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE)==EXIF_DATETIME_SIZE-1 &&
            !strcmp(tempBuf,tempBuf+EXIF_DATETIME_SIZE) && !strcmp(tempBuf,tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE))
    {
        //all dates are present and they are the same. Digitize only once.
        captureTime=exifDateTime2UnixTime(tempBuf);

        printf("\n%s: All dates present and the same.\n",filenameIn);
        printf("Old time for %s: %s (%d)\n",filenameIn,tempBuf,captureTime);
        captureTime+=nrSeconds;
        unixTime2exifDateTime(captureTime,tempBuf);
        printf("New time for %s: %s (%d)\n",filenameIn,tempBuf,captureTime);

        set_string_tag(EXIF_IFD_0,EXIF_TAG_DATE_TIME,ed,tempBuf);
        set_string_tag(EXIF_IFD_EXIF,EXIF_TAG_DATE_TIME_ORIGINAL,ed,tempBuf);
        set_string_tag(EXIF_IFD_EXIF,EXIF_TAG_DATE_TIME_DIGITIZED,ed,tempBuf);
        changed=1;
    } else {

        //possibly not all dates are present, and possibly not all are the same
        //we select the earliest date available
        if(strlen(tempBuf)==EXIF_DATETIME_SIZE-1){
            dt1=dt=exifDateTime2UnixTime(tempBuf);
            //LOG0("Date from tempBuf1: %s",tempBuf);
            if(dt<captureTime)
                captureTime=dt;
        } else dt1=DATE_MAX;
        if(strlen(tempBuf+EXIF_DATETIME_SIZE)==EXIF_DATETIME_SIZE-1){
            dt2=dt=exifDateTime2UnixTime(tempBuf+EXIF_DATETIME_SIZE);
            //LOG0("Date from tempBuf2: %s",tempBuf+EXIF_DATETIME_SIZE);
            if(dt<captureTime)
                captureTime=dt;
        } else dt2=DATE_MAX;
        if(strlen(tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE)==EXIF_DATETIME_SIZE-1){
            dt3=dt=exifDateTime2UnixTime(tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE);
            //LOG0("Date from tempBuf3: %s",tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE);
            if(dt<captureTime)
                captureTime=dt;
        } else dt3=DATE_MAX;
        //some phones (e.g. Moto G) have the DATE_TIME_DIGITIZED screwed up, and we want to avoid using it instead of the "right" date
        //on the other hand the DATE_TIME_DIGITIZED gets the capture date for edited pictures.
        //the next if catches the case when DATE_TIME_DIGITIZED is bigger than 1 year from the others
        //TODO: this is not a perfect solution
        if(dt1==dt2 && dt1!=dt3 && dt1!=DATE_MAX && dt3!=DATE_MAX && (dt1-dt3>31536000)){
            //LOG0("DIGITIZED TIME is more than 1 year behind. Ignoring.");
            captureTime=dt1;
        }
    }

    if(captureTime==DATE_MAX && (strlen(tempBuf) || strlen(tempBuf+EXIF_DATETIME_SIZE) || strlen(tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE))){
        printf("WARNING: Malformed date:\n");
        printf("%s: #%s#%s#%s#\n",filenameIn,tempBuf,tempBuf+EXIF_DATETIME_SIZE,tempBuf+EXIF_DATETIME_SIZE+EXIF_DATETIME_SIZE);
    }
    if(captureTime!=DATE_MAX && !changed){
        int newCaptureTime=captureTime+nrSeconds;
        printf("Old time for %s: %s (%d)\n",filenameIn,tempBuf,captureTime);
        unixTime2exifDateTime(newCaptureTime,tempBuf);
        printf("New time for %s: %s (%d)\n",filenameIn,tempBuf,newCaptureTime);

        if(captureTime==dt1){
            set_string_tag(EXIF_IFD_0,EXIF_TAG_DATE_TIME,ed,tempBuf);
            changed=1;
        }
        if(captureTime==dt2){
            set_string_tag(EXIF_IFD_EXIF,EXIF_TAG_DATE_TIME_ORIGINAL,ed,tempBuf);
            changed=1;
        }
        if(captureTime==dt3){
            set_string_tag(EXIF_IFD_EXIF,EXIF_TAG_DATE_TIME_DIGITIZED,ed,tempBuf);
            changed=1;
        }
    }


    if(changed){
        //save
        JPEGData *jpgData=jpeg_data_new_from_file(filenameIn);
        if(!jpgData){
            printf("Could not load <<%s>>\n",filenameIn);
            return -1;
        }

        //write the Exif data to a jpeg file
        jpeg_data_set_exif_data (jpgData,ed);
        jpeg_data_save_file(jpgData,filenameOut);
        printf("Saved to <<%s>>",filenameOut);
    } else {
        if(captureTime==DATE_MAX)printf("captureTIme==DATE_MAX\n");
        assert(0);
    }



    printf("\n");
    return 0;
}

int main(int argc, char **argv){
    if(argc!=2 && argc!=3){
        printf("Usage:   dateShift minutes [pattern]\n");
        printf("Example: dateShift 60 IMG_\n");
        return 0;
    }

    char nameOut[PATH_MAX];
    struct dirent **namelist=NULL;
    int i,n,r,nrSeconds,l;

    nrSeconds=strtol(argv[1],NULL,10);
    printf("Nr minutes to add/substract: %d\n",nrSeconds);
    nrSeconds*=60;

    if(argc==3){
        patternLen=strlen(argv[2]);
        if(patternLen>=100){
            printf("Pattern too big!\n");
            return -1;
        }
        strcpy(pattern,argv[2]);
    }

    n=scandir(".",&namelist,scandir_jpeg_and_pattern_filter,NULL);
    for(i=0;i<n;i++){
        l=strlen(namelist[i]->d_name);
        memcpy(nameOut,namelist[i]->d_name,l);
        memcpy(nameOut+l,KShiftedExtension,KShiftedExtensionLen+1);

        r=shiftDate(namelist[i]->d_name,nameOut,nrSeconds);
        if(!r){
            //shift succeeded, delete the old file, replace it with the new file
            rename(nameOut,namelist[i]->d_name);
        }
        free(namelist[i]);
    }
    free(namelist);

    return 0;
}




/*
int main(int argc, char **argv)
{
    if(argc<4 || argc>4){
        printf("Usage: dateshift folderIn folderOut seconds\n");
        return 0;
    }

    char pathIn[PATH_MAX];
    char pathOut[PATH_MAX];
    struct dirent **namelist=NULL;
    int i,n,pathLenIn,pathLenOut,nrSeconds,l;

    strcpy(pathIn,argv[1]);
    pathLenIn=strlen(pathIn);
    if(pathLenIn<1){
        printf("Input path too short\n");
        return 0;
    }
    if(pathIn[pathLenIn-1]!='/')
        pathIn[pathLenIn++]='/';

    strcpy(pathOut,argv[2]);
    pathLenOut=strlen(pathOut);
    if(pathLenOut<1){
        printf("Output path too short\n");
        return 0;
    }
    if(pathOut[pathLenOut-1]!='/')
        pathOut[pathLenOut++]='/';


    nrSeconds=strtol(argv[3],NULL,10);
    printf("Nr secconds to add/substract: %d\n",nrSeconds);

    n=scandir(pathIn,&namelist,NULL,NULL);
    for(i=0;i<n;i++){
        l=strlen(namelist[i]->d_name);
        memcpy(pathIn+pathLenIn,namelist[i]->d_name,l+1);
        memcpy(pathOut+pathLenOut,namelist[i]->d_name,l+1);
        shiftDate(pathIn,pathOut,nrSeconds);
        free(namelist[i]);
    }
    free(namelist);


    return 0;
}*/

