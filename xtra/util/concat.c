#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include <zlib.h>

#define LINE_SIZE 1024
#define FILENAME_LEN 256

const char *KFilenameSrc="../photostovis/web/index.html";
const char *KFilenameAllUncompressed="../photostovis/web/index.all.html";
const char *KFilenameAllCompressed="../photostovis/web/index.all.html.gz";

const char *KCssLine="<link rel=\"stylesheet\" media=\"screen\" href=\"";
const char *KJsLine ="<script type=\"text/javascript\" src=\"";
const char *KJsRemoteLine ="<script type=\"text/javascript\" src=\"http";

void addFile(FILE* fOut, const char *filenameLine){
    char filename[FILENAME_LEN];
    const char *KPath="../photostovis/web/";
    const int KPathLen=strlen(KPath);
    int lineLen=strlen(filenameLine);
    if(lineLen>=FILENAME_LEN-KPathLen)
        lineLen=FILENAME_LEN-KPathLen-1;


    memcpy(filename,KPath,KPathLen);
    memcpy(filename+KPathLen,filenameLine,lineLen+1);
    filename[FILENAME_LEN-1]='\0'; //so we have an end of line
    char *end=strrchr(filename,'"');
    assert(end);
    *end='\0';
    printf("Including file: %s\n",filename);

    FILE *fIn=fopen(filename,"r");
    assert(fIn);
    fseek(fIn,0,SEEK_END);
    long size=ftell(fIn);
    fseek(fIn,0,SEEK_SET);

    char *buf=(char*)malloc(size);
    assert(buf);
    int r=fread(buf,size,1,fIn);
    assert(r==1);
    r=fwrite(buf,size,1,fOut);
    assert(r==1);
    fclose(fIn);
    free(buf);
}

int main(void){
    printf("Concatenating and compressing index.html...\n");
    FILE *fIn=fopen(KFilenameSrc,"r");
    FILE *fOut=fopen(KFilenameAllUncompressed,"w");
    char line[LINE_SIZE],*ret;
    int inCss=0,inJs=0;

    //read the input file and insert stuff
    while(1){
        ret=fgets(line,sizeof(line),fIn);
        if(!ret)break;

        //handle CSS insertions
        if(!strncmp(line,"</style>",8)){
            inCss=1;
            continue;
        }
        if(inCss && !strncmp(line,KCssLine,strlen(KCssLine))){
            //we will replace this line with a file
            addFile(fOut,line+strlen(KCssLine));
            continue;
        }
        if(inCss){
            //all css includes are included, end the </style>
            inCss=0;
            fprintf(fOut,"</style>\n");
            fputs(line,fOut);
            continue;
        }

        //handle JS insertions
        if(!strncmp(line,"</script>",9)){
            inJs=1;
            continue;
        }
        if(inJs && !strncmp(line,KJsLine,strlen(KJsLine)) && strncmp(line,KJsRemoteLine,strlen(KJsRemoteLine))){
            //we will replace this line with a file
            addFile(fOut,line+strlen(KJsLine));
            continue;
        }
        if(inJs){
            //all css includes are included, end the </script>
            inJs=0;
            fprintf(fOut,"</script>\n");
            fputs(line,fOut);
            continue;
        }


        fputs(line,fOut);
    }//while

    fclose(fIn);
    fclose(fOut);
    printf("Done creating index.all.html\n");

    //now compress the file
    gzFile gzfOut=gzopen(KFilenameAllCompressed,"wb9");
    assert(gzfOut);

    fIn=fopen(KFilenameAllUncompressed,"r");
    assert(fIn);
    fseek(fIn,0,SEEK_END);
    long size=ftell(fIn);
    fseek(fIn,0,SEEK_SET);

    char *buf=(char*)malloc(size);
    assert(buf);
    int r=fread(buf,size,1,fIn);
    assert(r==1);
    r=gzwrite(gzfOut,buf,size);
    assert(r==size);
    fclose(fIn);
    free(buf);
    gzclose_w(gzfOut);
    printf("Done creating index.all.html.gz\n");

    return 0;
}
