#define _FILE_OFFSET_BITS 64
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

#ifdef __APPLE__
#include <sys/syslimits.h>
#endif


#include "../src/log.h"
#include "../src/photostovis.h"

#include "../src/minizip/zip.h"

const char *KPhotostovisName="photostovis";
const char *KPhotostovisData="/.photostovis_cache";

const char *KDefaultDocRootLocal="../photostovis/web/";

#ifdef __APPLE__
const char * const KOptionsPicsFolder="/Users/lohanf/Documents/photostovis";
#else
const char * const KOptionsPicsFolder="/data/photostovis";
#endif

#if 0
void sendStatusToConnectedClients(struct md_entry_id const * const __restrict impactedAlbum, const int mustSend){
    //
}

int shouldExit=0;

int scanPictures(){
    int r,forcePicturesFullRescan=0;

    while(1){
        r=lookForPictures(&forcePicturesFullRescan); //includes counting the pictures
        ph_assert(r==0 || r==1,NULL);
        if(shouldExit)
            break;

        ph_assert(statusFlagsCheck(StatusFlagPictureScanThreadActive),NULL);
        statusFlagsAdd(StatusFlagFirstScanDone);

        if(r)//returned by lookForPictures()
            statusFlagsAdd(StatusFlagDoARescan);

        if(statusFlagsCheck(StatusFlagDoARescan)){
            statusFlagsRemove(StatusFlagDoARescan);
            continue;
        }
        LOG_FLUSH;

        //scanning pictures done, now we go to checking thumbnails part
        r=checkAndCreateThumbnails();
        ph_assert(r==0,NULL);
        if(shouldExit)
            break;

        if(statusFlagsCheck(StatusFlagDoARescan)){
            statusFlagsRemove(StatusFlagDoARescan);
            continue;
        }
    }//while(1)

    statusFlagsRemove(StatusFlagPictureScanThreadActive);

    return 0;
}


int main(){
    LOG0("Photostovis tests");

    //Initialize md structures.
    initMd();
    struct ps_md_config *mdConfigW=getMdConfigWritable();
    //the path to web document root
    mdConfigW->srvDocRoot=(char*)KDefaultDocRootLocal;
    mdConfigW->rootFolder=(char*)KOptionsPicsFolder;
#ifndef __APPLE__
    //initialize inotify
    mdConfigW->inotifyFd=inotify_init();
    LOG0return(mdConfigW->inotifyFd==-1,EXIT_DO_NOT_RESTART,"inotify_init() failed: %s",strerror(errno));
#endif
    releaseMdConfigW(&mdConfigW);
    scanPictures();
#ifndef __APPLE__
    mdConfigW=getMdConfigWritable();
    close(mdConfigW->inotifyFd);
    mdConfigW->inotifyFd=-1;
    releaseMdConfigW(&mdConfigW);
#endif

    return 0;
}
#endif

const char *KJpegExtension=".jpg";
const char *KMp4Extension=".mp4";
const char *KMovExtension=".mov";

static int scandir_known_files_filter(const struct dirent *d)
{
    //we filter out ., .. and all hidden folders and files. AND WE ALSO FILTER OUT SYNOLOGY-CREATED FOLDERS
    if(d->d_name[0]=='.' || d->d_name[0]=='@')
        return 0;

    int r,l=0,d_type=DT_UNKNOWN;
    //we return 1 for jpegs and folders
    if(d->d_type==DT_UNKNOWN){
        //we must find the type
        ph_assert(0,NULL);
    }
    if(d->d_type==DT_DIR || d_type==DT_DIR){
        return 0; //TODO: we skip folders for the moment
    }
    else if(d->d_type==DT_REG || d_type==DT_REG){
        //check extension
        if(!l)l=strlen(d->d_name);
        if(l>4){
            char *extension=(char*)d->d_name+l-4;
            if(!strcasecmp(extension,KJpegExtension) || !strcasecmp(extension,KMp4Extension) || !strcasecmp(extension,KMovExtension))r=1;
            else r=0;
        } else r=0;
    }
    else return 0;//skipping this file

    return r;
}

int makeZipFromAlbum(const char *folder, const char *zipFilename){

    zipFile zf=zipOpen64(zipFilename,APPEND_STATUS_CREATE);
    LOG0return(!zf,-1,"ERROR: could not open the zip output file (%s)",zipFilename);

    char path[PATH_MAX];
    char buf[ZIP_BUF_SIZE];
    zip_fileinfo zi;
    FILE *f;
    struct dirent **namelist;
    int i,r,pathLen,bufLen;
    uint64_t totalBytes=0;
    int n=scandir(folder,&namelist,&scandir_known_files_filter,NULL);
    ph_assert(n>=0,NULL);
    pathLen=strlen(folder);
    if(pathLen>0){
        memcpy(path,folder,pathLen+1);
        if(path[pathLen-1]!='/')
            path[pathLen++]='/';
    }
    path[pathLen]='\0';

    for(i=0;i<n;i++){
        struct dirent *d=namelist[i];
        //open the file to compress
        memcpy(path+pathLen,d->d_name,strlen(d->d_name)+1);
        f=fopen(path,"r");
        LOG0c_continue(!path,"WARNING: File %s could not be opened. Skipping.",path);

        //set the additional data and add the file to the zip
        memset(&zi,0,sizeof(zip_fileinfo));
        //TODO: fill-in time
        r=zipOpenNewFileInZip64(zf,d->d_name,&zi,NULL,0,NULL,0,NULL,0,0,1);
        LOG0close_return(r!=ZIP_OK,-1,fclose(f);f=NULL;zipClose(zf,NULL);zf=NULL;for(;i<n;i++)free(namelist[i]);free(namelist),
                "ERROR opening %s in zipfile. Exiting zip creration.",d->d_name);

        //read the file and add it to the zipfile
        while(1){
            bufLen=fread(buf,1,ZIP_BUF_SIZE,f);
            LOG0close_return(bufLen<0,-1,fclose(f);f=NULL;zipClose(zf,NULL);zf=NULL;for(;i<n;i++)free(namelist[i]);free(namelist),
                    "ERROR reading from file (%s): %s. Exiting zip creration.",path,strerror(errno));
            if(!bufLen)break;

            //if here, we add the data to the zip file
            r=zipWriteInFileInZip (zf,buf,bufLen);
            LOG0close_return(r!=ZIP_OK,-1,fclose(f);f=NULL;zipClose(zf,NULL);zf=NULL;for(;i<n;i++)free(namelist[i]);free(namelist),
                    "ERROR writing data into zipfile. Exiting zip creration.");
            //increase the number of processed bytes
            totalBytes+=bufLen;
        }
        //we are done with this file
        fclose(f);f=NULL;
        r=zipCloseFileInZip(zf);
        LOG0close_return(r!=ZIP_OK,-1,zipClose(zf,NULL);zf=NULL;for(;i<n;i++)free(namelist[i]);free(namelist),
                "ERROR writing data into zipfile. Exiting zip creration.");
        free(d);
    }
    //close the zipfile and free the data
    free(namelist);namelist=NULL;
    zipClose(zf,NULL);zf=NULL;

    return 0;
}

int main(){
    makeZipFromAlbum("./pics","test.zip");
    return 0;
}
