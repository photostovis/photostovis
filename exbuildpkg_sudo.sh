#!/bin/sh
#set -x

#get the version and architecture
v_major=$(cat "src/photostovis.h" | grep "#define KPhotostovisVersionMajor" | cut -d' ' -f3)
v_minor=$(cat "src/photostovis.h" | grep "#define KPhotostovisVersionMinor" | cut -d' ' -f3)
architecture=$(uname -m)
#remove old files
sudo rm -f phextras_*.deb
sudo rm -f pkg/phextras/usr/bin/*
sudo rm -f pkg/phextras/DEBIAN/md5sums
sudo rm -f pkg/phextras/DEBIAN/control
#copy the proper control file
cp pkg/phextras_arch/control.$(echo $architecture) pkg/phextras/DEBIAN/control
#append the version to the control file, e.g.: Version: 2.30-1
echo "Version: ${v_major}.${v_minor}-1" >> pkg/phextras/DEBIAN/control

#copy the photostovis binary
sudo mkdir -p pkg/phextras/usr/bin
sudo cp /usr/local/bin/ffmpeg pkg/phextras/usr/bin/photostovis_ffmpeg
#create the md5 sums
cd pkg/phextras
sudo find . -type f ! -regex '.*.hg.*' ! -regex '.*?debian-binary.*' ! -regex '.*?DEBIAN.*' -printf '%P ' | xargs md5sum > DEBIAN/md5sums
sudo chown root:root DEBIAN/md5sums
sudo chmod 0644 DEBIAN/md5sums
cd ../..
#build the package
dpkg-deb --build pkg/phextras phextras_$(echo "${v_major}.${v_minor}-1_${architecture}").deb
